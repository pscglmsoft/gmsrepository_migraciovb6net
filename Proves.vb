Imports System.Text

Public Class Proves

    Function ReadIni(ByVal Section As String, ByVal KeyName As String, ByVal Default_Renamed As String, ByVal FileName As String) As String
        Dim Verif, nSize As Integer
        Dim ReturnedString As New StringBuilder(255)

        On Error GoTo Err_ReadIni
        ''INCORRECTEReturnedString = ""
        ''INCORRECTEnSize = Len(ReturnedString)
        nSize = ReturnedString.Capacity

        ReadIni = ""
        Verif = SafeNativeMethods.GetPrivateProfileString(Section, KeyName, Default_Renamed, ReturnedString, nSize, FileName)
        If Verif <> 0 Then
            ReadIni = Left(ReturnedString.ToString, Verif)
        End If

        Exit Function

Err_ReadIni:
        ReadIni = ""
        Exit Function

    End Function


End Class
