﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmTestos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lvTestos = New System.Windows.Forms.ListView()
        Me.Sel = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.descripcio = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.esRegex = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.expressioRegularCerca = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.expressioRegularSubstitucio = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.compara = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.operador = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnExecutarTestos = New System.Windows.Forms.Button()
        Me.lvResultat = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnSubstitucio = New System.Windows.Forms.Button()
        Me.ContextMenuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OcultaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ObrirFitxerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblNumFitxer = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTotalFitxers = New System.Windows.Forms.Label()
        Me.lblFitxer = New System.Windows.Forms.Label()
        Me.ContextMenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'lvTestos
        '
        Me.lvTestos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvTestos.CheckBoxes = True
        Me.lvTestos.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Sel, Me.descripcio, Me.esRegex, Me.expressioRegularCerca, Me.expressioRegularSubstitucio, Me.compara, Me.operador})
        Me.lvTestos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTestos.FullRowSelect = True
        Me.lvTestos.GridLines = True
        Me.lvTestos.HideSelection = False
        Me.lvTestos.Location = New System.Drawing.Point(11, 42)
        Me.lvTestos.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.lvTestos.Name = "lvTestos"
        Me.lvTestos.Size = New System.Drawing.Size(1400, 293)
        Me.lvTestos.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvTestos.TabIndex = 30
        Me.lvTestos.UseCompatibleStateImageBehavior = False
        Me.lvTestos.View = System.Windows.Forms.View.Details
        '
        'Sel
        '
        Me.Sel.Text = "Sel"
        '
        'descripcio
        '
        Me.descripcio.Text = "Descripció"
        Me.descripcio.Width = 371
        '
        'esRegex
        '
        Me.esRegex.Text = "es Regex ?"
        '
        'expressioRegularCerca
        '
        Me.expressioRegularCerca.Text = "Expressió regular"
        Me.expressioRegularCerca.Width = 251
        '
        'expressioRegularSubstitucio
        '
        Me.expressioRegularSubstitucio.Text = "Expressio regular substitucio"
        Me.expressioRegularSubstitucio.Width = 218
        '
        'compara
        '
        Me.compara.Text = "Compara"
        '
        'operador
        '
        Me.operador.Text = "Operador"
        '
        'btnExecutarTestos
        '
        Me.btnExecutarTestos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExecutarTestos.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExecutarTestos.Location = New System.Drawing.Point(1219, 615)
        Me.btnExecutarTestos.Name = "btnExecutarTestos"
        Me.btnExecutarTestos.Size = New System.Drawing.Size(95, 35)
        Me.btnExecutarTestos.TabIndex = 31
        Me.btnExecutarTestos.Text = "Buscar"
        Me.btnExecutarTestos.UseVisualStyleBackColor = True
        '
        'lvResultat
        '
        Me.lvResultat.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvResultat.CheckBoxes = True
        Me.lvResultat.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5})
        Me.lvResultat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvResultat.FullRowSelect = True
        Me.lvResultat.GridLines = True
        Me.lvResultat.HideSelection = False
        Me.lvResultat.Location = New System.Drawing.Point(11, 341)
        Me.lvResultat.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.lvResultat.Name = "lvResultat"
        Me.lvResultat.Size = New System.Drawing.Size(1400, 267)
        Me.lvResultat.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvResultat.TabIndex = 32
        Me.lvResultat.UseCompatibleStateImageBehavior = False
        Me.lvResultat.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Sel"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Linia"
        Me.ColumnHeader2.Width = 684
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Fitxer"
        Me.ColumnHeader3.Width = 339
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Possible substitucio"
        Me.ColumnHeader4.Width = 200
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Num. Linia"
        '
        'btnSubstitucio
        '
        Me.btnSubstitucio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubstitucio.Location = New System.Drawing.Point(1320, 615)
        Me.btnSubstitucio.Name = "btnSubstitucio"
        Me.btnSubstitucio.Size = New System.Drawing.Size(91, 35)
        Me.btnSubstitucio.TabIndex = 33
        Me.btnSubstitucio.Text = "Subtituir"
        Me.btnSubstitucio.UseVisualStyleBackColor = True
        '
        'ContextMenuStrip
        '
        Me.ContextMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OcultaToolStripMenuItem, Me.ObrirFitxerToolStripMenuItem})
        Me.ContextMenuStrip.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip.Size = New System.Drawing.Size(132, 48)
        Me.ContextMenuStrip.Text = "Oculta"
        '
        'OcultaToolStripMenuItem
        '
        Me.OcultaToolStripMenuItem.Name = "OcultaToolStripMenuItem"
        Me.OcultaToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.OcultaToolStripMenuItem.Text = "Elimina"
        '
        'ObrirFitxerToolStripMenuItem
        '
        Me.ObrirFitxerToolStripMenuItem.Name = "ObrirFitxerToolStripMenuItem"
        Me.ObrirFitxerToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.ObrirFitxerToolStripMenuItem.Text = "Obrir fitxer"
        '
        'lblNumFitxer
        '
        Me.lblNumFitxer.AutoSize = True
        Me.lblNumFitxer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumFitxer.Location = New System.Drawing.Point(18, 627)
        Me.lblNumFitxer.Name = "lblNumFitxer"
        Me.lblNumFitxer.Size = New System.Drawing.Size(0, 16)
        Me.lblNumFitxer.TabIndex = 34
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(45, 627)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(26, 16)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "de"
        '
        'lblTotalFitxers
        '
        Me.lblTotalFitxers.AutoSize = True
        Me.lblTotalFitxers.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFitxers.Location = New System.Drawing.Point(63, 627)
        Me.lblTotalFitxers.Name = "lblTotalFitxers"
        Me.lblTotalFitxers.Size = New System.Drawing.Size(0, 16)
        Me.lblTotalFitxers.TabIndex = 36
        '
        'lblFitxer
        '
        Me.lblFitxer.AutoSize = True
        Me.lblFitxer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFitxer.Location = New System.Drawing.Point(112, 627)
        Me.lblFitxer.Name = "lblFitxer"
        Me.lblFitxer.Size = New System.Drawing.Size(0, 16)
        Me.lblFitxer.TabIndex = 37
        '
        'frmTestos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1440, 662)
        Me.Controls.Add(Me.lblFitxer)
        Me.Controls.Add(Me.lblTotalFitxers)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblNumFitxer)
        Me.Controls.Add(Me.btnSubstitucio)
        Me.Controls.Add(Me.lvResultat)
        Me.Controls.Add(Me.btnExecutarTestos)
        Me.Controls.Add(Me.lvTestos)
        Me.Name = "frmTestos"
        Me.Text = "frmTestos"
        Me.ContextMenuStrip.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lvTestos As Windows.Forms.ListView
    Friend WithEvents descripcio As Windows.Forms.ColumnHeader
    Friend WithEvents esRegex As Windows.Forms.ColumnHeader
    Friend WithEvents expressioRegularCerca As Windows.Forms.ColumnHeader
    Friend WithEvents btnExecutarTestos As Windows.Forms.Button
    Friend WithEvents expressioRegularSubstitucio As Windows.Forms.ColumnHeader
    Friend WithEvents Sel As Windows.Forms.ColumnHeader
    Friend WithEvents lvResultat As Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As Windows.Forms.ColumnHeader
    Friend WithEvents compara As Windows.Forms.ColumnHeader
    Friend WithEvents operador As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As Windows.Forms.ColumnHeader
    Friend WithEvents btnSubstitucio As Windows.Forms.Button
    Friend WithEvents ColumnHeader5 As Windows.Forms.ColumnHeader
    Friend WithEvents ContextMenuStrip As Windows.Forms.ContextMenuStrip
    Friend WithEvents OcultaToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents ObrirFitxerToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblNumFitxer As Windows.Forms.Label
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents lblTotalFitxers As Windows.Forms.Label
    Friend WithEvents lblFitxer As Windows.Forms.Label
End Class
