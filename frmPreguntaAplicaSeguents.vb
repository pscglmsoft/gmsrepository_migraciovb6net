﻿Public Class frmPreguntaAplicaSeguents

    Dim _pregunta As String
    Dim _descripcio As String


    Public Sub New(ByVal pregunta As String, ByVal descripcio As String)
        MyBase.New

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        _pregunta = pregunta
        _descripcio = descripcio
    End Sub

    Public Property AplicaSeguents() As Boolean

    Public Property Resposta() As Boolean

    Private Sub frmPreguntaAplicaSeguents_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Label1.Text = _pregunta
        Me.Label2.Text = _descripcio
    End Sub

    Private Sub btnSI_Click(sender As Object, e As EventArgs) Handles btnSI.Click
        Resposta = True
        Me.Close()
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        AplicaSeguents = CheckBox1.Checked
    End Sub

    Private Sub btnNO_Click(sender As Object, e As EventArgs) Handles btnNO.Click
        Resposta = False
        Me.Close()
    End Sub
End Class