﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Substituir
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ' Dim frmkkkk as Object

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TreeListViewItemCollectionComparer1 As System.Windows.Forms.TreeListViewItemCollection.TreeListViewItemCollectionComparer = New System.Windows.Forms.TreeListViewItemCollection.TreeListViewItemCollectionComparer()
        Dim ListViewGroup1 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup2 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("")
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("")
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("")
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Substituir))
        Me.cboDirectoryVB6 = New System.Windows.Forms.ComboBox()
        Me.btnCercaVB6ModificatsDesDe = New System.Windows.Forms.Button()
        Me.TreeViewVB6 = New System.Windows.Forms.TreeView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkDiferenciesDreta = New System.Windows.Forms.CheckBox()
        Me.chkDiferenciesEsquerra = New System.Windows.Forms.CheckBox()
        Me.chkMigrarBackup = New System.Windows.Forms.CheckBox()
        Me.chkSelectAll = New System.Windows.Forms.CheckBox()
        Me.chkInclouFormsNous = New System.Windows.Forms.CheckBox()
        Me.dtDesde = New System.Windows.Forms.DateTimePicker()
        Me.lblProcessant = New System.Windows.Forms.Label()
        Me.lblEtiqueta = New System.Windows.Forms.Label()
        Me.chkInclouConfiguracio = New System.Windows.Forms.CheckBox()
        Me.chkInclouProyecto1 = New System.Windows.Forms.CheckBox()
        Me.TreeViewNET = New System.Windows.Forms.TreeView()
        Me.cboDirectoryNet = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblTrobats_1 = New System.Windows.Forms.Label()
        Me.Label_1 = New System.Windows.Forms.Label()
        Me.Label_3 = New System.Windows.Forms.Label()
        Me.lblTrobatModulsVB6 = New System.Windows.Forms.Label()
        Me.lstItemsFoundVB6_2 = New System.Windows.Forms.ListView()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnProjectesVB6 = New System.Windows.Forms.Button()
        Me.lstItemsFoundVB6 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkNomesProjecte = New System.Windows.Forms.CheckBox()
        Me.btnCercaNET = New System.Windows.Forms.Button()
        Me.lblTrobats_2 = New System.Windows.Forms.Label()
        Me.chkMigrarCodi = New System.Windows.Forms.CheckBox()
        Me.chkMigrarDesigner = New System.Windows.Forms.CheckBox()
        Me.Label_2 = New System.Windows.Forms.Label()
        Me.lblTrobatClassesNet = New System.Windows.Forms.Label()
        Me.Label_4 = New System.Windows.Forms.Label()
        Me.lstItemsFoundNet_2 = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lstItemsFoundNet = New System.Windows.Forms.ListView()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnCrearBackups = New System.Windows.Forms.Button()
        Me.cboDirectoryVB6_BACKUP = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ContextMenuStripProjectesNET = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DiferenciesProjectesNet_MenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiferenciesCarpetesNet_MenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ObrirProjecteNet_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarProjecteNet_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ObrirCarpetaProjecteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarProjecteNet_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloneProjecteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReferenciesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WinMergeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpgradeSupportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStrip_FormsVB6 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EliminarFormsVB6_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopiarBackupAMPNetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStrip_FormsNET = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EliminarFormsNet_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeleccionarFormsNet_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStripModuls = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EliminarModuls_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStripClasses = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EliminarClasses_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStripProjectesVB6 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItemObrirProjecte = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarProjecteVB6_ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ObtenirReferencieVB6_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.lblFitxersModificats = New System.Windows.Forms.Label()
        Me.chkInclouProjectesNet_Nous = New System.Windows.Forms.CheckBox()
        Me.lblProjectesVB6 = New System.Windows.Forms.Label()
        Me.lblProjectesNET = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnDiferenciesProjectes = New System.Windows.Forms.Button()
        Me.btnDiferenciesCarpetes = New System.Windows.Forms.Button()
        Me.chkMigrarNomesModificats = New System.Windows.Forms.CheckBox()
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnPreWizard = New System.Windows.Forms.Button()
        Me.btnFoto = New System.Windows.Forms.Button()
        Me.btnWizard2008 = New System.Windows.Forms.Button()
        Me.btnTestos = New System.Windows.Forms.Button()
        Me.btnCopiarBackupNet_to_Net = New System.Windows.Forms.Button()
        Me.btnPostMigracio = New System.Windows.Forms.Button()
        Me.btnValidacions = New System.Windows.Forms.Button()
        Me.btnCleanMigrar = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.treeListViewUpgrades = New System.Windows.Forms.TreeListView()
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader16 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.chkTots = New System.Windows.Forms.CheckBox()
        Me.chkCritic = New System.Windows.Forms.CheckBox()
        Me.chkWarning = New System.Windows.Forms.CheckBox()
        Me.chkAltres = New System.Windows.Forms.CheckBox()
        Me.chkResolt = New System.Windows.Forms.CheckBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lvWarningsWorkAround = New System.Windows.Forms.ListView()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.lvTwipsPixels = New System.Windows.Forms.ListView()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.optTwipsPixelsNomesNumeros = New System.Windows.Forms.RadioButton()
        Me.optTwipsPixelsIncorrectes = New System.Windows.Forms.RadioButton()
        Me.optTwipsPixelsCorrectes = New System.Windows.Forms.RadioButton()
        Me.optTwipsPixelsTots = New System.Windows.Forms.RadioButton()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.lviAsObject = New System.Windows.Forms.ListView()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.lviFontsTrueType = New System.Windows.Forms.ListView()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.lviTreeViewAxGroupBox = New System.Windows.Forms.ListView()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.chkFitxerMigrat = New System.Windows.Forms.CheckBox()
        Me.chkNoTotalmentRedissenyat = New System.Windows.Forms.CheckBox()
        Me.chkFitxerNoExisteix = New System.Windows.Forms.CheckBox()
        Me.chkFitxerValidar = New System.Windows.Forms.CheckBox()
        Me.chkFitxerRedissenyar = New System.Windows.Forms.CheckBox()
        Me.chkFitxerMigrar = New System.Windows.Forms.CheckBox()
        Me.chkFitxerNou = New System.Windows.Forms.CheckBox()
        Me.lviFormsRedisseny = New System.Windows.Forms.ListView()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.txtCercaWorkAround = New System.Windows.Forms.TextBox()
        Me.chkNA = New System.Windows.Forms.CheckBox()
        Me.chkNoRevisats = New System.Windows.Forms.CheckBox()
        Me.chkRevisats = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.chkADO = New System.Windows.Forms.CheckBox()
        Me.chkDatabase = New System.Windows.Forms.CheckBox()
        Me.chkClasses = New System.Windows.Forms.CheckBox()
        Me.chkFormsControls = New System.Windows.Forms.CheckBox()
        Me.chkLanguage = New System.Windows.Forms.CheckBox()
        Me.chkGeneral = New System.Windows.Forms.CheckBox()
        Me.chkBUG = New System.Windows.Forms.CheckBox()
        Me.chkHOWTO = New System.Windows.Forms.CheckBox()
        Me.chkINFO = New System.Windows.Forms.CheckBox()
        Me.chkPRB = New System.Windows.Forms.CheckBox()
        Me.lviArquitectura = New System.Windows.Forms.ListView()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.lviBugs = New System.Windows.Forms.ListView()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.lviColeccio1_0 = New System.Windows.Forms.ListView()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lviFormularis_Sense_Caption = New System.Windows.Forms.ListView()
        Me.TabPage12 = New System.Windows.Forms.TabPage()
        Me.lviNomClasseUserControl_igual_nomInstancia = New System.Windows.Forms.ListView()
        Me.TabPage13 = New System.Windows.Forms.TabPage()
        Me.lvFormLoad = New System.Windows.Forms.ListView()
        Me.TabPage14 = New System.Windows.Forms.TabPage()
        Me.lvErrorsMigracio = New System.Windows.Forms.ListView()
        Me.chkMigrarTwipsPixels = New System.Windows.Forms.CheckBox()
        Me.chkTwipsPixelsDuplicades = New System.Windows.Forms.CheckBox()
        Me.lblProces = New System.Windows.Forms.Label()
        Me.lblItemProcessat = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnRefrescarVista = New System.Windows.Forms.Button()
        Me.btnSincrBACKUP_NET = New System.Windows.Forms.Button()
        Me.chkMigracioIncremental = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.ContextMenuStripProjectesNET.SuspendLayout()
        Me.ContextMenuStrip_FormsVB6.SuspendLayout()
        Me.ContextMenuStrip_FormsNET.SuspendLayout()
        Me.ContextMenuStripModuls.SuspendLayout()
        Me.ContextMenuStripClasses.SuspendLayout()
        Me.ContextMenuStripProjectesVB6.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage10.SuspendLayout()
        Me.TabPage11.SuspendLayout()
        Me.TabPage12.SuspendLayout()
        Me.TabPage13.SuspendLayout()
        Me.TabPage14.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboDirectoryVB6
        '
        Me.cboDirectoryVB6.FormattingEnabled = True
        Me.cboDirectoryVB6.Location = New System.Drawing.Point(12, 25)
        Me.cboDirectoryVB6.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.cboDirectoryVB6.Name = "cboDirectoryVB6"
        Me.cboDirectoryVB6.Size = New System.Drawing.Size(328, 21)
        Me.cboDirectoryVB6.TabIndex = 3
        '
        'btnCercaVB6ModificatsDesDe
        '
        Me.btnCercaVB6ModificatsDesDe.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnCercaVB6ModificatsDesDe.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCercaVB6ModificatsDesDe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCercaVB6ModificatsDesDe.Location = New System.Drawing.Point(161, 24)
        Me.btnCercaVB6ModificatsDesDe.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnCercaVB6ModificatsDesDe.Name = "btnCercaVB6ModificatsDesDe"
        Me.btnCercaVB6ModificatsDesDe.Size = New System.Drawing.Size(143, 30)
        Me.btnCercaVB6ModificatsDesDe.TabIndex = 5
        Me.btnCercaVB6ModificatsDesDe.Text = "Modificats  des de"
        Me.btnCercaVB6ModificatsDesDe.UseVisualStyleBackColor = True
        '
        'TreeViewVB6
        '
        Me.TreeViewVB6.Location = New System.Drawing.Point(11, 48)
        Me.TreeViewVB6.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.TreeViewVB6.Name = "TreeViewVB6"
        Me.TreeViewVB6.Size = New System.Drawing.Size(328, 453)
        Me.TreeViewVB6.TabIndex = 8
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.chkDiferenciesDreta)
        Me.GroupBox1.Controls.Add(Me.chkDiferenciesEsquerra)
        Me.GroupBox1.Location = New System.Drawing.Point(656, 3)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox1.Size = New System.Drawing.Size(870, 30)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'chkDiferenciesDreta
        '
        Me.chkDiferenciesDreta.AutoSize = True
        Me.chkDiferenciesDreta.Checked = True
        Me.chkDiferenciesDreta.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDiferenciesDreta.Location = New System.Drawing.Point(140, 9)
        Me.chkDiferenciesDreta.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkDiferenciesDreta.Name = "chkDiferenciesDreta"
        Me.chkDiferenciesDreta.Size = New System.Drawing.Size(106, 17)
        Me.chkDiferenciesDreta.TabIndex = 43
        Me.chkDiferenciesDreta.Text = "Diferencies dreta"
        Me.chkDiferenciesDreta.UseVisualStyleBackColor = True
        '
        'chkDiferenciesEsquerra
        '
        Me.chkDiferenciesEsquerra.AutoSize = True
        Me.chkDiferenciesEsquerra.Checked = True
        Me.chkDiferenciesEsquerra.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDiferenciesEsquerra.Location = New System.Drawing.Point(12, 10)
        Me.chkDiferenciesEsquerra.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkDiferenciesEsquerra.Name = "chkDiferenciesEsquerra"
        Me.chkDiferenciesEsquerra.Size = New System.Drawing.Size(123, 17)
        Me.chkDiferenciesEsquerra.TabIndex = 34
        Me.chkDiferenciesEsquerra.Text = "Diferencies esquerra"
        Me.chkDiferenciesEsquerra.UseVisualStyleBackColor = True
        '
        'chkMigrarBackup
        '
        Me.chkMigrarBackup.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkMigrarBackup.AutoSize = True
        Me.chkMigrarBackup.Checked = True
        Me.chkMigrarBackup.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMigrarBackup.Location = New System.Drawing.Point(464, 27)
        Me.chkMigrarBackup.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkMigrarBackup.Name = "chkMigrarBackup"
        Me.chkMigrarBackup.Size = New System.Drawing.Size(80, 21)
        Me.chkMigrarBackup.TabIndex = 48
        Me.chkMigrarBackup.Text = "Backup"
        Me.chkMigrarBackup.UseVisualStyleBackColor = True
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelectAll.Location = New System.Drawing.Point(9, 59)
        Me.chkSelectAll.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(99, 17)
        Me.chkSelectAll.TabIndex = 32
        Me.chkSelectAll.Text = "Selecciona tots"
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'chkInclouFormsNous
        '
        Me.chkInclouFormsNous.AutoSize = True
        Me.chkInclouFormsNous.Enabled = False
        Me.chkInclouFormsNous.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInclouFormsNous.Location = New System.Drawing.Point(114, 59)
        Me.chkInclouFormsNous.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkInclouFormsNous.Name = "chkInclouFormsNous"
        Me.chkInclouFormsNous.Size = New System.Drawing.Size(109, 17)
        Me.chkInclouFormsNous.TabIndex = 33
        Me.chkInclouFormsNous.Text = "Inclou forms nous"
        Me.chkInclouFormsNous.UseVisualStyleBackColor = True
        '
        'dtDesde
        '
        Me.dtDesde.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.dtDesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDesde.Location = New System.Drawing.Point(202, 64)
        Me.dtDesde.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.dtDesde.Name = "dtDesde"
        Me.dtDesde.Size = New System.Drawing.Size(102, 23)
        Me.dtDesde.TabIndex = 11
        '
        'lblProcessant
        '
        Me.lblProcessant.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblProcessant.AutoSize = True
        Me.lblProcessant.BackColor = System.Drawing.Color.Transparent
        Me.lblProcessant.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcessant.ForeColor = System.Drawing.Color.Red
        Me.lblProcessant.Location = New System.Drawing.Point(796, 908)
        Me.lblProcessant.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblProcessant.Name = "lblProcessant"
        Me.lblProcessant.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProcessant.Size = New System.Drawing.Size(0, 17)
        Me.lblProcessant.TabIndex = 12
        '
        'lblEtiqueta
        '
        Me.lblEtiqueta.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEtiqueta.AutoSize = True
        Me.lblEtiqueta.BackColor = System.Drawing.Color.Transparent
        Me.lblEtiqueta.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqueta.ForeColor = System.Drawing.Color.Red
        Me.lblEtiqueta.Location = New System.Drawing.Point(16, 712)
        Me.lblEtiqueta.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblEtiqueta.Name = "lblEtiqueta"
        Me.lblEtiqueta.Size = New System.Drawing.Size(0, 17)
        Me.lblEtiqueta.TabIndex = 13
        '
        'chkInclouConfiguracio
        '
        Me.chkInclouConfiguracio.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chkInclouConfiguracio.AutoSize = True
        Me.chkInclouConfiguracio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInclouConfiguracio.Location = New System.Drawing.Point(5, 65)
        Me.chkInclouConfiguracio.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkInclouConfiguracio.Name = "chkInclouConfiguracio"
        Me.chkInclouConfiguracio.Size = New System.Drawing.Size(117, 17)
        Me.chkInclouConfiguracio.TabIndex = 16
        Me.chkInclouConfiguracio.Text = "Inclou Configuracio"
        Me.chkInclouConfiguracio.UseVisualStyleBackColor = True
        '
        'chkInclouProyecto1
        '
        Me.chkInclouProyecto1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chkInclouProyecto1.AutoSize = True
        Me.chkInclouProyecto1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInclouProyecto1.Location = New System.Drawing.Point(5, 86)
        Me.chkInclouProyecto1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkInclouProyecto1.Name = "chkInclouProyecto1"
        Me.chkInclouProyecto1.Size = New System.Drawing.Size(106, 17)
        Me.chkInclouProyecto1.TabIndex = 17
        Me.chkInclouProyecto1.Text = "Inclou Proyecto1"
        Me.chkInclouProyecto1.UseVisualStyleBackColor = True
        '
        'TreeViewNET
        '
        Me.TreeViewNET.Location = New System.Drawing.Point(344, 48)
        Me.TreeViewNET.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.TreeViewNET.Name = "TreeViewNET"
        Me.TreeViewNET.Size = New System.Drawing.Size(303, 477)
        Me.TreeViewNET.TabIndex = 18
        '
        'cboDirectoryNet
        '
        Me.cboDirectoryNet.FormattingEnabled = True
        Me.cboDirectoryNet.Location = New System.Drawing.Point(344, 25)
        Me.cboDirectoryNet.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.cboDirectoryNet.Name = "cboDirectoryNet"
        Me.cboDirectoryNet.Size = New System.Drawing.Size(303, 21)
        Me.cboDirectoryNet.TabIndex = 19
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Honeydew
        Me.GroupBox2.Controls.Add(Me.lblTrobats_1)
        Me.GroupBox2.Controls.Add(Me.Label_1)
        Me.GroupBox2.Controls.Add(Me.Label_3)
        Me.GroupBox2.Controls.Add(Me.lblTrobatModulsVB6)
        Me.GroupBox2.Controls.Add(Me.lstItemsFoundVB6_2)
        Me.GroupBox2.Controls.Add(Me.btnProjectesVB6)
        Me.GroupBox2.Controls.Add(Me.lstItemsFoundVB6)
        Me.GroupBox2.Controls.Add(Me.chkInclouProyecto1)
        Me.GroupBox2.Controls.Add(Me.chkInclouConfiguracio)
        Me.GroupBox2.Controls.Add(Me.btnCercaVB6ModificatsDesDe)
        Me.GroupBox2.Controls.Add(Me.dtDesde)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(656, 32)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(314, 495)
        Me.GroupBox2.TabIndex = 30
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Arquitectura VB6"
        '
        'lblTrobats_1
        '
        Me.lblTrobats_1.AutoSize = True
        Me.lblTrobats_1.ForeColor = System.Drawing.Color.Red
        Me.lblTrobats_1.Location = New System.Drawing.Point(275, 322)
        Me.lblTrobats_1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTrobats_1.Name = "lblTrobats_1"
        Me.lblTrobats_1.Size = New System.Drawing.Size(0, 17)
        Me.lblTrobats_1.TabIndex = 42
        '
        'Label_1
        '
        Me.Label_1.AutoSize = True
        Me.Label_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_1.ForeColor = System.Drawing.Color.Red
        Me.Label_1.Location = New System.Drawing.Point(8, 322)
        Me.Label_1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label_1.Name = "Label_1"
        Me.Label_1.Size = New System.Drawing.Size(0, 16)
        Me.Label_1.TabIndex = 41
        '
        'Label_3
        '
        Me.Label_3.AutoSize = True
        Me.Label_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_3.ForeColor = System.Drawing.Color.Navy
        Me.Label_3.Location = New System.Drawing.Point(9, 554)
        Me.Label_3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label_3.Name = "Label_3"
        Me.Label_3.Size = New System.Drawing.Size(0, 16)
        Me.Label_3.TabIndex = 34
        '
        'lblTrobatModulsVB6
        '
        Me.lblTrobatModulsVB6.AutoSize = True
        Me.lblTrobatModulsVB6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrobatModulsVB6.ForeColor = System.Drawing.Color.Red
        Me.lblTrobatModulsVB6.Location = New System.Drawing.Point(275, 472)
        Me.lblTrobatModulsVB6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTrobatModulsVB6.Name = "lblTrobatModulsVB6"
        Me.lblTrobatModulsVB6.Size = New System.Drawing.Size(0, 16)
        Me.lblTrobatModulsVB6.TabIndex = 33
        '
        'lstItemsFoundVB6_2
        '
        Me.lstItemsFoundVB6_2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstItemsFoundVB6_2.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5})
        Me.lstItemsFoundVB6_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstItemsFoundVB6_2.HideSelection = False
        Me.lstItemsFoundVB6_2.Location = New System.Drawing.Point(8, 342)
        Me.lstItemsFoundVB6_2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.lstItemsFoundVB6_2.Name = "lstItemsFoundVB6_2"
        Me.lstItemsFoundVB6_2.Size = New System.Drawing.Size(300, 126)
        Me.lstItemsFoundVB6_2.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstItemsFoundVB6_2.TabIndex = 32
        Me.lstItemsFoundVB6_2.UseCompatibleStateImageBehavior = False
        Me.lstItemsFoundVB6_2.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Width = 379
        '
        'btnProjectesVB6
        '
        Me.btnProjectesVB6.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnProjectesVB6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProjectesVB6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProjectesVB6.Location = New System.Drawing.Point(11, 27)
        Me.btnProjectesVB6.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnProjectesVB6.Name = "btnProjectesVB6"
        Me.btnProjectesVB6.Size = New System.Drawing.Size(93, 30)
        Me.btnProjectesVB6.TabIndex = 46
        Me.btnProjectesVB6.Text = "Projectes"
        Me.btnProjectesVB6.UseVisualStyleBackColor = True
        '
        'lstItemsFoundVB6
        '
        Me.lstItemsFoundVB6.CheckBoxes = True
        Me.lstItemsFoundVB6.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lstItemsFoundVB6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstItemsFoundVB6.FullRowSelect = True
        Me.lstItemsFoundVB6.HideSelection = False
        Me.lstItemsFoundVB6.Location = New System.Drawing.Point(8, 107)
        Me.lstItemsFoundVB6.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.lstItemsFoundVB6.Name = "lstItemsFoundVB6"
        Me.lstItemsFoundVB6.Size = New System.Drawing.Size(300, 212)
        Me.lstItemsFoundVB6.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstItemsFoundVB6.TabIndex = 30
        Me.lstItemsFoundVB6.UseCompatibleStateImageBehavior = False
        Me.lstItemsFoundVB6.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Width = 379
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.BackColor = System.Drawing.Color.Azure
        Me.GroupBox3.Controls.Add(Me.chkNomesProjecte)
        Me.GroupBox3.Controls.Add(Me.btnCercaNET)
        Me.GroupBox3.Controls.Add(Me.chkSelectAll)
        Me.GroupBox3.Controls.Add(Me.chkMigrarBackup)
        Me.GroupBox3.Controls.Add(Me.lblTrobats_2)
        Me.GroupBox3.Controls.Add(Me.chkMigrarCodi)
        Me.GroupBox3.Controls.Add(Me.chkMigrarDesigner)
        Me.GroupBox3.Controls.Add(Me.Label_2)
        Me.GroupBox3.Controls.Add(Me.chkInclouFormsNous)
        Me.GroupBox3.Controls.Add(Me.lblTrobatClassesNet)
        Me.GroupBox3.Controls.Add(Me.Label_4)
        Me.GroupBox3.Controls.Add(Me.lstItemsFoundNet_2)
        Me.GroupBox3.Controls.Add(Me.lstItemsFoundNet)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(977, 32)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.GroupBox3.Size = New System.Drawing.Size(551, 496)
        Me.GroupBox3.TabIndex = 31
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Arquitectura .Net"
        '
        'chkNomesProjecte
        '
        Me.chkNomesProjecte.AutoSize = True
        Me.chkNomesProjecte.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNomesProjecte.Location = New System.Drawing.Point(233, 59)
        Me.chkNomesProjecte.Name = "chkNomesProjecte"
        Me.chkNomesProjecte.Size = New System.Drawing.Size(100, 17)
        Me.chkNomesProjecte.TabIndex = 54
        Me.chkNomesProjecte.Text = "Només projecte"
        Me.chkNomesProjecte.UseVisualStyleBackColor = True
        '
        'btnCercaNET
        '
        Me.btnCercaNET.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCercaNET.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCercaNET.Location = New System.Drawing.Point(3, 25)
        Me.btnCercaNET.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnCercaNET.Name = "btnCercaNET"
        Me.btnCercaNET.Size = New System.Drawing.Size(145, 30)
        Me.btnCercaNET.TabIndex = 53
        Me.btnCercaNET.Text = "Cerca TOTS .Net"
        Me.btnCercaNET.UseVisualStyleBackColor = True
        '
        'lblTrobats_2
        '
        Me.lblTrobats_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTrobats_2.AutoSize = True
        Me.lblTrobats_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrobats_2.ForeColor = System.Drawing.Color.Red
        Me.lblTrobats_2.Location = New System.Drawing.Point(514, 322)
        Me.lblTrobats_2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTrobats_2.Name = "lblTrobats_2"
        Me.lblTrobats_2.Size = New System.Drawing.Size(0, 17)
        Me.lblTrobats_2.TabIndex = 44
        '
        'chkMigrarCodi
        '
        Me.chkMigrarCodi.AutoSize = True
        Me.chkMigrarCodi.Checked = True
        Me.chkMigrarCodi.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMigrarCodi.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMigrarCodi.Location = New System.Drawing.Point(9, 79)
        Me.chkMigrarCodi.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkMigrarCodi.Name = "chkMigrarCodi"
        Me.chkMigrarCodi.Size = New System.Drawing.Size(79, 17)
        Me.chkMigrarCodi.TabIndex = 52
        Me.chkMigrarCodi.Text = "Migrar Codi"
        Me.chkMigrarCodi.UseVisualStyleBackColor = True
        '
        'chkMigrarDesigner
        '
        Me.chkMigrarDesigner.AutoSize = True
        Me.chkMigrarDesigner.Enabled = False
        Me.chkMigrarDesigner.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMigrarDesigner.Location = New System.Drawing.Point(92, 79)
        Me.chkMigrarDesigner.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkMigrarDesigner.Name = "chkMigrarDesigner"
        Me.chkMigrarDesigner.Size = New System.Drawing.Size(100, 17)
        Me.chkMigrarDesigner.TabIndex = 51
        Me.chkMigrarDesigner.Text = "Migrar Designer"
        Me.chkMigrarDesigner.UseVisualStyleBackColor = True
        '
        'Label_2
        '
        Me.Label_2.AutoSize = True
        Me.Label_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_2.ForeColor = System.Drawing.Color.Navy
        Me.Label_2.Location = New System.Drawing.Point(2, 322)
        Me.Label_2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label_2.Name = "Label_2"
        Me.Label_2.Size = New System.Drawing.Size(0, 16)
        Me.Label_2.TabIndex = 43
        '
        'lblTrobatClassesNet
        '
        Me.lblTrobatClassesNet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTrobatClassesNet.AutoSize = True
        Me.lblTrobatClassesNet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrobatClassesNet.ForeColor = System.Drawing.Color.Red
        Me.lblTrobatClassesNet.Location = New System.Drawing.Point(524, 472)
        Me.lblTrobatClassesNet.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTrobatClassesNet.Name = "lblTrobatClassesNet"
        Me.lblTrobatClassesNet.Size = New System.Drawing.Size(0, 16)
        Me.lblTrobatClassesNet.TabIndex = 33
        '
        'Label_4
        '
        Me.Label_4.AutoSize = True
        Me.Label_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_4.ForeColor = System.Drawing.Color.Navy
        Me.Label_4.Location = New System.Drawing.Point(13, 554)
        Me.Label_4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label_4.Name = "Label_4"
        Me.Label_4.Size = New System.Drawing.Size(0, 16)
        Me.Label_4.TabIndex = 32
        '
        'lstItemsFoundNet_2
        '
        Me.lstItemsFoundNet_2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstItemsFoundNet_2.CheckBoxes = True
        Me.lstItemsFoundNet_2.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3})
        Me.lstItemsFoundNet_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstItemsFoundNet_2.HideSelection = False
        Me.lstItemsFoundNet_2.Location = New System.Drawing.Point(6, 342)
        Me.lstItemsFoundNet_2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.lstItemsFoundNet_2.Name = "lstItemsFoundNet_2"
        Me.lstItemsFoundNet_2.Size = New System.Drawing.Size(538, 127)
        Me.lstItemsFoundNet_2.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstItemsFoundNet_2.TabIndex = 31
        Me.lstItemsFoundNet_2.UseCompatibleStateImageBehavior = False
        Me.lstItemsFoundNet_2.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Width = 371
        '
        'lstItemsFoundNet
        '
        Me.lstItemsFoundNet.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstItemsFoundNet.CheckBoxes = True
        Me.lstItemsFoundNet.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2})
        Me.lstItemsFoundNet.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstItemsFoundNet.HideSelection = False
        Me.lstItemsFoundNet.Location = New System.Drawing.Point(6, 107)
        Me.lstItemsFoundNet.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.lstItemsFoundNet.Name = "lstItemsFoundNet"
        Me.lstItemsFoundNet.Size = New System.Drawing.Size(538, 212)
        Me.lstItemsFoundNet.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstItemsFoundNet.TabIndex = 29
        Me.lstItemsFoundNet.UseCompatibleStateImageBehavior = False
        Me.lstItemsFoundNet.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Width = 371
        '
        'btnCrearBackups
        '
        Me.btnCrearBackups.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnCrearBackups.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCrearBackups.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.btnCrearBackups.Location = New System.Drawing.Point(12, 617)
        Me.btnCrearBackups.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnCrearBackups.Name = "btnCrearBackups"
        Me.btnCrearBackups.Size = New System.Drawing.Size(252, 50)
        Me.btnCrearBackups.TabIndex = 35
        Me.btnCrearBackups.Text = "Nova migració (1)"
        Me.btnCrearBackups.UseCompatibleTextRendering = True
        Me.btnCrearBackups.UseVisualStyleBackColor = False
        '
        'cboDirectoryVB6_BACKUP
        '
        Me.cboDirectoryVB6_BACKUP.Enabled = False
        Me.cboDirectoryVB6_BACKUP.FormattingEnabled = True
        Me.cboDirectoryVB6_BACKUP.Location = New System.Drawing.Point(11, 507)
        Me.cboDirectoryVB6_BACKUP.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.cboDirectoryVB6_BACKUP.Name = "cboDirectoryVB6_BACKUP"
        Me.cboDirectoryVB6_BACKUP.Size = New System.Drawing.Size(328, 21)
        Me.cboDirectoryVB6_BACKUP.TabIndex = 37
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(14, 3)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 16)
        Me.Label4.TabIndex = 38
        Me.Label4.Text = "Font VB6"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(341, 3)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(73, 16)
        Me.Label7.TabIndex = 40
        Me.Label7.Text = "Font NET"
        '
        'ContextMenuStripProjectesNET
        '
        Me.ContextMenuStripProjectesNET.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiferenciesProjectesNet_MenuItem, Me.DiferenciesCarpetesNet_MenuItem, Me.ObrirProjecteNet_ToolStripMenuItem, Me.EditarProjecteNet_ToolStripMenuItem, Me.ObrirCarpetaProjecteToolStripMenuItem, Me.EliminarProjecteNet_ToolStripMenuItem, Me.CloneProjecteToolStripMenuItem, Me.ReferenciesToolStripMenuItem, Me.WinMergeToolStripMenuItem, Me.UpgradeSupportToolStripMenuItem})
        Me.ContextMenuStripProjectesNET.Name = "ContextMenuStrip1"
        Me.ContextMenuStripProjectesNET.Size = New System.Drawing.Size(215, 224)
        '
        'DiferenciesProjectesNet_MenuItem
        '
        Me.DiferenciesProjectesNet_MenuItem.Name = "DiferenciesProjectesNet_MenuItem"
        Me.DiferenciesProjectesNet_MenuItem.Size = New System.Drawing.Size(214, 22)
        Me.DiferenciesProjectesNet_MenuItem.Text = "Diferencies projectes"
        '
        'DiferenciesCarpetesNet_MenuItem
        '
        Me.DiferenciesCarpetesNet_MenuItem.Name = "DiferenciesCarpetesNet_MenuItem"
        Me.DiferenciesCarpetesNet_MenuItem.Size = New System.Drawing.Size(214, 22)
        Me.DiferenciesCarpetesNet_MenuItem.Text = "Diferencies carpetes"
        '
        'ObrirProjecteNet_ToolStripMenuItem
        '
        Me.ObrirProjecteNet_ToolStripMenuItem.Name = "ObrirProjecteNet_ToolStripMenuItem"
        Me.ObrirProjecteNet_ToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.ObrirProjecteNet_ToolStripMenuItem.Text = "Obrir projecte/solucio NET"
        '
        'EditarProjecteNet_ToolStripMenuItem
        '
        Me.EditarProjecteNet_ToolStripMenuItem.Name = "EditarProjecteNet_ToolStripMenuItem"
        Me.EditarProjecteNet_ToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.EditarProjecteNet_ToolStripMenuItem.Text = "Editar projecte (vbproj)"
        '
        'ObrirCarpetaProjecteToolStripMenuItem
        '
        Me.ObrirCarpetaProjecteToolStripMenuItem.Name = "ObrirCarpetaProjecteToolStripMenuItem"
        Me.ObrirCarpetaProjecteToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.ObrirCarpetaProjecteToolStripMenuItem.Text = "Obrir carpeta"
        '
        'EliminarProjecteNet_ToolStripMenuItem
        '
        Me.EliminarProjecteNet_ToolStripMenuItem.Name = "EliminarProjecteNet_ToolStripMenuItem"
        Me.EliminarProjecteNet_ToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.EliminarProjecteNet_ToolStripMenuItem.Text = "Eliminar projecte NET"
        Me.EliminarProjecteNet_ToolStripMenuItem.ToolTipText = "Elimina tot el projecte NET"
        '
        'CloneProjecteToolStripMenuItem
        '
        Me.CloneProjecteToolStripMenuItem.Name = "CloneProjecteToolStripMenuItem"
        Me.CloneProjecteToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.CloneProjecteToolStripMenuItem.Text = "Clone Projecte Git"
        '
        'ReferenciesToolStripMenuItem
        '
        Me.ReferenciesToolStripMenuItem.Name = "ReferenciesToolStripMenuItem"
        Me.ReferenciesToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.ReferenciesToolStripMenuItem.Text = "Referencies/Components"
        '
        'WinMergeToolStripMenuItem
        '
        Me.WinMergeToolStripMenuItem.Name = "WinMergeToolStripMenuItem"
        Me.WinMergeToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.WinMergeToolStripMenuItem.Text = "WinMerge"
        '
        'UpgradeSupportToolStripMenuItem
        '
        Me.UpgradeSupportToolStripMenuItem.Name = "UpgradeSupportToolStripMenuItem"
        Me.UpgradeSupportToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.UpgradeSupportToolStripMenuItem.Text = "UpgradeSupport"
        '
        'ContextMenuStrip_FormsVB6
        '
        Me.ContextMenuStrip_FormsVB6.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EliminarFormsVB6_ToolStripMenuItem, Me.CopiarBackupAMPNetToolStripMenuItem})
        Me.ContextMenuStrip_FormsVB6.Name = "ContextMenuStrip_FormsVB6"
        Me.ContextMenuStrip_FormsVB6.Size = New System.Drawing.Size(206, 48)
        '
        'EliminarFormsVB6_ToolStripMenuItem
        '
        Me.EliminarFormsVB6_ToolStripMenuItem.Name = "EliminarFormsVB6_ToolStripMenuItem"
        Me.EliminarFormsVB6_ToolStripMenuItem.Size = New System.Drawing.Size(205, 22)
        Me.EliminarFormsVB6_ToolStripMenuItem.Text = "Eliminar"
        '
        'CopiarBackupAMPNetToolStripMenuItem
        '
        Me.CopiarBackupAMPNetToolStripMenuItem.Name = "CopiarBackupAMPNetToolStripMenuItem"
        Me.CopiarBackupAMPNetToolStripMenuItem.Size = New System.Drawing.Size(205, 22)
        Me.CopiarBackupAMPNetToolStripMenuItem.Text = "Copiar Backup a MP_Net"
        '
        'ContextMenuStrip_FormsNET
        '
        Me.ContextMenuStrip_FormsNET.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EliminarFormsNet_ToolStripMenuItem, Me.SeleccionarFormsNet_ToolStripMenuItem})
        Me.ContextMenuStrip_FormsNET.Name = "ContextMenuStrip_FormsVB6"
        Me.ContextMenuStrip_FormsNET.Size = New System.Drawing.Size(135, 48)
        '
        'EliminarFormsNet_ToolStripMenuItem
        '
        Me.EliminarFormsNet_ToolStripMenuItem.Name = "EliminarFormsNet_ToolStripMenuItem"
        Me.EliminarFormsNet_ToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.EliminarFormsNet_ToolStripMenuItem.Text = "Eliminar"
        '
        'SeleccionarFormsNet_ToolStripMenuItem
        '
        Me.SeleccionarFormsNet_ToolStripMenuItem.Name = "SeleccionarFormsNet_ToolStripMenuItem"
        Me.SeleccionarFormsNet_ToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.SeleccionarFormsNet_ToolStripMenuItem.Text = "Seleccionar"
        '
        'ContextMenuStripModuls
        '
        Me.ContextMenuStripModuls.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EliminarModuls_ToolStripMenuItem})
        Me.ContextMenuStripModuls.Name = "ContextMenuStrip_FormsVB6"
        Me.ContextMenuStripModuls.Size = New System.Drawing.Size(118, 26)
        '
        'EliminarModuls_ToolStripMenuItem
        '
        Me.EliminarModuls_ToolStripMenuItem.Name = "EliminarModuls_ToolStripMenuItem"
        Me.EliminarModuls_ToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.EliminarModuls_ToolStripMenuItem.Text = "Eliminar"
        '
        'ContextMenuStripClasses
        '
        Me.ContextMenuStripClasses.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EliminarClasses_ToolStripMenuItem})
        Me.ContextMenuStripClasses.Name = "ContextMenuStrip_FormsVB6"
        Me.ContextMenuStripClasses.Size = New System.Drawing.Size(118, 26)
        '
        'EliminarClasses_ToolStripMenuItem
        '
        Me.EliminarClasses_ToolStripMenuItem.Name = "EliminarClasses_ToolStripMenuItem"
        Me.EliminarClasses_ToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.EliminarClasses_ToolStripMenuItem.Text = "Eliminar"
        '
        'ContextMenuStripProjectesVB6
        '
        Me.ContextMenuStripProjectesVB6.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemObrirProjecte, Me.EditarProjecteVB6_ToolStripMenuItem1, Me.ObtenirReferencieVB6_ToolStripMenuItem})
        Me.ContextMenuStripProjectesVB6.Name = "ContextMenuStrip1"
        Me.ContextMenuStripProjectesVB6.Size = New System.Drawing.Size(209, 70)
        '
        'ToolStripMenuItemObrirProjecte
        '
        Me.ToolStripMenuItemObrirProjecte.Name = "ToolStripMenuItemObrirProjecte"
        Me.ToolStripMenuItemObrirProjecte.Size = New System.Drawing.Size(208, 22)
        Me.ToolStripMenuItemObrirProjecte.Text = "Obrir projecte"
        '
        'EditarProjecteVB6_ToolStripMenuItem1
        '
        Me.EditarProjecteVB6_ToolStripMenuItem1.Name = "EditarProjecteVB6_ToolStripMenuItem1"
        Me.EditarProjecteVB6_ToolStripMenuItem1.Size = New System.Drawing.Size(208, 22)
        Me.EditarProjecteVB6_ToolStripMenuItem1.Text = "Editar projecte"
        '
        'ObtenirReferencieVB6_ToolStripMenuItem
        '
        Me.ObtenirReferencieVB6_ToolStripMenuItem.Name = "ObtenirReferencieVB6_ToolStripMenuItem"
        Me.ObtenirReferencieVB6_ToolStripMenuItem.Size = New System.Drawing.Size(208, 22)
        Me.ObtenirReferencieVB6_ToolStripMenuItem.Text = "Referencies/Components"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'lblFitxersModificats
        '
        Me.lblFitxersModificats.AutoSize = True
        Me.lblFitxersModificats.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFitxersModificats.ForeColor = System.Drawing.Color.Navy
        Me.lblFitxersModificats.Location = New System.Drawing.Point(16, 652)
        Me.lblFitxersModificats.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblFitxersModificats.Name = "lblFitxersModificats"
        Me.lblFitxersModificats.Size = New System.Drawing.Size(0, 16)
        Me.lblFitxersModificats.TabIndex = 47
        '
        'chkInclouProjectesNet_Nous
        '
        Me.chkInclouProjectesNet_Nous.AutoSize = True
        Me.chkInclouProjectesNet_Nous.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInclouProjectesNet_Nous.Location = New System.Drawing.Point(525, 5)
        Me.chkInclouProjectesNet_Nous.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.chkInclouProjectesNet_Nous.Name = "chkInclouProjectesNet_Nous"
        Me.chkInclouProjectesNet_Nous.Size = New System.Drawing.Size(127, 17)
        Me.chkInclouProjectesNet_Nous.TabIndex = 47
        Me.chkInclouProjectesNet_Nous.Text = "Inclou projectes nous"
        Me.chkInclouProjectesNet_Nous.UseVisualStyleBackColor = True
        '
        'lblProjectesVB6
        '
        Me.lblProjectesVB6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProjectesVB6.AutoSize = True
        Me.lblProjectesVB6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProjectesVB6.ForeColor = System.Drawing.Color.Red
        Me.lblProjectesVB6.Location = New System.Drawing.Point(99, 535)
        Me.lblProjectesVB6.Name = "lblProjectesVB6"
        Me.lblProjectesVB6.Size = New System.Drawing.Size(0, 16)
        Me.lblProjectesVB6.TabIndex = 48
        '
        'lblProjectesNET
        '
        Me.lblProjectesNET.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProjectesNET.AutoSize = True
        Me.lblProjectesNET.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lblProjectesNET.ForeColor = System.Drawing.Color.Red
        Me.lblProjectesNET.Location = New System.Drawing.Point(429, 535)
        Me.lblProjectesNET.Name = "lblProjectesNET"
        Me.lblProjectesNET.Size = New System.Drawing.Size(0, 16)
        Me.lblProjectesNET.TabIndex = 49
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Red
        Me.Label6.Location = New System.Drawing.Point(14, 534)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 16)
        Me.Label6.TabIndex = 50
        Me.Label6.Text = "#Carpetes:"
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label9.ForeColor = System.Drawing.Color.Red
        Me.Label9.Location = New System.Drawing.Point(347, 534)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(83, 16)
        Me.Label9.TabIndex = 51
        Me.Label9.Text = "#Carpetes:"
        '
        'btnDiferenciesProjectes
        '
        Me.btnDiferenciesProjectes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDiferenciesProjectes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btnDiferenciesProjectes.Location = New System.Drawing.Point(344, 553)
        Me.btnDiferenciesProjectes.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnDiferenciesProjectes.Name = "btnDiferenciesProjectes"
        Me.btnDiferenciesProjectes.Size = New System.Drawing.Size(303, 29)
        Me.btnDiferenciesProjectes.TabIndex = 52
        Me.btnDiferenciesProjectes.Text = "Diferencies entre projectes (vbp vs vbproj)"
        Me.btnDiferenciesProjectes.UseVisualStyleBackColor = True
        '
        'btnDiferenciesCarpetes
        '
        Me.btnDiferenciesCarpetes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDiferenciesCarpetes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btnDiferenciesCarpetes.Location = New System.Drawing.Point(344, 586)
        Me.btnDiferenciesCarpetes.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnDiferenciesCarpetes.Name = "btnDiferenciesCarpetes"
        Me.btnDiferenciesCarpetes.Size = New System.Drawing.Size(303, 26)
        Me.btnDiferenciesCarpetes.TabIndex = 53
        Me.btnDiferenciesCarpetes.Text = "Diferencies entre carpetes"
        Me.btnDiferenciesCarpetes.UseVisualStyleBackColor = True
        '
        'chkMigrarNomesModificats
        '
        Me.chkMigrarNomesModificats.AutoSize = True
        Me.chkMigrarNomesModificats.Location = New System.Drawing.Point(14, 674)
        Me.chkMigrarNomesModificats.Name = "chkMigrarNomesModificats"
        Me.chkMigrarNomesModificats.Size = New System.Drawing.Size(147, 17)
        Me.chkMigrarNomesModificats.TabIndex = 55
        Me.chkMigrarNomesModificats.Text = "Migrar NOMES modificats"
        Me.chkMigrarNomesModificats.UseVisualStyleBackColor = True
        '
        'btnPreWizard
        '
        Me.btnPreWizard.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnPreWizard.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreWizard.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreWizard.Location = New System.Drawing.Point(345, 617)
        Me.btnPreWizard.Name = "btnPreWizard"
        Me.btnPreWizard.Size = New System.Drawing.Size(302, 50)
        Me.btnPreWizard.TabIndex = 56
        Me.btnPreWizard.Text = "Pre-Wizard2008 (3)"
        Me.btnPreWizard.UseVisualStyleBackColor = False
        '
        'btnFoto
        '
        Me.btnFoto.BackColor = System.Drawing.SystemColors.ControlDark
        Me.btnFoto.Enabled = False
        Me.btnFoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFoto.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte), True)
        Me.btnFoto.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnFoto.Location = New System.Drawing.Point(269, 617)
        Me.btnFoto.Name = "btnFoto"
        Me.btnFoto.Size = New System.Drawing.Size(70, 50)
        Me.btnFoto.TabIndex = 57
        Me.btnFoto.Text = "Foto (2)"
        Me.btnFoto.UseVisualStyleBackColor = False
        '
        'btnWizard2008
        '
        Me.btnWizard2008.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnWizard2008.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnWizard2008.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWizard2008.ForeColor = System.Drawing.Color.Black
        Me.btnWizard2008.Location = New System.Drawing.Point(650, 617)
        Me.btnWizard2008.Name = "btnWizard2008"
        Me.btnWizard2008.Size = New System.Drawing.Size(126, 50)
        Me.btnWizard2008.TabIndex = 58
        Me.btnWizard2008.Text = "Wizard2008   (4)"
        Me.btnWizard2008.UseVisualStyleBackColor = False
        '
        'btnTestos
        '
        Me.btnTestos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTestos.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnTestos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTestos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTestos.Location = New System.Drawing.Point(1210, 617)
        Me.btnTestos.Name = "btnTestos"
        Me.btnTestos.Size = New System.Drawing.Size(75, 50)
        Me.btnTestos.TabIndex = 65
        Me.btnTestos.Text = "Testos (8)"
        Me.btnTestos.UseVisualStyleBackColor = False
        '
        'btnCopiarBackupNet_to_Net
        '
        Me.btnCopiarBackupNet_to_Net.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCopiarBackupNet_to_Net.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCopiarBackupNet_to_Net.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCopiarBackupNet_to_Net.Location = New System.Drawing.Point(1288, 617)
        Me.btnCopiarBackupNet_to_Net.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnCopiarBackupNet_to_Net.Name = "btnCopiarBackupNet_to_Net"
        Me.btnCopiarBackupNet_to_Net.Size = New System.Drawing.Size(128, 50)
        Me.btnCopiarBackupNet_to_Net.TabIndex = 64
        Me.btnCopiarBackupNet_to_Net.Text = "Copia Backup Net a M:\MP_Net"
        Me.btnCopiarBackupNet_to_Net.UseVisualStyleBackColor = True
        '
        'btnPostMigracio
        '
        Me.btnPostMigracio.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnPostMigracio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPostMigracio.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPostMigracio.Location = New System.Drawing.Point(779, 617)
        Me.btnPostMigracio.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnPostMigracio.Name = "btnPostMigracio"
        Me.btnPostMigracio.Size = New System.Drawing.Size(121, 50)
        Me.btnPostMigracio.TabIndex = 63
        Me.btnPostMigracio.Text = "PostMigració (5)"
        Me.btnPostMigracio.UseVisualStyleBackColor = False
        '
        'btnValidacions
        '
        Me.btnValidacions.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnValidacions.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnValidacions.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValidacions.Location = New System.Drawing.Point(903, 617)
        Me.btnValidacions.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnValidacions.Name = "btnValidacions"
        Me.btnValidacions.Size = New System.Drawing.Size(113, 50)
        Me.btnValidacions.TabIndex = 61
        Me.btnValidacions.Text = "Validacions (6)"
        Me.btnValidacions.UseVisualStyleBackColor = False
        '
        'btnCleanMigrar
        '
        Me.btnCleanMigrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCleanMigrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCleanMigrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCleanMigrar.Location = New System.Drawing.Point(1418, 617)
        Me.btnCleanMigrar.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnCleanMigrar.Name = "btnCleanMigrar"
        Me.btnCleanMigrar.Size = New System.Drawing.Size(109, 50)
        Me.btnCleanMigrar.TabIndex = 62
        Me.btnCleanMigrar.Text = "Clean Migrar"
        Me.btnCleanMigrar.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage10)
        Me.TabControl1.Controls.Add(Me.TabPage11)
        Me.TabControl1.Controls.Add(Me.TabPage12)
        Me.TabControl1.Controls.Add(Me.TabPage13)
        Me.TabControl1.Controls.Add(Me.TabPage14)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 700)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1518, 205)
        Me.TabControl1.TabIndex = 69
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.treeListViewUpgrades)
        Me.TabPage2.Controls.Add(Me.GroupBox5)
        Me.TabPage2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Upgrades"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'treeListViewUpgrades
        '
        Me.treeListViewUpgrades.AllowColumnReorder = True
        Me.treeListViewUpgrades.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.treeListViewUpgrades.CheckBoxes = System.Windows.Forms.CheckBoxesTypes.Recursive
        Me.treeListViewUpgrades.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader13, Me.ColumnHeader14, Me.ColumnHeader15, Me.ColumnHeader16})
        TreeListViewItemCollectionComparer1.Column = 0
        TreeListViewItemCollectionComparer1.SortOrder = System.Windows.Forms.SortOrder.Ascending
        Me.treeListViewUpgrades.Comparer = TreeListViewItemCollectionComparer1
        Me.treeListViewUpgrades.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeListViewUpgrades.HideSelection = False
        Me.treeListViewUpgrades.LabelEdit = True
        Me.treeListViewUpgrades.Location = New System.Drawing.Point(6, 8)
        Me.treeListViewUpgrades.Name = "treeListViewUpgrades"
        Me.treeListViewUpgrades.Size = New System.Drawing.Size(1391, 132)
        Me.treeListViewUpgrades.TabIndex = 66
        Me.treeListViewUpgrades.UseCompatibleStateImageBehavior = False
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Codi"
        Me.ColumnHeader11.Width = 300
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Descripció"
        Me.ColumnHeader12.Width = 500
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Solució"
        Me.ColumnHeader13.Width = 100
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Observació"
        Me.ColumnHeader14.Width = 550
        '
        'ColumnHeader15
        '
        Me.ColumnHeader15.Text = "Tipus Upgrade"
        Me.ColumnHeader15.Width = 160
        '
        'ColumnHeader16
        '
        Me.ColumnHeader16.Text = "Descripció upgrade"
        Me.ColumnHeader16.Width = 250
        '
        'GroupBox5
        '
        Me.GroupBox5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox5.Controls.Add(Me.chkTots)
        Me.GroupBox5.Controls.Add(Me.chkCritic)
        Me.GroupBox5.Controls.Add(Me.chkWarning)
        Me.GroupBox5.Controls.Add(Me.chkAltres)
        Me.GroupBox5.Controls.Add(Me.chkResolt)
        Me.GroupBox5.Location = New System.Drawing.Point(1398, 15)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(109, 125)
        Me.GroupBox5.TabIndex = 78
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "UpgradeS"
        '
        'chkTots
        '
        Me.chkTots.AutoSize = True
        Me.chkTots.Checked = True
        Me.chkTots.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTots.Location = New System.Drawing.Point(246, 19)
        Me.chkTots.Name = "chkTots"
        Me.chkTots.Size = New System.Drawing.Size(59, 21)
        Me.chkTots.TabIndex = 74
        Me.chkTots.Text = "Tots"
        Me.chkTots.UseVisualStyleBackColor = True
        '
        'chkCritic
        '
        Me.chkCritic.AutoSize = True
        Me.chkCritic.Checked = True
        Me.chkCritic.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCritic.Location = New System.Drawing.Point(5, 19)
        Me.chkCritic.Name = "chkCritic"
        Me.chkCritic.Size = New System.Drawing.Size(64, 21)
        Me.chkCritic.TabIndex = 70
        Me.chkCritic.Text = "Critic"
        Me.chkCritic.UseVisualStyleBackColor = True
        '
        'chkWarning
        '
        Me.chkWarning.AutoSize = True
        Me.chkWarning.Checked = True
        Me.chkWarning.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkWarning.Location = New System.Drawing.Point(6, 40)
        Me.chkWarning.Name = "chkWarning"
        Me.chkWarning.Size = New System.Drawing.Size(87, 21)
        Me.chkWarning.TabIndex = 71
        Me.chkWarning.Text = "Warning"
        Me.chkWarning.UseVisualStyleBackColor = True
        '
        'chkAltres
        '
        Me.chkAltres.AutoSize = True
        Me.chkAltres.Checked = True
        Me.chkAltres.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAltres.Location = New System.Drawing.Point(6, 63)
        Me.chkAltres.Name = "chkAltres"
        Me.chkAltres.Size = New System.Drawing.Size(69, 21)
        Me.chkAltres.TabIndex = 72
        Me.chkAltres.Text = "Altres"
        Me.chkAltres.UseVisualStyleBackColor = True
        '
        'chkResolt
        '
        Me.chkResolt.AutoSize = True
        Me.chkResolt.Checked = True
        Me.chkResolt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkResolt.Location = New System.Drawing.Point(7, 86)
        Me.chkResolt.Name = "chkResolt"
        Me.chkResolt.Size = New System.Drawing.Size(73, 21)
        Me.chkResolt.TabIndex = 73
        Me.chkResolt.Text = "Resolt"
        Me.chkResolt.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lvWarningsWorkAround)
        Me.TabPage3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage3.Location = New System.Drawing.Point(4, 25)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "WorkAround_NomMetodeEvent"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lvWarningsWorkAround
        '
        Me.lvWarningsWorkAround.AllowColumnReorder = True
        Me.lvWarningsWorkAround.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.lvWarningsWorkAround.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvWarningsWorkAround.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvWarningsWorkAround.FullRowSelect = True
        Me.lvWarningsWorkAround.GridLines = True
        ListViewGroup1.Header = "ListViewGroup"
        ListViewGroup1.Name = "ListViewGroup1"
        ListViewGroup2.Header = "ListViewGroup"
        ListViewGroup2.Name = "ListViewGroup2"
        Me.lvWarningsWorkAround.Groups.AddRange(New System.Windows.Forms.ListViewGroup() {ListViewGroup1, ListViewGroup2})
        Me.lvWarningsWorkAround.HideSelection = False
        ListViewItem1.StateImageIndex = 0
        ListViewItem2.StateImageIndex = 0
        ListViewItem3.StateImageIndex = 0
        Me.lvWarningsWorkAround.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2, ListViewItem3})
        Me.lvWarningsWorkAround.Location = New System.Drawing.Point(0, 0)
        Me.lvWarningsWorkAround.Name = "lvWarningsWorkAround"
        Me.lvWarningsWorkAround.Size = New System.Drawing.Size(1510, 176)
        Me.lvWarningsWorkAround.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvWarningsWorkAround.TabIndex = 59
        Me.lvWarningsWorkAround.UseCompatibleStateImageBehavior = False
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.lvTwipsPixels)
        Me.TabPage5.Controls.Add(Me.GroupBox4)
        Me.TabPage5.Location = New System.Drawing.Point(4, 25)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Twips-Pixels"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'lvTwipsPixels
        '
        Me.lvTwipsPixels.AllowColumnReorder = True
        Me.lvTwipsPixels.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvTwipsPixels.CheckBoxes = True
        Me.lvTwipsPixels.FullRowSelect = True
        Me.lvTwipsPixels.GridLines = True
        Me.lvTwipsPixels.HideSelection = False
        Me.lvTwipsPixels.Location = New System.Drawing.Point(0, 0)
        Me.lvTwipsPixels.Name = "lvTwipsPixels"
        Me.lvTwipsPixels.Size = New System.Drawing.Size(1362, 161)
        Me.lvTwipsPixels.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvTwipsPixels.TabIndex = 0
        Me.lvTwipsPixels.UseCompatibleStateImageBehavior = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(Me.optTwipsPixelsNomesNumeros)
        Me.GroupBox4.Controls.Add(Me.optTwipsPixelsIncorrectes)
        Me.GroupBox4.Controls.Add(Me.optTwipsPixelsCorrectes)
        Me.GroupBox4.Controls.Add(Me.optTwipsPixelsTots)
        Me.GroupBox4.Location = New System.Drawing.Point(1368, 6)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(136, 120)
        Me.GroupBox4.TabIndex = 75
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Twips/Pixels"
        '
        'optTwipsPixelsNomesNumeros
        '
        Me.optTwipsPixelsNomesNumeros.AutoSize = True
        Me.optTwipsPixelsNomesNumeros.Location = New System.Drawing.Point(10, 94)
        Me.optTwipsPixelsNomesNumeros.Name = "optTwipsPixelsNomesNumeros"
        Me.optTwipsPixelsNomesNumeros.Size = New System.Drawing.Size(129, 21)
        Me.optTwipsPixelsNomesNumeros.TabIndex = 3
        Me.optTwipsPixelsNomesNumeros.TabStop = True
        Me.optTwipsPixelsNomesNumeros.Text = "Només numeros"
        Me.optTwipsPixelsNomesNumeros.UseVisualStyleBackColor = True
        '
        'optTwipsPixelsIncorrectes
        '
        Me.optTwipsPixelsIncorrectes.AutoSize = True
        Me.optTwipsPixelsIncorrectes.Location = New System.Drawing.Point(10, 67)
        Me.optTwipsPixelsIncorrectes.Name = "optTwipsPixelsIncorrectes"
        Me.optTwipsPixelsIncorrectes.Size = New System.Drawing.Size(96, 21)
        Me.optTwipsPixelsIncorrectes.TabIndex = 2
        Me.optTwipsPixelsIncorrectes.Text = "Incorrectes"
        Me.optTwipsPixelsIncorrectes.UseVisualStyleBackColor = True
        '
        'optTwipsPixelsCorrectes
        '
        Me.optTwipsPixelsCorrectes.AutoSize = True
        Me.optTwipsPixelsCorrectes.Location = New System.Drawing.Point(10, 42)
        Me.optTwipsPixelsCorrectes.Name = "optTwipsPixelsCorrectes"
        Me.optTwipsPixelsCorrectes.Size = New System.Drawing.Size(87, 21)
        Me.optTwipsPixelsCorrectes.TabIndex = 1
        Me.optTwipsPixelsCorrectes.Text = "Correctes"
        Me.optTwipsPixelsCorrectes.UseVisualStyleBackColor = True
        '
        'optTwipsPixelsTots
        '
        Me.optTwipsPixelsTots.AutoSize = True
        Me.optTwipsPixelsTots.Checked = True
        Me.optTwipsPixelsTots.Location = New System.Drawing.Point(10, 21)
        Me.optTwipsPixelsTots.Name = "optTwipsPixelsTots"
        Me.optTwipsPixelsTots.Size = New System.Drawing.Size(54, 21)
        Me.optTwipsPixelsTots.TabIndex = 0
        Me.optTwipsPixelsTots.TabStop = True
        Me.optTwipsPixelsTots.Text = "Tots"
        Me.optTwipsPixelsTots.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.lviAsObject)
        Me.TabPage6.Location = New System.Drawing.Point(4, 25)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "As Object"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'lviAsObject
        '
        Me.lviAsObject.AllowColumnReorder = True
        Me.lviAsObject.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lviAsObject.CheckBoxes = True
        Me.lviAsObject.FullRowSelect = True
        Me.lviAsObject.GridLines = True
        Me.lviAsObject.HideSelection = False
        Me.lviAsObject.Location = New System.Drawing.Point(0, 0)
        Me.lviAsObject.Name = "lviAsObject"
        Me.lviAsObject.Size = New System.Drawing.Size(1510, 143)
        Me.lviAsObject.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lviAsObject.TabIndex = 0
        Me.lviAsObject.UseCompatibleStateImageBehavior = False
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.lviFontsTrueType)
        Me.TabPage8.Location = New System.Drawing.Point(4, 25)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage8.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage8.TabIndex = 7
        Me.TabPage8.Text = "FontsTrueType"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'lviFontsTrueType
        '
        Me.lviFontsTrueType.AllowColumnReorder = True
        Me.lviFontsTrueType.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lviFontsTrueType.CheckBoxes = True
        Me.lviFontsTrueType.FullRowSelect = True
        Me.lviFontsTrueType.GridLines = True
        Me.lviFontsTrueType.HideSelection = False
        Me.lviFontsTrueType.Location = New System.Drawing.Point(0, 0)
        Me.lviFontsTrueType.Name = "lviFontsTrueType"
        Me.lviFontsTrueType.Size = New System.Drawing.Size(1510, 162)
        Me.lviFontsTrueType.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lviFontsTrueType.TabIndex = 0
        Me.lviFontsTrueType.UseCompatibleStateImageBehavior = False
        '
        'TabPage9
        '
        Me.TabPage9.Controls.Add(Me.lviTreeViewAxGroupBox)
        Me.TabPage9.Location = New System.Drawing.Point(4, 25)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage9.TabIndex = 8
        Me.TabPage9.Text = "TreeView/AxGroupBox"
        Me.TabPage9.UseVisualStyleBackColor = True
        '
        'lviTreeViewAxGroupBox
        '
        Me.lviTreeViewAxGroupBox.AllowColumnReorder = True
        Me.lviTreeViewAxGroupBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lviTreeViewAxGroupBox.CheckBoxes = True
        Me.lviTreeViewAxGroupBox.FullRowSelect = True
        Me.lviTreeViewAxGroupBox.GridLines = True
        Me.lviTreeViewAxGroupBox.HideSelection = False
        Me.lviTreeViewAxGroupBox.Location = New System.Drawing.Point(0, 2)
        Me.lviTreeViewAxGroupBox.Name = "lviTreeViewAxGroupBox"
        Me.lviTreeViewAxGroupBox.Size = New System.Drawing.Size(1510, 143)
        Me.lviTreeViewAxGroupBox.TabIndex = 1
        Me.lviTreeViewAxGroupBox.UseCompatibleStateImageBehavior = False
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkFitxerMigrat)
        Me.TabPage1.Controls.Add(Me.chkNoTotalmentRedissenyat)
        Me.TabPage1.Controls.Add(Me.chkFitxerNoExisteix)
        Me.TabPage1.Controls.Add(Me.chkFitxerValidar)
        Me.TabPage1.Controls.Add(Me.chkFitxerRedissenyar)
        Me.TabPage1.Controls.Add(Me.chkFitxerMigrar)
        Me.TabPage1.Controls.Add(Me.chkFitxerNou)
        Me.TabPage1.Controls.Add(Me.lviFormsRedisseny)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage1.TabIndex = 9
        Me.TabPage1.Text = "Forms Redisseny"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'chkFitxerMigrat
        '
        Me.chkFitxerMigrat.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkFitxerMigrat.AutoSize = True
        Me.chkFitxerMigrat.BackColor = System.Drawing.Color.LightGreen
        Me.chkFitxerMigrat.Location = New System.Drawing.Point(1369, 168)
        Me.chkFitxerMigrat.Name = "chkFitxerMigrat"
        Me.chkFitxerMigrat.Size = New System.Drawing.Size(100, 21)
        Me.chkFitxerMigrat.TabIndex = 85
        Me.chkFitxerMigrat.Text = "fitxer migrat"
        Me.chkFitxerMigrat.UseVisualStyleBackColor = False
        '
        'chkNoTotalmentRedissenyat
        '
        Me.chkNoTotalmentRedissenyat.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkNoTotalmentRedissenyat.AutoSize = True
        Me.chkNoTotalmentRedissenyat.BackColor = System.Drawing.Color.Yellow
        Me.chkNoTotalmentRedissenyat.Checked = True
        Me.chkNoTotalmentRedissenyat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNoTotalmentRedissenyat.Enabled = False
        Me.chkNoTotalmentRedissenyat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNoTotalmentRedissenyat.Location = New System.Drawing.Point(1369, 142)
        Me.chkNoTotalmentRedissenyat.Name = "chkNoTotalmentRedissenyat"
        Me.chkNoTotalmentRedissenyat.Size = New System.Drawing.Size(139, 20)
        Me.chkNoTotalmentRedissenyat.TabIndex = 89
        Me.chkNoTotalmentRedissenyat.Text = "fitxer a redissenyar"
        Me.chkNoTotalmentRedissenyat.UseVisualStyleBackColor = False
        '
        'chkFitxerNoExisteix
        '
        Me.chkFitxerNoExisteix.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkFitxerNoExisteix.AutoSize = True
        Me.chkFitxerNoExisteix.BackColor = System.Drawing.Color.LightGray
        Me.chkFitxerNoExisteix.Checked = True
        Me.chkFitxerNoExisteix.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFitxerNoExisteix.Location = New System.Drawing.Point(1369, 7)
        Me.chkFitxerNoExisteix.Name = "chkFitxerNoExisteix"
        Me.chkFitxerNoExisteix.Size = New System.Drawing.Size(91, 21)
        Me.chkFitxerNoExisteix.TabIndex = 88
        Me.chkFitxerNoExisteix.Text = "fitxer antic"
        Me.chkFitxerNoExisteix.UseVisualStyleBackColor = False
        '
        'chkFitxerValidar
        '
        Me.chkFitxerValidar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkFitxerValidar.AutoSize = True
        Me.chkFitxerValidar.BackColor = System.Drawing.Color.LightBlue
        Me.chkFitxerValidar.Checked = True
        Me.chkFitxerValidar.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFitxerValidar.Location = New System.Drawing.Point(1369, 90)
        Me.chkFitxerValidar.Name = "chkFitxerValidar"
        Me.chkFitxerValidar.Size = New System.Drawing.Size(115, 21)
        Me.chkFitxerValidar.TabIndex = 87
        Me.chkFitxerValidar.Text = "fitxer a validar"
        Me.chkFitxerValidar.UseVisualStyleBackColor = False
        '
        'chkFitxerRedissenyar
        '
        Me.chkFitxerRedissenyar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkFitxerRedissenyar.AutoSize = True
        Me.chkFitxerRedissenyar.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.chkFitxerRedissenyar.Checked = True
        Me.chkFitxerRedissenyar.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFitxerRedissenyar.Enabled = False
        Me.chkFitxerRedissenyar.Location = New System.Drawing.Point(1369, 116)
        Me.chkFitxerRedissenyar.Name = "chkFitxerRedissenyar"
        Me.chkFitxerRedissenyar.Size = New System.Drawing.Size(147, 21)
        Me.chkFitxerRedissenyar.TabIndex = 86
        Me.chkFitxerRedissenyar.Text = "fitxer a redissenyar"
        Me.chkFitxerRedissenyar.UseVisualStyleBackColor = False
        '
        'chkFitxerMigrar
        '
        Me.chkFitxerMigrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkFitxerMigrar.AutoSize = True
        Me.chkFitxerMigrar.BackColor = System.Drawing.Color.PaleVioletRed
        Me.chkFitxerMigrar.Checked = True
        Me.chkFitxerMigrar.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFitxerMigrar.Location = New System.Drawing.Point(1369, 64)
        Me.chkFitxerMigrar.Name = "chkFitxerMigrar"
        Me.chkFitxerMigrar.Size = New System.Drawing.Size(113, 21)
        Me.chkFitxerMigrar.TabIndex = 85
        Me.chkFitxerMigrar.Text = "fitxer a migrar"
        Me.chkFitxerMigrar.UseVisualStyleBackColor = False
        '
        'chkFitxerNou
        '
        Me.chkFitxerNou.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkFitxerNou.AutoSize = True
        Me.chkFitxerNou.BackColor = System.Drawing.Color.Red
        Me.chkFitxerNou.Checked = True
        Me.chkFitxerNou.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFitxerNou.Location = New System.Drawing.Point(1369, 37)
        Me.chkFitxerNou.Name = "chkFitxerNou"
        Me.chkFitxerNou.Size = New System.Drawing.Size(85, 21)
        Me.chkFitxerNou.TabIndex = 12
        Me.chkFitxerNou.Text = "fitxer nou"
        Me.chkFitxerNou.UseVisualStyleBackColor = False
        '
        'lviFormsRedisseny
        '
        Me.lviFormsRedisseny.AllowColumnReorder = True
        Me.lviFormsRedisseny.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lviFormsRedisseny.CheckBoxes = True
        Me.lviFormsRedisseny.FullRowSelect = True
        Me.lviFormsRedisseny.GridLines = True
        Me.lviFormsRedisseny.HideSelection = False
        Me.lviFormsRedisseny.Location = New System.Drawing.Point(0, 0)
        Me.lviFormsRedisseny.Name = "lviFormsRedisseny"
        Me.lviFormsRedisseny.Size = New System.Drawing.Size(1363, 155)
        Me.lviFormsRedisseny.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lviFormsRedisseny.TabIndex = 0
        Me.lviFormsRedisseny.UseCompatibleStateImageBehavior = False
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.txtCercaWorkAround)
        Me.TabPage7.Controls.Add(Me.chkNA)
        Me.TabPage7.Controls.Add(Me.chkNoRevisats)
        Me.TabPage7.Controls.Add(Me.chkRevisats)
        Me.TabPage7.Controls.Add(Me.GroupBox6)
        Me.TabPage7.Controls.Add(Me.chkBUG)
        Me.TabPage7.Controls.Add(Me.chkHOWTO)
        Me.TabPage7.Controls.Add(Me.chkINFO)
        Me.TabPage7.Controls.Add(Me.chkPRB)
        Me.TabPage7.Controls.Add(Me.lviArquitectura)
        Me.TabPage7.Location = New System.Drawing.Point(4, 25)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "Arquitectura"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'txtCercaWorkAround
        '
        Me.txtCercaWorkAround.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCercaWorkAround.Location = New System.Drawing.Point(1405, 70)
        Me.txtCercaWorkAround.Name = "txtCercaWorkAround"
        Me.txtCercaWorkAround.Size = New System.Drawing.Size(100, 23)
        Me.txtCercaWorkAround.TabIndex = 9
        '
        'chkNA
        '
        Me.chkNA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkNA.AutoSize = True
        Me.chkNA.Location = New System.Drawing.Point(1418, 46)
        Me.chkNA.Name = "chkNA"
        Me.chkNA.Size = New System.Drawing.Size(51, 21)
        Me.chkNA.TabIndex = 8
        Me.chkNA.Text = "n.a."
        Me.chkNA.UseVisualStyleBackColor = True
        '
        'chkNoRevisats
        '
        Me.chkNoRevisats.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkNoRevisats.AutoSize = True
        Me.chkNoRevisats.Checked = True
        Me.chkNoRevisats.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNoRevisats.Location = New System.Drawing.Point(1418, 25)
        Me.chkNoRevisats.Name = "chkNoRevisats"
        Me.chkNoRevisats.Size = New System.Drawing.Size(98, 21)
        Me.chkNoRevisats.TabIndex = 7
        Me.chkNoRevisats.Text = "No revisats"
        Me.chkNoRevisats.UseVisualStyleBackColor = True
        '
        'chkRevisats
        '
        Me.chkRevisats.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkRevisats.AutoSize = True
        Me.chkRevisats.Checked = True
        Me.chkRevisats.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRevisats.Location = New System.Drawing.Point(1418, 4)
        Me.chkRevisats.Name = "chkRevisats"
        Me.chkRevisats.Size = New System.Drawing.Size(81, 21)
        Me.chkRevisats.TabIndex = 6
        Me.chkRevisats.Text = "Revisats"
        Me.chkRevisats.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox6.Controls.Add(Me.chkADO)
        Me.GroupBox6.Controls.Add(Me.chkDatabase)
        Me.GroupBox6.Controls.Add(Me.chkClasses)
        Me.GroupBox6.Controls.Add(Me.chkFormsControls)
        Me.GroupBox6.Controls.Add(Me.chkLanguage)
        Me.GroupBox6.Controls.Add(Me.chkGeneral)
        Me.GroupBox6.Location = New System.Drawing.Point(1331, 89)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(128, 140)
        Me.GroupBox6.TabIndex = 5
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Categoria"
        '
        'chkADO
        '
        Me.chkADO.AutoSize = True
        Me.chkADO.Location = New System.Drawing.Point(7, 115)
        Me.chkADO.Name = "chkADO"
        Me.chkADO.Size = New System.Drawing.Size(101, 21)
        Me.chkADO.TabIndex = 5
        Me.chkADO.Text = "ADOLibrary"
        Me.chkADO.UseVisualStyleBackColor = True
        '
        'chkDatabase
        '
        Me.chkDatabase.AutoSize = True
        Me.chkDatabase.Location = New System.Drawing.Point(7, 95)
        Me.chkDatabase.Name = "chkDatabase"
        Me.chkDatabase.Size = New System.Drawing.Size(88, 21)
        Me.chkDatabase.TabIndex = 4
        Me.chkDatabase.Text = "Database"
        Me.chkDatabase.UseVisualStyleBackColor = True
        '
        'chkClasses
        '
        Me.chkClasses.AutoSize = True
        Me.chkClasses.Checked = True
        Me.chkClasses.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkClasses.Location = New System.Drawing.Point(7, 75)
        Me.chkClasses.Name = "chkClasses"
        Me.chkClasses.Size = New System.Drawing.Size(118, 21)
        Me.chkClasses.TabIndex = 3
        Me.chkClasses.Text = "Classes i COM"
        Me.chkClasses.UseVisualStyleBackColor = True
        '
        'chkFormsControls
        '
        Me.chkFormsControls.AutoSize = True
        Me.chkFormsControls.Checked = True
        Me.chkFormsControls.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFormsControls.Location = New System.Drawing.Point(7, 57)
        Me.chkFormsControls.Name = "chkFormsControls"
        Me.chkFormsControls.Size = New System.Drawing.Size(122, 21)
        Me.chkFormsControls.TabIndex = 2
        Me.chkFormsControls.Text = "Forms/Controls"
        Me.chkFormsControls.UseVisualStyleBackColor = True
        '
        'chkLanguage
        '
        Me.chkLanguage.AutoSize = True
        Me.chkLanguage.Checked = True
        Me.chkLanguage.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLanguage.Location = New System.Drawing.Point(7, 37)
        Me.chkLanguage.Name = "chkLanguage"
        Me.chkLanguage.Size = New System.Drawing.Size(91, 21)
        Me.chkLanguage.TabIndex = 1
        Me.chkLanguage.Text = "Language"
        Me.chkLanguage.UseVisualStyleBackColor = True
        '
        'chkGeneral
        '
        Me.chkGeneral.AutoSize = True
        Me.chkGeneral.Checked = True
        Me.chkGeneral.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkGeneral.Location = New System.Drawing.Point(7, 19)
        Me.chkGeneral.Name = "chkGeneral"
        Me.chkGeneral.Size = New System.Drawing.Size(78, 21)
        Me.chkGeneral.TabIndex = 0
        Me.chkGeneral.Text = "General"
        Me.chkGeneral.UseVisualStyleBackColor = True
        '
        'chkBUG
        '
        Me.chkBUG.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkBUG.AutoSize = True
        Me.chkBUG.Checked = True
        Me.chkBUG.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBUG.Location = New System.Drawing.Point(1337, 63)
        Me.chkBUG.Name = "chkBUG"
        Me.chkBUG.Size = New System.Drawing.Size(57, 21)
        Me.chkBUG.TabIndex = 4
        Me.chkBUG.Text = "BUG"
        Me.chkBUG.UseVisualStyleBackColor = True
        '
        'chkHOWTO
        '
        Me.chkHOWTO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkHOWTO.AutoSize = True
        Me.chkHOWTO.Checked = True
        Me.chkHOWTO.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkHOWTO.Location = New System.Drawing.Point(1337, 43)
        Me.chkHOWTO.Name = "chkHOWTO"
        Me.chkHOWTO.Size = New System.Drawing.Size(81, 21)
        Me.chkHOWTO.TabIndex = 3
        Me.chkHOWTO.Text = "HOWTO"
        Me.chkHOWTO.UseVisualStyleBackColor = True
        '
        'chkINFO
        '
        Me.chkINFO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkINFO.AutoSize = True
        Me.chkINFO.Checked = True
        Me.chkINFO.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkINFO.Location = New System.Drawing.Point(1337, 24)
        Me.chkINFO.Name = "chkINFO"
        Me.chkINFO.Size = New System.Drawing.Size(59, 21)
        Me.chkINFO.TabIndex = 2
        Me.chkINFO.Text = "INFO"
        Me.chkINFO.UseVisualStyleBackColor = True
        '
        'chkPRB
        '
        Me.chkPRB.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkPRB.AutoSize = True
        Me.chkPRB.Checked = True
        Me.chkPRB.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPRB.Location = New System.Drawing.Point(1337, 5)
        Me.chkPRB.Name = "chkPRB"
        Me.chkPRB.Size = New System.Drawing.Size(55, 21)
        Me.chkPRB.TabIndex = 1
        Me.chkPRB.Text = "PRB"
        Me.chkPRB.UseVisualStyleBackColor = True
        '
        'lviArquitectura
        '
        Me.lviArquitectura.AllowColumnReorder = True
        Me.lviArquitectura.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lviArquitectura.CheckBoxes = True
        Me.lviArquitectura.FullRowSelect = True
        Me.lviArquitectura.GridLines = True
        Me.lviArquitectura.HideSelection = False
        Me.lviArquitectura.Location = New System.Drawing.Point(0, 0)
        Me.lviArquitectura.Name = "lviArquitectura"
        Me.lviArquitectura.Size = New System.Drawing.Size(1326, 143)
        Me.lviArquitectura.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lviArquitectura.TabIndex = 0
        Me.lviArquitectura.UseCompatibleStateImageBehavior = False
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.lviBugs)
        Me.TabPage4.Location = New System.Drawing.Point(4, 25)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage4.TabIndex = 10
        Me.TabPage4.Text = "Bugs"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'lviBugs
        '
        Me.lviBugs.AllowColumnReorder = True
        Me.lviBugs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lviBugs.FullRowSelect = True
        Me.lviBugs.GridLines = True
        Me.lviBugs.HideSelection = False
        Me.lviBugs.Location = New System.Drawing.Point(0, 0)
        Me.lviBugs.Name = "lviBugs"
        Me.lviBugs.ShowItemToolTips = True
        Me.lviBugs.Size = New System.Drawing.Size(1510, 161)
        Me.lviBugs.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lviBugs.TabIndex = 0
        Me.lviBugs.UseCompatibleStateImageBehavior = False
        Me.lviBugs.View = System.Windows.Forms.View.List
        '
        'TabPage10
        '
        Me.TabPage10.Controls.Add(Me.lviColeccio1_0)
        Me.TabPage10.Location = New System.Drawing.Point(4, 25)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage10.TabIndex = 11
        Me.TabPage10.Text = "Coleccio_1_0"
        Me.TabPage10.UseVisualStyleBackColor = True
        '
        'lviColeccio1_0
        '
        Me.lviColeccio1_0.AllowColumnReorder = True
        Me.lviColeccio1_0.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lviColeccio1_0.FullRowSelect = True
        Me.lviColeccio1_0.GridLines = True
        Me.lviColeccio1_0.HideSelection = False
        Me.lviColeccio1_0.Location = New System.Drawing.Point(0, 2)
        Me.lviColeccio1_0.Name = "lviColeccio1_0"
        Me.lviColeccio1_0.ShowItemToolTips = True
        Me.lviColeccio1_0.Size = New System.Drawing.Size(1510, 161)
        Me.lviColeccio1_0.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lviColeccio1_0.TabIndex = 1
        Me.lviColeccio1_0.UseCompatibleStateImageBehavior = False
        Me.lviColeccio1_0.View = System.Windows.Forms.View.List
        '
        'TabPage11
        '
        Me.TabPage11.Controls.Add(Me.Label2)
        Me.TabPage11.Controls.Add(Me.Label1)
        Me.TabPage11.Controls.Add(Me.lviFormularis_Sense_Caption)
        Me.TabPage11.Location = New System.Drawing.Point(4, 25)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage11.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage11.TabIndex = 12
        Me.TabPage11.Text = "Formularis sense Caption"
        Me.TabPage11.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Magenta
        Me.Label2.Location = New System.Drawing.Point(354, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(268, 20)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "--> Aplicable als fitxers Designer"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(7, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(346, 20)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "MOLT IMPORTANT LA SEVA APLICACIÓ "
        '
        'lviFormularis_Sense_Caption
        '
        Me.lviFormularis_Sense_Caption.AllowColumnReorder = True
        Me.lviFormularis_Sense_Caption.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lviFormularis_Sense_Caption.FullRowSelect = True
        Me.lviFormularis_Sense_Caption.GridLines = True
        Me.lviFormularis_Sense_Caption.HideSelection = False
        Me.lviFormularis_Sense_Caption.Location = New System.Drawing.Point(0, 29)
        Me.lviFormularis_Sense_Caption.Name = "lviFormularis_Sense_Caption"
        Me.lviFormularis_Sense_Caption.ShowItemToolTips = True
        Me.lviFormularis_Sense_Caption.Size = New System.Drawing.Size(1510, 134)
        Me.lviFormularis_Sense_Caption.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lviFormularis_Sense_Caption.TabIndex = 2
        Me.lviFormularis_Sense_Caption.UseCompatibleStateImageBehavior = False
        Me.lviFormularis_Sense_Caption.View = System.Windows.Forms.View.List
        '
        'TabPage12
        '
        Me.TabPage12.Controls.Add(Me.lviNomClasseUserControl_igual_nomInstancia)
        Me.TabPage12.Location = New System.Drawing.Point(4, 25)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage12.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage12.TabIndex = 13
        Me.TabPage12.Text = "NomClasseUserControl_NomInstancia"
        Me.TabPage12.UseVisualStyleBackColor = True
        '
        'lviNomClasseUserControl_igual_nomInstancia
        '
        Me.lviNomClasseUserControl_igual_nomInstancia.AllowColumnReorder = True
        Me.lviNomClasseUserControl_igual_nomInstancia.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lviNomClasseUserControl_igual_nomInstancia.FullRowSelect = True
        Me.lviNomClasseUserControl_igual_nomInstancia.GridLines = True
        Me.lviNomClasseUserControl_igual_nomInstancia.HideSelection = False
        Me.lviNomClasseUserControl_igual_nomInstancia.Location = New System.Drawing.Point(0, 2)
        Me.lviNomClasseUserControl_igual_nomInstancia.Name = "lviNomClasseUserControl_igual_nomInstancia"
        Me.lviNomClasseUserControl_igual_nomInstancia.ShowItemToolTips = True
        Me.lviNomClasseUserControl_igual_nomInstancia.Size = New System.Drawing.Size(1510, 161)
        Me.lviNomClasseUserControl_igual_nomInstancia.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lviNomClasseUserControl_igual_nomInstancia.TabIndex = 3
        Me.lviNomClasseUserControl_igual_nomInstancia.UseCompatibleStateImageBehavior = False
        Me.lviNomClasseUserControl_igual_nomInstancia.View = System.Windows.Forms.View.List
        '
        'TabPage13
        '
        Me.TabPage13.Controls.Add(Me.lvFormLoad)
        Me.TabPage13.Location = New System.Drawing.Point(4, 25)
        Me.TabPage13.Name = "TabPage13"
        Me.TabPage13.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage13.TabIndex = 14
        Me.TabPage13.Text = "FormLoad"
        Me.TabPage13.UseVisualStyleBackColor = True
        '
        'lvFormLoad
        '
        Me.lvFormLoad.AllowColumnReorder = True
        Me.lvFormLoad.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvFormLoad.FullRowSelect = True
        Me.lvFormLoad.GridLines = True
        Me.lvFormLoad.HideSelection = False
        Me.lvFormLoad.Location = New System.Drawing.Point(0, 2)
        Me.lvFormLoad.Name = "lvFormLoad"
        Me.lvFormLoad.ShowItemToolTips = True
        Me.lvFormLoad.Size = New System.Drawing.Size(1510, 161)
        Me.lvFormLoad.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvFormLoad.TabIndex = 0
        Me.lvFormLoad.UseCompatibleStateImageBehavior = False
        Me.lvFormLoad.View = System.Windows.Forms.View.List
        '
        'TabPage14
        '
        Me.TabPage14.Controls.Add(Me.lvErrorsMigracio)
        Me.TabPage14.Location = New System.Drawing.Point(4, 25)
        Me.TabPage14.Name = "TabPage14"
        Me.TabPage14.Size = New System.Drawing.Size(1510, 176)
        Me.TabPage14.TabIndex = 15
        Me.TabPage14.Text = "Errors migracio"
        Me.TabPage14.UseVisualStyleBackColor = True
        '
        'lvErrorsMigracio
        '
        Me.lvErrorsMigracio.AllowColumnReorder = True
        Me.lvErrorsMigracio.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvErrorsMigracio.CheckBoxes = True
        Me.lvErrorsMigracio.FullRowSelect = True
        Me.lvErrorsMigracio.GridLines = True
        Me.lvErrorsMigracio.HideSelection = False
        Me.lvErrorsMigracio.Location = New System.Drawing.Point(3, 8)
        Me.lvErrorsMigracio.Name = "lvErrorsMigracio"
        Me.lvErrorsMigracio.Size = New System.Drawing.Size(1433, 161)
        Me.lvErrorsMigracio.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvErrorsMigracio.TabIndex = 1
        Me.lvErrorsMigracio.UseCompatibleStateImageBehavior = False
        '
        'chkMigrarTwipsPixels
        '
        Me.chkMigrarTwipsPixels.AutoSize = True
        Me.chkMigrarTwipsPixels.Location = New System.Drawing.Point(1021, 623)
        Me.chkMigrarTwipsPixels.Name = "chkMigrarTwipsPixels"
        Me.chkMigrarTwipsPixels.Size = New System.Drawing.Size(113, 17)
        Me.chkMigrarTwipsPixels.TabIndex = 76
        Me.chkMigrarTwipsPixels.Text = "Migrar TwipsPixels"
        Me.chkMigrarTwipsPixels.UseVisualStyleBackColor = True
        '
        'chkTwipsPixelsDuplicades
        '
        Me.chkTwipsPixelsDuplicades.AutoSize = True
        Me.chkTwipsPixelsDuplicades.Location = New System.Drawing.Point(1022, 647)
        Me.chkTwipsPixelsDuplicades.Name = "chkTwipsPixelsDuplicades"
        Me.chkTwipsPixelsDuplicades.Size = New System.Drawing.Size(211, 17)
        Me.chkTwipsPixelsDuplicades.TabIndex = 77
        Me.chkTwipsPixelsDuplicades.Text = "Visualitzar linies twips/pixels duplicades"
        Me.chkTwipsPixelsDuplicades.UseVisualStyleBackColor = True
        '
        'lblProces
        '
        Me.lblProces.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblProces.AutoSize = True
        Me.lblProces.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProces.ForeColor = System.Drawing.Color.Red
        Me.lblProces.Location = New System.Drawing.Point(13, 908)
        Me.lblProces.Name = "lblProces"
        Me.lblProces.Size = New System.Drawing.Size(0, 16)
        Me.lblProces.TabIndex = 79
        '
        'lblItemProcessat
        '
        Me.lblItemProcessat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblItemProcessat.AutoSize = True
        Me.lblItemProcessat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemProcessat.ForeColor = System.Drawing.Color.Red
        Me.lblItemProcessat.Location = New System.Drawing.Point(1453, 910)
        Me.lblItemProcessat.Name = "lblItemProcessat"
        Me.lblItemProcessat.Size = New System.Drawing.Size(0, 15)
        Me.lblItemProcessat.TabIndex = 47
        '
        'ToolTip1
        '
        '
        'btnRefrescarVista
        '
        Me.btnRefrescarVista.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefrescarVista.Image = CType(resources.GetObject("btnRefrescarVista.Image"), System.Drawing.Image)
        Me.btnRefrescarVista.Location = New System.Drawing.Point(1487, 674)
        Me.btnRefrescarVista.Name = "btnRefrescarVista"
        Me.btnRefrescarVista.Size = New System.Drawing.Size(40, 43)
        Me.btnRefrescarVista.TabIndex = 81
        Me.btnRefrescarVista.UseVisualStyleBackColor = True
        '
        'btnSincrBACKUP_NET
        '
        Me.btnSincrBACKUP_NET.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSincrBACKUP_NET.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSincrBACKUP_NET.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSincrBACKUP_NET.Location = New System.Drawing.Point(1256, 674)
        Me.btnSincrBACKUP_NET.Name = "btnSincrBACKUP_NET"
        Me.btnSincrBACKUP_NET.Size = New System.Drawing.Size(149, 31)
        Me.btnSincrBACKUP_NET.TabIndex = 83
        Me.btnSincrBACKUP_NET.Text = "Sincr. BACKUP vs NET"
        Me.btnSincrBACKUP_NET.UseVisualStyleBackColor = False
        '
        'chkMigracioIncremental
        '
        Me.chkMigracioIncremental.AutoSize = True
        Me.chkMigracioIncremental.Checked = True
        Me.chkMigracioIncremental.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMigracioIncremental.Location = New System.Drawing.Point(781, 674)
        Me.chkMigracioIncremental.Name = "chkMigracioIncremental"
        Me.chkMigracioIncremental.Size = New System.Drawing.Size(123, 17)
        Me.chkMigracioIncremental.TabIndex = 84
        Me.chkMigracioIncremental.Text = "Migració incremental"
        Me.chkMigracioIncremental.UseVisualStyleBackColor = True
        '
        'Substituir
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1542, 929)
        Me.Controls.Add(Me.chkMigracioIncremental)
        Me.Controls.Add(Me.btnSincrBACKUP_NET)
        Me.Controls.Add(Me.btnRefrescarVista)
        Me.Controls.Add(Me.lblItemProcessat)
        Me.Controls.Add(Me.lblProces)
        Me.Controls.Add(Me.chkTwipsPixelsDuplicades)
        Me.Controls.Add(Me.chkMigrarTwipsPixels)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnTestos)
        Me.Controls.Add(Me.btnCopiarBackupNet_to_Net)
        Me.Controls.Add(Me.btnPostMigracio)
        Me.Controls.Add(Me.btnValidacions)
        Me.Controls.Add(Me.btnCleanMigrar)
        Me.Controls.Add(Me.btnWizard2008)
        Me.Controls.Add(Me.btnFoto)
        Me.Controls.Add(Me.btnPreWizard)
        Me.Controls.Add(Me.chkMigrarNomesModificats)
        Me.Controls.Add(Me.btnDiferenciesCarpetes)
        Me.Controls.Add(Me.btnDiferenciesProjectes)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblProjectesNET)
        Me.Controls.Add(Me.lblProjectesVB6)
        Me.Controls.Add(Me.chkInclouProjectesNet_Nous)
        Me.Controls.Add(Me.lblFitxersModificats)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboDirectoryVB6_BACKUP)
        Me.Controls.Add(Me.btnCrearBackups)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.TreeViewNET)
        Me.Controls.Add(Me.cboDirectoryNet)
        Me.Controls.Add(Me.lblEtiqueta)
        Me.Controls.Add(Me.lblProcessant)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TreeViewVB6)
        Me.Controls.Add(Me.cboDirectoryVB6)
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Name = "Substituir"
        Me.Text = "Form1"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ContextMenuStripProjectesNET.ResumeLayout(False)
        Me.ContextMenuStrip_FormsVB6.ResumeLayout(False)
        Me.ContextMenuStrip_FormsNET.ResumeLayout(False)
        Me.ContextMenuStripModuls.ResumeLayout(False)
        Me.ContextMenuStripClasses.ResumeLayout(False)
        Me.ContextMenuStripProjectesVB6.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage10.ResumeLayout(False)
        Me.TabPage11.ResumeLayout(False)
        Me.TabPage11.PerformLayout()
        Me.TabPage12.ResumeLayout(False)
        Me.TabPage13.ResumeLayout(False)
        Me.TabPage14.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboDirectoryVB6 As Windows.Forms.ComboBox
    Friend WithEvents btnCercaVB6ModificatsDesDe As Windows.Forms.Button
    Friend WithEvents TreeViewVB6 As Windows.Forms.TreeView
    Friend WithEvents GroupBox1 As Windows.Forms.GroupBox
    Friend WithEvents dtDesde As Windows.Forms.DateTimePicker
    Friend WithEvents lblProcessant As Windows.Forms.Label
    Friend WithEvents lblEtiqueta As Windows.Forms.Label
    Friend WithEvents chkInclouConfiguracio As Windows.Forms.CheckBox
    Friend WithEvents chkInclouProyecto1 As Windows.Forms.CheckBox
    Friend WithEvents TreeViewNET As Windows.Forms.TreeView
    Friend WithEvents cboDirectoryNet As Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As Windows.Forms.GroupBox
    Friend WithEvents Label_3 As Windows.Forms.Label
    Friend WithEvents lblTrobatModulsVB6 As Windows.Forms.Label
    Friend WithEvents lstItemsFoundVB6_2 As Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As Windows.Forms.ColumnHeader
    Friend WithEvents lstItemsFoundVB6 As Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As Windows.Forms.ColumnHeader
    Friend WithEvents BackgroundWorker1 As ComponentModel.BackgroundWorker
    Friend WithEvents GroupBox3 As Windows.Forms.GroupBox
    Friend WithEvents Label_4 As Windows.Forms.Label
    Friend WithEvents lstItemsFoundNet_2 As Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As Windows.Forms.ColumnHeader
    Friend WithEvents lstItemsFoundNet As Windows.Forms.ListView
    Friend WithEvents ColumnHeader2 As Windows.Forms.ColumnHeader
    Friend WithEvents chkSelectAll As Windows.Forms.CheckBox
    Friend WithEvents lblTrobatClassesNet As Windows.Forms.Label
    Friend WithEvents btnCrearBackups As Windows.Forms.Button
    Friend WithEvents cboDirectoryVB6_BACKUP As Windows.Forms.ComboBox
    Friend WithEvents Label4 As Windows.Forms.Label
    Friend WithEvents Label7 As Windows.Forms.Label
    Friend WithEvents lblTrobats_1 As Windows.Forms.Label
    Friend WithEvents Label_1 As Windows.Forms.Label
    Friend WithEvents chkMigrarBackup As Windows.Forms.CheckBox
    Friend WithEvents lblTrobats_2 As Windows.Forms.Label
    Friend WithEvents Label_2 As Windows.Forms.Label
    Friend WithEvents chkMigrarDesigner As Windows.Forms.CheckBox
    Friend WithEvents chkMigrarCodi As Windows.Forms.CheckBox
    Friend WithEvents ContextMenuStripProjectesNET As Windows.Forms.ContextMenuStrip
    Friend WithEvents DiferenciesProjectesNet_MenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStrip_FormsVB6 As Windows.Forms.ContextMenuStrip
    Friend WithEvents ContextMenuStrip_FormsNET As Windows.Forms.ContextMenuStrip
    Friend WithEvents ContextMenuStripModuls As Windows.Forms.ContextMenuStrip
    Friend WithEvents ContextMenuStripClasses As Windows.Forms.ContextMenuStrip
    Friend WithEvents EliminarFormsVB6_ToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarClasses_ToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarFormsNet_ToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarModuls_ToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents ObrirProjecteNet_ToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripProjectesVB6 As Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStripMenuItemObrirProjecte As Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditarProjecteNet_ToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditarProjecteVB6_ToolStripMenuItem1 As Windows.Forms.ToolStripMenuItem
    Friend WithEvents ObrirCarpetaProjecteToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As Windows.Forms.OpenFileDialog
    Friend WithEvents EliminarProjecteNet_ToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnProjectesVB6 As Windows.Forms.Button
    Friend WithEvents chkInclouFormsNous As Windows.Forms.CheckBox
    Friend WithEvents ReferenciesToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents ObtenirReferencieVB6_ToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkDiferenciesDreta As Windows.Forms.CheckBox
    Friend WithEvents chkDiferenciesEsquerra As Windows.Forms.CheckBox
    Friend WithEvents btnCercaNET As Windows.Forms.Button
    Friend WithEvents lblFitxersModificats As Windows.Forms.Label
    Friend WithEvents chkInclouProjectesNet_Nous As Windows.Forms.CheckBox
    Friend WithEvents lblProjectesVB6 As Windows.Forms.Label
    Friend WithEvents lblProjectesNET As Windows.Forms.Label
    Friend WithEvents Label6 As Windows.Forms.Label
    Friend WithEvents Label9 As Windows.Forms.Label
    Friend WithEvents CloneProjecteToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnDiferenciesProjectes As Windows.Forms.Button
    Friend WithEvents DiferenciesCarpetesNet_MenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnDiferenciesCarpetes As Windows.Forms.Button
    Friend WithEvents WinMergeToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkMigrarNomesModificats As Windows.Forms.CheckBox
    Friend WithEvents CopiarBackupAMPNetToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents ColumnHeader4 As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As Windows.Forms.ColumnHeader
    Friend WithEvents btnPreWizard As Windows.Forms.Button
    Friend WithEvents btnFoto As Windows.Forms.Button
    Friend WithEvents btnWizard2008 As Windows.Forms.Button
    Friend WithEvents btnTestos As Windows.Forms.Button
    Friend WithEvents btnCopiarBackupNet_to_Net As Windows.Forms.Button
    Friend WithEvents btnPostMigracio As Windows.Forms.Button
    Friend WithEvents btnValidacions As Windows.Forms.Button
    Friend WithEvents btnCleanMigrar As Windows.Forms.Button
    Friend WithEvents ColumnHeader7 As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As Windows.Forms.ColumnHeader
    Friend WithEvents treeListViewUpgrades As Windows.Forms.TreeListView
    Private WithEvents ColumnHeader11 As Windows.Forms.ColumnHeader
    Private WithEvents ColumnHeader12 As Windows.Forms.ColumnHeader
    Private WithEvents ColumnHeader13 As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader14 As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader15 As Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader16 As Windows.Forms.ColumnHeader
    Friend WithEvents TabControl1 As Windows.Forms.TabControl
    Friend WithEvents TabPage2 As Windows.Forms.TabPage
    Friend WithEvents TabPage3 As Windows.Forms.TabPage
    Friend WithEvents lvWarningsWorkAround As Windows.Forms.ListView
    Friend WithEvents chkCritic As Windows.Forms.CheckBox
    Friend WithEvents chkWarning As Windows.Forms.CheckBox
    Friend WithEvents chkAltres As Windows.Forms.CheckBox
    Friend WithEvents chkResolt As Windows.Forms.CheckBox
    Friend WithEvents chkTots As Windows.Forms.CheckBox
    Friend WithEvents TabPage5 As Windows.Forms.TabPage
    Friend WithEvents lvTwipsPixels As Windows.Forms.ListView
    Friend WithEvents GroupBox4 As Windows.Forms.GroupBox
    Friend WithEvents optTwipsPixelsIncorrectes As Windows.Forms.RadioButton
    Friend WithEvents optTwipsPixelsCorrectes As Windows.Forms.RadioButton
    Friend WithEvents optTwipsPixelsTots As Windows.Forms.RadioButton
    Friend WithEvents optTwipsPixelsNomesNumeros As Windows.Forms.RadioButton
    Friend WithEvents chkMigrarTwipsPixels As Windows.Forms.CheckBox
    Friend WithEvents chkTwipsPixelsDuplicades As Windows.Forms.CheckBox
    Friend WithEvents TabPage6 As Windows.Forms.TabPage
    Friend WithEvents lviAsObject As Windows.Forms.ListView
    Friend WithEvents GroupBox5 As Windows.Forms.GroupBox
    Friend WithEvents UpgradeSupportToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabPage7 As Windows.Forms.TabPage
    Friend WithEvents lviArquitectura As Windows.Forms.ListView
    Friend WithEvents chkPRB As Windows.Forms.CheckBox
    Friend WithEvents chkINFO As Windows.Forms.CheckBox
    Friend WithEvents chkHOWTO As Windows.Forms.CheckBox
    Friend WithEvents chkBUG As Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As Windows.Forms.GroupBox
    Friend WithEvents chkLanguage As Windows.Forms.CheckBox
    Friend WithEvents chkGeneral As Windows.Forms.CheckBox
    Friend WithEvents chkClasses As Windows.Forms.CheckBox
    Friend WithEvents chkFormsControls As Windows.Forms.CheckBox
    Friend WithEvents chkRevisats As Windows.Forms.CheckBox
    Friend WithEvents chkNoRevisats As Windows.Forms.CheckBox
    Friend WithEvents chkDatabase As Windows.Forms.CheckBox
    Friend WithEvents chkADO As Windows.Forms.CheckBox
    Friend WithEvents chkNA As Windows.Forms.CheckBox
    Friend WithEvents SeleccionarFormsNet_ToolStripMenuItem As Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtCercaWorkAround As Windows.Forms.TextBox
    Friend WithEvents TabPage8 As Windows.Forms.TabPage
    Friend WithEvents lviFontsTrueType As Windows.Forms.ListView
    Friend WithEvents lblProces As Windows.Forms.Label
    Friend WithEvents TabPage9 As Windows.Forms.TabPage
    Friend WithEvents lviTreeViewAxGroupBox As Windows.Forms.ListView
    Friend WithEvents lblItemProcessat As Windows.Forms.Label
    Friend WithEvents TabPage1 As Windows.Forms.TabPage
    Friend WithEvents lviFormsRedisseny As Windows.Forms.ListView
    Friend WithEvents ToolTip1 As Windows.Forms.ToolTip
    Friend WithEvents TabPage4 As Windows.Forms.TabPage
    Friend WithEvents lviBugs As Windows.Forms.ListView
    Friend WithEvents btnRefrescarVista As Windows.Forms.Button
    Friend WithEvents btnSincrBACKUP_NET As Windows.Forms.Button
    Friend WithEvents chkMigracioIncremental As Windows.Forms.CheckBox
    Friend WithEvents chkFitxerMigrat As Windows.Forms.CheckBox
    Friend WithEvents chkFitxerRedissenyar As Windows.Forms.CheckBox
    Friend WithEvents chkFitxerMigrar As Windows.Forms.CheckBox
    Friend WithEvents chkFitxerNou As Windows.Forms.CheckBox
    Friend WithEvents chkFitxerValidar As Windows.Forms.CheckBox
    Friend WithEvents chkFitxerNoExisteix As Windows.Forms.CheckBox
    Friend WithEvents TabPage10 As Windows.Forms.TabPage
    Friend WithEvents lviColeccio1_0 As Windows.Forms.ListView
    Friend WithEvents TabPage11 As Windows.Forms.TabPage
    Friend WithEvents lviFormularis_Sense_Caption As Windows.Forms.ListView
    Friend WithEvents TabPage12 As Windows.Forms.TabPage
    Friend WithEvents lviNomClasseUserControl_igual_nomInstancia As Windows.Forms.ListView
    Friend WithEvents chkNomesProjecte As Windows.Forms.CheckBox
    Friend WithEvents chkNoTotalmentRedissenyat As Windows.Forms.CheckBox
    Friend WithEvents TabPage13 As Windows.Forms.TabPage
    Friend WithEvents lvFormLoad As Windows.Forms.ListView
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents TabPage14 As Windows.Forms.TabPage
    Friend WithEvents lvErrorsMigracio As Windows.Forms.ListView
End Class
