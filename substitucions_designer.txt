﻿
-- Herencia FormParent
Inherits System.Windows.Forms.Form;N;Inherits FormParent
Inherits MDIForm;N;Inherits FormParent

Formulari As System.Windows.Forms.Form;N;Formulari As FormParent
FRM As System.Windows.Forms.Form;N;FRM As FormParent

(?i)Me.MDIParent = \w+\.MDI;R;Me.MdiParent = MDI
(?i)\w+\.MDI.Show;R;MDI.Show()


-- OBSERVACIONS : 
--      Podem escollir si volem migrar el AxGroupBoxArray a GmsGroupBox o bé GmsAxGroupBox
--      Per defecte (en aquest fitxer) migrem a GmsGroupBox.Per codi si l'hem de migrar a GmsAxGroupBox ho canviem
-- Instanciacio de GmsGroupBox
(?i)Me\.(\w+) = New AxXtremeSuiteControls.AxGroupBox;R;Me.$1 = New DataControl.GmsGroupBox


-- Instanciacio de GmsTemps
(?i)Me\.(\w+) = New AxgmsTime.AxgmsTemps;R;Me.$1 = New gmsTime.GmsTemps()

-- Instanciacio de AxSSDBCombo
(?i)Me\.(\w+) = New AxSSDataWidgets_B.AxSSDBCombo;R;Me.$1 = New DataControl.GmsCombo()

-- Instanciacio de controls de DataControl
(?i)Me\.(\w+) = New AxDataControl\.Ax(\w+);R;Me.$1 = New DataControl.$2

-- Instanciacio de FlexCell.Grid
(?i)Me\.(\w+) = New AxFlexCell.AxGrid;R;Me.$1 = New FlexCell.Grid()

-- Instanciacio de AxCalendarControl (es com l'Outlook!)
Me\.(\w+) = New CalendarControl;R;Me.$1 = New AxXtremeCalendarControl.AxCalendarControl()

-- Instanciacio de AxDatePicker
Me\.(\w+) = New DatePicker;R;Me.$1 = New AxXtremeCalendarControl.AxDatePicker


(?i)AxDataControl.AxGmsData;R;DataControl.GmsData
(?i)AxDataControl.AxGmsImports;R;DataControl.GmsImports
(?i)AxgmsTime.AxgmsTemps;R;GmsTemps
(?i)AxSSDataWidgets_B.AxSSDBCombo;R;DataControl.GmsCombo
(?i)AxFlexCell.AxGrid;R;FlexCell.Grid
(?i)AxDataControl.AxGmsSetmana;R;DataControl.GmsSetmana
(?i)AxDataControl.AxGmsBoto;R;

----------------------------------------------------------
-- Migracio ControlArrays (VB6 --> NET)
----------------------------------------------------------
      	  
-- Instanciacio de controlArray Codejoc
(?i)(\w+ = New )((?!Gms)[Ax]*[Gms]*)(\w+Array\(New List\(Of )([AxXtremeSuiteControls]+([\.]*)[Ax]+([Gms]*\w+\)\({[\w+=\(, ]+}\)\)));R;$1Gms$2$3$4

-- Instanciacio de controlArray DataControl
(?i)(\w+ = New )[Ax]*([Gms]*)(\w+Array\(New List\(Of )[AxDataControl]+([\.]*)[Ax]+(([Gms]*\w+\)\({[\w+=\(, ]+}\)\)));R;$1$2$3DataControl$4$5

-- Instanciacio de controlArray AxFlexCell.AxGrid
(?i)(\w+ = New )[Ax]*((\w+Array\(New List\(Of ))AxFlexCell.AxGrid;R;$1$2FlexCell.Grid

-- Instanciacio de controlArray GmsCombo
(?i)(\w+ = New )[Ax]*[SSDBCombo]+(\w*Array\(New List\(Of )[AxSSDataWidgets_B\.AxSSDBCombo]+([\.]*)((\w*\)\({[\w+=\(, ]+}\)\)));R;$1GmsCombo$2GmsCombo$3$4

-- Instanciacio de controlArray GmsTime
(?i)(\w+ = New )[Ax]*([Gms]*)(\w+Array\(New List\(Of )AxgmsTime+[\.]*[Ax]+(([Gms]*\w+\)\({[\w+=\(, ]+}\)\)));R;$1$2$3$4


Microsoft\.VisualBasic\.Compatibility\.VB6\.(\w+)Array\(components\);R;Gms$1Array()

-- Instanciacio de controlArray GridArray
(?i)Me.(\w+) = New (AxGridArray\(components\));R;Me.$1 = New DataControl.GridArray()

-- Instanciacio de controlArray GmsTabPageArray
(?i)(Me.\w+ = New )((?!Gms)[Ax]*)Tab\w*\(components\);R;$1GmsTabPageArray()

-- Instanciacio de controlArray GmsComboArray
(?i)(Me.\w+ = New )(AxSSDBComboArray)\(components\);R;$1GmsComboArray()

-- Instanciacio de controlArray GmsDataArray
(?i)(Me.\w+ = New )(AxGmsDataArray)\(components\);R;$1GmsDataArray()

-- Instanciacio de controlArray GmsImportsArray
(?i)(Me.\w+ = New )(AxGmsImportsArray)\(components\);R;$1GmsImportsArray()

-- Instanciacio de controlArray GmsTempsArray
(?i)(Me.\w+ = New )(AxgmsTempsArray)\(components\);R;$1GmsTempsArray()

-- OBSERVACIONS : 
--      Podem escollir si volem migrar el AxGroupBoxArray a GmsGroupBox o bé GmsAxGroupBox
--      Per defecte (en aquest fitxer) migrem a GmsGroupBox.Per codi si l'hem de migrar a GmsAxGroupBox ho canviem
-- Instanciacio de controlArray GmsGroupBoxArray
(?i)(Me.\w+ = New )(AxGroupBoxArray)\(components\);R;$1GmsGroupBoxArray()


-- Instanciacio de la  resta de "Array de controls"
(?i)(Me.\w+ = New )((?!Gms)[Ax]*\w+Array\w*)\(components\);R;$1Gms$2()
(?i)(Me.\w+ = New )([Ax]*\w+Array\w*)\(components\);R;$1$2()

-- Instanciacio de LineShapeArray
(?i)(\w+) = New LineShapeArray;R;$1 = New GmsLineShapeArray

-- Nota : la seguent linia ha d'estar després de totes les migracions relacionades amb
--        controls VB6
Microsoft.VisualBasic.Compatibility.VB6.(\w+);R;Gms$1

-- controlArray (ActiveX) + SetIndex
Me\.\w+\.SetIndex\(;R



------------------------------------------------------
-- Controls DataControl
------------------------------------------------------
(?i)Public WithEvents (\w+) As (Ax\w+)\.AxSSDBCombo;R;Public WithEvents $1 As DataControl.GmsCombo
(?i)Public WithEvents (\w+) As (Ax\w+)\.AxGmsData;R;Public WithEvents $1 As DataControl.GmsData
(?i)Public WithEvents (\w+) As (Ax\w+)\.AxgmsTemps;R;Public WithEvents $1 As gmsTime.GmsTemps
(?i)Public WithEvents (\w+) As (Ax\w+)\.AxGrid;R;Public WithEvents $1 As FlexCell.Grid
(?i)Public WithEvents (\w+) As AxDataControl\.Ax(\w+);R;Public WithEvents $1 As DataControl.$2

-- OBSERVACIONS : 
--      Podem escollir si volem migrar el AxGroupBoxArray a GmsGroupBox o bé GmsAxGroupBox
--      Per defecte (en aquest fitxer) migrem a GmsGroupBox.Per codi si l'hem de migrar a GmsAxGroupBox ho canviem
(?i)(\w+) WithEvents (\w+) As AxXtremeSuiteControls\.AxGroupBox;R;$1 WithEvents $2 As DataControl.GmsGroupBox


(?i)Public WithEvents (\w+) As CalendarControl;R;Public WithEvents $1 As AxXtremeCalendarControl.AxCalendarControl
(?i)Public WithEvents (\w+) As DatePicker;R;Public WithEvents $1 As AxXtremeCalendarControl.AxDatePicker

------------------------------------------------------
-- Declaracio de controlArray de Codejoc i SSDBCombo
-- IMPORTANT : l'ordre ha de ser primer el de SSDBCombo
------------------------------------------------------
(?i)Public WithEvents (\w+) As AxSSDBComboArray;R;Public WithEvents $1 As GmsComboArray
(?i)Public WithEvents (\w+) As AxTab\w+Array;R;Public WithEvents $1 As GmsTabPageArray
(?i)Public WithEvents (\w+) As Ax(Gms\w+)Array;R;Public WithEvents $1 As $2Array
(?i)(Public WithEvents \w+ As) LineShapeArray;R;$1 GmsLineShapeArray
(?i)Public WithEvents (\w+) As AxGridArray;R;Public WithEvents $1 As DataControl.GridArray


-- OBSERVACIONS : 
--      Podem escollir si volem migrar el AxGroupBoxArray a GmsGroupBox o bé GmsAxGroupBox
--      Per defecte (en aquest fitxer) migrem a GmsGroupBox.Per codi si l'hem de migrar a GmsAxGroupBox ho canviem
(?i)Public WithEvents (\w+) As AxGroupBoxArray;R;Public WithEvents $1 As DataControl.GmsGroupBoxArray


-- La resta de casos (ActiveX)
(?i)Public WithEvents (\w+) As (Ax\w+)Array;R;Public WithEvents $1 As Gms$2Array



\bAxScrollBarArray\b;R;GmsAxScrollBarArray
\bAxWebBrowserArray;R;GmsAxWebBrowserArray

-----------------------------------------------------------------------------------------------------------------------------
-- GdPicture   -->   VintaSoft

-- GdViewer : mentre no haguem migrat el GdViewer, hem de comentar totes les linies que facin referencia a objectes GdViewer
-----------------------------------------------------------------------------------------------------------------------------

AxGdpicturePro.AxgdViewer;N;Vintasoft.Imaging.UI.ImageViewer

As gdViewer;R;As Vintasoft.Imaging.UI.ImageViewer
= New gdViewer;R;= New Vintasoft.Imaging.UI.ImageViewer

(\w+)\.ZoomMode = (\d+);R;$1.Zoom = $2

Me\.\w+\.RectBorderStyle = \d+;R;''MIGRAR$0
Me\.\w+\.ScrollSmallChange = \d+;R;''MIGRAR$0
Me\.\w+\.RectBorderColor = \d+;R;''MIGRAR$0
Me\.\w+\.EnableMenu = \d+;R;''MIGRAR$0
-----------------------------------------------------------------------------------------------------------------------------




AxXtremeSuiteControls.AxTabControlPage;N;GmsTabPage
AxXtremeSuiteControls.AxTabControl;N;GmsTabControl

Public ToolTip1 As System.Windows.Forms.ToolTip
Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)

(?i)Public WithEvents (\w+) As (Ax\w+)\.AxWinsock;R
Me\.(\w+) = New AxMSWinsockLib.AxWinsock;R
Me\.Winsock1\.Location = New System\.Drawing\.Point\(\d+, \d+\);R
Me\.Winsock1\.Name = \"Winsock1\";R
Me\.Controls\.Add\(Winsock1\);R

CType\(Me.Winsock1, System.ComponentModel.ISupportInitialize\).BeginInit\(\);R
CType\(Me.Winsock1, System.ComponentModel.ISupportInitialize\).EndInit\(\);R



-- ImageList Name 
-- Per evitar l'excepcio : Indice fuera de los limites de la matriz.
-----------Me\.\w+\.Images\.SetKeyName\(\d+, [\w+\"]+\);R;''ELIMINADA$0       sembla ser que no cal. tot i axi revisar
--(Me.ImageList\d).(Key_)(\d+)(\s=\s)(\"\w*\");R;$1.Images.SetKeyName($3,$5)    PROVOCA ELIMINACIONS EN EL FRMIMATGESXP.DESIGNER.VB MOLT IMPORTANTS!!!!!
Me\.ImageList(\d+)\.Key_(\d+) = \"(\w+)\";R;Me.ImageList$1.Images.SetKeyName($2, "$3")
Me\.ImageList(\d+)\.Images\.Keys\((\d+)\) = \"(\w*)\";R;Me.ImageList$1.Images.SetKeyName($2, "$3")
--(?i)(\w*.ImageList\w+)\.ListImages;R;$1.Images
(?i)(ImageList\d*)\.Key_(\d*);R;$1.Images.Keys($2)


-- Centrar Formularis
System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;N;System.Windows.Forms.FormStartPosition.Manual


-- OBSERVACIO : les Label utilitzen ContentAlignment
-- IMPORTANT : cal seguir un criteri, si en els Designers posem TopLeft en el codi hem de posar TopLeft (també hi ha MiddleCenter i BottomCenter!!!!!)
(?i)(Me.lbl\w+)\.Alignment = 0;R;$1.TextAlign = System.Drawing.ContentAlignment.TopLeft
(?i)(Me.Label\w+)\.Alignment = 0;R;$1.TextAlign = System.Drawing.ContentAlignment.TopLeft
(?i)(Me.lbl\w+)\.Alignment = 2;R;$1.TextAlign = System.Drawing.ContentAlignment.TopCenter
(?i)(Me.Label\w+)\.Alignment = 2;R;$1.TextAlign = System.Drawing.ContentAlignment.TopCenter
(?i)(Me.lbl\w+)\.Alignment = 1;R;$1.TextAlign = System.Drawing.ContentAlignment.TopRight
(?i)(Me.Label\w+)\.Alignment = 1;R;$1.TextAlign = System.Drawing.ContentAlignment.TopRight

-- OBSERVACIO : els TextBox utilitzen HorizontalAlignment
(?i)(Me.txt\w+)\.Alignment = 0;R;$1.TextAlign = HorizontalAlignment.Left
(?i)(Me.txt\w+)\.Alignment = 2;R;$1.TextAlign = HorizontalAlignment.Center
(?i)(Me.txt\w+)\.Alignment = 1;R;$1.TextAlign = HorizontalAlignment.Right
(?i)(Me.Text\w+)\.Alignment = 0;R;$1.TextAlign = HorizontalAlignment.Left
(?i)(Me.Text\w+)\.Alignment = 2;R;$1.TextAlign = HorizontalAlignment.Center
(?i)(Me.Text\w+)\.Alignment = 1;R;$1.TextAlign = HorizontalAlignment.Right


(?i)(Me\.txt\w+)\.MousePointer = System.Windows.Forms.Cursors;R;$1.Cursor = System.Windows.Forms.Cursors
(?i)(Me\.GmsData\w+)\.MousePointer = System.Windows.Forms.Cursors;R;$1.Cursor = System.Windows.Forms.Cursors
(?i)(Me\.GmsImports\w+)\.MousePointer = System.Windows.Forms.Cursors;R;$1.Cursor = System.Windows.Forms.Cursors

(?i)(Me\.Label\w+)\.MousePointer = 0;R;$1.Cursor = System.Windows.Forms.Cursors.Default
(?i)(Me\.lbl\w+)\.MousePointer = 0;R;$1.Cursor = System.Windows.Forms.Cursors.Default
(?i)(Me\.txt\w+)\.MousePointer = 3;R;$1.Cursor = System.Windows.Forms.Cursors.Default
(?i)(Me\.text\w+)\.MousePointer = 3;R;$1.Cursor = System.Windows.Forms.Cursors.Default
(?i)(Me\.pic\w+)\.MousePointer = 0;R;$1.Cursor = System.Windows.Forms.Cursors.Default


-- En Net la propietat Locked no existeix i equival a la propietat ReadOnly
(?i)(Me\.txt\w+)\.Locked;R;$1.ReadOnly
(?i)(Me\.Text\w+)\.Locked;R;$1.ReadOnly

-- La propietat ReadOnly del FlexCell Grid/VB6 no existeix en NET i l'equivalència és la propietat Locked
(?i)(Grid\w*)\.ReadOnly;R;$1.Locked
(?i)([\w\.]*grd\w*)\.ReadOnly;R;$1.Locked
^\s*\.(ReadOnly =);R;.Locked =


--------------------------------------------------------------------------------------------------------------------
-- COLORS
--------------------------------------------------------------------------------------------------------------------
(?i)estil.colorObligatori;R;System.Drawing.ColorTranslator.FromOle($0);C;ColorTranslator.FromOle
(?i)System.Convert.ToUInt32\(System.Drawing.ColorTranslator.ToOle\(System.Drawing.(\w+\.\w+)(\)\));R;$1
(?i)System.Convert.ToUInt32\((\d+)\);R;System.Drawing.ColorTranslator.FromOle($1)
(?i)System.Convert.ToUInt32\((\w+)\);R;System.Drawing.ColorTranslator.FromOle($1)
(?i)System.Convert.ToUInt32\(Val\((\w+)\)\);R;System.Drawing.ColorTranslator.FromOle(Val($1))
\.BackColor = System\.Convert\.ToUInt32\((&H\w+)\);R;.BackColor = System.Drawing.ColorTranslator.FromOle($1)

BackColor = System.Convert.ToUInt32\(Estil.BackColorGridHead\);R;BackColor = System.Drawing.ColorTranslator.FromOle(Estil.BackColorGridHead)
\.BackColor = System\.Convert\.ToUInt32\(Estil\.BackColorGridScrollbar\);R;.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.BackColorGridScrollbar)

(\w*ForeColor) = (Val\(\w+\));R;$1 = System.Drawing.ColorTranslator.FromOle($2)
(\w*BackColor) = (Val\(\w+\));R;$1 = System.Drawing.ColorTranslator.FromOle($2)

HeadBackColor = System\.Drawing\.ColorTranslator\.FromOle\(Estil\.BackColorGridHead\);R;HeadBackColor = Estil.BackColorGridHead

-- Labels
-- Excepcio : labels TransparentS
Me\.(\w+)\.BackColor\s*=\s*100;R;Me.$1.BackColor = System.Drawing.SystemColors.Control

(?i)(Me\.lbl\w+)\.BackColor = "(&H\w+)&";R;$1.BackColor = ColorTranslator.FromOle($2)
(?i)(Me\.lbl\w+)\.ForeColor = "(&H\w+)&";R;$1.ForeColor = ColorTranslator.FromOle($2)
(?i)(Me\.Label\w+)\.BackColor = "(&H\w+)&";R;$1.BackColor = ColorTranslator.FromOle($2)
(?i)(Me\.Label\w+)\.ForeColor = "(&H\w+)&";R;$1.ForeColor = ColorTranslator.FromOle($2)

-- TextBox
(?i)(Me\.txt\w+)\.BackColor = "(&H\w+)&";R;$1.BackColor = ColorTranslator.FromOle($2)
(?i)(Me\.txt\w+)\.ForeColor = "(&H\w+)&";R;$1.ForeColor = ColorTranslator.FromOle($2)
(?i)(Me\.Text\w+)\.BackColor = "(&H\w+)&";R;$1.BackColor = ColorTranslator.FromOle($2)
(?i)(Me\.Text\w+)\.ForeColor = "(&H\w+)&";R;$1.ForeColor = ColorTranslator.FromOle($2)


(\w+)\.BackColor = ([+-]*\d+);R;$1.BackColor = System.Drawing.ColorTranslator.FromOle($2)
(\w+)\.BackColor = System\.Drawing\.ColorTranslator\.FromOle\(([\w\.]*\.BackColor)\);R;$1.BackColor = $2

(\w*\.BackColor = )(&H8\w+);R;$1ColorTranslator.FromOle($2)
(\w*\.ForeColor = )(&H8\w+);R;$1ColorTranslator.FromOle($2)
(\w*\.TitleBackColor = )(&H8\w+);R;$1ColorTranslator.FromOle($2)
(\w*\.BackColorFixed = )(&H8\w+);R;$1ColorTranslator.FromOle($2)


SubItems\.Item\((\w+)\)\.ForeColor = Color\.(\w+);R;SubItems.Item($1).ForeColor = System.Drawing.ColorTranslator.ToOle(Color.$2)


\.BackColor = Estil\.(\w+);R;.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.$1)
\.BackColor = System\.Drawing\.ColorTranslator\.FromOle\(([\w+\.]*Parent.BackColor)\);R;.BackColor = $1

TextBackColor = System.Drawing.ColorTranslator.FromOle\(([\w-\(-\)-,\s]+)\);R;TextBackColor = Estil.$1

(\w*)\.BackColorEven = Estil\.BackColorEven;R;$1.BackColorEven = System.Drawing.ColorTranslator.FromOle(Estil.BackColorEven)
(\w*)\.BackColorOdd = Estil\.BackColorOdd;R;$1.BackColorOdd = System.Drawing.ColorTranslator.FromOle(Estil.BackColorOdd)

(\w+\.Color) = (RGB\(\w+, \w+, \w+\));R;$1 = System.Drawing.Color.FromArgb($2)

--------------------------------------------------------------------------------------------------------------------


------- BACKSTYLE
------(?i)([Me\.]*lbl\w+)\.BackStyle = 0;R;$1.BorderStyle = BorderStyle.None
------(?i)([Me\.]*lbl\w+)\.BackStyle = 1;R;$1.BorderStyle = BorderStyle.FixedSingle
------(?i)([Me\.]*lbl\w+)\.BackStyle = 2;R;$1.BorderStyle = BorderStyle.Fixed3D
------(?i)([Me\.]*label\w+)\.BackStyle = 0;R;$1.BorderStyle = BorderStyle.None
------(?i)([Me\.]*label\w+)\.BackStyle = 1;R;$1.BorderStyle = BorderStyle.FixedSingle
------(?i)([Me\.]*label\w+)\.BackStyle = 2;R;$1.BorderStyle = BorderStyle.Fixed3D

--------------------------------------------------------------------------------------------------------------------------------------------------------
-- BackStyle (transparencia/VB6)  :        BackStyle = 0 (Transparent)     BackStyle = 1 (Opac)
-- En .Net la propietat BackStyle no existeix i és substituida per la propietat BackColor :
--               BackStyle = 0 --> BackColor = System.Drawing.Color.Transparent
--               BackStyle = 1 --> BackColor = BackColor/VB6 ( si en frm no existeix la propietat BackColor)
--											 BackColor = System.Drawing.SystemColors.Control ()    ( si en frm no existeix la propietat BackColor)
-- Condicio
If ([\w\(\)]+)\.BackStyle\s*=\s*1;R;If DirectCast($1, Label).BackColor <> System.Drawing.Color.Transparent

-- Assignacio
Me\.([\w\(\)]+)\.BackStyle\s*=\s*1;R;''MIGRAR$0
--------------------------------------------------------------------------------------------------------------------------------------------------------


-- AxDatePicker
-------------------------------------------------------------------------------------------------
Me\.(\w+)\.Show3DBorder = 2;R;Me.$1.BorderStyle = XtremeCalendarControl.DatePickerBorderStyle.xtpDatePickerBorderOffice


------- FSBORDERSTYLE
(?i)([Me\.]*lbl\w+)\.FSBorderStyle = 0;R;$1.FlatStyle = FlatStyle.Flat
(?i)([Me\.]*lbl\w+)\.FSBorderStyle = 1;R;$1.FlatStyle = FlatStyle.Popup
(?i)([Me\.]*lbl\w+)\.FSBorderStyle = 2;R;$1.FlatStyle = FlatStyle.Standard
(?i)([Me\.]*lbl\w+)\.FSBorderStyle = 3;R;$1.FlatStyle = FlatStyle.System
(?i)([Me\.]*label\w+)\.FSBorderStyle = 0;R;$1.FlatStyle = FlatStyle.Flat
(?i)([Me\.]*label\w+)\.FSBorderStyle = 1;R;$1.FlatStyle = FlatStyle.Popup
(?i)([Me\.]*label\w+)\.FSBorderStyle = 2;R;$1.FlatStyle = FlatStyle.Standard
(?i)([Me\.]*label\w+)\.FSBorderStyle = 3;R;$1.FlatStyle = FlatStyle.System
(?i)([Me\.]*txt\w+)\.FSBorderStyle = 0;R;$1.BorderStyle = BorderStyle.None
(?i)([Me\.]*txt\w+)\.FSBorderStyle = 1;R;$1.BorderStyle = BorderStyle.FixedSingle
(?i)([Me\.]*txt\w+)\.FSBorderStyle = 2;R;$1.BorderStyle = BorderStyle.Fixed3D
(?i)([Me\.]*text\w+)\.FSBorderStyle = 0;R;$1.BorderStyle = BorderStyle.None
(?i)([Me\.]*text\w+)\.FSBorderStyle = 1;R;$1.BorderStyle = BorderStyle.FixedSingle
(?i)([Me\.]*text\w+)\.FSBorderStyle = 2;R;$1.BorderStyle = BorderStyle.Fixed3D




--(?i)(frm\w+)\.Caption;R;$1.Text
--MDI.Caption;N;MDI.Text
Formulari.Caption;N;Formulari.Text
(?i)([Me\.]*lbl\w+)\.Caption;R;$1.Text
(?i)([Me\.]*Label\w+)\.Caption;R;$1.Text
--(?i)(frm\w+).Caption;R;$1.Text
--(\w*)\.Caption;R;$1.Text     INCORRECTE PERQUE EN EL CAS CODEJOC/CHECKBOX S'HA DE MANTENIR EL CAPTION I FEM LA SUBSTITUCIO PORTA MOLTS PROBLEMES DE RESULTATS DE LOGICA DE NEGOCI
Menus\((\w+)\)\.Caption;R;Menus($1).Text
Menus\((\w+)\)\.MenuItems\((\w+)\)\.Caption;R;Menus($1).MenuItems($2).Text
Menus\((\w+)\)\.MenuItems\.Item\((\w+)\)\.Caption;R;Menus($1).MenuItems.Item($2).Text

-- propietat Text d'un formulari
(frm\w+)\.Caption;R;$1.Text
(frame\w+)\.Caption;R;$1.Text


(?i)([Me\.]*\w+)\.RightToLeft = False;R;$1.RightToLeft = System.Windows.Forms.RightToLeft.No
(?i)([Me\.]*\w+)\.RightToLeft = True;R;$1.RightToLeft = System.Windows.Forms.RightToLeft.Yes


---------------------------------------------------------
-- Migració TreeView a GmsTreeView
---------------------------------------------------------
System\.Windows\.Forms\.TreeView\s*$;R;GmsTreeView
System\.Windows\.Forms\.TreeView,;R;GmsTreeView,
System\.Windows\.Forms\.TreeView\);R;GmsTreeView)
New System.Windows.Forms.TreeView\(\);R;New GmsTreeView()



AxMSComCtl2.AxDTPicker;R;System.Windows.Forms.DateTimePicker


-- per defecte els formularis tenen AutoSize = True.Cal que sigui False
-- Exemple : frmExecutaProcesClase
-------------Me.AutoSize = True;R;Me.AutoSize = False

-- WORKAROUND : Height dels controls
----------Me\.(\w+)\.AutoSize\s*=\s*False;R;Me.$1.AutoSize = True
Me\.(\w+)\.MultiLine\s*=\s*False;R;Me.$1.MultiLine = True


-- Transparencia
(?i)Me\.(Label\w+)\.BackColor = System.Drawing.Color.Transparent;R;Me.$1.BackColor = System.Drawing.SystemColors.Control
(?i)Me\.(lbl\w+)\.BackColor = System.Drawing.Color.Transparent;R;Me.$1.BackColor = System.Drawing.SystemColors.Control