VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4950
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   10695
   LinkTopic       =   "Form1"
   ScaleHeight     =   4950
   ScaleWidth      =   10695
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   3975
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   7011
      _Version        =   393217
      Style           =   7
      Appearance      =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Generar foto VBP"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   4320
      Width           =   2055
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9600
      Top             =   4200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FotoProjecte.frx":0000
            Key             =   "cd"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FotoProjecte.frx":0411
            Key             =   "drive"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FotoProjecte.frx":0643
            Key             =   "floppy"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FotoProjecte.frx":089F
            Key             =   "folder"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FotoProjecte.frx":0C91
            Key             =   "network"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Nom projecte :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2860
      TabIndex        =   3
      Top             =   4390
      Width           =   1815
   End
   Begin VB.Label lblProjecte 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   255
      Left            =   4800
      TabIndex        =   2
      Top             =   4390
      Width           =   2535
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Data()
Dim NBdata As Integer 'Obtener todos los archivos de una carpeta y subcarpetas
Dim fitxerVBP_Foto As Object
Dim fitxerVBP_Foto_Nom As String
Dim nomProjecte_Foto As String
Dim pathProjecte As String
Dim nomProjecte As String
Dim camiProjecte As String
Dim FSO As New FileSystemObject
Dim Dades() As String
Dim cami As String
Dim liniaMDIFormAfegida As Boolean    ' Revisio 2 : Maig 2020


'Si SubCarp = true `La carpeta de origen debe estar en Carp
Public Function LlegirFitxersVB6(ByVal Carp As String, Optional SubCarp As Boolean) As Integer
    Dim Obj, CarpP, F, S, sf, F1, Fsub
    Dim i As Integer, Ext As String
    Dim RepP As Folder
    Dim Ruta As String
    Dim T As Double ' Application.MousePointer = 13 'Para VB6
    Dim Index As Integer
    
    ReDim Dades(0)
    Index = 0
        
    Set Obj = CreateObject("Scripting.FileSystemObject")
    Set RepP = Obj.GetFolder(Carp)
    'CHEM = Carp:
    If Right(Ruta, 1) <> "\" Then Ruta = Ruta & "\"
        Set sf = RepP.SubFolders
       ' Set F = RepP.Files
       ' GoSub RellenarData 'los archivos de la carpeta principal
       ' If SubCarp Then 'los archivos de las subcarpetas
        
        For Each Fsub In sf
       '     Set CarpP = Fsub
       '     Set F = CarpP.Files
       '     GoSub RellenarData
       
       ' Revisio 2 : Maig 2020
       '------------------------------------------------------------------------------------------------------------------------------
        Dim camiRoot As String
        camiRoot = Replace(cami, pathProjecte, "M:\MP")   ' exemple : M:\MP\Media
                    
        Dim camiProjecteVB6 As String
        camiProjecteVB6 = Replace(Fsub, pathProjecte, "M:\MP")   ' exemples : M:\MP\Media\FormsComuns o M:\MP\Media\Colaboradors, o M:\MP\Configuracio\BORRAR , ...
                    
        Dim projecteExtensio As String
        projecteExtensio = Replace(camiProjecteVB6, camiRoot, "") ' exemple : FormsComuns o Colaboraors o Configuracio\BORRAR
                    
        Dim nomProjecteRoot As String
        If projecteExtensio = "" Then
            nomProjecteRoot = camiRoot
        Else
            nomProjecteRoot = camiRoot & "\" & projecteExtensio  '(1)
            
            Dim projecteVB6 As String
            projecteVB6 = Replace(nomProjecteRoot, pathProjecte, "M:\MP")
            
            If Not Obj.FolderExists(projecteVB6) Then GoTo continue   ' no existeix el projecte VB6.Per algun motiu pot haber-hi el NET (coses velles)
                                    
            Dim projecteFillVBP As String
            projecteFillVBP = projecteVBP(projecteVB6)
            
            If projecteFillVBP = "" Then    ' exemple : M:\MP\Media\FormsComuns
                ' els hem de tractar i per tant no hem de surtir de la Function
            Else
                ' ens hem de saltar el projecte
                GoTo continue
            End If
        End If
                    
        If Not Obj.FolderExists(nomProjecteRoot) Then GoTo continue
        ' -------------------------------------------------------------------------------------------------------------------------
              
        LlegirFitxersVB6 Fsub, True
            
continue:   ' Revisio 2 : Maig 2020
            
        Next Fsub
       
       Set F = RepP.Files
       GoSub RellenarData 'los archivos de la carpeta principal
       
       
        ' End If
    Exit Function '**********************************************************************

RellenarData:

    ' Revisio 2 : Maig 2020
    ' ------------------------------------
    'Dim liniaMDIFormAfegida As Boolean
    'liniaMDIFormAfegida = False
    ' ------------------------------------
        
    For Each F1 In F
        
    Dim j As String
    Dim longitudFitxer  As Integer
    
    camiProjecte = ""
        
    longitudFitxer = Len(F1.Name)
    j = Len(TreeView1.SelectedItem.FullPath)
        
    If (j + 1) < Len(F1) - longitudFitxer Then
        camiProjecte = Mid(F1, j + 1, Len(F1) - longitudFitxer - j - 1)
    End If
    
    If camiProjecte <> "" Then camiProjecte = "." & camiProjecte & "\"
   
        Ext = LCase(Right(F1.Name, 3))
        If Ext = "frm" Then
                ' Afegim a "pinyo" un formulari MDI "qualsevol"
                ' FER AIXO RESOLT EL PROBLEMA D'INCLOURE DURANT EL PROCES DE MIGRACIO WIZARD 2008
                ' LA INCLUSIO DEL "ME.MDIPARENT = MDI" (EN ELS "PROJECTES" QUE NO TENEN UN VBP, COM
                ' COMMON, FORMSCOMUNS,..., I QUE SON ELS QUE NECESSARIAMENT S'HA D'EXECUTAR
                ' AQUEST PROJECTE VB6 "FotoProjecteVB6")
                If Not liniaMDIFormAfegida = True Then
                    GravarLinia "MDI.frm", "Form="
                    
                    liniaMDIFormAfegida = True
                End If
                
                
                GravarLinia F1.Name, "Form=" & camiProjecte
                                
        End If
        If Ext = "bas" Then
        
            ' pot ser que el nom de la classe no coincideixi amb el del formulari
            ' busquem dins el fitxer "bas" una cadena de l'estil : Attribute VB_Name = "Conexio"
            ' llavors el nom de la classe es el que hi ha entre " . En aquest exemple "Conexio"
            ' Cal per� tamb� controlar que no hi sigui perque sino donaria error de conflicte de nom a l'obrir
            ' el projecte VB6
            Dim nomClasse As String
            
            nomClasse = BuscarNomClasse(F1.path)
            
            If ExisteixNomClasse(nomClasse) Then
                nomClasse = nomClasse & "1"   ' li afegim un _1
            End If
                                
            ReDim Preserve Dades(Index)
            Dades(Index) = nomClasse
            Index = Index + 1
                                                    
            GravarLinia F1.Name, "Module=" & nomClasse & "; " & camiProjecte
        End If
        If Ext = "cls" Then
            GravarLinia F1.Name, "Class=" & Extraer(F1.Name, ".") & "; " & camiProjecte
        End If
    Next F1
Return
End Function

Private Function ExisteixNomClasse(nomClasse As String) As Boolean
    Dim Index As Integer
    
    ExisteixNomClasse = False
        
    For Index = 0 To UBound(Dades)
      If Dades(Index) = nomClasse Then
        ExisteixNomClasse = True
        Exit For
      End If
    Next
End Function

Private Function BuscarNomClasse(fitxerBas As String) As String
    'Declare variables.
    Dim FSO As New FileSystemObject
    Dim ts As TextStream
    Dim linia As String
    Dim liniaNova As String
    Dim p1, p2 As String
    Dim p As String
    
    ' Obrim el fitxer "vbp" que utilitzarem per generar el no "vbp"
    ' Observacio : aquest fitxer ha d'existir previament
    ''''''Set ts = FSO.OpenTextFile(pathProjecte & nomProjecte & "\" & camiProjecte & "\" & fitxerBas)
    Set ts = FSO.OpenTextFile(fitxerBas)
           
    Do While Not ts.AtEndOfStream
        linia = ts.ReadLine

        liniaNova = InStr(1, linia, "Attribute VB_Name =")
        If liniaNova > 0 Then
            p1 = InStr(linia, """") + 1
            p2 = InStrRev(linia, """") - 1
            
            BuscarNomClasse = Mid(linia, p1, p2 - p1 + 1)
            Exit Do
        Else
             p = "1"
           
        End If
    Loop
    
trobat:
    'Close the file.
    ts.Close
End Function

Private Function PrimeraLletraMajuscula(cadena As String) As String
    PrimeraLletraMajuscula = UCase(Mid(cadena, 1, 1)) + Mid(cadena, 2, Len(cadena))
End Function

Function Extraer(path As String, Caracter As String) As String
    Dim ret As String
    If Caracter = "." And InStr(path, Caracter) = 0 Then Exit Function
    ret = Left(path, InStrRev(path, Caracter) - 1)
      
    ' -- Retorna el valor
    Extraer = ret
End Function

Private Function LlegirVBP(ByVal nomProjecte As String) As Boolean
    'Declare variables.
    Dim FSO As New FileSystemObject
    Dim ts As TextStream
    Dim linia As String
    Dim liniaNova As String
    
    ' Obrim el fitxer "vbp" que utilitzarem per generar el no "vbp"
    ' Observacio : aquest fitxer ha d'existir previament
    
    ' Revisio 2 : Maig 2020
    ' ---------------------------------------------------------------------------------
    ' Comprovem si existeix un fitxer vbp
    'If fso.FileExists(App.path & "\" & nomProjecte & "\" & nomProjecte & ".vbp") Then
    '    LlegirVBP = True
        
    '   Exit Function
    'End If
    ' ---------------------------------------------------------------------------------
    
    ' Comprovem si existeix el fitxer de projecte vbp "llavor"
    If FSO.FileExists(App.path & "\" & nomProjecte & "\" & nomProjecte & "_Pere.vbp") Then
        Set ts = FSO.OpenTextFile(App.path & "\" & nomProjecte & "\" & nomProjecte & "_Pere.vbp")
    Else
        Dim origen As String
        Dim desti As String
        origen = App.path & "\" & nomProjecte & "_Pere.vbp"
        desti = cami & "\" & nomProjecte & "_Pere.vbp"
         FSO.CopyFile origen, desti
         
         Set ts = FSO.OpenTextFile(desti)
    End If

   
    
    Do While Not ts.AtEndOfStream
        linia = ts.ReadLine
        
        ' afegim la linia si no cont� ni "Form=" , "Module=" , "Class=" , "Name="
        liniaNova = InStr(1, linia, "Form=") + InStr(1, linia, "Module=") + InStr(1, linia, "Class=") + InStr(1, linia, "Name=")
        If liniaNova = 0 Then
            GravarLinia linia
            GoSub trobat
        End If
        
        ' Mirem si la linia cont� "Name"
        liniaNova = InStr(1, linia, "Name=")
        If liniaNova > 0 Then
        
            ' Excloem si la linia cont� "VersionCompanyName="  o "VersionProductName"
            liniaNova = InStr(1, linia, "VersionCompanyName=") + InStr(1, linia, "VersionProductName")
            
            If liniaNova = 0 Then
                GravarLinia nomProjecte, "Name="
                GoSub trobat
            End If
        End If
                       
trobat:
        
    Loop
    'Close the file.
    ts.Close
    
    FSO.DeleteFile cami & "\" & nomProjecte & "_Pere.vbp"
    
    LlegirVBP = False
End Function

Private Sub GravarLinia(ByVal valor As String, Optional ByVal etiqueta As String = "")
   'Escribimos lineas
   If etiqueta = "" Then
        ' si hem d'escriure tota la linia
        fitxerVBP_Foto.WriteLine valor
   Else
        fitxerVBP_Foto.WriteLine etiqueta & valor
   End If
End Sub


Private Sub Listar_directorios(path As String, nodo As Node)

'On Error GoTo ErrSub

    Dim Carpeta As Folder, Subcarpeta As Folder
    
    ' referencia al directorio
    Set Carpeta = FSO.GetFolder(path)
    ' Recorre los directorios
    For Each Subcarpeta In Carpeta.SubFolders
        
        ' Agrega el directorio como nodo hijo _
         ( folder es la clave del icono en el imagelist)
        Call Listar_directorios(Subcarpeta.path, _
                                 TreeView1.Nodes.Add(nodo, tvwChild, , _
                                 Subcarpeta.Name, "folder"))
    Next
    
    
Exit Sub
'ErrSub:

    ' Por si da error por permiso denegado al intentar acceder a un directorio
    'If Err.Number = 70 Then
      '  Resume Next
    'End If
End Sub

' Revisio 2 : Maig 2020
'----------------------------------------------------------------------------------------------------------------------------
' retorna el nom del fitxer vbp a partir del cami del projecte,Si no existeix retorna ""
Private Function projecteVBP(cami As String) As String
    Dim Obj As Object
    Set Obj = CreateObject("Scripting.FileSystemObject")
    
   ' comprovem si la carpeta seleccionada correspon a un projecte (per tant t� un fitxer vbp associat) o no
    Dim camiProjecteVB6 As String
    camiProjecteVB6 = Replace(cami, pathProjecte, "M:\MP")  ' busquem el vbp en la part VB6
    
    If Not Obj.FolderExists(camiProjecteVB6) Then Exit Function
    
    Dim objFolder As Folder
    Set objFolder = Obj.GetFolder(camiProjecteVB6)
    Dim colFiles
    Set colFiles = objFolder.Files
    Dim objFile As File
    
       For Each objFile In colFiles
       If Obj.GetExtensionName(objFile) = "vbp" Then
        projecteVBP = objFile
        Exit Function
       End If
    Next
        
End Function

' retorna True o False si donat un cami dun projecte cont� un fitxer vbp o no
Private Function EsProjecte(cami As String) As Boolean
    Dim Obj As Object
    Set Obj = CreateObject("Scripting.FileSystemObject")
    
   ' comprovem si la carpeta seleccionada correspon a un projecte (per tant t� un fitxer vbp associat) o no
    Dim camiProjecteVB6 As String
    camiProjecteVB6 = Replace(cami, pathProjecte, "M:\MP")  ' busquem el vbp en la part VB6

    Dim objFolder As Folder
    Set objFolder = Obj.GetFolder(camiProjecteVB6)
    Dim colFiles
    Set colFiles = objFolder.Files
    Dim objFile As File
    
    EsProjecte = False
    For Each objFile In colFiles
       If Obj.GetExtensionName(objFile) = "vbp" Then
        EsProjecte = True
       End If
    Next
End Function
'----------------------------------------------------------------------------------------------------------------------------

Private Sub Command1_Click()
    Dim Obj As Object
    Dim dataFoto As String
    
    Dim teVBP As Boolean
                  
    Set Obj = CreateObject("Scripting.FileSystemObject")
       
    liniaMDIFormAfegida = False    ' Revisio 2 : Maig 2020

    ' data
    dataFoto = Format(Now, "YYYYMMDD")
    
    If TreeView1.SelectedItem Is Nothing Then Exit Sub
    
    lblProjecte.Caption = TreeView1.SelectedItem
    cami = TreeView1.SelectedItem.FullPath
        
    nomProjecte = Replace(Mid(cami, 43), "\", "_")     ' lblProjecte.Caption
        
        
    nomProjecte_Foto = nomProjecte & "_Pere_" & dataFoto
    
    ''''''fitxerVBP_Foto_Nom = App.path & "\" & nomProjecte & "\" & nomProjecte_Foto & ".vbp"
    fitxerVBP_Foto_Nom = cami & "\" & nomProjecte_Foto & ".vbp"

    If (Obj.FileExists(fitxerVBP_Foto_Nom)) Then
        ' Esborrem fitxer a l'inici
        Obj.DeleteFile fitxerVBP_Foto_Nom, True
    End If

    ' Creem l'arxiu "vbp" de la "foto"
    ' App.path = M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP
    Set fitxerVBP_Foto = Obj.CreateTextFile(fitxerVBP_Foto_Nom, True)
           
    ' Llegim el fitxer "vbp" actual i gravem les linies que no son ni "Form=" ni "Module=" al fitxer "vbp" que estem creant
    teVBP = LlegirVBP(nomProjecte)
        
   ''''''' LlegirFitxersVB6 pathProjecte & nomProjecte, True
    If teVBP = False Then
        LlegirFitxersVB6 cami, True
    End If
    
    'Cerramos el fichero
    fitxerVBP_Foto.Close
    
    Dim desti As String
    desti = cami & "\" & nomProjecte & ".vbp"
    If (Obj.FileExists(desti)) Then
        FSO.CopyFile fitxerVBP_Foto_Nom, desti
        Obj.DeleteFile fitxerVBP_Foto_Nom, True
    Else
        FSO.MoveFile fitxerVBP_Foto_Nom, desti
    End If
    
    ' ELS FORMULARIS DE PER EXEMPLE ELS PROJECTES "COMMON", "FORMSCOMUNS",... I QUE
    ' SON MDICHILD, A L'EXECUTAR EL WIZARD 2008 NO AFEGEIX LA LINIA "Me.MdiParent = MDI"
    ' SOLUCIO :
    '        1.-COPIAR UN "MDI.FRM" I "MDI.FRX" QUALSEVOL (en aquest cas copiem un que tenima <CARPETA_BACKUP>
    '        2.-INCLOURE EN ELS CORRESPONENTS FITXERS <CARPETA_BACKUP>\XXXXXXX_pere.vbp
    '           LA LINIA  "Form=MDI.frm"
    FSO.CopyFile "M:\\Usuaris\\Pere\\BACKUP_LOCAL_FONTS_VB6\\MP\\MDI.frm", cami & "\"
    FSO.CopyFile "M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP\MDI.frx", cami & "\"
    
End Sub

Private Sub Form_Load()
    ' path de backups per migrar
    pathProjecte = "M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP"
    
    TreeView1.ImageList = ImageList1

    ' Carreguem arbre de carpetes
    Call Listar_directorios(pathProjecte, TreeView1.Nodes.Add(, , , pathProjecte, "folder"))
                                                
End Sub

Private Sub Text1_Change()

End Sub
