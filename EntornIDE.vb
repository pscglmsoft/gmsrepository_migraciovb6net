﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Windows.Forms
Imports EnvDTE
Imports EnvDTE80
Imports Microsoft.VisualStudio.Shell

Friend Module EntornIDE


    ''' <summary>
    ''' Retorna DTE
    ''' </summary>
    ''' <returns></returns>
    Public Function GetActiveIDE() As EnvDTE.DTE
        Return TryCast(Marshal.GetActiveObject("VisualStudio.DTE.15.0"), EnvDTE.DTE)
    End Function

    'Public Function GetProjects() As IList(Of Project)
    '    Dim list As List(Of Project) = New List(Of Project)()
    '    Dim item = GetActiveIDE().Solution.Projects.GetEnumerator()

    '    While item.MoveNext()
    '        Dim project = TryCast(item.Current, Project)

    '        list.Add(project)

    '        If project Is Nothing Then
    '            Continue While
    '        End If
    '    End While

    '    Return list
    'End Function

    ''' <summary>
    ''' Itera "tots" (amb un format) els documents de la solució i aplica uns testos
    ''' </summary>
    ''' <returns></returns>
    Friend Function iterateFiles(ByVal lv As ListView) 'As [Function]

        Dim soln As Solution

        'soln.Open("M:\MP_Net\Entigest\Relaciones Publicas\RelacionesPublicasSolucio.sln")



        For i = 1 To GetActiveIDE.Solution.Projects.Count
            Dim KK = GetActiveIDE.Solution.Projects.Item(i).FullName
            iterateProjectFiles(GetActiveIDE.Solution.Projects.Item(i).ProjectItems, lv)
        Next
    End Function

    ''' <summary>
    ''' Retorna un "projecte" a partir del nom
    ''' </summary>
    ''' <param name="Name"></param>
    ''' <returns></returns>
    Public Function GetProject(ByVal Name As String) As EnvDTE.Project
        For Each item As EnvDTE.Project In GetActiveIDE.Solution.Projects

            If item.Name = Name Then
                Return item
            End If
        Next

        Return Nothing
    End Function

    ''' <summary>
    ''' Retorna una coleccio de "Document" que cumpleix un "format"
    ''' </summary>
    ''' <param name="projectItems"></param>
    ''' <returns></returns>
    Private Function iterateProjectFiles(ByVal projectItems As ProjectItems, ByVal lv As ListView) As List(Of Document)
        Dim llistaDocuments As New List(Of Document)

        For i = 1 To projectItems.Count
            Dim file = projectItems.Item(i)
            Dim document As Document = Nothing

            If file.SubProject IsNot Nothing Then
                document = formatFile(file)
                iterateProjectFiles(file.ProjectItems, lv)
            ElseIf file.ProjectItems IsNot Nothing AndAlso file.ProjectItems.Count > 0 Then
                document = formatFile(file)
                iterateProjectFiles(file.ProjectItems, lv)
            Else
                document = formatFile(file)
            End If

            If document IsNot Nothing Then
                llistaDocuments.Add(document)

                For Each lvi As ListViewItem In lv.Items

                    ' 3 = RegexCerca
                    ' 4 = RegexSubstitucio

                    FindRegex(document, lvi.SubItems(3).Text)

                Next

                'GetActiveIDE.ExecuteCommand("Edit.CollapsetoDefinitions")
                SaveAndClose(document)

            End If

        Next

        Return llistaDocuments
    End Function

    Private Function GetSolutionFolderProjects(ByVal solutionFolder As Project) As IEnumerable(Of Project)
        Dim list As List(Of Project) = New List(Of Project)()

        For i = 1 To solutionFolder.ProjectItems.Count
            Dim subProject = solutionFolder.ProjectItems.Item(i).SubProject

            If subProject Is Nothing Then
                Continue For
            End If
        Next

        Return list
    End Function

    '''' <summary>
    '''' 
    '''' </summary>
    'Private Sub GetProjectOfSolution()
    '    Dim Content = File.ReadAllText("M:\MP_Net\Entigest\Relaciones Publicas\RelacionesPublicasSolucio.sln")
    '    Dim projReg As Regex = New Regex("Project\(""\{[\w-]*\}""\) = ""([\w _]*.*)"", ""(.*\.(cs|vcx|vb)proj)""", RegexOptions.Compiled)
    '    Dim matches = projReg.Matches(Content).Cast(Of Match)()
    '    Dim Projects = matches.[Select](Function(x) x.Groups(2).Value).ToList()

    '    For i As Integer = 0 To Projects.Count - 1
    '        If Not Path.IsPathRooted(Projects(i)) Then Projects(i) = Path.Combine(Path.GetDirectoryName("M:\MP_Net\Entigest\Relaciones Publicas\RelacionesPublicasSolucio.sln"), Projects(i))
    '        Projects(i) = Path.GetFullPath(Projects(i))
    '    Next
    'End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="DTE"></param>
    ''' <param name="package"></param>
    Public Sub Run(ByVal DTE As EnvDTE.DTE, ByVal package As Microsoft.VisualStudio.Shell.Package)
        Dim options As Integer = CInt((EnvDTE.vsFindOptions.vsFindOptionsRegularExpression Or EnvDTE.vsFindOptions.vsFindOptionsMatchCase Or EnvDTE.vsFindOptions.vsFindOptionsMatchInHiddenText Or EnvDTE.vsFindOptions.vsFindOptionsSearchSubfolders Or EnvDTE.vsFindOptions.vsFindOptionsKeepModifiedDocumentsOpen))
        DTE.Find.FindReplace(EnvDTE.vsFindAction.vsFindActionReplaceAll, "(\.Register\w*)\(""([^""]+)""", options, "$1(nameof($2)", EnvDTE.vsFindTarget.vsFindTargetCurrentDocument)
    End Sub

    ''' <summary>
    ''' Cerca/Substitueix amb expressions regulars
    ''' </summary>
    ''' <param name="document"></param>
    ''' <param name="patro"></param>
    Sub FindReplaceRegex(ByVal document As Document, ByVal patro As String, ByVal cadenaSubstitucio As String)
        Dim objTextDoc As TextDocument
        Dim objEditPt As EditPoint
        Dim objFind As Find

        ' Get a handle to the new document and create an EditPoint.  
        objTextDoc = document.Object("TextDocument")
        objEditPt = objTextDoc.StartPoint.CreateEditPoint
        objFind = objTextDoc.DTE.Find

        objEditPt.StartOfDocument()
        objFind.FindReplace(vsFindAction.vsFindActionReplaceAll, patro, vsFindOptions.vsFindOptionsRegularExpression, cadenaSubstitucio, vsFindTarget.vsFindTargetOpenDocuments, , , vsFindResultsLocation.vsFindResultsNone)
    End Sub

    Sub FindRegex(ByVal document As Document, ByVal patro As String)
        Dim objTextDoc As TextDocument
        Dim objEditPt As EditPoint
        Dim objFind As Find

        ' Get a handle to the new document and create an EditPoint.  
        objTextDoc = document.Object("TextDocument")
        objEditPt = objTextDoc.StartPoint.CreateEditPoint
        objFind = objTextDoc.DTE.Find
        objFind.FilesOfType = "*.vb"
        objFind.KeepModifiedDocumentsOpen = False

        objEditPt.StartOfDocument()
        Dim resultat As vsFindResult = objFind.FindReplace(vsFindAction.vsFindActionFindAll, patro, vsFindOptions.vsFindOptionsRegularExpression, , vsFindTarget.vsFindTargetOpenDocuments, , , vsFindResultsLocation.vsFindResults1)

        Dim l = 1
    End Sub


    ''' <summary>
    ''' Retorna el document si cumpleix un criteri
    ''' </summary>
    ''' <param name="file"></param>
    ''' <returns></returns>
    Private Function formatFile(ByVal file As ProjectItem) As Document
        If file.Name.ToUpper.Contains("SUBSTITUIR.VB") OrElse
                file.Name.ToUpper.Contains("FRMTESTOS.VB") OrElse
                file.Name.ToUpper.Contains("MACRO.VB") OrElse
                file.Name.ToUpper.Contains("ENTORNIDE.VB") Then
            Exit Function
        End If


        GetActiveIDE.ExecuteCommand("View.SolutionExplorer")

        If (file.Name.IndexOf(".vb", file.Name.Length - ".vb".Length) <> -1) Then
            file.Open()
            file.Document.Activate()

            Return file.Document
        End If

        Return Nothing
    End Function

    Private Sub SaveAndClose(ByVal document As Document)
        'GetActiveIDE.ExecuteCommand("Edit.CollapsetoDefinitions")
        document.Save()
        document.Close()
    End Sub
End Module
