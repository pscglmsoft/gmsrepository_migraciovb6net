﻿Imports System.Drawing
Imports System.Runtime.InteropServices
Imports System.Security
Imports System.Text

''' <summary>
''' WORKAROUND
''' Autor : Pere Santasusana Cots
''' http://www.pinvoke.net/
''' 
''' Utilitat : PInvoke.net Visual Studio Extension   (PInvoke.net)
''' </summary>
<SuppressUnmanagedCodeSecurityAttribute()>
Public NotInheritable Class SafeNativeMethods

    Private Sub New()
    End Sub

    Public Const MAX_PATH As Short = 260

    ''''Structure RECT
    ''''    Dim left As Integer
    ''''    Dim top As Integer
    ''''    Dim right As Integer
    ''''    Dim bottom As Integer
    ''''End Structure

    ''' <summary>
    ''' http://www.pinvoke.net/default.aspx/Structures/RECT.html
    ''' </summary>
    <StructLayout(LayoutKind.Sequential)>
    Public Structure RECT
        Private _Left As Integer, _Top As Integer, _Right As Integer, _Bottom As Integer

        Public Sub New(ByVal Rectangle As Rectangle)
            Me.New(Rectangle.Left, Rectangle.Top, Rectangle.Right, Rectangle.Bottom)
        End Sub
        Public Sub New(ByVal Left As Integer, ByVal Top As Integer, ByVal Right As Integer, ByVal Bottom As Integer)
            _Left = Left
            _Top = Top
            _Right = Right
            _Bottom = Bottom
        End Sub

        Public Property X As Integer
            Get
                Return _Left
            End Get
            Set(ByVal value As Integer)
                _Right = _Right - _Left + value
                _Left = value
            End Set
        End Property
        Public Property Y As Integer
            Get
                Return _Top
            End Get
            Set(ByVal value As Integer)
                _Bottom = _Bottom - _Top + value
                _Top = value
            End Set
        End Property
        Public Property Left As Integer
            Get
                Return _Left
            End Get
            Set(ByVal value As Integer)
                _Left = value
            End Set
        End Property
        Public Property Top As Integer
            Get
                Return _Top
            End Get
            Set(ByVal value As Integer)
                _Top = value
            End Set
        End Property
        Public Property Right As Integer
            Get
                Return _Right
            End Get
            Set(ByVal value As Integer)
                _Right = value
            End Set
        End Property
        Public Property Bottom As Integer
            Get
                Return _Bottom
            End Get
            Set(ByVal value As Integer)
                _Bottom = value
            End Set
        End Property
        Public Property Height() As Integer
            Get
                Return _Bottom - _Top
            End Get
            Set(ByVal value As Integer)
                _Bottom = value + _Top
            End Set
        End Property
        Public Property Width() As Integer
            Get
                Return _Right - _Left
            End Get
            Set(ByVal value As Integer)
                _Right = value + _Left
            End Set
        End Property
        Public Property Location() As Point
            Get
                Return New Point(Left, Top)
            End Get
            Set(ByVal value As Point)
                _Right = _Right - _Left + value.X
                _Bottom = _Bottom - _Top + value.Y
                _Left = value.X
                _Top = value.Y
            End Set
        End Property
        ''''Public Property Size() As Size
        ''''    Get
        ''''        Return New Size(Width, Height)
        ''''    End Get
        ''''    Set(ByVal value As Size)
        ''''        _Right = value.Width + _Left
        ''''        _Bottom = value.Height + _Top
        ''''    End Set
        ''''End Property

        Public Shared Widening Operator CType(ByVal Rectangle As RECT) As Rectangle
            Return New Rectangle(Rectangle.Left, Rectangle.Top, Rectangle.Width, Rectangle.Height)
        End Operator
        Public Shared Widening Operator CType(ByVal Rectangle As Rectangle) As RECT
            Return New RECT(Rectangle.Left, Rectangle.Top, Rectangle.Right, Rectangle.Bottom)
        End Operator
        Public Shared Operator =(ByVal Rectangle1 As RECT, ByVal Rectangle2 As RECT) As Boolean
            Return Rectangle1.Equals(Rectangle2)
        End Operator
        Public Shared Operator <>(ByVal Rectangle1 As RECT, ByVal Rectangle2 As RECT) As Boolean
            Return Not Rectangle1.Equals(Rectangle2)
        End Operator

        Public Overrides Function ToString() As String
            Return "{Left: " & _Left & "; " & "Top: " & _Top & "; Right: " & _Right & "; Bottom: " & _Bottom & "}"
        End Function

        Public Overloads Function Equals(ByVal Rectangle As RECT) As Boolean
            Return Rectangle.Left = _Left AndAlso Rectangle.Top = _Top AndAlso Rectangle.Right = _Right AndAlso Rectangle.Bottom = _Bottom
        End Function
        Public Overloads Overrides Function Equals(ByVal [Object] As Object) As Boolean
            If TypeOf [Object] Is RECT Then
                Return Equals(DirectCast([Object], RECT))
            ElseIf TypeOf [Object] Is Rectangle Then
                Return Equals(New RECT(DirectCast([Object], Rectangle)))
            End If

            Return False
        End Function
    End Structure


    Structure SID_IDENTIFIER_AUTHORITY
        <VBFixedArray(6)> Dim Value() As Byte

        'UPGRADE_TODO: Se debe llamar a "Initialize" para inicializar instancias de esta estructura. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
        Public Sub Initialize()
            ReDim Value(6)
        End Sub
    End Structure



    'UPGRADE_WARNING: La estructura Point puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'Public Declare Function GetCursorPos Lib "user32" (ByRef lpPoint As POINTAPI) As Integer


    Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias "SHGetPathFromIDListA" (ByVal pidl As Integer, ByVal pszPath As String) As Integer

    'UPGRADE_WARNING: La estructura BrowseInfo puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Declare Function SHBrowseForFolder Lib "shell32.dll" Alias "SHBrowseForFolderA" (ByRef lpBrowseInfo As BrowseInfo) As Integer
    'UPGRADE_WARNING: La estructura OperatingSystem puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (ByRef lpVersionInformation As OperatingSystem) As Integer

    'Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As Integer)

    Declare Function GetKeyboardState Lib "user32" (ByRef pbKeyState As Byte) As Integer

    Declare Function SetKeyboardState Lib "user32" (ByRef lppbKeyState As Byte) As Integer

    'Public Declare Function GetAsyncKeyState Lib "user32.dll" (ByVal vKey As Integer) As Short

    'Declare Function GetProcAddress Lib "kernel32" (ByVal hModule As Integer, ByVal lpProcName As String) As Integer

    Declare Function GetModuleHandle Lib "kernel32" Alias "GetModuleHandleA" (ByVal lpModuleName As String) As Integer

    'Declare Function GetCurrentProcess Lib "kernel32" () As Integer

    Declare Function IsWow64Process Lib "kernel32" (ByVal hProc As IntPtr, ByRef bWow64Process As Boolean) As Boolean
    'Public Declare Function GetWindowTextA Lib "user32" (ByVal hwnd As Integer, ByVal lpString As String, ByVal aint As Integer) As Integer
    'Public Declare Function GetForegroundWindow Lib "user32.dll" () As Integer
    Public Declare Function ImageList_Draw Lib "comctl32.dll" (ByVal himl As Integer, ByVal I As Integer, ByVal hDCDest As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal Flags As Integer) As Integer
    ''UPGRADE_WARNING: La estructura SHFILEINFO puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Public Declare Function SHGetFileInfo Lib "shell32.dll" Alias "SHGetFileInfoA" (ByVal pszPath As String, ByVal dwFileAttributes As Integer, ByRef psfi As SHFILEINFO, ByVal cbFileInfo As Integer, ByVal uFlags As Integer) As Integer
    Public Declare Function GetActiveWindow Lib "user32" () As IntPtr
    'Public Declare Function SetWindowPos Lib "user32" (ByVal hwnd As IntPtr, ByVal hWndInsertAfter As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer) As Integer


    'Public Declare Function GetSystemDirectory Lib "kernel32.dll" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
    Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As IntPtr, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Integer) As Integer

    '=== funcions per tractar l'imatge de fons del MDI
    Public Declare Function FindWindowEx Lib "user32" Alias "FindWindowExA" (ByVal hWnd1 As Integer, ByVal hWnd2 As Integer, ByVal lpsz1 As String, ByVal lpsz2 As String) As Integer
    'UPGRADE_WARNING: La estructura RECT puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Public Declare Function InvalidateRect Lib "user32" (ByVal hwnd As Integer, ByRef lpRect As RECT, ByVal bErase As Integer) As Integer

    ''= funcions per pintar icones a les capçaleres dels flex cell grid
    ''UPGRADE_WARNING: La estructura RECT puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'Public Declare Function SetRect Lib "user32" (ByRef lpRect As RECT, ByVal X1 As Integer, ByVal Y1 As Integer, ByVal X2 As Integer, ByVal Y2 As Integer) As Integer
    'Public Declare Function DrawIconEx Lib "user32" (ByVal hDC As Integer, ByVal xLeft As Integer, ByVal yTop As Integer, ByVal hIcon As Integer, ByVal cxWidth As Integer, ByVal cyWidth As Integer, ByVal istepIfAniCur As Integer, ByVal hbrFlickerFreeDraw As Integer, ByVal diFlags As Integer) As Integer
    ''UPGRADE_WARNING: La estructura RECT puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'Public Declare Function DrawText Lib "user32" Alias "DrawTextA" (ByVal hDC As Integer, ByVal lpStr As String, ByVal nCount As Integer, ByRef lpRect As RECT, ByVal wFormat As Integer) As Integer
    Public Declare Function WriteProfileString Lib "kernel32" Alias "WriteProfileStringA" (ByVal lpszSection As String, ByVal lpszKeyName As String, ByVal lpszString As String) As Integer
    ''UPGRADE_ISSUE: No se admite la declaración de un parámetro 'As Any'. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    ''UPGRADE_ISSUE: No se admite la declaración de un parámetro 'As Any'. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Object, ByVal lpString As Object, ByVal lpFileName As String) As Integer
    'Public Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer
    'Public Declare Function GetPrivateProfileSection Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    ''UPGRADE_ISSUE: No se admite la declaración de un parámetro 'As Any'. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    ''''Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Object, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Declare Function GetProfileString Lib "kernel32" Alias "GetProfileStringA" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer) As Integer
    ''UPGRADE_WARNING: La estructura RECT puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'Public Declare Function DrawFocusRect Lib "user32" (ByVal hDC As Integer, ByRef lpRect As RECT) As Integer
    'Public Declare Function GetDC Lib "user32" (ByVal hwnd As Integer) As Integer
    ''UPGRADE_WARNING: La estructura RECT puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Public Declare Function GetWindowRect Lib "user32" (ByVal hwnd As IntPtr, ByRef lpRect As SafeNativeMethods.RECT) As Boolean
    'Public Declare Function ReleaseDC Lib "user32" (ByVal hwnd As Integer, ByVal hDC As Integer) As Integer

    Public Declare Function GetPrivateProfileSectionNames Lib "kernel32" Alias "GetPrivateProfileSectionNamesA" (ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer

    ' Pere : s'ha afegit per suportar la funció degut a la migració de VB6.FixedLengthString a StringBuilder
    Public Declare Ansi Function GetPrivateProfileString _
  Lib "kernel32.dll" Alias "GetPrivateProfileStringA" _
  (ByVal lpApplicationName As String,
  ByVal lpKeyName As String, ByVal lpDefault As String,
  ByVal lpReturnedString As System.Text.StringBuilder,
  ByVal nSize As Integer, ByVal lpFileName As String) _
  As Integer


    'Const MF_BYPOSITION As Integer = &H400
    'Public Declare Function SetMenuItemBitmaps Lib "user32" (ByVal hMenu As Integer, ByVal nPosition As Integer, ByVal wFlags As Integer, ByVal hBitmapUnchecked As Integer, ByVal hBitmapChecked As Integer) As Integer
    'Public Declare Function GetMenu Lib "user32" (ByVal hwnd As Integer) As Integer
    'Public Declare Function GetSubMenu Lib "user32" (ByVal hMenu As Integer, ByVal nPos As Integer) As Integer
    ''UPGRADE_WARNING: La estructura MENUITEMINFO puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'Public Declare Function GetMenuItemInfo Lib "user32" Alias "GetMenuItemInfoA" (ByVal hMenu As Integer, ByVal un As Integer, ByVal B As Boolean, ByRef lpmii As MENUITEMINFO) As Integer
    ''UPGRADE_WARNING: La estructura MENUITEMINFO puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'Public Declare Function SetMenuItemInfo Lib "user32" Alias "SetMenuItemInfoA" (ByVal hMenu As Integer, ByVal un As Integer, ByVal bool As Boolean, ByRef lpcMenuItemInfo As MENUITEMINFO) As Integer

    'Public Declare Function SetParent Lib "user32" (ByVal hWndChild As IntPtr, ByVal hWndNewParent As Integer) As Integer
    Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As IntPtr, ByVal nIndex As Integer, ByVal dwNewLong As Integer) As Integer

    Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Integer, ByVal nIndex As Integer) As Integer

    ''UPGRADE_WARNING: La estructura SID_IDENTIFIER_AUTHORITY puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Declare Function AllocateAndInitializeSid Lib "Advapi32" (ByRef pIdentifierAuthority As SID_IDENTIFIER_AUTHORITY, ByVal nSubAuthorityCount As Byte, ByVal nSubAuthority0 As Integer, ByVal nSubAuthority1 As Integer, ByVal nSubAuthority2 As Integer, ByVal nSubAuthority3 As Integer, ByVal nSubAuthority4 As Integer, ByVal nSubAuthority5 As Integer, ByVal nSubAuthority6 As Integer, ByVal nSubAuthority7 As Integer, ByRef lpPSid As Integer) As Integer
    Declare Function CheckTokenMembership Lib "Advapi32" (ByVal TokenHandle As Integer, ByVal SidToCheck As Integer, ByRef IsMember As Integer) As Integer
    'Declare Sub FreeSid Lib "advapi32.dll" (ByVal pSid As Integer)

    'UPGRADE_ISSUE: No se admite la declaración de un parámetro 'As Any'. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.AsAny)> ByRef lParam As Object) As Integer ' <---

    '<DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    'Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As StringBuilder) As IntPtr
    'End Function

    '<DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    'Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As IntPtr, ByVal wParam As IntPtr, ByRef lParam As SafeNativeMethods.RECT) As Integer
    'End Function

    '<DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    'Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Boolean, ByRef lParam As SafeNativeMethods.TVHITTESTINFO) As IntPtr
    'End Function


    '<DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    'Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.AsAny)> ByVal lParam As Object) As Integer
    'End Function

    '<DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    'Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, <MarshalAs(UnmanagedType.LPWStr)> ByVal lParam As String) As IntPtr
    'End Function

    '<DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    'Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.LPWStr)> ByVal lParam As String) As IntPtr
    'End Function

    '<DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    'Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As IntPtr) As IntPtr
    'End Function



    Public Structure MENUITEMINFO
        Dim cbSize As Integer
        Dim fMask As Integer
        Dim fType As Integer
        Dim fState As Integer
        Dim wID As Integer
        Dim hSubMenu As Integer
        Dim hbmpChecked As Integer
        Dim hbmpUnchecked As Integer
        Dim dwItemData As Integer
        Dim dwTypeData As String
        Dim cch As Integer
    End Structure


    ''' <summary>
    ''' http://www.pinvoke.net/default.aspx/user32.drawfocusrect
    ''' </summary>
    ''' <param name="hDC"></param>
    ''' <param name="lpRect"></param>
    ''' <returns></returns>
    Public Declare Function DrawFocusRect Lib "user32" (ByVal hDC As IntPtr, ByRef lpRect As RECT) As Integer


#Region "Apis"

    Structure POINTAPI
        Dim X As Integer
        Dim Y As Integer
    End Structure

    ' Declare Type for API call:
    ''' <summary>
    ''' Estructura basada en API de Windows
    ''' </summary>
    <Obsolete("Aquesta estructura té una alternativa en NET")>
    Structure OSVERSIONINFO
        Dim dwOSVersionInfoSize As Integer
        Dim dwMajorVersion As Integer
        Dim dwMinorVersion As Integer
        Dim dwBuildNumber As Integer
        Dim dwPlatformId As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)> Public szCSDVersion As String
    End Structure


    Structure SHFILEINFO 'Structure used by SHGetFileInfo
        Dim hIcon As Integer
        Dim iIcon As Integer
        Dim dwAttributes As Integer
        'UPGRADE_WARNING: El tamaño de la cadena de longitud fija debe caber en el búfer. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
        <VBFixedString(MAX_PATH), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=MAX_PATH)> Public szDisplayName() As Char
        'UPGRADE_WARNING: El tamaño de la cadena de longitud fija debe caber en el búfer. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
        <VBFixedString(80), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=80)> Public szTypeName() As Char
    End Structure

    Public Structure BrowseInfo
        Dim hOwner As Integer
        Dim pIDLRoot As Integer
        Dim pszDisplayName As String
        Dim lpszTitle As String
        Dim ulFlags As Integer
        Dim lpfn As Integer
        Dim lParam As Integer
        Dim iImage As Integer
    End Structure

    '''' <summary>
    '''' 
    '''' </summary>
    '<System.Runtime.InteropServices.StructLayout(Runtime.InteropServices.LayoutKind.Sequential)>
    'Public Structure POINT
    '    Public X As Integer
    '    Public Y As Integer

    '    Public Sub New(ByVal X As Integer, ByVal Y As Integer)
    '        Me.X = X
    '        Me.Y = Y
    '    End Sub
    'End Structure


    ''' <summary>
    ''' http://pinvoke.net/default.aspx/Enums/SetWindowPosFlags.html
    ''' </summary>
    <Flags>
    Public Enum SetWindowPosFlags As UInteger
        ''' <summary>If the calling thread and the thread that owns the window are attached to different input queues, 
        ''' the system posts the request to the thread that owns the window. This prevents the calling thread from 
        ''' blocking its execution while other threads process the request.</summary>
        ''' <remarks>SWP_ASYNCWINDOWPOS</remarks>
        SynchronousWindowPosition = &H4000
        ''' <summary>Prevents generation of the WM_SYNCPAINT message.</summary>
        ''' <remarks>SWP_DEFERERASE</remarks>
        DeferErase = &H2000
        ''' <summary>Draws a frame (defined in the window's class description) around the window.</summary>
        ''' <remarks>SWP_DRAWFRAME</remarks>
        DrawFrame = &H20
        ''' <summary>Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to 
        ''' the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE 
        ''' is sent only when the window's size is being changed.</summary>
        ''' <remarks>SWP_FRAMECHANGED</remarks>
        FrameChanged = &H20
        ''' <summary>Hides the window.</summary>
        ''' <remarks>SWP_HIDEWINDOW</remarks>
        HideWindow = &H80
        ''' <summary>Does not activate the window. If this flag is not set, the window is activated and moved to the 
        ''' top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter 
        ''' parameter).</summary>
        ''' <remarks>SWP_NOACTIVATE</remarks>
        DoNotActivate = &H10
        ''' <summary>Discards the entire contents of the client area. If this flag is not specified, the valid 
        ''' contents of the client area are saved and copied back into the client area after the window is sized or 
        ''' repositioned.</summary>
        ''' <remarks>SWP_NOCOPYBITS</remarks>
        DoNotCopyBits = &H100
        ''' <summary>Retains the current position (ignores X and Y parameters).</summary>
        ''' <remarks>SWP_NOMOVE</remarks>
        IgnoreMove = &H2
        ''' <summary>Does not change the owner window's position in the Z order.</summary>
        ''' <remarks>SWP_NOOWNERZORDER</remarks>
        DoNotChangeOwnerZOrder = &H200
        ''' <summary>Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to 
        ''' the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent 
        ''' window uncovered as a result of the window being moved. When this flag is set, the application must 
        ''' explicitly invalidate or redraw any parts of the window and parent window that need redrawing.</summary>
        ''' <remarks>SWP_NOREDRAW</remarks>
        DoNotRedraw = &H8
        ''' <summary>Same as the SWP_NOOWNERZORDER flag.</summary>
        ''' <remarks>SWP_NOREPOSITION</remarks>
        DoNotReposition = &H200
        ''' <summary>Prevents the window from receiving the WM_WINDOWPOSCHANGING message.</summary>
        ''' <remarks>SWP_NOSENDCHANGING</remarks>
        DoNotSendChangingEvent = &H400
        ''' <summary>Retains the current size (ignores the cx and cy parameters).</summary>
        ''' <remarks>SWP_NOSIZE</remarks>
        IgnoreResize = &H1
        ''' <summary>Retains the current Z order (ignores the hWndInsertAfter parameter).</summary>
        ''' <remarks>SWP_NOZORDER</remarks>
        IgnoreZOrder = &H4
        ''' <summary>Displays the window.</summary>
        ''' <remarks>SWP_SHOWWINDOW</remarks>
        ShowWindow = &H40
    End Enum

    ''' <summary>
    ''' http://www.pinvoke.net/default.aspx/user32.GetWindowLong
    ''' </summary>
    Public Enum GWL
        GWL_WNDPROC = -4
        GWL_HINSTANCE = -6
        GWL_HWNDPARENT = -8
        GWL_STYLE = -16
        GWL_EXSTYLE = -20
        GWL_USERDATA = -21
        GWL_ID = -12
    End Enum

    ''' <summary>
    ''' http://pinvoke.net/default.aspx/Enums/WindowLongFlags.html
    ''' </summary>
    Public Enum WindowLongFlags As Integer
        GWL_EXSTYLE = -20
        GWLP_HINSTANCE = -6
        GWLP_HWNDPARENT = -8
        GWL_ID = -12
        GWL_STYLE = -16
        GWL_USERDATA = -21
        GWL_WNDPROC = -4
        DWLP_USER = &H8
        DWLP_MSGRESULT = &H0
        DWLP_DLGPROC = &H4
    End Enum


    ''' <summary>Values to pass to the GetDCEx method.
    ''' http://www.pinvoke.net/default.aspx/Enums/DeviceContextValues.html
    ''' </summary>
    <Flags()>
    Private Enum DeviceContextValues As UInteger
        ''' <summary>DCX_WINDOW: Returns a DC that corresponds to the window rectangle rather 
        ''' than the client rectangle.</summary>
        Window = &H1
        ''' <summary>DCX_CACHE: Returns a DC from the cache, rather than the OWNDC or CLASSDC 
        ''' window. Essentially overrides CS_OWNDC and CS_CLASSDC.</summary>
        Cache = &H2
        ''' <summary>DCX_NORESETATTRS: Does not reset the attributes of this DC to the 
        ''' default attributes when this DC is released.</summary>
        NoResetAttrs = &H4
        ''' <summary>DCX_CLIPCHILDREN: Excludes the visible regions of all child windows 
        ''' below the window identified by hWnd.</summary>
        ClipChildren = &H8
        ''' <summary>DCX_CLIPSIBLINGS: Excludes the visible regions of all sibling windows 
        ''' above the window identified by hWnd.</summary>
        ClipSiblings = &H10
        ''' <summary>DCX_PARENTCLIP: Uses the visible region of the parent window. The 
        ''' parent's WS_CLIPCHILDREN and CS_PARENTDC style bits are ignored. The origin is 
        ''' set to the upper-left corner of the window identified by hWnd.</summary>
        ParentClip = &H20
        ''' <summary>DCX_EXCLUDERGN: The clipping region identified by hrgnClip is excluded 
        ''' from the visible region of the returned DC.</summary>
        ExcludeRgn = &H40
        ''' <summary>DCX_INTERSECTRGN: The clipping region identified by hrgnClip is 
        ''' intersected with the visible region of the returned DC.</summary>
        IntersectRgn = &H80
        ''' <summary>DCX_EXCLUDEUPDATE: Unknown...Undocumented</summary>
        ExcludeUpdate = &H100
        ''' <summary>DCX_INTERSECTUPDATE: Unknown...Undocumented</summary>
        IntersectUpdate = &H200
        ''' <summary>DCX_LOCKWINDOWUPDATE: Allows drawing even if there is a LockWindowUpdate 
        ''' call in effect that would otherwise exclude this window. Used for drawing during 
        ''' tracking.</summary>
        LockWindowUpdate = &H400
        ''' <summary>DCX_VALIDATE When specified with DCX_INTERSECTUPDATE, causes the DC to 
        ''' be completely validated. Using this function with both DCX_INTERSECTUPDATE and 
        ''' DCX_VALIDATE is identical to using the BeginPaint function.</summary>
        Validate = &H200000
    End Enum

    Friend Declare Function RegCloseKey Lib "advapi32.dll" (ByVal lngRootKey As IntPtr) As Integer

    'Friend Declare Function DrawFocusRect Lib "user32" (ByVal hDC As Integer, ByRef lpRect As RECT) As Integer
    Friend Declare Function GetClientRect Lib "user32" (ByVal hwnd As IntPtr, ByRef lpRect As RECT) As Integer



    ''MIGRATPublic Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (ByRef lpVersionInformation As OSVERSIONINFO) As Integer

    <DllImport("user32.dll")>
    Private Shared Function GetDCEx(ByVal hWnd As IntPtr, ByVal hrgnClip As IntPtr, ByVal DeviceContextValues As Integer) As IntPtr
    End Function

    Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Integer, ByVal dwExtraInfo As IntPtr)


    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto, SetLastError:=True)>
    Public Shared Function SetParent(ByVal hWndChild As IntPtr, ByVal hWndNewParent As IntPtr) As IntPtr
    End Function


    Public Declare Function ReleaseDC Lib "user32" (ByVal hwnd As IntPtr, ByVal hDC As IntPtr) As Integer

    <DllImport("user32.dll", EntryPoint:="GetWindowLong")>
    Public Shared Function GetWindowLongPtr(ByVal hWnd As HandleRef, <MarshalAs(UnmanagedType.I4)> nIndex As GWL) As IntPtr
    End Function


    Friend Declare Function SetMenuItemBitmaps Lib "user32" (ByVal hMenu As IntPtr, ByVal nPosition As Integer, ByVal wFlags As Integer, ByVal hBitmapUnchecked As IntPtr, ByVal hBitmapChecked As IntPtr) As Integer

    ''' <summary>
    ''' http://www.pinvoke.net/default.aspx/user32.SetWindowLong
    ''' </summary>
    ''' <param name="hWnd"></param>
    ''' <param name="nIndex"></param>
    ''' <param name="dwNewLong"></param>
    ''' <returns></returns>
    <System.Runtime.InteropServices.DllImport("user32.dll", EntryPoint:="SetWindowLong")>
    Private Shared Function SetWindowLong32(ByVal hWnd As IntPtr, <MarshalAs(UnmanagedType.I4)> nIndex As WindowLongFlags, ByVal dwNewLong As Integer) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("user32.dll", EntryPoint:="SetWindowLongPtr")>
    Private Shared Function SetWindowLongPtr64(ByVal hWnd As IntPtr, <MarshalAs(UnmanagedType.I4)> nIndex As WindowLongFlags, ByVal dwNewLong As IntPtr) As IntPtr
    End Function

    Public Shared Function SetWindowLongPtr(ByVal hWnd As IntPtr, nIndex As WindowLongFlags, ByVal dwNewLong As IntPtr) As IntPtr
        If IntPtr.Size = 8 Then
            Return SetWindowLongPtr64(hWnd, nIndex, dwNewLong)
        Else
            Return New IntPtr(SetWindowLong32(hWnd, nIndex, dwNewLong.ToInt32))
        End If
    End Function


    <DllImport("user32.dll", SetLastError:=True)>
    Public Shared Function SetWindowPos(ByVal hWnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As SetWindowPosFlags) As Boolean
    End Function

#End Region

#Region "DragDropTreeView"

    '
    ' Copyright © 1997-1999 Brad Martinez, http://www.mvps.org
    '
    ' ===========================================================================
    ' user-defined enumerations
    'Type POINTAPI          ' pt
    '  x As Long
    '  y As Long
    'End Type
    Structure Size
        Dim cx As Integer
        Dim cy As Integer
    End Structure
    'Type RECT          ' rct
    '  Left As Long
    '  Top As Long
    '  Right As Long
    '  Bottom As Long
    'End Type

    Public Enum ScrollDirectionFlags
        sdLeft = &H1
        sdUp = &H2
        sdRight = &H4
        sdDown = &H8
    End Enum

    Public Enum RectFlags
        rfLeft = &H1
        rfTop = &H2
        rfRight = &H4
        rfBottom = &H8
    End Enum

    Public Enum CBoolean
        CFalse = 0
        CTrue = 1
    End Enum

    ' ===========================================================================
    ' general window definitions

    Public Const SM_CXDRAG As Integer = 68
    Public Const SM_CYDRAG As Integer = 69
    Public Declare Function SetCapture Lib "user32" (ByVal hwnd As Integer) As Integer
    Public Declare Function ReleaseCapture Lib "user32" () As Integer

    'UPGRADE_WARNING: La estructura RECT puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Public Declare Function GetClientRect Lib "user32" (ByVal hwnd As Integer, ByRef lpRect As RECT) As Integer
    'UPGRADE_ISSUE: No se admite la declaración de un parámetro 'As Any'. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    Public Declare Function ScreenToClient Lib "user32" (ByVal hwnd As IntPtr, ByRef lpPoint As POINTAPI) As Integer ' lpPoint As POINTAPI) As Long
    'UPGRADE_ISSUE: No se admite la declaración de un parámetro 'As Any'. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    Public Declare Function ClientToScreen Lib "user32" (ByVal hwnd As Integer, ByRef lpPoint As Object) As Integer ' lpPoint As POINTAPI) As Long
    'UPGRADE_WARNING: La estructura RECT puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Public Declare Function PtInRect Lib "user32" (ByRef lprc As RECT, ByVal x As Integer, ByVal y As Integer) As Integer

    Public Declare Function IsWindow Lib "user32" (ByVal hwnd As Integer) As Integer
    Public Declare Function UpdateWindow Lib "user32" (ByVal hwnd As Integer) As Integer

    Public Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Integer) As Integer

    'UPGRADE_WARNING: La estructura SCROLLINFO puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'UPGRADE_WARNING: La estructura SB_Type puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Public Declare Function GetScrollInfo Lib "user32" (ByVal hwnd As Integer, ByRef fnBar As SB_Type, ByRef lpsi As SCROLLINFO) As Boolean
    Public Declare Function GetScrollPos Lib "user32" (ByVal hwnd As Integer, ByVal nBar As Integer) As Integer

    'Public const GWL_STYLE = (-16)

    ' ===========================================================================
    ' scrollbar definitions

    Public Structure SCROLLINFO
        Dim cbSize As Integer
        Dim fMask As SIF_Mask
        Dim nMin As Integer
        Dim nMax As Integer
        Dim nPage As Integer
        Dim nPos As Integer
        Dim nTrackPos As Integer
    End Structure

    Public Enum SIF_Mask
        SIF_RANGE = &H1
        SIF_PAGE = &H2
        SIF_POS = &H4
        SIF_DISABLENOSCROLL = &H8
        SIF_TRACKPOS = &H10
        SIF_ALL = (SIF_Mask.SIF_RANGE Or SIF_Mask.SIF_PAGE Or SIF_Mask.SIF_POS Or SIF_Mask.SIF_TRACKPOS)
    End Enum

    Public Enum SB_Type
        SB_HORZ = 0
        SB_VERT = 1
        SB_CTL = 2
        SB_BOTH = 3
    End Enum

    Public Const WS_HSCROLL As Integer = &H100000
    Public Const WS_VSCROLL As Integer = &H200000

    ' Scroll Bar Commands for WM_H/VSCROLL
    Public Const SB_LINEUP As Short = 0
    Public Const SB_LINELEFT As Short = 0
    Public Const SB_LINEDOWN As Short = 1
    Public Const SB_LINERIGHT As Short = 1

    Public Const WM_HSCROLL As Integer = &H114
    Public Const WM_VSCROLL As Integer = &H115

    ' ===========================================================================
    ' imagelist definitions

    'Declare Function ImageList_GetImageCount Lib "comctl32.dll" (ByVal himl As Long) As Long
    Declare Function ImageList_GetIconSize Lib "comctl32.dll" (ByVal himl As Integer, ByRef lpcx As Integer, ByRef lpcy As Integer) As Boolean

    Declare Function ImageList_BeginDrag Lib "comctl32.dll" (ByVal himlTrack As Integer, ByVal iTrack As Integer, ByVal dxHotspot As Integer, ByVal dyHotspot As Integer) As CBoolean
    Declare Function ImageList_DragEnter Lib "comctl32.dll" (ByVal hwndLock As Integer, ByVal x As Integer, ByVal y As Integer) As CBoolean
    Declare Function ImageList_DragMove Lib "comctl32.dll" (ByVal x As Integer, ByVal y As Integer) As CBoolean
    Declare Function ImageList_DragLeave Lib "comctl32.dll" (ByVal hwndLock As Integer) As CBoolean
    Declare Sub ImageList_EndDrag Lib "comctl32.dll" ()

    Declare Function ImageList_DragShowNolock Lib "comctl32.dll" (ByVal fShow As Boolean) As CBoolean

    Declare Function ImageList_Destroy Lib "comctl32.dll" (ByVal himl As Integer) As CBoolean

    ' ===========================================================================
    ' treeview definitions

    '' User-defined as the maximum treeview item text length.
    '' If an items text exceeds this value when calling GetTVItemText, there will be problems...
    'Public const MAX_ITEM = 256
    '
    '' Callback constants
    '
    '' T/LVITEM.pszText
    'Public const LPSTR_TEXTCALLBACK = (-1)
    '
    '' TVITEM.cChildren
    'Public const I_CHILDRENCALLBACK = (-1&)

    ' TVITEM.iImage/iSelectedImage, LVITEM.iImage
    Public Const I_IMAGECALLBACK As Short = (-1)

    ' messages
    Public Const TV_FIRST As Integer = &H1100
    Public Const TVM_EXPAND As Integer = (TV_FIRST + 2)
    Public Const TVM_GETITEMRECT As Integer = (TV_FIRST + 4)
    Public Const TVM_GETNEXTITEM As Integer = (TV_FIRST + 10)
    Public Const TVM_SELECTITEM As Integer = (TV_FIRST + 11)
    Public Const TVM_GETITEM As Integer = (TV_FIRST + 12)
    Public Const TVM_SETITEM As Integer = (TV_FIRST + 13)
    Public Const TVM_HITTEST As Integer = (TV_FIRST + 17)
    Public Const TVM_CREATEDRAGIMAGE As Integer = (TV_FIRST + 18)

    ' TVM_EXPAND wParam action flags
    Public Const TVE_EXPAND As Integer = &H2

    ' TVM_GETNEXTITEM wParam values
    Public Const TVGN_ROOT As Integer = &H0
    Public Const TVGN_PARENT As Integer = &H3
    'Public const TVGN_CHILD = &H4
    Public Const TVGN_DROPHILITE As Integer = &H8

    ' TVM_GET/SETITEM lParam
    Public Structure TVITEM ' was TV_ITEM
        Dim mask As Integer
        Dim hitem As Integer
        Dim State As Integer
        Dim stateMask As Integer
        Dim pszText As Integer ' pointer, if a string, must be pre-allocated before being filled
        Dim cchTextMax As Integer
        Dim iImage As Integer
        Dim iSelectedImage As Integer
        Dim cChildren As Integer
        Dim lParam As Integer
    End Structure

    ' TVITEM mask
    'Public const TVIF_TEXT = &H1
    Public Const TVIF_IMAGE As Integer = &H2
    Public Const TVIF_STATE As Integer = &H8
    'Public const TVIF_SELECTEDIMAGE = &H20
    Public Const TVIF_CHILDREN As Integer = &H40

    ' TVITEM state, stateMask
    Public Const TVIS_EXPANDED As Integer = &H20

    Public Structure TVHITTESTINFO ' was TV_HITTESTINFO
        Dim pt As SafeNativeMethods.POINTAPI
        Dim flags As UInteger   ''''TVHITTESTINFO_flags
        Dim hitem As Integer
    End Structure

    Public Enum TVHITTESTINFO_flags
        '  TVHT_NOWHERE = &H1   ' In the client area, but below the last item
        TVHT_ONITEMICON = &H2
        TVHT_ONITEMLABEL = &H4
        TVHT_ONITEMINDENT = &H8
        TVHT_ONITEMBUTTON = &H10
        TVHT_ONITEMRIGHT = &H20
        TVHT_ONITEMSTATEICON = &H40
        TVHT_ONITEM = (TVHITTESTINFO_flags.TVHT_ONITEMICON Or TVHITTESTINFO_flags.TVHT_ONITEMLABEL Or TVHITTESTINFO_flags.TVHT_ONITEMSTATEICON)
        ' user-defined
        TVHT_ONITEMLINE = (TVHITTESTINFO_flags.TVHT_ONITEM Or TVHITTESTINFO_flags.TVHT_ONITEMINDENT Or TVHITTESTINFO_flags.TVHT_ONITEMBUTTON Or TVHITTESTINFO_flags.TVHT_ONITEMRIGHT)
        '  TVHT_ABOVE = &H100
        '  TVHT_BELOW = &H200
        '  TVHT_TORIGHT = &H400
        '  TVHT_TOLEFT = &H800
    End Enum


    Declare Function ImageList_BeginDrag Lib "comctl32.dll" (ByVal himlTrack As IntPtr, ByVal iTrack As Integer, ByVal dxHotspot As Integer, ByVal dyHotspot As Integer) As Boolean
    Declare Function ImageList_DragEnter Lib "comctl32.dll" (ByVal hwndLock As IntPtr, ByVal x As Integer, ByVal y As Integer) As Boolean

    Declare Function ImageList_DragLeave Lib "comctl32.dll" (ByVal hwndLock As IntPtr) As Boolean

    Declare Function ImageList_Destroy Lib "comctl32.dll" (ByVal himl As IntPtr) As CBoolean

    Friend Declare Function IsWindow Lib "user32" (ByVal hwnd As IntPtr) As Integer
    Friend Declare Function UpdateWindow Lib "user32" (ByVal hwnd As IntPtr) As Integer

    Friend Declare Function PtInRect Lib "user32" (ByRef lprc As RECT, pt As Point) As Boolean


    <Flags()>
    Public Enum SendMessageTimeoutFlags
        SMTO_NORMAL = 0
        SMTO_BLOCK = 1
        SMTO_ABORTIFHUNG = 2
        SMTO_NOTIMEOUTIFNOTHUNG = 8
        SMTO_ERRORONEXIT = 32
    End Enum

    ''''Friend Declare Sub SendMessageTimeout Lib "user32" Alias "SendMessageA" (ByVal windowHandle As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr, ByVal flags As SendMessageTimeoutFlags, ByVal timeout As Integer, ByRef result As IntPtr)

    '<DllImport("user32.dll", SetLastError:=True)>
    'Friend Shared Sub SendMessageTimeout(ByVal windowHandle As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr, ByVal flags As SendMessageTimeoutFlags, ByVal timeout As Integer, ByRef result As IntPtr)

    'End Sub


    <DllImport("user32.dll", SetLastError:=True)>
    Public Shared Function SendMessageTimeout(ByVal windowHandle As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr, ByVal flags As SendMessageTimeoutFlags, ByVal timeout As Integer, ByRef result As IntPtr) As IntPtr
    End Function



#End Region

#Region "FrmGDAdquirirDocumentosAplicacion"

    'Friend Declare Function ImageList_Draw Lib "comctl32.dll" (ByVal himl As IntPtr, ByVal i As Integer, ByVal hDCDest As IntPtr, ByVal x As Integer, ByVal y As Integer, ByVal flags As Integer) As Integer

#End Region

#Region "Registre"

    'Friend Declare Function RegCreateKey Lib "advapi32.dll" Alias "RegCreateKeyA" (ByVal lngRootKey As Integer, ByVal lpSubKey As String, ByRef phkResult As Integer) As Integer
    'Friend Declare Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" (ByVal lngRootKey As Integer, ByVal lpSubKey As String) As Integer
    'Friend Declare Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal lngRootKey As Integer, ByVal lpValueName As String) As Integer
    'Friend Declare Function RegEnumKey Lib "advapi32.dll" Alias "RegEnumKeyA" (ByVal hKey As Integer, ByVal dwIndex As Integer, ByVal lpName As String, ByVal cbName As Integer) As Integer
    'Friend Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Integer, ByVal dwIndex As Integer, ByVal lpValueName As String, ByRef lpcbValueName As Integer, ByVal lpReserved As Integer, ByRef lpType As Integer, ByRef lpData As String, ByRef lpcbData As Integer) As Integer
    'Friend Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" (ByVal lngRootKey As Integer, ByVal lpSubKey As String, ByRef phkResult As Integer) As Integer
    'Friend Declare Auto Function RegOpenKeyEx Lib "advapi32.dll" (ByVal hKey As IntPtr, ByVal lpSubKey As String, ByVal ulOptions As System.Int32, ByVal samDesired As System.Int32, ByRef phkResult As System.Int32) As Integer
    'Friend Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal lngRootKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByRef lpData As Integer, ByRef lpcbData As Integer) As Integer
    'Friend Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal lngRootKey As Integer, ByVal lpValueName As String, ByVal reserved As Integer, ByVal dwType As Integer, ByRef lpData As Integer, ByVal cbData As Integer) As Integer
    'Friend Declare Function RegSetValueExS Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal lngRootKey As Integer, ByVal lpValueName As String, ByVal reserved As Integer, ByVal dwType As Integer, ByRef lpData As String, ByVal cbData As Integer) As Integer
    'Friend Declare Function RegQueryValueExS Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal lngRootKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByRef lpData As String, ByRef lpcbData As Integer) As Integer
    'Friend Declare Function RegCloseKey Lib "advapi32.dll" (ByVal lngRootKey As UIntPtr) As Integer

#End Region

End Class

