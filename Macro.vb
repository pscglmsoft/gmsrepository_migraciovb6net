﻿Imports EnvDTE
Imports Microsoft.VisualStudio.Shell

Namespace Macro
    Class Program
        Private Shared Sub Main(ByVal args As String())
            Projects()
        End Sub

        Public Shared Function GetActiveIDE() As DTE
            Dim test = (CType(ServiceProvider.GlobalProvider.GetService(GetType(EnvDTE.DTE).GUID), EnvDTE.DTE)).FullName
            Dim dte2 As DTE = TryCast(Package.GetGlobalService(GetType(DTE)), DTE)
            Return dte2
        End Function

        Public Shared Function Projects() As IList(Of Project)
            '''' Cannot convert LocalDeclarationStatementSyntax, System.InvalidCastException: Unable to cast object of type 'Microsoft.CodeAnalysis.VisualBasic.Syntax.UnaryExpressionSyntax' to type 'Microsoft.CodeAnalysis.VisualBasic.Syntax.TypeSyntax'.
            ''''    at ICSharpCode.CodeConverter.VB.CommonConversions.RemodelVariableDeclaration(VariableDeclarationSyntax declaration)
            ''''    at ICSharpCode.CodeConverter.VB.MethodBodyVisitor.VisitLocalDeclarationStatement(LocalDeclarationStatementSyntax node)
            ''''    at Microsoft.CodeAnalysis.CSharp.Syntax.LocalDeclarationStatementSyntax.Accept[TResult](CSharpSyntaxVisitor`1 visitor)
            ''''    at Microsoft.CodeAnalysis.CSharp.CSharpSyntaxVisitor`1.Visit(SyntaxNode node)
            ''''    at ICSharpCode.CodeConverter.VB.CommentConvertingMethodBodyVisitor.ConvertWithTrivia(SyntaxNode node)
            ''''    at ICSharpCode.CodeConverter.VB.CommentConvertingMethodBodyVisitor.DefaultVisit(SyntaxNode node)
            '''' 
            '''' Input: 
            ''''                 Projects projects = GetActiveIDE().Solution.Projects;

            '''' 
            Dim list As List(Of Project) = New List(Of Project)()
            Dim item = Projects.GetEnumerator()

            While item.MoveNext()
                Dim project = TryCast(item.Current, Project)

                If project Is Nothing Then
                    Continue While
                End If
            End While

            Return list
        End Function

        Private Shared Function GetSolutionFolderProjects(ByVal solutionFolder As Project) As IEnumerable(Of Project)
            Dim list As List(Of Project) = New List(Of Project)()

            For i = 1 To solutionFolder.ProjectItems.Count
                Dim subProject = solutionFolder.ProjectItems.Item(i).SubProject

                If subProject Is Nothing Then
                    Continue For
                End If
            Next

            Return list
        End Function
    End Class
End Namespace

