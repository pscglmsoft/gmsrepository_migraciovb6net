﻿Imports System.Drawing
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Windows.Forms
Imports System.Windows.Forms.ListViewItem
Imports EnvDTE
Imports System
Imports System.Resources
Imports System.Collections

Imports Microsoft.VisualStudio.Shell

Public Class Substituir

    Dim numFitxerProcessat As Integer = 0
    Dim numCoincidenciesVB6 As Integer = 0
    Dim numCoincidenciesNet As Integer = 0
    Dim numCoincidenciesVB6_2 As Integer = 0
    Dim numCoincidenciesNet_2 As Integer = 0

    Dim encoding As Encoding

    Const CARPETA_VB6 As String = "M:\MP\"
    Const CARPETA_NET As String = "M:\MP_NET\"
    Const CARPETA_VB6_BACKUP As String = "M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP\"
    Const CARPETA_TEMP As String = "C:\Tmp\"
    Const FITXER_TRACING_MIGRACIO As String = "tracing_migracio.txt"
    Const FORMAT_FITXER_TRACING_MIGRACIO As String = "{0}|{1}|{2}|{3}"

    Const REGEX_CADENA_SUBSTITUCIO As String = "Regex : substituit per "

    Const EXTENSIO_VBP As String = ".vbp"
    Const EXTENSIO_VBPROJ As String = ".vbproj"
    Const EXTENSIO_SLN As String = ".sln"
    Const EXTENSIO_VB As String = ".vb"
    Const EXTENSIO_BAS As String = ".bas"
    Const EXTENSIO_FRM As String = ".frm"
    Const EXTENSIO_FRX As String = ".frx"
    Const EXTENSIO_CLS As String = ".cls"
    Const EXTENSIO_CTL As String = ".ctl"
    Const EXTENSIO_RESX As String = ".resX"
    Const EXTENSIO_DESIGNER As String = ".Designer.vb"
    Const EXTENSIO_FRM_DUPLICATS As String = "_frm_"    '! el Wizard VS2008 quan detecta un formulari amb el mateix nom (en diferents carpetes 2007, 2011), crea <nomformulari>_frm_X.vb

    '! -------------------------------------------------------------------------------------------------------------------------------------
    '! Gestió Twips/Pixels

    Const GETPIXELSTOTWIPS As String = "GetPixelsToTwips"
    Const GETTWIPSTOPIXELS As String = "GetTwipsToPixels"
    Const TWIPSPERPIXEL As String = "TwipsPerPixel"

    Const REGEX_GTP As String = GETTWIPSTOPIXELS & "\w+"
    Const REGEX_GPT As String = GETPIXELSTOTWIPS & "\w+"
    Const REGEX_TWIPSPERPIXEL As String = TWIPSPERPIXEL

    Const REGEX_OR As String = "|"

    Const REGEX_MESMENYS As String = "[\+\-]*"
    Const REGEX_MULTDIV As String = "[\*\\\/]*"
    Const REGEX_OPERADORS As String = " [<>=]+ "
    Const REGEX_OPERADORS_SENSE_ESPAIS As String = "[<>=]+"
    Const REGEX_ESPAI As String = "\s*"   '! espai/S
    Const REGEX_DOT As String = "\."       '! un punt    
    Const REGEX_NUM As String = REGEX_MESMENYS & "\d+\.*\d*"    '! numeros i double
    Const REGEX_OPE As String = "[\+\-\*/]*"    '! operadors aritmetics
    Const REGEX_COMP As String = "[><=]+"
    Const REGEX_PARENTESIS As String = "\(\)"
    Const REGEX_OPE_2 As String = "[\-]*"    '! signe negatiu davant el numero
    Const REGEX_OPE_ESP As String = REGEX_ESPAI & REGEX_OPE & REGEX_ESPAI    '! operadors aritmetics (conté els espais dreta/esquerra)
    Const REGEX_COMP_ESP As String = REGEX_ESPAI & REGEX_COMP & REGEX_ESPAI    '! operadors aritmetics (conté els espais dreta/esquerra)

    Const REGEX_ALFA As String = "[a-zA-ZçÑ_]*"       '! noms de variables sense numeros (inclou string empty)
    Const REGEX_ALFA_2 As String = "[a-zA-ZçÑ_]+"       '! noms de variables sense numeros (NO inclou string empty)
    Const REGEX_ALFANUM As String = REGEX_ALFA & REGEX_NUM     '! exemple : TabControl1  (numeros al final!)  (inclou string empty)
    Const REGEX_ALFANUM_2 As String = REGEX_ALFA_2 & REGEX_NUM     '! exemple : TabControl1  (numeros al final!)  (NO inclou string empty)
    Const REGEX_ALFANUMS_OPES As String = REGEX_ALFANUM & REGEX_OPE_ESP & REGEX_ALFANUM
    Const REGEX_ALFANUM_DOT As String = "[\w+\." & REGEX_PARENTESIS & "]+"    '!  exemple : Tabcontrol1.selected , .selected
    Const REGEX_ALFANUMS_DOTS_OPES As String = REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM_DOT   ' xxxx.yyyy + zzzz.ddd

    Const REGEX_COLEC_SIMPLE As String = REGEX_ALFANUM_2 & "\(" & REGEX_ALFANUM & "\)"
    Const REGEX_COLEC As String = REGEX_ALFANUM_2 & "\(" & REGEX_ALFANUMS_DOTS_OPES & "\)"    '! coleccio(index) (el ALFANUM NO inclou string empty)
    Const REGEX_COLEC_SIMPLE_PROP As String = REGEX_ALFANUM_2 & "\(" & REGEX_ALFANUM & "\)" & REGEX_DOT & REGEX_ALFANUM
    Const REGEX_COLEC_PROP As String = REGEX_COLEC & REGEX_DOT & REGEX_ALFANUM    '! coleccio(index).propietat
    Const REGEX_DOT_COLEC As String = REGEX_ALFANUM & REGEX_DOT & REGEX_COLEC    '! nom.coleccio(index)
    Const REGEX_DOT_COLEC_PROP As String = REGEX_ALFANUM & REGEX_DOT & REGEX_COLEC_PROP    '! nom.coleccio(index).propietat
    Const REGEX_DOT_DOT_COLEC_PROP As String = REGEX_ALFANUM & REGEX_DOT & REGEX_ALFANUM & REGEX_DOT & REGEX_COLEC_PROP    '! nom1.nom2.coleccio(index).propietat

    Const REGEX_COLEC_DOT As String = REGEX_ALFANUM & "\(" & REGEX_ALFANUMS_DOTS_OPES & "\)"    '! coleccio(index.XX + index.YY)
    Const REGEX_COLEC_DOT_PROP As String = REGEX_COLEC_DOT & REGEX_DOT & REGEX_ALFA    '! coleccio(index.XX + index.YY).propietat

    Const REGEX_TOTS_ALFANUM_SENSEPARENTESIS As String = "[" & "a-zA-ZçÑ_\s\t\.\d\+\-\*\\""\|\,\&" & "]+"     '! lletres, numeros, parentesis, operadors aritmetics, tabulacions,...
    Const REGEX_TOTS_ALFANUM As String = "[" & "a-zA-ZçÑ_\s\t\.\d\+\-\*\\""\/|\,\&" & REGEX_PARENTESIS & "]+"     '! lletres, numeros, parentesis, operadors aritmetics, tabulacions,...

    Const REGEX_CONTE_NUMS As String = "\(" & REGEX_NUM & "\s|\s+" & REGEX_NUM & "\s+|\s+" & REGEX_NUM & "$|\s+" & REGEX_NUM & "\)|" & REGEX_GTP & "\([\-]*" & REGEX_NUM & "\)|" & REGEX_GTP & "\(" & REGEX_NUM & "\s"


    Const FORMAT_DATAHORA_24H As String = "yyyyMMddHHmm"


    ''' <summary>
    ''' TOTS ELS GRIDS SON FLAT
    ''' </summary>
    Const BORDERSTYLE_FLAT_FCGRID As String = ".BorderStyle = FlexCell.BorderStyleEnum.FixedSingle"

    Const FONT_CONTROL As String = ".Font = New System.Drawing.Font"
    Const FONT_CONTROL_FCGRID As String = ".DefaultFont = New System.Drawing.Font"

    Const ALIGNMENT_TABCONTROL As String = ".Alignment = System.Windows.Forms.TabAlignment."

    ''' <summary>
    ''' Tipus de controls
    ''' </summary>
    Const TIPUSCONTROL_CODEJOC_TABPAGE As String = "XtremeSuiteControls.TabControlPage"
    Const TIPUSCONTROL_CODEJOC_TABCONTROL As String = "XtremeSuiteControls.TabControl"
    Const TIPUSCONTROL_CODEJOC_GROUPBOX As String = "XtremeSuiteControls.GroupBox"
    Const TIPUSCONTROL_CODEJOC_PUSHBUTTON As String = "XtremeSuiteControls.PushButton"
    Const TIPUSCONTROL_CODEJOC_TABCONTROL_v2 As String = "AxXtremeSuiteControls.AxTabControl"
    Const TIPUSCONTROL_CODEJOC_CHECKBOX As String = "XtremeSuiteControls.CheckBox"
    Const TIPUSCONTROL_CODEJOC_RADIOBUTTON As String = "XtremeSuiteControls.RadioButton"
    Const TIPUSCONTROL_CODEJOC_LISTVIEW As String = "XtremeSuiteControls.ListView"
    Const TIPUSCONTROL_FCGRID As String = "FlexCell.Grid"
    Const TIPUSCONTROL_TREEVIEW As String = "MSComctlLib.TreeView"
    Const TIPUSCONTROL_TOOLSTRIPBUTTON As String = "ToolStripButton"
    Const TIPUSCONTROL_TOOLSTRIPBUTTONS As String = "ToolStripButtons"
    Const TIPUSCONTROL_TOOLSTRIP As String = "ToolStrip"   '! qualsevol tipus de ToolStrip (ToolStripButton, ToolStripSeparator, ToolStripStatusLabel,....)
    Const TIPUSCONTROL_TOOLBAR As String = "MSComctlLib.Toolbar"
    Const TIPUSCONTROL_LISTVIEW As String = "MSComctlLib.ListView"
    Const TIPUSCONTROL_RICHTEXTBOX_FONT As String = "Font"
    Const TIPUSCONTROL_SSCOMBO As String = "SSDataWidgets_B.SSDBCombo"
    Const TIPUSCONTROL_GMSTEMPS As String = "gmsTime.gmsTemps"
    Const TIPUSCONTROL_GMSIMPORTS As String = "DataControl.GmsImports"
    Const TIPUSCONTROL_GMSSETMANA As String = "DataControl.GmsSetmana"
    Const TIPUSCONTROL_GMSDATA As String = "DataControl.GmsData"
    Const TIPUSCONTROL_GMSCOMBO As String = "SSDataWidgets_B.SSDBCombo"
    Const TIPUSCONTROL_LABEL As String = "VB.Label"
    Const TIPUSCONTROL_IMAGE As String = "VB.Image"
    Const TIPUSCONTROL_COMBOBOX As String = "XtremeSuiteControls.ComboBox"


    Const FITXER_REDISSENY As String = "fitxerDatesRedisseny.txt"


    Const REGEX_RESX As String = "(?i)\.resX"
    Const REGEX_DESIGNER As String = "(?i)\.Designer\.vb"
    Const REGEX_VBP As String = "*.vbp"

    Const REGEX_DESIGNER_SUBSTITUCIO As String = ".Designer.vb"
    Const REGEX_FRM_SUBSTITUCIO As String = ".frm"
    Const REGEX_RESX_SUBSTITUCIO As String = ".resX"

    Const UNDERSCORE As String = "_"

    ''' <summary>
    ''' comptabilitza el numero de linies que te GETTWIPSTOPIXELS i/o GETPIXELSTOTWIPS 
    ''' </summary>
    Dim comptadorLiniesTwipsPixels As Integer = 0
    '! -------------------------------------------------------------------------------------------------------------------------------------

    ''' <summary>
    ''' comptabilitza el numero de linies que conté Dim A,B, C, D As Object
    ''' <para>Relacionar amb el WORKAROUND (pestanya : As Object)</para>
    ''' </summary>
    Dim comptadorAsObject As Integer = 0

    Dim seleccionarTwipsPixelsTots As Boolean = True '! si volem visualitzar tan els que fallen com els que correctes
    Dim seleccionarTwipsPixelsCorrectes As Boolean = False  '! si volem visualitzar només els correctes
    Dim seleccionarTwipsPixelsInCorrectes As Boolean = False  '! si volem visualitzar només els NO  correctes
    Dim seleccionarTwipsPixelsNomesNumeros As Boolean = False  '! si volem visualitzar només els contenen numeros + (width,left,top,...)

    ''' <summary>
    ''' 
    ''' </summary>
    Dim comptadorFontTrueType As Integer = 0


    ''' <summary>
    ''' conté la llista de tots el item de controlArray en un formulari
    ''' </summary>
    Dim lstItemControlArray As New List(Of ControlArray)

    ''' <summary>
    ''' guardem tots el controlArray i els seus items
    ''' </summary>
    Dim dicControlArray As New Dictionary(Of String, List(Of ControlArray))

    ''' <summary>
    ''' WORKAROUND : en el procés de re-ordenar l'ordre dels elements dels controlArrayS, cal saber si ja s'ha fet
    ''' </summary>
    Dim dicEsOrdenatElementsControlArray As New Dictionary(Of String, Boolean)

    ''' <summary>
    ''' WORKAROUND : per no duplicar linies WithEvents i New dels Toolbar si executem varies vegades el procés de migració
    ''' Exemple : _Toolbar1_Button1 , {Name, WithEvents, ImageKey,.....}
    ''' </summary>
    Dim dictLiniesDuplicadesToolbar As New Dictionary(Of String, List(Of String))

    ''' <summary>
    ''' llista de controls que té un FRM
    ''' </summary>
    Dim lstControlsVB6 As New List(Of ControlVB6)

    ''' <summary>
    ''' en memoria TOTES les linies del fitxer de substitucions
    ''' </summary>
    Dim lstLiniesSubstitucio As List(Of String)

    Dim caracterDobleCometa As String = New String("""")

    Dim dtVB6 As DataTable = New DataTable()
    Dim dtVB6_2 As DataTable = New DataTable()
    Dim dtNET As DataTable = New DataTable()
    Dim dtNET_2 As DataTable = New DataTable()

    ''' <summary>
    ''' si es una migració "real" o de proves
    ''' </summary>
    Dim esProva As Boolean = False

    Dim aplicaPostMigracio As Boolean = False

    Dim carpetaAProcessar As String = String.Empty

    Dim novaPosicio As Integer = -1

    ''' <summary>
    ''' si hem modificat el designer cal gravar el nou
    ''' </summary>
    Dim calGravar As Boolean = False

    ''' <summary>
    ''' Numero total d'UpgradeS. Ha de coincidir amb els del fitxer html UpgradeSupport.htm del projecte en questió
    ''' </summary>
    Dim numTotalUpgrades As Integer = 0

    ''' <summary>
    ''' Numero total d'UpgradeS segons el criteri seleccionat
    ''' </summary>
    Dim numTotalUpgradesSeleccionats As Integer = 0

    Dim numTotal_Upgrade_Issue As Integer = 0
    Dim numTotal_Upgrade_Note As Integer = 0
    Dim numTotal_Upgrade_Warning As Integer = 0
    Dim numTotal_Upgrade_Todo As Integer = 0

    ''' <summary>
    ''' ListViewItem del listview lvTwipsPixels seleccionat
    ''' </summary>
    Dim lviTwipsPixelsSelected As ListViewItem = Nothing

    ''' <summary>
    ''' ListViewItem del listview lvTwipsPixels seleccionat
    ''' </summary>
    Dim lviFontsTrueTypeSelected As ListViewItem = Nothing

    ''' <summary>
    ''' ListViewItem del listview lviAsObject seleccionat
    ''' </summary>
    Dim lviAsObjectSelected As ListViewItem = Nothing

    ''' <summary>
    ''' ListViewItem del listview lviColeccio_1_0 seleccionat
    ''' </summary>
    Dim lviColeccio_1_0Selected As ListViewItem = Nothing

    ''' <summary>
    ''' ListViewItem del listview lviFormularis_Sense_Caption seleccionat
    ''' </summary>
    Dim lviFormularis_Sense_CaptionSelected As ListViewItem = Nothing

    ''' <summary>
    ''' ListViewItem del listview lviFormsRedisseny seleccionat
    ''' </summary>
    Dim lviFormsRedissenySelected As ListViewItem = Nothing

    ''' <summary>
    ''' ListViewItem del listview lviArquitectura seleccionat
    ''' </summary>
    Dim lviArquitecturaSelected As ListViewItem = Nothing

    ''' <summary>
    ''' TreeListViewItem del TreeListView Upgrade seleccionat
    ''' </summary>
    Dim treeListViewUpgradesSelected As TreeListViewItem = Nothing

    ''' <summary>
    ''' ListViewItem del listview lvWarningsWorkAround seleccionat
    ''' </summary>
    Dim lvWarningsWorkAroundSelected As ListViewItem = Nothing

    ''' <summary>
    ''' ListViewItem del listview lvWarningsWorkAround seleccionat
    ''' </summary>
    Dim lviBugsSelected As ListViewItem = Nothing

    ''' <summary>
    ''' Flag per permetre modificar la propietat Checked sense provocar un raise de l'event
    ''' </summary>
    Dim flagCheckboxes As Boolean = False

    ''' <summary>
    ''' diccionari per guardar els duplicats de tipus d'upgrades
    ''' </summary>
    Dim tipusControl_Propietat As New Dictionary(Of String, List(Of String))

    ''' <summary>
    ''' una carpeta pot correspondre a un projecte (conté vbp) o no
    ''' </summary>
    Dim esCarpetaRootProjecte As Boolean = False

    ''' <summary>
    ''' Controls NET que té un formulari
    ''' </summary>
    Dim controlsNetByForm As New List(Of ControlNET)

    ''' <summary>
    ''' determina si hem de tornar a carregar les pestanyes
    ''' </summary>
    Dim calRecarregarUpgrades As Boolean = False
    Dim calRecarregarNomMetodeEvent_Igual_NomEventHandler As Boolean = False
    Dim calRecarregarLiniesTwipsPixels As Boolean = False
    Dim calRecarregarAround_AsObject As Boolean = False
    Dim calRecarregarFontsTrueType As Boolean = False
    Dim calRecarregarTreeViewDinsAxGroupBox As Boolean = False

    ''' <summary>
    ''' 
    ''' </summary>
    Dim liniesFitxerRedisseny As New List(Of String)

    ''' <summary>
    ''' Podem fer 2 tipus de migració : total o incremental
    ''' </summary>
    Dim flagMigracioIncremental As Boolean = False


    Dim respostaPreguntaOCX As Boolean = False
    Dim aplicaSeguents As Boolean = False

    ''' <summary>
    ''' alguns controls han passat de ser ActiveX a NET (exemple : GroupBox a GmsGroupBox)
    ''' </summary>
    Dim esborrarOCX As Boolean = True


    ''' <summary>
    ''' 
    ''' </summary>
    Dim propertyFontTrobada As Boolean = False

    ''' <summary>
    ''' migració MSComctlLib.Toolbar Toolbar1/VB6
    ''' </summary>
    Dim controlToolbarTrobat As Boolean = False
    Dim propertyButtonsToolbar As Boolean = False
    Dim propertyButtonToolbar As Boolean = False
    Dim nomToolbar As String

    ''' <summary>
    ''' guarda els controls pare
    ''' </summary>
    Dim qua As New List(Of ControlVB6)   '! per obtenir el pare de cada control

    ''' <summary>
    ''' 
    ''' </summary>
    Dim liniaTabPageTrobada As Boolean = False
    Dim liniaTabControlTrobada As Boolean = False

    Dim nomcontrolTabPage As String = String.Empty
    Dim tipusControlPare As String = String.Empty

    ''' <summary>
    ''' Flag que indica que hem trobat una linia d'inici de definició d'un control
    ''' </summary>
    Dim liniaBeginTrobada As Boolean = False

    ''' <summary>
    ''' 
    ''' </summary>
    Dim liniesFitxerNET As List(Of String) = New List(Of String)

    ''' <summary>
    ''' relacionat amb els WorkArounds del Load
    ''' </summary>
    Dim loadTrobat As Boolean = False
    Dim propietatTrobada As Boolean = False
    Dim withTrobat As Boolean = False
    Dim showTrobat As Boolean = False


    Dim indexControlTabControl As Integer = 0


    Dim trobatElementControlArray As Boolean = False
    Dim indexElementControlArray As Integer = -1

    ''' <summary>
    ''' controls NET a partir del fitxer Designer
    ''' </summary>
    Dim lstControlsNET As New List(Of ControlNET)


    ''' <summary>
    ''' per poder resoldre el problema que en VB6 podem tenir un TabControl amb +1 control x TabPage (exemple: Item(0).ControlCount=   2)
    ''' NOMES hauriem de tractar el primer control tq : Item(x).Control(y)=   "TabControlPageZZ"
    ''' </summary>
    Dim trobatTabpage As Boolean = False

    ''' <summary>
    ''' propietats que el Wizrd2008 no migra
    ''' </summary>
    Const PROPIETAT_TAG As String = "Tag"
    Const PROPIETAT_BACKSTYLE As String = "BackStyle"
    Const PROPIETAT_CAPTION As String = "Caption"
    Const PROPIETAT_BACKCOLOR As String = "BackColor"
    Const PROPIETAT_FORECOLOR As String = "ForeColor"
    Const PROPIETAT_TEXT As String = "Text"
    Const PROPIETAT_BORDERSTYLE As String = "BorderStyle"
    Const PROPIETAT_FONT As String = "Font"
    Const PROPIETAT_FONTNAME As String = "FontName"     '? GmsTemps
    Const PROPIETAT_FONTSIZE As String = "FontSize"     '? GmsTemps
    Const PROPIETAT_DEFAULTFONTNAME As String = "DefaultFontName"     '? FlexCell grid
    Const PROPIETAT_DEFAULTROWHEIGHT As String = "DefaultRowHeight"     '? FlexCell grid
    Const PROPIETAT_SELECTIONMODE As String = "SelectionMode"     '? FlexCell grid
    Const PROPIETAT_DEFAULTFONTSIZE As String = "DefaultFontSize"     '? FlexCell grid
    Const PROPIETAT_READONLY As String = "ReadOnly"     '? FlexCell grid
    Const PROPIETAT_TABINDEX As String = "TabIndex"
    Const PROPIETAT_TABSTOP As String = "TabStop"
    Const PROPIETAT_LOCATION As String = "Location"
    Const PROPIETAT_CURSOR As String = "Cursor"
    Const PROPIETAT_INDENTATION As String = "Indentation"
    Const PROPIETAT_INDENT As String = "Indent"

    Const PROPIETAT_INDEX As String = "Index"
    Const PROPIETAT_PAGE As String = "Page"
    Const PROPIETAT_PAINTMANAGER_POSITION As String = "PaintManager.Position"    '? posició de les tabs ( 2 = Bottom,...). S'ha de migrar a la propietat Alignment/NET (GmsTabControl)
    Const PROPIETAT_STYLE As String = "Style"
    Const PROPIETAT_LINESTYLE As String = "LineStyle"     '? Treeview
    Const PROPIETAT_APPEARANCE As String = "Appearance"
    Const PROPIETAT_KEY As String = "Key"
    Const PROPIETAT_VALUE As String = "Value"
    Const PROPIETAT_WIDTH_TOOLSTRIPBUTTON As String = "Object.Width"
    Const PROPIETAT_SIZE As String = "Size"
    Const PROPIETAT_WIDTH As String = "Width"
    Const PROPIETAT_HEIGHT As String = "Height"
    Const PROPIETAT_NAME As String = "Name"
    Const PROPIETAT_ITEMCOUNT As String = "ItemCount"

    Const PROPIETAT_FORMAT_TEMPS As String = "Format_Temps"     '? GmsTemps

    Const PROPIETAT_DECIMALES As String = "Decimales"     '? GmsImports
    Const PROPIETAT_MAXLENGTH As String = "MaxLength"     '? GmsImports
    Const PROPIETAT_SEPARADORMILES As String = "SeparadorMiles"     '? GmsImports
    Const PROPIETAT_OCULTARCEROS As String = "OcultarCeros"     '? GmsImports
    Const PROPIETAT_SIGNE As String = "Signe"     '? GmsImports
    Const PROPIETAT_ARRODONIMENT As String = "Arrodoniment"     '? GmsImports

    Const PROPIETAT_COLUMNHEADERS As String = "ColumnHeaders"     '? GmsCombo
    Const PROPIETAT_BACKCOLORODD As String = "BackColorOdd"     '? GmsCombo
    Const PROPIETAT_DATAFIELDLIST As String = "DataFieldList"     '? GmsCombo      Equivalent a la propietat DisplayMember/NET
    Const PROPIETAT_DISPLAYMEMBER As String = "DisplayMember"
    Const PROPIETAT_COLUMNSNAME As String = "ColumnsName"    '? ******* no afecta !!!!!

    Const PROPIETAT_ENABLED As String = "Enabled"    '? GmsGroupBox,...

    Const PROPIETAT_SHOWPLUSMINUS As String = "ShowPlusMinus"   '? Treeview
    Const PROPIETAT_SHOWLINES As String = "ShowLines"   '? Treeview
    Const PROPIETAT_SHOWROOTLINES As String = "ShowRootLines"   '? Treeview

    Const PROPIETAT_AUTOSIZE As String = "AutoSize"
    Const PROPIETAT_MULTILINE As String = "MultiLine"

    Const CONST_ME As String = "Me."
    Const CONST_TRUE As String = "True"
    Const CONST_FALSE As String = "False"
    Const CONST_EQUALS As String = "\s*=\s*"

    ''' <summary>
    ''' Regex
    ''' </summary>
    Const REGEX_CONST_ME As String = "Me\."
    Const REGEX_INICI_LINIA_BLANC As String = "(?i)^\s*"
    Const REGEX_NOMTOOLBAR As String = "(Toolbar\w+)"
    Const REGEX_SUBITEMS As String = "SubItems"
    Const REGEX_VALOR_PROPIETAT_CONTROL As String = "\s*=\s*([-+]*\d+)"

    ''' <summary>
    ''' Linies que hem afegit/substituit
    ''' </summary>
    Const LINIA_AFEGIDA_AFTERCHECK As String = "If FlagCarregantTree Then Exit Sub  '? WORKAROUND : node.Checked = True/False"
    Const LINIA_AFEGIDA_TEXTCHANGED As String = "'? En Design, cal que executi el codi (com VB6)"
    Const LINIA_AFEGIDA_RESIZE As String = "'? En Design, cal que executi el codi (com VB6)"
    Const LINIA_AFEGIDA_SELECTEDINDEXCHANGED = "'? En Design, cal que executi el codi (com VB6)"
    Const LINIA_AFEGIDA_ITEMCHECKED_FOCUSEDITEM = "If ListView1.FocusedItem Is Nothing Then Exit Sub   '! si ItemChecked"
    Const LINIA_AFEGIDA_AFTERSELECT As String = "If flag_formshowing Then Exit Sub  '? WORKAROUND : <treeview>.SelectedNode = XXXXX + OnLoad , no ha de processar el codi"
    Const LINIA_AFEGIDA_VIEWCHANGED As String = "'? En Design, cal que executi el codi (com VB6)"

    ''' <summary>
    ''' per indicar que la linia en questió ja s'ha "migrat" en algun procés anterior
    ''' </summary>
    Const LINIA_MIGRADA_COLECCIO_1_0 As String = "      '! Migrat_Coleccio"
    Const LINIA_MIGRADA_TWIPSPIXELS As String = "       '! MigratPixels"
    Const LINIA_MIGRADA_OBREFORMULARI As String = "     '! Migrat_ObreFormulari"
    Const LINIA_MIGRADA_REMOVEAT As String = "      '! Migrat_RemoveAt"


    ''' <summary>
    ''' Linies que afegim en el procés de migració/validació.l Wizard2008 (per algun motiu) no ho fa
    ''' </summary>
    Const TIPUSLINIA_TOOLBAR_ANCHOR As String = "Anchor"
    Const TIPUSLINIA_TOOLBAR_WITHEVENTS As String = "WithEvents"
    Const TIPUSLINIA_TOOLBAR_DOCK As String = "Dock"
    Const TIPUSLINIA_TOOLBAR_IMAGEKEY As String = "ImageKey"
    Const TIPUSLINIA_TOOLBAR_DISPLAYSTYLE As String = "DisplayStyle"
    Const TIPUSLINIA_TOOLBAR_ADDRANGE As String = "AddRange"
    Const TIPUSLINIA_TOOLBAR_ADD As String = "Add"
    Const TIPUSLINIA_TOOLBAR_NEW As String = "New"
    Const TIPUSLINIA_TOOLBAR_AUTOSIZE As String = "AutoSize"
    Const TIPUSLINIA_TOOLBAR_SIZE As String = "Size"
    Const TIPUSLINIA_TOOLBAR_NAME As String = "Name"
    Const TIPUSLINIA_TOOLBAR_IMAGESTREAM As String = "ImageStream"
    Const TIPUSLINIA_TOOLBAR_GRIPSTYLE As String = "GripStyle"
    Const TIPUSLINIA_TOOLBAR_BEGINPROPERTYBUTTONS As String = "BeginProperty Buttons"
    Const TIPUSLINIA_TOOLBAR_BEGINPROPERTYBUTTON As String = "BeginProperty Button"




    Const LINIA_ENDPROPERTY As String = "\s*EndProperty"
    Const LINIA_END As String = "\s*End$"



    Enum E_EstatProcesMigracio
        NoCreat = 0
        Executat_CopiaBackupVB6 = 1
        Executat_Wizard2008 = 2
        Executat_PostMigracio = 3
        Executat_Validacio = 4
        Executat_MigratsFitxersModificats = 5
        Executat_MigratsFitxers = 6
    End Enum

    Enum E_TipusControl
        TabControlPage = 0      '! XtremeSuiteControls.TabControlPage
        Grid = 1                '! FlexCell.Grid
        PushButton = 2          '! XtremeSuiteControls.PushButton
        GmsData = 3             '! DataControl.GmsData
        SSDBCombo = 4           '! SSDataWidgets_B.SSDBCombo migrat a gmsCombo
        CheckBox = 5            '! XtremeSuiteControls.CheckBox
        GroupBox = 6            '! XtremeSuiteControls.GroupBox
    End Enum

    Enum ColorUpgrade
        Sense = 0   '! no catalogat
        Resolt = 1  '! Color.LightGreen
        Warning = 2 '! Color.Orange
        Critic = 3 '! Color.OrangeRed
        Altres = 4  '! Color.PaleVioletRed
        GestioEvents = 5  '! 
    End Enum

    ''' <summary>
    ''' Criticitat d'error segons : https://www.vbmigration.com/knowledgebase.aspx
    ''' </summary>
    Enum CriticitatMigracioError
        INFO = 0
        PRB = 1
        HOWTO = 2
        BUG = 3
    End Enum

    ''' <summary>
    ''' Categoria d'error segons : https://www.vbmigration.com/knowledgebase.aspx
    ''' </summary>
    Enum CategoriaMigracioError
        GENERAL = 0
        LANGUAGE = 1
        FORMSCONTROLS = 2
        CLASSES = 3
        DATABASE = 4
        ADO = 5
    End Enum

    ''' <summary>
    ''' Tipus de "bugs"
    ''' </summary>
    Enum TipusBug
        AFTERCHECK = 0       '! '? WORKAROUND : node.Checked = True/False
        TEXTCHANGED = 1    '! 'UPGRADE_WARNING: El evento txtXXXXXX.TextChanged se puede desencadenar cuando se inicializa el formulario
        AXFORMEXTENDER = 2
        RESIZE = 3     '! 'UPGRADE_WARNING: El evento frmXXXXX.Resize se puede desencadenar cuando se inicializa el formulario
        SELECTEDINDEXCHANGED = 4      '! 'UPGRADE_WARNING: El evento cmbXXXXX.SelectedIndexChanged se puede desencadenar cuando se inicializa el formulario
        CLIPBOARD_SETTEXT = 5
        ITEMCHECKED = 6   '? WORKAROUND : https://stackoverflow.com/questions/7506588/why-does-listview-fire-event-indicating-that-checked-item-is-unchecked-after-add/7507293
        AFTERSELECT = 7   '? WORKAROUND : <treeview>.SelectedNode = XXXXXX  +  Load
        VIEWCHANGED = 8    '! 'UPGRADE_WARNING: El evento calAgenda_ViewChanged se puede desencadenar cuando se inicializa el formulario
        MANUAL = 9  '? SON AQUELLS BUGS QUE HEM DETECTAT I CAL MODIFICAR MANUALMENT
    End Enum

    Enum TipusErrorVB6
        CONTROL_PENJA_TABCONTROL = 0   '? un control que no es un TabPage i "penja" directament de TabControl.En .Net no és possible
        CONTROL_NO_ELEMENT_CONTROLARRAY = 1   '? un control ens marca com element d'un controlArray i realment no ho és
        CONTROL_NO_PROCESSAT = 2
    End Enum

    ''' <summary>
    ''' Guarda o bé un ControlArray o bé un "item" d'un ControlArray
    ''' El ControlArray conté una llista d'items
    ''' </summary>
    Structure ControlArray
        Dim tipusControl As String
        Dim nomControl As String
        Dim tipusControlArray As String
        ''''Dim lstItemsControlArray As List(Of Object)
    End Structure

    ''' <summary>
    ''' 
    ''' </summary>
    Public Class ControlVB6
        Public nomControl As String
        Public tipusControl As String
        Public tag As String
        Public caption As String    '! en el cas de controls de tipus TabcontrolPage, GmsGroupBox (on el Wizard2008 NO es capaç de migrar la propietat Caption a la propietat Text)
        Public nomControlPare As String
        Public tipusControlPare As String
        Public borderStyleGrid As String   '? WORKAROUND: Flat FlexCell Grid
        Public borderStyleGroupBox As String   '? WORKAROUND: NoBorder/... d'un GroupBox (Codejoc, GmsGroupBox,....)
        Public Indentation As String   '? WORKAROUND: Indentation en Pixels
        Public BackColor As String     '? WORKAROUND: BackColor (GmsGroupBox, elements de controlArray,...)
        Public BackStyle As String    '? Label
        Public BorderStyle As String
        Public ForeColor As String     '? WORKAROUND: ForeColor (GmsGroupBox, elements de controlArray,...)
        Public Enabled As String     '? WORKAROUND: Enabled (GmsGroupBox, elements de controlArray,...)
        Public Index As String  '? He trobat un bug que em ve de VB6. Hi ha formularis que tenen controls que teòricament em venen com elements de controlArray i realment no ho son.Exemple : els frmLinPressupostXXXX tenen un control txtImportBrutuf que segurament es va construir a partir d'un copiar/pegar.
        Public Alignment As String
        Public Appearance As String = "System.Windows.Forms.BorderStyle.Fixed3D"   '? exemple : appearance/TreeView passa a ser BorderStyle en NET.Per defecte : Fixed3D
        Public FormatTemps As String   '? GmsTemps
        Public Decimales As String   '? GmsImports
        Public MaxLength As String   '? GmsImports
        Public SeparadorMiles As String   '? GmsImports
        Public OcultarCeros As String   '? GmsImports
        Public Signe As String   '? GmsImports
        Public Arrodoniment As String   '? GmsImports
        Public ColumnHeaders As String   '? GmsCombo
        Public ColumnsName As String    '? GmsCombo
        Public BackColorOdd As String   '? GmsCombo
        Public DataFieldList As String   '? GmsCombo
        Public Height As String   '? Label, checkBox
        Public AutoSize As String   '? Label, checkBox

        '! Property Font
        Public FontName As String
        Public FontSize As String
        Public FontCharSet As String
        Public FontWeight As String
        Public FontUnderline As String
        Public FontItalic As String
        Public FontStrikethrough As String

        '! Properties dels Buttons de ToolBars
        Public ButtonToolbarStyle As String
        Public ButtonToolbarKey As String
        Public ButtonToolbarObjectWidth As String
        Public ButtonToolbarValue As String  '! s'aplica només en el cas ButtonToolbarStyle = 2 (butons "excluyents")

        '! Properties dels Treeview
        Public TreeViewStyle As String
        Public TreeViewLineStyle As String

        '! Properties dels FCGrid
        Public FCGridDefaultFontName As String
        Public FCGridDefaultFontSize As String
        Public FCGridDefaultRowHeight As String
        Public SelectionMode As String

        '! per saber si un control és un element d'un controlArray.El nom del control és el mateix
        Public EsElementControlArray As Boolean = False

        '! Properties TabControlS
        Public ItemCount As String

        '! Properties TabControlPage
        Public Page As String

        '! Properties del tamany.En casos com els AxRadioButton tot i que té un OCXState TAMBÉ cal assignar els valors de Width i Height que venen del fitxer frm/VB6
        'x Public Height As String       dijous 28/10
        'x Public Width As String       dijous 28/10

        Public Sub New()
        End Sub

    End Class

    ''' <summary>
    ''' 
    ''' </summary>
    Structure ControlNET
        Dim nomControl As String
        Dim tipusControl As String
    End Structure

    Structure ComponentVB6
        Dim identificador As String
        Dim nomComponent As String
        Dim versio As String
    End Structure

    Structure ReglaRegex
        Dim patroCerca As String
        Dim patroSubstitucio As String
        Dim numRegla As Integer

        Public Sub New(_patroCerca As String, _patroSubstitucio As String, _numRegla As Integer)
            Me.New()
            Me.patroCerca = _patroCerca
            Me.patroSubstitucio = _patroSubstitucio
            Me.numRegla = _numRegla
        End Sub
    End Structure

    ''' <summary>
    ''' Test Unitaris sobre la conversio Twips-Pixels
    ''' </summary>
    Structure TestConversioTwipsPixels
        Dim cadena As String
        Dim cadenaEsperada As String

        Public Sub New(_cadena As String, _cadenaEsperada As String)
            Me.New()
            Me.cadena = _cadena
            Me.cadenaEsperada = _cadenaEsperada
        End Sub
    End Structure

    ''' <summary>
    ''' Pestanya Twips/Pixels.
    ''' Només afegim aquelles linies que "matches" diferents regles Regex
    ''' </summary>
    Dim lstPatronsRegexTwipsPixelsProcessats As New List(Of String)

    ''' <summary>
    ''' carrega el màxim nombre de regles Regex que pot fer "matchs" amb linies que contenen GetTwipsToPixels i/o GetPixelsToTwips
    ''' </summary>
    Dim lstReglesRegex As New List(Of ReglaRegex)

    ''' <summary>
    ''' diccionari per guardar els diferents "fitxerDatesRedisseny" del projecte seleccionat
    ''' </summary>
    Dim dictFitxersDatesRedisseny As New Dictionary(Of String, List(Of String))


    Public Property VB6() As Boolean
        Get
            Return chkInclouConfiguracio.Enabled
        End Get
        Set(ByVal value As Boolean)
            chkInclouConfiguracio.Enabled = value
            chkInclouProyecto1.Enabled = value

            lblEtiqueta.Text = String.Empty
            lblProcessant.Text = String.Empty
            lblTrobats_1.Text = String.Empty
            lblTrobats_2.Text = String.Empty
        End Set
    End Property

    ''' <summary>
    ''' Llegeix del fitxer vbp (VB6), els forms, moduls, classes, userControls que conté una determinada aplicacio (ex.:RRPP)
    ''' </summary>
    Private Sub GetFormsVB6_Aplicacio(ByVal fitxerVBP As String, ByVal ambExtensio As Boolean)
        If Not File.Exists(fitxerVBP) Then Exit Sub

        Cursor = Cursors.WaitCursor

        Dim liniesFitxerVBP As List(Of String) = LlegeixFitxer(fitxerVBP, encoding)

        For Each linia As String In liniesFitxerVBP

            Dim splitList() As String = {}

            If RegexCercaCadena(linia, "(?i)Form=(.+)\.frm", splitList) Then  '! Formularis
                If ambExtensio Then
                    AfegirRowDatatable(dtVB6, splitList(1).Trim & EXTENSIO_FRM)
                Else
                    AfegirRowDatatable(dtVB6, splitList(1).Trim)
                End If

            ElseIf RegexCercaCadena(linia, "(?i)Module=\w+;(.+)\.", splitList) Then  '! classes
                If ambExtensio Then
                    AfegirRowDatatable(dtVB6_2, splitList(1).Trim & EXTENSIO_BAS)
                Else
                    AfegirRowDatatable(dtVB6_2, splitList(1).Trim)
                End If

            ElseIf RegexCercaCadena(linia, "(?i)Class=\w+;(.+)\.cls", splitList) Then  '! classes
                If ambExtensio Then
                    AfegirRowDatatable(dtVB6_2, splitList(1).Trim & EXTENSIO_CLS)
                Else
                    AfegirRowDatatable(dtVB6_2, splitList(1).Trim)
                End If

            ElseIf RegexCercaCadena(linia, "(?i)UserControl=(.+)\.ctl", splitList) Then  '! classes                
                If ambExtensio Then
                    AfegirRowDatatable(dtVB6_2, splitList(1).Trim & EXTENSIO_CTL)
                Else
                    AfegirRowDatatable(dtVB6_2, splitList(1).Trim)
                End If

            Else
                Dim kk = 1
            End If
        Next

        lblTrobats_1.Text = numCoincidenciesVB6.ToString
        lblTrobatModulsVB6.Text = numCoincidenciesVB6_2.ToString

        lblProcessant.Text = String.Format("Processant VB6 dins el projecte {0}  ....", fitxerVBP)
        lstItemsFoundVB6.Columns.Item(0).Text = String.Format("Formularis VB6 dins el projecte {0}", fitxerVBP)
        lstItemsFoundVB6_2.Columns.Item(0).Text = String.Format("Classes VB6 dins el projecte {0}", fitxerVBP)

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Llegeix del fitxer vbproj (VBNet), els forms/classes que conté una determinada aplicació (ex.:RRPP)
    ''' </summary>
    Private Sub GetFormsVBNet_Aplicacio(ByVal fitxerVBNet As String, ByVal ambExtensio As Boolean, ByVal incloureDesigners As Boolean)
        If Not File.Exists(fitxerVBNet) Then Exit Sub

        Cursor = Cursors.WaitCursor

        Try
            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerVBNet, encoding)

            Dim possibleForm As String = String.Empty

            Dim esForm As Boolean = False

            For Each linia As String In liniesFitxerVBNet

                '! descartem els seguents
                If linia.ToUpper.Contains("ASSEMBLYINFO.") Then Continue For

                Dim splitList() As String = {}

                '! Formularis
                If RegexCercaCadena(linia, "<Compile Include=""(\\)*(.+)\.(.+)""\s*/>", splitList) Then  '! Classe "acabats" en />

                    If Not EsNouForm(splitList(1)) Then
                        AfegirRowDatatable(dtNET_2, splitList(1) & "." & splitList(2))
                    End If
                End If

                '! Formularis
                If RegexCercaCadena(linia, "<Compile Include=""(\\)*(.+)\.(.+)"">", splitList) Then  '! Formularis

                    '! excloim els fitxers "designer"
                    If Not linia.ToUpper.Contains(".DESIGNER.") Then
                        If ambExtensio Then
                            possibleForm = splitList(1) & "." & splitList(2)
                        Else
                            possibleForm = splitList(1)
                        End If
                    Else
                        esForm = True
                    End If
                End If

                If (linia.Contains("SubType>Form</SubType>") AndAlso (Not chkInclouFormsNous.Checked AndAlso Not EsNouForm(possibleForm))) OrElse
                    (linia.Contains("SubType>Form</SubType>") AndAlso (chkInclouFormsNous.Checked AndAlso EsNouForm(possibleForm))) OrElse
                    (linia.Contains("SubType>Form</SubType>") AndAlso Not EsNouForm(possibleForm)) Then  '! comprovem realment si es un formulari o una classe

                    If chkMigrarCodi.Checked Then
                        AfegirRowDatatable(dtNET, possibleForm)
                    End If

                    If chkMigrarDesigner.Checked AndAlso incloureDesigners Then
                        AfegirRowDatatable(dtNET, possibleForm.Replace(EXTENSIO_VB, EXTENSIO_DESIGNER))
                    End If

                    esForm = True
                End If

                If linia.Contains("</Compile>") Then
                    If Not esForm AndAlso Not (Not chkInclouFormsNous.Checked AndAlso EsNouForm(possibleForm)) Then  '! NO FORM
                        AfegirRowDatatable(dtNET_2, possibleForm.Replace("&amp;", "&"))
                    End If

                    esForm = False
                End If

            Next

            RefrescarProces("Diferencies projectes", String.Format("Processant VBNet dins el projecte {0} ....", fitxerVBNet), "", "")

            lstItemsFoundNet.Columns.Item(0).Text = String.Format("Formularis VBNet dins el projecte {0}", fitxerVBNet)
            lstItemsFoundNet_2.Columns.Item(0).Text = String.Format("Classes VBNet dins el projecte {0}", fitxerVBNet)

        Catch ex As Exception
            Dim A = 1
        End Try

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Formatejar el ListView de Warnings
    ''' </summary>
    ''' <param name="captionColumnes"></param>
    ''' <param name="tamanyColumnes"></param>
    Private Sub FormatListViewWarnings(ByVal lvWarnings As ListView, ByVal captionColumnes As List(Of String), ByVal tamanyColumnes As List(Of Integer))
        lvWarnings.Columns.Clear()

        '! Set the view to show details.
        lvWarnings.View = View.Details
        '! Sort the items in the list in ascending order.
        lvWarnings.Sorting = SortOrder.Ascending

        '! Create columns for the items And subitems.
        '! Width of -2 indicates auto-size.

        For Each c In captionColumnes
            Dim cc As New ColumnHeader()
            cc.Text = c
            cc.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)

            lvWarnings.Columns.Add(cc)    ', -2, HorizontalAlignment.Left)
        Next

        For Each t In tamanyColumnes
            lvWarnings.Columns.Item(tamanyColumnes.IndexOf(t)).Width = t
        Next
    End Sub

    Private Sub FormatListViewErrorsMigracio(ByVal lvErrorsMigracio As ListView, ByVal captionColumnes As List(Of String), ByVal tamanyColumnes As List(Of Integer))
        lvErrorsMigracio.Columns.Clear()

        '! Set the view to show details.
        lvErrorsMigracio.View = View.Details
        '! Sort the items in the list in ascending order.
        lvErrorsMigracio.Sorting = SortOrder.Ascending

        '! Create columns for the items And subitems.
        '! Width of -2 indicates auto-size.

        For Each c In captionColumnes
            Dim cc As New ColumnHeader()
            cc.Text = c
            cc.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)

            lvErrorsMigracio.Columns.Add(cc)    ', -2, HorizontalAlignment.Left)
        Next

        For Each t In tamanyColumnes
            lvErrorsMigracio.Columns.Item(tamanyColumnes.IndexOf(t)).Width = t
        Next
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="lvwTemp"></param>
    ''' <returns></returns>
    Private Function CopiarListView(ByVal lvwTemp) As ListView
        Dim lvwObjectsFound As New ListView()

        For Each itm As ListViewItem In lvwTemp.Items
            lvwObjectsFound.Items.Add(itm.Clone)
        Next

        Return lvwObjectsFound
    End Function

    ''' <summary>
    ''' Busca un item un texte en una ListView
    ''' </summary>
    ''' <param name="lv"></param>
    ''' <param name="searchstring"></param>
    ''' <returns></returns>
    Private Function ExisteixItemInListView(ByVal lv As ListView, ByVal searchstring As String) As Boolean
        For Each lvi As ListViewItem In lv.Items
            For Each lvisub As ListViewItem.ListViewSubItem In lvi.SubItems
                If lvisub.Text.ToUpper = searchstring.ToUpper Then
                    Return True
                    Exit For
                End If
            Next
        Next
        Return False
    End Function

    ''' <summary>
    ''' "Calcula" les diferencies entre els listViews VB6 i NET
    ''' </summary>
    Private Sub DiferenciesItemsVB6_Net()
        '! Forms VB6 versus forms VB.Net
        For Each lviVB6 As ListViewItem In lstItemsFoundVB6.Items

            If Not ExisteixItemInListView(lstItemsFoundNet, lviVB6.Text) Then
                lviVB6.BackColor = Color.Red  '! no existeix el corresponent fitxer en .Net
            ElseIf Not lviVB6.Text.Contains("..") AndAlso
                (dtDesde.Value < File.GetLastWriteTime(cboDirectoryVB6.Text & lviVB6.Text & EXTENSIO_FRM) _
                OrElse dtDesde.Value < File.GetLastWriteTime(cboDirectoryVB6.Text & lviVB6.Text & EXTENSIO_FRX)) Then

                lviVB6.BackColor = Color.Orange  '! s'ha modificat el fitxer des de la ultima migracio
            Else
                If chkDiferenciesEsquerra.Checked Then
                    '? lstItemsFoundVB6.Items.Remove(lviVB6)
                End If
            End If
        Next

        For Each lviNet In lstItemsFoundNet.Items

            If Not ExisteixItemInListView(lstItemsFoundVB6, lviNet.text) Then
                lviNet.BackColor = Color.Red  '! no existeix el corresponent fitxer en VB6
            Else
                If chkDiferenciesDreta.Checked Then
                    '? lstItemsFoundNet.Items.Remove(lviNet)            
                End If
            End If
        Next

        '! Moduls VB6 versus Classes VB.Net
        For Each lviVB6 In lstItemsFoundVB6_2.Items
            If Not ExisteixItemInListView(lstItemsFoundNet_2, lviVB6.text) Then
                lviVB6.BackColor = Color.Red  '! no existeix el corresponent fitxer en .Net
            ElseIf Not lviVB6.Text.Contains("..") AndAlso dtDesde.Value < File.GetLastWriteTime(cboDirectoryVB6.Text & lviVB6.text & EXTENSIO_BAS) Then
                lviVB6.BackColor = Color.Orange  '! s'ha modificat el fitxer des de la ultima migracio
            Else
                If chkDiferenciesEsquerra.Checked Then
                    '? lstItemsFoundVB6_2.Items.Remove(lviVB6)                   
                End If
            End If
        Next

        For Each lviNet In lstItemsFoundNet_2.Items

            If Not ExisteixItemInListView(lstItemsFoundVB6_2, lviNet.text) Then
                lviNet.BackColor = Color.Red  '! no existeix el corresponent fitxer en VB6
            Else
                If chkDiferenciesDreta.Checked Then
                    '? lstItemsFoundNet_2.Items.Remove(lviNet)                   
                End If
            End If
        Next

        lstItemsFoundVB6.Refresh()
        lstItemsFoundNet.Refresh()

        lblTrobats_1.Text = numCoincidenciesVB6.ToString
        lblTrobats_2.Text = numCoincidenciesNet.ToString
        lblTrobatModulsVB6.Text = numCoincidenciesVB6_2.ToString
        lblTrobatClassesNet.Text = numCoincidenciesNet_2.ToString
    End Sub

    ''' <summary>
    ''' Afegim una "row" a un "datatable"
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <param name="texte"></param>
    Private Sub AfegirRowDatatable(dt As DataTable, texte As String, Optional data As Date = Nothing)
        Dim dtRow As DataRow = dt.NewRow()
        dtRow("fitxer") = texte
        If data <> Nothing Then
            dtRow("data") = data.ToShortDateString
        End If
        dt.Rows.Add(dtRow)

        Debug.WriteLine(String.Format("Fitxer {0} modificat", dtRow.Item(0).ToString))
    End Sub

    ''' <summary>
    ''' Obté els fitxers que segeueixen un patró des d'una carpeta, amb o sense recursivitat
    ''' </summary>
    ''' <param name="sDir"></param>
    Private Sub GetFitxersVBP_NET(ByVal sDir As String, ByVal patro As String, ByVal recursiu As Boolean, ByVal opcional As Boolean)
        Try
            '! Fitxers dins el directori
            For Each f As String In Directory.GetFiles(sDir, patro)

                If Not EstaEnLlistaNegra(f, opcional) AndAlso EsMigrableFitxer(f) Then

                    '! Incloim els projectes de "Configuracio" i/o de "test" ?
                    If (f.Contains("Configuración") OrElse f.Contains("Configuracio")) AndAlso Not chkInclouConfiguracio.Checked Then
                    ElseIf f.Contains("Proyecto1") AndAlso Not chkInclouProyecto1.Checked Then
                    Else
                        If sDir.Contains(CARPETA_VB6) Then   '! VB6

                            If EsProjecte(f) AndAlso patro = REGEX_VBP AndAlso Not EsProjecteObsolet(f) Then
                                AfegirRowDatatable(dtVB6, f)
                            Else
                                If Not f.EndsWith(EXTENSIO_VBP) Then
                                    Dim K = cboDirectoryNet.Text & "\"

                                    If f.Contains(EXTENSIO_BAS) Then
                                        AfegirRowDatatable(dtVB6, f.Replace(K.Replace(CARPETA_NET, CARPETA_VB6), "").Replace(EXTENSIO_BAS, ""))
                                    ElseIf f.ToUpper.Contains(".FRM") Then
                                        AfegirRowDatatable(dtVB6, f.Replace(K.Replace(CARPETA_NET, CARPETA_VB6), "").Replace(EXTENSIO_FRM, "").Replace(".FRM", ""))
                                    Else
                                        AfegirRowDatatable(dtVB6, f.Replace(K.Replace(CARPETA_NET, CARPETA_VB6), "").Replace(EXTENSIO_CLS, "").Replace(EXTENSIO_CTL, ""))
                                    End If
                                End If

                            End If

                        Else  '! NET

                            If EsProjecte(f) AndAlso patro = "*.vbproj" Then
                                AfegirRowDatatable(dtNET, f)
                            Else

                                If EsProjecte(f) AndAlso Not EsProjecteObsolet(f) Then   '! "eliminem" els projectes que realment no son projectes i els "obsolets"

                                    If Not chkInclouFormsNous.Checked AndAlso EsNouForm(f) Then
                                    Else

                                        If Not f.EndsWith(EXTENSIO_VBP) AndAlso Not EsFitxerDesigner(f) Then
                                            Dim K = cboDirectoryNet.Text & "\"

                                            AfegirRowDatatable(dtNET, f.Replace(K, "").Replace(EXTENSIO_VB, ""))
                                        End If

                                    End If
                                End If
                            End If
                        End If
                    End If

                End If

            Next

            If recursiu Then
                '! Subdirectoris
                For Each d As String In Directory.GetDirectories(sDir)
                    If Not EstaEnLlistaNegra(d, opcional) Then
                        GetFitxersVBP_NET(d, patro, recursiu, opcional)
                    End If

                Next
            End If

        Catch excpt As System.Exception
            Debug.WriteLine(excpt.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Retorna la carpeta dels fitxers Net dins la carpeta BACKUP del projecte seleccionat
    ''' La carpeta de Backup acaba amb ".NET"
    ''' </summary>
    ''' <returns></returns>
    Private Function CarpetaDotNET() As String
        If Not Directory.Exists(cboDirectoryVB6_BACKUP.Text) Then Return String.Empty

        Dim carpetes() As String

        '! dependent de si estem migrant només els fitxers modificats o tots el fitxers del projecte, la CarpetaDotNet sera diferent
        If chkMigrarNomesModificats.Checked Then
            carpetes = Directory.GetDirectories(cboDirectoryVB6_BACKUP.Text, "*.NET1")
        Else
            carpetes = Directory.GetDirectories(cboDirectoryVB6_BACKUP.Text, "*.NET")
        End If

        If carpetes.Count = 0 Then
            Return String.Empty
        End If

        Return carpetes(0)
    End Function

    ''' <summary>
    ''' El “wizard” de VS2008 ens ha generat a partir d’un projecte VB6 (amb vbp) un projecte .NET (amb un vbproj)
    ''' Aquest “wizard” no migra correctament del tot les “referencies” de fitxers (ex Main.bas, FrmComarca.frm,...) en .NET
    ''' </summary>
    Function PostMigracio() As Boolean
        Try
            Cursor = Cursors.WaitCursor

            '! calculem els fitxer de projecte VB6 i .Net dins la carpeta de Backup
            Dim fitxerVB6Proj As String = BuscarFitxersProjectesVB6(CarpetaDotNET())
            Dim fitxerVBNetProj As String = BuscarFitxersProjectesNET(CarpetaDotNET())

            '! calculem el "fitxerRedisseny" del projecte que estem migrant
            If Not flagMigracioIncremental Then
                liniesFitxerRedisseny = New List(Of String)
            Else
                If Not File.Exists(cboDirectoryNet.Text & FITXER_REDISSENY) Then
                    MsgBox(String.Format("El fitxer de dates redisseny no existeix. Desmarca el check incremental", CarpetaDotNET()), MsgBoxStyle.Information, "PostMigració")
                    Return True
                End If

                liniesFitxerRedisseny = LlegeixFitxer(cboDirectoryNet.Text & FITXER_REDISSENY, encoding)
            End If

            If Not BuscarItemsVB6_NET(True, False, True, CarpetaDotNET()) Then
                MsgBox(String.Format("Per fer la PostMigracio es necessari tenir tant el fitxer VBP com VBPROJ a la carpeta {0}" & vbCrLf & vbCrLf & "Els projectes que no tenen un fitxer de projecte VB6, s'ha d'executar abans el projecte Foto/VB6", CarpetaDotNET()), MsgBoxStyle.Information, "PostMigració")

                Cursor = Cursors.Default

                Return False
            End If

            lblEtiqueta.Text = "Postmigració :  "
            lblProcessant.Text = String.Format("Processant fitxer de projecte .Net : {0} ....", fitxerVBNetProj)

            '! Fitxer creat generat pel "wizard" VS2008 i que s'ha de "corregir"
            Dim liniesVBProj As List(Of String) = LlegeixFitxer(fitxerVBNetProj, encoding)

            Dim linesNew As New List(Of String)

            '! hem de buscar dins el fitxer vbproj (.NET), les linies que continguin : "Compile Include="  , "EmbeddedResource Include="
            For i As Integer = 0 To liniesVBProj.Count - 1

                Try
                    Dim novalinia As String = String.Empty
                    Dim splitList() As String = {}

                    Dim nomModul As String = BuscarNomModul(liniesVBProj(i))

                    If nomModul IsNot Nothing Then

                        Dim s() As String = liniesVBProj(i).Split("""")

                        If RegexCercaCadena(liniesVBProj(i), "<EmbeddedResource Include=\""", splitList) Then   '! Resources
                            Actualitza(liniesVBProj(i), nomModul, s, novalinia, fitxerVB6Proj, "(?i)Form=(.+\\)*?" & nomModul & EXTENSIO_FRM, "EmbeddedResource")

                        ElseIf RegexCercaCadena(liniesVBProj(i), "<Compile Include=\""(.+\\)*frm", splitList) OrElse liniesVBProj(i).Contains("_frm.") OrElse liniesVBProj(i).ToUpper.Contains(".DESIGNER") Then  '! Formularis
                            Actualitza(liniesVBProj(i), nomModul, s, novalinia, fitxerVB6Proj, "(?i)Form=(.+\\)*?" & nomModul & EXTENSIO_FRM, "Compile")

                        ElseIf RegexCercaCadena(liniesVBProj(i), "<Compile Include=\""", splitList) OrElse liniesVBProj(i).Contains("_bas.") Then   '! Modules
                            '! tenim 2 casos :
                            '!       - Modul
                            '!       - Classe
                            '! Els hem de provar els 2. Exemple : ExecutaProces.vb es una Class, NO es un Module
                            Actualitza(liniesVBProj(i), nomModul, s, novalinia, fitxerVB6Proj, "(?i)Module=" & "(\w*);\s(.+\\)*" & nomModul & EXTENSIO_BAS, "Compile")

                            Actualitza(liniesVBProj(i), nomModul, s, novalinia, fitxerVB6Proj, "(?i)Class=" & "(\w*);\s(.+\\)*" & nomModul & EXTENSIO_CLS, "Compile")
                        End If


                        If String.IsNullOrEmpty(novalinia) Then
                            linesNew.Add(liniesVBProj(i))   '! deixem la mateixa linia
                        Else
                            linesNew.Add(novalinia)  '! la linia ha cambiat
                        End If

                    Else

                        If RegexCercaCadena(liniesVBProj(i), "<DependentUpon>\w+?_frm(_\d)?\.vb</DependentUpon>", splitList) OrElse RegexCercaCadena(liniesVBProj(i), "<DependentUpon>\w+?_bas(_\d)?\.vb</DependentUpon>", splitList) Then   '! ALTRES
                            novalinia = RegexSubstitucioCadena(liniesVBProj(i), "(<DependentUpon>\w+?)(_frm(_\d)?)(\.vb)(</DependentUpon>)", "$1$4$5")

                        Else
                            linesNew.Add(liniesVBProj(i))   '! deixem la mateixa linia
                        End If

                    End If

                Catch ex As Exception
                    Dim K = 3
                End Try
            Next

            '! generem el nou vbproj amb les correccions sobre referencies a fitxers
            Dim nomProjecteAmbCami As String = BuscarNomProjecteAmbCami()
            File.WriteAllLines(nomProjecteAmbCami & "_PostMigracio.vbproj", linesNew.ToArray, encoding)

            '! Renombrem el fitxer de projecte NET
            File.Delete(nomProjecteAmbCami & EXTENSIO_VBPROJ)
            File.Copy(nomProjecteAmbCami & "_PostMigracio.vbproj", Directory.GetFiles(CarpetaDotNET(), "*.vbproj")(0), True)

            '! guardem el fitxer de "redisseny"
            File.WriteAllLines(cboDirectoryNet.Text & FITXER_REDISSENY, liniesFitxerRedisseny.ToArray, encoding)


            '! gravem l'estat del procés de migració
            RegistrarTrazaEstatMigracio(String.Format(FORMAT_FITXER_TRACING_MIGRACIO, Date.Now.ToString("dd/MM/yyyy HH:mm:ss"), E_EstatProcesMigracio.Executat_PostMigracio.ToString, "OK", "Executat postMigracio"))

        Catch ex As Exception
            Cursor = Cursors.Default
        End Try

        Cursor = Cursors.Default

        lblEtiqueta.Text = "PostMigració : "

        RefrescarProces("Migració", "Postmigració finalitzat", "", "")

        Return True
    End Function

    ''' <summary>
    ''' Construeix un fitxer de projecte VB6 (vbp) amb NOMES els fitxers especificats a partir d'un altra fitxer vbp
    ''' (inclou també els fitxers que son una referencia a altres projectes!!)
    ''' </summary>
    ''' <param name="fitxerVB"></param>
    Private Sub ConstruirFitxerVBP_amb_llistafitxers_modificats_desde(ByVal fitxerVB As String)
        Try
            '! fitxer temporal
            Dim fitxerVBTemp As String = fitxerVB & "tmp"
            File.Copy(fitxerVB, fitxerVBTemp)

            Dim liniesVB As List(Of String) = LlegeixFitxer(fitxerVBTemp, encoding)

            Dim linesNew As New List(Of String)

            For i As Integer = 0 To liniesVB.Count - 1

                Dim novalinia As String = String.Empty
                Dim splitList() As String = {}

                If RegexCercaCadena(liniesVB(i), "Form=(.+)\.frm", splitList) Then  '! Formularis
                    If Not splitList(1).Contains("..") AndAlso Not EsFitxerModificatDesde(splitList(1), EXTENSIO_FRM) Then   '! si no es una referencia i no es un dels fitxers a migrar no l'hem d'afegir
                        Continue For
                    End If
                ElseIf RegexCercaCadena(liniesVB(i), "Module=\w+;(.+)\.", splitList) Then  '! classes
                    If Not splitList(1).Contains("..") AndAlso Not EsFitxerModificatDesde(splitList(1), EXTENSIO_BAS) Then   '! si no es una referencia i no es un dels fitxers a migrar no l'hem d'afegir
                        Continue For
                    End If
                ElseIf RegexCercaCadena(liniesVB(i), "Class=\w+;(.+)\.cls", splitList) Then  '! classes
                    If Not splitList(1).Contains("..") AndAlso Not EsFitxerModificatDesde(splitList(1), EXTENSIO_CLS) Then   '! si no es una referencia i no es un dels fitxers a migrar no l'hem d'afegir
                        Continue For
                    End If
                ElseIf RegexCercaCadena(liniesVB(i), "UserControl=(.+)\.ctl", splitList) Then  '! classes                
                    If Not splitList(1).Contains("..") AndAlso Not EsFitxerModificatDesde(splitList(1), EXTENSIO_CTL) Then   '!! si no es una referencia i no es un dels fitxers a migrar no l'hem d'afegir
                        Continue For
                    End If
                End If

                '! deixem la mateixa linia
                linesNew.Add(liniesVB(i))
            Next

            '! guardem el nou fitxer de projecte VB6 amb NOMES els "fitxers modificats des de"
            File.WriteAllLines(fitxerVB, linesNew.ToArray, encoding)

            '! esborrem el fitxer temporal
            File.Delete(fitxerVBTemp)

        Catch ex As Exception
            Dim K = 3
        End Try

    End Sub

    ''' <summary>
    ''' Retorna si el fitxer s'ha d'incloure en el fitxer vbp del Backup
    ''' </summary>
    ''' <returns></returns>
    Private Function EsFitxerModificatDesde(ByVal fitxer As String, ByVal extensio As String) As Boolean
        For Each fitxerLv As ListViewItem In lstItemsFoundVB6.Items

            If fitxerLv.Text.Contains(fitxer & extensio) Then Return True
        Next

        Return False
    End Function

    ''' <summary>
    ''' Per saber si un projecte és obsolet a partir del nom del projecte
    ''' </summary>
    ''' <param name="nomProjecte"></param>
    ''' <returns></returns>
    Private Function EsNodeTreeProjecteVB6Obsolet(ByVal nomProjecte As String) As Boolean
        If nomProjecte = "Gestio CRM" Then
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' Projectes que no s'han de migrar
    ''' </summary>
    ''' <param name="projecte"></param>
    ''' <returns></returns>
    Private Function EsProjecteObsolet(ByVal projecte As String) As Boolean

        '? ampans\informatica
        '? ampans\finances

        ' projecte.Contains("M:\MP\Ampans") OrElse

        If projecte.Contains("Informatica.vbp") OrElse
            projecte.Contains("Finances.vbp") OrElse
            projecte.ToUpper.Contains("UNRARDLL") OrElse
            projecte.ToUpper.Contains("FERFRANS") OrElse
            projecte.Contains("IncidenciesExpress") OrElse
            projecte.Contains("\Obsolets") OrElse
            projecte.ToUpper.Contains("\STREAM") OrElse
            projecte.ToUpper.Contains("\CLASES") OrElse
            projecte.ToUpper.Contains("\EXES") OrElse
            projecte.ToUpper.Contains("\EXCEL") OrElse
            projecte.ToUpper.Contains("\BORRAR") OrElse
            projecte.ToUpper.Contains("\ESBORRAT") OrElse
            projecte.ToUpper.Contains("\IMATGES") Then
            '?projecte.ToUpper.Contains("CONEXIOSISTEMA") Then   es correspon al GmsConexio

            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' Retorna si un "projecte" amb un fitxer de projecte asociat
    ''' </summary>
    ''' <param name="projecte"></param>
    ''' <returns></returns>
    Private Function EsProjecte(ByVal projecte As String) As Boolean

        If (Not projecte.Contains("Common.vbproj") AndAlso Not projecte.Contains("Controls.vbproj") AndAlso Not projecte.Contains("Entigest.vbproj") AndAlso
            Not projecte.Contains("Media.vbproj") AndAlso Not projecte.Equals("CRM\CRM.vbproj") AndAlso
                Not projecte.Contains("FormsComuns.vbproj") AndAlso
            Not projecte.Contains("Fin.vbproj") AndAlso Not projecte.Contains("Nex.vbproj")) Then

            Return True
        End If

        Return False
    End Function

    Private Function EsProjecteNET_Nou(ByVal projecte As String) As Boolean

        If projecte.ToUpper.Contains("VSCOMPONENTS") OrElse
            projecte.ToUpper.Contains("NOVESCLASSES") OrElse
            projecte.ToUpper.Contains("PRINTER") OrElse
            projecte.ToUpper.Contains("UTILS") Then

            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="camiProjecte"></param>
    ''' <returns></returns>
    Private Function ExisteixFitxerMigracio(ByVal camiProjecte As String) As Boolean
        Return File.Exists(camiProjecte & "\" & FITXER_TRACING_MIGRACIO)
    End Function

    ''' <summary>
    ''' retorna la linia seguent a una linia dins un fitxer
    ''' Observació : és la primera que no contingui 'UPGRADE
    ''' </summary>
    ''' <param name="numlinia"></param>
    ''' <param name="liniesFitxer"></param>
    ''' <returns></returns>
    Private Function GetLiniaSeguent(ByRef numlinia As Integer, ByVal liniesFitxer As List(Of String)) As String
        Dim splitList() As String = {}

        numlinia = numlinia + 1

        '!
        If (numlinia >= liniesFitxer.Count) Then Return ""

        Dim liniaSeguent As String = liniesFitxer.Item(numlinia)

        Dim t As Boolean = RegexCercaCadena(liniaSeguent, "'UPGRADE_\w+:", splitList)

        If t Then
            Return GetLiniaSeguent(numlinia, liniesFitxer)
        Else
            Return liniesFitxer(numlinia)
        End If
    End Function

    ''' <summary>
    ''' Retorna l'última data donat un estat dins el fitxer de tracing
    ''' </summary>
    ''' <returns></returns>
    Private Function GetDate_Ultima_by_Estat_Tracing(ByVal fitxerTracingMigracio As String, ByVal estat As E_EstatProcesMigracio, ByVal dataActualFitxerDesigner As String, ByVal dataActualFitxerResX As String) As Date
        Dim provider As CultureInfo = CultureInfo.InvariantCulture

        Dim dataIniciBuscar As DateTime = DateTime.MinValue

        '? comprovem si el fitxer està redissenyat i estem buscant la data de la foto corresponent
        If estat = E_EstatProcesMigracio.Executat_CopiaBackupVB6 AndAlso dataActualFitxerDesigner.CompareTo(dataActualFitxerResX) < 1 Then

            '! data a partir de la qual buscar
            dataIniciBuscar = dataActualFitxerResX

        Else
            Dim kk = 1

        End If

        Dim liniesFitxerTracing As List(Of String) = LlegeixFitxer(fitxerTracingMigracio & "\" & FITXER_TRACING_MIGRACIO, encoding)

        '! recorrem de l'última linia a la primera i busquem la que coincideixi amb "estat"
        For i = liniesFitxerTracing.Count - 1 To 0 Step -1
            If estat = EstatProcesMigracio(liniesFitxerTracing.Item(i)) Then

                Dim dataEstat As String = liniesFitxerTracing.Item(i).Split("|")(0)

                ' 03/09/2019 9:30:14
                Dim dataTemp As Date = DateTime.ParseExact(dataEstat, "dd/MM/yyyy HH:mm:ss", provider)

                '? busquem
                If dataIniciBuscar <> DateTime.MinValue AndAlso dataTemp < dataIniciBuscar Then
                    Return dataTemp
                ElseIf dataIniciBuscar = DateTime.MinValue Then
                    Return dataTemp

                End If
            End If
        Next

        Return Nothing
    End Function

    ''' <summary>
    ''' Actualitza les dates de redisseny dels fitxer NET
    ''' </summary>
    Private Sub Sincronitza_Dates_Redisseny()
        Dim provider As CultureInfo = CultureInfo.InvariantCulture

        Cursor = Cursors.WaitCursor

        For Each li As ListViewItem In lstItemsFoundNet.CheckedItems

            '? només hem de tractar els Designer.vb
            If Not EsFitxerDesigner(li.Text) Then Continue For

            Dim fitxerDesigner As String = li.Text
            Dim projecte As String = String.Empty
            Dim nomProjecteBAckup As String = String.Empty

            CalculaFitxerDatesRedisseny(fitxerDesigner, projecte)

            Dim num As Integer = 0
            Dim trobat As Boolean = False

            For Each l As String In dictFitxersDatesRedisseny.Item(projecte.ToUpper)
                Dim fitxer As String = String.Empty

                If String.IsNullOrEmpty(projecte) Then
                    fitxer = l.Split("|")(0)
                Else
                    fitxer = projecte & "\" & l.Split("|")(0).Replace(".\", "")    '? MIRARRRRRRRR
                End If


                '? busquem la linia coincident
                If GetFitxerResX(fitxerDesigner) = fitxer Then

                    '! calculem les diferents dates
                    Dim dataResX As Date = File.GetLastWriteTime(cboDirectoryNet.Text & fitxer)

                    '? modifiquem el valor
                    dictFitxersDatesRedisseny.Item(projecte.ToUpper).Item(num) = String.Format("{0}|{1}", l.Split("|")(0), Strings.Format(dataResX, FORMAT_DATAHORA_24H))

                    trobat = True

                    '! ja l'hem trobat
                    Exit For
                End If

                num = num + 1
            Next

            '? modifiquem el valor
            If Not trobat Then
                Dim dataResX As Date = File.GetLastWriteTime(cboDirectoryNet.Text & GetFitxerResX(fitxerDesigner))
                Dim linia As String = String.Format("{0}|{1}", GetFitxerResX(fitxerDesigner), Strings.Format(dataResX, FORMAT_DATAHORA_24H))

                '! modifiquem la linia
                dictFitxersDatesRedisseny.Item(projecte.ToUpper).Add(linia)
            End If

        Next

        '! guardem TOTS els fitxers de "redisseny" amb els nous valors
        For Each projecte As String In dictFitxersDatesRedisseny.Keys

            '! calculem el fitxer de "dates redisseny" del projecte
            Dim fitxersDatesRedissenyProjecte As String = cboDirectoryNet.Text & GetFitxerRedisseny(projecte)

            '! gravem el fitxer
            File.WriteAllLines(fitxersDatesRedissenyProjecte, dictFitxersDatesRedisseny.Item(projecte.ToUpper).ToArray, encoding)
        Next

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Sincronitza (copia) fitxers e BACKUP --> NET o de NET --> BACKUP
    ''' fitxer = .\2007\frmConexionDB.VB
    ''' </summary>
    Private Sub Sincronitza_Fitxers_BACKUP_I_NET(ByVal liniesFitxerRedisseny As List(Of String))

        Cursor = Cursors.WaitCursor

        Sincronitza_Dates_Redisseny()

        For Each f As String In liniesFitxerRedisseny
            '? agafem la part del fitxer (exemple : .\2007\frmConexionDB.resX)
            Dim fitxerResX As String = f.Split("|")(0)
            Dim fitxerDesigner As String = RegexSubstitucioCadena(fitxerResX, REGEX_RESX, REGEX_DESIGNER_SUBSTITUCIO)

            '? calculem la data de redisseny
            Dim dataRedisseny As Date = CalculaDataRedisseny(fitxerResX)

            '? calculem la data de l'ultima modificació del corresponent fitxer FRM (VB6)
            Dim dataFRM As Date = File.GetLastWriteTime(RegexSubstitucioCadena(cboDirectoryVB6.Text & fitxerResX, REGEX_RESX, REGEX_FRM_SUBSTITUCIO))

            '? si la data de l'última modificació del fitxer FRM és més gran que la data de redisseny (Designer/ResX) --> copiem el fitxer (BACKUP --> NET)
            If dataFRM > dataRedisseny Then
                Dim designerNET As String = RegexSubstitucioCadena(cboDirectoryNet.Text & fitxerResX, REGEX_RESX, REGEX_DESIGNER_SUBSTITUCIO)
                Dim designerBACKUP As String = RegexSubstitucioCadena(CarpetaDotNET() & "\" & fitxerResX, REGEX_RESX, REGEX_DESIGNER_SUBSTITUCIO)

                '? copiem el resX I el Designer.vb (si existeix!)
                If File.Exists(CarpetaDotNET() & "\" & fitxerResX) Then
                    File.Copy(CarpetaDotNET() & "\" & fitxerResX, cboDirectoryNet.Text & fitxerResX, True)
                    File.Copy(designerBACKUP, designerNET, True)

                    Debug.WriteLine("COPIAT : BACKUP --> NET " + fitxerResX)
                End If

            Else  '? copiem   NET --> BACKUP

                If File.Exists(cboDirectoryNet.Text & fitxerResX) Then

                    Dim designerNET As String = RegexSubstitucioCadena(cboDirectoryNet.Text & fitxerResX, REGEX_RESX, REGEX_DESIGNER_SUBSTITUCIO)
                    Dim designerBACKUP As String = RegexSubstitucioCadena(CarpetaDotNET() & "\" & fitxerResX, REGEX_RESX, REGEX_DESIGNER_SUBSTITUCIO)

                    '? copiem el resX I el Designer.vb
                    If File.Exists(cboDirectoryNet.Text & fitxerResX) Then
                        File.Copy(cboDirectoryNet.Text & fitxerResX, CarpetaDotNET() & "\" & fitxerResX, True)
                        File.Copy(cboDirectoryNet.Text & fitxerDesigner, CarpetaDotNET() & "\" & fitxerDesigner, True)
                    End If

                    If File.Exists(designerNET) Then
                        ''''''File.Copy(designerNET, designerBACKUP, True)   
                    Else
                        Debug.WriteLine("ALTOOOOOOOOOOOOOOOOOO : NO EXISTEIX " + designerNET)
                    End If

                    Debug.WriteLine("COPIAT : NET --> BACKUP " + fitxerResX)
                Else
                    Debug.WriteLine("------------ Fitxer nou " + fitxerResX)
                End If
            End If

        Next

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Afegeix la linia al fitxer "fitxerRedisseny"
    ''' WORKAROUND : SABER SI UN FITXER LI HEM FET EL "REDISSENY"
    '''          - creem linia fitxer.txt ( nomFitxer.resX|dataHora)
    ''' </summary>
    ''' <param name="cami"></param>
    ''' <param name="fitxer"></param>
    Private Sub ModificaLiniaFitxerRedisseny(ByVal cami As String, ByVal fitxer As String)
        Dim provider As CultureInfo = CultureInfo.InvariantCulture

        If fitxer.ToUpper.Contains("RESX") Then

            Dim dataActualFitxerResX As Date = File.GetLastWriteTime(cboDirectoryNet.Text & cami & fitxer)
            Dim dataActualFitxerFRM As Date = File.GetLastWriteTime(RegexSubstitucioCadena(cboDirectoryVB6.Text & fitxer, REGEX_RESX, REGEX_FRM_SUBSTITUCIO))

            Dim projecte As String = String.Empty
            Dim linia As String = String.Empty

            '! busquem la linia dins el fitxer "redisseny" i donat el fitxer designer
            Dim index As Integer = BuscarLinia(CalculaFitxerDatesRedisseny(fitxer, projecte), fitxer, linia)

            '? si estem fent una migració "total" , afegim linies
            If Not flagMigracioIncremental Then
                liniesFitxerRedisseny.Add(String.Format("{0}|{1}", cami & fitxer, Strings.Format(Now, FORMAT_DATAHORA_24H)))

            Else  '! si és una migració incremental, NOMÉS modifiquem les linies corresponents a fitxers que s'han modificat

                '? hem de comprovar si la linia l'hem de mantenir (= el corresponent fitxer VB6 no s'ha modificat)
                If dataActualFitxerFRM > dataActualFitxerResX Then

                    If index = -1 Then
                        liniesFitxerRedisseny.Add(String.Format("{0}|{1}", cami & fitxer, Strings.Format(Now, FORMAT_DATAHORA_24H)))
                    Else
                        '? actualitzem la linia
                        liniesFitxerRedisseny.Item(index) = String.Format("{0}|{1}", cami & fitxer, Strings.Format(Now, FORMAT_DATAHORA_24H))
                    End If
                End If
            End If

        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="liniaVBProj"></param>
    ''' <param name="nomModul"></param>
    ''' <param name="s"></param>
    ''' <param name="novalinia"></param>
    ''' <param name="fitxerVB6Proj"></param>
    ''' <param name="cadenaTipusVB6"></param>
    ''' <param name="cadenaTipusNET"></param>
    Private Sub Actualitza(liniaVBProj As String, nomModul As String, s() As String, ByRef novalinia As String, fitxerVB6Proj As String, cadenaTipusVB6 As String, cadenaTipusNET As String)
        Try
            Dim cami As String = BuscarPathFitxer(liniaVBProj, cadenaTipusVB6.Replace("&amp;", "&").Replace("_BAS.", "."), fitxerVB6Proj)

            Dim carpetaDesti As String = CarpetaDotNET() & "\" & cami

            '! creem subcarpeta (ex.2007)
            If Not Directory.Exists(carpetaDesti) AndAlso Not cami.Contains("..\") Then
                Directory.CreateDirectory(carpetaDesti)
            End If

            '! movem el fitxer a la carpeta 
            Dim fitxerOrigen As String = CarpetaDotNET() & "\" & s(1)

            If File.Exists(fitxerOrigen.Replace("&amp;", "&")) Then      '! exemple : Drag&amp;DropTreeView.vb
                If s(1).Contains(EXTENSIO_FRM_DUPLICATS) Then
                    Dim X As Integer = CInt(nomModul.Substring(nomModul.IndexOf(EXTENSIO_FRM_DUPLICATS) + 5, 1))
                    Dim nouPatro As String = s(1).Replace(EXTENSIO_FRM_DUPLICATS & CStr(X), "")
                    Dim fitxerDesti As String = carpetaDesti & nouPatro

                    If Not File.Exists(fitxerDesti) Then
                        File.Copy(fitxerOrigen, fitxerDesti, True)

                        File.Delete(fitxerOrigen)

                        novalinia = "<" & cadenaTipusNET & " Include=""" & cami & nouPatro & """" & s(2)

                        '! modifiquem la linia al fitxer redisseny                      
                        ModificaLiniaFitxerRedisseny(cami, nouPatro)
                    End If

                ElseIf s(1).Contains("_bas") OrElse s(1).Contains("_frm") Then  '! el Wizard VS2008 quan detecta un formulari i un bas amb el mateix nom (exemple : Directoris.frm, Directoris.bas), crea un <fitxer>_frm.vb i un <fitxer>_bas.vb
                    If File.Exists(fitxerOrigen) Then
                        Dim fitxerDesti As String = carpetaDesti & s(1).Replace("_frm.", ".").Replace("_bas.", ".")

                        If cami.Contains("..\") AndAlso Not fitxerOrigen.Contains("..\") Then
                            File.Delete(fitxerOrigen)

                            novalinia = "<" & cadenaTipusNET & " Include=""" & cami & s(1).Replace("_frm.", ".").Replace("_bas.", ".") & """" & s(2)

                            '! modifiquem la linia al fitxer redisseny
                            ModificaLiniaFitxerRedisseny(cami, s(1))

                            Exit Sub
                        End If

                        If Not File.Exists(fitxerDesti) Then
                            File.Copy(fitxerOrigen, fitxerDesti, True)

                            novalinia = "<" & cadenaTipusNET & " Include=""" & cami & s(1).Replace("_frm.", ".").Replace("_bas.", ".") & """" & s(2)

                        Else
                            File.Delete(fitxerOrigen)
                        End If

                        '! modifiquem la linia al fitxer redisseny
                        ModificaLiniaFitxerRedisseny(cami, s(1))
                    End If

                Else

                    Dim pos1 = cami.LastIndexOf("..\") + 3
                    Dim pos2 = cami.LastIndexOf("\")

                    Dim nomProjecteMigrat As String = If(pos1 = 2 OrElse pos2 < pos1, "", cami.Substring(pos1, pos2 - pos1) & ".NET\")   '! ES EL NOM DE LA CARPETA DEL PROJECTE QUE FEM REFERENCIA
                    Dim camiProjecteMigrat As String = carpetaDesti & nomProjecteMigrat

                    If cami.Contains("..\") AndAlso Not fitxerOrigen.Contains("..\") Then

                        '! OPCIO 1 : ELIMINAR EL FITXER PERQUE PERTANY A UN ALTRE PROJECTE O CARPETA (EXEMPLE: COMMON, ENTIGEST\FORMCOMUNS,...)
                        File.Delete(fitxerOrigen.Replace("&amp;", "&"))

                        '! OPCIO 2 : APROFITAR EL PROCES DE MIGRACIÓ PER MIGRAR EL FITXER QUE PERTANY A UN ALTRE PROJECTE O CARPETA
                        '!           (EXEMPLE : PROCES DE MIGRACIO DE RRPP I TAMBÉ TRACTO FITXERS DE L'ENTIGEST\FORMCOMUNS)
                        'Dim nomProjecte As String = cami.Replace("..\", "").Split("\")(0)
                        'Dim camiBackupProjecte = cami.Replace(nomProjecte, nomProjecte & "\" & nomProjecte & ".NET")

                        'Dim carpetaDestiAltraProjecte As String = CarpetaDotNET() & "\..\" & camiBackupProjecte & Path.GetFileName(fitxerOrigen)
                        'File.Move(fitxerOrigen, carpetaDestiAltraProjecte)


                        novalinia = "<" & cadenaTipusNET & " Include=""" & cami & s(1).Replace("_frm.", ".").Replace("_bas.", ".") & """" & s(2)

                        Exit Sub
                    End If

                    Dim fitxerDesti = camiProjecteMigrat & "\" & s(1).Replace("&amp;", "&")
                    If Not File.Exists(fitxerDesti) Then
                        If Directory.Exists(camiProjecteMigrat) AndAlso Not s(1).Contains("\") Then
                            File.Copy(fitxerOrigen, fitxerDesti, True)

                            File.Delete(fitxerOrigen)

                            novalinia = "<" & cadenaTipusNET & " Include=""" & cami & s(1).Replace("_frm.", ".").Replace("_bas.", ".") & """" & s(2)

                            If s(1).ToUpper.Contains("RESX") Then
                                liniesFitxerRedisseny.Add(String.Format("{0}|{1}", cami & s(1), Strings.Format(Now, FORMAT_DATAHORA_24H)))

                                '! aprofitem també per copiar el fitxer en la part NET (produccio)
                                If Not Directory.Exists(cboDirectoryNet.Text & cami & s(1)) Then Exit Sub

                                File.Copy(CarpetaDotNET() & "\" & cami & s(1), cboDirectoryNet.Text & cami & s(1), True)

                            ElseIf EsFitxerDesigner(s(1)) Then
                                '! aprofitem també per copiar el fitxer en la part NET (produccio)

                                If Not Directory.Exists(cboDirectoryNet.Text & cami) Then
                                    Directory.CreateDirectory(cboDirectoryNet.Text & cami)
                                End If

                            End If

                        End If
                    Else

                        '! modifiquem la linia al fitxer redisseny
                        ModificaLiniaFitxerRedisseny(cami, s(1))
                    End If

                End If
            Else
                '! modifiquem la linia al fitxer redisseny
                ModificaLiniaFitxerRedisseny(cami, s(1))
            End If

        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Cslcula la data de redisseny (Designer/ResX) d'un fitxer
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function CalculaDataRedisseny(ByVal fitxer As String) As Date
        Dim provider As CultureInfo = CultureInfo.InvariantCulture

        Dim liniesFitxerRedisseny As List(Of String) = LlegeixFitxer(cboDirectoryNet.Text & FITXER_REDISSENY, encoding)

        For Each l As String In liniesFitxerRedisseny
            Dim fitxerTemp As String = Split(l, "|")(0)

            If fitxerTemp.Equals(fitxer) Then
                Return DateTime.ParseExact(l.Split("|")(1), FORMAT_DATAHORA_24H, provider)
            End If
        Next

        Return Nothing
    End Function

    ''' <summary>
    ''' Retorna el fitxer de projecte VB6 dins una carpeta.Si no existeix, retorna String.Empty
    ''' </summary>
    ''' <param name="carpeta"></param>
    ''' <returns></returns>
    Private Function BuscarFitxersProjectesVB6(carpeta As String) As String
        If Not Directory.Exists(carpeta) Then Return String.Empty

        Dim fitxersProj As List(Of String)

        Try
            Dim fitxersVBP() As String = Directory.GetFiles(carpeta, REGEX_VBP)
            If fitxersVBP.Count = 0 Then Return String.Empty

            fitxersProj = Directory.GetFiles(carpeta, REGEX_VBP).Where(Function(s) s.EndsWith(EXTENSIO_VBP)).ToList     '! si no afegim la part LINQ inclou el fitxer vbproj (.NET) que no volem

            If fitxersProj.Count = 0 Then
                Return String.Empty
            End If

        Catch ex As Exception
            Return String.Empty
        End Try

        Return fitxersProj(0)
    End Function

    ''' <summary>
    ''' Retorna el fitxer de projecte NET dins una carpeta.Si no existeix, retorna String.Empty
    ''' </summary>
    ''' <param name="carpeta"></param>
    ''' <returns></returns>
    Private Function BuscarFitxersProjectesNET(carpeta As String) As String
        Dim fitxersProj() As String

        Try
            fitxersProj = Directory.GetFiles(carpeta, "*.vbproj")

            If fitxersProj.Count = 0 Then
                Return String.Empty
            End If

        Catch ex As Exception
            Return String.Empty
        End Try

        Return fitxersProj(0)
    End Function

    ''' <summary>
    ''' Carrega les files d'una datatable a un listview
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <param name="lv"></param>
    Private Function CarregarListView(dt As DataTable, lv As ListView, capsalera As String) As Integer
        For Each dtr As DataRow In dt.Rows
            Dim lvi As ListViewItem = Nothing

            If dt.Columns.Contains("data") Then
                lvi = New ListViewItem({dtr("fitxer").ToString(), dtr("data").ToString()})
            Else
                lvi = New ListViewItem({dtr("fitxer").ToString()})
            End If

            '! marquem amb un altra color els fitxers Obsolets
            If EsObsoletFitxer(lvi.Text) Then
                lvi.BackColor = Color.PaleVioletRed
            End If

            '! comprovem si hem de visualitzar els formularis de "prova"
            If Not chkInclouFormsNous.Checked Then
                If RegexCercaCadena(lvi.Text, "(?i)form\d+") OrElse RegexCercaCadena(lvi.Text, "(?i)formtest\d+") Then
                    Continue For
                End If
            End If

            lv.Items.Add(lvi)
        Next

        Return lv.Items.Count
    End Function

    ''' <summary>
    ''' Busquem els fitxers : VB6 Proj i NET Proj. Els dos contenen la extensio "vbp"  !!! dins de carpeta seleccionada
    ''' </summary>
    Private Function BuscarItemsVB6_NET(buscarFitxersProjecte As Boolean, ByVal ambExtensio As Boolean, ByVal inclouDesigners As Boolean, Optional cami As String = "") As Boolean
        Dim carpetaVB6 As String = cami.Replace(CARPETA_NET, CARPETA_VB6)
        Dim carpetaNET As String = cami.Replace(CARPETA_VB6, CARPETA_NET)

        Dim fitxerProjecteVB6 As String = BuscarFitxersProjectesVB6(carpetaVB6)
        Dim fitxerProjecteNET As String = BuscarFitxersProjectesNET(carpetaNET)

        Try
            '! si volem comparar entre projectes (VB6 versus NET)
            If buscarFitxersProjecte Then
                '! si no existeix el fitxer de projecte VB6, vol dir que no es un projecte
                If String.IsNullOrEmpty(fitxerProjecteVB6) Then
                    'MessageBox.Show(String.Format("No es correspon amb un projecte"))
                    Return False
                End If

                Dim nomProjecte As String = fitxerProjecteNET.Replace(cboDirectoryNet.Text & "\", "")

                If String.IsNullOrEmpty(nomProjecte) Then
                    GetFitxersVBP_NET(carpetaVB6, "*", True, False)
                    GetFitxersVBP_NET(carpetaNET, "*", True, True)

                    Label_1.Text = "Forms: "
                    Label_2.Text = "Forms:"
                    Label_3.Text = "Moduls:"
                    Label_4.Text = "Classes:"

                    '! Si no es un projecte
                    '!ElseIf Not EsProjecte(nomProjecte) Then
                    '!    GetFitxersVBP_NET(carpetaVB6, "*", True, True)
                    '!    GetFitxersVBP_NET(carpetaNET, "*", True, True)

                    '!    Label_1.Text = "Fitxers:"
                    '!    Label_2.Text = "Fitxers:"

                ElseIf String.IsNullOrEmpty(fitxerProjecteVB6) AndAlso Not String.IsNullOrEmpty(fitxerProjecteNET) Then  '! si es un projecte , però ens falta algun dels fitxer de projecte (VB6 i/o NET)
                    GetFitxersVBP_NET(carpetaVB6, "*", True, False)
                    GetFitxersVBP_NET(carpetaNET, "*", True, False)

                    Label_1.Text = "Forms:"
                    Label_2.Text = "Forms:"
                    Label_3.Text = "Moduls:"
                    Label_4.Text = "Classes:"

                ElseIf Not String.IsNullOrEmpty(fitxerProjecteVB6) AndAlso Not String.IsNullOrEmpty(fitxerProjecteNET) Then   '! Es un projecte > comparem vbp versus vbpProj
                    GetFormsVB6_Aplicacio(fitxerProjecteVB6, ambExtensio)
                    GetFormsVBNet_Aplicacio(fitxerProjecteNET, ambExtensio, inclouDesigners)

                    Label_1.Text = "Forms:"
                    Label_2.Text = "Forms:"
                    Label_3.Text = "Moduls:"
                    Label_4.Text = "Classes:"
                End If
            Else
                GetFitxersVBP_NET(carpetaVB6, "*", True, True)
                GetFitxersVBP_NET(carpetaNET, "*", True, True)

                Label_1.Text = "Fitxers:"
                Label_2.Text = "Fitxers:"

            End If

        Catch ex As Exception
            Return False
        End Try

        Return (Not String.IsNullOrEmpty(fitxerProjecteVB6) AndAlso Not String.IsNullOrEmpty(fitxerProjecteNET))
    End Function

    ''' <summary>
    ''' Retorna 
    ''' </summary>
    ''' <returns></returns>
    Private Function BuscarNomProjecteAmbCami() As String
        Dim carpetaProjecteMigrat As String = cboDirectoryVB6_BACKUP.Text

        Dim fitxersProj() As String = If(Not chkMigrarBackup.Checked, Directory.GetFiles(cboDirectoryNet.Text, REGEX_VBP), Directory.GetFiles(cboDirectoryVB6_BACKUP.Text & "\", REGEX_VBP))

        If fitxersProj.Count > 0 Then
            Return fitxersProj(0).Substring(0, fitxersProj(0).LastIndexOf("."))
        End If

        Return String.Empty
    End Function

    ''' <summary>
    ''' Retorna el path (vincle) que hem d'afegir. Exemple : 
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <returns></returns>
    Private Function BuscarPathFitxer(cadena As String, patro As String, fitxerVB6Proj As String) As String
        Dim pos As Short = 1  '! indica el numero de coincidencia quan tenim formularis amb el mateix nom i el Wizard VS2008 genera : <nomformulari>_frm_1, <nomformulari>_frm_2, <nomformulari>_frm_3. Observació: comença per 1

        Try
            Dim liniesVBP As List(Of String) = LlegeixFitxer(fitxerVB6Proj, encoding)

            Dim splitList() As String = {}

            For i As Integer = 0 To liniesVBP.Count - 1

                If patro.Contains(EXTENSIO_FRM_DUPLICATS) Then  '! el Wizard VS2008 quan detecta un formulari amb el mateix nom (en diferents carpetes 2007, 2011), crea <nomformulari>_frm_X.vb
                    '! calculem l'X de <nomformulari>_frm_X
                    Dim X As Integer = CInt(patro.Substring(patro.IndexOf(EXTENSIO_FRM_DUPLICATS) + 5, 1))
                    Dim nouPatro As String = patro.Replace(EXTENSIO_FRM_DUPLICATS & CStr(X), "")

                    If RegexCercaCadena(liniesVBP(i), nouPatro, splitList) Then   '? CAL TENIR EN COMPTE QUE EL REGEX ES CASE SENSITIVE

                        If pos = X Then   '! coincideix 
                            Return splitList(1)  '! la primera posicio retorna el nom del path                        
                        End If

                        pos = pos + 1
                    End If
                ElseIf patro.Contains("_bas") Then  '! el Wizard VS2008 quan detecta un formulari i un bas amb el mateix nom (exemple : Directoris.frm, Directoris.bas), crea un <fitxer>_frm.vb i un <fitxer>_bas.vb
                    Dim nouPatro As String = patro.Replace("_bas", "")

                    If RegexCercaCadena(liniesVBP(i), nouPatro, splitList) Then   '? CAL TENIR EN COMPTE QUE EL REGEX ES CASE SENSITIVE
                        Return splitList(1)  '! la primera posicio retorna el nom del path                        
                    End If

                ElseIf patro.Contains("_frm") Then  '! el Wizard VS2008 quan detecta un formulari i un bas amb el mateix nom (exemple : Directoris.frm, Directoris.bas), crea un <fitxer>_frm.vb i un <fitxer>_bas.vb
                    Dim nouPatro As String = patro.Replace("_frm", "")

                    If RegexCercaCadena(liniesVBP(i), nouPatro, splitList) Then   '? CAL TENIR EN COMPTE QUE EL REGEX ES CASE SENSITIVE
                        Return splitList(1)  '! la primera posicio retorna el nom del path                        
                    End If

                Else
                    If RegexCercaCadena(liniesVBP(i), patro, splitList) Then
                        If splitList.Count >= 3 AndAlso Not liniesVBP(i).Contains("Form=") Then
                            Return splitList(2)  '! la primera posicio retorna el nom del path                        
                        Else
                            Return splitList(1)  '! la primera posicio retorna el nom del path                        
                        End If

                    End If
                End If

            Next

        Catch ex As Exception

        End Try

        Return ""
    End Function


    ''' <summary>
    ''' Busquem fitxers d'un directori modificats a partir d'una data i que contenen un "patro"
    ''' Només retorna els fitxers "migrables"
    ''' </summary>
    ''' <param name="sDir"></param>
    ''' <param name="desde"></param>
    ''' <param name="patro"></param>
    Private Sub FitxersModificatsDirectori(ByVal sDir As String, desde As Date, patro As String, ByVal VB6 As Boolean)
        If Not Directory.Exists(sDir) Then Exit Sub

        Try
            For Each f In Directory.GetFiles(sDir, patro)
                If EsMigrableFitxer(f) Then

                    If VB6 Then '! cas fitxers VB6

                        Label_1.Text = "Fitxers VB6 modificats:"

                        If desde = Date.MinValue Then
                            AfegirRowDatatable(dtVB6, f)
                        Else
                            If File.GetLastWriteTime(f) > desde Then
                                AfegirRowDatatable(dtVB6, f, File.GetLastWriteTime(f))
                            End If
                        End If

                    Else  '! cas fitxers .Net

                        Label_2.Text = "Fitxers NET modificats:"

                        If (chkMigrarDesigner.Checked AndAlso EsFitxerDesigner(f)) Then
                            AfegirRowDatatable(dtNET, f.Replace(carpetaAProcessar, ""))
                        End If

                        If (chkMigrarCodi.Checked AndAlso Not EsFitxerDesigner(f)) Then
                            AfegirRowDatatable(dtNET, f.Replace(carpetaAProcessar, ""))
                        End If
                    End If
                End If

            Next
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sDir"></param>
    Sub DirSearch(ByVal sDir As String, desde As Date, patro As String, ByVal VB6 As Boolean)
        Dim carpetaOrigen As String = CARPETA_NET

        If chkMigrarBackup.Checked Then
            carpetaOrigen = CarpetaDotNET()
        End If

        Try
            For Each d As String In Directory.GetDirectories(sDir)

                If String.IsNullOrEmpty(CarpetaDotNET) Then Exit Sub

                If Not Directory.Exists(d.Replace(CarpetaDotNET, cboDirectoryVB6.Text)) Then
                    MessageBox.Show(String.Format("La corresponent subcarpeta en BACKUP {0} no existeix en VB6 {1} !!!!", carpetaOrigen, carpetaOrigen.Replace(CARPETA_VB6_BACKUP, CARPETA_VB6)))
                    Continue For
                End If

                '! comprovem si la subcarpeta es un projecte (conté VBP) o no
                Dim esProjecteSubCarpeta As Boolean = If(Directory.GetFiles(d, REGEX_VBP).Count = 0, False, True)

                If Not esCarpetaRootProjecte Then
                    If esProjecteSubCarpeta Then
                        Continue For
                    End If
                End If


                FitxersModificatsDirectori(d, desde, patro, VB6)

                DirSearch(d, desde, patro, VB6)
            Next
        Catch excpt As System.Exception
            Debug.WriteLine(excpt.Message)
        End Try
    End Sub

    Private Function ObtenirControlFRM(ByVal fitxerFRM As String) As List(Of ControlVB6)
        Dim lstControlsVB6 As List(Of ControlVB6)

        Dim carpeta As String = String.Empty
        If chkMigrarBackup.Checked Then
            '! construim el path del fitxer FRM sense la carpeta ".NET" (cas que "validem" un projecte de BACKUP)
            Dim carpetaTemp = fitxerFRM.Replace(CARPETA_VB6_BACKUP.ToUpper, "")
            For Each subcarpeta In carpetaTemp.Split("\")
                If Not subcarpeta.ToUpper.Contains(".NET") Then
                    carpeta = carpeta & "\" & subcarpeta
                End If
            Next

            lstControlsVB6 = ProcessaControls(CARPETA_VB6 & carpeta)

        Else
            lstControlsVB6 = ProcessaControls(fitxerFRM)
        End If

        '? dilluns 12/7
        '? la propietat Page (fitxer FRM) s'ha de processar després de carregar tots els controls
        '''''''''ProcessaPropietat_Page_TabControlPage(fitxerFRM)

        '? dilluns 12/7
        ''''''''''PostProcesTabControl()

        Return lstControlsVB6
    End Function

    ''' <summary>
    ''' Formulari que demana una pregunta i "retorna" la resposta i si "aplica" la mateixa reposta a les seguents preguntes
    ''' </summary>
    ''' <param name="pregunta"></param>
    ''' <returns></returns>
    Private Function PreguntaAplicaSeguents(ByVal pregunta As String, ByVal descripcio As String) As Boolean
        ' check del formulari frmPreguntaAplicaSeguents
        If Not aplicaSeguents Then
            Dim frmPreguntaAplicaSeguents As New frmPreguntaAplicaSeguents(pregunta, descripcio)

            frmPreguntaAplicaSeguents.ShowDialog()

            respostaPreguntaOCX = frmPreguntaAplicaSeguents.Resposta    ' True = SI
            aplicaSeguents = frmPreguntaAplicaSeguents.AplicaSeguents
        End If

        Return respostaPreguntaOCX
    End Function

    ''' <summary>
    ''' en funcio del tipus de control eliminem la linia OCXState
    ''' </summary>
    ''' <returns></returns>
    Private Function CalEliminarOCXState(ByVal tipusControl As String) As Boolean
        '? dimecres 2
        '? Per tal d'eliminar les excepcions System.Runtime.InteropServices.COMException' en System.Windows.Forms.dll
        '? cal eliminar les linies OCXState i redissenya 1 a 1 toes els formularis
        '? EFECTE COLATERAL: elimina valors de propietats dels controls ActiveX
        '? i de moment no sabem com fer-ho!!!!!!!!!!!!
        '''''''Return True

        '? AxXtremeSuiteControls.AxPushButton
        '? AxXtremeSuiteControls.AxDateTimePicker
        '? AxXtremeSuiteControls.AxCheckBox
        '? AxXtremeSuiteControls.AxGroupBox
        '? AxXtremeSuiteControls.AxRadioButton

        esborrarOCX = False  '! inicialitzacio.Per defecte esborrem les linies OCXState.IMPORTANT : si esborrem la linia OCXState podem perdre els valors del control

        '! El control AxTabControl s'ha migrat a .Net i per tant cal eliminar la linia OCXState
        If tipusControl.Contains(TIPUSCONTROL_CODEJOC_TABCONTROL_v2) Then
            esborrarOCX = True
            '! WORKAROUND : si modifiquem la propietat Text en runtime de labels que estan dins de AxGroupBox, aquesta propietat no es modifica
        ElseIf tipusControl.Contains("AxXtremeSuiteControls.AxGroupBox") Then

            esborrarOCX = PreguntaAplicaSeguents("Es substitueix AxGroupBox per GmsGroupBox ?", "Si passem d'un control ActiveX a un NET es poden perdre valors de propietats (exemple : Size,...)")

        Else   '? dilluns 13/9
            If tipusControl.Contains("FlexCell.Grid") OrElse tipusControl.Contains("DataControl.GmsCombo") OrElse
                tipusControl.Contains("DataControl.GmsData") OrElse tipusControl.Contains("DataControl.GmsImports") OrElse
                tipusControl.Contains("DataControl.GmsSetmana") OrElse tipusControl.Contains("gmsTime.GmsTemps") OrElse
                tipusControl.Contains("DataControl.gmsBarra") OrElse tipusControl.Contains("Vintasoft.Imaging.UI.ImageViewer") OrElse
                tipusControl.Contains("AxFlexCell.AxGrid") OrElse tipusControl.Contains("AxSSDataWidgets_B.AxSSDBCombo") OrElse
                tipusControl.Contains("AxDataControl.AxGmsData") OrElse tipusControl.Contains("AxDataControl.AxGmsImports") OrElse
                tipusControl.Contains("AxDataControl.AxGmsSetmana") OrElse tipusControl.Contains("AxgmsTime.AxgmsTemps") OrElse
                tipusControl.Contains("AxDataControl.AxgmsBarra") OrElse tipusControl.Contains("AxGdpicturePro.AxgdViewer") OrElse
                tipusControl.Contains("AxMSWinsockLib.AxWinsock") OrElse tipusControl.Contains("System.Net.Sockets.Socket") OrElse
                tipusControl.Contains("XtremeSuiteControls.GroupBox") Then

                esborrarOCX = True
            End If

            If tipusControl.Contains("AxXtremeSuiteControls") Then  '? el ActiveX no li hem d'esborrar MAI la linia OCX
                esborrarOCX = False
            End If
        End If

        Return esborrarOCX
    End Function

    ''' <summary>
    ''' en funcio del tipus de control eliminem la linia OCXState
    ''' </summary>
    ''' <returns></returns>
    Private Function CalEliminarBeginInitEndInit(ByVal tipusControl As String) As Boolean
        Dim resultat As Boolean = False
        '? dilluns 21
        '! El control AxTabControl s'ha migrat a .Net i per tant cal eliminar la linia OCXState
        If (tipusControl.Contains("GmsGroupBox") OrElse tipusControl.Contains("AxXtremeSuiteControls.AxGroupBox")) Then
            resultat = True
        ElseIf tipusControl.Contains("AxGdpicturePro.AxgdViewer") Then
            resultat = True
        ElseIf esborrarOCX AndAlso tipusControl.Contains("GroupBox") Then
            resultat = True
        ElseIf esborrarOCX AndAlso tipusControl.Contains("frame") Then
            resultat = True
        ElseIf tipusControl.Contains("GmsTabPage") Then
            resultat = False    '? excepcio
        ElseIf tipusControl.Contains("GmsTabControl") Then
            Return False      '? excepcio
        ElseIf tipusControl.Contains("Grid") Then
            resultat = True
        ElseIf tipusControl.Contains("AxXtremeSuiteControls") Then
            resultat = False
        ElseIf tipusControl.Contains("AxSSDBCombo") Then
            Return True
        ElseIf tipusControl.Contains("AxGmsData") Then
            Return True

        ElseIf tipusControl.Contains("AxGmsImports") OrElse tipusControl.ToUpper.Contains("GMSIMPORTS") Then
            Return False   '! GmsImports implementa ISupportInitialize
        ElseIf tipusControl.Contains("AxgmsTemps") OrElse tipusControl.ToUpper.Contains("GMSTEMPS") Then
            Return False   '! gmsTemps implementa ISupportInitialize
        ElseIf tipusControl.Contains("AxGmsSetmana") Then
            Return False   '! GmsSetmana implementa ISupportInitialize
        ElseIf tipusControl.Contains("Array") Then
            Return False   '! els ControlArray implementen ISupportInitialize
        Else
            Dim kk = 1
        End If

        Return resultat
    End Function

    ''' <summary>
    ''' cal comprovar si té sentit aplicar la regla segons el tipus de fitxer 
    ''' </summary>
    ''' <returns></returns>
    Private Function CalAplicarRegleRegex(ByVal liniaRegex As String, ByVal fitxerNET As String) As Boolean
        If liniaRegex.Split(";").Count > 5 Then
            If liniaRegex.Split(";")(5) = "V" Then    '! regla que NOMES té sentit per fitxers vb (codi)
                If EsFitxerDesigner(fitxerNET) Then    '! si ES un fitxer Designer
                    Return False
                End If
            ElseIf liniaRegex.Split(";")(5) = "D" Then    '! regla que NOMES té sentit per fitxers Designer
                If Not EsFitxerDesigner(fitxerNET) Then   '! si NO es un fitxer Designer
                    Return False
                End If
            End If
        End If

        Return True
    End Function

    ''' <summary>
    '''  aplica filtres (regex)
    ''' </summary>
    ''' <param name="numLinia"></param>
    ''' <param name="novaLinia"></param>
    ''' <param name="liniesSubstituides"></param>
    ''' <param name="fitxerNET"></param>
    ''' <param name="lstControlsVB6"></param>
    ''' <param name="trobat"></param>
    Private Sub AplicaSubstitucions(ByVal numLinia As Integer, ByRef novaLinia As String, liniesSubstituides As List(Of String), ByVal fitxerNET As String, lstControlsVB6 As List(Of ControlVB6), trobat As Boolean)
        Dim liniaAtractar As String = liniesFitxerNET(numLinia)

        '? WORKAROUND : EXCEPCIONS   '? dimecres 22/6/2022
        '? OBSERVACIONS :
        '?      - per tal de migrar correctament el Height dels controls cal posar AutoSize = True. Hi ha PERÒ EXCEPCIONS         
        '?      - per defecte en .Net la propietat AutoSize és False, per tant no cal afegir la linia
        If liniaAtractar.Trim = "Me.lblLock." & PROPIETAT_AUTOSIZE & " = False" Then   '? PUNT FEBLE
            novaLinia = liniaAtractar

            liniesSubstituides.Add(novaLinia)

            Exit Sub
        End If

        '! recorrem totes les reglex (regex)
        For Each liniaRegex As String In lstLiniesSubstitucio

            '! excloim les linies en blanc i les linies que comencen en "" (comentaris)
            If liniaRegex.Trim <> String.Empty AndAlso Mid(liniaRegex, 1, 2) <> "--" Then

                '? cal comprovar si té sentit aplicar la regla
                Dim aplicable As Boolean = CalAplicarRegleRegex(liniaRegex, fitxerNET)

                Dim splitList() As String = {}

                Dim t1Linia As DateTime = Now

                If aplicable Then

                    If CoincideixCadena(liniaAtractar, liniaRegex, splitList) Then
                        '! Si té una cadena de subtitucio
                        If liniaRegex.Split(";").Count > 1 Then

                            Select Case liniaRegex.Split(";")(1)
                                Case "N"    '! CAS : (XXXXXX;N;YYYYYYY)
                                    novaLinia = liniaAtractar.Replace(liniaRegex.Split(";")(0), liniaRegex.Split(";")(2))
                                Case "R"
                                    If liniaRegex.Split(";").Count = 6 Then
                                        If Not liniaAtractar.Contains(liniaRegex.Split(";")(3)) Then
                                            novaLinia = liniaAtractar.Replace(liniaRegex.Split(";")(2), liniaRegex.Split(";")(3))
                                        Else
                                            novaLinia = RegexSubstitucioCadena(liniaAtractar.Trim, liniaRegex.Split(";")(0), liniaRegex.Split(";")(2))
                                        End If

                                    ElseIf liniaRegex.Split(";").Count = 5 Then   '! CAS : REGEX (<PATRO>;<CADENA_BUSCAR>;C;<CONDICIO_NOT_CONTAINS>)  
                                        If Not liniaAtractar.Contains(liniaRegex.Split(";")(4)) Then
                                            novaLinia = RegexSubstitucioCadena(liniaAtractar.Trim, liniaRegex.Split(";")(0), liniaRegex.Split(";")(2))
                                        Else
                                            novaLinia = liniaAtractar
                                        End If

                                    ElseIf liniaRegex.Split(";").Count = 4 Then   '! CAS : REGEX (<PATRO>;<CADENA_BUSCAR>;<CADENA_SUBSTITUCIO>)  > SUBSTITUEIX SUBSTRING
                                        novaLinia = liniaAtractar.Replace(liniaRegex.Split(";")(2), liniaRegex.Split(";")(3))
                                    ElseIf liniaRegex.Split(";").Count = 3 Then   '! CAS : REGEX (<PATRO>;<PATRO_SUBSTITUCIO>) 
                                        novaLinia = RegexSubstitucioCadena(liniaAtractar, liniaRegex.Split(";")(0), liniaRegex.Split(";")(2))

                                        '? comprovem si la linia que estem tractant fa referencia a un control de tipus AxGroupBox
                                        '? Recordem que sen's ha preguntat si voliem migrar o no a GmsGroupBox!!!!!!!
                                        If RegexCercaCadena(liniaAtractar, "AxGroupBox") Then       '? dijous 28/10
                                            '? IMPORTANT : segons el fitxer : substitucions_designer.txt, per defecte migrem a GmsGroupBox
                                            '?EN AQUEST CAS NO VOLEM MIGRAR A GMSGROUPBOX SINO A GMSAXGROUPBOX!!!!
                                            If Not esborrarOCX Then
                                                novaLinia = RegexSubstitucioCadena(novaLinia, "(?i)GmsGroupBox", "GmsGroupBox")
                                            End If
                                        End If

                                    End If

                                Case "C"    '! CALCUL (Exemple : GetTwipsToPixelsX(234)
                                    Dim cadenaACalcular As String = liniaAtractar.Substring(liniaAtractar.IndexOf(liniaRegex.Split(";")(2)))

                                    '! Flag per activar/desactivar el processament de la conversio automatica twips/pixels
                                    If chkMigrarTwipsPixels.Checked AndAlso Not cadenaACalcular.ToUpper.Contains("MIGRATPIXELS") Then

                                        novaLinia = SubstituirTwipsPixels(cadenaACalcular, fitxerNET, numLinia)

                                        '! Tests unitari
                                        Dim numRegla As Integer = PassaTest(cadenaACalcular, fitxerNET, numLinia)

                                        If numRegla > 0 Then    '! si numRegla > 0, ha trobat una regla Regex que cumpleix
                                            novaLinia = novaLinia & LINIA_MIGRADA_TWIPSPIXELS
                                        Else
                                            '! afegim a la listView
                                            WorkAround_LiniesAmbTwipsPixels(cadenaACalcular, fitxerNET, numLinia)

                                            '! deixem la linia sense modificar
                                            novaLinia = cadenaACalcular
                                        End If
                                    Else
                                        novaLinia = cadenaACalcular
                                    End If

                            End Select

                            If Not String.IsNullOrEmpty(novaLinia) Then
                                liniesSubstituides.Add(novaLinia)
                            Else
                                liniesSubstituides.Add("")
                            End If
                        End If

                        trobat = True

                        Exit For   '!  SI VOLEM SEGUIR SUBSTITUINT

                        'Debug.WriteLine("#Linia :  {0} : {1}", i.ToString, liniesFitxerNET(i))
                    Else
                        'Debug.WriteLine("#Linia :  {0} : {1}", i.ToString, liniesFitxerNET(i))
                    End If
                End If

                Dim difLinia As Double = Now.Subtract(t1Linia).TotalMilliseconds

                If difLinia > 2 Then '! milisegons
                    Debug.Print(String.Format("         Temps durada regla {0} sobre linia {1} :: durada : {2}", liniaRegex, liniaAtractar, difLinia.ToString))
                End If

            Else
                '''' Continue For
            End If

        Next


        '! si la linia no fa "match" amb cap regla (regex), la deixem tal qual
        If Not trobat Then

            Dim splitList() As String = {}
            '? linies que no hem d'afegir
            If RegexCercaCadena(liniaAtractar, REGEX_CONST_ME & "\w+\.Items\." & TIPUSLINIA_TOOLBAR_ADDRANGE & "\(New System.Windows.Forms.ToolStripItem\(\)", splitList) Then
                '? en aquest cas es substituida
                '? NO FER RES     
            End If

            '? eliminar les linies del tipus Me.Toolbar1.Items.Add(_Toolbar1_Button19)
            '? És substiuit per la linia AddRange que hem "construit" previament
            If RegexCercaCadena(liniaAtractar, REGEX_CONST_ME & "Toolbar\w+\.Items\." & TIPUSLINIA_TOOLBAR_ADD & "\(\w+\)", splitList) Then
                Exit Sub '! no l'afegim a l'array de "liniesSubstituides"
            End If


            '! re-ordenar els elements de controlArray (si ho és!)
            'x If Workaround_OrdreElementsControlArray(numLinia, liniesSubstituides) Then Exit Sub         '? dijous 28/10 . Sembla que no s'ha de posar!

            '! propietats que al migrar han de tenir valors diferents
            '? Nota : aquesta linia ha d'estar abans que la linia (99)
            If WorkAround_ActualitzarPropietats(liniesFitxerNET, numLinia, liniesSubstituides) Then Exit Sub

            '! la resta de casos (99)
            liniesSubstituides.Add(liniaAtractar)  '! deixem la mateixa linia    

            '? propietats no migrades
            WorkAround_AfegirPropietatsNoMigrades(liniesFitxerNET, numLinia, liniesSubstituides)

        End If

    End Sub

    ''' <summary>
    ''' Propietats que tenen valors diferents
    ''' </summary>
    ''' <param name="liniesFitxerVB"></param>
    ''' <param name="numLinia"></param>
    ''' <param name="liniesSubstituides"></param>
    ''' <returns>True = s'ha trobat una coincideincia i no cal seguir processant la linia</returns>
    Private Function WorkAround_ActualitzarPropietats(ByVal liniesFitxerVB As List(Of String), ByVal numLinia As Integer, ByRef liniesSubstituides As List(Of String)) As Boolean
        Dim splitList() As String = {}

        '! comprovem si la linia es la definicio de la propietat Size d'un ToolStripItem
        If RegexCercaCadena(liniesFitxerVB(numLinia), REGEX_CONST_ME & "(_Toolbar\w+)\." & PROPIETAT_SIZE & " =", splitList) Then

            If ObtenirControlVB6ByName(splitList(1), lstControlsVB6).ButtonToolbarStyle = "3" Then
                Dim nouValor As String = "6"    '? el valor dels ToolStripItem (Style = 3) tenen Width = 6

                Construir_Linia_Propietat_Valor(splitList(1), TIPUSLINIA_TOOLBAR_SIZE, "New System.Drawing.Size(" & nouValor & ", 24)", liniesSubstituides)

                Return True '! no seguir processant la linia

            ElseIf ObtenirControlVB6ByName(splitList(1), lstControlsVB6).ButtonToolbarStyle = "4" Then
                Dim nouValor As String = "1"  '? el valor dels ToolStripItem (Style = 4) tenen Width = 1

                Construir_Linia_Propietat_Valor(splitList(1), TIPUSLINIA_TOOLBAR_SIZE, "New System.Drawing.Size(" & nouValor & ", 24)", liniesSubstituides)

                Return True '! no seguir processant la linia
            End If

        End If

        '"BorderStyle = System.Windows.Forms.BorderStyle"
        '! Propietat APPEARANCE      borderStyle despres de Location
        If RegexCercaCadena(liniesFitxerVB(numLinia), "Me\.(\w+)\.BorderStyle = System\.Windows\.Forms\.BorderStyle", splitList) Then
            Dim controlVB6 As ControlVB6 = ObtenirControlVB6ByName(splitList(1), lstControlsVB6)

            If controlVB6 Is Nothing Then
                Return False
            End If

            If String.IsNullOrEmpty(controlVB6.Appearance) Then Return False

            Construir_Linia_Propietat_Valor(splitList(1), PROPIETAT_BORDERSTYLE, controlVB6.Appearance, liniesSubstituides)

            Return True '! no seguir processant la linia
        End If

        Return False
    End Function

    ''' <summary>
    ''' WORKAROUNDs : hi ha propietats que el Wizard2008 no migra o no migra correctament
    ''' </summary>
    ''' <param name="liniesSubstituides"></param>
    Private Sub WorkAround_AfegirPropietatsNoMigrades(ByVal liniesFitxerVB As List(Of String), ByVal numLinia As Integer, ByRef liniesSubstituides As List(Of String))
        Try


            '? dimecres 2
            '! Propietat BORDERSTYLE/GRID
            ''''If liniesFitxerVB.Count - 1 > numLinia AndAlso numLinia > 0 Then
            ''''    '! Insertem la linia corresponent a la propietat BorderStyle DELS FLEXCELL GRID (QUE NO ES MIGRA!!!!!)
            ''''    '? Observació: l'insertem abans que la linia que estem tractant (en aquest cas abans de la linia Me.<grid>.Location = 
            ''''    Dim liniaBorderStyleGrid As String = ObtenirLiniaByPropietatBorderStyleGrid(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB(numLinia - 1), PROPIETAT_BORDERSTYLE)
            ''''    If liniaBorderStyleGrid IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count - 1).Trim <> liniaBorderStyleGrid.Trim Then
            ''''        liniesSubstituides.Add(liniaBorderStyleGrid)
            ''''    End If
            ''''End If

            If liniesFitxerVB.Count - 1 > numLinia AndAlso numLinia > 0 Then
                '! Propietat BORDERSTYLE GMSGROUPBOX        borderStyle despres de Location
                Dim liniaBorderStyleGmsGroupBox As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_BORDERSTYLE, PROPIETAT_LOCATION)
                If liniaBorderStyleGmsGroupBox IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count - 1).Trim <> liniaBorderStyleGmsGroupBox.Trim Then
                    liniesSubstituides.Add(liniaBorderStyleGmsGroupBox)
                End If

                Try


                    '! Propietat BACKCOLOR GMSGROUPBOX  
                    '? no se perque hi algun formulari que em provoca un pet !!!!!!!!       '? dimecres 11/1/2023
                    Dim liniaBackColorGmsGroupBox As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_BACKCOLOR, PROPIETAT_LOCATION)
                    If liniaBackColorGmsGroupBox IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count - 1).Trim <> liniaBackColorGmsGroupBox.Trim Then
                        liniesSubstituides.Add(liniaBackColorGmsGroupBox)
                    End If

                Catch ex As Exception
                    Dim KKKKKKK = 1
                End Try

                '! Propietat FORECOLOR GMSGROUPBOX
                Dim liniaForeColorGmsGroupBox As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_FORECOLOR, PROPIETAT_LOCATION)
                If liniaForeColorGmsGroupBox IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaForeColorGmsGroupBox.Trim Then
                    liniesSubstituides.Add(liniaForeColorGmsGroupBox)
                End If

                '! Propietat ENABLED GMSGROUPBOX
                Dim liniaEnabledGmsGroupBox As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_ENABLED, PROPIETAT_LOCATION)
                If liniaEnabledGmsGroupBox IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaEnabledGmsGroupBox.Trim Then
                    liniesSubstituides.Add(liniaEnabledGmsGroupBox)
                End If

                '! Propietat ALIGNMENT/TABCONTROL
                Dim liniaAlignmentGmsTabControl As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_PAINTMANAGER_POSITION, PROPIETAT_LOCATION)
                If liniaAlignmentGmsTabControl IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaAlignmentGmsTabControl.Trim Then
                    liniesSubstituides.Add(liniaAlignmentGmsTabControl)
                End If

                '! Propietat FORMAT_TEMPS/GMSTEMPS
                Dim liniaFormatTemps As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_FORMAT_TEMPS, PROPIETAT_LOCATION)
                If liniaFormatTemps IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaFormatTemps.Trim Then
                    liniesSubstituides.Add(liniaFormatTemps)
                End If

                '! Propietat DECIMALES/GMSIMPORTS
                Dim liniaDecimales As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_DECIMALES, PROPIETAT_LOCATION)
                If liniaDecimales IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaDecimales.Trim Then
                    liniesSubstituides.Add(liniaDecimales)
                End If

                '! Propietat MAXLENGHT/GMSIMPORTS
                Dim liniaMaxLenght As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_MAXLENGTH, PROPIETAT_LOCATION)
                If liniaMaxLenght IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaMaxLenght.Trim Then
                    liniesSubstituides.Add(liniaMaxLenght)
                End If

                '! Propietat SEPARADORMILES/GMSIMPORTS
                Dim liniaSeparadorMiles As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_SEPARADORMILES, PROPIETAT_LOCATION)
                If liniaSeparadorMiles IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaSeparadorMiles.Trim Then
                    liniesSubstituides.Add(liniaSeparadorMiles)
                End If

                '! Propietat CEROS/GMSIMPORTS
                Dim liniaCeros As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_OCULTARCEROS, PROPIETAT_LOCATION)
                If liniaCeros IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaCeros.Trim Then
                    liniesSubstituides.Add(liniaCeros)
                End If

                '! Propietat SIGNO/GMSIMPORTS
                Dim liniaSigno As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_SIGNE, PROPIETAT_LOCATION)
                If liniaSigno IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaSigno.Trim Then
                    liniesSubstituides.Add(liniaSigno)
                End If

                '! Propietat ARRODONIMENT/GMSIMPORTS
                Dim liniaArrodoniment As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_ARRODONIMENT, PROPIETAT_LOCATION)
                If liniaArrodoniment IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaArrodoniment.Trim Then
                    liniesSubstituides.Add(liniaArrodoniment)
                End If

                '! Propietat COLUMNHEADERS/GMSCOMBO
                Dim liniaColumnHeaders As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_COLUMNHEADERS, PROPIETAT_LOCATION)
                If liniaColumnHeaders IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaColumnHeaders.Trim Then
                    liniesSubstituides.Add(liniaColumnHeaders)
                End If

                '! Propietat BACKCOLORODD/GMSCOMBO
                Dim liniaBackColorOdd As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_BACKCOLORODD, PROPIETAT_LOCATION)
                If liniaBackColorOdd IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaBackColorOdd.Trim Then
                    liniesSubstituides.Add(liniaBackColorOdd)
                End If

                '! Propietat DATAFIELDLIST/GMSCOMBO --> EQUIVALENT A DISPLAYMEMBER/NET
                Dim liniaDataFieldList As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_DATAFIELDLIST, PROPIETAT_LOCATION)
                If liniaDataFieldList IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaDataFieldList.Trim Then
                    liniesSubstituides.Add(liniaDataFieldList)
                End If

                '! Propietat COLUMNSNAME/GMSCOMBO 
                Dim liniaColumnsNameList As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_COLUMNSNAME, PROPIETAT_LOCATION)
                If liniaColumnsNameList IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaColumnsNameList.Trim Then
                    liniesSubstituides.Add(liniaColumnsNameList)
                End If

                '! Propietat Style/TreeView
                Dim liniaStyle As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_STYLE, PROPIETAT_LOCATION)
                If liniaStyle IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaStyle.Trim Then
                    liniesSubstituides.Add(liniaStyle)
                End If

                '! Propietat LineStyle/TreeView
                Dim liniaLineStyle As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_LINESTYLE, PROPIETAT_LOCATION)
                If liniaLineStyle IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaLineStyle.Trim Then
                    liniesSubstituides.Add(liniaLineStyle)
                End If

                '! Propietat FontName/GmsTemps
                Dim liniaFontName As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_FONTNAME, PROPIETAT_LOCATION)
                If liniaFontName IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaFontName.Trim Then
                    liniesSubstituides.Add(liniaFontName)
                End If

                '! Propietat FontSize/GmsTemps
                Dim liniaFontSize As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_FONTSIZE, PROPIETAT_LOCATION)
                If liniaFontSize IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaFontSize.Trim Then
                    liniesSubstituides.Add(liniaFontSize)
                End If

                '! Propietat DefaultFontName/FCGrid
                Dim liniaDefaultFontName As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_DEFAULTFONTNAME, PROPIETAT_LOCATION)
                If liniaDefaultFontName IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaDefaultFontName.Trim Then
                    liniesSubstituides.Add(liniaDefaultFontName)
                End If

                '! Propietat DefaultFontSize/FCGrid
                Dim liniaDefaultFontSize As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_DEFAULTFONTSIZE, PROPIETAT_LOCATION)
                If liniaDefaultFontSize IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaDefaultFontSize.Trim Then
                    liniesSubstituides.Add(liniaDefaultFontSize)
                End If

                '! Propietat DefaultRowHeight/FCGrid
                Dim liniaDefaultRowHeight As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_DEFAULTROWHEIGHT, PROPIETAT_LOCATION)
                If liniaDefaultRowHeight IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaDefaultRowHeight.Trim Then
                    liniesSubstituides.Add(liniaDefaultRowHeight)
                End If

                '! Propietat SelectionMode/FCGrid
                Dim liniaSelectionMode As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_SELECTIONMODE, PROPIETAT_LOCATION)
                If liniaSelectionMode IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaSelectionMode.Trim Then
                    liniesSubstituides.Add(liniaSelectionMode)
                End If

                '! Propietat ReadOnly/FCGrid
                Dim liniaLocked As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_READONLY, PROPIETAT_LOCATION)
                If liniaLocked IsNot Nothing AndAlso liniesFitxerVB(liniesSubstituides.Count).Trim <> liniaLocked.Trim Then
                    liniesSubstituides.Add(liniaLocked)
                End If

            End If

            '? toolbar
            If WorkAround_Toolbar(liniesFitxerVB, numLinia, liniesSubstituides) Then Exit Sub


            If liniesFitxerVB.Count - 1 > numLinia Then
                Dim splitList() As String = {}

                RegexCercaCadena(liniesFitxerVB(numLinia), REGEX_CONST_ME & "(\w+)\.\w+ =", splitList)

                If splitList.Count = 0 OrElse splitList(1) Is Nothing Then
                    Exit Sub
                End If

                Dim control As ControlVB6 = ObtenirControlVB6ByName(splitList(1), lstControlsVB6)

                If control Is Nothing Then Exit Sub

                '! Propietat TAG     tag despres de tabindex. (En principi tots els controls tenen la propietat "TabIndex")
                Dim liniaTag As String = AfegeixPropietatNoMigrada(lstControlsVB6, liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_TAG, PROPIETAT_LOCATION)
                If liniaTag IsNot Nothing Then
                    liniesSubstituides.Add(liniaTag)
                End If

                '! Insertem la linia corresponent a la propietat CAPTION DELS TABCONTROLPAGE (QUE NO ES MIGRA!!!!!). (En principi tots els controls tenen la propietat "TabIndex")
                AfegirPropietatNoMigrada(liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_CAPTION, PROPIETAT_TABINDEX, liniesSubstituides)

                '! Insertem la linia corresponent a la propietat FONT 
                AfegirPropietatNoMigrada(liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_FONT, PROPIETAT_CURSOR, liniesSubstituides)


                If control.tipusControl = TIPUSCONTROL_FCGRID Then
                    AfegirPropietatNoMigrada(liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_FONT, PROPIETAT_SIZE, liniesSubstituides)
                End If

                If control.tipusControl = TIPUSCONTROL_LABEL Then
                    ''''AfegirPropietatNoMigrada(liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_BACKSTYLE, PROPIETAT_SIZE, liniesSubstituides)    '? dijous 21/7/2022
                End If

                '? Propietat Autosize + Label/CheckBox
                ''''If control.tipusControl = TIPUSCONTROL_LABEL OrElse control.tipusControl = TIPUSCONTROL_CODEJOC_CHECKBOX Then
                ''''AfegirPropietatNoMigrada(liniesFitxerVB(numLinia), liniesFitxerVB, PROPIETAT_AUTOSIZE, PROPIETAT_SIZE, liniesSubstituides)    '? dijous 21/7/2022
                ''''End If
                Dim KKKK = 1

            End If

        Catch ex As Exception
            Dim KKK = 1
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    Private Sub AfegirPropietatNoMigrada(ByVal linia As String, ByVal liniesFitxerVB As List(Of String), ByVal propietat As String, ByVal propietatAnterior As String, ByRef liniesSubstituides As List(Of String))
        Dim liniaAfegir As String = AfegeixPropietatNoMigrada(lstControlsVB6, linia, liniesFitxerVB, propietat, propietatAnterior)
        If liniaAfegir IsNot Nothing Then
            liniesSubstituides.Add(liniaAfegir)
        End If
    End Sub

    Private Function Construir_Instanciacio_Toolbar(ByVal liniesFitxerVB As List(Of String), ByVal numLinia As Integer, ByRef liniesSubstituides As List(Of String)) As Boolean
        Dim splitList() As String = {}

        '! instanciació
        If RegexCercaCadena(liniesFitxerVB(numLinia), REGEX_CONST_ME & "\w+ = New System.Windows.Forms.ToolStrip$", splitList) Then
            Dim ltsb As List(Of ControlVB6) = lstControlsVB6.Where(Function(s) s.tipusControl = TIPUSCONTROL_TOOLSTRIPBUTTON).ToList

            For Each tsb As ControlVB6 In ltsb
                Dim inst1 As String = ConstruirToolStripButton_New(tsb)

                '?
                If String.IsNullOrEmpty(inst1) Then Continue For

                '? excepció: no sé perquè el Wizard2008 SI migra els ToolStrip de tipus ToolStripSeparator
                If tsb.ButtonToolbarStyle = "3" Then Continue For

                '! afegim la linia
                liniesSubstituides.Add(inst1)
            Next

            Return True  ' ja no cal seguir processant la linia
        End If

        Return False
    End Function

    Private Function Construir_WithEvents_Toolbar(ByVal liniesFitxerVB As List(Of String), ByVal numLinia As Integer, ByRef liniesSubstituides As List(Of String)) As Boolean
        Dim splitList() As String = {}

        If RegexCercaCadena(liniesFitxerVB(numLinia), "\w+ WithEvents \w+ As System.Windows.Forms.ToolStrip$", splitList) Then
            Dim ltsb As List(Of ControlVB6) = lstControlsVB6.Where(Function(s) s.tipusControl = TIPUSCONTROL_TOOLSTRIPBUTTON).ToList

            For Each tsb As ControlVB6 In ltsb
                Dim inst1 As String = ConstruirToolStripButton_WithEvents(tsb)

                '? 
                If String.IsNullOrEmpty(inst1) Then Continue For

                '? excepció: no sé perquè el Wizard2008 SI migra els ToolStrip de tipus ToolStripSeparator
                If tsb.ButtonToolbarStyle = "3" Then Continue For

                '! afegim la linia
                liniesSubstituides.Add(inst1)
            Next

            Return True  ' ja no cal seguir processant la linia
        End If

        Return False
    End Function

    ''' <summary>
    ''' AddRange + Toolbar
    ''' Exemple :  Me.Toolbar1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._Toolbar1_Button1, Me._Toolbar1_Button5, Me._Toolbar1_Button7, Me._Toolbar1_Button11, Me._Toolbar1_Button15, Me._Toolbar1_Button17, Me._Toolbar1_Button19})
    ''' </summary>
    ''' <param name="liniesFitxerVB"></param>
    ''' <param name="numLinia"></param>
    ''' <param name="liniesSubstituides"></param>
    ''' <returns></returns>
    Private Function Construir_AddRange_Toolbar(ByVal liniesFitxerVB As List(Of String), ByVal numLinia As Integer, ByRef liniesSubstituides As List(Of String)) As Boolean
        Dim splitList() As String = {}

        If RegexCercaCadena(liniesFitxerVB(numLinia), REGEX_CONST_ME & REGEX_NOMTOOLBAR & "\.ImageList =", splitList) Then
            '? agafem TOTS els tipus de ToolStripS!!!
            Dim ltsb As List(Of ControlVB6) = lstControlsVB6.Where(Function(s) s.tipusControl = TIPUSCONTROL_TOOLSTRIPBUTTON).ToList

            '? IMPORTANT : cal comprovar que en un procés de migració/validacio no es dupliquin les linies New
            If Not ConteLiniaToolbar(splitList(1), TIPUSLINIA_TOOLBAR_ADDRANGE) Then

                Dim inst As String = CONST_ME & splitList(1) & ".Items." & TIPUSLINIA_TOOLBAR_ADDRANGE & "(New System.Windows.Forms.ToolStripItem() {"

                For Each tsb As ControlVB6 In ltsb
                    '! si és l'últim element no hem d'afegir la , al final
                    If ltsb.Last.Equals(tsb) Then
                        inst = inst & CONST_ME & tsb.nomControl
                    Else
                        inst = inst & CONST_ME & tsb.nomControl & ", "
                    End If
                Next

                '! tanquem el Range
                inst = inst & "})"

                '! afegim la linia
                liniesSubstituides.Add(inst)

                Return True  ' ja no cal seguir processant la linia
            End If
        End If

        Return False
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="nomControl"></param>
    ''' <param name="propietat"></param>
    ''' <param name="valor"></param>
    ''' <param name="liniesSubstituides"></param>
    Private Sub Construir_Linia_Propietat_Valor(ByVal nomControl As String, ByVal propietat As String, ByVal valor As String, ByRef liniesSubstituides As List(Of String))
        If Not ConteLiniaToolbar(nomControl, propietat) Then
            Dim inst1 As String = CONST_ME & nomControl & "." & propietat & " = " & valor
            liniesSubstituides.Add(inst1)
        End If
    End Sub

    ''' <summary>
    ''' Migració del control MSComctlLib.Toolbar Toolbar1/VB6
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="controlVB6"></param>
    Private Sub WorkAround_Toolbar(ByVal linia As String, ByRef controlVB6 As ControlVB6)
        Dim splitList() As String = {}

        If controlToolbarTrobat AndAlso RegexCercaCadena(linia, LINIA_END, splitList) Then
            controlToolbarTrobat = False

            nomToolbar = String.Empty   '! reset

        ElseIf controlToolbarTrobat Then  '! estem dins "MSComctlLib.Toolbar Toolbar1"

            '! inici property Buttons
            If RegexCercaCadena(linia, REGEX_ESPAI & TIPUSLINIA_TOOLBAR_BEGINPROPERTYBUTTONS, splitList) Then
                propertyButtonsToolbar = True

                '! creem un nou control
                controlVB6 = New ControlVB6()

                '! assignem els valors inicials de les propietat del control
                With controlVB6
                    .tipusControl = TIPUSCONTROL_TOOLSTRIPBUTTONS
                    .nomControl = UNDERSCORE & nomToolbar & "_Button" & splitList(1)    '? nota : el nom dels ToolStripButton, ToolStripSeparator, ... comencen en un _
                End With

                '!
                ActualitzaControl(controlVB6)

                '! inici property Buttons
            ElseIf RegexCercaCadena(linia, REGEX_ESPAI & TIPUSLINIA_TOOLBAR_BEGINPROPERTYBUTTON & "(\d+)", splitList) Then
                propertyButtonToolbar = True

                '! creem un nou control
                controlVB6 = New ControlVB6()

                '! assignem els valors inicials de les propietat del control
                With controlVB6
                    .tipusControl = TIPUSCONTROL_TOOLSTRIPBUTTON
                    .nomControl = UNDERSCORE & nomToolbar & "_Button" & splitList(1)    '? nota : el nom dels ToolStripButton, ToolStripSeparator, ... comencen en un _
                End With

                '!
                ActualitzaControl(controlVB6)

            End If


            '? EndProperty del corresponent BeginProperty ButtonXX
            If propertyButtonToolbar AndAlso RegexCercaCadena(linia, LINIA_ENDPROPERTY, splitList) Then
                propertyButtonToolbar = False

                Debug.WriteLine(String.Format("========>> QUA.REMOVE : {0}", qua.Item(qua.Count - 1).nomControl))

                '! eliminem el propi control
                qua.RemoveAt(qua.Count - 1)
            End If

            '? EndProperty del corresponent BeginProperty Buttons
            If propertyButtonsToolbar AndAlso Not propertyButtonToolbar AndAlso RegexCercaCadena(linia, LINIA_ENDPROPERTY, splitList) Then
                propertyButtonsToolbar = False

                Debug.WriteLine(String.Format("========>> QUA.REMOVE : {0}", qua.Item(qua.Count - 1).nomControl))

                '! eliminem el propi control
                qua.RemoveAt(qua.Count - 1)
            End If


            If propertyButtonToolbar Then   '! a partir d'aqui hi ha els valors d'un Button del Toolbar

                '! Propietats dels buttons de Tollbar
                If RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_STYLE & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then
                    controlVB6.ButtonToolbarStyle = splitList(1)

                    '? 0 = Default
                    '? 1 = Check
                    '? 2 = Button     --->    System.Windows.Forms.ToolStripButton
                    '? 3 = Separator    --->   System.Windows.Forms.ToolStripSeparator
                    '? 4 = PlaceHolder   --->    System.Windows.Forms.ToolStripContainer


                ElseIf RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_KEY & CONST_EQUALS & "\""(\w+)\""", splitList) Then
                    controlVB6.ButtonToolbarKey = splitList(1)
                ElseIf RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_WIDTH_TOOLSTRIPBUTTON & CONST_EQUALS & "([\w-]+)", splitList) Then    '? exemple :   Object.Width  =   1e-4
                    controlVB6.ButtonToolbarObjectWidth = splitList(1)
                ElseIf RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_VALUE & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then
                    controlVB6.ButtonToolbarValue = splitList(1)
                End If

            End If

        End If

    End Sub

    ''' <summary>
    ''' WORKAROUND : el Wizard2008 no migra correctament del tot els ToolBar
    ''' </summary>
    ''' <param name="liniesFitxerVB"></param>
    ''' <param name="numLinia"></param>
    ''' <param name="liniesSubstituides"></param>
    Private Function WorkAround_Toolbar(ByVal liniesFitxerVB As List(Of String), ByVal numLinia As Integer, ByRef liniesSubstituides As List(Of String)) As Boolean
        Dim splitList() As String = {}

        '! instanciació
        If Construir_Instanciacio_Toolbar(liniesFitxerVB, numLinia, liniesSubstituides) Then Return True

        '! WithEvents
        If Construir_WithEvents_Toolbar(liniesFitxerVB, numLinia, liniesSubstituides) Then Return True

        '? AddRange
        If Construir_AddRange_Toolbar(liniesFitxerVB, numLinia, liniesSubstituides) Then Return True


        '? comprovem si estem a la linia TabIndex d'una Toolbar (nota : tot Toolbar té la propietat TabIndex)
        If RegexCercaCadena(liniesFitxerVB(numLinia), REGEX_CONST_ME & "Toolbar\w*\." & PROPIETAT_TABINDEX & " = \d+", splitList) Then
            '? agafem TOTS els tipus de ToolStripS!!!
            Dim ltsb As List(Of ControlVB6) = lstControlsVB6.Where(Function(s) s.tipusControl = TIPUSCONTROL_TOOLSTRIPBUTTON).ToList

            For Each tsb As ControlVB6 In ltsb

                If Not String.IsNullOrEmpty(tsb.ButtonToolbarKey) Then     '!  System.Windows.Forms.ToolStripButton

                    '? 1 = Check  , 2 = ButtonGroup , 4 = PlaceHolder , Nothing
                    If tsb.ButtonToolbarStyle = "4" OrElse tsb.ButtonToolbarStyle = "1" OrElse tsb.ButtonToolbarStyle = "2" OrElse tsb.ButtonToolbarStyle Is Nothing Then

                        Construir_Linia_Propietat_Valor(tsb.nomControl, TIPUSLINIA_TOOLBAR_DISPLAYSTYLE, "System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText", liniesSubstituides)


                        Construir_Linia_Propietat_Valor(tsb.nomControl, TIPUSLINIA_TOOLBAR_IMAGEKEY, """" & tsb.ButtonToolbarKey & """", liniesSubstituides)


                        Construir_Linia_Propietat_Valor(tsb.nomControl, TIPUSLINIA_TOOLBAR_AUTOSIZE, CONST_FALSE, liniesSubstituides)


                        Construir_Linia_Propietat_Valor(tsb.nomControl, TIPUSLINIA_TOOLBAR_SIZE, "New System.Drawing.Size(24, 24)", liniesSubstituides)

                    End If

                End If
            Next

            Return True    '! ja no cal seguir processant
        End If


        '? propietats de controls ImageList
        If RegexCercaCadena(liniesFitxerVB(numLinia), REGEX_CONST_ME & "(ImageList\w+)\.TransparentColor =", splitList) Then
            Construir_Linia_Propietat_Valor(splitList(1), TIPUSLINIA_TOOLBAR_IMAGESTREAM, "CType(resources.GetObject(""" & splitList(1) & ".ImageStream""), System.Windows.Forms.ImageListStreamer)", liniesSubstituides)

            Return True    '! ja no cal seguir processant
        End If

        '? afegim propietats no migrades després del la linia que defineix la propietat Size
        If RegexCercaCadena(liniesFitxerVB(numLinia), REGEX_CONST_ME & REGEX_NOMTOOLBAR & "\." & PROPIETAT_SIZE & " =", splitList) Then

            '! Anchor
            Construir_Linia_Propietat_Valor(splitList(1), TIPUSLINIA_TOOLBAR_ANCHOR, "System.Windows.Forms.AnchorStyles.None", liniesSubstituides)

            '! AutoSize
            Construir_Linia_Propietat_Valor(splitList(1), TIPUSLINIA_TOOLBAR_AUTOSIZE, CONST_FALSE, liniesSubstituides)

            '! Dock
            Construir_Linia_Propietat_Valor(splitList(1), TIPUSLINIA_TOOLBAR_DOCK, "System.Windows.Forms.DockStyle.None", liniesSubstituides)

            '! GripStyle
            Construir_Linia_Propietat_Valor(splitList(1), TIPUSLINIA_TOOLBAR_GRIPSTYLE, "System.Windows.Forms.ToolStripGripStyle.Hidden", liniesSubstituides)

            Return True    '! ja no cal seguir processant
        End If

        Return False   '! hem de seguir processant la linia
    End Function

    ''' <summary>
    ''' Mètode que aplica les "validacions" sobre un fitxer de NET (*.vb, *.designer.vb)
    ''' </summary>
    ''' <param name="fitxerNET"></param>
    Private Sub AplicaReglesFitxerNet(ByVal fitxerNET As String, ByRef liniesSubstituides As List(Of String))
        '! excloim si el fitxer no es "migrable"
        If EstaEnLlistaNegra(fitxerNET, False) Then Exit Sub

        Try
            If Not esProva Then fitxerNET = fitxerNET

            Dim fitxerFRM As String = If(EsFitxerDesigner(fitxerNET), fitxerNET.ToUpper.Replace(CARPETA_NET, CARPETA_VB6).Replace(".DESIGNER.VB", EXTENSIO_FRM), fitxerNET.ToUpper.Replace(CARPETA_NET, CARPETA_VB6).Replace(".VB", EXTENSIO_FRM))

            '! Obtenim tots els controls del fitxer FRM
            Dim lstControlsVB6 As List(Of ControlVB6) = ObtenirControlFRM(fitxerFRM)

            dicControlArray = New Dictionary(Of String, List(Of ControlArray))
            dicEsOrdenatElementsControlArray = New Dictionary(Of String, Boolean)
            dictLiniesDuplicadesToolbar = New Dictionary(Of String, List(Of String))

            Dim numLiniaInitializeComponent As Integer = -1  '! numero de linia de la instruccio InitializeComponent()

            If File.Exists(fitxerNET) Then
                '! carreguem les linies del fitxer NET
                liniesFitxerNET = LlegeixFitxer(fitxerNET, encoding)

                lstItemControlArray = New List(Of ControlArray)

                Debug.WriteLine(String.Format("---- Processant fitxer : {0} , #Linies : {1}", fitxerNET, liniesFitxerNET.Count))

                Dim t1Fitxer As DateTime = Now

                For numLinia As Integer = 0 To liniesFitxerNET.Count - 1
                    Dim liniaAtractar As String = liniesFitxerNET(numLinia)

                    '? ***********   no tractem les linies que ja s'han migrat
                    If EsLiniaMigrada(liniaAtractar) Then
                        liniesSubstituides.Add(liniaAtractar)
                        Continue For
                    End If


                    '! Pel Bug dels controlArray i la/s inicialitzacio/ns dels "items" dels ControlArray
                    If EsFitxerDesigner(fitxerNET) Then
                        GetControlArray(liniaAtractar)

                        '! busquem quina linia del fitxer hi ha una instrucció que comença per "InitializeComponent()"
                        Dim rx As Regex = New Regex("^InitializeComponent\(\)")

                        If rx.Matches(liniaAtractar.Trim).Count > 0 Then
                            numLiniaInitializeComponent = numLinia + 1
                        End If
                    End If

                    Dim novaLinia As String = String.Empty
                    Dim trobat As Boolean = False
                    Dim splitList() As String = {}


                    '! 1.- Processem l'eliminacio de les linies OCXState si cal (en funció del tipus de control)
                    If EsFitxerDesigner(fitxerNET) AndAlso RegexCercaCadena(liniaAtractar, "(\w+)\.OcxState =", splitList) Then
                        For Each c As ControlNET In controlsNetByForm
                            If c.nomControl.ToUpper = splitList(1).ToUpper Then
                                '! calculem si cal eliminar la linia OcxState
                                novaLinia = If(CalEliminarOCXState(c.tipusControl), "", liniaAtractar)

                                liniesSubstituides.Add(novaLinia)

                                Continue For   '! saltem perque ja l'hem trobat
                            End If
                        Next

                    ElseIf EsFitxerDesigner(fitxerNET) AndAlso RegexCercaCadena(liniaAtractar, "CType\(Me\.(\w+), System.ComponentModel.ISupportInitialize\)", splitList) Then
                        For Each c As ControlNET In controlsNetByForm
                            If c.nomControl.ToUpper = splitList(1).ToUpper Then
                                '! calculem si cal eliminar la linia BeginInit/EndInit
                                novaLinia = If(CalEliminarBeginInitEndInit(c.tipusControl), "", liniaAtractar)

                                liniesSubstituides.Add(novaLinia)

                                Continue For   '! saltem perque ja l'hem trobat
                            End If
                        Next


                    ElseIf RegexCercaCadena(liniaAtractar, "Me\.(\w+)\.AutoSize" & CONST_EQUALS & "(\w+)", splitList) Then
                        Dim ctrl As ControlVB6 = ObtenirControlVB6ByName(splitList(1), lstControlsVB6)

                        Dim kkkk = 1

                        '! WORKAROUND : propietat Indent/TreeView s'ha de passar a pixels
                    ElseIf RegexCercaCadena(liniaAtractar, REGEX_CONST_ME & "(\w+)(\." & PROPIETAT_INDENT & CONST_EQUALS & ")(\d+)", splitList) Then

                        Dim ctrl As ControlVB6 = ObtenirControlVB6ByName(splitList(1), lstControlsVB6)

                        '! comprovem si ja ho hem processat anteriorment
                        If splitList(3) = Math.Round(ctrl.Indentation / 15) Then
                            liniesSubstituides.Add(liniaAtractar)
                            Continue For
                        End If

                        liniesSubstituides.Add(RegexSubstitucioCadena(liniaAtractar, "(\." & PROPIETAT_INDENT & "\s*=)\s*(\d+)", "$1 " & Math.Round(splitList(3) / 15).ToString()))

                    Else  '! no es cap de les anteriors

                        '? 'UPGRADE_WARNING: MSComctlLib.Nodes método MenuTreeView.Nodes.Remove tiene un nuevo comportamiento
                        If WorkAround_TreeeviewNodeRemoveString(liniaAtractar, liniesSubstituides) Then Continue For

                        '? 
                        WorkAround_ObreFormulari(liniaAtractar, numLinia, liniesSubstituides)

                        '! la resta
                        AplicaSubstitucions(numLinia, novaLinia, liniesSubstituides, fitxerNET, lstControlsVB6, trobat)
                    End If

                Next

                Dim difFitxer As Double = Now.Subtract(t1Fitxer).Seconds
                Debug.Print(String.Format("---- Temps durada aplicar regles sobre {0} : {1}", fitxerNET, difFitxer.ToString))
                Console.WriteLine("")


                '!**************************************************************
                '! Pel Bug de les inicialitzacions dels ControlArray
                '! Insertem totes les instruccions d'inicialitzacio dels controlArray just despres de
                '! la instruccií InitializeComponent
                '? ES POSSIBLE QUE JA NO CALGUI PERQUE HO IMPLEMENTA EN RUNTIME LA CLASSE CONTROLARRAY!!!!!!!!!!
                ''''''''''If fitxerNET.ToUpper.Contains(".DESIGNER") Then
                ''''''''''    If numLiniaInitializeComponent = -1 Then
                ''''''''''        MessageBox.Show(String.Format("El fitxer {0} no té cap linia InitializeComponent", fitxerNET))
                ''''''''''    Else

                ''''''''''        '! comprovem si ja hem insertat linies d'inicialitzacio per algun altra procés de migració anterior                    
                ''''''''''        Dim rx As Regex = New Regex("\w+= New \w+Array\(New")

                ''''''''''        If rx.Matches(liniesFitxerNET(numLiniaInitializeComponent)).Count = 0 Then

                ''''''''''            '! afegim la inicialització dels controlArry amb els seus "items"
                ''''''''''            For Each inicialitzacio As String In LlistaInicialitzacionsControlArray()

                ''''''''''                liniesSubstituides.Insert(numLiniaInitializeComponent, inicialitzacio)

                ''''''''''                numLiniaInitializeComponent = numLiniaInitializeComponent + 1
                ''''''''''            Next
                ''''''''''        End If
                ''''''''''    End If

                ''''''''''End If
                '!**************************************************************

                '? generem el fitxer nou
                ''''File.WriteAllLines(fitxerNET, liniesSubstituides.ToArray, encoding)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Retorna el <see cref=" controlvb6"/> donat el nom del control
    ''' </summary>
    ''' <param name="nom"></param>
    ''' <param name="lstControlsVB6"></param>
    ''' <returns></returns>
    Private Function ObtenirControlVB6ByName(ByVal nom As String, lstControlsVB6 As List(Of ControlVB6)) As ControlVB6
        Return lstControlsVB6.Where(Function(s) s.nomControl = nom).FirstOrDefault
    End Function

    ''' <summary>
    ''' En el procés de validació, si hem trobat un controlArray no només hem d'afegir la linia d'instanciació del controlArray, sinó que
    ''' aprofitem per aplicar les "regex" d'instanciació del controlArray
    ''' NOTA : tenen diferents expressions regulars depenent del tipus de controlArray
    ''' </summary>
    ''' <param name="texte"></param>
    ''' <returns></returns>
    Private Function AplicaSubstitucioControlArray(ByVal texte As String) As String
        Dim texteSubstituit As String = String.Empty

        '! Instanciacio de controlArray Codejoc
        If texte.Contains("AxXtremeSuiteControls") Then
            Return RegexSubstitucioCadena(texte, "(?i)(\w+ = New )([Ax]*[Gms]*)(\w+Array\(New List\(Of )([AxXtremeSuiteControls]+([\.]*)[Ax]+([Gms]*\w+\)\({[\w+=\(, ]+}\)\)))", "$1Gms$2$3$4")
        End If

        '! Instanciacio de controlArray DataControl
        If texte.ToUpper.Contains("AXDATACONTROL") Then
            Return RegexSubstitucioCadena(texte, "(?i)(\w+ = New )[Ax]*([Gms]*)(\w+Array\(New List\(Of )[AxDataControl]+([\.]*)[Ax]+(([Gms]*\w+\)\({[\w+=\(, ]+}\)\)))", "$1$2$3DataControl$4$5")
        End If

        '! Instanciacio de controlArray AxFlexCell.AxGrid
        If texte.Contains("AxFlexCell.AxGrid") Then
            Return RegexSubstitucioCadena(texte, "(?i)(\w+ = New )[Ax]*((\w+Array\(New List\(Of ))AxFlexCell.AxGrid", "$1$2FlexCell.Grid")
        End If

        '! Instanciacio de controlArray GmsCombo
        If texte.ToUpper.Contains("AXSSDBCOMBO") Then
            Return RegexSubstitucioCadena(texte, "(?i)(\w+ = New )[Ax]*[SSDBCombo]+(\w*Array\(New List\(Of )[AxSSDataWidgets_B\.AxSSDBCombo]+([\.]*)((\w*\)\({[\w+=\(, ]+}\)\)))", "$1GmsCombo$2GmsCombo$3$4")
        End If

        '! Instanciacio de controlArray GmsTime
        If texte.ToUpper.Contains("AXGMSTIME") Then
            Return RegexSubstitucioCadena(texte, "(?i)(\w+ = New )[Ax]*([Gms]*)(\w+Array\(New List\(Of )[AxgmsTime]+[\.]*[Ax]+(([Gms]*\w+\)\({[\w+=\(, ]+}\)\)))", "$1$2$3$4")
        End If

        '! Instanciacio dels "Array de controls"
        Return RegexSubstitucioCadena(texte, "(?i)(Me.\w+ = New )((?!Gms)[Ax]*\w+Array\w*)\(components\)", "$1Gms$2()")
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="srcFile"></param>
    ''' <returns></returns>
    Public Function GetFileEncoding(ByVal srcFile As String) As Encoding
        Dim enc As Encoding = Encoding.GetEncoding(28591)

        Try
            Dim buffer As Byte() = New Byte(4) {}
            Dim file As FileStream = New FileStream(srcFile, FileMode.Open)
            file.Read(buffer, 0, 5)
            file.Close()
            If buffer(0) = 239 AndAlso buffer(1) = 187 AndAlso buffer(2) = 191 Then
                enc = Encoding.UTF8
            ElseIf buffer(0) = 254 AndAlso buffer(1) = 255 Then
                enc = Encoding.Unicode
            ElseIf buffer(0) = 0 AndAlso buffer(1) = 0 AndAlso buffer(2) = 254 AndAlso buffer(3) = 255 Then
                enc = Encoding.UTF32
            ElseIf buffer(0) = 43 AndAlso buffer(1) = 47 AndAlso buffer(2) = 118 Then
                enc = Encoding.UTF7
            End If
        Catch ex As Exception

        End Try

        Return enc
    End Function

    Private Sub CarregaTooltips()
        Dim tooltip As New ToolTip
        tooltip.Active = True
        tooltip.UseAnimation = True
        tooltip.BackColor = Color.LightGreen

        tooltip.ToolTipTitle = "Descripció"

        Dim messageCrearBackups As System.Text.StringBuilder = New System.Text.StringBuilder()
        messageCrearBackups.AppendFormat(" 1.- {0}", "Fa una còpia a BACKUP_NET")

        tooltip.SetToolTip(btnCrearBackups, messageCrearBackups.ToString)

        Dim messageFoto As System.Text.StringBuilder = New System.Text.StringBuilder()
        messageFoto.AppendFormat("1.- {0}", "Només s'ha de fer si la migració NO es fa sobre un projecte d'aplicació")

        tooltip.SetToolTip(btnFoto, messageFoto.ToString)

        Dim messageWizard2008 As System.Text.StringBuilder = New System.Text.StringBuilder()
        messageWizard2008.AppendFormat("1.- {0}", "Primer pas del procés de migració.")

        tooltip.SetToolTip(btnWizard2008, messageWizard2008.ToString)

        Dim messagePostMigracio As System.Text.StringBuilder = New System.Text.StringBuilder()
        messagePostMigracio.AppendFormat("1.- {0}", "Mou els fitxers en les carpetes que toquen.El Wizard2008 no ho fa!")
        messagePostMigracio.AppendLine()
        messagePostMigracio.AppendFormat("2.- {0}", "Còpia automàticament els fitxers resX i Designer en la part NET (producció)")
        messagePostMigracio.AppendLine()

        tooltip.SetToolTip(btnPostMigracio, messagePostMigracio.ToString)

        Dim messageValidacions As System.Text.StringBuilder = New System.Text.StringBuilder()
        messageValidacions.AppendFormat("1.- {0}", "Aplica les regles de substitució sobre tots els fitxers")
        messageValidacions.AppendLine()
        messageValidacions.AppendFormat("2.- {0}", "No s'aplica ni als fitxers resX ni Designer en el cas que el check Backup estigui marcat")
        messageValidacions.AppendLine()
        messageValidacions.AppendFormat("     ORDRE")
        messageValidacions.AppendLine()
        messageValidacions.AppendFormat("         - Validar amb el check Backup checked (migra NOMES els codi/vb)")
        messageValidacions.AppendLine()
        messageValidacions.AppendFormat("         - Validar amb el check Backup unchecked (migra NOMES els designer)")
        messageValidacions.AppendLine()

        tooltip.SetToolTip(btnValidacions, messageValidacions.ToString)

        Dim messageRedisseny As System.Text.StringBuilder = New System.Text.StringBuilder()
        messageRedisseny.AppendFormat("1.- {0}", "Mostra els fitxers que s'han redissenyat (Net 4)")
        messageRedisseny.AppendLine()
        messageRedisseny.AppendFormat("2.- {0}", "Copia automàticament els fitxer resX i Designer de la part NET (producció) a la part NET_BACKUP")
        messageRedisseny.AppendLine()


        Me.TabControl1.ShowToolTips = True

        Dim messageTabpage1 As System.Text.StringBuilder = New System.Text.StringBuilder()
        messageTabpage1.AppendFormat("1.- {0}", "Upgrades : mostra els Upgrades que cal resoldre per projecte (Runtime/Compile)")
        messageTabpage1.AppendLine()
        messageTabpage1.AppendFormat("2.- {0}", "Nom mètode event : requeriment perquè funcioni la gestió d'events (Runtime)")
        messageTabpage1.AppendLine()
        messageTabpage1.AppendFormat("3.- {0}", "TwipsPixels: linies que cal migrar a nivell de conversió Twips/Pixels (Estètica)")
        messageTabpage1.AppendLine()
        messageTabpage1.AppendFormat("4.- {0}", "As Object : possible linies definides com a As Object i que no calen (Runtime)")
        messageTabpage1.AppendLine()
        messageTabpage1.AppendFormat("5.- {0}", "Fonts TrueType  (Rendiment)")
        messageTabpage1.AppendLine()
        messageTabpage1.AppendFormat("6.- {0}", "Treeview/AxGroupBox : ()")
        messageTabpage1.AppendLine()
        messageTabpage1.AppendFormat("7.- {0}", "Forms redisseny : mostra els formularis redissenyats i pendent de redissenyar.De NET 1.0 a NET 4 els desigenr han canviat(Runtime)")
        messageTabpage1.AppendLine()
        messageTabpage1.AppendFormat("8.- {0}", "Arquitectura : resum de tots els canvis existents en un procés de migracio VB6 a NET")
        messageTabpage1.AppendLine()

        tooltip.SetToolTip(TabControl1, messageTabpage1.ToString)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <param name="linia"></param>
    ''' <returns></returns>
    Private Function CoincideixCadena(ByVal cadena As String, ByVal linia As String, ByRef splitList() As String) As Boolean
        Dim patro As String = linia.Split(";")(0)
        Dim esExpressioRegular As Boolean = If(linia.Split(";").Count > 1 AndAlso (linia.Split(";")(1) = "R" OrElse linia.Split(";")(1) = "C"), True, False)

        '! Comprovem si tenim una expressio Regular o no
        If esExpressioRegular Then
            Return RegexCercaCadena(cadena, patro, splitList)
        End If

        '! si no es una "expressio regular"
        Return cadena.Contains(patro)
    End Function

    ''' <summary>
    ''' Extreu el contingut d'una cadena a partir d'un patró
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <param name="patro"></param>
    ''' <returns></returns>
    Private Function RegexSubstring(ByVal cadena As String, ByVal patro As String) As String
        Dim rgx As New Regex(patro, RegexOptions.IgnoreCase)

        For Each g As Group In rgx.Match(cadena).Groups
            For Each c As Capture In g.Captures
                Return c.Value
            Next
        Next

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cadena">Cadena a fer "match"</param>
    ''' <param name="patro">Regex pattern</param>
    ''' <param name="substrings">Retorna la llista</param>
    ''' <returns></returns>
    Private Function RegexCercaCadena(ByVal cadena As String, ByVal patro As String, Optional ByRef substrings() As String = Nothing) As Boolean
        Try
            Dim rgx As New Regex(patro)

            If rgx.IsMatch(cadena) Then
                If substrings IsNot Nothing Then
                    substrings = Regex.Split(cadena, patro)
                End If

                Return True
            End If

            Return False

        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Retorna la cadena substituida a partir d'un patro de cerca i un patro de substitució
    ''' Si no el troba, retorna Empty
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <param name="patro"></param>
    ''' <param name="patroSubstitucio"></param>
    ''' <returns></returns>
    Private Function BuscarISubstituir(ByVal cadena As String, ByVal patro As String, ByVal patroSubstitucio As String) As String
        Dim rgx As New Regex(patro)

        '! si no "matches"
        If rgx.Matches(cadena).Count = 0 Then Return String.Empty

        Return rgx.Replace(cadena, patroSubstitucio)
    End Function

    ''' <summary>
    ''' Substitució d'una cadena amb regex
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <param name="patro"></param>
    ''' <param name="patroSubstitucio"></param>
    ''' <returns></returns>
    Private Function RegexSubstitucioCadena(ByVal cadena As String, ByVal patro As String, ByVal patroSubstitucio As String) As String
        Dim rgx As New Regex(patro)

        '! si no "matches"
        If rgx.Matches(cadena).Count = 0 Then Return cadena

        Return rgx.Replace(cadena, patroSubstitucio)
    End Function

    ''' <summary>
    ''' Retorna si un fitxer se li ha s'aplicar les "validacions"
    ''' </summary>
    ''' <param name="item"></param>
    ''' <returns></returns>
    Private Function EstaEnLlistaNegra(ByVal item As String, ByVal opcional As Boolean) As Boolean
        If item.ToUpper.Contains(".GIT") OrElse
                item.ToUpper.Contains("MY PROJECT") OrElse
                item.ToUpper.Contains("PACKAGES") OrElse
                item.ToUpper.Contains("_CREATENEWNUGETPACKAGE") OrElse
                item.ToUpper.Contains(".VS") OrElse
                item.ToUpper.Contains("AXGMSIMPORTSARRAY") OrElse
                item.ToUpper.Contains("UPGRADESUPPORT") OrElse
                item.ToUpper.Contains("AXGROUPBOXARRAY") OrElse
                item.ToUpper.Contains("AXCHECKBOXARRAY") OrElse
                item.ToUpper.Contains("AXGMSDATAARRAY") OrElse
                item.ToUpper.Contains("AXGMSTEMPSARRAY") OrElse
                item.ToUpper.Contains("AXPUSHBUTTONARRAY") OrElse
                item.ToUpper.Contains("AXRADIOBUTTONARRAY") OrElse
                item.ToUpper.Contains("AXTABCONTROLPAGEARRAY") OrElse
                item.ToUpper.Contains("AXWEBBROWSERARRAY") OrElse
                item.ToUpper.Contains("LINESHAPEARRAY") OrElse
                item.ToUpper.Contains("AXSSDBCOMBOARRAY") OrElse
                item.ToUpper.Contains("_UPGRADEREPORT") OrElse
                item.ToUpper.Contains(".LOG") OrElse
                item.ToUpper.Contains(".SLN") OrElse
                item.ToUpper.Contains(".VBPROJ") OrElse
                item.ToUpper.Contains(".RESX") OrElse
                item.ToUpper.Contains(".SCC") OrElse
                item.ToUpper.Contains(".VBW") OrElse
                item.ToUpper.Contains(".CHM") OrElse
                item.ToUpper.Contains(".FRX") OrElse
                item.ToUpper.Contains(".SUO") OrElse
                item.ToUpper.Contains(".HTM") OrElse
                item.ToUpper.Contains(".ICO") OrElse
                item.ToUpper.Contains("APP.CONFIG") OrElse
                item.ToUpper.Contains("ASSEMBLYINFO") OrElse
                item.ToUpper.Contains(".XML") OrElse
                item.ToUpper.Contains(".BMP") OrElse
                item.ToUpper.Contains(".CDL") OrElse
                item.ToUpper.Contains(".CMD") OrElse
                item.ToUpper.Contains(".BAT") OrElse
                item.ToUpper.Contains(".PPTX") OrElse
                item.ToUpper.Contains(".XPS") OrElse
                item.ToUpper.Contains(".XLSX") OrElse
                item.ToUpper.Contains(".ZIP") OrElse
                item.ToUpper.Contains(".DB") OrElse
                item.ToUpper.Contains(".DLL") OrElse
                item.ToUpper.Contains(".LIB") OrElse
                item.ToUpper.Contains(".VBG") OrElse
                item.ToUpper.Contains(".OCX") OrElse
                item.ToUpper.Contains(".EXP") OrElse
                item.ToUpper.Contains(".OCA") OrElse
                item.ToUpper.Contains(".PAG") OrElse
                item.ToUpper.Contains(".PGX") OrElse
                item.ToUpper.Contains(".CHW") OrElse
                item.ToUpper.Contains(".DB") OrElse
                item.ToUpper.Contains(".CTX") OrElse
                item.ToUpper.Contains(".RAR") OrElse
                item.ToUpper.Contains("\BIN") OrElse
                item.ToUpper.Contains("\OBJ") OrElse
                item.ToUpper.Contains(".GSA") OrElse   '! Rutines Cache
                item.ToUpper.Contains("GMSBOTO") OrElse   '? SEGONS JULI ELIMINAT
                item.ToUpper.Contains("CONTROLBARRA") OrElse   '? SEGONS JULI ELIMINAT
                item.ToUpper.Contains(".DEF") OrElse
                item.ToUpper.Equals("BIN") OrElse
                item.ToUpper.Equals("OBJ") OrElse
                item.ToUpper.Equals("GRAFICS") OrElse     '? CARPETA QUE NO CAL MIGRAR
                item.ToUpper.Equals("MANUALS") OrElse     '? CARPETA QUE NO CAL MIGRAR
                item.ToUpper.Equals("MBD") OrElse     '? CARPETA QUE NO CAL MIGRAR
                item.ToUpper.Equals("MENU DINAMIC CSP") OrElse     '? CARPETA QUE NO CAL MIGRAR
                item.ToUpper.Equals("CAPTURADORS") OrElse     '? CARPETA QUE NO CAL MIGRAR
                item.ToUpper.Equals("DOCS") OrElse     '? CARPETA QUE NO CAL MIGRAR
                item.ToUpper.Equals("FONTS") OrElse     '? CARPETA QUE NO CAL MIGRAR
                item.ToUpper.Equals("CSP") OrElse     '? CARPETA QUE NO CAL MIGRAR                
                item.ToUpper.Equals("MDI.VB") Then  '? ES INTERESSANT PERQUE MODIFICAR PER EXEMPLE MDIFORM PER FORMPARENT

            '''''''''item.ToUpper.Contains(".TXT") OrElse

            '? item.ToUpper.Contains(".VBP") OrElse
            '? item.ToUpper.Contains(".CTL") OrElse

            Return True
        End If

        If item.ToUpper.Contains("DRAG&DROP_TREEVIEW") OrElse
                item.ToUpper.Contains("SAFENATIVEMETHODS") Then

            MessageBox.Show(String.Format("El fitxer {0} , no és migrable", item))

            Return True
        End If


        '! opcionals (especials)
        If opcional AndAlso item.ToUpper.Contains(".DESIGNER") Then
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' Retorna un valor de True/False en funció de si un fitxer es pot esborrar o no
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function EsPotEsborrarFitxer(ByVal fitxer As String) As Boolean
        If fitxer.Contains(FITXER_TRACING_MIGRACIO) OrElse
                fitxer.ToUpper.Contains(".SLN") OrElse
                fitxer.ToUpper.Contains("APP.CONFIG") OrElse
                fitxer.ToUpper.Contains(".GITIGNORE") OrElse
                fitxer.ToUpper.Contains(".SUO") OrElse
                fitxer.ToUpper.Contains(".VBPROJ") OrElse
                fitxer.ToUpper.Contains("MIGRACIO") Then

            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' Retorna un valor de True/False en funció de si una carpeta es pot esborrar o no
    ''' </summary>
    ''' <param name="carpeta"></param>
    ''' <returns></returns>
    Private Function EsPotEsborrarCarpeta(ByVal carpeta As String) As Boolean
        If carpeta.Contains(FITXER_TRACING_MIGRACIO) OrElse
                carpeta.ToUpper.Contains(".GIT") OrElse
                carpeta.ToUpper.Contains(".VS") OrElse
                carpeta.ToUpper.Contains("MY PROJECT") Then

            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' Esborrar els fitxers de totes les subCarpetes a partir d'una carpeta (si inclouCarpeta = True, també s'esborren les subcarpetes)
    ''' </summary>
    ''' <param name="carpeta"></param>
    Public Sub EsborrarDirectori(ByVal carpeta As String, ByVal inclouCarpeta As Boolean, ByVal aplicaFiltres As Boolean)
        Dim carpetaActual As String

        Try
            '! Esborrem els fitxers de la carpeta
            For Each fitxer In Directory.GetFiles(carpeta)
                Dim condicio As Boolean = EsPotEsborrarFitxer(fitxer)
                If condicio OrElse (Not condicio AndAlso Not aplicaFiltres) Then
                    File.SetAttributes(fitxer, FileAttributes.Normal)
                    File.Delete(fitxer)
                End If
            Next

            '! comprovem si estem esborrant una carpeta associada a un projecte (conté VBP) o no
            Dim esProjecteCarpeta As Boolean = If(Directory.GetFiles(cboDirectoryVB6.Text, REGEX_VBP).Count = 0, False, True)

            For Each carpetaActual In My.Computer.FileSystem.GetDirectories(carpeta)

                '! si no es un projecte (exemple : Entigest), comprovem si la subCarpeta es un projecte
                If Not esProjecteCarpeta Then

                    If Not Directory.Exists(carpetaActual.Replace(CARPETA_VB6_BACKUP, CARPETA_VB6)) Then
                        MessageBox.Show(String.Format("La corresponent subcarpeta en BACKUP {0} no existeix en VB6 {1} !!!!", carpetaActual, carpetaActual.Replace(CARPETA_VB6_BACKUP, CARPETA_VB6)))

                        Continue For
                    End If

                    Dim esProjecteSubcarpeta As Boolean = If(Directory.GetFiles(carpetaActual.Replace(CARPETA_VB6_BACKUP, CARPETA_VB6), REGEX_VBP).Count = 0, False, True)

                    '! si la subcarpeta correspon a un projecte (exemple : RRPP), NO hem d'esborrar la subcarpeta
                    If esProjecteSubcarpeta Then
                        Continue For
                    End If
                End If


                Dim condicio As Boolean = EsPotEsborrarCarpeta(carpeta)
                If condicio OrElse (Not condicio AndAlso Not aplicaFiltres) Then

                    '! només podem esborrar si estem migrant TOTS els fitxers del projecte
                    '! si estem migrant NOMES els fitxers VB6 modificats desde, NO HEM D'ESBORRAR
                    If Not chkMigrarNomesModificats.Checked Then
                        EsborrarDirectori(carpetaActual, inclouCarpeta, aplicaFiltres)
                    End If

                    If inclouCarpeta Then
                        Directory.Delete(carpetaActual, True)  '? PODRIA SER QUE ESBORRESSIM EL FITXER "FITXER_TRACING_MIGRACIO" DE SUBCARPETES!!!!!
                    End If
                End If
            Next

        Catch ex As Exception

        End Try

    End Sub

    Public Function CargarSubcarpetas(ByVal rutaRaiz As String, ByVal nodoTree As TreeNode) As Integer
        Dim numSubCarpetes As Integer = 0

        Try
            Dim carpetaActual As String
            Dim indice As Integer

            If nodoTree.Nodes.Count = 0 Then
                nodoTree.Nodes.Add("..")
                nodoTree.LastNode.Tag = ".."
                'nodoTree.LastNode.ImageIndex = 0

                For Each carpetaActual In My.Computer.FileSystem.GetDirectories(rutaRaiz)
                    indice = carpetaActual.LastIndexOf(Path.PathSeparator)
                    Dim item As String = carpetaActual.Substring(indice + 1, carpetaActual.Length - indice - 1).Remove(0, rutaRaiz.Length)

                    If Not EstaEnLlistaNegra(item, False) AndAlso Not EsProjecteObsolet(carpetaActual) AndAlso Not (Not chkInclouProjectesNet_Nous.Checked AndAlso EsProjecteNET_Nou(item)) Then
                        nodoTree.Nodes.Add(item)
                        nodoTree.LastNode.Tag = carpetaActual
                        nodoTree.LastNode.ImageIndex = 0

                        If EsNodeTreeProjecteVB6Obsolet(nodoTree.LastNode.Text) Then
                            nodoTree.LastNode.ForeColor = Color.PaleVioletRed
                        End If

                        numSubCarpetes = numSubCarpetes + 1
                    End If
                Next

            End If

        Catch ex As Exception
            Return 0
        End Try

        Return numSubCarpetes
    End Function

    ''' <summary>
    ''' Carrega els valor de propietats de controls que cal migrar
    ''' </summary>
    ''' <param name="controlVB6"></param>
    ''' <param name="linia"></param>
    Private Sub CargarValorsPropietatsControls(ByRef controlVB6 As ControlVB6, ByVal linia As String)
        Dim splitList() As String = {}

        '! propietat BORDERSTYLE GROUPBOX (CODEJOC, GMSGROUPBOX,....)     dimecres 2
        If RegexCercaCadena(linia, PROPIETAT_BORDERSTYLE & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.borderStyleGroupBox = splitList(1)

            '! propietat Style (exemple TreeView).Es migra a la propietat BorderStyle
        ElseIf RegexCercaCadena(linia, PROPIETAT_APPEARANCE & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.Appearance = ConstruirBorderStyle(splitList(1), controlVB6)

            '! propietat INDENTATION TREEVIEW     dilluns 7
        ElseIf RegexCercaCadena(linia, PROPIETAT_INDENTATION & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.Indentation = splitList(1)

            '! propietat FORMAT_TEMPS/GMSTEMPS
        ElseIf RegexCercaCadena(linia, PROPIETAT_FORMAT_TEMPS & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.FormatTemps = ConstruirFormatTemps(splitList(1))

            '! propietat DECIMALES/GMSIMPORTS
        ElseIf RegexCercaCadena(linia, PROPIETAT_DECIMALES & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.Decimales = splitList(1)

            '! propietat DECIMALES/GMSIMPORTS
        ElseIf RegexCercaCadena(linia, PROPIETAT_MAXLENGTH & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.MaxLength = splitList(1)

            '! propietat SEPARADOR_MILES/GMSIMPORTS
        ElseIf RegexCercaCadena(linia, PROPIETAT_SEPARADORMILES & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.SeparadorMiles = If(splitList(1) = "-1", CONST_TRUE, CONST_FALSE)

            '! propietat CEROS/GMSIMPORTS
        ElseIf RegexCercaCadena(linia, PROPIETAT_OCULTARCEROS & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.OcultarCeros = If(splitList(1) = "-1", CONST_TRUE, CONST_FALSE)

            '! propietat SIGNO/GMSIMPORTS
        ElseIf RegexCercaCadena(linia, PROPIETAT_SIGNE & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.Signe = If(splitList(1) = "-1", CONST_TRUE, CONST_FALSE)

            '! propietat ARRODONIMENT/GMSIMPORTS
        ElseIf RegexCercaCadena(linia, PROPIETAT_ARRODONIMENT & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.Arrodoniment = splitList(1)

            '! propietat COLUMNHEADERS/GMSCOMBO
        ElseIf RegexCercaCadena(linia, PROPIETAT_COLUMNHEADERS & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.ColumnHeaders = If(splitList(1) = "-1", CONST_TRUE, CONST_FALSE)

            '! propietat "noms de columnes d'un gmsCombo"
            '? OBSERVACIO : el nom de les columnes s'accedeix a través de la propietat Caption/VB6
        ElseIf RegexCercaCadena(linia, "Columns\(\d+\)\.Caption\s*=\s*\""([\w\s]+)\""", splitList) Then

            '? *************  IMPORTANTISSIM !!!! 
            '? 2 POSSIBILITATS PERÒ QUE HA D'ANAR CORRESPOST AMB LA CLASSE GMSCOMBO!!!!!!!!!!!!!!!!!!!!!!!!!
            '! SI UTILITZEM LA OPCIO 1 NO CAL IMPLEMENTAR LA 2 , I AL REVÉS!!!!!

            '! OPCIO 1.- Me.cmbModulo.Items.AddRange(New Object() {"NOMCOL1|NOMCOL2|NOMCOL3"})

            'x If controlVB6.ColumnsName Is Nothing Then
            'x   controlVB6.ColumnsName = splitList(1)
            'x Else
            'x    controlVB6.ColumnsName = controlVB6.ColumnsName & "|" & splitList(1)     '? punt feble . tabulador
            'x End If

            '! OPCIO 2.- Me.cmbDispositius.Items.AddRange(New Object() {"NOMCOL1","NOMCOL2", "NOMCOL2"})

            '! SI UTILITZEM LA OPCIO 1 NO CAL IMPLEMENTAR !!!!!

            If controlVB6.ColumnsName Is Nothing Then
                controlVB6.ColumnsName = """" & splitList(1) & """"
            Else
                controlVB6.ColumnsName = controlVB6.ColumnsName & "," & """" & splitList(1) & """"
            End If

            '! propietat DATAFIELDLIST/GMSCOMBO
        ElseIf RegexCercaCadena(linia, PROPIETAT_DATAFIELDLIST & CONST_EQUALS & "\""Column (\d+)\""", splitList) Then

            controlVB6.DataFieldList = splitList(1)

            '! propietat BACKCOLOR
        ElseIf RegexCercaCadena(linia, PROPIETAT_BACKCOLOR & CONST_EQUALS & "([\w\&\-]+)", splitList) Then

            controlVB6.BackColor = splitList(1)


            '! propietat BACKCOLORODD/GMSCOMBO
        ElseIf RegexCercaCadena(linia, PROPIETAT_BACKCOLORODD & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.BackColorOdd = splitList(1)

            '! propietat INDEX (CONTROLARRAY) index d'un element d'un ControlArray.L'index marca l'ordre de l'element dins del controlArray
        ElseIf RegexCercaCadena(linia, "\s+" & PROPIETAT_INDEX & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            '? si té la propietat Index --> es un element d'un ControlArray
            controlVB6.EsElementControlArray = True

            '? assignem l'Index (element d'un controlArray)
            controlVB6.Index = splitList(1)

            '! propietat PAGE (TabPage).Indica el numero de pestanya dins la TabControl (per tan l'ordre de visualització de les pestanyes)
        ElseIf RegexCercaCadena(linia, "\s+" & PROPIETAT_PAGE & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            Dim nomControl As String = controlVB6.nomControl

            '? si estem dins del Begin d'una tabpage i linia = index, recuperem el controlVB6 DE LA TABPAGE
            If liniaTabPageTrobada AndAlso controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_TABPAGE Then

            Else
                '! recuperem el controlVB6 de la colecció
                controlVB6 = lstControlsVB6.Where(Function(S) S.nomControl = nomControl).FirstOrDefault
            End If

            '? assignem la propietat Page
            controlVB6.Page = splitList(1)


            '! propietat BACKCKOLOR GMSTABCONTROL
        ElseIf RegexCercaCadena(linia, PROPIETAT_BACKCOLOR & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.BackColor = splitList(1)


            '! propietat FORECOLOR GMSTABCONTROL
        ElseIf RegexCercaCadena(linia, PROPIETAT_FORECOLOR & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.ForeColor = splitList(1)

            '! propietat ENABLED GMSGROUPBOX,...
        ElseIf RegexCercaCadena(linia, PROPIETAT_ENABLED & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            controlVB6.Enabled = splitList(1)

            '! propietat Alignment/NET (GmsTabControl)
        ElseIf RegexCercaCadena(linia, PROPIETAT_PAINTMANAGER_POSITION & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then
            controlVB6.Alignment = ConstruirAlignmentTabControl(splitList(1))


            '! propietat Height          dijous 28/10 . Sembla que no cal posar
        ElseIf RegexCercaCadena(linia, "\s+" & PROPIETAT_HEIGHT & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then

            '? WORKAROUND : calcular el valor de la propietat AutoSize/NET a partir de la propietat Height/VB6
            '?        -   si Height/Label/VB6 = 255 - -> AutoSize = True
            '?        -   si Height/Label/VB6 <> 255 - -> AutoSize = False
            '?        -   si Height/CheckBox/VB6 = 285 - -> AutoSize = True
            '?        -   si Height/CheckBox/VB6 <> 285 - -> AutoSize = False
            If controlVB6.tipusControl = TIPUSCONTROL_LABEL AndAlso splitList(1) = "255" Then
                controlVB6.AutoSize = CONST_TRUE
            ElseIf controlVB6.tipusControl = TIPUSCONTROL_LABEL AndAlso splitList(1) <> "255" Then
                controlVB6.AutoSize = CONST_FALSE
            ElseIf controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_CHECKBOX AndAlso splitList(1) = "285" Then
                controlVB6.AutoSize = CONST_TRUE
            ElseIf controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_CHECKBOX AndAlso splitList(1) <> "285" Then
                controlVB6.AutoSize = CONST_FALSE
            End If

            'xcontrolVB6.Height = splitList(1)

            '! propietat TAG
        ElseIf RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_TAG & CONST_EQUALS, splitList) Then
            Dim pos0 As Integer = linia.IndexOf("""")
            Dim pos1 As Integer = linia.LastIndexOf("""")

            controlVB6.tag = linia.Substring(pos0, linia.Length - pos0)


            '! propietat BACKSTYLE
        ElseIf RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_BACKSTYLE & CONST_EQUALS & "(\d+)", splitList) Then

            '? dijous 30/6/2022
            If splitList(1) = "0" Then    '? 0 = Transparent
                controlVB6.BackColor = Nothing
            End If


            controlVB6.BackStyle = splitList(1)    '? en VB6, per defecte només existeix la propietat BackStyle quan es transparent

            '! propietat CAPTION dels XtremeSuiteControls.GroupBox
        ElseIf RegexCercaCadena(linia, "\s+" & PROPIETAT_CAPTION & CONST_EQUALS & "(\""[\'\/\.\&\w\s\:,]+\"")", splitList) Then   '? PUNT FEBLE. Cal incloure tots els possibles caracters

            '? CUIDADO : no tots els controls hem de "migrar" la propietat Caption a Text 
            '? exemple : la VB6.Label.Caption, el Wizard2008 ja ho passa a la propietat Text
            If controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_GROUPBOX OrElse controlVB6.tipusControl = TIPUSCONTROL_GMSSETMANA OrElse controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_PUSHBUTTON Then
                controlVB6.caption = splitList(1)
            End If

            '? propietat "Page" d'una TabControlPage
        ElseIf RegexCercaCadena(linia, "\s+" & PROPIETAT_PAGE & REGEX_VALOR_PROPIETAT_CONTROL, splitList) Then
            '! obtenim la tabpage de la llista de tabpageS amb l'index el valor de "Page"
            Dim tabPages As List(Of ControlVB6) = lstControlsVB6.Where(Function(s) s.tipusControl = TIPUSCONTROL_CODEJOC_TABPAGE).ToList

            Dim tabpage As ControlVB6 = tabPages.Item(CInt(splitList(1)))
            controlVB6.Page = splitList(1)


        Else
            If controlVB6 Is Nothing Then Exit Sub


            '! migració propietats del la Font del control
            WorkAround_PropertyFont(linia, controlVB6)

            '! migracio MSComctlLib.Toolbar
            If controlVB6.tipusControl = TIPUSCONTROL_TOOLBAR OrElse controlVB6.tipusControl = TIPUSCONTROL_TOOLSTRIPBUTTON OrElse controlVB6.tipusControl = TIPUSCONTROL_TOOLSTRIPBUTTONS Then
                WorkAround_Toolbar(linia, controlVB6)
            End If


            If controlVB6.tipusControl = TIPUSCONTROL_SSCOMBO Then
                'WorkAround_Properties_Combo(linia, controlVB6)
            End If

            If controlVB6.tipusControl = TIPUSCONTROL_GMSTEMPS Then
                WorkAround_Properties_GmsTemps(linia, controlVB6)
            End If

            If controlVB6.tipusControl = TIPUSCONTROL_FCGRID Then
                WorkAround_Properties_FCGrid(linia, controlVB6)
            End If

            If controlVB6.tipusControl = TIPUSCONTROL_TREEVIEW Then
                WorkAround_Properties_TreeView(linia, controlVB6)
            End If

        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="rutaRaiz"></param>
    Public Sub CargarTreeViewVB6(ByVal rutaRaiz As String)
        Dim nodoBase As TreeNode

        '! Carreguem el combo VB6
        If IO.Directory.Exists(rutaRaiz) Then
            TreeViewVB6.Nodes.Clear()

            If rutaRaiz.Length <= 12 Then
                nodoBase = TreeViewVB6.Nodes.Add(rutaRaiz)
            Else
                nodoBase = TreeViewVB6.Nodes.Add(rutaRaiz)
            End If

            nodoBase.Tag = rutaRaiz
            lblProjectesVB6.Text = CargarSubcarpetas(rutaRaiz, nodoBase)
            TreeViewVB6.Nodes(0).Expand()
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="rutaRaiz"></param>
    Public Sub CargarTreeViewNET(ByVal rutaRaiz As String)
        Dim nodoBase As TreeNode

        '! Carregume el combo .NET
        Dim nomProjecte As String = If(rutaRaiz.Split("\").Count = 3, rutaRaiz.Split("\")(2), "")

        rutaRaiz = rutaRaiz.Replace(CARPETA_VB6, CARPETA_NET)
        If IO.Directory.Exists(rutaRaiz) Then
            cboDirectoryNet.Text = rutaRaiz

            TreeViewNET.Nodes.Clear()

            'Dim nodoBase As TreeNode
            If rutaRaiz.Length <= 12 Then
                nodoBase = TreeViewNET.Nodes.Add(rutaRaiz)
            Else
                nodoBase = TreeViewNET.Nodes.Add(rutaRaiz)
            End If

            nodoBase.Tag = rutaRaiz
            lblProjectesNET.Text = CargarSubcarpetas(rutaRaiz, nodoBase)
            TreeViewNET.Nodes(0).Expand()
        Else '! no existeix el corresponent directori en l'entorn .Net
            TreeViewNET.Nodes.Clear()
        End If
    End Sub

    ''' <summary>
    ''' CarregaTreeView
    ''' </summary>
    ''' <param name="rutaRaiz"></param>
    Public Sub CargarTreeViews(ByVal rutaRaiz As String)
        CargarTreeViewVB6(rutaRaiz)
        CargarTreeViewNET(rutaRaiz)
    End Sub

    Private Function ExisteixPostMigracio() As Boolean
        Return File.Exists(BuscarNomProjecteAmbCami() & "_PostMigracio.vbproj")
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="liniesFitxerVB"></param>
    ''' <returns></returns>
    Private Function ExisteixLinia(ByVal linia As String, ByVal liniesFitxerVB As List(Of String)) As Boolean
        For Each liniat As String In liniesFitxerVB
            If liniat.Trim.ToUpper = linia.Trim.ToUpper Then Return True
        Next

        Return False
    End Function

    ''' <summary>
    ''' WORKAROUNDs : "migra" propietats que el procés de migració no ha migrat
    '''         - Caption (TabControlPage, GroupBox/GmsGroupBox,...)
    '''         - Tag
    '''         - BorderStyle (FlexCell Grid,...)
    '''         - BackColor
    '''         - Font
    '''         - .......
    ''' </summary>
    ''' <param name="lstControlsVB6"></param>
    ''' <param name="linia"></param>
    ''' <param name="nomPropietat"></param>
    ''' <param name="nomPropietatPosterior"></param>
    ''' <returns></returns>
    Private Function AfegeixPropietatNoMigrada(lstControlsVB6 As List(Of ControlVB6), ByVal linia As String, ByVal liniesFitxerVB As List(Of String), ByVal nomPropietat As String, ByVal nomPropietatPosterior As String) As String
        If lstControlsVB6 Is Nothing Then Return Nothing

        '! recorrem tots els controls
        For Each controlVB6 As ControlVB6 In lstControlsVB6
            If controlVB6.nomControl Is Nothing Then
                Dim kk = 1
            End If

            If controlVB6.nomControl IsNot Nothing AndAlso linia.Contains(controlVB6.nomControl & "." & nomPropietatPosterior) Then     '? dimarts 10/1/2023
                Dim cadenaPropietat As String = "." & nomPropietat & " = "

                Dim cadena As String = Nothing

                Select Case nomPropietat
                    Case PROPIETAT_TAG

                        '? hem d'excloure els controls tq no s'ha de "migrar" la propietat TAG (però SI d'altres propietats!)
                        If controlVB6.tipusControl <> TIPUSCONTROL_CODEJOC_TABPAGE AndAlso controlVB6.tipusControl <> TIPUSCONTROL_CODEJOC_GROUPBOX Then
                            '! si no té valor NO hem d'afegir la linia
                            If String.IsNullOrEmpty(controlVB6.tag) Then Return Nothing

                            cadena = CONST_ME & controlVB6.nomControl & cadenaPropietat & controlVB6.tag
                        Else
                            Dim KK = 1
                        End If

                    Case PROPIETAT_CAPTION
                        '! si la propietat Caption no té valor, surtim
                        If String.IsNullOrEmpty(controlVB6.caption) Then
                            ''''Return Nothing          '? dilluns 9/1/2023
                            Continue For
                        End If

                        '? la propietat CAPTION dels AxPushButtons es migren automaticament
                        If controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_PUSHBUTTON Then Return Nothing

                        '! Caption --> Text en Tabpage/Net
                        If controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_TABPAGE Then
                            If controlVB6.nomControl Is Nothing Then
                                '! afegim l'error al listview
                                Dim lvi As New ListViewItem()
                                lvi.SubItems.Add(TipusErrorVB6.CONTROL_PENJA_TABCONTROL.ToString())   ' Tipus
                                '''' lvi.SubItems.Add(fitxerFRM)   ' Fitxer
                                '''' lvi.SubItems.Add(splitList(3))   ' Control
                                lvi.BackColor = Color.Orange

                                lvErrorsMigracio.Items.Add(lvi)

                                Return Nothing
                            End If

                            '! hem de comprovar si ja s'ha afegit en "execucions del procés" anteriors la linia del "CAPTION" per evitar linies duplicades
                            cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_TEXT & " = """ & controlVB6.caption.Trim & """"
                        End If

                        '? Caption --> Text en GroupBox/Net
                        If controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_GROUPBOX Then
                            cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_TEXT & " = " & controlVB6.caption.Trim
                        End If

                        '? Caption --> Caption en GmsSetmana
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSSETMANA Then
                            cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_CAPTION & " = " & controlVB6.caption.Trim
                        End If

                    Case PROPIETAT_BACKCOLOR
                        '! si no té valor, surtim
                        If String.IsNullOrEmpty(controlVB6.BackColor) Then Return Nothing

                        '? casos que cal excloure    '? dimecres 29/06/2022
                        If controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_LISTVIEW Then Return Nothing
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSDATA Then Return Nothing
                        If controlVB6.tipusControl = TIPUSCONTROL_TREEVIEW Then Return Nothing
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSIMPORTS Then Return Nothing
                        If controlVB6.tipusControl = TIPUSCONTROL_COMBOBOX Then Return Nothing

                        cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_BACKCOLOR & " = " & ConversioColor(controlVB6.BackColor)


                    Case PROPIETAT_FORECOLOR

                        '! si no té valor, surtim
                        If String.IsNullOrEmpty(controlVB6.ForeColor) Then Return Nothing

                        cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_FORECOLOR & " = System.Drawing.ColorTranslator.FromOle(" & controlVB6.ForeColor & ")"


                    Case PROPIETAT_ENABLED
                        '! si no té valor, surtim
                        If String.IsNullOrEmpty(controlVB6.Enabled) Then Return Nothing

                        '? conversio
                        Dim valor As String = CONST_TRUE
                        If controlVB6.Enabled = "0" Then valor = CONST_FALSE

                        If controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_GROUPBOX Then
                            cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_ENABLED & " = " & valor
                        Else
                            Dim kkk = 1
                        End If


                    Case PROPIETAT_BORDERSTYLE

                        Select Case controlVB6.tipusControl
                            Case TIPUSCONTROL_CODEJOC_GROUPBOX

                                '? **********************************  dimecres 2
                                '? IMPORTANT : HEM DE SABER SI S'HA MIGRAT A GMSGROUPBOX O S'HA MANTINGUT EN CODEJOC!!!!!!!!!!!!!!!!!!!!!!!!!!
                                Dim valorBorderStyle As String = ConstruirBorderStyleGmsGroupBox(controlVB6)
                                If valorBorderStyle Is Nothing Then Return Nothing

                                cadena = CONST_ME & controlVB6.nomControl & valorBorderStyle

                            Case TIPUSCONTROL_FCGRID
                                cadena = CONST_ME & controlVB6.nomControl & BORDERSTYLE_FLAT_FCGRID


                            Case Else
                                Dim kk = 1

                        End Select

                        '? CUIDADO : EN VB6 EL TIPUS DE CONTROL ES CODEJOC
                        ''''If controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_GROUPBOX Then

                        ''''    '? **********************************  dimecres 2
                        ''''    '? IMPORTANT : HEM DE SABER SI S'HA MIGRAT A GMSGROUPBOX O S'HA MANTINGUT EN CODEJOC!!!!!!!!!!!!!!!!!!!!!!!!!!

                        ''''    cadena = CONST_ME & controlVB6.nomControl & ConstruirBorderStyleGmsGroupBox(controlVB6)
                        ''''End If


                        ''''If controlVB6.tipusControl = TIPUSCONTROL_FCGRID Then
                        ''''    cadena = CONST_ME & controlVB6.nomControl & BORDERSTYLE_FLAT_FCGRID
                        ''''End If


                    Case PROPIETAT_FONT

                        '? exemple : Me.lblWeb.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                        '? exemple :
                        '?    Me.lblWeb.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType(((System.Drawing.FontStyle.Italic Or System.Drawing.FontStyle.Underline) _
                        '?    Or System.Drawing.FontStyle.Strikeout), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))

                        '! si el control no té la propietat Font
                        If controlVB6.FontSize = Nothing AndAlso controlVB6.FCGridDefaultFontSize = Nothing Then
                            Return Nothing
                        End If

                        Dim cadena1 As String = String.Empty

                        '? dimecres 15/12
                        '? cal excloure els controls que no tenen propietats a migrar
                        If controlVB6.tipusControl = TIPUSCONTROL_LABEL Then
                            If String.IsNullOrEmpty(controlVB6.FontName) Then
                                Return Nothing
                            End If
                        End If


                        If controlVB6.tipusControl = TIPUSCONTROL_FCGRID Then
                            cadena1 = String.Format("""{0}"", {1}!, {2}, System.Drawing.GraphicsUnit.Point, CType(0, Byte)", controlVB6.FCGridDefaultFontName, controlVB6.FCGridDefaultFontSize, ConstruirFontStyle(controlVB6))

                            cadena = CONST_ME & controlVB6.nomControl & FONT_CONTROL_FCGRID & "(" & cadena1 & ")"
                        Else
                            cadena1 = String.Format("""{0}"", {1}!, {2}, System.Drawing.GraphicsUnit.Point, CType(0, Byte)", controlVB6.FontName, controlVB6.FontSize, ConstruirFontStyle(controlVB6))

                            cadena = CONST_ME & controlVB6.nomControl & FONT_CONTROL & "(" & cadena1 & ")"
                        End If


                    Case PROPIETAT_PAINTMANAGER_POSITION
                        ' "Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom"

                        If controlVB6.tipusControl = TIPUSCONTROL_CODEJOC_TABCONTROL AndAlso controlVB6.Alignment IsNot Nothing Then
                            cadena = CONST_ME & controlVB6.nomControl & ALIGNMENT_TABCONTROL & controlVB6.Alignment
                        End If

                        '! Format_Temps/GmsTemps
                    Case PROPIETAT_FORMAT_TEMPS
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSTEMPS Then
                            If controlVB6.FormatTemps Is Nothing Then
                                Debug.WriteLine(String.Format("WARNING:: control {0} sense la propietat FormatTemps definida", controlVB6.nomControl))
                                Return Nothing
                            End If

                            cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_FORMAT_TEMPS & " = " & controlVB6.FormatTemps
                        End If

                        '! Decimales/GmsImports
                    Case PROPIETAT_DECIMALES
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSIMPORTS Then
                            If controlVB6.Decimales Is Nothing Then
                                '! Observació : si no té valor, per defecte el valor es 2 (mirar classe GmsImports!!!!!!!)
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_DECIMALES & " = 2"
                            Else
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_DECIMALES & " = " & controlVB6.Decimales
                            End If
                        End If

                    '! MaxLength/GmsImports
                    Case PROPIETAT_MAXLENGTH
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSIMPORTS Then
                            cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_MAXLENGTH & " = " & controlVB6.MaxLength
                        End If


                    '! MaxLength/GmsImports
                    Case PROPIETAT_SEPARADORMILES
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSIMPORTS Then
                            If controlVB6.SeparadorMiles Is Nothing Then
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SEPARADORMILES & " = False"
                            Else
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SEPARADORMILES & " = " & controlVB6.SeparadorMiles
                            End If
                        End If


                    '! Ceros/GmsImports
                    Case PROPIETAT_OCULTARCEROS
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSIMPORTS Then
                            If controlVB6.OcultarCeros Is Nothing Then
                                '! Observació : si no té valor, per defecte el vaor es True (mirar classe GmsImports!!!!!!!)
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_OCULTARCEROS & " = True"
                            Else
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_OCULTARCEROS & " = " & controlVB6.OcultarCeros
                            End If
                        End If


                    '! Signo/GmsImports
                    Case PROPIETAT_SIGNE
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSIMPORTS Then
                            If controlVB6.Signe Is Nothing Then
                                '! Observació : si no té valor, per defecte el vaor es False (mirar classe GmsImports!!!!!!!)
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SIGNE & " = False"
                            Else
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SIGNE & " = " & controlVB6.Signe
                            End If
                        End If

                    '! Signo/GmsImports
                    Case PROPIETAT_ARRODONIMENT
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSIMPORTS Then
                            If controlVB6.Arrodoniment Is Nothing Then
                                '! Observació : si no té valor, per defecte el valor es 0 (mirar classe GmsImports!!!!!!!)
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_ARRODONIMENT & " = 0"
                            Else
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_ARRODONIMENT & " = " & controlVB6.Arrodoniment
                            End If
                        End If


                    '! ColumnHeaders/GmsCombo
                    Case PROPIETAT_COLUMNHEADERS
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSCOMBO Then
                            If controlVB6.ColumnHeaders Is Nothing Then
                                '! Observació : si no té valor, per defecte el valor es True (mirar classe GmsCombo!!!!!!!)
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_COLUMNHEADERS & " = True"
                            Else
                                cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_COLUMNHEADERS & " = " & controlVB6.ColumnHeaders
                            End If
                        End If


                    '! DataFieldList/GmsCombo
                    Case PROPIETAT_DATAFIELDLIST
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSCOMBO Then
                            cadena = CONST_ME & controlVB6.nomControl & "." & PROPIETAT_DISPLAYMEMBER & " = ""Columna" & controlVB6.DataFieldList & """"
                        End If

                    Case PROPIETAT_COLUMNSNAME
                        If controlVB6.ColumnsName IsNot Nothing Then
                            '? *************  IMPORTANTISSIM !!!! 
                            '? 2 POSSIBILITATS PERÒ QUE HA D'ANAR CORRESPOST AMB LA CLASSE GMSCOMBO!!!!!!!!!!!!!!!!!!!!!!!!!

                            '! 1.- Me.cmbModulo.Items.AddRange(New Object() {"NOMCOL1|NOMCOL2|NOMCOL3"})
                            'cadena = CONST_ME & controlVB6.nomControl & "." & "Items.AddRange(New Object() {""" & controlVB6.ColumnsName & """})"

                            '! 2.- Me.cmbDispositius.Items.AddRange({"NOMCOL1","NOMCOL2", "NOMCOL2"})
                            cadena = CONST_ME & controlVB6.nomControl & "." & "Items.AddRange(New Object() {" & controlVB6.ColumnsName & "})"
                        End If


                    '! BackColorOdd/GmsCombo
                    Case PROPIETAT_BACKCOLORODD
                        If controlVB6.tipusControl = TIPUSCONTROL_GMSCOMBO Then
                            If controlVB6.BackColorOdd Is Nothing Then
                                '! Observació : si no té valor, per defecte el valor es ColorTranslator.FromOle(&HE0E0E0&) (mirar classe GmsCombo!!!!!!!)
                                'x Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_BACKCOLORODD & " = ColorTranslator.FromOle(&HE0E0E0&)"    revisar
                            Else
                                'x Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_BACKCOLORODD & " = ColorTranslator.FromOle(" & controlVB6.BackColorOdd & ")"      revisar
                            End If
                        End If

                    Case PROPIETAT_STYLE

                        If controlVB6.tipusControl = TIPUSCONTROL_TREEVIEW Then
                            If controlVB6.TreeViewStyle = 5 Then
                                ' Me.MenuTreeView.ShowPlusMinus = False
                                Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SHOWPLUSMINUS & " = False"
                            End If
                        End If

                    Case PROPIETAT_LINESTYLE    '? Treeview

                        If controlVB6.tipusControl = TIPUSCONTROL_TREEVIEW Then
                            If controlVB6.TreeViewLineStyle = 0 Then   '? TreeLines
                                Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SHOWLINES & " = True" & vbCrLf & CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SHOWROOTLINES & " = False"
                            ElseIf controlVB6.TreeViewLineStyle = 1 Then   '? TreeRootLines
                                Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SHOWROOTLINES & " = True" & vbCrLf & CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SHOWLINES & " = False"
                            Else  '? per defecte es :  TreeLines = 0
                                Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SHOWLINES & " = True" & vbCrLf & CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SHOWROOTLINES & " = False"
                            End If
                        End If

                    Case PROPIETAT_FONTNAME

                        If controlVB6.tipusControl = TIPUSCONTROL_GMSTEMPS Then
                            Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_FONTNAME & " = """ & controlVB6.FontName & """"
                        End If

                    Case PROPIETAT_FONTSIZE

                        If controlVB6.tipusControl = TIPUSCONTROL_GMSTEMPS Then
                            Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_FONTSIZE & " = " & controlVB6.FontSize.Replace(",", ".")   '? decimal , per .
                        End If


                    Case PROPIETAT_DEFAULTROWHEIGHT     '? FC Grid

                        If controlVB6.tipusControl = TIPUSCONTROL_FCGRID Then
                            Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_DEFAULTROWHEIGHT & " = " & controlVB6.FCGridDefaultRowHeight
                        End If

                    Case PROPIETAT_READONLY     '? FC Grid

                        If controlVB6.tipusControl = TIPUSCONTROL_FCGRID AndAlso controlVB6.Enabled IsNot Nothing Then
                            Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_ENABLED & " = " & controlVB6.Enabled
                        End If

                    Case PROPIETAT_SELECTIONMODE     '? FC Grid

                        If controlVB6.tipusControl = TIPUSCONTROL_FCGRID AndAlso controlVB6.SelectionMode IsNot Nothing Then
                            Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_SELECTIONMODE & " = " & controlVB6.SelectionMode
                        End If


                    Case PROPIETAT_BACKSTYLE   '? Label

                        If controlVB6.BackStyle = "0" Then    '?  0 = Transparent 
                            ' Me.lbl1.BackColor = System.Drawing.Color.Transparent
                            Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_BACKCOLOR & " = System.Drawing.Color.Transparent"

                        ElseIf controlVB6.BackStyle = "1" AndAlso controlVB6.BackColor Is Nothing Then   '?  1 = Opac
                            '? si hem "marcat" com "opac" i no té en VB6 (frm) la propietat BackColor per tal de fer-lo "opac" en Net posem el BackColor = System.Drawing.SystemColors.Control
                            Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_BACKCOLOR & " = System.Drawing.SystemColors.Control"
                        End If


                    Case PROPIETAT_AUTOSIZE    '? Label, CheckBox
                        If controlVB6.AutoSize IsNot Nothing Then
                            Return CONST_ME & controlVB6.nomControl & "." & PROPIETAT_AUTOSIZE & " = " & controlVB6.AutoSize
                        End If

                End Select


                '! hem de comprovar si ja s'ha afegit en "execucions del procés" anteriors la linia del "TAG" per evitar linies duplicades
                If cadena Is Nothing OrElse ExisteixLinia(cadena, liniesFitxerVB) Then Return Nothing

                Return cadena

            End If
        Next

        Return Nothing
    End Function

    ''' <summary>
    ''' GmsTemps + Format_Temps
    ''' </summary>
    ''' <param name="valor"></param>
    ''' <returns></returns>
    Private Function ConstruirFormatTemps(ByVal valor As String) As String
        Dim formatTemps As String = String.Empty

        If valor = "0" Then
            formatTemps = "GmsTemps.P_Format_Temps.Minuts"
        ElseIf valor = "1" Then
            formatTemps = "GmsTemps.P_Format_Temps.Segons"
        ElseIf valor = "2" Then
            formatTemps = "GmsTemps.P_Format_Temps.Decimes"
        ElseIf valor = "3" Then
            formatTemps = "GmsTemps.P_Format_Temps.Centesimes"
        ElseIf valor = "4" Then
            formatTemps = "GmsTemps.P_Format_Temps.Milesimes"
        ElseIf valor = "5" Then
            formatTemps = "GmsTemps.P_Format_Temps.MultiHores"
        End If

        Return formatTemps
    End Function

    ''' <summary>
    ''' PaintManager.Position/VB6
    ''' </summary>
    ''' <param name="valor"></param>
    ''' <returns></returns>
    Private Function ConstruirAlignmentTabControl(ByVal valor As String) As String
        Dim alignment As String = String.Empty

        If valor = 0 Then
            alignment = "Top"
        ElseIf valor = 1 Then
            alignment = "Left"
        ElseIf valor = 2 Then
            alignment = "Bottom"
        ElseIf valor = 3 Then
            alignment = "Right"
        End If

        Return alignment
    End Function

    ''' <summary>
    ''' Valor de la propietat BorderStyle per defecte segons el tipus control
    ''' </summary>
    ''' <param name="tipuscontrol"></param>
    ''' <returns></returns>
    Private Function ConstruirBorderStyleDefaultByTipus(ByVal tipuscontrol As String) As String
        Dim borderStyle As String = String.Empty

        If tipuscontrol = TIPUSCONTROL_LABEL Then
            borderStyle = "System.Windows.Forms.BorderStyle.None"
        ElseIf tipuscontrol = TIPUSCONTROL_IMAGE Then
            ''''borderStyle = "System.Windows.Forms.BorderStyle.None"     '? dijous 30/6/2022
        Else
            borderStyle = "System.Windows.Forms.BorderStyle.None"
        End If

        Return borderStyle
    End Function

    ''' <summary>
    ''' WORKAROUND : propietat Appearance/NET no existeix
    ''' La propiedad VB6 Appearance se ha eliminado y su efecto se ha integrado en la propiedad VB.NET BorderStyle, de acuerdo con la siguiente regla: 
    '''     - si BorderStyle es igual a 0-None en VB6, el control no tiene borde y se ignora la propiedad Appearance; 
    '''     - si BorderStyle es igual a 1-FixedSingle y Appearance es igual a 0-Flat en VB6, entonces VB.NET BorderStyle se establece en 1-Flat; 
    '''     - si BorderStyle es igual a 1-FixedSingle y Appearance es igual a 1-ThreeD en VB6, entonces VB.NET BorderStyle se establece en 2-Fixed3D.
    ''' </summary>
    ''' <param name="appearance"></param>
    ''' <returns></returns>
    Private Function ConstruirBorderStyle(ByVal appearance As String, ByVal controlVB6 As ControlVB6) As String
        ''''Dim borderStyle As String = String.Empty

        If appearance = 0 Then

            If controlVB6.BorderStyle = "System.Windows.Forms.BorderStyle.None" Then Return controlVB6.BorderStyle

            Return "System.Windows.Forms.BorderStyle.FixedSingle"
        ElseIf appearance = 1 Then
            Return "System.Windows.Forms.BorderStyle.Fixed3D"
        End If

        Return String.Empty
    End Function

    Private Function ConversioColor(ByVal color As String) As String
        If color = "-2147483638" Then Return "System.Drawing.SystemColors.ActiveBorder"
        If color = "-2147483643" Then Return "System.Drawing.SystemColors.Window"
        If color = "System.Drawing.SystemColors.Control" Then Return color
        If color = "System.Windows.Forms.Control.DefaultBackColor" Then Return color

        '? WORKAROUND : si l'ultim caracter es "&" , cal eliminar-lo
        If color.Last = "&" Then
            Return "System.Drawing.ColorTranslator.FromOle(" & color.Substring(0, color.Length - 1) & ")"
        End If

        Return "System.Drawing.ColorTranslator.FromOle(" & color & ")"
    End Function

    ''' <summary>
    ''' Construeix la propietat BorderStyle GmsGroupBox
    ''' </summary>
    ''' <param name="controlVB6"></param>
    ''' <returns></returns>
    Private Function ConstruirBorderStyleGmsGroupBox(ByVal controlVB6 As ControlVB6) As String
        Dim borderStyle As String = String.Empty
        '? si el control es de tipus GmsGroupBox, hem definit l'Enum seguent :
        '?      FrameBorder = 0
        '?      FrameSingleLine = 1
        '?      FrameNone = 2
        If controlVB6.borderStyleGroupBox = 0 Then
            ' borderStyle = ".BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameBorder"
            '? per defecte la classe GmsGroupBox té la propietat Borderstyle amb el valor  <DefaultValue(EBorderstyle.FrameBorder)>
            '? Això implica que no cal afegir la linia
            Return Nothing
        ElseIf controlVB6.borderStyleGroupBox = 1 Then
            borderStyle = ".BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameSingleLine"
        ElseIf controlVB6.borderStyleGroupBox = 2 Then
            borderStyle = ".BorderStyle = DataControl.GmsGroupBox.EBorderstyle.FrameNone"
        End If

        Return borderStyle
    End Function

    ''' <summary>
    ''' construeix la linia New d'un control ToolStrip
    ''' </summary>
    ''' <param name="controlVB6"></param>
    ''' <returns></returns>
    Private Function ConstruirToolStripButton_New(ByVal controlVB6 As ControlVB6) As String
        '? ToolStripComboBox
        '? ToolStripContentPanel
        '? ToolStripControlHost
        '? ToolStripDropDown
        '? ToolStripDropDownDirection
        '? ToolStripDropDownButton
        '? ToolStripDropDownItem
        '? ToolStripDropDownMenu
        '? ToolStripGripDisplayStyle
        '? ToolStripItem
        '? ToolStripItemAlignment
        '? ToolStripItemCollection
        '? ToolStripItemDisplayStyle
        '? ToolStripItemImageScaling
        '? ToolStripItemOverflow
        '? ToolStripItemPlacement
        '? ToolStripLabel
        '? ToolStripLayoutStyle
        '? ToolStripManager
        '? ToolStripMenuItem
        '? ToolStripOverflow
        '? ToolStripOverflowButton
        '? ToolStripPanel
        '? ToolStripPanelRow
        '? ToolStripProgressBar
        '? ToolStripRenderer
        '? ToolStripRenderMode
        '? ToolStripSplitButton
        '? ToolStripStatusLabel
        '? ToolStripTextBox

        '? IMPORTANT : cal comprovar que en un procés de migració/validacio no es dupliquin les linies New
        If ConteLiniaToolbar(controlVB6.nomControl, TIPUSLINIA_TOOLBAR_NEW) Then Return String.Empty


        '! casos segons el tipus de ToolStrip
        If controlVB6.ButtonToolbarStyle = "0" Then
        ElseIf controlVB6.ButtonToolbarStyle = "1" Then
            '? en WinForms el tbrCheck/VB6 s'ha de convertir en un ToolStripButton
            '? mirar : https://stackoverflow.com/questions/43802743/unable-to-add-checkbox-to-toolstrip
            Return CONST_ME & controlVB6.nomControl & " = New System.Windows.Forms." & TIPUSCONTROL_TOOLSTRIPBUTTON & "()"

        ElseIf controlVB6.ButtonToolbarStyle = "2" Then   '!  System.Windows.Forms.ToolStripButton
            Return CONST_ME & controlVB6.nomControl & " = New System.Windows.Forms." & TIPUSCONTROL_TOOLSTRIPBUTTON & "()"

        ElseIf controlVB6.ButtonToolbarStyle = "3" Then   '!  System.Windows.Forms.ToolStripSeparator
            '? no s'ha de tractar perque el Wizard2008 ja el migra

        ElseIf controlVB6.ButtonToolbarStyle = "4" Then  '? PlaceHolder/VB6
            '? el Wizard2008 migra el tipus PlaceHolder a Separator. No cal fer res

        Else  '! altres tipus de ToolStrip
            Return CONST_ME & controlVB6.nomControl & " = New System.Windows.Forms." & TIPUSCONTROL_TOOLSTRIPBUTTON & "()"
        End If

        Return String.Empty
    End Function

    ''' <summary>
    ''' construeix la linia WithEevents d'un control ToolStrip
    ''' </summary>
    ''' <param name="controlVB6"></param>
    ''' <returns></returns>
    Private Function ConstruirToolStripButton_WithEvents(ByVal controlVB6 As ControlVB6) As String

        '? IMPORTANT : cal comprovar que en un procés de migració/validacio no es dupliquin les linies WithEvents
        If ConteLiniaToolbar(controlVB6.nomControl, TIPUSLINIA_TOOLBAR_WITHEVENTS) Then Return String.Empty



        '? exemple :   Friend WithEvents _Toolbar1_Button1 As System.Windows.Forms.ToolStripSeparator
        '! casos segons el tipus de ToolStrip
        If controlVB6.ButtonToolbarStyle = "0" Then


        ElseIf controlVB6.ButtonToolbarStyle = "1" Then
            '? en WinForms el tbrCheck/VB6 s'ha de convertir en un ToolStripButton
            '? mirar : https://stackoverflow.com/questions/43802743/unable-to-add-checkbox-to-toolstrip
            Return "Friend WithEvents " & controlVB6.nomControl & " As " & TIPUSCONTROL_TOOLSTRIPBUTTON    '? es probable que no calgui

        ElseIf controlVB6.ButtonToolbarStyle = "2" Then    '!  System.Windows.Forms.ToolStripButton
            Return "Friend WithEvents " & controlVB6.nomControl & " As " & TIPUSCONTROL_TOOLSTRIPBUTTON     '? es probable que no calgui

        ElseIf controlVB6.ButtonToolbarStyle = "3" Then   '!  System.Windows.Forms.ToolStripSeparator

        ElseIf controlVB6.ButtonToolbarStyle = "4" Then
            '? el Wizard2008 migra el tipus PlaceHolder a Separator. No cal fer res

        Else   '! altres tipus de ToolStrip o bé controlVB6.ButtonToolbarStyle is Nothing
            Return "Friend WithEvents " & controlVB6.nomControl & " As " & TIPUSCONTROL_TOOLSTRIPBUTTON     '? es probable que no calgui
        End If

        Return String.Empty
    End Function

    ''' <summary>
    ''' Construeix la propietat FontStyle
    ''' </summary>
    ''' <param name="controlVB6"></param>
    ''' <returns></returns>
    Private Function ConstruirFontStyle(ByVal controlVB6 As ControlVB6) As String
        Dim fontStyle As String = String.Empty

        '! Bold (equivalent a Weight/VB6)
        If controlVB6.FontWeight = "700" Then    '! 700 = Bold
            If String.IsNullOrEmpty(fontStyle) Then
                fontStyle = "CType(" & "System.Drawing.FontStyle.Bold"
            Else
                fontStyle = fontStyle & " Or " & "System.Drawing.FontStyle.Bold"
            End If
        End If

        ''''If controlVB6.FontWeight = "400" Then    '! 400 = Normal
        ''''    controlVB6.FontSize = "14.25"
        ''''End If

        '! Italic
        If controlVB6.FontItalic = "1" Then   '! 0 = False
            If String.IsNullOrEmpty(fontStyle) Then
                fontStyle = "CType(" & "System.Drawing.FontStyle.Italic"
            Else
                fontStyle = fontStyle & " Or " & "System.Drawing.FontStyle.Italic"
            End If
        End If

        '! Underline
        If controlVB6.FontUnderline = "1" Then    '! 0 = False
            If String.IsNullOrEmpty(fontStyle) Then
                fontStyle = "CType(" & "System.Drawing.FontStyle.Underline"
            Else
                fontStyle = fontStyle & " Or " & "System.Drawing.FontStyle.Underline"
            End If
        End If

        '! Strikethrough
        If controlVB6.FontStrikethrough = "1" Then    '! 0 = False
            If String.IsNullOrEmpty(fontStyle) Then
                fontStyle = "CType(" & "System.Drawing.FontStyle.Strikeout"
            Else
                fontStyle = fontStyle & " Or " & "System.Drawing.FontStyle.Strikeout"
            End If
        End If

        '! defecte
        If String.IsNullOrEmpty(fontStyle) Then Return "System.Drawing.FontStyle.Regular"


        '? MS Sans Serif/VB6   -->    Microsoft Sans Serif/NET
        fontStyle = fontStyle.Replace("MS Sans Serif", "Microsoft Sans Serif")


        Return fontStyle & ", System.Drawing.FontStyle)"
    End Function

    ''' <summary>
    ''' WORKAROUND : un TreeView din un AxGroupBox en un formulari MDIChild, posa el backcolor com el del MDI
    ''' Solució : substiutir el AxGroupBox per Panel
    ''' </summary>
    ''' <param name="lstControls"></param>
    ''' <returns></returns>
    Private Function WorkAround_TreeViewDinsAxGroupBox(ByVal lstControls As List(Of ControlVB6), ByVal fitxerDesigner As String)
        If lstControls.Count = 0 Then Return False

        For Each ctrl In lstControls

            '! condicio
            If ctrl.tipusControl = TIPUSCONTROL_TREEVIEW AndAlso ctrl.tipusControlPare = TIPUSCONTROL_CODEJOC_GROUPBOX Then

                Dim lvItem As New ListViewItem()

                With lvItem
                    .BackColor = Color.CadetBlue

                    .SubItems.Add(fitxerDesigner)
                    .SubItems.Add(ctrl.nomControlPare)
                    .SubItems.Add(ctrl.nomControl)
                End With

                lviTreeViewAxGroupBox.Items.Add(lvItem)
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' 'UPGRADE_WARNING: MSComctlLib.Nodes método MenuTreeView.Nodes.Remove tiene un nuevo comportamiento
    ''' </summary>
    Private Function WorkAround_TreeeviewNodeRemoveString(ByVal linia As String, ByRef liniesSubstituides As List(Of String)) As Boolean
        Dim splitList() As String = {}
        Dim patroCerca As String = "([\w\(\)]+)\.Nodes\.RemoveAt\(\""(\w+)\""\)"

        '! comprovem si es una linia del tipus Nodes.RemoveAt("xxx")
        If RegexCercaCadena(linia, patroCerca, splitList) Then

            '? construim amb LINQ la instrucció que ens calcula l'index d'un node a partir d'una condició
            Dim resultat As String = RegexSubstitucioCadena(linia, patroCerca, "Dim indexCalculat As Integer = $1.Nodes.Select(Function(o, indx) New With {.Widget = o, .Index = indx}).FirstOrDefault(Function(item) item.Widget.Name = ""$2"").Index" & LINIA_MIGRADA_REMOVEAT)

            '? afegim la linia que calcula l'index del RemoveAt
            liniesSubstituides.Add(resultat)

            '? substituim la linia passant com a paràmetre del RemoveAt l'index (indexCalculat)
            Dim resultat2 As String = RegexSubstitucioCadena(linia, patroCerca, "$1.Nodes.RemoveAt(indexCalculat)")
            liniesSubstituides.Add(resultat2)

            Return True   '! no cal processar més
        End If

        Return False
    End Function

    ''' <summary>
    ''' WORKAROUNDS REL.LACIONATS AMB EL LOAD
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <param name="arrText"></param>
    Private Sub WorkAround_FormLoad(ByVal fitxer As String, ByRef arrText As List(Of String))
        For i As Integer = 0 To arrText.Count - 1

            '!
            WorkAround_FormLoad_Assignacio_Propietats_Raise_Resize(fitxer, arrText(i), i)

            '!
            WorkAround_FormLoad_With_Show(fitxer, arrText(i), i)
        Next
    End Sub

    ''' <summary>
    ''' WORKAROUND : Hi ha determinades propietats dels formularis (exemple : Text, Font,...) que PODEN PROVOCAR un raise de l'event Resize del formulari
    ''' Exemple : si en temps de Runtime assignem un nou valor de la propietat Text (difererent al valor en Design!!)
    ''' Mirar frmExecutaProcesClase_Load
    ''' </summary>
    ''' <param name="fitxer"></param>
    Private Sub WorkAround_FormLoad_Assignacio_Propietats_Raise_Resize(ByVal fitxer As String, ByVal linia As String, ByVal numliniaWith As Integer)
        Dim splitList() As String = {}

        '! comprovem si es una linia del tipus : With <formulari>
        If RegexCercaCadena(linia, "(?i)Private Sub \w+_Load\(", splitList) Then
            loadTrobat = True
        End If

        '! si estem dins del With i trobem un Show, teòricament no hem de fer res sempre i quan el Show estigui just després de l'última assignació 
        '! d'una propietat d'un control dins el With
        If loadTrobat Then
            If RegexCercaCadena(linia, REGEX_INICI_LINIA_BLANC & REGEX_CONST_ME & PROPIETAT_FONT & " =", splitList) OrElse RegexCercaCadena(linia, REGEX_INICI_LINIA_BLANC & REGEX_CONST_ME & PROPIETAT_TEXT & " =", splitList) Then
                propietatTrobada = True
            End If
        End If

        '! si estem dins el With <formulari> i trobem el corresponet End With
        If loadTrobat AndAlso RegexCercaCadena(linia, REGEX_INICI_LINIA_BLANC & "End Sub", splitList) Then
            loadTrobat = False

            '? hem trobat alguna de les instruccions seguents :  Me.Text = , Me.Font = ,.... entre el Load i el End Sub
            If propietatTrobada Then

                '! afegim a la listView
                Dim lvItem As New ListViewItem()
                lvItem.SubItems.Add(fitxer)
                lvItem.SubItems.Add(numliniaWith)
                lvItem.SubItems.Add("Assignació propietats Formulari en Load provoquen Resize")

                lvItem.BackColor = Color.LightPink

                lvFormLoad.Items.Add(lvItem)
            End If

            '! re-inicialitzo
            propietatTrobada = False
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    Private Sub WorkAround_ObreFormulari(ByVal linia As String, ByVal numlinia As Integer, ByRef noveslinies As List(Of String))
        Dim splitList() As String = {}

        '! comprovem si es una linia del tipus "ObreFormulari(
        If RegexCercaCadena(linia, "ObreFormulari\((\w+)", splitList) Then

            If noveslinies.Count < numlinia Then
                Debug.WriteLine(String.Format("WARNING::WorkAround_ObreFormulari"))
                Exit Sub
            End If

            '! comprovem si la linia anterior conté, evitar duplicar si executem varies vegades el procés de migració
            If Not RegexCercaCadena(noveslinies(numlinia - 1), LINIA_MIGRADA_OBREFORMULARI) Then

                '! hi ha casos on el primer paramentre de la funcio "ObreFormulari" és Nothing
                If splitList(1) <> "Nothing" Then
                    '? afegim la linia que corregeix el WorkAround.Exemple : Dim frmCalendariComensals As Object = Nothing
                    noveslinies.Add("Dim " & splitList(1) & " As Object = Nothing" & LINIA_MIGRADA_OBREFORMULARI)
                End If
            End If

        End If
    End Sub

    ''' <summary>
    ''' Mirar : https://docs.mobilize.net/vbuc/upgrade-options/code-conversion - Form Load
    ''' </summary>
    ''' <param name="fitxer"></param>
    Private Sub WorkAround_FormLoad_With_Show(ByVal fitxer As String, ByVal linia As String, ByVal numliniaWith As Integer)
        Dim splitList() As String = {}

        '! comprovem si es una linia del tipus : With <formulari>
        If RegexCercaCadena(linia, REGEX_INICI_LINIA_BLANC & "With frm\w+", splitList) OrElse RegexCercaCadena(linia, REGEX_INICI_LINIA_BLANC & "With form\w+", splitList) Then
            withTrobat = True
        End If

        '! si estem dins del With i trobem un Show, teòricament no hem de fer res sempre i quan el Show estigui just després de l'última assignació 
        '! d'una propietat d'un control dins el With
        If withTrobat Then
            If RegexCercaCadena(linia, "\.Show", splitList) Then
                showTrobat = True
            End If
        End If

        '! si estem dins el With <formulari> i trobem el corresponet End With
        If withTrobat AndAlso RegexCercaCadena(linia, REGEX_INICI_LINIA_BLANC & "End With", splitList) Then
            withTrobat = False

            '? no hem trobat cap show entre el With i el End With
            '? WORKAROUND : VB6 versus .NET . Per defecte el .NET no executa el el mètode Load fins que explícitament no es faci un Show del formulari.
            '? En canvi en VB6, quan es fa una assignació d'una propietat d'un dels controls del formulari s'executa el Load
            If Not showTrobat Then

                '! afegim a la listView
                Dim lvItem As New ListViewItem()
                lvItem.SubItems.Add(fitxer)
                lvItem.SubItems.Add(numliniaWith)
                lvItem.SubItems.Add("Show dins With després de la última ....")

                lvItem.BackColor = Color.IndianRed

                lvFormLoad.Items.Add(lvItem)
            End If

            '! re-inicialitzo
            showTrobat = False
        End If
    End Sub

    ''' <summary>
    ''' WORKAROUND : fuentes truetype compatible
    ''' Problema : controls contenidor ActiveX (exemple : AxGroupBox) amb controls .Net
    ''' Motiu : el que cal fer es :
    '''         - Obtenir els Controls ActiveX (exemple Codejock)
    '''         - Comprovar si hi ha Controls .NET (exemple : textBox, Label,..) dins el control ActiveX (exemple: AxGroupBox + Textbox/Label/...)
    ''' Solució : obrir els formulari que tenen controls ActiveX amb controls NET dins i editar la propietat Font del control ActiveX
    ''' </summary>
    Private Function WorkAround_FontsTrueType(ByVal liniesFitxerVB As List(Of String), ByVal fitxerDESIGNER As String) As Boolean
        Dim lstControlsActiveXVB6 As New List(Of ControlVB6)
        Dim splitList() As String = {}

        Dim controlVB6 As New ControlVB6()

        For i As Integer = 0 To liniesFitxerVB.Count - 1

            If RegexCercaCadena(liniesFitxerVB(i), "Attribute\sVB_Name\s=\s", splitList) Then
                Exit For
            End If

            '! busquem els controls Codejoc (o en seu cas controls ActiveX!) que té el formulari
            If RegexCercaCadena(liniesFitxerVB(i), "Begin\s(XtremeSuiteControls\.GroupBox)\s(\w+)", splitList) Then
                controlVB6.tipusControl = splitList(1).Trim
                controlVB6.nomControl = splitList(2).Trim

                lstControlsActiveXVB6.Add(controlVB6)
            End If
        Next

        If lstControlsActiveXVB6.Count = 0 Then Return False

        Dim liniesFitxerDesigner As List(Of String) = LlegeixFitxer(fitxerDESIGNER, encoding)

        '! 2. comprovem si hi ha controls NET dins de controls ActiveX  
        '!    Me.Frame1.Controls.Add(Me.Label1)

        '! mirem realment si hi ha algun control NET dins un control ActiveX
        For Each ctrlAX In lstControlsActiveXVB6

            Dim cadenaCerca As String = REGEX_CONST_ME & ctrlAX.nomControl & "\.Controls\.Add\([Me\.]*(\w+)\)"

            For Each linia As String In liniesFitxerDesigner
                Dim splitlistControlNET() As String = {}

                '! l'hem trobat i per tant cal afegir a la listview (I PER CADA CONTROL ACTIVEX!!!)
                If RegexCercaCadena(linia, cadenaCerca, splitlistControlNET) Then

                    Dim lvItem As New ListViewItem()
                    lvItem.SubItems.Add(fitxerDESIGNER)
                    lvItem.SubItems.Add(ctrlAX.nomControl)
                    lvItem.SubItems.Add(splitlistControlNET(1))
                    'lvItem.SubItems.Add("OPCIO_1:Modificar la propietat Font d'aquest control ActiveX : 9pt , OPCIO_2 :Modificar la propietat Font de cada un dels controls .NET dins aquest control ActiveX")

                    lviFontsTrueType.Items.Add(lvItem)
                End If
            Next
        Next

        Return False
    End Function

    ''' <summary>
    ''' WORKAROUND : fuentes truetype compatible
    ''' Problema : controls contenidor ActiveX (exemple : AxGroupBox) amb controls .Net
    ''' Motiu : el que cal fer es :
    '''         - Obtenir els Controls ActiveX (exemple Codejock)
    '''         - Comprovar si hi ha Controls .NET (exemple : textBox, Label,..) dins el control ActiveX (exemple: AxGroupBox + Textbox/Label/...)
    ''' Solució : obrir els formulari que tenen controls ActiveX amb controls NET dins i editar la propietat Font del control ActiveX
    ''' 
    ''' Observacio : aquesta versió del mètode només utilitza el fitxer FRM
    ''' </summary>
    Private Function WorkAround_FontsTrueType_v2(ByVal lstControls As List(Of ControlVB6), ByVal fitxerDesigner As String)
        If lstControls.Count = 0 Then Return False

        For Each ctrl In lstControls

            '! condicio
            If ctrl.tipusControlPare = TIPUSCONTROL_CODEJOC_GROUPBOX Then

                Dim lvItem As New ListViewItem()

                With lvItem
                    .BackColor = Color.CadetBlue

                    .SubItems.Add(fitxerDesigner)
                    .SubItems.Add(ctrl.nomControlPare)
                    .SubItems.Add(ctrl.nomControl)

                    .SubItems.Add("OPCIO_1:Modificar la propietat Font d'aquest control ActiveX : 9pt , OPCIO_2 :Modificar la propietat Font de cada un dels controls .NET dins aquest control ActiveX")
                End With

                lviFontsTrueType.Items.Add(lvItem)
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' Retorna els controls NET a partir del fitxer Designer
    ''' </summary>
    ''' <param name="fitxerDesigner"></param>
    ''' <returns></returns>
    Private Function ObtenirControlsByDesigner(ByVal fitxerDesigner As String) As List(Of ControlNET)
        If Not File.Exists(fitxerDesigner) Then Return New List(Of ControlNET)

        Dim liniesFitxerDesigner As List(Of String) = LlegeixFitxer(fitxerDesigner, encoding)
        Dim splitList() As String = {}
        lstControlsNET = New List(Of ControlNET)

        For i As Integer = 0 To liniesFitxerDesigner.Count - 1
            If RegexCercaCadena(liniesFitxerDesigner(i), "Public WithEvents (\w+) As (([\w+-\.])*)", splitList) Then
                Dim controlNET As New ControlNET()

                controlNET.nomControl = splitList(1)
                controlNET.tipusControl = splitList(2)

                lstControlsNET.Add(controlNET)
            End If
        Next

        Return lstControlsNET
    End Function

    ''' <summary>
    ''' Elimina les linies OCXState a partir del tipus de control
    ''' </summary>
    ''' <param name="fitxerDesigner"></param>
    'Private Sub EliminarOCXstate(ByVal fitxerDesigner As String)
    '    ObtenirControlsByDesigner(fitxerDesigner)
    'End Sub

    '''' <summary>
    '''' Elimina les linies BeginInit/EndInit a partir del tipus de control
    '''' </summary>
    '''' <param name="fitxerDesigner"></param>
    'Private Sub EliminarBeginInitEndInit(ByVal fitxerDesigner As String)
    '    ObtenirControdlsByDesigner(fitxerDesigner)
    'End Sub

    ''' <summary>
    ''' Aquest mètode s'ha fet perquè hem detectat algun formulari FRM mal construit (exemple: frmMailRebut), on hi havia definit 2 TabControlPage però en
    ''' canvi la propietat ItemCount = 1
    ''' </summary>
    Private Sub PostProcesTabControl()

        Dim tabPages As List(Of ControlVB6) = lstControlsVB6.Where(Function(s) s.tipusControl = TIPUSCONTROL_CODEJOC_TABPAGE).ToList

        For Each t As ControlVB6 In tabPages
            '! busquem la tabcontrol de la tabPageControl
            Dim tabControl As ControlVB6 = lstControlsVB6.Where(Function(s) s.nomControl = t.nomControlPare).FirstOrDefault

            If tabControl.ItemCount <= t.Page Then
                '? cas especial : frmMailRebut
                Dim jj = 1
            End If

        Next

    End Sub

    ''' <summary>
    ''' Processa linies que corresponen a una TabControl i/o TabControlPage
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="controlVB6"></param>
    ''' <returns></returns>
    Private Function ProcessaTabControl_TabControlPage(ByVal fitxerFRM As String, ByVal linia As String, ByRef controlVB6 As ControlVB6) As Boolean
        Dim splitList() As String = {}

        '? ********* Observació : la linia que defineix la propietat Caption d'una tabControlPage "marca" l'inici d'una TabControlPage/VB6
        If RegexCercaCadena(linia, "Item\(" & REGEX_NUM & "\)\." & PROPIETAT_CAPTION & "\s*\=\s*\""([\&\w\s\-\/\.\(\)]+)\""", splitList) Then
            '! creem un nou control
            controlVB6 = New ControlVB6()

            With controlVB6
                .caption = splitList(1)
                .tipusControl = TIPUSCONTROL_CODEJOC_TABPAGE
            End With

            '! afegim el control a la llista
            lstControlsVB6.Add(controlVB6)

            Return True   '? hem processat una linia d'una tabControl o TabPage.No cal seguir processant


        ElseIf RegexCercaCadena(linia, "(?i)item\(\d+\)\.ControlCount\s*=\s*(\d+)", splitList) Then
            trobatTabpage = False  '! inicialitzem el processament dels controls (controlCount) que pot tenir en VB6 una TabPage


            '! controls d'una TabPage
        ElseIf RegexCercaCadena(linia, "Item\((" & REGEX_NUM & ")\)\.Control\((\d+)\)\s*\=\s*\""(\w+\(*\d*\)*)\""", splitList) Then

            If splitList(3).Contains("TabControlPage") Then      '? dijous 5/1/2023

                '? CAS ESPECIAL : una tabPage que "penja" directament d'una tabcontrol i NO es el "primer". Exemple : frmPersonalGen (CET)
                If trobatTabpage Then
                    '! afegim l'error al listview
                    Dim lvi As New ListViewItem()
                    lvi.SubItems.Add(TipusErrorVB6.CONTROL_PENJA_TABCONTROL.ToString())   ' Tipus
                    lvi.SubItems.Add(fitxerFRM)   ' Fitxer
                    lvi.SubItems.Add(splitList(3))   ' Control
                    lvi.BackColor = Color.Orange

                    lvErrorsMigracio.Items.Add(lvi)

                    Return True
                End If

                '? "marquem" conforme hem trobat la tabpage
                trobatTabpage = True

                '! modifiquem propietats
                controlVB6.Index = indexControlTabControl
                controlVB6.nomControl = RegexSubstitucioCadena(splitList(3), "(\w+)\((\d+)\)", "_$1_$2")

                indexControlTabControl = indexControlTabControl + 1
            Else

                '! afegim l'error al listview
                Dim lvi As New ListViewItem()
                lvi.SubItems.Add(TipusErrorVB6.CONTROL_PENJA_TABCONTROL.ToString())   ' Tipus
                lvi.SubItems.Add(fitxerFRM)   ' Fitxer
                lvi.SubItems.Add(splitList(3))   ' Control
                lvi.BackColor = Color.Orange

                lvErrorsMigracio.Items.Add(lvi)

                Return True
            End If

            '? comprovem si és un element d'un control Array. Exemple :    Item(0).Control(0)=   "TabControlPage(0)"
            If RegexCercaCadena(splitList(3), "(\w+)\(\d+\)") Then
                controlVB6.EsElementControlArray = True
                controlVB6.nomControl = RegexSubstitucioCadena(splitList(3), "(\w+)\((\d+)\)", "_$1_$2")   '? dijous 5/1/2023
            End If

            Return True   '? hem processat una linia d'una tabControl o TabPage.No cal seguir processant

            '! propietat ItemCount d'una TabControl
        ElseIf RegexCercaCadena(linia, "\s+" & PROPIETAT_ITEMCOUNT & "\s+\=\s+(\d+)", splitList) Then
            controlVB6.ItemCount = splitList(1)

            Return True   '? hem processat una linia d'una tabControl o TabPage.No cal seguir processant


            '? fi de la definició d'una TabPageControl
        ElseIf liniaTabPageTrobada AndAlso RegexCercaCadena(linia, LINIA_END, splitList) Then
            If qua.Count = 0 Then
                MessageBox.Show("Detectat error/S en el fitxer FRM que estem migrant.Possible/S definicio/ns incorrectes en el formulari en VB6", "Detecció warnings", MessageBoxButtons.OK)
                Return True
            End If

            If qua.Item(qua.Count - 1).tipusControl = TIPUSCONTROL_CODEJOC_TABPAGE Then
                liniaTabPageTrobada = False
                Debug.WriteLine(String.Format("========>> QUA.REMOVE : {0}", qua.Item(qua.Count - 1).nomControl))

                '! eliminem el propi control
                qua.RemoveAt(qua.Count - 1)

                Return True   '? hem processat una linia d'una tabControl o TabPage.No cal seguir processant
            End If


            '? fi de la definició d'un TabControl
        ElseIf liniaTabControlTrobada AndAlso RegexCercaCadena(linia, LINIA_END, splitList) Then

            If qua.Item(qua.Count - 1).tipusControl = TIPUSCONTROL_CODEJOC_TABCONTROL Then
                liniaTabControlTrobada = False

                Debug.WriteLine(String.Format("========>> QUA.REMOVE : {0}", qua.Item(qua.Count - 1).nomControl))

                '! eliminem el propi control
                qua.RemoveAt(qua.Count - 1)

                Return True   '? hem processat una linia d'una tabControl o TabPage.No cal seguir processant
            End If

        End If

        Return False '! la linia que processem no pertany a cap TabControl
    End Function

    ''' <summary>
    ''' Set el control pare d'un control
    ''' </summary>
    ''' <param name="controlVB6"></param>
    Private Sub SetPare(ByRef controlVB6 As ControlVB6)
        Try
            If qua.Count > 0 Then
                controlVB6.nomControlPare = If(qua.Count = 0, String.Empty, qua.Item(qua.Count - 1).nomControl)
                controlVB6.tipusControlPare = If(qua.Count = 0, String.Empty, qua.Item(qua.Count - 1).tipusControl)
            End If
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Processa un control
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="controlVB6"></param>
    ''' <returns></returns>
    Private Function ProcessaControl(ByVal fitxerFRM As String, ByVal linia As String, ByRef controlVB6 As ControlVB6, ByRef nomcontrolTabPage As String)
        Dim splitList() As String = {}

        '? inici de la declaració d'un control
        If RegexCercaCadena(linia, "Begin\s(\w+\.\w+)\s(\w+)", splitList) Then
            '! "marquem" l'inici de la definició d'un control
            liniaBeginTrobada = True

            If splitList(1) = TIPUSCONTROL_CODEJOC_TABPAGE Then
                liniaTabPageTrobada = True

                '! nom del control TabPage
                nomcontrolTabPage = splitList(2)

                '! recuperem el controlVB6 de la colecció
                controlVB6 = lstControlsVB6.Where(Function(s) s.nomControl = splitList(2)).FirstOrDefault


                If controlVB6 Is Nothing Then  '? pot ser un element d'un controlArray
                    '? comprovem si la tabpage es un element d'un controlArray
                    Dim elementsControlArray = lstControlsVB6.Where(Function(s) s.nomControl.StartsWith(UNDERSCORE & splitList(2) & UNDERSCORE))

                    If elementsControlArray.Count > 1 Then

                        If Not trobatElementControlArray Then
                            indexElementControlArray = elementsControlArray.Count

                            trobatElementControlArray = True
                        End If

                        If indexElementControlArray > 0 Then
                            '? hem de calcular l'index de l'element (tabpage) dins el controlArray
                            indexElementControlArray = indexElementControlArray - 1     '? dijous 5/1/2023

                            '! obtenim la tabPage
                            controlVB6 = lstControlsVB6.Where(Function(s) s.nomControl = UNDERSCORE & splitList(2) & UNDERSCORE & indexElementControlArray).FirstOrDefault
                        End If

                    End If

                Else  '? NO ES UN ELEMENT D'UN CONTROLARRAY
                    Dim KKK = 1
                End If

                '! ERROR DETECTAT.PROBABLEMENT DEGUT AL FORMULARI EN VB6
                If controlVB6 Is Nothing Then
                    '! afegim l'error al listview
                    Dim lvi As New ListViewItem()
                    lvi.SubItems.Add(TipusErrorVB6.CONTROL_NO_PROCESSAT.ToString())   ' Tipus
                    lvi.SubItems.Add(fitxerFRM)   ' Fitxer
                    lvi.SubItems.Add(splitList(2))   ' Control
                    lvi.BackColor = Color.Orange

                    lvErrorsMigracio.Items.Add(lvi)

                    Return True
                End If


                '! set el pare
                SetPare(controlVB6)

                '! afegim a la llista de controls pare
                qua.Add(controlVB6)

                Return True     '? excepcio : ja s'ha creat abans el controlVB6
            End If

            If splitList(1) = TIPUSCONTROL_CODEJOC_TABCONTROL Then
                indexControlTabControl = 0       '? dimecres 4/1/2023

                liniaTabControlTrobada = True
            End If


            '! creem un nou control
            controlVB6 = New ControlVB6()

            '! assignem els valors inicials de les propietat del control
            With controlVB6
                .tipusControl = splitList(1)
                .nomControl = splitList(2)

                '? dimecres 11/1/2023
                Dim cc As ControlNET = lstControlsNET.Where(Function(s) s.nomControl.Contains(splitList(2))).FirstOrDefault
                If cc.nomControl IsNot Nothing Then
                    .nomControl = cc.nomControl
                    Dim kk = 1
                End If

                '? per defecte el BackColor dels controls és :
                .BackColor = "System.Windows.Forms.Control.DefaultBackColor"

                If .tipusControl = TIPUSCONTROL_TREEVIEW Then
                    .BackColor = "System.Drawing.SystemColors.Control"
                End If

                .Appearance = ConstruirBorderStyleDefaultByTipus(splitList(1))  '? assignem segons el tipus de control l'appearance/VB6 (BorderStyle/NET)
            End With

            ''''If controlVB6.tipusControl <> TIPUSCONTROL_CODEJOC_TABPAGE Then
            ActualitzaControl(controlVB6)
            ''''End If


            '? comprovem si és un control de tipus ToolBar (cas especial perquè el Wizard2008 no el sap migrar correctament!)
            If EsToolbar(linia) Then
                controlToolbarTrobat = True
                nomToolbar = splitList(2).Trim
            End If

            Return True


        ElseIf liniaBeginTrobada AndAlso RegexCercaCadena(linia, LINIA_END, splitList) Then
            '! eliminem el propi control
            qua.RemoveAt(qua.Count - 1)


            '? en VB6, per defecte les Labels son "opaques" (BackStyle = 1)
            If controlVB6 IsNot Nothing AndAlso controlVB6.tipusControl = TIPUSCONTROL_LABEL AndAlso controlVB6.BackStyle Is Nothing Then
                controlVB6.BackStyle = "1"
            End If

        ElseIf liniaBeginTrobada AndAlso Not liniaTabPageTrobada AndAlso Not liniaTabControlTrobada AndAlso RegexCercaCadena(linia, LINIA_END, splitList) Then
            liniaBeginTrobada = False

            '! eliminem el propi control
            qua.RemoveAt(qua.Count - 1)

            Return True

        ElseIf RegexCercaCadena(linia, LINIA_END, splitList) Then
            liniaBeginTrobada = False

            '! eliminem el propi control
            qua.RemoveAt(qua.Count - 1)

            Return True
        End If

        Return False '! la linia que processem no és ni Begin ni End (fitxer FRM)
    End Function

    ''' <summary>
    ''' Retorna a partir d'un fitxer frm/VB6, la llista de controls que té aplicant canvis en certes propietats que no es migren automaticament
    ''' Exemple : el valor de la propietat "Tag" dels controls
    ''' Exemple : el valor de la propietat "Caption" dels TabControl
    ''' Exemple : 
    ''' </summary>
    ''' <param name="fitxerFRM"></param>
    ''' <returns></returns>
    Private Function ProcessaControls(ByVal fitxerFRM As String) As List(Of ControlVB6)
        If Not File.Exists(fitxerFRM) Then Return Nothing

        Dim liniesFitxerVB As List(Of String) = LlegeixFitxer(fitxerFRM, encoding)
        Dim splitList() As String = {}
        Dim controlVB6 As ControlVB6

        '! inicialitzem la llista de controls que té el FRM
        lstControlsVB6 = New List(Of ControlVB6)

        '!
        liniaBeginTrobada = False

        propertyFontTrobada = False

        '! guardem el valor del nom d'un TabControlPage
        nomcontrolTabPage = String.Empty


        trobatElementControlArray = False

        '? comencem a processar les linies
        For i As Integer = 0 To liniesFitxerVB.Count - 1

            '! Quan trobem una linia del tipus :     Attribute VB_Name = "frmDistritos", ja no cal comprovar mes perque hem arribat al final de la definicio dels controls (dins del fitxer frm)
            If RegexCercaCadena(liniesFitxerVB(i), "Attribute\sVB_Name\s=\s", splitList) Then

                '? actualitzem el valor de per indicar quins controls son elements de controlArray
                ActualitzaElementControlArray(fitxerFRM, lstControlsVB6)

                Return lstControlsVB6
            End If


            splitList = {}

            '! TabControl o TabControlPage
            If ProcessaTabControl_TabControlPage(fitxerFRM, liniesFitxerVB(i), controlVB6) Then Continue For

            '! la resta
            If ProcessaControl(fitxerFRM, liniesFitxerVB(i), controlVB6, nomcontrolTabPage) Then Continue For

            '? linies que no s'han de tractar--> continuem
            If controlVB6 Is Nothing Then
                Continue For
            End If

            CargarValorsPropietatsControls(controlVB6, liniesFitxerVB(i))

        Next

        '? un cop hem "carregat" tots els controls d'un formulari , actualitzem certes propietats dels elements de controlArray
        ActualitzaElementControlArray(fitxerFRM, lstControlsVB6)

        Return lstControlsVB6
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="controlVB6"></param>
    Private Sub ActualitzaControl(ByRef controlVB6 As ControlVB6)
        '! set el pare
        SetPare(controlVB6)

        '! afegim a la llista de controls pare
        qua.Add(controlVB6)

        Debug.WriteLine(String.Format("========>> QUA.ADD : {0}", controlVB6.nomControl))

        '! afegim el control a la llista
        lstControlsVB6.Add(controlVB6)
    End Sub

    ''' <summary>
    ''' Retorna la llista de controls que té un formulari a partir del fitxer FRM
    ''' En alguns casos a la llista d'aquests controls s'haurà de fer un processament per afegir/modificar certes propietats que el Wizard2008 no migra correctament (<see cref="ProcessaControls"/>)
    ''' </summary>
    ''' <param name="fitxerFRM"></param>
    ''' <returns></returns>
    Private Function ObtenirControls(ByVal fitxerFRM As String) As List(Of ControlVB6)
        If Not File.Exists(fitxerFRM) Then Return Nothing

        Dim liniesFitxerVB As List(Of String) = LlegeixFitxer(fitxerFRM, encoding)
        Dim splitList() As String = {}
        Dim liniaBeginTrobada As Boolean = False

        Dim lstControlsVB6 As New List(Of ControlVB6)

        Dim controlVB6 As ControlVB6

        For i As Integer = 0 To liniesFitxerVB.Count - 1

            '! Quan trobem una linia del tipus :     Attribute VB_Name = "frmDistritos", ja no cal comprovar mes perque hem arribat al final de la definicio dels controls (dins del fitxer frm)
            If RegexCercaCadena(liniesFitxerVB(i), "Attribute\sVB_Name\s=\s", splitList) Then

                '? actualitzem el valor de per indicar quins controls son elements de controlArray
                ActualitzaElementControlArray(fitxerFRM, lstControlsVB6)

                Return lstControlsVB6
            End If

            If liniaBeginTrobada AndAlso RegexCercaCadena(liniesFitxerVB(i), LINIA_END, splitList) Then
                liniaBeginTrobada = False

                Debug.WriteLine(String.Format("========>> QUA.REMOVE : {0}", qua.Item(qua.Count - 1).nomControl))

                '! eliminem el propi control
                qua.RemoveAt(qua.Count - 1)
            End If

            splitList = {}

            '! Begins anidats. Excloim els controls de tipus "VB.xxxxx"   (exemple : VB.Form, VB.TextBox, VB.Label,,...)
            If RegexCercaCadena(liniesFitxerVB(i), "Begin\s(\w+\.\w+)\s(\w+)", splitList) Then
                '! "marquem" l'inici de la definició d'un control
                liniaBeginTrobada = True

                controlVB6 = New ControlVB6

                '! assignem els valors inicials de les propietat del control
                With controlVB6
                    .tipusControl = splitList(1)
                    .nomControl = splitList(2)
                End With


                ActualitzaControl(controlVB6)
            End If

        Next

        '? un cop hem "carregat" tots els controls d'un formulari , actualitzem certes propietats dels elements de controlArray
        ActualitzaElementControlArray(fitxerFRM, lstControlsVB6)

        Return lstControlsVB6
    End Function

    ''' <summary>
    ''' Actualitza certes propietats d'un controlVB6 que és un element d'un ControlArray
    ''' </summary>
    ''' <param name="lstControlsVB6"></param>
    Private Sub ActualitzaElementControlArray(ByVal fitxerFRM As String, ByRef lstControlsVB6 As List(Of ControlVB6))
        For Each c As ControlVB6 In lstControlsVB6
            '? tots els elements d'un controlArray tenen el mateix nom
            '? *******  IMPORTANT hem trobat formularis on les tabControlPages són elements d'un ControlArray (exemples : frmFotoPersona, frmGDAdquirirMultiDocumentosAplicacion, frmGDAdquirirDocumentosAplicacion)            
            Dim elements As List(Of ControlVB6) = lstControlsVB6.Where(Function(s) s.nomControl = c.nomControl).ToList

            '! si n'hi ha més d'1 de control amb el mateix nom --> és un element d'un ControlArray
            If elements.Count > 1 Then

                For Each e As ControlVB6 In elements

                    '? EXCEPCIO : Toolbar        '? dijous 12/1/2023
                    If e.tipusControl = TIPUSCONTROL_TOOLBAR Then
                        Exit For
                    End If

                    '! és un element d'un controlArray
                    e.EsElementControlArray = True

                    ' actualitzem el nom de l'element (exemple:  _OptVisualitzacio_0)
                    e.nomControl = UNDERSCORE & e.nomControl & UNDERSCORE & e.Index
                Next
            End If
        Next
    End Sub

    ''' <summary>
    ''' Fitxers VB6 modificats porteriorment a una data
    ''' </summary>
    ''' <param name="data"></param>
    Private Function ExisteixFitxersVB6ModificatsDesDe(data As Date) As Boolean
        Netejar("VB6")

        '! en el cas de VB6, els unics fitxers que hem de "processar" son els que acaben en :
        Dim patro As String = "*.frm,*.frx,*.bas,*.cls,*.ctl"

        Dim lstPatro() As String = patro.Split(",")

        '! busquem fitxers dins la carpeta inicial amb un patro
        For Each p As String In lstPatro
            FitxersModificatsDirectori(cboDirectoryVB6.Text, data, p, True)
        Next

        '! busquem fitxers dins les subcarpetes de la carpeta inicial
        For Each p As String In lstPatro
            DirSearch(cboDirectoryVB6.Text, data, p, True)
        Next

        If dtVB6.Rows.Count > 0 Then Return True

        Return False
    End Function

    ''' <summary>
    ''' Retorna la llista de totes les "dates de migracio"
    ''' Les "dates de migracio" son aquelles on l'"estat" es E_EstatProcesMigracio.Executat_PostMigracio
    ''' </summary>
    ''' <returns></returns>
    Private Function RetornaDatesMigracioFitxerTrazaMigracio(Optional fitxerTracingMigracio As String = "") As List(Of Date)
        Dim lstDatesMigracio As New List(Of Date)  ' totes les dates de migracio

        Dim fitxerEstatProcesMigracio As String = GetFitxerTracingMigracio(fitxerTracingMigracio)

        If Not File.Exists(fitxerEstatProcesMigracio) Then Return lstDatesMigracio

        Dim linies As List(Of String) = LlegeixFitxer(fitxerEstatProcesMigracio, encoding)

        For Each linia As String In linies
            '? Observació : considerem que quan hem executat "PostMigracio" ja hem acabat el procés de migració (encara que no haguem validat!!!!)
            If Me.EstatProcesMigracio(linia) = E_EstatProcesMigracio.Executat_PostMigracio Then
                lstDatesMigracio.Add(linia.Split("|")(0))
            End If
        Next

        Return lstDatesMigracio
    End Function

    ''' <summary>
    ''' Existeix el mateix UPGRADE
    ''' </summary>
    ''' <param name="codi"></param>
    ''' <returns></returns>
    Private Function ExisteixUpgrade(ByVal codi As String, ByVal descripcio As String) As Boolean

        If upgrades.ContainsKey(codi) Then

            For Each u In upgrades.Item(codi)
                If u.descripcio = descripcio Then
                    Return True
                End If
            Next
        End If

        Return False
    End Function

    Private Function ExisteixEvent(ByVal codi As String, ByVal evento As String) As Boolean

        If upgrades.ContainsKey(codi) Then

            For Each u In upgrades.Item(codi)
                If u.descripcio.Contains(evento) Then
                    Return False
                End If
            Next
        End If

        Return True
    End Function

    Private Function ExisteixPropietat(ByVal codi As String, ByVal propietat As String) As Boolean

        If upgrades.ContainsKey(codi) Then

            For Each u In upgrades.Item(codi)
                If u.descripcio.Contains(propietat) Then
                    Return False
                End If
            Next
        End If

        Return True
    End Function

    Private Function ExisteixMetode(ByVal codi As String, ByVal metode As String) As Boolean

        If upgrades.ContainsKey(codi) Then

            For Each u In upgrades.Item(codi)
                If u.descripcio.Contains(metode) Then
                    Return False
                End If
            Next
        End If

        Return True
    End Function

    Private Function ExisteixColeccio(ByVal codi As String, ByVal coleccio As String) As Boolean

        If upgrades.ContainsKey(codi) Then

            For Each u In upgrades.Item(codi)
                If u.descripcio.Contains(coleccio) Then
                    Return False
                End If
            Next
        End If

        Return True
    End Function

    ''' <summary>
    ''' Obté <see cref="ColorUpgrade"/> a partir de la descripció
    ''' </summary>
    ''' <param name="codi"></param>
    ''' <returns></returns>
    Private Function colorUpgradeRoot(ByVal codi As String) As ColorUpgrade
        Dim colorRoot As ColorUpgrade = ColorUpgrade.Sense

        If Not upgrades.ContainsKey(codi) Then Return colorRoot

        For Each u As Upgrade In upgrades.Item(codi)
            If [Enum].Parse(GetType(ColorUpgrade), u.color) > colorRoot Then
                colorRoot = [Enum].Parse(GetType(ColorUpgrade), u.color)
            End If
        Next

        Return colorRoot
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="codi"></param>
    ''' <param name="descripcio"></param>
    ''' <param name="tipus"></param>
    ''' <returns></returns>
    Private Function BuscaUpgrade(ByVal codi As String, ByVal descripcio As String, ByVal tipus As String, ByRef color As ColorUpgrade, ByRef solucio As String, ByRef observacio As String) As Boolean
        If ExisteixUpgrade(codi, descripcio) Then Return False

        numTotalUpgrades = numTotalUpgrades + 1

        Dim splitList() As String = {}

        '? per evitar i/o provar determinats errors
        '? If codi <> "6A50421D-15FE-4896-8A1B-2EC21E9037B2" AndAlso
        '?        codi <> "6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305" AndAlso
        '?        codi <> "D0BD8832-D1AC-487C-8AA5-B36F9284E51E" Then Return False



        Select Case codi
            Case "0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"
                color = ColorUpgrade.Resolt
                solucio = "Manual"

                RegexCercaCadena(descripcio, "(?i)El límite inferior de la matriz ([\w+-\.\(\)]+) ha cambiado de 1 a 0.", splitList)

            Case "A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"
                color = ColorUpgrade.Resolt
                solucio = "Manual"

                Dim getAllNodes As Boolean = RegexCercaCadena(descripcio, "(?i)El límite inferior de la colección ([\w+-\.\(\)]+) cambió de 1 a 0.", splitList)

                If Not splitList(1).Contains(".") Then
                    If splitList(1).Contains("GetAllNodes") Then
                        Return ExisteixColeccio(codi, "GetAllNodes")
                    Else
                        Return ExisteixColeccio(codi, splitList(1))
                    End If
                Else
                    Return ExisteixColeccio(codi, splitList(1).Split(".")(1))
                End If

                '! UPGRADE_WARNING: La propiedad Timer ([\w+-\.])+ no puede tener un valor de 0
            Case "169ECF4A-1968-402D-B243-16603CC08604"
                color = ColorUpgrade.Resolt
                solucio = "Regex"

                RegexCercaCadena(descripcio, "(?i)La propiedad Timer ([\w+-\.\(\)]+) no puede tener un valor de 0", splitList)

                Return ExisteixPropietat(codi, splitList(2).Split(".")(1))

                '! UPGRADE_WARNING: Se detectó el uso de Null/IsNull()
            Case "2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"
                color = ColorUpgrade.Warning    'colorUpgradeRoot(codi)
                solucio = "https://www.vbmigration.com/detknowledgebase.aspx?Id=19"

                '! PropertyBag
            Case "371DAEFA-CEB3-4528-82E8-213DF0EA63C3"
                color = colorUpgradeRoot(codi)

                RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) objeto no se actualizó.", splitList)

                '! CommonDialog
            Case "671167DC-EA81-475D-B690-7A40C7BF4A23"
                color = ColorUpgrade.Resolt
                solucio = "Manual"

                RegexCercaCadena(descripcio, "(?i)La variable ([\w+-\.]+) no se actualizó.", splitList)

            Case "D94970AE-02E7-43BF-93EF-DCFCD10D27B5"
                color = ColorUpgrade.Altres

                RegexCercaCadena(descripcio, "(?i)La propiedad ([\w+-\.]+) no se actualizó", splitList)

            Case "CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"
                color = colorUpgradeRoot(codi)

                Dim casPropietat As Boolean = False
                Dim casMetode As Boolean = False

                casPropietat = RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) propiedad ([\w+-\.\(\)]+) no se actualizó", splitList)
                If Not casPropietat Then
                    casMetode = RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) método ([\w+-\.\(\)]+) no se actualizó", splitList)

                    If casMetode Then
                        Dim metode As String = splitList(2).Split(".")(1)
                        '! no dupliquem el tipus de metode

                        If metode = "TextWidth" Then
                            color = ColorUpgrade.Resolt
                            solucio = "Formparent/Utils.TextWidth"
                        ElseIf metode = "Drag" Then
                            color = ColorUpgrade.Critic
                            observacio = "ListView/NET"
                        ElseIf metode = "DragIcon" Then
                            color = ColorUpgrade.Critic
                            observacio = "ListView/NET"
                        ElseIf metode = "SelectedItem" Then
                            observacio = "ListView/NET"
                        ElseIf metode = "SelPrint" Then
                            color = ColorUpgrade.Critic
                            solucio = "Encapsulat classe RichTextBoxPrintCtrl"
                            observacio = "https://docs.microsoft.com/es-es/previous-versions/7d29f66t(v=vs.90)?redirectedfrom=MSDN"
                        ElseIf metode.Contains("Link") Then
                            color = ColorUpgrade.Resolt
                            observacio = "Desactivat segons Juli"
                        ElseIf metode.Contains("APPEARANCE") Then
                            color = ColorUpgrade.Warning
                            observacio = "El tcCodejockTab ha de mantenir la propietat Appearance"
                        Else
                            color = ColorUpgrade.Critic
                            observacio = String.Format("Mètode {0} de {1}", metode, splitList(1))
                        End If

                        Return ExisteixMetode(codi, metode)

                    End If
                ElseIf casPropietat Then
                    If Not splitList(2).Contains(".") Then
                        Dim control As String = splitList(1)
                        Dim propietat As String = splitList(2)

                        If control = TIPUSCONTROL_TREEVIEW Then
                            If propietat = "DropHighlight" Then
                                color = ColorUpgrade.Critic
                                solucio = "Manual"
                            Else
                                color = ColorUpgrade.Altres
                            End If

                        ElseIf control = TIPUSCONTROL_LISTVIEW Then
                            color = ColorUpgrade.Altres

                        ElseIf control = "MSComctlLib.ListItem" Then
                            If propietat.ToUpper.Contains("GHOSTED") Then
                                color = ColorUpgrade.Warning
                            Else
                                color = ColorUpgrade.Altres
                            End If

                        ElseIf control = "Screen" Then
                            If propietat = "Fonts" OrElse propietat = "FontCount" Then
                                color = ColorUpgrade.Resolt
                                solucio = "Encapsulat a Utils"
                                observacio = "http://www.vbmigration.com/resources/detmigratingfromvb6.aspx?Id=19#531"
                            Else
                                color = ColorUpgrade.Altres
                            End If
                        Else
                            Dim kk = 1
                        End If

                    Else
                        Dim control As String = splitList(1)
                        Dim propietat As String = splitList(2).Split(".")(splitList(2).Split(".").Count - 1)

                        If control = "MSComctlLib.Node" Then
                            If propietat = "Root" Then
                                color = ColorUpgrade.Warning
                                solucio = "Manual"
                                observacio = "Substituir Node.Root per Node.TreeView.TopNode"
                            ElseIf propietat = "Selected" Then
                                color = ColorUpgrade.Resolt
                                solucio = "Manual"
                            ElseIf propietat = "SelectedItem" Then
                                color = ColorUpgrade.Warning
                            ElseIf propietat = "Item" Then
                            ElseIf propietat = "LastSibling" Then
                                color = ColorUpgrade.Warning
                            ElseIf propietat = "FirstSibling" Then
                                color = ColorUpgrade.Warning
                            ElseIf propietat = "Parent" Then
                                color = ColorUpgrade.Altres
                            ElseIf propietat = "ExpandedImage" Then
                                color = ColorUpgrade.Warning
                            ElseIf propietat = "Expanded" Then
                                color = ColorUpgrade.Warning

                            Else
                                color = ColorUpgrade.Altres
                                observacio = String.Format("Propietat {0} del control {1}", propietat, control)
                            End If
                        ElseIf control = "Screen" Then
                            If propietat = "Fonts" OrElse propietat = "FontCount" Then
                                color = ColorUpgrade.Resolt
                                solucio = "Encapsulat a Utils"
                                observacio = "http://www.vbmigration.com/resources/detmigratingfromvb6.aspx?Id=19#531"
                            Else
                                color = ColorUpgrade.Altres
                            End If
                        ElseIf control = "Form" Then
                            color = ColorUpgrade.Warning

                        ElseIf control = "Printer" Then
                            color = ColorUpgrade.Warning

                        ElseIf control = "RichTextLib.RichTextBox" Then
                            color = ColorUpgrade.Warning

                        ElseIf control = "MSComDlg.CommonDialog" Then
                            color = ColorUpgrade.Warning

                        ElseIf control = "PictureBox" Then
                            color = ColorUpgrade.Warning

                            If propietat.ToUpper.Contains("AUTOREDRAW") Then
                                solucio = "https://www.vbmigration.com/resources/detmigratingfromvb6controls.aspx?Id=9#384"
                            ElseIf propietat.ToUpper.Contains("HDC") Then
                                solucio = "https://www.vbmigration.com/resources/detmigratingfromvb6controls.aspx?Id=9#384"
                            Else
                                color = ColorUpgrade.Altres
                            End If

                        ElseIf control = TIPUSCONTROL_TREEVIEW Then
                            color = ColorUpgrade.Warning

                        ElseIf control = "Picture" Then
                            color = ColorUpgrade.Warning

                        ElseIf control = "Control" Then
                            color = ColorUpgrade.Warning

                        ElseIf control = TIPUSCONTROL_LISTVIEW Then
                            color = ColorUpgrade.Warning

                        ElseIf control = "MSComctlLib.ListImage" Then
                            color = ColorUpgrade.Warning

                        ElseIf control = "VBControlExtender" Then
                            color = ColorUpgrade.Warning



                        Else
                            color = ColorUpgrade.Altres
                            observacio = String.Format("Propietat {0} del control {1}", propietat, control)
                        End If

                        Return ExisteixPropietat(codi, propietat)
                    End If
                Else
                    Dim control As String = splitList(1)
                    Dim propietat As String = splitList(2).Split(".")(1)

                    If control = "MSComctlLib.Node" Then
                        If propietat = "Selected" Then
                            solucio = "Manual"
                            color = ColorUpgrade.Resolt
                        ElseIf propietat = "Root" Then
                            color = ColorUpgrade.Warning
                            solucio = "Manual"
                            observacio = "Substituir Node.Root per Node.TreeView.TopNode"
                        Else
                            color = ColorUpgrade.Altres
                            observacio = String.Format("{0} de {1}", propietat, control)
                        End If
                    ElseIf propietat.Contains("Link") Then
                        color = ColorUpgrade.Resolt
                        observacio = "Desactivat segons Juli"
                    ElseIf propietat.Contains("OLEDrag") Then
                        color = ColorUpgrade.Resolt
                        observacio = "Desactivat segons Juli"
                    ElseIf propietat.Contains("OLEDrop") Then
                        color = ColorUpgrade.Resolt
                        observacio = "Desactivat segons Juli"
                    Else
                        color = ColorUpgrade.Altres
                        observacio = String.Format("{0} de {1}", propietat, control)
                    End If

                    '! no dupliquem el tipus de propietat
                    Return ExisteixPropietat(codi, propietat)
                End If

            Case "6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"
                RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) objeto no se actualizó", splitList)

                If splitList.Count = 0 Then Return False

                Dim objecte As String = splitList(1)
                If objecte.Contains(".") Then
                    Dim subControl As String = objecte.Split(".")(0)
                    If subControl = "ciaXPListView30" Then
                        color = ColorUpgrade.Resolt
                    ElseIf subControl = "Vintasoft" Then
                        color = ColorUpgrade.Critic
                    End If
                ElseIf objecte = "XPPopUp30" Then
                    color = ColorUpgrade.Resolt
                ElseIf objecte.Contains("Calendar") Then
                    color = ColorUpgrade.Resolt
                    observacio = "Es Codejock"
                ElseIf objecte = "gmsMenu" Then
                    color = ColorUpgrade.Resolt
                    observacio = "Etil_2000, Estil_2003"
                Else

                    color = ColorUpgrade.Critic
                End If


            Case "55B59875-9A95-4B71-9D6A-7C294BF7139D"
                color = ColorUpgrade.Altres

                RegexCercaCadena(descripcio, "(?i)No se actualizó la constante ([\w+-\.\(\)]+)\.", splitList)

                If splitList(1) = "vbBeginDrag" OrElse splitList(1) = "vbEndDrag" Then
                    color = ColorUpgrade.Critic
                ElseIf splitList(1) = "cdlCCRGBInit" Then
                    color = ColorUpgrade.Critic
                    descripcio = "És el valor inicial de color del commonDialog"
                    observacio = "Relacionat amb la propietat Flags dels MSComDlg.CommonDialog"
                    solucio = "S'ha de veure l'equivalent i/o és necessari"
                ElseIf splitList(1) = "vbPixels" Then
                    observacio = ""
                    color = ColorUpgrade.Resolt
                    solucio = "No s'usa"
                ElseIf splitList(1) = "vbTwips" Then
                    observacio = ""
                    color = ColorUpgrade.Resolt
                    solucio = "No s'usa"
                ElseIf splitList(1) = "Default" Then
                    observacio = ""
                    color = ColorUpgrade.Resolt
                    solucio = "rel.lacionat amb System.Windows.Forms.Cursor.Current"
                ElseIf splitList(1) = "tvwPrevious" Then
                    observacio = ""
                    color = ColorUpgrade.Resolt
                    solucio = "Regex"
                ElseIf splitList(1) = "tvwNext" Then
                    observacio = ""
                    color = ColorUpgrade.Resolt
                    solucio = "Regex"
                Else
                    color = ColorUpgrade.Altres
                End If

                '! CommonDialog
            Case "6E047632-2D91-44D6-B2A3-0801707AF686"
                color = ColorUpgrade.Warning
                solucio = "Manual"

                RegexCercaCadena(descripcio, "(?i)El control ([\w+-\.]+) ([\w+-\.]+) no se actualizó", splitList)

                Dim control As String = splitList(1)

                If control = "MSComDlg.CommonDialog" Then
                    solucio = "Manual : Cal substituir per OpenFileDialog/SaveFileDialog/ColorDialog/FontDialog/PrintDialog en funció del cas"
                Else
                    color = ColorUpgrade.Altres
                End If


            Case "ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"
                color = colorUpgradeRoot(codi)

                RegexCercaCadena(descripcio, "(?i)El evento ([\w+-\.]+) ([\w+-\.]+) no se actualizó", splitList)

                Dim control As String = splitList(1)
                Dim evento As String = splitList(2)

                If evento.Contains("MDIForm") Then
                    color = ColorUpgrade.Resolt
                    solucio = "Regex"

                ElseIf control = "Label" Then
                    If evento.Split(".")(1) = "Change" Then
                        color = ColorUpgrade.Resolt
                        solucio = "Regex"
                    ElseIf evento.Contains("OLEDragDrop") Then
                        color = ColorUpgrade.Warning
                        solucio = "Nova arquitectura DragDrop"
                    Else
                        color = ColorUpgrade.Altres
                    End If

                ElseIf control = TIPUSCONTROL_TREEVIEW Then
                    If evento.Split(".")(1) = "DragDrop" OrElse evento.Split(".")(1) = "DragOver" Then
                        'color = ColorUpgrade.Resolt
                        solucio = "Nova arquitectura DragDrop"
                    Else
                        color = ColorUpgrade.Altres
                    End If

                ElseIf control = TIPUSCONTROL_LISTVIEW Then
                    If evento.Contains("ItemClick") Then
                        color = ColorUpgrade.Warning
                    Else
                        color = ColorUpgrade.Altres
                    End If

                ElseIf control = "VBControlExtender" Then
                    color = ColorUpgrade.Altres

                ElseIf control = "PictureBox" Then
                    color = ColorUpgrade.Altres

                Else
                    color = ColorUpgrade.Altres
                End If

                Return ExisteixEvent(codi, evento.Split(".")(1))

            Case "14981025-6E0D-4DD3-82F8-AD5B7837D779"
                color = colorUpgradeRoot(codi)

                RegexCercaCadena(descripcio, "(?i)La propiedad ([\w+-\.]+) no se actualizó", splitList)

                If splitList(1) = "SelectedImage" Then
                    color = ColorUpgrade.Altres
                Else
                    color = ColorUpgrade.Altres
                End If

                '! App.HelpFile
            Case "076C26E5-B7A9-4E77-B69C-B4448DF39E58"
                color = ColorUpgrade.Resolt
                solucio = "Obsolete segons correo Juli"
                observacio = "https://www.vbmigration.com/detknowledgebase.aspx?Id=121"

                RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) propiedad ([\w+-\.]+) no se actualizó", splitList)

                '! UnLoad
            Case "875EBAD7-D704-4539-9969-BC7DBDAA62A2"
                color = ColorUpgrade.Resolt
                solucio = "Formparent"

                RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) de descarga no se actualizó", splitList)

            Case "6A50421D-15FE-4896-8A1B-2EC21E9037B2"
                color = ColorUpgrade.Altres
                observacio = "1.-Variables definides en altres projectes, 2.-VB6 no té declarada el tipus, 3.-VB6 variable declarada As Object"

                RegexCercaCadena(descripcio, "(?i)No se puede resolver la propiedad predeterminada del objeto ([\w+-\.\(\)\s\$\*\/\=\;\:]+)", splitList)

                If splitList(1).Contains(".") Then
                    Dim control As String = splitList(1).Split(".")(0)
                    Dim propietat As String = splitList(1).Split(".")(1)
                    If splitList(1).Split(".").Count > 3 Then
                        propietat = splitList(1).Split(".")(1) & "." & splitList(1).Split(".")(2)
                    End If

                    '! per defecte
                    Dim tipusObjecte As String = 99999

                    Dim splitListObjecte() As String = {}
                    RegexCercaCadena(control, "(?i)(^frm)|(^formulari)|(^MCache)|(^MDI)|(^Estil)" &
                                        "|(^Vars)|(^PopMenuXP)|(^CacheXecute)|(^Emp)|(^Piece)|(^regQuery_A_Key\(\))" &
                                        "|(^PdfBox)|(^AllPDF)|(^ItemCalendari)|(^CalendarGlobalSettings)" &
                                        "|(^cmb)|(^combo)" &
                                        "|(^LlibAplicacio)|(^BarraProgres)" &
                                        "|(^Conexio_Renamed)" &
                                        "|(^ocache)" &
                                        "|(^grd)" &
                                        "|(^Switch)" &
                                        "|(^BarraProgres)|(^prg)" &
                                        "|(^oGdPicture)|(^gdPicture)|(^oGdViewer)|(^gdvFotografia)", splitListObjecte)

                    '! hem trobat el tipus d'objecte
                    If splitListObjecte.Count > 0 Then
                        tipusObjecte = splitListObjecte(1).ToUpper

                        If splitListObjecte.Count > 0 AndAlso (tipusObjecte = "FRM" OrElse tipusObjecte = "FORMULARI") Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "MDI" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "MCACHE" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "PIECE" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "CONEXIO_RENAMED" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "CACHEXECUTE" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "REGQUERY_A_KEY()" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "LLIBAPLICACIO" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso (tipusObjecte = "CMB" OrElse tipusObjecte = "COMBO") Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso (tipusObjecte = "BARRAPROGRES" OrElse tipusObjecte = "PRG") Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "OCACHE" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "GRD" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE


                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "SWITCH" Then
                            observacio = "Encapsulat en GmsUtilitats"
                            color = ColorUpgrade.Warning
                            tipusObjecte = "9999"
                            propietat = "Switch"
                            solucio = "Implementació"

                            If Not esDuplicatTipusControlPropietat("Switch", "") Then
                                Return True
                            Else
                                Return False
                            End If

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "PDFBOX" Then
                            observacio = "PdfBox"
                            color = ColorUpgrade.Warning
                            tipusObjecte = "9998"
                            propietat = "PdfBox"

                            If Not esDuplicatTipusControlPropietat(propietat, "") Then
                                Return True
                            Else
                                Return False
                            End If

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "ALLPDF" Then
                            observacio = "AllPDF"
                            color = ColorUpgrade.Warning
                            tipusObjecte = "9915"
                            propietat = "AllPDF"

                            If Not esDuplicatTipusControlPropietat(propietat, "") Then
                                Return True
                            Else
                                Return False
                            End If


                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "CALENDARGLOBALSETTINGS" Then
                            observacio = "Exemple : CRM\GestioCRM"
                            color = ColorUpgrade.Warning
                            tipusObjecte = "9926"
                            propietat = "CalendarGlobalSettings"
                            solucio = "Cal declarar i instanciar una variable de tipus CalendarGlobalSettingsClass"

                            If Not esDuplicatTipusControlPropietat("CalendarGlobalSettings", "") Then
                                Return True
                            Else
                                Return False
                            End If

                        ElseIf splitListObjecte.Count > 0 AndAlso (tipusObjecte = "OGDPICTURE" OrElse tipusObjecte = "OGDVIEWER" OrElse tipusObjecte = "GDPICTURE" OrElse tipusObjecte = "GDVFOTOGRAFIA") Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "ESTIL" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "VARS" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "EMP" Then
                            Return False   '! NO VISUALITZEM L'UPGRADE

                        ElseIf splitListObjecte.Count > 0 AndAlso tipusObjecte = "POPMENUXP" Then
                            Return False  '! NO VISUALITZEM L'UPGRADE
                        Else

                            'If Not esDuplicatTipusControlPropietat(propietat, "") Then
                            '    Debug.WriteLine(String.Format("2.------NO_TIPUSOBJECTE : Control : {0} , Propietat : {1} , TipusObjecte : {2}", control, propietat, tipusObjecte))
                            'End If
                        End If

                    End If

                    '! PROCESSAMENT PEL NOM DEL CONTROL
                    '! ---------------------------------

                    If control.ToUpper.Contains("STYLEPIE") Then


                    Else
                        color = ColorUpgrade.Altres
                    End If


                    '! PROCESSAMENT PEL NOM DE LA PROPIETAT
                    '! ------------------------------------

                    '! Propietats GdPicture
                    Dim esDelTipus As Boolean = False
                    Dim duplicat As Boolean = False

                    duplicat = Propietats_GdPicture(propietat, tipusObjecte, observacio, color, esDelTipus)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat GmsXpTab
                    duplicat = Propietats_GmsXpTab(propietat, tipusObjecte, observacio, color, esDelTipus)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat FlexCell.Grid
                    duplicat = Propietats_FCGrid(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietats CIAXPStyle
                    duplicat = Propietats_CIAXPStyle(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat ALLPDF
                    duplicat = Propietats_ALLPDF(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat Textbox
                    duplicat = Propietats_Textbox(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat CacheActivex
                    duplicat = Propietats_CacheActivex(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat CalendarEvent
                    duplicat = Propietats_CalendarEvent(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat CalendarControl
                    duplicat = Propietats_CalendarControl(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat MSComDlg.CommonDialog
                    duplicat = Propietats_CommondDialog(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)

                    If color = ColorUpgrade.Critic OrElse color = ColorUpgrade.Warning Then
                        Return True
                    ElseIf esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat Excel
                    duplicat = Propietats_Excel(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat SSDBCombo, SSDBGrid, SSIndexTab, SSTab,...
                    duplicat = Propietats_Sheridan(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat ChartControl (Codejoc)
                    duplicat = Propietats_CharControl(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat WMI (The Win32_Process WMI class represents a process on an operating system.)
                    duplicat = Propietats_WMI(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If

                    '! Propietat OREF ()
                    duplicat = Propietats_OREF(propietat, tipusObjecte, observacio, color, esDelTipus, solucio)
                    If esDelTipus Then
                        Return duplicat
                    End If



                    '! La resta de propietats

                    If propietat.ToUpper.Contains("CONTAINER") Then
                        color = ColorUpgrade.Resolt
                        observacio = "El Wizard2008 ja actualitza Container per Parent"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "Parent"

                    ElseIf propietat.ToUpper = "ELLIPSE" Then

                    ElseIf propietat.ToUpper = "WANTSHOW" Then

                    ElseIf propietat.ToUpper = "AUTHOR" Then
                    ElseIf propietat.ToUpper = "PRODUCER" Then


                    ElseIf propietat.ToUpper = "LENGTHUNIT" Then

                    ElseIf propietat.ToUpper = "LOADSTYLESHEET" Then
                    ElseIf propietat.ToUpper = "FONTITALIC" Then

                    ElseIf propietat.ToUpper = "APPENDLINKPARA" Then
                    ElseIf propietat.ToUpper = "APPENDPARA" Then

                    ElseIf propietat.ToUpper = "SMARTSTRETCHDRAW" Then
                    ElseIf propietat.ToUpper = "STRETCHDRAW" Then
                    ElseIf propietat.ToUpper = "BEGINPARA" Then

                    ElseIf propietat.ToUpper = "DRAWTEXT" Then
                    ElseIf propietat.ToUpper = "WRITERICHTEXT" Then

                    ElseIf propietat.ToUpper = "FONTDEGREEANGLE" Then

                    ElseIf propietat.ToUpper = "FONTUNDERLINE" Then

                    ElseIf propietat.ToUpper = "MOVETO" Then
                    ElseIf propietat.ToUpper = "NEWSECTION" Then

                    ElseIf propietat.ToUpper = "ROUNDRECT" Then
                    ElseIf propietat.ToUpper = "STRETCHDRAWFROMFILE" Then

                    ElseIf propietat.ToUpper = "EXTENDER" Then
                    ElseIf propietat.ToUpper = "PARENT" Then
                    ElseIf propietat.ToUpper = "READPROPERTY()" Then

                    ElseIf propietat.ToUpper = "POPMENU1" Then

                    ElseIf propietat.ToUpper = "CLEARALL" Then


                    ElseIf propietat.ToUpper = "NAME, MODIFICACIONS)" Then
                    ElseIf propietat.ToUpper = "NAME, ALTES)" Then
                    ElseIf propietat.ToUpper = "COLORENABLED" Then

                    ElseIf propietat.ToUpper = "LOADCERTIFICATE" Then
                    ElseIf propietat.ToUpper = "CERTIFICATEPARAM" Then


                        ' *******************************************************


                    ElseIf propietat.ToUpper = "LISTSUBITEMS().TAG" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9957"
                        propietat = "XtremSuiteControls.ListViewSubItems"


                    ElseIf propietat.ToUpper = "APP" Then
                        observacio = ""
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9956"
                        propietat = "XtremeCommandBars.CommandBarsGlobalSettings"
                        solucio = ""

                    ElseIf propietat.ToUpper = PROPIETAT_SIZE.ToUpper Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9955"
                        propietat = "IO.FileInfo"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "Length"

                    ElseIf propietat.ToUpper = "STRETCH" Then
                        observacio = ""
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9420"
                        propietat = "Image"
                        solucio = "Cal migrar"

                    ElseIf propietat.ToUpper = "TOOLBAR" Then
                        observacio = "Estil2000"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9490"
                        propietat = "MDI"
                        solucio = "OBSOLET"

                    ElseIf propietat.ToUpper = "STATUSBAR" Then
                        observacio = "Estil2000/Estil2003"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9360"
                        propietat = "MDI"
                        solucio = "OBSOLET"
                    ElseIf propietat.ToUpper = "PICTILE" Then
                        observacio = "Estil2000/Estil2003"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9360"
                        propietat = "MDI"
                        solucio = "OBSOLET"
                    ElseIf propietat.ToUpper = "PICHIDDEN" Then
                        observacio = "Estil2000/Estil2003"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9360"
                        propietat = "MDI"
                        solucio = "OBSOLET"
                    ElseIf propietat.ToUpper = "PICSTRETCHED" Then
                        observacio = "Estil2000/Estil2003"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9360"
                        propietat = "MDI"
                        solucio = "OBSOLET"


                    ElseIf propietat.ToUpper = "PT" Then
                        observacio = "Relacionat amb el DragDrop"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9190"
                        propietat = "SafeNativeMethods.TVHITTESTINFO"
                        solucio = "OBSOLET"


                    ElseIf propietat.ToUpper = "BARCODETYPE" Then
                        observacio = "Barcode"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9770"
                        propietat = "Barcode"
                        solucio = ""

                    ElseIf propietat.ToUpper = "HITTEST" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9470"
                        propietat = "DataPicker"
                        solucio = ""

                    ElseIf propietat.ToUpper = "ITEM" Then
                        observacio = "TabControl (Codejoc) la propietat es correcte"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9120"
                        propietat = "Ctrl"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "SelectedItem (en GmsTabControl)"

                    ElseIf propietat.ToUpper = PROPIETAT_CAPTION.ToUpper Then
                        observacio = "el XtremeCommandBars.CommandBarControl i gmsPopUpCode.CPopMenuItem han de mantenir els CAPTION"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9190"
                        propietat = "Ctrl"
                        solucio = ""

                    ElseIf propietat.ToUpper = PROPIETAT_TAG.ToUpper Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9440"
                        propietat = "ListViewItem Codejock"
                        solucio = ""

                    ElseIf propietat.ToUpper = "COLUMNS" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9490"
                        propietat = "SSDBGrid,ListView,SSDBCombo"
                        solucio = ""


                    ElseIf propietat.ToUpper = "TABPAGES" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9430"
                        propietat = "GmsTabcontrol"
                        solucio = ""

                    ElseIf propietat.ToUpper = "SELECTEDITEM" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9430"
                        propietat = "GmsTabcontrol"
                        solucio = ""


                    ElseIf propietat.ToUpper = PROPIETAT_BACKCOLOR.ToUpper Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9660"
                        propietat = "Label"
                        solucio = ""


                    ElseIf propietat.ToUpper = "LOCKED" Then
                        observacio = "El FlexCell.Grid manté Locked"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9780"
                        propietat = "Ctrl"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "ReadOnly"


                    ElseIf propietat.ToUpper = "READONLY" Then
                        observacio = "Tots els controls (excepte FlexCell.Grid) cal mantenir ReadOnly"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9781"
                        propietat = "Ctrl"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "Locked"


                    ElseIf propietat.ToUpper = PROPIETAT_INDEX.ToUpper Then
                        observacio = "Es pot intentar utilitzar Regex : opt.indeOf(radio)"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9770"
                        propietat = "RadioButton"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "<opt>.indeOf(radio)"

                    ElseIf propietat.ToUpper = "SELSTART" Then
                        observacio = "Revisar la propietat ScrollToCaret (WorkAround)"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9740"
                        propietat = "TextBox, GmsData, GmsTemps, GmsImports, GmsCombo"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "SelectionStart"
                    ElseIf propietat.ToUpper = "SELLENGTH" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9740"
                        propietat = "TextBox, GmsData, GmsTemps"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "SelectionLength"


                    ElseIf propietat.ToUpper = PROPIETAT_AUTOSIZE.ToUpper Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9510"
                        propietat = "Label"
                        solucio = ""
                    ElseIf propietat.ToUpper = "FORECOLOR" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9520"
                        propietat = "Label"
                        solucio = ""

                    ElseIf propietat.ToUpper = "ALIGNMENT" Then
                        observacio = "Label : System.Drawing.ContentAlignment, TextBox : System.Windows.Forms.HorizontalAlignment"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9790"
                        propietat = "TextBox i Label"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "TextAlign"

                    ElseIf propietat.ToUpper = "MULTILINE" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9780"
                        propietat = "TextBox"

                    ElseIf propietat.ToUpper = "HWND" Then
                        observacio = "Relacionat amb la migració de metodes SafeNativeMetods"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9730"
                        propietat = "CTRL"

                    ElseIf propietat.ToUpper = "MAXLENGTH" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9690"
                        propietat = "Textbox,gmsTemps,gmsImports,gmsData"


                    ElseIf propietat.ToUpper = PROPIETAT_ITEMCOUNT.ToUpper Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9610"
                        propietat = "CodejockTab"



                    ElseIf propietat.ToUpper = "INTERVAL" Then
                        observacio = "El valor no pot ser 0"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9690"
                        propietat = "Timer"

                    ElseIf propietat.ToUpper = "SETIMAGELIST" Then
                        observacio = "PopMenuXP"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9680"
                        propietat = "PopMenuXP"
                    ElseIf propietat.ToUpper = "POPUPMENU" Then
                        observacio = "PopMenuXP"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9680"
                        propietat = "PopMenuXP"
                    ElseIf propietat.ToUpper = "MENUS" Then
                        observacio = "PopMenuXP"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9680"



                    ElseIf propietat.ToUpper = "PICTURE" Then
                        observacio = "Wizard2008 ha substituit Image.Picture = LoadPicture per Image.Picture = Nothing"
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9380"
                        propietat = "Image"
                    ElseIf propietat.ToUpper = "STRETCH" Then
                        observacio = ""
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9381"
                        propietat = "Image"


                    ElseIf propietat.ToUpper = "ITEMCALENDARI" Then
                        observacio = "ItemCalendari"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9935"
                        propietat = "ItemCalendari"

                    ElseIf propietat.ToUpper = "COMMANDBARS" Then
                        observacio = "MDI"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9370"
                        propietat = "MDI"

                    ElseIf propietat.ToUpper = "COLUMNHEADERS" Then
                        observacio = "listView"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9390"
                        propietat = "listView"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "Columns"


                    ElseIf propietat.ToUpper = "SENDKEYS" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9390"
                        propietat = "SendKeys"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "SendKeys.Send"


                    ElseIf propietat.ToUpper = "TIPUSBOTO" Then
                        observacio = "OBSOLET"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9370"
                        propietat = "GmsBoto"


                    ElseIf propietat.ToUpper = "TITLE" Then
                        observacio = "CalendarGlobalSettings"
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9340"
                        propietat = "CalendarGlobalSettings"
                    ElseIf propietat.ToUpper = "RESOURCEFILE" Then
                        observacio = "CalendarGlobalSettings"
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9341"
                        propietat = "CalendarGlobalSettings"
                    ElseIf propietat.ToUpper = "USERESOURCEFILELOCALE" Then
                        observacio = "CalendarGlobalSettings"
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9342"
                        propietat = "CalendarGlobalSettings"



                    ElseIf propietat.ToUpper = "ZORDER" Then
                        observacio = "Form/TextBox/AxPushButton/..."
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9600"
                        propietat = "Form/TextBox/AxPushButton/..."
                        solucio = "Cal migrar"


                    ElseIf propietat.ToUpper = "SELECTEDFOLDER" Then
                        observacio = "AxFolderTreeView"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9500"
                        propietat = "AxFolderTreeView"


                    ElseIf propietat.ToUpper = "PAINTMANAGER" Then
                        observacio = "XtremeSuiteControls.TabControl"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9400"
                        propietat = "XtremeSuiteControls.TabControl"



                    ElseIf propietat.ToUpper = "HITDATETIME" Then
                        observacio = "CalendarControl (Codejoc)"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9310"
                        propietat = "Control CalendarHitTestInfo"
                    ElseIf propietat.ToUpper = "VIEWEVENT" Then
                        observacio = "CalendarControl (Codejoc)"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9310"
                        propietat = "Control CalendarHitTestInfo"



                    ElseIf propietat.ToUpper = "REPORTFILENAME" Then
                        observacio = "CrystalReport"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9100"
                        propietat = "CrystalReport"
                    ElseIf propietat.ToUpper = "PRINTREPORT" Then
                        observacio = "CrystalReport"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9100"
                        propietat = "CrystalReport"

                    ElseIf propietat.ToUpper = "FLATSTYLE" Then
                        observacio = "XtremeSuiteControls.PushButton"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9200"
                        propietat = "XtremeSuiteControls.PushButton"




                    ElseIf propietat.ToUpper = "MIN" Then
                        observacio = "BarraProgres"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9350"
                        propietat = "BarraProgres"
                        solucio = "OBSOLET"
                    ElseIf propietat.ToUpper = "MAX" Then
                        observacio = "BarraProgres"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9350"
                        propietat = "BarraProgres"
                        solucio = "OBSOLET"
                    ElseIf propietat.ToUpper = PROPIETAT_VALUE.ToUpper Then
                        observacio = "BarraProgres"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9350"
                        propietat = "BarraProgres"
                        solucio = "OBSOLET"
                    ElseIf propietat.ToUpper = "BORDERSTYLE" Then
                        observacio = "BarraProgres"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9350"
                        solucio = "OBSOLET"


                    ElseIf propietat.ToUpper = "ACTIVETABBACKCOLOR" Then
                        observacio = "SSIndexTab"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9990"
                        propietat = "SSIndexTab"
                    ElseIf propietat.ToUpper = "TABBACKCOLORDEFAULT" Then
                        observacio = "SSIndexTab"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9990"
                        propietat = "SSIndexTab"


                    ElseIf propietat.ToUpper = "DISCONNECT" Then
                        observacio = "CacheActiveX.Factory"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9960"
                        propietat = "CacheActiveX.Factory"
                    ElseIf propietat.ToUpper = "ISCONNECTED" Then
                        observacio = "CacheActiveX.Factory"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9961"
                        propietat = "CacheActiveX.Factory"
                    ElseIf propietat.ToUpper = "CONNECT" Then
                        observacio = "CacheActiveX.Factory"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9962"
                        propietat = "CacheActiveX.Factory"
                    ElseIf propietat.ToUpper = "DELETEDOC" Then
                        observacio = "CacheActiveX.Factory"
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9963"
                        propietat = "CacheActiveX.Factory"

                    ElseIf propietat.ToUpper = "MDILEFT" Then
                        observacio = "Propietat encapsulada en GmsData"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9950"
                        propietat = "GmsData"
                    ElseIf propietat.ToUpper = "MDITOP" Then
                        observacio = "GmsData"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9950"
                        propietat = "GmsData"
                    ElseIf propietat.ToUpper = "MIDAGRUIXTOP" Then
                        observacio = "GmsData"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9950"
                        propietat = "GmsData"


                    ElseIf propietat.ToUpper = "BACKSTYLE" Then
                        color = ColorUpgrade.Warning
                        observacio = "Solució Juli"


                    ElseIf propietat.ToUpper = "SELECTEDINDEX" Then
                        observacio = "GmsTabControl"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9460"
                        propietat = "GmsTabControl"


                    ElseIf propietat.ToUpper = "X1" OrElse propietat.ToUpper = "X2" OrElse propietat.ToUpper = "Y1" OrElse propietat.ToUpper = "Y2" Then
                        observacio = "LineShape"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9901"
                        propietat = "LineShape"
                    ElseIf propietat.ToUpper = "BORDERWIDTH" Then
                        observacio = "LineShape"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9902"
                        propietat = "LineShape"
                    ElseIf propietat.ToUpper = "SHAPE" Then
                        observacio = "Defineix l'aparenca d'un control.Es de tipus integer"
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9903"
                        propietat = "Shape (LineShape,...)"
                        solucio = "Cal migrar"
                    ElseIf propietat.ToUpper = "DRAWMODE" Then
                        observacio = "https://www.vbmigration.com/Resources/detmigratingfromvb6controls.aspx?Id=9"
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9904"
                        propietat = "Shape (LineShape,...)"
                        solucio = "Cal migrar"


                    ElseIf propietat.ToUpper = "MOVEFIRST" OrElse propietat.ToUpper = "MOVENEXT" OrElse propietat.ToUpper = "MOVELAST" OrElse propietat.ToUpper = "REMOVEITEM" Then
                        observacio = "GmsCombo classe nova.Mètode encapsulat"
                        descripcio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9874"
                        propietat = "SSDBCombo/GmsCombo"
                    ElseIf propietat.ToUpper = "LISTINDEX" Then
                        observacio = "SSDBCombo/GmsCombo"
                        descripcio = ""
                        color = ColorUpgrade.Critic
                        tipusObjecte = "9975"
                        propietat = "SSDBCombo/GmsCombo"
                        solucio = "Cal migrar"
                    ElseIf propietat.ToUpper = "LISTCOUNT" Then
                        observacio = "SSDBCombo/GmsCombo"
                        descripcio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9976"
                        propietat = "SSDBCombo/GmsCombo"
                        solucio = REGEX_CADENA_SUBSTITUCIO & "Items.Count"
                    ElseIf propietat.ToUpper = "LIST" Then
                        observacio = "SSDBCombo/GmsCombo"
                        descripcio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9873"
                        propietat = REGEX_CADENA_SUBSTITUCIO & "Items"


                    ElseIf propietat.ToUpper = "TEXT" Then
                        '! SSDBCOMBO/GMSCOMBO


                    ElseIf propietat.ToUpper = "DECIMALES" Then
                        '! GmsImports
                        Return False  ' NO FER RES

                    ElseIf propietat.ToUpper = "SELTEXT" Then
                        color = ColorUpgrade.Resolt
                        solucio = "Regex : substituit per SelectedText"


                    ElseIf propietat.ToUpper = "TAB" Then
                        observacio = "OBSOLET"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9850"
                        propietat = "SStab/SSIndexTab"
                    ElseIf propietat.ToUpper = "TABHEIGHT" Then
                        observacio = "OBSOLET"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9850"
                        propietat = "SStab/SSIndexTab"
                    ElseIf propietat.ToUpper = "TABCAPTION" Then
                        observacio = "OBSOLET"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9850"
                        propietat = "SStab/SSIndexTab"
                    ElseIf propietat.ToUpper = "TABPICTURE" Then
                        observacio = "OBSOLET"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9850"
                        propietat = "SStab/SSIndexTab"
                    ElseIf propietat.ToUpper = "TABS" Then
                        observacio = "OBSOLET"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9850"
                        propietat = "SStab/SSIndexTab"


                    ElseIf propietat.ToUpper = "SELECTED" Then
                        observacio = "cListItem"
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9997"
                        propietat = "cListItem"


                    ElseIf propietat.ToUpper = "COLOR" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9500"
                        propietat = "FlexCell.ReportTitle"
                    ElseIf propietat.ToUpper = "ALIGN" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9501"
                        propietat = "FlexCell.ReportTitle"
                    ElseIf propietat.ToUpper = "PRINTONALLPAGES" Then
                        observacio = ""
                        color = ColorUpgrade.Resolt
                        tipusObjecte = "9502"
                        propietat = "FlexCell.ReportTitle"
                    ElseIf propietat.ToUpper = PROPIETAT_FONT.ToUpper Then
                        observacio = ""
                        color = ColorUpgrade.Warning
                        tipusObjecte = "9503"
                        propietat = "FlexCell.ReportTitle"
                        solucio = "Cal declarar/instanciar una variablede tipus Font"


                    ElseIf propietat.ToUpper = "VERSIO" Then

                    ElseIf propietat.ToUpper = "EXPLODEDDISTANCEPERCENT" Then
                        '! "ChartPieSeriesStyle"

                    ElseIf propietat.ToUpper = "ROTATION" Then

                    ElseIf propietat.ToUpper = "LABELTEXT" Then
                        '! "ChartSeriesPoint"

                    ElseIf propietat = "" Then

                        '! comprovem si és un metode
                        If RegexCercaCadena(control, "\w+\([\w\,\&\s]*\)") Then
                            Return False   '! NO CAL FER RES
                        Else
                            color = ColorUpgrade.Resolt
                            observacio = "1.-Variable definida en altres projectes, 2.-VB6 no té declarada el tipus, 3.-VB6 variable declarada As Object"
                        End If

                    Else  '! la resta

                        If Not esDuplicatTipusControlPropietat(propietat, "") Then
                            ''''Debug.WriteLine(String.Format("3.------NO_TIPUSOBJECTE : Control : {0} , Propietat : {1}", control, propietat))
                            'Debug.WriteLine(String.Format("*************** Propietat {0} no controlada !!!!!!", propietat))
                            MessageBox.Show(String.Format("*************** Propietat {0} no controlada !!!!!!", propietat))
                        End If
                    End If

                    If esDuplicatTipusControlPropietat(tipusObjecte, propietat) Then Return False

                End If

            Case "6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"

                color = ColorUpgrade.Warning
                solucio = "Manual"

                RegexCercaCadena(descripcio, "(?i)El comportamiento de ([\w+-\.]+) puede ser diferente", splitList)

            Case "6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"
                color = ColorUpgrade.Warning    'colorUpgradeRoot(codi)

                Dim casPropietat As Boolean = False
                Dim casMetode As Boolean = False

                Dim casEvent As Boolean = RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) evento ([\w+-\.]+) tiene un nuevo comportamiento", splitList)
                If Not casEvent AndAlso splitList.Count > 2 Then
                    color = ColorUpgrade.Warning
                    Dim control As String = splitList(1)
                    Dim propietatOMetode As String = splitList(2)

                    casPropietat = RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) propiedad ([\w+-\.]+) tiene un nuevo comportamiento", splitList)
                    If Not casPropietat Then
                        casMetode = RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) método ([\w+-\.\(\)]+) tiene un nuevo comportamiento", splitList)

                        Dim metode As String = propietatOMetode.Split(".")(propietatOMetode.Split(".").Count - 1)   ' ultima cadena separada pel .
                        If metode.ToUpper = "LISTITEMS" Then
                            If propietatOMetode.Split(".")(2).ToUpper = "ENSUREVISIBLE" Then
                                observacio = "EnsureVisible de ListItems"
                            End If
                        ElseIf metode.ToUpper = "ZORDER" Then
                            color = ColorUpgrade.Resolt
                            solucio = "Substituit per BringToFront i SendToBack"
                            observacio = "https://www.vbmigration.com/resources/detmigratingfromvb6controls.aspx?Id=9#431"
                        Else
                            If control = "Controls" Then
                                color = ColorUpgrade.Resolt
                                solucio = "Formparent"
                            Else
                                color = ColorUpgrade.Altres
                                observacio = String.Format("{0} de {1}", metode, control)
                            End If

                        End If
                    Else
                        Dim metode As String = splitList(2).Split(".")(1)

                        If metode.ToUpper = "MAXLENGTH" Then
                            color = ColorUpgrade.Warning
                            observacio = "En NET si assignem un string > MaxLength, no es fa TRIM"
                        ElseIf metode.ToUpper = "MOUSEPOINTER" Then
                            color = ColorUpgrade.Warning
                            solucio = "https://www.vbmigration.com/resources/detmigratingfromvb6.aspx?Id=19#531"
                        ElseIf metode.ToUpper = "SELECTEDITEM" Then
                            If propietatOMetode.Split(".")(2).ToUpper = "IMAGE" Then
                                observacio = "Image d'un TreeNode"
                            End If
                        Else
                            color = ColorUpgrade.Altres
                            observacio = String.Format("{0} de {1}", propietatOMetode.Split(".")(1), control)
                        End If
                    End If
                Else

                    casPropietat = RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) propiedad ([\w+-\.]+) tiene un nuevo comportamiento", splitList)

                    If splitList.Count > 0 Then   ' propietat
                        Dim control As String = splitList(1)
                        Dim propietat As String = splitList(2).Split(".")(1)

                        If propietat.ToUpper.Contains("ACTIVATE") Then
                            color = ColorUpgrade.Resolt
                            solucio = "Formparent"

                        ElseIf propietat.ToUpper.Contains("EXENAME") Then
                            observacio = "https://www.vbmigration.com/detknowledgebase.aspx?Id=121"

                        ElseIf propietat.ToUpper.Contains("MAXLENGTH") Then

                        ElseIf propietat.ToUpper.Contains("MOUSEPOINTER") Then
                            color = ColorUpgrade.Warning
                            solucio = "https://www.vbmigration.com/resources/detmigratingfromvb6.aspx?Id=19#531"
                            observacio = "El Wizard2008 converteix Screen.MousePointer = 0 (VB6) en System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default"

                        ElseIf propietat.ToUpper.Contains("CHANGE") Then

                        ElseIf propietat.ToUpper.Contains("IMAGE") Then

                        Else
                            color = ColorUpgrade.Altres
                        End If

                    Else

                        casMetode = RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) método ([\w+-\.]+) tiene un nuevo comportamiento", splitList)

                        Dim control As String = splitList(1)
                        Dim metode As String = splitList(2).Split(".").Last

                        If metode.ToUpper.Contains("ENSUREVISIBLE") Then

                        ElseIf metode.ToUpper.Contains("REMOVE") Then
                        ElseIf metode.ToUpper.Contains("COUNT") Then
                        ElseIf metode.ToUpper.Contains("ZORDER") Then
                        ElseIf metode.ToUpper.Contains("ADD") Then

                        Else
                            color = ColorUpgrade.Altres
                            solucio = "Formparent"
                        End If

                    End If

                End If

                If casEvent Then
                    Dim tipusEvent As String = splitList(2).Split(".")(1)

                    Return ExisteixEvent(codi, tipusEvent)

                ElseIf casPropietat Then
                    Dim tipusPropietat As String = splitList(2).Split(".")(1)

                    Return ExisteixPropietat(codi, tipusPropietat)

                ElseIf casMetode Then
                    Dim tipusMetode As String = splitList(2).Split(".")(1)

                    Return ExisteixMetode(codi, tipusMetode)
                End If

            Case "9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"
                color = ColorUpgrade.Warning
                solucio = "Manual"

                Dim cas1 As Boolean = RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) se actualizó a ([\w+-\.]+) tiene un nuevo comportamiento.", splitList)

                Dim cas2 As Boolean = False
                If Not cas1 Then
                    cas2 = RegexCercaCadena(descripcio, "(?i)([\w+-\. ]+) se actualizó a \w+", splitList)
                    If splitList.Count > 0 Then

                        If splitList(1) = "TextRTF" Then
                            observacio = "Substituir Text per Rtf"
                            solucio = "Manual"
                        ElseIf splitList(1) = "SelRTF" Then
                            solucio = "Manual"
                            observacio = "No podem aplicar una regex sobre la cadena SelectedText perque no tots hem de substituir"
                        ElseIf splitList(1) = "Put" Then


                        ElseIf splitList(1) = "Get" Then

                        ElseIf splitList(1) = "SavePicture" Then


                        Else
                            color = ColorUpgrade.Altres
                        End If
                    Else
                        RegexCercaCadena(descripcio, "(\w+) tiene un nuevo comportamiento.", splitList)

                        If splitList(1) = "Filter" Then
                        ElseIf splitList(1) = "VarType" Then
                            color = ColorUpgrade.Warning
                            solucio = "Manual.En VB6 retorna sempre Long.En NET pot retornar String,Boolean,Integer,Single,..."
                            observacio = "https://docs.microsoft.com/en-us/office/vba/language/reference/user-interface-help/vartype-function"
                        ElseIf splitList(1) = "Picture" Then
                        ElseIf splitList(1) = "Parent" Then
                        ElseIf splitList(1) = "Dir" Then
                        ElseIf splitList(1) = "TypeOf" Then
                            color = ColorUpgrade.Warning
                            solucio = "https://www.vbmigration.com/resources/detmigratingfromvb6.aspx?Id=6#220"


                        ElseIf splitList(1) = "TypeName" Then
                            color = ColorUpgrade.Warning
                            solucio = "Manual"
                            descripcio = "https://docs.microsoft.com/en-us/dotnet/api/microsoft.visualbasic.information.typename?view=netframework-4.8"
                        ElseIf splitList(1) = "DataValue" Then

                        ElseIf splitList(1) = "Mod" Then

                        Else
                            color = ColorUpgrade.Altres
                        End If

                    End If

                End If
                If cas2 Then
                    If splitList(1) = "TypeName" Then
                        color = ColorUpgrade.Warning
                        observacio = "http://www.vbmigration.com/resources/detmigratingfromvb6.aspx?Id=6#219"
                    ElseIf splitList(1) = "Picture" Then
                        color = ColorUpgrade.Warning
                        observacio = "http://www.vbmigration.com/resources/detmigratingfromvb6controls.aspx?Id=11#459"
                    ElseIf splitList(1) = "Parent" Then
                        color = ColorUpgrade.Warning
                        observacio = "http://www.vbmigration.com/resources/detmigratingfromvb6controls.aspx?Id=9#421"
                    ElseIf splitList(1) = "Dir" Then
                        color = ColorUpgrade.Warning
                        observacio = "http://www.vbmigration.com/resources/detmigratingfromvb6.aspx?Id=6#150"
                    Else
                        color = ColorUpgrade.Warning
                    End If
                End If


            Case "DFCDE711-9694-47D7-9C50-45A99CD8E91E"
                color = colorUpgradeRoot(codi)

                RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) \w+ ([\w+-\.]+) se actualizó a ([\w+-\.]+)", splitList)

                If splitList.Count > 0 Then
                    Dim control As String = splitList(1)
                    Dim propietat As String = splitList(2)
                    Dim propietatMigrada As String = splitList(3)

                    If control = "ComboBox" Then
                        If propietat = "Change" Then
                            color = ColorUpgrade.Resolt
                            observacio = "relacionat amb l'event : se puede desencadenar cuando se inicializa el formulario"
                        End If
                    ElseIf control = "Control" Then
                        If propietat.ToUpper.Contains("PARENT") Then
                            color = ColorUpgrade.Warning
                            solucio = "El comportament és el mateix si el control NO pertany a un UserControl"
                            observacio = "http://www.vbmigration.com/resources/detmigratingfromvb6controls.aspx?Id=9#421"
                        Else
                            color = ColorUpgrade.Altres
                        End If
                    ElseIf control = "MSComDlg.CommonDialog" Then
                        If propietat.ToUpper.Contains("FLAGS") Then
                            If propietatMigrada.ToUpper.Contains("SHOWREADONLY") Then
                                color = ColorUpgrade.Warning
                                observacio = "FileOpenConstants.cdlOFNHideReadOnly/VB6"
                                solucio = REGEX_CADENA_SUBSTITUCIO & propietatMigrada
                            ElseIf propietatMigrada.ToUpper.Contains("CHECKFILEEXISTS") Then
                                color = ColorUpgrade.Warning
                                observacio = "FileOpenConstants.cdlOFNFileMustExist/VB6"
                                solucio = REGEX_CADENA_SUBSTITUCIO & propietatMigrada
                            ElseIf propietatMigrada.ToUpper.Contains("CHECKPATHEXISTS") Then
                                color = ColorUpgrade.Warning
                                observacio = "FileOpenConstants.??????/VB6"
                                solucio = REGEX_CADENA_SUBSTITUCIO & propietatMigrada
                            ElseIf propietatMigrada.ToUpper.Contains("RESTOREDIRECTORY") Then
                                color = ColorUpgrade.Warning
                                observacio = "FileOpenConstants.cdlOFNNoChangeDir/VB6"
                                solucio = REGEX_CADENA_SUBSTITUCIO & propietatMigrada
                            ElseIf propietatMigrada.ToUpper.Contains("OVERWRITEPROMPT") Then
                                color = ColorUpgrade.Warning
                                observacio = "FileOpenConstants.cdlOFNOverwritePrompt/VB6"
                                solucio = REGEX_CADENA_SUBSTITUCIO & propietatMigrada
                            ElseIf propietatMigrada.ToUpper.Contains("FULLOPEN") Then
                                color = ColorUpgrade.Warning
                                observacio = ""
                                solucio = REGEX_CADENA_SUBSTITUCIO & propietatMigrada
                            Else
                                color = ColorUpgrade.Altres
                                observacio = ""
                            End If

                        Else
                            color = ColorUpgrade.Altres
                            observacio = ""
                        End If

                    ElseIf control = "FileOpenConstants" Then
                        color = ColorUpgrade.Warning
                        observacio = "Enum MSComDlg.FileOpenConstants/VB6"
                        solucio = "Cal analitzar i migrar totes les constant de l'Enum"
                    Else
                        color = ColorUpgrade.Altres
                    End If

                End If

                    '! UPGRADE_WARNING: Puede que necesite inicializar las matrices de la estructura NtAuthority, antes de poder utilizarlas
            Case "814DF224-76BD-4BB4-BFFB-EA359CB9FC48"
                color = ColorUpgrade.Resolt

            Case "88B12AE1-6DE0-48A0-86F1-60C0686C026A"
                color = ColorUpgrade.Resolt
                solucio = "Formparent"

                Dim casEvent As Boolean = RegexCercaCadena(descripcio, "(?i)El evento ([\w+-\.]+) se puede desencadenar cuando se inicializa el formulario.", splitList)

                If casEvent Then
                    '! no dupliquem el tipus d'event
                    Return ExisteixEvent(codi, splitList(1).Split(".")(1))
                End If


            Case "8B377936-3DF7-4745-AA26-DD00FA5B9BE1"
                color = ColorUpgrade.Critic
                solucio = "Manual"

                RegexCercaCadena(descripcio, "(?i)La propiedad ([\w+-\.]+) de ([\w+-\.]+) no se admite en Visual Basic .NET.", splitList)

                ' ReadProperties, WriteProperties
            Case "92F3B58C-F772-4151-BE90-09F4A232AEAD"
                color = ColorUpgrade.Warning
                solucio = "Manual"
                observacio = "http://www.vbmigration.com/resources/detmigratingfromvb6.aspx?Id=8#292"

                RegexCercaCadena(descripcio, "(?i)El evento ([\w+-\.]+) ([\w+-\.]+) no se admite", splitList)

                '! UPGRADE_WARNING: La estructura \w+ puede requerir que se pasen atributos de cálculo de referencia como argumento en esta instrucción Declare
                '! Això vol dir que segurament el paràmetre cal definirlo ByRef
            Case "C429C3A5-5D47-4CD9-8F51-74A1616405DC"
                color = ColorUpgrade.Altres

                '! UPGRADE_WARNING: El tamaño de la cadena de longitud fija debe caber en el búfer
            Case "3C1E4426-0B80-443E-B943-0627CD55D48B"
                color = ColorUpgrade.Altres

                '! TreNodes/Add
            Case "DBD08912-7C17-401D-9BE9-BA85E7772B99"
                color = ColorUpgrade.Critic
                solucio = "Manual"

                RegexCercaCadena(descripcio, "(?i)El comportamiento del método ([\w+-\.]+) ha cambiado", splitList)

            Case "E863E941-79BB-4962-A003-8309A0B2BB28"
                color = ColorUpgrade.Resolt

                RegexCercaCadena(descripcio, "(?i)La variable ([\w+-\.\(\)\s\=]+) no puede devolver un entero", splitList)

                Dim variable As String = splitList(1)

                If variable.ToUpper.Contains("PIECE") OrElse variable.ToUpper.Contains("PARTS") Then
                    color = ColorUpgrade.Resolt

                ElseIf variable.ToUpper.Contains("TVWPREVIOUS") OrElse variable.ToUpper.Contains("TVWNEXT") Then
                    color = ColorUpgrade.Resolt
                    solucio = "Formparent"
                Else
                    color = ColorUpgrade.Altres
                End If


                        '! UPGRADE_WARNING: La creación de instancias de clase cambió a pública
            Case "ED41034B-3890-49FC-8076-BD6FC2F42A85"
                color = ColorUpgrade.Altres

                '! UnLoadEvent/Cancel
            Case "FB723E3C-1C06-4D2B-B083-E6CD0D334DA8"
                color = ColorUpgrade.Resolt
                solucio = "Formparent"
                observacio = "Relacionat amb el mètode d'event UnLoadEvent.Nova signatura"

                '! UPGRADE_ISSUE:\sControl\s\w+\sno\sse\spudo\sresolver\sporque\sestá\sdentro\sdel\sespacio\sde\snombres\s\w*
            Case "084D22AD-ECB1-400F-B4C7-418ECEC5E36E"
                color = ColorUpgrade.Resolt
                observacio = "Propietat definida en "

                RegexCercaCadena(descripcio, "Control (\w+) no se pudo resolver porque está dentro del espacio de nombres genérico (\w+)", splitList)

                Dim control As String = splitList(1)
                Dim espaiNoms As String = splitList(2)

                Return ExisteixPropietat(codi, espaiNoms)

                '! Label/BackStyle
            Case "74E732F3-CAD8-417B-8BC9-C205714BB4A7"
                color = ColorUpgrade.Resolt
                solucio = "Modificacions en VB6"

                RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) propiedad ([\w+-\.]+) no se admite en tiempo de ejecución", splitList)

                Return ExisteixPropietat(codi, splitList(2).Split(".")(1))

                '! UPGRADE_NOTE: El bloque #If #EndIf no se actualizó porque la expresión DebugAgenda = 1 no dio como resultado True o ni siquiera se evaluó.
            Case "27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"
                color = ColorUpgrade.Altres

            Case "4E2DC008-5EDA-4547-8317-C9316952674F"
                color = ColorUpgrade.Resolt

                RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) pasó de ser un evento a un procedimiento", splitList)

            Case "6E35BFF6-CD74-4B09-9689-3E1A43DF8969"
                color = ColorUpgrade.Altres
                solucio = "Manual"

                RegexCercaCadena(descripcio, "(?i)El objeto ([\w+-\.]+) no se puede destruir hasta que no se realice la recolección de los elementos no utilizados", splitList)

                '! IsMissing()
            Case "8AE1CB93-37AB-439A-A4FF-BE3B6760BB23"
                color = ColorUpgrade.Resolt

                RegexCercaCadena(descripcio, "(?i)([\w+-\.\(\)]+) ha cambiado a ([\w+-\.]+\(\))", splitList)

            Case "94ADAC4D-C65D-414F-A061-8FDC6B83C5EC"
                color = ColorUpgrade.Resolt

                RegexCercaCadena(descripcio, "(?i)El nombre de la variable ([\w+-\.]+) se cambió a ([\w+-\.]+)", splitList)

                '! Class_Terminate, Class_Initialize,Command_Renamed
            Case "A9E4979A-37FA-4718-9994-97DD76ED70A7"
                color = ColorUpgrade.Resolt

                RegexCercaCadena(descripcio, "(?i)El nombre de la variable ([\w+-\.]+) se cambió a ([\w+-\.]+)", splitList)

            Case "B3FC1610-34F3-43F5-86B7-16C984F0E88E"
                RegexCercaCadena(descripcio, "(?i)La propiedad ([\w+-\.]+) se marcó con comentarios", splitList)

                color = ColorUpgrade.Warning

                observacio = "https://riptutorial.com/vba/example/18935/vb--var-usermemid"

                '! paràmetre CommonDialog a Object
            Case "D0BD8832-D1AC-487C-8AA5-B36F9284E51E"
                color = ColorUpgrade.Resolt

                        '! UPGRADE_TODO: Se debe llamar a "Initialize" para inicializar instancias de esta estructura
            Case "B4BFF9E0-8631-45CF-910E-62AB3970F27B"
                color = ColorUpgrade.Warning

                        '! UPGRADE_TODO: Quite los comentarios y cambie la siguiente línea para devolver el enumerador de colecciones
            Case "95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"
                color = ColorUpgrade.Resolt

            Case "45116EAB-7060-405E-8ABE-9DBB40DC2E86"
                color = ColorUpgrade.Critic

                RegexCercaCadena(descripcio, "(?i)La propiedad ([\w+-\.]+) ([\w+-\.]+) no admite punteros de mouse personalizados", splitList)

                Return ExisteixPropietat(codi, splitList(2).Split(".")(1))


            Case "C54B49D7-5804-4D48-834B-B3D81E4C2F13"
                color = ColorUpgrade.Resolt

                RegexCercaCadena(descripcio, "(?i)([\w+-\.]+) ha cambiado de Constant a Variable", splitList)

                '! No se admite Load instrucción
            Case "B530EFF2-3132-48F8-B8BC-D88AF543D321"
                color = ColorUpgrade.Warning

                RegexCercaCadena(descripcio, "(?i)No se admite ([\w+-\.]+) instrucción", splitList)

                solucio = "Linia eliminada"
                observacio = "Crear una instancia de formulari té més o menys el mateix efecte"

            Case "B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"
                color = ColorUpgrade.Resolt
                solucio = "Regex"

                RegexCercaCadena(descripcio, "(?i)No se puede determinar a qué constante se va a actualizar ([\w+-\.]+)", splitList)

                observacio = "Les labels utilitzen System.Drawing.ContentAlignment."

            Case "E9E157F7-EF0C-4016-87B7-7D7FBBC6EE08"
                color = ColorUpgrade.Warning

                RegexCercaCadena(descripcio, "(?i)Agregue un delegado para el operador ([\w+-\.]+)", splitList)

            Case "367764E5-F3F8-4E43-AC3E-7FE0B5E074E2"
                color = ColorUpgrade.Critic
                observacio = "http://www.vbmigration.com/resources/detmigratingfromvb6.aspx?Id=6#213"

                RegexCercaCadena(descripcio, "(?i)No se admite la función ([\w+-\.]+)", splitList)

            Case "FAE78A8D-8978-4FD4-8208-5B7324A8F795"
                color = ColorUpgrade.Resolt

                RegexCercaCadena(descripcio, "(?i)No se admite la declaración de un parámetro 'As Any'", splitList)

            Case "ED2DEFAA-C59C-4EDB-AF23-73A16C92C3AC"
                color = ColorUpgrade.Resolt

                RegexCercaCadena(descripcio, "(?i)La propiedad de panel ([\w+-\.\(\)]+) se mostrará en mosaico", splitList)

            Case "971F4DF4-254E-44F4-861D-3AA0031FE361"
                color = ColorUpgrade.Warning

                RegexCercaCadena(descripcio, "(?i)En los formularios Windows Forms sólo se admiten fuentes TrueType y OpenType", splitList)

            Case "0FF1188E-27C0-4FED-842D-159C65894C9B"
                color = ColorUpgrade.Resolt

                RegexCercaCadena(descripcio, "(?i)([\w+-\.\(\)]+) se actualizó a Panel y no se puede convertir en PictureBox", splitList)

            Case "C676271D-1A65-46CA-8403-027613C5E565"
                RegexCercaCadena(descripcio, "(?i)El método ([\w+-\.\(\)]+) de .NET no produce ningún valor", splitList)

                '! ScaleMode/Form
            Case "8027179A-CB3B-45C0-9863-FAA1AF983B59"
                color = ColorUpgrade.Warning

                RegexCercaCadena(descripcio, "(?i)La propiedad ([\w+-\.\(\)]+) ([\w+-\.\(\)]+) no se admite", splitList)

                Return ExisteixPropietat(codi, splitList(2).Split(".")(1))

            Case "899FA812-8F71-4014-BAEE-E5AF348BA5AA"
                color = ColorUpgrade.Altres

                RegexCercaCadena(descripcio, "(?i)([\w+-\.\(\)]+) no puede asignar un tipo a otro", splitList)

            Case Else

                Dim K = 1

                Return False
        End Select

        If splitList.Count > 0 Then
            Dim objecte As String = splitList(1)
        End If

        Return True
    End Function


    ''' <summary>
    ''' Eliminem/Invisibilitzem una linia del treelistview d'Upgrade a partir d'una criticitat
    ''' </summary>
    ''' <param name="nomColor"></param>
    Private Sub EliminarUpgradeSegonsCriticitat(ByVal nomColor As String)
        Dim numEliminats As Integer = 0

        '! primer nivell
        Dim compt As Integer = treeListViewUpgrades.Items.Count
        For i = compt - 1 To 0 Step -1
            '! segon nivell
            Dim compt2 As Integer = treeListViewUpgrades.Items(i).Items.Count
            For j = compt2 - 1 To 0 Step -1
                If treeListViewUpgrades.Items(i).Items(j).BackColor.Name = nomColor Then
                    treeListViewUpgrades.Items(i).Items.RemoveAt(j)

                    numEliminats = numEliminats + 1
                End If
            Next

            '! si no en queda cap del segon nivell eliminem el primer nivell
            If treeListViewUpgrades.Items(i).Items.Count = 0 Then
                numEliminats = numEliminats + 1
                treeListViewUpgrades.Items.RemoveAt(i)
            End If
        Next

        RefrescarProces("Upgrades", String.Format("Calculats seleccionats : {0} , Totals : {1}", numTotalUpgradesSeleccionats - numEliminats, numTotalUpgrades), "", "")

        numTotalUpgradesSeleccionats = numTotalUpgradesSeleccionats - numEliminats
    End Sub

    ''' <summary>
    ''' Retorna True/False en funcio de si l'UPGRADE ja s'ha tractat (duplicat) segons el tipusControl i la propietat
    ''' Si no es duplicat, l'afegeix a una llista
    ''' </summary>
    ''' <param name="tipusControl"></param>
    ''' <param name="propietat"></param>
    ''' <returns></returns>
    Private Function esDuplicatTipusControlPropietat(ByVal tipusControl As String, ByVal propietat As String) As Boolean
        If Not tipusControl_Propietat.ContainsKey(tipusControl) Then

            '! creem la llista de propietats
            Dim propietats As New List(Of String)

            '! afegim la propietat
            propietats.Add(propietat)

            '! afegim l'item al diccionari
            tipusControl_Propietat.Add(tipusControl, propietats)

        Else '! si existeix el tipus de control

            Dim propietats As List(Of String) = tipusControl_Propietat.Item(tipusControl)

            For Each p As String In propietats
                If p = propietat Then
                    Return True
                End If
            Next

            '! afegim la propietat que no existia
            propietats.Add(propietat)

            '! afegim l'item al diccionari
            tipusControl_Propietat.Item(tipusControl) = propietats
        End If

        'Debug.WriteLine(String.Format("TipusControl : {0} , Propietat : {1}", tipusControl, propietat))

        Return False
    End Function

    ''' <summary>
    ''' Obté la descripció d'un tipus d'Upgrade (segons Wizard2008)
    ''' https://www.vbmigration.com/detknowledgebase.aspx?Id=4
    ''' </summary>
    ''' <param name="tipusUpgrade"></param>
    ''' <returns></returns>
    Private Function GetDescripcioUpgrade(ByVal tipusUpgrade As String) As String
        Select Case tipusUpgrade
            Case "'UPGRADE_ISSUE"
                Return "No permet que compili"
            Case "'UPGRADE_NOTE"
                Return "Grans canvis"
            Case "'UPGRADE_WARNING"
                Return "Investigar i intentar corregir"
            Case "'UPGRADE_TODO"
                Return "Corregir abans d'executar l'aplicació"
        End Select

        Return String.Empty
    End Function

    ''' <summary>
    ''' Omple el listview amb totes les linies que contene UPGRADE_
    ''' </summary>
    ''' <param name="linia"></param>
    Private Sub OmpleUpgrades_TreeListView(ByVal linia As String)
        Dim splitList() As String = {}

        RegexCercaCadena(linia, "(?i)('UPGRADE_\w+): ([\w+-\.- -\=-?]+) Haga clic[\w+-\.- -?]+\s*=\s*\""(\w+-\w+-\w+-\w+-\w+)\""'", splitList)

        Dim color As ColorUpgrade = Nothing
        Dim solucio As String = String.Empty
        Dim observacio As String = String.Empty

        If splitList.Count > 0 Then
            Dim tipusUpgrade As String = splitList(1)
            Dim descripcio As String = splitList(2)
            Dim codi As String = splitList(3)

            '! comptadors
            If linia.ToUpper.Contains("UPGRADE_ISSUE") Then
                numTotal_Upgrade_Issue = numTotal_Upgrade_Issue + 1
            ElseIf linia.ToUpper.Contains("UPGRADE_NOTE") Then
                numTotal_Upgrade_Note = numTotal_Upgrade_Note + 1
            ElseIf linia.ToUpper.Contains("UPGRADE_WARNING") Then
                numTotal_Upgrade_Warning = numTotal_Upgrade_Warning + 1
            ElseIf linia.ToUpper.Contains("UPGRADE_TODO") Then
                numTotal_Upgrade_Todo = numTotal_Upgrade_Todo + 1
            End If

            If BuscaUpgrade(codi.ToUpper, descripcio, tipusUpgrade, color, solucio, observacio) Then

                '! comprovem segons la criticitat si ho hem de visualitzar
                If color = ColorUpgrade.Critic AndAlso Not chkCritic.Checked Then Exit Sub
                If color = ColorUpgrade.Resolt AndAlso Not chkResolt.Checked Then Exit Sub
                If color = ColorUpgrade.Altres AndAlso Not chkAltres.Checked Then Exit Sub
                If color = ColorUpgrade.Warning AndAlso Not chkWarning.Checked Then Exit Sub

                numTotalUpgradesSeleccionats = numTotalUpgradesSeleccionats + 1

                Dim upgrade As Upgrade = New Upgrade(descripcio, solucio, observacio, tipusUpgrade, color)

                Dim lstUpgrade As List(Of Upgrade)

                If Not upgrades.ContainsKey(codi.ToUpper) Then
                    lstUpgrade = New List(Of Upgrade)
                    upgrades.Add(codi.ToUpper, lstUpgrade)
                Else
                    lstUpgrade = upgrades.Item(codi.ToUpper)
                End If

                lstUpgrade.Add(upgrade)
            End If
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="criticitat"></param>
    ''' <param name="descripcio"></param>
    ''' <param name="observacio"></param>
    ''' <param name="url"></param>
    ''' <param name="regexCerca"></param>
    ''' <param name="categoria"></param>
    ''' <param name="nouEstat"></param>
    ''' <param name="textCerca"></param>
    Private Sub AfegirWorkAroundArquitectura_v2(ByVal criticitat As CriticitatMigracioError, ByVal descripcio As String, ByVal observacio As String, ByVal url As String, ByVal regexCerca As String, ByVal categoria As CategoriaMigracioError, ByVal nouEstat As ColorUpgrade, Optional textCerca As String = "")
        '! criticitats
        If criticitat = CriticitatMigracioError.BUG AndAlso Not chkBUG.Checked Then Exit Sub
        If criticitat = CriticitatMigracioError.HOWTO AndAlso Not chkHOWTO.Checked Then Exit Sub
        If criticitat = CriticitatMigracioError.INFO AndAlso Not chkINFO.Checked Then Exit Sub
        If criticitat = CriticitatMigracioError.PRB AndAlso Not chkPRB.Checked Then Exit Sub

        '! categories
        If categoria = CategoriaMigracioError.CLASSES AndAlso Not chkClasses.Checked Then Exit Sub
        If categoria = CategoriaMigracioError.GENERAL AndAlso Not chkGeneral.Checked Then Exit Sub
        If categoria = CategoriaMigracioError.LANGUAGE AndAlso Not chkLanguage.Checked Then Exit Sub
        If categoria = CategoriaMigracioError.FORMSCONTROLS AndAlso Not chkFormsControls.Checked Then Exit Sub
        If categoria = CategoriaMigracioError.DATABASE AndAlso Not chkDatabase.Checked Then Exit Sub
        If categoria = CategoriaMigracioError.ADO AndAlso Not chkADO.Checked Then Exit Sub

        '! nouEstat
        If nouEstat = ColorUpgrade.Resolt AndAlso Not chkRevisats.Checked Then Exit Sub
        If nouEstat = ColorUpgrade.Altres AndAlso Not chkNoRevisats.Checked Then Exit Sub
        If nouEstat = ColorUpgrade.Sense AndAlso Not chkNA.Checked Then Exit Sub

        '! si tenim texte de cerca
        If Not String.IsNullOrEmpty(textCerca) Then
            If Not descripcio.ToUpper.Contains(textCerca.ToUpper) Then Exit Sub
        End If

        Dim lvi As New ListViewItem()

        lvi.BackColor = GetColorUpgrade(nouEstat)

        lvi.SubItems.Add(criticitat.ToString)
        lvi.SubItems.Add(descripcio)
        lvi.SubItems.Add(observacio)
        lvi.SubItems.Add(url)
        lvi.SubItems.Add(regexCerca)

        lviArquitectura.Items.Add(lvi)
    End Sub

    ''' <summary>
    ''' Afegim tots els Workaround d'arquitectura (Runtime errors, Compile errors,...)
    ''' </summary>
    Private Sub AfegirWorkAroundsArquitectura(Optional ByVal texteCerca As String = "")
        Const REGEX_NO_EXISTEIX As String = "No existeix regla"

        Dim fitxerWorkAroundsArquitectura As String = String.Format("{0}\{1}", Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory).ToString).ToString, "workarounds_arquitectura.txt")
        Dim liniesWorkAroundsArquitectura As List(Of String) = LlegeixFitxer(fitxerWorkAroundsArquitectura, GetFileEncoding(fitxerWorkAroundsArquitectura))

        Dim categoria As CategoriaMigracioError = CategoriaMigracioError.GENERAL   ' es la categoria que es troba primer dins el fitxer

        For Each linia As String In liniesWorkAroundsArquitectura
            '! excloim els que contenen --
            If linia.Contains("--") Then Continue For

            Dim splitList() As String = Split(linia, "||")   ''linia.Split("||")

            If splitList.Count = 1 Then   '! categoria
                If linia = "Classes and COM objects" Then
                    categoria = CategoriaMigracioError.CLASSES
                ElseIf linia = "General" Then
                    categoria = CategoriaMigracioError.GENERAL
                ElseIf linia = "Language" Then
                    categoria = CategoriaMigracioError.LANGUAGE
                ElseIf linia = "Forms and controls" Then
                    categoria = CategoriaMigracioError.FORMSCONTROLS
                ElseIf linia = "Database and data-binding" Then
                    categoria = CategoriaMigracioError.DATABASE
                ElseIf linia = "ADOLibrary" Then
                    categoria = CategoriaMigracioError.ADO
                End If

            Else
                Dim criticitat As CriticitatMigracioError = GetCriticitatWarningArquitectura(splitList(0))
                Dim url As String = splitList(1)
                Dim descripcio As String = splitList(2)
                Dim nouEstat As ColorUpgrade

                If splitList(3) = "ColorUpgrade.Altres" Then
                    nouEstat = ColorUpgrade.Altres
                ElseIf splitList(3) = "ColorUpgrade.Resolt" Then
                    nouEstat = ColorUpgrade.Resolt
                ElseIf splitList(3) = "ColorUpgrade.Sense" Then
                    nouEstat = ColorUpgrade.Sense
                ElseIf splitList(3) = "ColorUpgrade.GestioEvents" Then
                    nouEstat = ColorUpgrade.GestioEvents
                End If

                AfegirWorkAroundArquitectura_v2(criticitat, descripcio, "", url, REGEX_NO_EXISTEIX, categoria, nouEstat, texteCerca)
            End If
        Next



        'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Reduce runtime errors caused by null strings", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=118", REGEX_NO_EXISTEIX, CategoriaMigracioError.LANGUAGE)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Late binding or undetected conversions", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=18", REGEX_NO_EXISTEIX, CategoriaMigracioError.LANGUAGE)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "ObjectDisposed exception where re-opening a closed form", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=633", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Form_Load events in default form instances", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=728", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The migrated application takes too long at loading its first form", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=656", REGEX_NO_EXISTEIX, CategoriaMigracioError.LANGUAGE)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The Load event of a form doesn't fire when you access its property or control from another form", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=323", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The system hangs when using SendKeys to simulate key presses in an external application", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=56", REGEX_NO_EXISTEIX, CategoriaMigracioError.LANGUAGE)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Unsupported ActiveX controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=9", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Reduce flickering when loading or resizing a form with many controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=692", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "How VB Migration Partner implements drag-And-drop features", "PENDENT", "https://www.vbmigration.com/detknowledgebase.aspx?Id=609", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The focus doesn't go to the parent window when closing a modal form", "INTERESSANT: PODEM INCLOURE EN FORMPARENT", "https://www.vbmigration.com/detknowledgebase.aspx?Id=735", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Reduce flickering in graphic operations", "En Form, PictureBox i UserControl.Considerar utilitzar la propietat DoubleBuffered", "https://www.vbmigration.com/detknowledgebase.aspx?Id=625", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "A form behaves differently the second time it Is opened", "Relacionat amb l'excepcio : System.ArgumentException: Property can only be set to Nothing", "https://www.vbmigration.com/detknowledgebase.aspx?Id=27", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Permanently display label And button accelerators", "relacionat amb els caracters amb underscore que hi ha en labels de butons", "https://www.vbmigration.com/detknowledgebase.aspx?Id=364", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The VB.NET application ends with a fatal exception when the splash screen closes", "NO ENTENC DEL TOT", "https://www.vbmigration.com/detknowledgebase.aspx?Id=344", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "ObjectDisposed exception where re-opening a closed form", "IMPORTANT", "https://www.vbmigration.com/detknowledgebase.aspx?Id=633", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Form - level variables in default forms don't retain their values between calls", "IMPORTANT", "https://www.vbmigration.com/detknowledgebase.aspx?Id=642", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "NullReference exception in Form_Initialize", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=614", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Determine the font used for forms And controls", "non TrueType fonts", "https://www.vbmigration.com/detknowledgebase.aspx?Id=20", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "A reference to the Controls collection isn't converted to Controls6", "Coleccio Controls VB6 versus NET", "https://www.vbmigration.com/detknowledgebase.aspx?Id=311", "\.Parent\.Controls", CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "A nonexisting property Is generated for a control in the *.Designer.vb file", "exemple : DTPicker/DateIsNull", "https://www.vbmigration.com/detknowledgebase.aspx?Id=347", "\.DateIsNull\s+=\s+-1", CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "VB.NET controls are taller than the original VB6 control", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=365", "(?<=\.Font = New System\.Drawing\.Font\(.+?,\s+[0-9.]+)!", CategoriaMigracioError.FORMSCONTROLS)
        'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Form_Load events in default form instances", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=728", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Define a more reliable Ambient.UserMode property", "PENDENT", "", "\bAmbient\.UserMode", CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The VB.NET application hangs on the splash screen Or terminates earlier than expected", "CONSIDERAR", "", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Setting the Font property at runtime causes an unexpected Paint event which may freeze the application", "", "", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Setting the Font property inside a Paint event causes an endless loop", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=667", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "A form loaded with Load method might cause errors in its Unload event handler", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=684", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Speed up For Each loops that iterate over the Controls collection", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=21", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Iterate over all forms of current application", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=22", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "ScaleHeight And ScaleWidth properties aren't correctly serialized at design-time", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=674", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Change the value of a given property of all controls of specified type Or name", "CONSIDERAR", "", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Multiple Or missing Paint events", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=652", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The CLS method fails to refresh child controls on the form", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=653", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Manually invoking the Paint event handler may Not deliver the expected results", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=714", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Drag -And -drop operations inside a ListBox control may deliver fake results", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Elements of control arrays can't be referenced before being created", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "PictureBox, Frame And other container controls don't correctly process the Tab key if TabStop=False", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=589", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Forms with same name as one of their controls can't be opened in Visual Studio designer", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Arrays of menu controls can't contain separators", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=26", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "All ListBox And Frame controls in an array must have same style", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=601", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Can't mix VB graphic properties and GDI native calls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=654", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Graphic output Is cleared when the form becomes visible", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=28", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Negative values for ScaleWidth And ScaleHeight properties cause incorrect output in Microsoft Vista", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Closing a form causes the ObjectDisposed exception", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=637", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "An MDI Child form can mistakenly display a menu bar", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=655", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Menus on MDI child forms doesn't replace the toplevel menu of the MDI parent form", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "VB.NET forms don't display the size grip handle", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Accessing a control from the Initialize event handler causes a NullReference exception", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=713", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "MDI child forms don't become visible", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=34", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Assignments to Font properties generate runtime errors in late-bound mode", "Propietats de l'objecte Font son ReadOnly", "https://www.vbmigration.com/detknowledgebase.aspx?Id=651", "\b(?<ctrl>.+?)\.Font\.(?<prop>Name|Size|Bold|Italic|Underline)\s*=\s*(?<value>.+?)(?=\r\n)", CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The font of a control changes unexpectedly at runtime", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=35", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Object variables inside Form_Initialize And UserControl_Initialize events are always Nothing", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=738", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Arrays of menu items can throw an exception if items are destroyed too early", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=36", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Invoking the Show method of MDI child forms containing one Or more ActiveX controls might Not fire the Load event", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=673", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "ActiveX controls aren't migrated correctly", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=602", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Assignments to VB6 Form's Left and Top properties are ignored", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=38", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Using the hDC property causes an InvalidOperationException error", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=40", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Setting the TabIndex property in VB.NET doesn't affect the TabIndex of other controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=42", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Controls in migrated VB.NET forms may be hidden by other controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=44", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Disabled controls And read-only TextBox controls appears with grayed background", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=45", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Selecting the contents of a TextBox when the control gets the input focus doesn't work in VB.NET", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=324", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Setting the Visible=False For a Frame may cause spurious GotFocus, LostFocus, And Validate() events", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=675", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Shape And Line controls aren't updated correctly", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Forms with Line And Shape controls may exhibit a lot of flickering", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "PictureBox controls can be cleared unexpectedly Or receive spurious Paint events", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=49", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The MsgBox, InputBox, And Common dialogs cause spurious LostFocus And GotFocus events", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=664", "\(\) Handles (?<ctrl>\w+)\.GotFocus", CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Convert from OLE types to .NET types", "Conversio dels objectes StdPicture and StdFont de VB6 en els corresponents en NET", "https://www.vbmigration.com/detknowledgebase.aspx?Id=355", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Runtime errors in Form_Load events in 64-bit apps", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Leverage the features of native .NET controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Leverage.NET TextAlign Property With command, CheckBox, And option button controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=693", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between CommonDialog, Printer, Line, And Shape VB6 classes And their implementation in CodeArchitects.VBPowerPack.dll", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=694", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "VB Migration Partner uses the Line, Shape, And Printer classes in Microsoft VB PowerPacks", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Migrate binary properties of nested user controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Speed up execution by removing unnecessary Refresh methods", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=50", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "VB.NET automatically sets the Checked property of the first control in a group of Option buttons", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Image controls whose Picture property Is Nothing aren't transparent", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=61", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, ".NET behaves differently from VB6 if you disable a checked OptionButton control in Form_Load event handler", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=715", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Transparent VB6 Label controls aren't transparent in the VB.NET application", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=64", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Mouse event aren't received by transparent Image and Label controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=120", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Label And Image controls appear in front of other controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=658", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "DoEvents6 calls can cause weird user interface behaviors", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET implementation of DDE", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Timers are disabled inside VB6 IDE when a MsgBox Or InputBox statement Is active", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=65", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Double - clicking on a control array element at design-time creates a brand New event handler", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=67", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Implementing the Enter/Leave event handlers for a control array", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=730", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Replacing a VB6Placeholder control with another control causes an error in the VB.NET designer", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The VB6 form has border, but the migrated VB.NET form hasn't", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=69", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "ScrollBars in MDI forms are always visible", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=627", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "'Classic'(VB3 - style) drag-And-drop stops working", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The ClientSize property of a form can return an incorrect value", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=731", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences among different versions of Microsoft Windows Common Controls library", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "User - defined coordinate systems can have unexpected results", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=71", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)

                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Controlling the LostFocus And Validate event sequence", "Ordre Validating i Lostfocus events", "https://www.vbmigration.com/detknowledgebase.aspx?Id=331", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Controlling the GotFocus And MouseDown event sequence", "Ordre MouseDown i Gotfocus events", "https://www.vbmigration.com/detknowledgebase.aspx?Id=332", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET TreeView controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=312", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "HitTest method of the TreeView control can return different values in VB.NET", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=125", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Images displayed In TreeView, ListView, And other controls aren't transparent", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=73", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET StatusBar controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=313", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET ImageCombo controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET DTPicker controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=315", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET MonthView controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=316", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET UpDown controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET SSTab controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=318", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET ListView controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=307", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET Toolbar controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=326", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET MSChart controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "How To migrate the PictureDn, PictureUp, And PictureDisabled properties of SSCommand And SSRibbon controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "It's impossible to set the initial tab in an SSTab control", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=717", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Create an instance of the VB6ImageList control at runtime", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=716", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Top property of controls hosted inside an SSTab control Is relative to TabPage border", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Forms containing an ImageList control don't display at design-time after updating the CodeArchitects.VBLibrary DLL.", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Setting the current tab in a SSTab control brings the input focus to the control", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Hotkeys in SSTab caption work only if the form's KeyPreview property is set to True", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET ImageList controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=610", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET Command controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=376", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Binary properties of a few controls aren't migrated correctly", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Assignments to the ListView.SelectedItem property are ignored if the control isn't visible", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=325", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET DriveListBox, DirListBox, And FileListBox controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The ListView's background image doesn’t migrate correctly", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=74", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Deleting a Toolbar, ListView, Or StatusBar control prevents the form from loading in Visual Studio designer", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "The.NET Toolbar control can't contain standard controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=77", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Move a control over a toolbar at runtime", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The UpDown control can affect the wrong property of its buddy control", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Removing an item from a multi-selectable ListBox resets ListIndex property", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=80", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Check an item of a ListBox with Style=CheckBoxes with a single mouse click", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=668", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Removing an item from a single-selectable ListBox selects the previous item", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=660", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "SmallChange property can't be higher than LargeChange", "HScrollBar, VScrollBar, FlatScrollBar, WLHScroll, and WLVScroll", "https://www.vbmigration.com/detknowledgebase.aspx?Id=686", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "ScrollBar controls ignore the LargeChange property", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=81", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "DTPicker controls display their content using the LongDate format", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=82", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The DTPicker control doesn't accept dates earlier than 1/1/1753", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET TabStrip controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=341", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET RichTextBox controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=319", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "SelFontName, SelFontSize, SelBold, SelItalic, SelUnderline, And SelStrikeThru properties of RichTextBox control return Nothing instead of Null", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=85", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Invalid assignments to the Rtf property might Not throw an exception", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=86", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Fields are restored in a different way if an error occurs with a Data control", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET DataList controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=321", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET DataCombo controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET RemoteData controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET DataGrid controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Minor differences between VB6 And VB.NET controls belonging to the Threed (ActiveTreedPlus) control library", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Migrating VB6 reports to NET", "CrystalReports", "https://www.vbmigration.com/detknowledgebase.aspx?Id=643", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "WLOption controls ignore the Group property", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "The cdlOFNShareAware flag of the CommonDialog control isn't supported", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "TabStrip And SSTab controls don't display hotkeys in button captions", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.BUG, "The Scroll event of the VB6FlatScrollBar control doesn't fire repeatedly while dragging the indicator", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=91", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Create custom labels in the tab portion of an SSTab control", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.BUG, "The MouseIcon isn't migrated for MMControl and MSHFlexGrid controls", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Properties of the MSChart control might Not be migrated correctly", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "GotFocus And LostFocus events in UserControls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=335", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Serializable properties in UserControls using system objects may crash Visual Studio", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=634", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Simplify code produced for UserControl classes", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=96", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Property references in a form might Not match property names in a UserControl", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=98", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Errors When re-opening modal, default forms that host an ActiveX control", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=688", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The Default And Cancel properties of a VB6UserControl object don't behave like in VB6", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Stand - alone UserControl, UserDocument, And PropertyPage keywords might Not be converted correctly", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Private UserControls aren't loaded with Controls.Add method", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=605", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The application stops with the 'ActiveX control Is windowless. Windowless ActiveX controls aren't supported' error message", "InvalidOperationException: el control ActiveX es un control sin ventanas", "https://www.vbmigration.com/detknowledgebase.aspx?Id=698", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Common problems when generating an ActiveX wrapper with AxWrapperGen", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "UserControl classes don't raise the InitProperties, ReadProperties and WriteProperties events", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=103", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Can't use Tab key to move the focus to a DataGrid ActiveX control", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Mouse custom cursor in drag-And-drop operations with ActiveX controls doesn't work correctly", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Write an extender DLL that can manipulate design-time properties of converted controls", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=665", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Retrieving a reference to the actual ActiveX control wrapped in a class generated with AxWrapperGen", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "You need a runtime license for RDO And for ActiveX controls when running on a computer on which VB6 isn't installed", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The list area of a DataCombo Or DataList control isn't automatically updated", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Form Left And Top properties Not correctly migrated if StartupPosition Is Manual", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=703", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The ADODC Data control doesn't work with server-side cursors that use Access databases", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Common issues When converting VB6 applications that use type libraries", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=66", REGEX_NO_EXISTEIX, CategoriaMigracioError.CLASSES)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "A reference To a COM type library Is missing", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=70", REGEX_NO_EXISTEIX, CategoriaMigracioError.CLASSES)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, ".NET 4.0 TlbImp tool generates wrong wrappers for optional Boolean parameters", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=719", REGEX_NO_EXISTEIX, CategoriaMigracioError.CLASSES)

                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The VB.NET application throws a NullReference exception (Or some other unexpected error) during the initialization phase", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=72", REGEX_NO_EXISTEIX, CategoriaMigracioError.CLASSES)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Get rid of the 'Class' suffix used for COM classes", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Use different versions of same COM type library Or ActiveX control", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Members in COM libraries might be missing Or raise unexpected errors", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=379", REGEX_NO_EXISTEIX, CategoriaMigracioError.CLASSES)

                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Deal With Null, Empty, And missing values", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=339", REGEX_NO_EXISTEIX, CategoriaMigracioError.CLASSES)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Ensure that properties with both Property Let And Property Set are translated correctly", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=597", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)

                'AfegirWorkAroundArquitetura(CriticitatMigracioError.INFO, "Properties may be migrated without any warning related to VB6's ByRef parameters being converted to VB.NET’s ByVal parameters", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=84", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Change the base class of a VB.NET class", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Work with auto-instancing arrays", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=87", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "Objects returned from COM methods may cause an InvalidCastException error", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=89", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.PRB, "The ScriptControl can't interact with classes that aren’t visible to COM", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Avoid compilation errors caused by unsupported designers", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Fix VB6 client applications that reference a COM DLL that has migrated to VB.NET using binary compatibility", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Optimize startup time in N-tier applications", "", "")
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Expose arrays with nonzero lower index to COM clients", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=733", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                'AfegirWorkAroundArquitetura(CriticitatMigracioError.HOWTO, "Avoid reentrancy problems in free-threaded .NET components", "", "https://www.vbmigration.com/detknowledgebase.aspx?Id=591", REGEX_NO_EXISTEIX, CategoriaMigracioError.FORMSCONTROLS)
                End Sub

    ''' <summary>
    ''' Bugs detectats mentre hem testejat les aplicacions
    ''' </summary>
    Private Sub CalculaWorkAroundsBugs()
        Cursor = Cursors.WaitCursor

        lviBugs.Clear()

        FormatListViewWarnings(lviBugs, New List(Of String)({"#", "Categoria", "Linia", "Fitxer", "Num", "Solucio"}), New List(Of Integer)({10, 150, 100, 200, 300, 300}))

        Dim numprocessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numProcessat = numProcessat + 1

            Dim fitxerCodiNET As String = CalculaFitxerProcessar(fitxerVB.Text)

            If fitxerCodiNET Is Nothing Then
                MsgBox("Cal iniciar un nou procés de migració per aquest projecte", MsgBoxStyle.Information, "Error")

                Exit Sub
            End If

            If Not File.Exists(fitxerCodiNET) Then Continue For

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerCodiNET, encoding)

            Dim numLinia As Integer = 0
            For Each linia As String In liniesFitxerVBNet
                WorkAround_Bugs(numLinia, fitxerVB.Text, liniesFitxerVBNet)
                numLinia = numLinia + 1
            Next

            RefrescarProces("Bugs detectats", fitxerVB.Text, numprocessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)
        Next

        '? ***************************************************************************************
        '? AQUI POSEM TOTS ELS BUGS QUE HEM DETECTAT I QUE CAL ARREGLAR MANUALMENT

        WorkAround_Bug("", TipusBug.MANUAL, Color.Pink, "frmExecutaProcesClasse:: 1.- Afegeix : AutoSize = True (Designer) , 2.- Afegeix la linia : Me.Text = """" (Designer) ", "n.a.", "frmExecutaProcesClase.vb", -1, "n.a.")

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    Private Sub CalculaWorkAround_FormLoad()
        Cursor = Cursors.WaitCursor

        lvFormLoad.Clear()

        FormatListViewWarnings(lvFormLoad, New List(Of String)({"#", "Fitxer", "Num Linia", "Descripcio"}), New List(Of Integer)({10, 350, 50, 200}))

        Dim numProcessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numProcessat = numProcessat + 1

            Dim fitxerProcessar As String = cboDirectoryNet.Text & fitxerVB.Text

            If Not File.Exists(fitxerProcessar) Then Continue For

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)

            RefrescarProces("lvFormLoad", fitxerProcessar, numProcessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)

            WorkAround_FormLoad(fitxerProcessar, liniesFitxerVBNet)
        Next

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' WORKAROUND : 
    ''' BUG DETECTAT : que passa si el nom de la classe/UserControl té el mateix nom que el nom de l'instancia d'un control ? 
    ''' Exemple : el UserControl GmsSetmana i el control GmsSetmana del formulari frmAvisosProgramats.
    ''' El Formparent executa els events segons l'ordre adequat.En primer lloc s'executen els events de classe i després els events de l'instancia.
    ''' Al recuperar els "delegats", com es recupera pel nom del mètode i aquest és el mateix, es "raise" 2 vegades l'event
    ''' </summary>
    Private Sub CalculaWorkAround_NomClasseUserControl_igual_NomInstancia()
        Cursor = Cursors.WaitCursor

        lviNomClasseUserControl_igual_nomInstancia.Clear()

        FormatListViewWarnings(lviNomClasseUserControl_igual_nomInstancia, New List(Of String)({"#", "Fitxer"}), New List(Of Integer)({10, 500}))

        Dim numProcessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numProcessat = numProcessat + 1

            Dim fitxerProcessar As String = cboDirectoryVB6.Text & fitxerVB.Text.ToUpper.Replace(".VB", EXTENSIO_FRM).Replace(".DESIGNER", "")

            If Not File.Exists(fitxerProcessar) Then Continue For

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)

            RefrescarProces("lviNomClasseUserControl_igual_nomInstancia", fitxerProcessar, numProcessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)

            WorkAround_NomClasseUserControl_igual_nomInstancia(fitxerProcessar, liniesFitxerVBNet)
        Next

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    '''  WORKAROUND : el procés de migració (Wizard 2008) quan troba un formulari que té la propietat Caption, migra la propietat Caption a la propietat Text 
    '''  PERÒ si el valor és "", li posa el nom del formulari.Això pot provocar efectes colaterals quan es modifica la propietat Text en Runtime
    ''' Exemple : frmExecutaProcesClase --> Load --> Me.Text = ""
    ''' </summary>
    Private Sub CalculaWorkAround_Formularis_Sense_Caption()

        If chkMigrarBackup.Checked Then
            MessageBox.Show("Has de desmarcar el check perquè busca en els designers")
            Exit Sub
        End If


        Cursor = Cursors.WaitCursor

        lviFormularis_Sense_Caption.Clear()

        FormatListViewWarnings(lviFormularis_Sense_Caption, New List(Of String)({"#", "Fitxer"}), New List(Of Integer)({10, 500}))

        Dim numProcessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numProcessat = numProcessat + 1

            Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not File.Exists(fitxerProcessar) Then Continue For

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)

            RefrescarProces("Formularis_Sense_Caption", fitxerProcessar, numProcessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)

            WorkAround_Formularis_Sense_Caption(fitxerProcessar, liniesFitxerVBNet)
        Next

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    Private Sub CalculaWorkAround_Coleccio_1_0()
        Cursor = Cursors.WaitCursor

        lviColeccio1_0.Clear()

        FormatListViewWarnings(lviColeccio1_0, New List(Of String)({"#", "Linia", "Linia_subtituida", "Control", "Fitxer", "Num"}), New List(Of Integer)({10, 500, 500, 200, 200, 100}))

        Dim numprocessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numprocessat = numprocessat + 1

            Dim fitxerCodiNET As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not File.Exists(fitxerCodiNET) Then Continue For

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerCodiNET, encoding)

            Dim numlinia As Integer = 0
            For Each linia As String In liniesFitxerVBNet
                WorkAround_Coleccio1_0(fitxerVB.Text, numlinia, liniesFitxerVBNet)
                numlinia = numlinia + 1
            Next

            numlinia = 0
            For Each linia As String In liniesFitxerVBNet
                WorkAround_Matriu1_0(fitxerVB.Text, numlinia, liniesFitxerVBNet)
                numlinia = numlinia + 1
            Next

            RefrescarProces("Coleccio_1_0", fitxerVB.Text, numprocessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)
        Next

        For Each fitxerVB As ListViewItem In lstItemsFoundNet_2.CheckedItems
            numprocessat = numprocessat + 1

            Dim fitxerCodiNET As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not File.Exists(fitxerCodiNET) Then Continue For

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerCodiNET, encoding)

            Dim numlinia As Integer = 0
            For Each linia As String In liniesFitxerVBNet
                WorkAround_Coleccio1_0(fitxerVB.Text, numlinia, liniesFitxerVBNet)
                numlinia = numlinia + 1
            Next


            numlinia = 0
            For Each linia As String In liniesFitxerVBNet
                WorkAround_Matriu1_0(fitxerVB.Text, numlinia, liniesFitxerVBNet)
                numlinia = numlinia + 1
            Next

            RefrescarProces("Coleccio_1_0", fitxerVB.Text, numprocessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)
        Next

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Errors d'Arquitectura que poden provocar Runtimer Errors
    ''' Exemple : https://www.vbmigration.com/detknowledgebase.aspx?Id=27
    ''' </summary>
    Private Sub CalculaWorkAroundsArquitectura(ByVal textCerca As String)
        Cursor = Cursors.WaitCursor

        lviArquitectura.Clear()

        FormatListViewWarnings(lviArquitectura, New List(Of String)({"#", "Criticitat", "Descripcio", "Observacio", "URL"}), New List(Of Integer)({20, 100, 500, 600, 400}))

        AfegirWorkAroundsArquitectura(textCerca)

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    Private Sub CalculaWorkAround_NomMetodeEvent_Igual_NomEventHandler()
        Cursor = Cursors.WaitCursor

        lvWarningsWorkAround.Clear()

        FormatListViewWarnings(lvWarningsWorkAround, New List(Of String)({"#", "NomControl", "Linia", "Fitxer", "Num"}), New List(Of Integer)({10, 150, 1000, 200, 300}))

        Dim numprocessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numprocessat = numFitxerProcessat + 1

            Dim fitxerDesigner As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not chkMigrarDesigner.Checked Then   ' calcular el corresponent fitxer designer
                fitxerDesigner = ""
            ElseIf chkMigrarDesigner.Checked AndAlso chkMigrarCodi.Checked Then
                'If Not EsFitxerDesigner(fitxerDesigner) Then Continue For  ' si no es designer, saltem
            End If

            controlsNetByForm = ObtenirControlsByDesigner(fitxerDesigner)


            Dim fitxerCodiNET As String = CalculaFitxerProcessar(fitxerVB.Text.Replace(".Designer", ""))

            If Not File.Exists(fitxerCodiNET) Then Continue For

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerCodiNET, encoding)

            For Each linia As String In liniesFitxerVBNet
                WorkAround_NomMetodeEvent_Igual_NomEventHandler(linia, fitxerCodiNET, liniesFitxerVBNet.IndexOf(linia))
            Next

            RefrescarProces("NomMetodeEvent_Igual_NomEventHandler", fitxerVB.Text, numProcessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)

        Next

        Cursor = Cursors.Default
    End Sub


    'Dim bag As Hashtable

    'Public Property OcxState() As System.Windows.Forms.AxHost.State

    '    Get
    '        Return Nothing
    '    End Get

    '    Set(ByVal state As System.Windows.Forms.AxHost.State)

    '        Dim stateType As Type = GetType(System.Windows.Forms.AxHost.State)
    '        Dim bindingFlags As BindingFlags = BindingFlags.GetField + BindingFlags.Instance + BindingFlags.NonPublic
    '        Dim propBag As Object = stateType.InvokeMember("propBag", bindingFlags, Nothing, state, Nothing)

    '        If propBag Is Nothing Then

    '            Dim buffer As Byte() = stateType.InvokeMember("buffer", bindingFlags, Nothing, state, Nothing)

    '            Dim stream As MemoryStream = New MemoryStream(buffer)

    '            Dim formatter As System.Runtime.Serialization.Formatters.Binary.BinaryFormatter = New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter

    '            Try
    '                bag = CType(formatter.Deserialize(stream), Hashtable)
    '            Catch
    '                bag = New Hashtable
    '            End Try

    '        Else
    '            bag = propBag.GetType().InvokeMember("bag", bindingFlags, Nothing, propBag, Nothing)
    '        End If

    '    End Set
    'End Property

    ''' <summary>
    ''' Sembla ser que els TreeView dins AxGroupBox (en un formulari MDIChild!!!), fa que el backcolor sigui el del 
    ''' formulari MDI
    ''' Solucio : substituir AxGropBox per Panel (WinForms)
    ''' Exemple : 
    ''' </summary>
    Private Sub CalculaWorkAround_TreeViewDinsAxGroupBox()
        ' Busquem els controls de tipus TreeView que estan dins un AxGroupBox
        Cursor = Cursors.WaitCursor

        lviTreeViewAxGroupBox.Clear()

        FormatListViewWarnings(lviTreeViewAxGroupBox, New List(Of String)({"#", "Fitxer", "AxGroupBox", "Treeview"}), New List(Of Integer)({10, 600, 200, 200}))

        Dim numProcessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numProcessat = numProcessat + 1

            Dim fitxerFrm As String = CalculaFitxerFrm(fitxerVB.Text)
            Dim nouFitxerVB As String = CalculaNouFitxerVB(fitxerVB.Text)

            Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not File.Exists(fitxerProcessar) Then
                Continue For
            End If

            '! si estan els dos marcats hem d'evitar processar 2 vegades.El que no es designer no el tractem
            If chkMigrarDesigner.Checked AndAlso chkMigrarCodi.Checked Then
                If nouFitxerVB.ToUpper.Contains("DESIGNER.VB") Then Continue For
            End If


            Dim fitxerDesigner As String = String.Empty

            If chkMigrarBackup.Checked = True Then
                fitxerDesigner = CarpetaDotNET() & "\" & nouFitxerVB
            Else
                fitxerDesigner = cboDirectoryNet.Text & "\" & nouFitxerVB
            End If

            '! si hi ha algun fitxer que no existeix seguim amb en seguent
            If Not File.Exists(fitxerFrm) OrElse Not File.Exists(fitxerDesigner) Then Continue For

            RefrescarProces("TreeView dins AxGroupBox", fitxerFrm, numProcessat, lstItemsFoundNet.CheckedItems.Count)

            WorkAround_TreeViewDinsAxGroupBox(ObtenirControls(fitxerFrm), fitxerDesigner)
        Next

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Donat un fitxer VB retorna el corresponent fitxer frm
    ''' </summary>
    ''' <param name="fitxerVB"></param>
    ''' <returns></returns>
    Private Function CalculaFitxerFrm(ByVal fitxerVB As String) As String
        Dim splitlist() As String = {}
        Dim fitxerFrm As String = String.Empty

        If chkMigrarBackup.Checked Then
            If fitxerVB.Contains("..") Then
                fitxerFrm = cboDirectoryVB6.Text & fitxerVB.Replace(EXTENSIO_VB, EXTENSIO_FRM)
            Else
                fitxerFrm = cboDirectoryVB6.Text & fitxerVB.Replace(EXTENSIO_VB, EXTENSIO_FRM)
            End If

        Else
            '! a partir del fitxer NET obtenim el corresponent fitxer FRM
            fitxerFrm = cboDirectoryVB6.Text & fitxerVB.ToUpper.Replace(".VB", EXTENSIO_FRM).Replace(".DESIGNER", "")
        End If

        Return fitxerFrm
    End Function

    ''' <summary>
    ''' True/False per si cal tornar a Validar un fitxer
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function CalValidar(ByVal fitxer As String) As Boolean
        '! en el cas de validar la part BACKUP podem validar tantes vegades com vulguem
        If chkMigrarBackup.Checked Then Return True

        '! si ja hem redissenyat el fitxer
        If Not CalRedisseny(fitxer) Then
            '! si la data de redisseny > la data del corresponent fitxer frm

            Dim dataFitxer As Date = File.GetLastWriteTime(fitxer)
            Dim fitxerFRM As String = Replace(fitxer, CARPETA_NET, CARPETA_VB6).Replace("Designer.vb", "frm").Replace(EXTENSIO_VB, EXTENSIO_FRM)

            Dim dataActualFitxerFrm As Date = File.GetLastWriteTime(fitxerFRM)

            '? dilluns 21
            If dataActualFitxerFrm < dataFitxer Then
                '''''''Return False
            End If
        End If

        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function EsFitxerDesigner(ByVal fitxer As String) As Boolean
        Return fitxer.ToUpper.Contains(".DESIGNER")
    End Function

    ''' <summary>
    ''' True/False per saber si cal redissenyar un fitxer 
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function CalRedisseny(ByVal fitxer As String) As Boolean
        '! només hem de redissenyar els fitxers Designer.vb/resX
        If Not EsFitxerDesigner(fitxer) Then Return False

        Dim dataActualFitxerResX As Date = File.GetLastWriteTime(GetFitxerResX(fitxer))
        Dim dataActualFitxerDesigner As Date = File.GetLastWriteTime(fitxer)

        '! si hem fet una validació del fitxer Designer, la data del designer serà > la data del Resx
        If dataActualFitxerDesigner > dataActualFitxerResX Then   ' cal redissenyar
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' retorna el fitxer de redisseny donat un projecte
    ''' </summary>
    ''' <returns></returns>
    Private Function GetFitxerRedisseny(ByVal directori As String) As String
        Return directori & "\" & FITXER_REDISSENY
    End Function

    ''' <summary>
    ''' retorna fitxer Designer a partir del vb
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function GetFitxerDesignerByVB(ByVal fitxer As String) As String
        Return RegexSubstitucioCadena(fitxer, "\.vb$", EXTENSIO_DESIGNER)
    End Function

    ''' <summary>
    ''' retorna fitxer resx a partir del designer
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function GetFitxerResX(ByVal fitxer As String) As String
        Return RegexSubstitucioCadena(fitxer, REGEX_DESIGNER, REGEX_RESX_SUBSTITUCIO)
    End Function

    ''' <summary>
    ''' '? dimecres 23
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function GetProjecteFitxer(ByVal fitxer As String)
        Dim splitList() As String = {}
        Dim projecte As String = String.Empty

        '! fitxer que pertany a un altre "projecte" (COMMON, FORMCOMUNS, i fins i tot pot ser RECURSOS HUMANOS!)
        If fitxer.Contains("..") Then

            '? comprovem si es fa refencia a un fitxer de la carpeta pare
            If EsFitxerDesigner(fitxer.Split("\")(1)) Then
                '! calculem el nom del directori pare
                Dim directoriPare As String = New DirectoryInfo(cboDirectoryVB6_BACKUP.Text & "..").FullName

                '! agafem el projecte
                projecte = Split(directoriPare, "\").Last
            Else
                RegexCercaCadena(fitxer, "((\.\.\\)+[\w\s]+)", splitList)    '? el nom d'un projecte pot tenir espais en blanc !

                projecte = splitList(1)
            End If

        End If

        Return projecte
    End Function

    ''' <summary>
    ''' Calcula  i carrega en un diccionari els "fitxerDataRedisseny" per cada un els projecte del qual depèn l'app qu estem migrant
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <param name="projecte"></param>    
    ''' <returns></returns>
    Private Function CalculaFitxerDatesRedisseny(ByVal fitxer As String, ByRef projecte As String) As String
        '! assignem perque es ByRef
        projecte = GetProjecteFitxer(fitxer)

        '? carreguem la llista de fitxerDatesRedisseny
        CarregarFitxerDatesRedissenyToList(fitxer)

        Return GetFitxerRedisseny(GetDirectoriProjecteFitxer(fitxer, projecte))
    End Function

    ''' <summary>
    ''' Es possible assegurar que el fitxer Designer està TOTALMENT redissenyat
    ''' </summary>
    ''' <returns></returns>
    Private Function AssegurarDesignerRedissenyat(ByVal fitxerDesigner As String) As Boolean
        If Not File.Exists(fitxerDesigner) Then
            Debug.WriteLine(String.Format("WARNING::AssegurarDesignerRedissenyat : fitxer {0} no existeix!!!!", fitxerDesigner))
            Return False
        End If
        '? hi ha una serie de testos que ens indiquen que el fitxer ENCARA no està TOTALMENT redissenyat
        For Each linia As String In File.ReadAllLines(fitxerDesigner, encoding)
            If RegexCercaCadena(linia, REGEX_CONST_ME & "\w+\.DisplayMember = Nothing") Then
                Return False
            End If
            '? si hi ha ENCARA controls on té Height = 19 (en .NET el minim és 20 !!!)
            'x If RegexCercaCadena(linia, REGEX_CONST_ME & "\w+\." & PROPIETAT_SIZE & " = New System.Drawing.Size\(\d+, 19\)") Then
            'x Return False
            'x End If
        Next

        Return True
    End Function

    '? dimarts 14/12
    ''' <summary>    
    ''' Un formulari està totalment redissenyat que té la linia :    Me.PerformLayout()
    ''' </summary>
    ''' <param name="fitxerDesigner"></param>
    ''' <returns></returns>
    Private Function EstaTotalmentRedissenyat(ByVal fitxerDesigner As String) As Boolean

        Dim liniesFitxerDesigner As List(Of String) = LlegeixFitxer(cboDirectoryNet.Text & fitxerDesigner, encoding)

        For numlinia As Integer = liniesFitxerDesigner.Count - 1 To 0 Step -1
            '? hem trobat el PerformLayout --> TOTALMENT REDISSENYAT
            If RegexCercaCadena(liniesFitxerDesigner(numlinia), "Me\.PerformLayout\(\)") Then Return True

            '? no hem trobat el PerformLayout i trobem el ResumLayout --> no cal buscar més
            If RegexCercaCadena(liniesFitxerDesigner(numlinia), "Me\.ResumeLayout\(\w+\)") Then Return False
        Next

        Return False
    End Function

    ''' <summary>
    ''' Calcula si estan "redissenyats" o no els formularis del projecte
    ''' Ho calcula a partir de la data de modificacio del fitxer "resX" del formulari
    ''' </summary>
    Private Sub CalculaFormsRedisseny()

        '! només té sentit quan NO es Backup
        If chkMigrarBackup.Checked Then
            MessageBox.Show("Desmarca el check Backup")
            Exit Sub
        End If


        Cursor = Cursors.WaitCursor

        Dim provider As CultureInfo = CultureInfo.InvariantCulture

        lviFormsRedisseny.Clear()

        '! format listview
        FormatListViewWarnings(lviFormsRedisseny, New List(Of String)({"#", "Fitxer", "Data Foto", "Data Redisseny", "Data FRM", "Data ResX", "Data Designer"}), New List(Of Integer)({20, 450, 150, 200, 250, 300, 600}))

        For Each fitxer As ListViewItem In lstItemsFoundNet.CheckedItems
            '! hem d'excloure els fitxer que no son Designer
            If Not EsFitxerDesigner(fitxer.Text) Then Continue For

            Dim linia As String = String.Empty
            Dim projecte As String = String.Empty

            '! busquem la linia dins el fitxer "redisseny" i donat el fitxer designer
            BuscarLinia(CalculaFitxerDatesRedisseny(fitxer.Text, projecte), GetFitxerResX(fitxer.Text), linia)

            '! calculem el directori BACKUP del projecte del fitxer
            Dim directoriBackupProjecteFitxer As String = GetDirectoriProjecteFitxer(fitxer.Text, projecte).Replace(CARPETA_NET, CARPETA_VB6_BACKUP)

            If Not ExisteixFitxerMigracio(directoriBackupProjecteFitxer) Then
                MessageBox.Show("El fitxer de tracing del projecte del fitxer no existeix.Cal iniciar un procés de migració del projecte")
                Exit For
            End If

            Dim dataFitxerRedisseny As Date = DateTime.MinValue
            If Not String.IsNullOrEmpty(linia) Then
                dataFitxerRedisseny = DateTime.ParseExact(linia.Split("|")(1), FORMAT_DATAHORA_24H, provider)
            End If
            Dim dataFRM As Date = File.GetLastWriteTime(RegexSubstitucioCadena(cboDirectoryVB6.Text & fitxer.Text, REGEX_DESIGNER, REGEX_FRM_SUBSTITUCIO))
            Dim dataActualFitxerResX As Date = File.GetLastWriteTime(cboDirectoryNet.Text & GetFitxerResX(fitxer.Text))
            Dim dataActualFitxerDesigner As Date = File.GetLastWriteTime(cboDirectoryNet.Text & fitxer.Text)

            '! diferent dates
            Dim dataUltimaFoto As Date = GetDate_Ultima_by_Estat_Tracing(directoriBackupProjecteFitxer, E_EstatProcesMigracio.Executat_CopiaBackupVB6, dataActualFitxerDesigner, dataActualFitxerResX)

            '! color per defecte
            Dim color As Color = Color.LightPink
            If AplicaColorFormRedisseny(fitxer.Text, color, dataUltimaFoto, dataFRM, dataFitxerRedisseny, dataActualFitxerResX, dataActualFitxerDesigner) Then

                '! afegim lvItem
                Dim lvItem As New ListViewItem()
                With lvItem
                    .SubItems.Add(fitxer.Text)
                    .SubItems.Add(dataUltimaFoto)
                    .SubItems.Add(dataFitxerRedisseny)
                    .SubItems.Add(dataFRM)
                    .SubItems.Add(dataActualFitxerResX)
                    .SubItems.Add(dataActualFitxerDesigner)
                End With

                '! color
                lvItem.BackColor = color

                '! afegim l'item
                lviFormsRedisseny.Items.Add(lvItem)

            Else  '! cal assegurar que el fitxer està TOTALMENT rEDISSENYAT
                If AssegurarDesignerRedissenyat(cboDirectoryNet.Text & fitxer.Text) Then Continue For

                '! afegim lvItem
                Dim lvItem As New ListViewItem()
                With lvItem
                    .SubItems.Add(fitxer.Text)
                    .SubItems.Add(dataUltimaFoto)
                    .SubItems.Add(dataFitxerRedisseny)
                    .SubItems.Add(dataFRM)
                    .SubItems.Add(dataActualFitxerResX)
                    .SubItems.Add(dataActualFitxerDesigner)
                End With

                '! color
                lvItem.BackColor = Color.Yellow

                '! afegim l'item
                lviFormsRedisseny.Items.Add(lvItem)

            End If
        Next

        RefrescarProces("Total : Forms Redisseny", lviFormsRedisseny.Items.Count, "", "")

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Aplica el color al item de la listView segons les diferents dates
    ''' </summary>
    ''' <param name="dataFitxerRedisseny"></param>
    ''' <param name="dataActualFitxerResX"></param>
    ''' <param name="dataActualFitxerDesigner"></param>
    Private Function AplicaColorFormRedisseny(ByVal fitxerDesigner As String, ByRef color As Color, ByVal dataUltimaFoto As Date, ByVal dataFRM As Date, ByVal dataFitxerRedisseny As Date, ByVal dataActualFitxerResX As Date, ByVal dataActualFitxerDesigner As Date) As Boolean
        '! fitxer ja no existeix
        If chkFitxerNoExisteix.Checked AndAlso dataFRM = DateTime.MinValue Then
            color = Color.LightGray

            Return True
            '! fitxer nou
        ElseIf chkFitxerNou.Checked AndAlso dataFitxerRedisseny = DateTime.MinValue Then
            color = Color.Red

            Return True
            '! fitxer migrat
        ElseIf chkFitxerMigrat.Checked AndAlso dataFitxerRedisseny < dataUltimaFoto AndAlso dataFRM < dataUltimaFoto AndAlso dataActualFitxerDesigner.CompareTo(dataActualFitxerResX) < 1 AndAlso dataActualFitxerDesigner > dataUltimaFoto Then
            color = Color.LightGreen

            Return True

            '! fitxer modificat, cal tornar a migrar
        ElseIf chkFitxerMigrar.Checked AndAlso dataFRM > dataUltimaFoto Then
            color = Color.PaleVioletRed

            Return True

            '! fitxer cal redisssenyar
        ElseIf chkFitxerRedissenyar.Checked AndAlso dataFRM < dataUltimaFoto AndAlso dataActualFitxerDesigner.CompareTo(dataActualFitxerResX) = 1 AndAlso dataActualFitxerDesigner > dataUltimaFoto Then
            color = Color.Orange

            Return True

        ElseIf chkFitxerValidar.Checked AndAlso dataUltimaFoto > dataActualFitxerDesigner AndAlso dataFitxerRedisseny < dataFRM Then
            color = Color.LightBlue

            Return True

            ''''ElseIf Not EstaTotalmentRedissenyat(fitxerDesigner) Then    '? comprovem si està totalment redissenyat
            ''''    color = Color.Yellow

            ''''    Return True

        ElseIf chkFitxerMigrat.Checked AndAlso
            color = Color.Green Then

            Return True
        End If

        Return False
    End Function

    Private Function BuscarLinia(ByVal fitxerDatesRedisseny As String, ByVal fitxer As String, ByRef linia As String) As Integer
        '! cal buscar la linia corresponent al fitxer
        Dim nomFitxer As String = fitxer.Split("\")(fitxer.Split("\").Count - 1).Split(".")(0)

        If Not File.Exists(fitxerDatesRedisseny) Then
            MessageBox.Show("El fitxer Dates/Redisseny no existeix.Probablement cal generar el procés de migració del projecte")

            Return Nothing
        End If

        Dim index As Integer = 0

        For Each liniaFitxer As String In File.ReadAllLines(fitxerDatesRedisseny, encoding)

            Dim nomFitxerLinia As String = liniaFitxer.Split("|")(0).Split("\")(liniaFitxer.Split("\").Count - 1).Split(".")(0)

            If nomFitxerLinia.ToUpper = nomFitxer.ToUpper Then
                linia = liniaFitxer

                Return index
            End If

            '! increment index
            index = index + 1
        Next

        Return -1
    End Function

    ''' <summary>
    ''' Retorna els fitxer a processar si en funcio si estem en BACKUP o no
    ''' </summary>
    ''' <param name="fitxerVB"></param>
    ''' <returns></returns>
    Private Function CalculaFitxerProcessar(ByVal fitxerVB As String) As String
        If Not chkMigrarBackup.Checked Then Return carpetaAProcessar & fitxerVB

        Dim fitxerProcessar As String = String.Empty

        Dim rgx As New Regex("\.\.")
        Dim numNivells As Integer = rgx.Matches(fitxerVB).Count

        Dim splitList() As String = {}

        If chkMigrarBackup.Checked Then

            ' Exemple 1
            ' ..\..\Nex\Logistica\<fitxer>
            ' M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP\Nex\Logistica\NEXLogística.NET\<fitxer>

            ' Exemple 2
            ' ..\formsComuns\2007\<fitxer>
            ' M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP\Entigest\FormComuns\Entigest_FormComuns.NET\<SUBDOMINI>\<fitxer>

            '! comprovem si el fitxer "conté" (comença) per ..\
            If fitxerVB.StartsWith("..\") Then
                Dim fitxer As String = fitxerVB.Split("\").Last

                '! regex
                RegexCercaCadena(fitxerVB, "[\\\.]+\\([\w\s]+)[\\]*([\w)]*)\\(\w+\.vb)", splitList)

                If splitList.Count = 0 Then
                    Debug.WriteLine(String.Format("WARNING::CalculaFitxerProcessar"))

                    Return Nothing
                End If

                Dim subdomini As String = splitList(1)    '? Nex o FormsComuns

                '! cal diferenciar si el subdomini és FORMSCOMUNS/COMMON de la resta
                If subdomini.ToUpper = "FORMSCOMUNS" OrElse subdomini.ToUpper = "COMMON" Then

                    Dim cami As DirectoryInfo = New DirectoryInfo(cboDirectoryVB6_BACKUP.Text)

                    Dim rx As Regex = New Regex("\.\.\\")
                    numNivells = rx.Matches(fitxerVB.Trim).Count
                    For i = 0 To numNivells   '? observacio : el cami acaba en \
                        cami = Directory.GetParent(cami.FullName)  '?   <path>\Entigest
                    Next

                    Dim domini As String = cami.FullName.Split("\").Last   '? Entigest

                    '! cal diferenciar si té 1 nivell o 2
                    If numNivells = 1 Then
                        Dim carpetaNET As String = Directory.GetDirectories(CARPETA_VB6_BACKUP & domini & "\" & subdomini & "\", "*.NET")(0)

                        Return carpetaNET & "\" & splitList(2) & "\" & fitxer
                    ElseIf numNivells = 2 Then
                        Dim carpetaNET As String = Directory.GetDirectories(CARPETA_VB6_BACKUP & "\" & subdomini & "\", "*.NET")(0)

                        Return carpetaNET & "\" & splitList(2) & "\" & fitxer
                    Else
                        Dim kk = 1
                    End If

                Else '! no es COMMON ni FORMSCOMUNS

                    Dim cami As DirectoryInfo = New DirectoryInfo(cboDirectoryVB6_BACKUP.Text)

                    '! calculem el numero de nivells
                    Dim rx As Regex = New Regex("\.\.\\")
                    numNivells = rx.Matches(fitxerVB.Trim).Count
                    For i = 0 To numNivells   '? observacio : el cami acaba en \
                        cami = Directory.GetParent(cami.FullName)  '?   <path>\Entigest
                    Next

                    Dim domini As String = cami.FullName.Split("\").Last   '? Entigest

                    '! cal diferenciar si té 1 nivell o 2
                    If numNivells = 1 Then
                        Dim directoris As String() = Directory.GetDirectories(CARPETA_VB6_BACKUP & domini & "\" & subdomini & "\", "*.NET")

                        If directoris.Count = 0 Then
                            MessageBox.Show(String.Format("WARNING::projecte {0} del qual depèn no migrat", subdomini))
                        Else
                            Dim carpetaNET As String = Directory.GetDirectories(CARPETA_VB6_BACKUP & domini & "\" & subdomini & "\", "*.NET")(0)
                            Return carpetaNET & splitList(2) & "\" & fitxer
                        End If


                    ElseIf numNivells = 2 Then
                        Dim carpetaNET As String = Directory.GetDirectories(CARPETA_VB6_BACKUP & subdomini & "\" & splitList(2) & "\", "*.NET")(0)

                        Return carpetaNET & "\" & fitxer
                    Else
                        Dim kk = 1
                    End If

                End If

            Else   '! és un fitxer del propi projecte

                Dim c As DirectoryInfo = New DirectoryInfo(cboDirectoryVB6_BACKUP.Text)

                For i = 0 To numNivells - 1
                    c = New DirectoryInfo(c.FullName).Parent
                Next

                Dim f As String = fitxerVB.Replace("..\", "") '''''& "." & "vb"  ' Exemple :  Common\2007\FrmConsultaAuditoria.vb

                Dim jj As String = String.Empty
                If numNivells = 1 Then  '! Exemple : Entigest\FormComuns
                    jj = c.FullName & "\" & f
                    fitxerProcessar = jj.Replace(c.Name, c.Name & "\" & c.Name & ".NET")    '! Exemple : M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP\Common\2007\FrmConsultaAuditoria.vb

                ElseIf numNivells = 2 Then   ' Exemple : Common
                    Dim o = f.Split("\")(0)
                    jj = c.FullName & "\" & f
                    fitxerProcessar = jj.Replace("\" & o & "\", "\" & o & "\" & o & ".NET\")    '! Exemple : M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP\Common\2007\FrmConsultaAuditoria.vb

                ElseIf numNivells = 0 Then   '
                    If Directory.GetDirectories(c.FullName, "*.NET").Count = 0 Then
                        Return Nothing
                    End If

                    fitxerProcessar = Directory.GetDirectories(c.FullName, "*.NET")(0) & "\" & f
                End If

            End If

        End If

        Return fitxerProcessar
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxerVB"></param>
    ''' <returns></returns>
    Private Function CalculaNouFitxerVB(ByVal fitxerVB As String) As String
        Dim nouFitxerVB As String = fitxerVB

        If chkMigrarBackup.Checked Then
            Dim splitlist() As String = {}
            RegexCercaCadena(fitxerVB, "[\.\.\\]+(\w+)", splitlist)
            Dim carpetaNET As String = splitlist(1) & "\" & splitlist(1) & ".NET"    '! Exemple : splitlist(1) = "Common"   --> Common\Common.NET
            Dim fitxerCarpetaNET As String = Replace(fitxerVB, splitlist(1), carpetaNET)   '! Exemple : ..\..\Common\Common.NET\2007\FrmConsultaAuditoria.vb
            nouFitxerVB = "\..\" & fitxerCarpetaNET
        End If

        Return nouFitxerVB
    End Function

    ''' <summary>
    ''' WORKAROUND : excepcio : Only TrueType fonts are supported. This is not a TrueType font
    ''' Observacions :
    '''         - VB6 fa servir la font MS Sans Serif , NET fa servir la Microsoft Sans Serif (son diferents!!!!)
    '''         - Les fonts que tene tamany decimals (exemple : 8,25pt) no son TrueType
    '''         - Els fitxers de fonts estan a C:\Windows\Fonts
    ''' Solució : s'afegeix en els fitxers designers dels formularis, una linia amb la nova Font
    '''           Exemple : Me.controlNET.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    ''' </summary>
    Private Sub CalculaWorkAround_FontsTrueType()
        Cursor = Cursors.WaitCursor

        lviFontsTrueType.Clear()

        FormatListViewWarnings(lviFontsTrueType, New List(Of String)({"#", "Fitxer", "ControlAX", "ControlNET", "Solució"}), New List(Of Integer)({10, 600, 200, 200, 600}))

        Dim numProcessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numProcessat = numProcessat + 1

            Dim fitxerFrm As String = CalculaFitxerFrm(fitxerVB.Text)
            ''''Dim nouFitxerVB As String = CalculaNouFitxerVB(fitxerVB.Text)
            Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not File.Exists(fitxerProcessar) Then Continue For

            '! si estan els dos marcats hem d'evitar processar 2 vegades.El que no es designer no el tractem
            If chkMigrarDesigner.Checked AndAlso chkMigrarCodi.Checked Then
                If EsFitxerDesigner(fitxerProcessar) Then Continue For
            End If


            '! nomes tractem els fitxers designer
            If EsFitxerDesigner(fitxerProcessar) Then Continue For

            Dim fitxerDESIGNER As String = String.Empty

            If chkMigrarBackup.Checked = True Then
                fitxerDESIGNER = Replace(fitxerProcessar, EXTENSIO_VB, EXTENSIO_DESIGNER)
            Else
                fitxerDESIGNER = Replace(fitxerProcessar, EXTENSIO_VB, EXTENSIO_DESIGNER)
            End If

            '! si hi ha algun fitxer que no existeix seguim amb en seguent
            If Not File.Exists(fitxerFrm) OrElse Not File.Exists(fitxerDESIGNER) Then Continue For

            RefrescarProces("Fonts TrueType", fitxerDESIGNER, numProcessat, lstItemsFoundNet.CheckedItems.Count)

            Dim liniesFitxerFRM As List(Of String) = LlegeixFitxer(fitxerFrm, encoding)

            '! opcio 1
            WorkAround_FontsTrueType_v2(ObtenirControls(fitxerFrm), fitxerDESIGNER)

            '! opcio 2
            'WorkAround_FontsTrueType(liniesFitxerFRM, fitxerDESIGNER)
        Next

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' <para>Hem detectat que el Wizard 2008, quan fa la migració, si troba una linia on es declara varies variables d'un tipus, llavors
    ''' crea 2 linies de declaracio.La primera amb la declaració de totes les variables menys la ultima però declarada com a As Object
    ''' i la segona linia amb la declaracio de la última variable declarada amb el tipus correcte</para>
    ''' Exemple :
    ''' VB6 : Dim A, B, C, D As Integer   ---> Wizard2008  ----> NET : Dim A, B, C As Object   i Dim D As Integer
    ''' </summary>
    Private Sub CalculaWorkAround_AsObject()
        Cursor = Cursors.WaitCursor

        lviAsObject.Clear()

        FormatListViewWarnings(lviAsObject, New List(Of String)({"#", "Linia", "Linia_subtituida", "Fitxer", "Num"}), New List(Of Integer)({10, 500, 700, 300, 100}))

        Dim numprocessat As Integer = 0

        CarregarSubstitucionsToList("substitucions_AsObject.txt")

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numprocessat = numprocessat + 1

            Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not File.Exists(fitxerProcessar) Then
                Continue For
            End If

            '! només migrem els checked
            If Not fitxerVB.Checked Then Continue For

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)

            Dim numlinia As Integer = 0
            For Each linia As String In liniesFitxerVBNet
                WorkAround_LiniesAmbAsObject(linia, fitxerVB.Text, numlinia, liniesFitxerVBNet)
                numlinia = numlinia + 1
            Next

            RefrescarProces("AsObject", fitxerProcessar, numprocessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)
        Next

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Calcula possibles substitucions de linies on cal convertir de Twips a Pixels
    ''' </summary>
    Private Sub CalculaWorkAround_LiniesTwipsPixels()
        If Not Me.IsHandleCreated Then Exit Sub

        Cursor = Cursors.WaitCursor

        lvTwipsPixels.Clear()

        FormatListViewWarnings(lvTwipsPixels, New List(Of String)({"#", "Linia", "Linia_subtituida", "NumRegla", "Fitxer", "Num"}), New List(Of Integer)({10, 500, 300, 60, 600, 100}))

        Dim numProcessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numProcessat = numProcessat + 1

            Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not File.Exists(fitxerProcessar) Then Continue For

            '! només migrem els checked
            If Not fitxerVB.Checked Then Continue For

            RefrescarProces("Twips-Pixels", fitxerProcessar, numProcessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)

            For Each linia As String In liniesFitxerVBNet
                WorkAround_LiniesAmbTwipsPixels(linia, fitxerVB.Text, liniesFitxerVBNet.IndexOf(linia))
            Next
        Next

        For Each fitxerVB As ListViewItem In lstItemsFoundNet_2.CheckedItems
            numProcessat = numProcessat + 1

            Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not File.Exists(fitxerProcessar) Then Continue For

            '! només migrem els checked
            If Not fitxerVB.Checked Then Continue For

            RefrescarProces("Twips-Pixels", fitxerProcessar, numProcessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)

            For Each linia As String In liniesFitxerVBNet
                WorkAround_LiniesAmbTwipsPixels(linia, fitxerVB.Text, liniesFitxerVBNet.IndexOf(linia))
            Next
        Next

        Cursor = Cursors.Default
    End Sub

    '''' <summary>
    '''' WORKAROUND : en runtime es provoca una excepcio del tipus ........
    '''' </summary>
    'Private Sub CalculaWorkAround_Property_can_only_be_set_to_Nothing()
    '    Cursor = Cursors.WaitCursor

    '    lvWorkAroundPropertyCanOnly.Clear()

    '    FormatListViewWarnings(lvWorkAroundPropertyCanOnly, New List(Of String)({"#", "Linia", "Fitxer", "Num"}), New List(Of Integer)({10, 1200, 200, 300}))

    '    For Each fitxerVB As ListViewItem In lstItemsFoundNet.checkedItems
    '        If Not File.Exists(fitxerProcessar) Then Continue For

    '        RefrescarProces("Property can only set to Nothing", fitxerProcessar)

    '        Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)

    '        For Each linia As String In liniesFitxerVBNet
    '            WorkAround__Property_can_only_be_set_to_Nothing(linia, fitxerVB.Text, liniesFitxerVBNet.IndexOf(linia))
    '        Next
    '    Next

    '    Cursor = Cursors.Default
    'End Sub

    Private Sub MostrarSubitemsVisible()
        For Each it As TreeListViewItem In treeListViewUpgrades.CheckedItems

            If (it.BackColor = Color.Orange AndAlso chkWarning.Checked) OrElse
                   (it.BackColor = Color.OrangeRed AndAlso chkCritic.Checked) OrElse
                   (it.BackColor = Color.PaleVioletRed AndAlso chkAltres.Checked) OrElse
                   (it.BackColor = Color.LightGreen AndAlso chkResolt.Checked) Then
            Else
                it.Remove()
                'sit.Visible = False
            End If

        Next

    End Sub

    ''' <summary>
    ''' Retorna el color en funcio de la categoria de Warning arquitectura (segons https://www.vbmigration.com/knowledgebase.aspx)
    ''' </summary>
    ''' <param name="CriticitatMigracioError"></param>
    ''' <returns></returns>
    Private Function GetColorWarningArquitectura(ByVal CriticitatMigracioError As CriticitatMigracioError) As Color
        If CriticitatMigracioError = CriticitatMigracioError.INFO Then Return Color.LightGreen
        If CriticitatMigracioError = CriticitatMigracioError.PRB Then Return Color.OrangeRed
        If CriticitatMigracioError = CriticitatMigracioError.HOWTO Then Return Color.Orange
        If CriticitatMigracioError = CriticitatMigracioError.BUG Then Return Color.PaleVioletRed
    End Function

    Private Function GetCriticitatWarningArquitectura(ByVal criticitat As String) As CriticitatMigracioError
        If criticitat = "[HOWTO]" Then Return CriticitatMigracioError.HOWTO
        If criticitat = "[BUG]" Then Return CriticitatMigracioError.BUG
        If criticitat = "[INFO]" Then Return CriticitatMigracioError.INFO
        If criticitat = "[PRB]" Then Return CriticitatMigracioError.PRB
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="colorUpgrade"></param>
    ''' <returns></returns>
    Private Function GetColorUpgrade(ByVal colorUpgrade As ColorUpgrade) As Color
        If colorUpgrade = ColorUpgrade.Resolt Then Return Color.LightGreen
        If colorUpgrade = ColorUpgrade.Warning Then Return Color.Orange
        If colorUpgrade = ColorUpgrade.Critic Then Return Color.OrangeRed
        If colorUpgrade = ColorUpgrade.Altres Then Return Color.PaleVioletRed
        If colorUpgrade = ColorUpgrade.GestioEvents Then Return Color.Yellow
    End Function

    ''' <summary>
    ''' Calcula els UPGRADE que hi ha al projecte seleccionat
    ''' </summary>
    Private Sub CalculaUpgrades()
        Cursor = Cursors.WaitCursor

        numTotalUpgrades = 0
        numTotalUpgradesSeleccionats = 0
        numTotal_Upgrade_Issue = 0
        numTotal_Upgrade_Note = 0
        numTotal_Upgrade_Todo = 0
        numTotal_Upgrade_Warning = 0

        upgrades.Clear()

        tipusControl_Propietat.Clear()

        '
        FormatListViewWarnings(lvWarningsWorkAround, New List(Of String)({"#", "Linia", "Fitxer", "Num"}), New List(Of Integer)({10, 150, 200, 300}))

        If lstItemsFoundNet.Items.Count = 0 Then
            MessageBox.Show("Has de recuperar els fitxers del projecte ")

            Exit Sub
        End If

        Dim numProcessat As Integer = 0

        For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
            numProcessat = numProcessat + 1

            Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxerVB.Text)

            If Not File.Exists(fitxerProcessar) Then Continue For

            RefrescarProces("Upgrades", fitxerProcessar, numProcessat, lstItemsFoundNet.CheckedItems.Count)

            Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)

            For Each linia As String In liniesFitxerVBNet
                OmpleUpgrades_TreeListView(linia)
            Next
        Next

        '! Aquest es el codi utilitzant el control TreeListView
        treeListViewUpgrades.Items.Clear()

        For Each pair As KeyValuePair(Of String, List(Of Upgrade)) In upgrades
            '! comprovem segons la criticitat si ho hem de visualitzar
            If colorUpgradeRoot(pair.Key) = ColorUpgrade.Critic AndAlso Not chkCritic.Checked Then Exit Sub
            If colorUpgradeRoot(pair.Key) = ColorUpgrade.Resolt AndAlso Not chkResolt.Checked Then Exit Sub
            If colorUpgradeRoot(pair.Key) = ColorUpgrade.Altres AndAlso Not chkAltres.Checked Then Exit Sub
            If colorUpgradeRoot(pair.Key) = ColorUpgrade.Warning AndAlso Not chkWarning.Checked Then Exit Sub


            numTotalUpgradesSeleccionats = numTotalUpgradesSeleccionats + 1

            Dim itemA As TreeListViewItem = New TreeListViewItem(pair.Key, 0)
            treeListViewUpgrades.Items.Add(itemA)

            itemA.BackColor = GetColorUpgrade(colorUpgradeRoot(pair.Key))
            itemA.SubItems.Add("--------------------------------------------------------------------------")

            For Each u As Upgrade In upgrades.Item(pair.Key).ToList
                Dim item As TreeListViewItem = New TreeListViewItem("", 1)

                item.BackColor = GetColorUpgrade(u.color)

                item.SubItems.Add(u.descripcio)
                item.SubItems.Add(u.solucio)
                item.SubItems.Add(u.observacio)
                item.SubItems.Add(u.tipus)
                item.SubItems.Add(GetDescripcioUpgrade(u.tipus))

                itemA.Items.Add(item)
            Next
        Next

        RefrescarProces("Upgrades", String.Format("UPGRADES calculats seleccionats : {0} , Totals : {1}", numTotalUpgradesSeleccionats, numTotalUpgrades), "", "")

        'MostrarSubitemsVisible()

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_CommondDialog(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "CANCELERROR" Then
            observacio = ""
            color = ColorUpgrade.Critic
            tipusObjecte = "9970"
            solucio = "Cal migrar"
        ElseIf propietat.ToUpper = "PRINTERDEFAULT" Then
            observacio = "Substituir el tipus per PrintDialog"
            color = ColorUpgrade.Critic
            tipusObjecte = "9974"
            solucio = "Cal migrar"
        ElseIf propietat.ToUpper = "SHOWPRINTER" Then
            observacio = "Substituir el tipus per PrintDialog"
            color = ColorUpgrade.Critic
            tipusObjecte = "9975"
            solucio = "Cal migrar"
        ElseIf propietat.ToUpper = "SHOWSAVE" Then
            observacio = "Requereix alguna modificació manual"
            color = ColorUpgrade.Critic
            tipusObjecte = "9976"
            solucio = REGEX_CADENA_SUBSTITUCIO & "ShowDialog" & "i substituir el tipus per SaveFileDialog"
        ElseIf propietat.ToUpper = "FILTER" Then
            observacio = "Substituir el tipus per SaveFileDialog o OpenFileDialog"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9971"
            solucio = "Mantenir la propietat i substituir el tipus per SaveFileDialog o OpenFileDialogt"
        ElseIf propietat.ToUpper = "SHOWOPEN" Then
            observacio = ""
            color = ColorUpgrade.Warning
            tipusObjecte = "9972"
            solucio = REGEX_CADENA_SUBSTITUCIO & "ShowDialog i substituir el tipus per OpenFileDialog"
        ElseIf propietat.ToUpper = "DIALOGTITLE" Then
            observacio = "Substituir el tipus per SaveFileDialog o OpenFileDialog"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9973"
            solucio = REGEX_CADENA_SUBSTITUCIO & "Title"
        ElseIf propietat.ToUpper = "FILENAME" Then
            observacio = "Substituir el tipus per SaveFileDialog o OpenFileDialog"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9977"
            solucio = "Mantenir la propietat i substituir el tipus per SaveFileDialog o OpenFileDialog"
        ElseIf propietat.ToUpper = "INITDIR" Then
            observacio = "Substituir el tipus per SaveFileDialog o OpenFileDialog"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9978"
            solucio = REGEX_CADENA_SUBSTITUCIO & "InitialDirectory"
        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            'color = ColorUpgrade.Resolt
            'tipusObjecte = "9970"
            propietat = "MSComDlg.CommonDialog"
        End If

        If esDuplicatTipusControlPropietat("MSComDlg.CommonDialog", propietat) Then Return False

        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_CalendarControl(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "ACTIVEVIEW" Then
            observacio = "Control CalendarControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "Control CalendarControl"
        ElseIf propietat.ToUpper = "PRINTPREVIEWOPTIONS" Then
            observacio = "Control CalendarControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "Control CalendarControl"
        ElseIf propietat.ToUpper = "PRINTPREVIEW" Then
            observacio = "Control CalendarControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "Control CalendarControl"
        ElseIf propietat.ToUpper = "PRINTOPTIONS" Then
            observacio = "Control CalendarControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "Control CalendarControl"
        ElseIf propietat.ToUpper = "DAYVIEW" Then
            observacio = "Control CalendarControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "Control CalendarControl"
        ElseIf propietat.ToUpper = "DATAPROVIDER" Then
            observacio = "Control CalendarControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "Control CalendarControl"
        ElseIf propietat.ToUpper = "OPTIONS" Then
            observacio = "Control CalendarControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "Control CalendarControl"
        ElseIf propietat.ToUpper = "VIEWTYPE" Then
            observacio = "Control CalendarControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "Control CalendarControl"
        ElseIf propietat.ToUpper = "POPULATE" Then
            observacio = "Control CalendarControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "Control CalendarControl"
        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "CalendarControl"
        End If

        If esDuplicatTipusControlPropietat("CalendarControl", propietat) Then Return False

        Return True
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_CalendarEvent(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "STARTTIME" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "ENDTIME" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "BUSYSTATUS" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "CUSTOMPROPERTIES" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "ALLDAYEVENT" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "MEETINGFLAG" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "PRIVATEFLAG" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "REMINDER" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "SUBJECT" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "BODY" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        ElseIf propietat.ToUpper = "LABEL" Then
            observacio = "Control CalendarEvent"
            color = ColorUpgrade.Warning
            tipusObjecte = "9320"
            propietat = "Control CalendarEvent"
        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9320"
            propietat = "CalendarEvent"
        End If

        If esDuplicatTipusControlPropietat("CalendarEvent", propietat) Then Return False

        Return True
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_ALLPDF(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "ALLPDF" Then
            observacio = "AllPdf"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9986"
            propietat = "AllPdf"
        ElseIf propietat.ToUpper = "PENWIDTH" Then
            observacio = "AllPdf"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9986"
            propietat = "AllPdf"
        ElseIf propietat.ToUpper = "CHANNEL" Then
            observacio = "AllPdf"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9986"
            propietat = "AllPdf"
        ElseIf propietat.ToUpper = "CREATESIGNATURE" Then
            observacio = "AllPdf"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9986"
            propietat = "AllPdf"
        ElseIf propietat.ToUpper = "ENDDOC" Then
            observacio = "AllPdf"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9986"
            propietat = "AllPdf"
        ElseIf propietat.ToUpper = "BRUSHCOLOR" Then
            observacio = "AllPdf"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9986"
            propietat = "AllPdf"
        ElseIf propietat.ToUpper = "PAGEWIDTH" Then
        ElseIf propietat.ToUpper = "RIGHTMARGIN" Then
        ElseIf propietat.ToUpper = "LEFTMARGIN" Then
        ElseIf propietat.ToUpper = "TOPMARGIN" Then
        ElseIf propietat.ToUpper = "BOTTOMMARGIN" Then
        ElseIf propietat.ToUpper = "FONTBOLD" Then
        ElseIf propietat.ToUpper = "FONTSIZE" Then
        ElseIf propietat.ToUpper = "FONTNAME" Then
        ElseIf propietat.ToUpper = "BEGINDOC" Then
        ElseIf propietat.ToUpper = "MARGINS" Then
        ElseIf propietat.ToUpper = "ORIENTATION" Then
        ElseIf propietat.ToUpper = "TEXTY" Then
        ElseIf propietat.ToUpper = "PAGEHEIGHT" Then
        ElseIf propietat.ToUpper = "TEXTOUT" Then
        ElseIf propietat.ToUpper = "PUT" Then
        ElseIf propietat.ToUpper = "NEWPAGE" Then
        ElseIf propietat.ToUpper = "FONTCOLOR" Then
        ElseIf propietat.ToUpper = "PENSTYLE" Then
        ElseIf propietat.ToUpper = "PENWIDTH" Then
        ElseIf propietat.ToUpper = "PENCOLOR" Then
        ElseIf propietat.ToUpper = "PENX" Then
        ElseIf propietat.ToUpper = "PENY" Then
        ElseIf propietat.ToUpper = "LINETO" Then
        ElseIf propietat.ToUpper = "BRUSHSTYLE" Then
        ElseIf propietat.ToUpper = "RECTANGLE" Then
        ElseIf propietat.ToUpper = "PAPERSIZENAME" Then
        ElseIf propietat.ToUpper = "TEXTWIDTH" Then
        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9986"
            propietat = "ALLPDF"
        End If

        If esDuplicatTipusControlPropietat("ALLPDF", propietat) Then Return False

        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_CIAXPStyle(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "BUTTONSTYLE" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9800"
            propietat = "ciaXPListView30.XPButton30"
        ElseIf propietat.ToUpper = "PICTUREDISABLED" Then
            observacio = "http://www.vbmigration.com/detknowledgebase.aspx?Id=670"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9800"
            propietat = "ciaXPListView30.XPButton30"
            solucio = "OBSOLET"
        ElseIf propietat.ToUpper = "LISTITEMS" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9800"
            propietat = "ciaXPListView30.XPListView30"
        ElseIf propietat.ToUpper = REGEX_SUBITEMS.ToUpper Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9800"
            propietat = "ciaXPListView30.XPListView30"
        ElseIf propietat.ToUpper = "ICONINDEX" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9800"
            propietat = "ciaXPListView30.XPListView30"
        ElseIf propietat.ToUpper = "CLEARITEMS" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9801"
            propietat = "XPListView30"
        ElseIf propietat.ToUpper = "CHECKBOXES" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9802"
            propietat = "XPListView30"
        ElseIf propietat.ToUpper = "ITEMDATA" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9802"
            propietat = "XPListView30"
        ElseIf propietat.ToUpper = "CHECKED" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9803"
            propietat = "XPListView30"
        ElseIf propietat.ToUpper = "SORTORDER" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9804"
            propietat = "XPListView30"
        ElseIf propietat.ToUpper = "SORTEDCOLUMN" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9805"
            propietat = "XPListView30"
        ElseIf propietat.ToUpper = "SORT" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9806"
            propietat = "XPListView30"
        ElseIf propietat.ToUpper = "BEGINUPDATE" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9807"
            propietat = "XPListView30"
        ElseIf propietat.ToUpper = "ENDUPDATE" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9807"
            propietat = "XPListView30"
        ElseIf propietat.ToUpper = "XPMENUBAR" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9807"
            propietat = "XPMenuBar30"
        ElseIf propietat.ToUpper = "IMLMENUHEADER" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9807"
            propietat = "XPMenuBar30"
        ElseIf propietat.ToUpper = "IMLMENUITEMS" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9807"
            propietat = "XPMenuBar30"
        ElseIf propietat.ToUpper = "IMLTOOLITEMS" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9807"
            propietat = "XPMenuBar30"
        ElseIf propietat.ToUpper = "CURRENTTAB" Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9330"
            propietat = "CiaXpTab"
        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9800"
            propietat = "CIAXPStyle"
        End If

        If esDuplicatTipusControlPropietat("CIAXPStyle", propietat) Then Return False

        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_Textbox(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "TOP" Then
            observacio = "TextBox"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9630"
            propietat = "TextBox"
        ElseIf propietat.ToUpper = PROPIETAT_WIDTH.ToUpper Then
            observacio = "TextBox"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9631"
            propietat = "TextBox"
        ElseIf propietat.ToUpper = PROPIETAT_HEIGHT.ToUpper Then
            observacio = "TextBox"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9632"
            propietat = "TextBox"
        ElseIf propietat.ToUpper = "LEFT" Then
            observacio = "TextBox"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9633"
            propietat = "TextBox"
        ElseIf propietat.ToUpper = "VISIBLE" Then
            observacio = "TextBox"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9634"
            propietat = "TextBox"
        ElseIf propietat.ToUpper = "TEXT" Then
            observacio = "TextBox"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9635"
            propietat = "TextBox"
        ElseIf propietat.ToUpper = "ENABLED" Then
            observacio = "TextBox"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9636"
            propietat = "TextBox"
        ElseIf propietat.ToUpper = "SETFOCUS" Then
            observacio = "TextBox"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9637"
            propietat = "TextBox"
        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Resolt
            ' tipusObjecte = "9630"
            propietat = "Textbox"
        End If

        If esDuplicatTipusControlPropietat("Textbox", propietat) Then Return False

        Return True
    End Function



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_OREF(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "ID" Then
        ElseIf propietat.ToUpper = "GENERANOUID" Then
        ElseIf propietat.ToUpper = "DOCUMENT" Then
        ElseIf propietat.ToUpper = "MINIIMG" Then
        ElseIf propietat.ToUpper = "NOMFITXER" Then
        ElseIf propietat.ToUpper = "EXTENSIOFITXER" Then
        ElseIf propietat.ToUpper = "DESCRIPCIO" Then
        ElseIf propietat.ToUpper = "DELETEDOC" Then

        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            observacio = "OREF"
            color = ColorUpgrade.Warning
            tipusObjecte = "9550"
            propietat = "OREF"
        End If

        If esDuplicatTipusControlPropietat("OREF", propietat) Then Return False

        Return True

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_CacheActivex(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "OPENID" Then
            observacio = "CacheActivex.Factory"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9300"
            propietat = "CacheActivex.Factory"
        ElseIf propietat.ToUpper = "NEW" Then
            observacio = "CacheActivex.Factory"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9301"
            propietat = "CacheActivex.Factory"
        ElseIf propietat.ToUpper = "ISPICTURE" Then
            observacio = "CacheActiveX.Factory"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9302"
            propietat = "CacheActiveX.Factory"
        ElseIf propietat.ToUpper = "SYS_SAVE" Then
            observacio = "CacheActiveX.Factory"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9303"
            propietat = "CacheActiveX.Factory"
        ElseIf propietat.ToUpper = "SYS_CLOSE" Then
            observacio = "CacheActiveX.Factory"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9304"
            propietat = "CacheActiveX.Factory"
        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Resolt
            ' tipusObjecte = "9300"
            propietat = "CacheActiveX"
        End If

        If esDuplicatTipusControlPropietat("CacheActiveX", propietat) Then Return False

        Return True

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_Excel(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "WORKBOOKS" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "SHEETS" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "ACTIVESHEET" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "APPLICATION" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "QUIT" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "SAVEAS" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "SAVED" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "CLOSE" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "USEDRANGE" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "CELLS" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"
        ElseIf propietat.ToUpper = "SAVE" Then
            observacio = "Excel"
            color = ColorUpgrade.Warning
            tipusObjecte = "9995"
            propietat = "Excel"

        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9995"
            propietat = "Excel"
        End If

        If esDuplicatTipusControlPropietat("Excel", propietat) Then Return False

        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_WMI(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "SERVICENAME" Then
        ElseIf propietat.ToUpper = "TERMINATEWINPROCESS" Then
        ElseIf propietat.ToUpper = "TERMINATE" Then
        ElseIf propietat.ToUpper = "INSTANCESOF" Then
        ElseIf propietat.ToUpper = PROPIETAT_NAME.ToUpper Then

        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Critic
            tipusObjecte = "9160"
            propietat = "WMI"
        End If

        If esDuplicatTipusControlPropietat("WMI", propietat) Then Return False

        Return True
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_CharControl(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "TITLES" Then
        ElseIf propietat.ToUpper = "LEGEND" Then
        ElseIf propietat.ToUpper = "PANELDIRECTION" Then
        ElseIf propietat.ToUpper = "DIAGRAMS" Then
        ElseIf propietat.ToUpper = "SERIES" Then
        ElseIf propietat.ToUpper = "ROTATED" Then
        ElseIf propietat.ToUpper = "CONTENT" Then
        ElseIf propietat.ToUpper = PROPIETAT_STYLE.ToUpper Then
        ElseIf propietat.ToUpper = "HOLEPERCENT" Then
        ElseIf propietat.ToUpper = "POINTS" Then
        ElseIf propietat.ToUpper = "AXISY" Then
        ElseIf propietat.ToUpper = "AXISX" Then
        ElseIf propietat.ToUpper = "DIAGRAM" Then
        ElseIf propietat.ToUpper = "SAVEASIMAGE" Then
        ElseIf propietat.ToUpper = "MOVE" Then
        ElseIf propietat.ToUpper = "ADD" Then

        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9110"
            propietat = "XtremeChartControl"
        End If

        If esDuplicatTipusControlPropietat("XtremeChartControl", propietat) Then Return False

        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_Sheridan(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "BEVELTYPE" Then
            observacio = "SSDBCombo/GmsCombo"
            color = ColorUpgrade.Warning
            tipusObjecte = "9911"
        ElseIf propietat.ToUpper = "BEVELCOLORFACE" Then
            observacio = "SSDBCombo/GmsCombo"
            color = ColorUpgrade.Warning
            tipusObjecte = "9912"
        ElseIf propietat.ToUpper = "BEVELCOLORSCHEME" Then
            observacio = "SSDBCombo/GmsCombo"
            color = ColorUpgrade.Warning
            tipusObjecte = "9912"
        ElseIf propietat.ToUpper = "DIVIDERSTYLE" Then
            observacio = "SSDBCombo/GmsCombo"
            color = ColorUpgrade.Warning
            tipusObjecte = "9912"
        ElseIf propietat.ToUpper = "BEVELCOLORHIGHLIGHT" Then
            observacio = "SSGrid"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9911"
        ElseIf propietat.ToUpper = "BEVELCOLORSHADOW" Then
            observacio = "SSGrid"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9911"
        ElseIf propietat.ToUpper = "BACKCOLOREVEN" Then
            observacio = "AxSSDBGrid"
            color = ColorUpgrade.Warning
            tipusObjecte = "9993"
        ElseIf propietat.ToUpper = "BACKCOLORODD" Then
            observacio = "AxSSDBGrid"
            color = ColorUpgrade.Warning
            tipusObjecte = "9993"
        ElseIf propietat.ToUpper = "VALUE()" Then
            observacio = "AxSSDBGrid"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9993"
            propietat = "AxSSDBGrid"
            solucio = ""
        ElseIf propietat.ToUpper = "BOOKMARK()" Then
            observacio = "AxSSDBGrid"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9993"
            propietat = "AxSSDBGrid"
            solucio = ""
        ElseIf propietat.ToUpper = "PICTUREDISABLED" Then
            observacio = "http://www.vbmigration.com/detknowledgebase.aspx?Id=670"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9800"
            propietat = ""
            solucio = "OBSOLET"
        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            color = ColorUpgrade.Resolt
            ' tipusObjecte = "9912"
            propietat = "Sheridan"
        End If

        If esDuplicatTipusControlPropietat("Sheridan", propietat) Then Return False

        Return True
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    ''' <param name="esDelTiPus"></param>
    ''' <param name="solucio"></param>
    ''' <returns></returns>
    Private Function Propietats_FCGrid(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean, ByRef solucio As String) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "ACTIVECELL" Then
            observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9400"
            propietat = TIPUSCONTROL_FCGRID
        ElseIf propietat.ToUpper = "COLUMN" Then
            observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9401"
            propietat = TIPUSCONTROL_FCGRID
        ElseIf propietat.ToUpper = "CELL" Then
            observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9402"
            propietat = TIPUSCONTROL_FCGRID
        ElseIf propietat.ToUpper = "ADDITEM" Then
            observacio = "FlexCell.Grid o SSDBCombo/GmsCombo"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9403"
            propietat = "FlexCell.Grid o SSDBCombo/GmsCombo"
        ElseIf propietat.ToUpper = "ROWS" Then
            observacio = "FlexCell.Grid o SSDBCombo/GmsCombo"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9404"
            propietat = "FlexCell.Grid o SSDBCombo/GmsCombo"
        ElseIf propietat.ToUpper = "AUTOFITROWHEIGHT" Then
            observacio = TIPUSCONTROL_FCGRID
            color = ColorUpgrade.Resolt
            tipusObjecte = "9404"
            propietat = TIPUSCONTROL_FCGRID
            solucio = REGEX_CADENA_SUBSTITUCIO & "AutoFit"
        ElseIf propietat.ToUpper = "COLS" Then
            observacio = TIPUSCONTROL_FCGRID
            color = ColorUpgrade.Resolt
            tipusObjecte = "9405"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""
        ElseIf propietat.ToUpper = "EXTENDLASTCOL" Then
            observacio = TIPUSCONTROL_FCGRID
            color = ColorUpgrade.Resolt
            tipusObjecte = "9406"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""
        ElseIf propietat.ToUpper = "AUTOREDRAW" Then
            observacio = "AutoRedraw dels Forms si que tenen WorkAround : https://www.c-sharpcorner.com/article/auto-redraw-in-VB-Net/"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9407"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""

        ElseIf propietat.ToUpper = "VSCROLLBARVISIBLE" Then
            observacio = TIPUSCONTROL_FCGRID
            color = ColorUpgrade.Resolt
            tipusObjecte = "9408"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""

        ElseIf propietat.ToUpper = "REFRESH" Then
            observacio = TIPUSCONTROL_FCGRID
            color = ColorUpgrade.Resolt
            tipusObjecte = "9409"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""
        ElseIf propietat.ToUpper = "DISPLAYFOCUSRECT" Then
            observacio = TIPUSCONTROL_FCGRID
            color = ColorUpgrade.Resolt
            tipusObjecte = "9410"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""
        ElseIf propietat.ToUpper = "ALLOWUSERRESIZING" Then
            observacio = TIPUSCONTROL_FCGRID

            color = ColorUpgrade.Resolt
            tipusObjecte = "9410"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""
        ElseIf propietat.ToUpper = "MOUSEROW" Then
            observacio = TIPUSCONTROL_FCGRID
            color = ColorUpgrade.Resolt
            tipusObjecte = "9411"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""
        ElseIf propietat.ToUpper = "CLEAR" Then
            observacio = ""
            color = ColorUpgrade.Critic
            tipusObjecte = "9412"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""
        ElseIf propietat.ToUpper = "DEFAULTROWHEIGHT" Then
            observacio = ""
            color = ColorUpgrade.Resolt
            tipusObjecte = "9413"
            propietat = TIPUSCONTROL_FCGRID
            solucio = ""

        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            'observacio = ""
            'color = ColorUpgrade.Resolt
            ' tipusObjecte = "9991"
            propietat = TIPUSCONTROL_FCGRID
        End If

        If esDuplicatTipusControlPropietat(TIPUSCONTROL_FCGRID, propietat) Then Return False

        Return True
    End Function
    ''' <summary>
    ''' Propietats de l'objecte GmsXpTab
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    ''' <param name="color"></param>
    Private Function Propietats_GmsXpTab(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTiPus As Boolean) As Boolean
        esDelTiPus = True

        If propietat.ToUpper = "ACTIVETAB" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        ElseIf propietat.ToUpper = "TABCOUNT" Then
            observacio = "GmsXpTab/SSIndexTab/GmsTabControl"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab/SSIndexTab/GmsTabControl"
        ElseIf propietat.ToUpper = "TABENABLED" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        ElseIf propietat.ToUpper = "IGNOREREDRAW" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        ElseIf propietat.ToUpper = "TABSTRIPBACKCOLOR" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        ElseIf propietat.ToUpper = "ACTIVETABBACKENDCOLOR" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        ElseIf propietat.ToUpper = "ACTIVETABBACKSTARTCOLOR" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        ElseIf propietat.ToUpper = "HOVERCOLORINVERTED" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        ElseIf propietat.ToUpper = "INACTIVETABBACKSTARTCOLOR" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        ElseIf propietat.ToUpper = "INACTIVETABBACKENDCOLOR" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        ElseIf propietat.ToUpper = "OUTERBORDERCOLOR" Then
            observacio = "GmsXpTab"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        Else
            esDelTiPus = False
        End If

        If esDelTiPus Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9991"
            propietat = "GmsXpTab"
        End If

        If esDuplicatTipusControlPropietat("GmsXpTab", "") Then Return False

        Return True
    End Function

    ''' <summary>
    ''' Propietats de l'objecte GdPicture
    ''' </summary>
    ''' <param name="propietat"></param>
    ''' <param name="tipusObjecte"></param>
    ''' <param name="observacio"></param>
    Private Function Propietats_GdPicture(ByVal propietat As String, ByRef tipusObjecte As String, ByRef observacio As String, ByRef color As ColorUpgrade, ByRef esDelTipus As Boolean) As Boolean
        esDelTipus = True

        If propietat.ToUpper = "PDFADDIMAGEFROMIMAGEID" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "OGDPICTURE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "CREATEIMAGEFROMFILE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PDFSETPAGEDIMENSIONS" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PDFNEWPAGE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PDFDRAWIMAGE" Then

            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PDFENDPAGE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PDFSAVEPDF" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TIFFSAVEASNATIVEMULTIPAGE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TIFFADDTONATIVEMULTIPAGE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TIFFCLOSENATIVEMULTIPAGE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINOPENDEFAULTSOURCE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETHIDEUI" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETCURRENTRESOLUTION" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETCURRENTPIXELTYPE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETCURRENTBITDEPTH" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETAUTOBRIGHTNESS" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETCURRENTBRIGHTNESS" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINENABLEDUPLEX" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETCURRENTCONTRAST" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETINDICATORS" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "CREATEIMAGEFROMTWAIN" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "RESIZE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "ROTATE90" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PICTURECLOSED" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "SAVEASPDF" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "CLOSEIMAGE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINCLOSESOURCEMANAGER" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINGETSTATE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETAUTOFEED" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "TWAINSETAUTOSCAN" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PDFNEWPDF" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PDFSETMEASUREMENTUNITS" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "ZOOMMODE" Then

            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "DISPLAYFROMSTDPICTURE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "SETZOOMFIT" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "LOADFROMPICTURE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "SAVEASJPEG" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "SETLICENCENUMBER" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "CREATEIMAGEFROMCLIPBOARD" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "INTERPOLATIONMODEHIGHQUALITY" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "GETNATIVEIMAGE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "DISPLAYFROMIMAGEREF" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "ROTATE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "ZOOMOUT" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "ZOOMIN" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "GETPICTURE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "DISPLAYFROMSTDPICTURE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "CREATEIMAGEFROMHWND" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PRINTSETACTIVEPRINTER" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PRINTSETCOPIES" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PRINTSETDUPLEXMODE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PRINTSETORIENTATION" Then

            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "PRINTIMAGE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "CREATEIMAGEFROMPICTURE" Then
            color = ColorUpgrade.Warning
        ElseIf propietat.ToUpper = "SAVEASGIF" Then
            color = ColorUpgrade.Warning
        ElseIf propietat.ToUpper = "GETSTAT" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "GETHEIGHT" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = PROPIETAT_WIDTH.ToUpper Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        ElseIf propietat.ToUpper = "GETWIDTH" Then
        ElseIf propietat.ToUpper = "CLOSEPICTURE" Then
            color = ColorUpgrade.Warning
            tipusObjecte = "9994"
            propietat = "GdPicture"
        Else
            esDelTipus = False
        End If

        If esDelTipus Then
            observacio = "OBSOLET"
            color = ColorUpgrade.Resolt
            tipusObjecte = "9994"
            propietat = "GdPicture"
        End If

        '
        If esDuplicatTipusControlPropietat("GdPicture", "") Then Return False

        Return True
    End Function

    ''' <summary>
    ''' carrega el màxim nombre de regles Regex que pot fer "matchs" amb linies que contenen GetTwipsToPixels i/o GetPixelsToTwips
    ''' </summary>
    Private Sub CarregarReglesRegexTwipsPixels()
        Dim REGEX_GETS As String = REGEX_GTP & "\(\(*" & REGEX_GPT
        Dim REGEX_PRIMARYSCREEN As String = "[System\.Windows\.Forms\.]*Screen\.PrimaryScreen\.Bounds\.\w+"

        '? dimarts 14/12
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFA & ") (" & REGEX_MESMENYS & ") (" & REGEX_NUM & ")\)", "$1 $2 $3", 1))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFA & ")\)", "$1", 1))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_NUM & ")\)", "$1/15", 2))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1/15$2", 77))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & ")\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & ")\(([\w+\.]+ " & REGEX_OPE_ESP & REGEX_NUM & ")\)\)\)", "$1/15 $2 ($3/15 $4 ($5))", 7))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_NUM & ") " & REGEX_OPE_ESP & "\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & "\w+)\)\)", "$1/15 ($2/15 $3)", 9))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)\)", "$1/15$2$3", 80))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & ")\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)\)", "$1/15$2($3/15$4)", 101))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & ")\((" & REGEX_NUM & ")( \* )(\(" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_NUM & "\))\)\)", "$1/15$2($3/15$4$5)", 103))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & ")\(\(\(" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)\)" & " / 2\)\)", "$1/15$2((($3$4$5/15)$6$7) / 2)", 200))




        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_SIMPLE_PROP & ")\)", "$1", 10))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((Val\(Piece\(" & REGEX_ALFANUM & ", \w+, " & REGEX_NUM & ")\)", "$1", 11))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(Val\(CStr\(CDbl\((" & REGEX_ALFANUM & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "Val(CStr(CDbl($1)$2$3/15))$4$5/15", 111))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(Val\(regQuery_A_Key\((" & REGEX_ALFANUM & "), (" & REGEX_ALFANUM & "), (\""" & REGEX_ALFANUM & "\"")\)\)\)", "Val(regQuery_A_Key($1, $2, $3))", 111))




        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM & REGEX_OPE_ESP & ")\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & "\w+)\)\)", "$1 ($2/15 $3)", 8))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 12))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15$3$4/15", 122))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM & REGEX_OPE_ESP & " [a-zA-Z\." & REGEX_NUM & "]+ " & REGEX_OPE & ") (" & REGEX_NUM & ")\)", "$1 $2/15", 13))

        ' OKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFA & REGEX_OPE_ESP & REGEX_ALFA & REGEX_OPE_ESP & REGEX_ALFA & ")\)", "$1", 34))


        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((Piece\(\w+, \""\|\"", " & REGEX_NUM & ")\)", "$1", 19))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_OPE_2 & REGEX_NUM & ")\)", "$1/15", 3))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_OPE_2 & REGEX_ALFA & REGEX_OPE_ESP & REGEX_ALFA & ")\)", "$1", 20))

        ' OKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_OPE_2 & REGEX_ALFA & REGEX_OPE_ESP & REGEX_ALFA & ")\)", "$1", 21))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_OPE_2 & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & "\(" & REGEX_ALFANUM & " / " & REGEX_NUM & "\))\)", "$1", 211))

        ' OKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_OPE_2 & REGEX_ALFA & REGEX_OPE_ESP & REGEX_ALFA & ")\)", "$1", 22))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_OPE_2 & REGEX_ALFANUM_DOT & REGEX_ALFANUM & ")\)", "$1", 23))
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_OPE_2 & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & ")\)", "$1", 24))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_OPE_2 & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & ")\)", "$1", 25))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_OPE_2 & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & "\(" & REGEX_ALFANUM_DOT & " \* 4\))\)", "$1", 26))

        ' OKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\((" & REGEX_ALFANUM & ")" & "(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)(" & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "($1$2$3/15)$4", 30))  ' nota : el patro de substitucio li cal )

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & " 2)\)\)", "($1)", 3334))   ' nota : el patro de substitucio li cal )

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM & "\)" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 40))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((\(" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)\)", "$1$2/15)", 333))   ' nota : el patro de substitucio li cal )

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & ")\)", "$1$2$3", 1010))


        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & ")" & "\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)\)", "$1($2/15$3)", 31))

        ' OKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1", 35))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1", 36))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 39))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")(" & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1$2/15$3", 401))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")(" & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & ")\)", "$1$2/15$3", 40))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")(" & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & ")\)", "$1$2/15$3", 81))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & "\(" & REGEX_ALFANUM_DOT & " \* 2\))\)", "$1", 88))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_DOT_DOT_COLEC_PROP & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 89))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 85))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((\(" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM & "\))(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3/15", 355))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_DOT_PROP & REGEX_OPE_ESP & REGEX_COLEC_DOT_PROP & REGEX_OPE_ESP & REGEX_NUM & ")\)", "$1", 28))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_DOT_PROP & REGEX_OPE_ESP & REGEX_COLEC_DOT_PROP & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1", 37))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_DOT_PROP & REGEX_OPE_ESP & REGEX_COLEC_DOT_PROP & REGEX_OPE_ESP & REGEX_ALFANUM & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1", 38))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_PROP & ")\)", "$1", 60))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_PROP & REGEX_OPE_ESP & REGEX_COLEC_PROP & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 61))

        ' OKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_PROP & REGEX_OPE_ESP & REGEX_COLEC_PROP & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1", 62))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_PROP & REGEX_OPE_ESP & REGEX_COLEC_PROP & REGEX_OPE_ESP & REGEX_ALFANUM & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1", 63))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_PROP & REGEX_OPE_ESP & REGEX_COLEC_PROP & REGEX_OPE_ESP & "\(" & REGEX_COLEC_PROP & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & "\))\)", "$1", 64))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_COLEC_PROP & REGEX_OPE_ESP & REGEX_COLEC_PROP & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 633))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((\(" & REGEX_PRIMARYSCREEN & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & "\))(" & " / 2)\)", "$1$2", 33))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\(" & REGEX_GPT & "(\(" & REGEX_ALFANUM_DOT & ")" & "\)(" & REGEX_OPE_ESP & REGEX_ALFANUM & "\))" & "(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3$4/15", 32))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\(\(" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)( / 2)\)(" & REGEX_OPE_ESP & ")" & "(" & REGEX_NUM & ")\)\)", "(($1$2)$3$4/15)", 80111))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((\(" & REGEX_ALFANUM_DOT & "\))(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3/15", 722))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\(" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)\)", "($1$2$3/15)", 800))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM & REGEX_OPE_ESP & REGEX_ALFANUM & REGEX_OPE_ESP & REGEX_COLEC_PROP & ")\)", "$1", 82))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_DOT_COLEC_PROP & ")\)", "$1", 83))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_DOT_COLEC_PROP & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 87))

        ' OKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 84))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_DOT_COLEC & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2/15", 86))


        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((\(*" & REGEX_ALFANUM_DOT & " \* \-1\)*)\)", "$1", 90))   ' exemple: en GmsDoc

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((\(" & REGEX_NUM & " \* )(" & REGEX_NUM & ")( \* 4\))\)", "$1$2/15$3", 100))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\((" & REGEX_NUM & " \* )(" & REGEX_NUM & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "($1$2/15)$3$4/15", 102))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\(\((eventArgs\.\w+)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)(" & REGEX_OPE_ESP & ")(SystemFontProporcio)\)", "(($1$2$3/15)$4$5/15)$6$7", 104))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\(\((eventArgs\.\w+)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)(" & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)(" & REGEX_OPE_ESP & ")(SystemFontProporcio)\)", "(($1$2$3/15)$4$5/15)$6$7", 105))




        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\(\(\((\w+)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\) / " & "(SystemFontProporcio)\)", "(($1$2$3/15)$4$5$6$7/15) / SystemFontProporcio", 1055))




        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((\(" & REGEX_ALFANUM_DOT & REGEX_OPE_ESP & REGEX_ALFANUM_DOT & "\) / 2)\)", "$1", 84))


        ' *****************************************************************************************
        ' REGEX_GETS
        ' *****************************************************************************************

        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_PRIMARYSCREEN & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3/15", 3344))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_PRIMARYSCREEN & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)\) / 2(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "($1$2$3) / 2$4$5/15", 3344))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3/15", 322))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_ALFANUM & ")\)", "$1$2$3", 801))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)\)", "$1$2$3/15$4$5", 701))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)\)", "$1$2$3$4", 7015))



        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)\)", "$1$2$3$4$5", 7019))



        lstReglesRegex.Add(New ReglaRegex("\(*" & REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "($1$2$3$4$5/15", 7011))

        lstReglesRegex.Add(New ReglaRegex("\(*" & REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3$4$5/15", 70116))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & "\(" & REGEX_ALFANUM & REGEX_OPE_ESP & REGEX_ALFANUM & "\))\)", "$1$2", 8013))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")\((" & REGEX_NUM & ")(" & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1$2($3/15$4", 80133))



        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")" & "\(" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\) \* 2\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2($3 * 2)$4$5/15", 70118))





        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_COLEC_PROP & ")\)(" & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1$2", 422))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_COLEC_PROP & ")\)(" & REGEX_OPE_ESP & REGEX_ALFANUM & REGEX_OPE_ESP & REGEX_COLEC_PROP & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3/15", 700))

        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GETS & "\((" & REGEX_COLEC_PROP & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3/15", 801))




        ' ***********************************************************************
        ' REGEX_GPT
        ' ***********************************************************************
        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_COLEC_PROP & ")\)(" & REGEX_OPE_ESP & REGEX_ALFANUM & REGEX_OPE_ESP & REGEX_NUM & ")", "$1$2", 70))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")", "$1$2$3/15", 734))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & REGEX_ALFANUM & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")", "$1$2$3/15", 73))

        lstReglesRegex.Add(New ReglaRegex("\(" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)" & " / " & "(" & REGEX_NUM & ")", "($1$2$3/15) / $4/15", 733))

        lstReglesRegex.Add(New ReglaRegex("\(" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)" & " / 1", "($1$2$3/15) / 1", 733))


        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ") \)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")", "$1$2$3/15", 72))

        lstReglesRegex.Add(New ReglaRegex("\(" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\) \\ TwipsPerPixel", "($1$2$3/15) \ 1", 723))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")", "$1$2$3$4$5/15", 72))


        ' OKOKOK
        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((eventArgs\.\w)\)", "$1", 71))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_COMP_ESP & ")(" & REGEX_NUM & ")", "$1$2$3/2", 7111))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_COMP_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")", "$1$2$3$4$5/15", 71113))




        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_COMP_ESP & ")\((" & REGEX_ALFANUM & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")(" & REGEX_OPE_ESP & REGEX_ALFANUM & ")\)", "$1$2($3$4/15$5)", 7119))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & "[a-zA-Z\.\s\,\(\)]+" & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3/15)", 71111))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GTP & "\((" & REGEX_ALFANUM & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & "[a-zA-Z\.\s\,\(\)]+" & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "$1$2$3$4/15", 711117))




        lstReglesRegex.Add(New ReglaRegex("\(*" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)* \\ 2", "($1$2$3$4$5/15) \ 2", 7112))

        lstReglesRegex.Add(New ReglaRegex("(\(*)" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & "\(*)" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_ALFANUM & ")(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")" & "(\)*) \\ 2", "$1$2$3$4$5$6$7$8/15$9 \ 2", 7117))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_PRIMARYSCREEN & ")\)", "$1", 33))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_PRIMARYSCREEN & ")\) Then (" & REGEX_ALFANUM_DOT & ") = " & REGEX_GTP & "\((" & REGEX_ALFANUM & ")\)", "$1 Then $2 = $3", 331))

        lstReglesRegex.Add(New ReglaRegex("\(*" & REGEX_GPT & "\((" & REGEX_PRIMARYSCREEN & ")\)(" & REGEX_OPE_ESP & ")\(" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & REGEX_ALFANUM & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)\)* \\ 2", "($1$2($3$4$5/15)) \ 2", 334))


        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_COLEC_DOT_PROP & ")\)", "$1", 7113))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPERADORS & ")(" & REGEX_NUM & ")", "$1$2$3/15", 7114))

        lstReglesRegex.Add(New ReglaRegex("\(" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPE_ESP & ")(" & REGEX_NUM & ")\)", "($1$2$3/15)", 7115))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPERADORS & ")(" & REGEX_NUM & ") Then (" & REGEX_ALFANUM_DOT & ") = " & REGEX_GTP & "\((" & REGEX_NUM & ")\)", "$1$2$3/15 Then $4 = $5/15", 7116))

        lstReglesRegex.Add(New ReglaRegex(REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPERADORS & ")(" & REGEX_NUM & ") Then (" & REGEX_ALFANUM_DOT & ") = " & "(" & REGEX_NUM & ")", "$1$2$3/15 Then $4 = $5/15", 7116))


        lstReglesRegex.Add(New ReglaRegex(REGEX_TWIPSPERPIXEL, "1", 888))


        '! **************************************************************************
        '! REGLES DE LINIES AMB WIDTH/TOP/LEFT/RIGHT/AMPLA/...... I NUMEROS "AILLATS"
        '! **************************************************************************
        ''''lstReglesRegex.Add(New ReglaRegex(REGEX_NUM & REGEX_OPE_ESP & REGEX_ALFANUM, "1", 9000))

        lstReglesRegex.Add(New ReglaRegex("(?i)[\W\.]*column\(\d+\)\.Width = \d+", "1", 9001))

        lstReglesRegex.Add(New ReglaRegex("Val\(Piece\(\w+, \w+, \d+\)\)", "1", 9002))

        lstReglesRegex.Add(New ReglaRegex("(?i)cmd\w+\.\w+ = [\w\.]+" & REGEX_OPE_ESP & REGEX_NUM, "1", 9002))

        lstReglesRegex.Add(New ReglaRegex("(?i)[\W\.]*Current", "1", 9002))

        lstReglesRegex.Add(New ReglaRegex("(?i)[\W\.]*DrawWidth", "1", 9002))

        lstReglesRegex.Add(New ReglaRegex("(?i)Me\.Line\w+\.\w+ =", "1", 9002))

        '! ListView + ColumnHeader
        lstReglesRegex.Add(New ReglaRegex("(Me\.\w+_ColumnHeader_\d+\.Width) = (\w+)", "$1 = $2/15", 9003))

        '! Toolbar/Button
        lstReglesRegex.Add(New ReglaRegex("(Me\.\w+_Button\d+\.Width) = (\w+)", "$1 = $2/15", 9004))

        '! MDI + StatusBar
        lstReglesRegex.Add(New ReglaRegex("(MDI\.StatusBar\.Panels\(\w+\)\.Width) = (\w+)", "$1 = $2/15", 9005))


        lstReglesRegex.Add(New ReglaRegex("If " & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPERADORS & ")(" & REGEX_ALFA & ") Then", "If $1$2$3 Then", 9006))



        lstReglesRegex.Add(New ReglaRegex("If " & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPERADORS & ") ([\(\)\w\s\+\-]+) Then", "If $1$2$3 Then", 9006))




        lstReglesRegex.Add(New ReglaRegex("If " & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPERADORS & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\) Then", "If $1$2$3 Then", 90066))



        lstReglesRegex.Add(New ReglaRegex("(" & REGEX_ALFANUM & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_OPERADORS & ")" & "(" & REGEX_NUM & ") Then", "$1$2$3$4/15 Then", 90068))




        lstReglesRegex.Add(New ReglaRegex("If (" & REGEX_ALFA & ")(" & REGEX_OPERADORS & ")" & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\) Then", "If $1$2$3 Then", 9007))


        lstReglesRegex.Add(New ReglaRegex("(" & REGEX_ALFANUM_DOT & ") = " & REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)$", "$1 = $2", 9008))





    End Sub

    ''' <summary>
    ''' Converteix la cadena que m'arriba del Wizard 2008 referent als Twips/Pixels i els cambiem pel corresponent GetXXXXXX
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <returns></returns>
    Private Function SubtituirVBTwipsToGets(ByVal cadena As String) As String
        ' abans de tots hem de substituim
        cadena = cadena.Replace("VB6.TwipsToPixels", GETTWIPSTOPIXELS)
        cadena = cadena.Replace("VB6.PixelsToTwips", GETPIXELSTOTWIPS)
        cadena = cadena.Replace("VB6.TwipsPerPixelX", "1")   '? conversio
        cadena = cadena.Replace("VB6.TwipsPerPixelY", "1")   '? conversio

        Return cadena
    End Function

    ''' <summary>
    ''' Compara el resultat esperat amb el resultat calculat de la conversio twips/pixels
    ''' <para>Si no troba cap regla que faci que el resultat esperat coincideix amb el resultat calculat, retorna -1</para>
    ''' En qualsevol altre cas, retorna el numero de regla
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <returns></returns>
    Private Function PassaTest(ByVal linia As String, ByVal fitxer As String, ByVal numLinia As Integer) As Integer
        Dim resultatEsperat As String = String.Empty

        linia = SubtituirVBTwipsToGets(linia)

        '! recorrem totes les regles Regex fins a trobar la que coincideix o fins al final si no la troba
        For Each rr As ReglaRegex In lstReglesRegex

            '! si no fa match busquem la seguent regla
            If Not RegexCercaCadena(linia, rr.patroCerca) Then Continue For

            '! apliquem la regla de substitucio
            resultatEsperat = RegexSubstitucioCadena(linia, rr.patroCerca, rr.patroSubstitucio)

            If resultatEsperat <> "" Then
                Dim splitList() As String = {}

                '! hem de treure/calcular totes les expressions que son REGEX_NUM/15 pel seu valor
                If RegexCercaCadena(resultatEsperat, "(" & REGEX_NUM & "/15)", splitList) Then

                    '! recorrem tots els possibles num/15 (n'hi pot haber més d'1!)
                    For Each n In splitList
                        Dim calcul As Integer

                        '! comprovem si és un Integer (per evitar InvalidCastException!)
                        If Not Integer.TryParse(n.Split("/")(0), calcul) Then Continue For   ' segueix buscant la seguent regla

                        '! condicio : només considerem que es un twips si el valor és >=15 O BÉ es negatiu (exemple : -450)
                        If CInt(calcul) >= 15 OrElse CInt(calcul) < 0 Then

                            '! dividim per convertir de Twips a Pixels
                            calcul = calcul / 15
                        End If

                        resultatEsperat = resultatEsperat.Replace(n, calcul.ToString)
                    Next

                End If

                '! comprovem si el resultat calculat coincideix amb el resultat esperat  
                If resultatEsperat = SubstituirTwipsPixels(linia, fitxer, numLinia) Then
                    Debug.Print(String.Format("     La regla {0} fa match", rr.numRegla))

                    Return rr.numRegla
                Else
                    Debug.Print(String.Format("     La regla {0} fa match però no passa el test. Resultat : {1}", rr.numRegla, resultatEsperat))
                End If

                '! hem trobat una regla que fa match però no coincideix amb la calculada
                '! NOTA : podria ser que hi hagués més regles posteriors a aquesta que fessin match
                ''''Return -1
            Else
                'Debug.Print(String.Format("        La regla {0} fa match però no passa el test", rr.numRegla))
            End If
        Next

        Return -1
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <param name="projecte"></param>
    ''' <returns></returns>
    Private Function GetDirectoriProjecteFitxer(ByVal fitxer As String, ByRef projecte As String) As String
        Dim splitList() As String = {}

        Dim directoriProjecte As String = cboDirectoryNet.Text

        '! fitxer que pertany a un altre "projecte" (COMMON, FORMCOMUNS, i fins i tot pot ser RECURSOS HUMANOS!)
        If fitxer.Contains("..") Then

            If EsFitxerDesigner(fitxer.Split("\")(1)) Then
                '! calculem el nom del directori pare
                directoriProjecte = New DirectoryInfo(cboDirectoryNet.Text & "..").FullName

                '! agafem el projecte
                projecte = "..\"   ''''Split(directoriProjecte, "\").Last
            Else
                RegexCercaCadena(fitxer, "((\.\.\\)+[\w\s]+)", splitList)    '? el nom d'un projecte pot tenir espais en blanc !

                projecte = splitList(1)

                directoriProjecte = cboDirectoryNet.Text & projecte
            End If

        End If

        Return directoriProjecte
    End Function

    ''' <summary>
    ''' Guardem en un diccionary la llista de dates de redisseny
    ''' </summary>
    Private Sub CarregarFitxerDatesRedissenyToList(ByVal fitxer As String)
        Dim projecte As String = String.Empty
        Dim lstLiniesFitxerRedisseny As New List(Of String)

        '! calculem el directori del projecte del fitxer i el projecte (byref)
        Dim directoriProjecte As String = GetDirectoriProjecteFitxer(fitxer, projecte)

        '! comprovem si ja existeix en el Dictionary
        If dictFitxersDatesRedisseny.ContainsKey(projecte.ToUpper) Then Exit Sub

        '! calculem el fitxer "dates redisseny"
        Dim fitxerDatesRedisseny As String = GetFitxerRedisseny(directoriProjecte)

        '! si no existeix el fitxer "dates redisseny", el creem
        If Not File.Exists(fitxerDatesRedisseny) Then
            Dim fs As FileStream = File.Create(fitxerDatesRedisseny)
            fs.Close()
        End If

        '! carregeum el fitxer "dates redisseny" a la llista
        For Each linia As String In File.ReadAllLines(fitxerDatesRedisseny, encoding)
            lstLiniesFitxerRedisseny.Add(linia)
        Next

        '! afegim al Dictionary
        dictFitxersDatesRedisseny.Add(projecte.ToUpper, lstLiniesFitxerRedisseny)
    End Sub

    ''' <summary>
    ''' Carreguem el fitxer de "substitucions" en una llista
    ''' </summary>
    Private Sub CarregarSubstitucionsToList(Optional ByVal fitxerSubstitucio As String = "substitucions.txt")
        lstLiniesSubstitucio = New List(Of String)

        'fitxerSubstitucio = "substitucions_ColeccioControls.txt"

        MsgBox(String.Format("Apliquem el fitxer de substitucions : {0}", fitxerSubstitucio), MsgBoxStyle.Information, "Migració")


        '! Fitxer de mapejos, expressions regulars, que ens serveix per validar
        Dim fitxer_substitucions As String = IIf(esProva, "substitucionsProves.txt", fitxerSubstitucio)
        Dim fitxerSubstitucions As String = String.Format("{0}\{1}", Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory).ToString).ToString, fitxer_substitucions)
        Dim liniesSubstitucions As List(Of String) = LlegeixFitxer(fitxerSubstitucions, encoding)

        For Each linia As String In liniesSubstitucions
            '! excloim les linies en blanc i les linies que comencen en "" (comentaris)
            If linia.Trim <> String.Empty AndAlso Mid(linia, 1, 2) <> "--" Then
                lstLiniesSubstitucio.Add(linia)
            End If
        Next
    End Sub

    ''' <summary>
    ''' "Valida". Executa totes les substitucions (inclou lògicament les linies Regex!)
    ''' </summary>
    Private Sub Validar(validacioAutomatica As Boolean)
        Try
            If Not chkMigrarTwipsPixels.Checked Then
                MsgBox("El check de migrar TwipsPixels està desmarcat", MsgBoxStyle.Information, "Migració")
            End If

            VB6 = False

            '! Proves o Real
            If Not esProva Then
                numFitxerProcessat = 1

                Dim quantsValidats As Integer
                If validacioAutomatica Then
                    quantsValidats = lstItemsFoundNet.Items.Count + lstItemsFoundNet_2.Items.Count
                Else
                    quantsValidats = (From itemChecked In lstItemsFoundNet.Items Where itemChecked.checked = True).Count
                    quantsValidats = quantsValidats + (From itemChecked In lstItemsFoundNet_2.Items Where itemChecked.checked = True).Count
                End If

                If quantsValidats > 0 Then
                    Dim numProcessat As Integer = 0

                    '! apliquem les validacions sobre els fitxers checkejatS
                    For Each fitxerVB As ListViewItem In lstItemsFoundNet.CheckedItems
                        numProcessat = numProcessat + 1

                        If fitxerVB.Checked OrElse validacioAutomatica Then
                            Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxerVB.Text)

                            '! comprovem si cal validar (per no repetir validacions sobre fitxers ja validats!)
                            If Not CalValidar(fitxerProcessar) Then Continue For

                            If EstaEnLlistaNegra(fitxerProcessar, False) Then Continue For

                            RefrescarProces("Validacions", String.Format("{0}", fitxerVB.Text), numProcessat, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)

                            '! calculem els controls que té el formulari
                            If EsFitxerDesigner(fitxerProcessar) Then
                                controlsNetByForm = ObtenirControlsByDesigner(fitxerProcessar)
                            End If


                            Dim liniesSubstituides As New List(Of String)

                            qua.Clear()

                            Try
                                '! apliquem totes les regles al fitxer
                                AplicaReglesFitxerNet(fitxerProcessar, liniesSubstituides)
                            Catch ex As Exception
                                MessageBox.Show(String.Format("El fitxer {0} no s'ha migrat per problemes en el procés", fitxerProcessar), "Error procés", MessageBoxButtons.OK)

                                Continue For
                            End Try


                            '! WorkAround : ordre tabpages (si utilitzem GmsTabPages!)
                            WorkAround_ReordenarTabControlTabPages(fitxerProcessar, liniesSubstituides)

                            '! guardem els controls ActiveX (ex: AxPushButton) que estàn dins un contenidor NET (ex : TabControlPage)
                            '! Nota : pot ser que estigui directament dins el contenidor NET o a través d'un altra contenidor (NET o AX)                        
                            Dim controlsAxINContenidor As New Dictionary(Of String, MovimentLinia)

                            ''''''WorkAround_ControlsAX_IN_ContenidorNET(liniesSubstituides, controlsAxINContenidor)     caldria fer-ho per tots els controls ActiveX.No és de moment una bona solució.

                            Workaround_ReordenarControlArray_Frame(fitxerProcessar, liniesSubstituides)
                            'x WorkAround_ModificarWidth_Height(fitxerProcessar, liniesSubstituides)          dijous 28/10 . Sembla que no s'ha de posar

                            If File.Exists(fitxerProcessar) Then
                                File.WriteAllLines(fitxerProcessar, liniesSubstituides.ToArray, GetFileEncoding(fitxerProcessar))
                            Else
                                Dim fitxerNoExisteix = 1
                            End If

                            numFitxerProcessat = numFitxerProcessat + 1

                        End If
                    Next

                    '! apliquem les validacions sobre els fitxers checkejatS
                    For Each fitxerVB As ListViewItem In lstItemsFoundNet_2.CheckedItems

                        If fitxerVB.Checked OrElse validacioAutomatica Then
                            lblProcessant.Text = String.Format("Processant fitxer {0} ....", fitxerVB.Text)
                            lblEtiqueta.Text = String.Format("Validacions : {0} de {1}", numFitxerProcessat.ToString, lstItemsFoundNet.CheckedItems.Count + lstItemsFoundNet_2.CheckedItems.Count)
                            lblProcessant.Refresh()
                            lblEtiqueta.Refresh()

                            Dim liniesSubstituides As New List(Of String)

                            Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxerVB.Text)

                            '! apliquem totes les regles al fitxer
                            AplicaReglesFitxerNet(fitxerProcessar, liniesSubstituides)

                            File.WriteAllLines(fitxerProcessar, liniesSubstituides.ToArray, encoding)

                            numFitxerProcessat = numFitxerProcessat + 1
                        End If
                    Next

                Else
                    MsgBox("No hi ha cap fitxer/S per validar", MsgBoxStyle.Information, "Migració")
                End If

            Else  '! es Prova
                Dim liniesSubstituides As New List(Of String)

                AplicaReglesFitxerNet("M:\MP_Net\Entigest\Relaciones Publicas\MigracioVB6_Net\Proves.vb", liniesSubstituides)
            End If


            '! gravem l'estat conforme hem fet les validacions
            RegistrarTrazaEstatMigracio(String.Format(FORMAT_FITXER_TRACING_MIGRACIO, Date.Now.ToString("dd/MM/yyyy HH:mm:ss"), E_EstatProcesMigracio.Executat_Validacio.ToString, "OK", "Executat validacions"))

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Copiem els fitxers Net BACKUP corresponents als fitxers VB6 de la llista lstItemsFoundVB6 a la llista lstItemsFoundNet i lstItemsFoundNet_2 
    ''' </summary>
    Private Sub CopiaFitxersNetBackupToMP_NET()
        Dim numCopiats As Integer = 0

        Try
            '! comprovem si el fitxer NET BACKUP correspon a un dels fitxer VB6 modificats
            For Each fitxerVB6 As ListViewItem In lstItemsFoundVB6.Items
                '
                Dim fitxerSenseExtensio As String = fitxerVB6.Text.Replace(EXTENSIO_FRM, "").Replace(EXTENSIO_BAS, "").Replace(EXTENSIO_CLS, "")

                '! SI EL FITXER ES NUCLEAR, AVISEM SI REALMENT VOL SOBREESCRIURE EL FITXER EN LA CARPETA MP_NET
                If EsFitxerNuclear(fitxerSenseExtensio & ".") Then
                    Dim result As DialogResult = MessageBox.Show(String.Format("EL FITXER {0} ES NUCLEAR.SI COPIES EL FITXER, POTS PERDRE MODIFICACIONS FETES!!!!", fitxerSenseExtensio), "Copia fitxers NetBackup a Net", MessageBoxButtons.YesNo)

                    If result = System.Windows.Forms.DialogResult.No Then
                        Continue For
                    End If

                End If

                '
                Dim subArbreSenseSlashFinal As String = cboDirectoryVB6.Text.Substring(0, cboDirectoryVB6.Text.Length - 1)
                Dim subArbre As String = Path.GetDirectoryName(fitxerVB6.Text).Replace(subArbreSenseSlashFinal, "")
                subArbre = IIf(String.IsNullOrEmpty(subArbre), "\", subArbre)

                If String.IsNullOrEmpty(CarpetaDotNET()) Then
                    MessageBox.Show("No existeix el fitxer NET corresponent a BACKUP")
                    Exit Sub
                End If

                Dim fitxerBackup As String = CarpetaDotNET() & subArbre & "\" & Path.GetFileNameWithoutExtension(fitxerVB6.Text)
                Dim fitxerNET As String = IIf(Not fitxerSenseExtensio.Contains(CARPETA_VB6), cboDirectoryNet.Text & fitxerSenseExtensio.Replace(CARPETA_VB6, CARPETA_NET), fitxerSenseExtensio.Replace(CARPETA_VB6, CARPETA_NET))

                '! comprovem si existeix el directori desti
                Dim carpetaDesti As String = Path.GetDirectoryName(fitxerNET)
                If Not String.IsNullOrEmpty(carpetaDesti) AndAlso Not Directory.Exists(carpetaDesti) Then
                    Directory.CreateDirectory(carpetaDesti)
                End If

                '! nomes copiem si es "migrable" i "no es nuclear"
                If File.Exists(fitxerBackup & EXTENSIO_VB) AndAlso EsMigrableFitxer(fitxerBackup & EXTENSIO_VB) AndAlso Not EsFitxerNuclear(fitxerBackup & EXTENSIO_VB) Then
                    File.Copy(fitxerBackup & EXTENSIO_VB, fitxerNET & EXTENSIO_VB, True)

                    numCopiats = numCopiats + 1
                    lblProcessant.Text = String.Format("{0} de {1} Fitxer : {2} ....", numCopiats, lstItemsFoundVB6.Items.Count, fitxerNET)

                    If fitxerVB6.Text.ToUpper().Contains(EXTENSIO_BAS.ToUpper) OrElse fitxerVB6.Text.ToUpper().Contains(EXTENSIO_CLS.ToUpper) Then   ' es modul o classe
                        lstItemsFoundNet_2.Items.Add(fitxerNET & EXTENSIO_VB)
                    Else '! es formulari
                        lstItemsFoundNet.Items.Add(fitxerNET & EXTENSIO_VB)
                    End If
                End If

                If File.Exists(fitxerBackup & EXTENSIO_DESIGNER) Then
                    File.Copy(fitxerBackup & EXTENSIO_DESIGNER, fitxerNET & EXTENSIO_DESIGNER, True)

                    lstItemsFoundNet.Items.Add(fitxerNET & EXTENSIO_DESIGNER)
                End If

                If File.Exists(fitxerBackup & EXTENSIO_RESX) Then
                    File.Copy(fitxerBackup & EXTENSIO_RESX, fitxerNET & EXTENSIO_RESX, True)
                End If

            Next

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Retorna si un fitxer pertany al Nucli de classes/formularis GMS
    ''' OBSERVACIO : un fitxer Nuclear pot ser que s'hagi modificat i per tant es "migrable"
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function EsFitxerNuclear(fitxer As String) As Boolean
        If fitxer.ToUpper.Contains("DATACONTROL.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\AGENDACALENDARI.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\GMSCOMBO.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\CODEJOC.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\CREAPDF.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\DIRECTORIS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\DRAG&DROP_TREEVIEW.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\MAIN.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\LLIBAPLICACIO.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\ZCONEXIO.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\INTERNET.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\MENUS.") OrElse   '  CLASSE NUCLEAR                
                fitxer.ToUpper.Contains("\CHTMLHELP.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\CONEXIO.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\CAIXES.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\DICCIONARI.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\DROP_TREEVIEW.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\EJECUCIONLISTADOS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\ENTITATS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\ESTIL_2007.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\ESTIL_2003.") OrElse   '  CLASSE NUCLEAR 
                fitxer.ToUpper.Contains("\FLEXCELLGRID.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FLEXCELLGRIDCALENDARI.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\GMSMAIL.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\GMSPOPUP.") OrElse   '  CLASSE NUCLEAR                
                fitxer.ToUpper.Contains("\LLIBUTILS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\LLIBSISTEMA.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\LLIBCACHEOBJECT.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\LLIBDOCMANAGER.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\LLIBINCIDENCIES.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\LLIBMANDATS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\LLIBCACHE.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\SSCOMBO.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\TOOLS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\TREEVIEW.") OrElse   '  CLASSE NUCLEAR                                               
                fitxer.ToUpper.Contains("\REGISTRE.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMBACKGROUND.") OrElse   '  CLASSE NUCLEAR                
                fitxer.ToUpper.Contains("\FRMBARRA.") OrElse   '  CLASSE NUCLEAR                
                fitxer.ToUpper.Contains("\FRMPRINTSCREEN.") OrElse   '  CLASSE NUCLEAR                
                fitxer.ToUpper.Contains("\FRMVISLLISTARTF.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMIMATGES.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMIMATGESFONDOS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMFITXERSREMOTS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMIMATGESEMPRESAS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMCONSULTES2007.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMCONSULTAGLOBAL.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMCONSULTAGLOBAL2007.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMPOPUPMENUS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMMOSTRAIMATGE.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMPRINTSCREEN.") OrElse   '  CLASSE NUCLEAR  
                fitxer.ToUpper.Contains("\FRMMENUS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMCONEXIONDB.") OrElse   '  CLASSE NUCLEAR                
                fitxer.ToUpper.Contains("\FRMCALENDARI.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMVERIFICABAIXES.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMCONSULTAGRID.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMPARAMETRESLOCALS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMGRUPOSUSUARIOSARBRE.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMDICCIONARI.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMDIRECTORIS.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMCONSULTES.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\MICROSOFTGMS.") OrElse   '  CLASSE NUCLEAR      
                fitxer.ToUpper.Contains("\FRMEDITORTEXT.") OrElse   '  CLASSE NUCLEAR  
                fitxer.ToUpper.Contains("\SSCOMBO.") OrElse   '  CLASSE NUCLEAR
                fitxer.ToUpper.Contains("\FRMEXECUTAPROCES.") OrElse   '  CLASSE NUCLEAR 
                fitxer.ToUpper.Contains("\IMPRESSIO.") Then   '  CLASSE NUCLEAR

            'fitxer.ToUpper.Contains("\FRMVISORPDF.") OrElse   '  CLASSE NUCLEAR

            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' si una linia està migrada
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <returns></returns>
    Private Function EsLiniaMigrada(ByVal linia As String) As Boolean
        '? ***********   no tractem les linies que continguin :
        If linia.ToUpper.Contains("UPGRADE_") _
                OrElse linia.ToUpper.Contains("MIGRAR") _
                OrElse linia.ToUpper.Contains("''MIGRAT") _
                OrElse linia.ToUpper.Contains("''RENDIMENT") _
                OrElse linia.ToUpper.Contains("NOCAL") _
                OrElse linia.ToUpper.Contains("INCORRECTE") _
                OrElse linia.ToUpper.Contains("SAFENATIVEMETHODS") _
                OrElse linia.ToUpper.Contains("DUPLICADA") _
                OrElse linia.ToUpper.Contains("''") _
                OrElse linia.ToUpper.Contains("OBSERVACIO_MIGRACIO") _
                OrElse linia.ToUpper.Contains("WORKAROUND") _
                OrElse linia.ToUpper.Contains("''NOCALMIGRAR") _
                OrElse linia.ToUpper.Contains("ELIMINADA") Then

            'OrElse liniesFitxerVB(i).ToUpper.Contains("MIGRATPIXELS") _    POT SER QUE S'HAGI DE MIGRAR COSES QUE NO TENEN A VEURE AMB ELS PIXELS/TWIPS!!!

            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' "DESCARTEM" ELS FITXERS OBSOLETS. SEGONS JULI I/O CACHETOOLS
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function EsObsoletFitxer(fitxer As String) As Boolean
        If fitxer.ToUpper.Contains("PENDENTELIMINAR") OrElse
                fitxer.ToUpper.Contains("FRMVISLLISTATRTF") Then
            Return True
        End If

        'fitxer.ToUpper.Contains("FRMVISOR") OrElse

        Return False
    End Function

    ''' <summary>
    ''' Retorna si un fitxer es "migrable" o no
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function EsMigrableFitxer(fitxer As String) As Boolean
        If fitxer.ToUpper.Contains("ASSEMBLYINFO.") OrElse
                fitxer.ToUpper.Contains("CONTROLARRAY.") OrElse     ' NOVA CLASSE
                fitxer.ToUpper.Contains("DRAGGEDITEM.") OrElse       ' NOVA CLASSE                
                fitxer.ToUpper.Contains("AXSSDBCOMBOARRAY.") OrElse    'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXGROUPBOXARRAY") OrElse    'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXGRIDARRAY") OrElse    'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXCHECKBOXARRAY.") OrElse     'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXRADIOBUTTONARRAY.") OrElse   'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXPUSHBUTTONARRAY.") OrElse    'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXTABCONTROLPAGEARRAY.") OrElse    'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXWEBBROWSERARRAY.") OrElse    'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXGMSIMPORTSARRAY.") OrElse   'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXGMSDATAARRAY.") OrElse    'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXGMSTEMPSARRAY.") OrElse    'CONTROL ARRAY
                fitxer.ToUpper.Contains("AXSCROLLBARARRAY") OrElse     'CONTROL ARRAY
                fitxer.ToUpper.Contains("GMSCHECKBOXARRAY.") OrElse    'CONTROL ARRAY
                fitxer.ToUpper.Contains("GMSRADIOBUTTONARRAY.") OrElse   'CONTROL ARRAY
                fitxer.ToUpper.Contains("UPGRADESUPPORT") OrElse
                fitxer.ToUpper.Contains("MY PROJECT") OrElse
                fitxer.ToUpper.Contains("FORMPARENT") OrElse    ' NOVA CLASSE
                fitxer.ToUpper.Equals("UTILS") OrElse    ' NOVA CLASSE                                
                fitxer.ToUpper.EndsWith(".EXE") OrElse  ' EXECUTABLES
                fitxer.ToUpper.EndsWith(".USER") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".CS") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".CSPROJ") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".DPR") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".PP") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".INC") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".PL") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".ASM") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".H") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".PAS") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".RC") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".RAP") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".C") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".GIF") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".JPG") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".CSP") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".PNG") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".CSS") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".PDF") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".TIF") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".JBF") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".DOC") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".JS") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.EndsWith(".DOCX") OrElse  ' EXTENSIO ESPECIAL
                fitxer.ToUpper.Contains("EXAMPLES") OrElse     ' EXAMPLES
                fitxer.ToUpper.Contains("FERFRANS") OrElse
                fitxer.ToUpper.Contains("SUBSTITUIR.") Then

            'fitxer.ToUpper.Contains("CREAPDF") OrElse


            ' fitxer.ToUpper.Contains("FRMVISLLISTATRTF") OrElse   '   OBSOLET JULI
            ' fitxer.ToUpper.Contains("CODEJOC") OrElse
            ' fitxer.ToUpper.Contains("FRMENTITATS.DESIGNER") OrElse   '   HEM D'EXCLOURE PERQUE HI HA ELS "ESPECIALS" QUE NO HEM D'ELIMINAR _
            ' fitxer.ToUpper.Contains("FRMENTITATSSUCURSALS.DESIGNER") OrElse   '   HEM D'EXCLOURE PERQUE HI HA ELS "ESPECIALS" QUE NO HEM D'ELIMINAR 
            'fitxer.ToUpper.Contains("FRMCONSULTAGLOBAL2007") OrElse   '   MIRAR : "SUBSTITUIT PERE CAS DIFERENT"

            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' Retorna si el fitxer es un formulari (o classe) "nou" (que hem hagut d'afegir a la part .NET!) o no
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function EsNouForm(ByVal fitxer As String) As Boolean
        If fitxer.ToUpper.Contains("FORMPARENT") OrElse
                fitxer.ToUpper.Contains("FRMOBRIRTOTSFORMULARIS") OrElse
                fitxer.ToUpper.Contains("FORMLOGOPARENT") OrElse
                fitxer.ToUpper.Contains("SAFENATIVEMETHODS") OrElse
                fitxer.ToUpper.Contains("DRAGGEDITEM") OrElse
                fitxer.ToUpper.Contains("PRINTERSCOLLECTION") OrElse
                fitxer.ToUpper.Contains("PRINTERSMODULE") OrElse
                fitxer.ToUpper.Contains("FORMSCOLLECTION") OrElse
                fitxer.ToUpper.Contains("PROPERTYBAG") OrElse
                fitxer.ToUpper.Contains("CONTROLARRAY") OrElse
                fitxer.ToUpper.Contains("GMSCOMBO") OrElse
                fitxer.ToUpper.Contains("GMSTABCONTROL") OrElse
                fitxer.ToUpper.Contains("NATIVEMETHODS") OrElse
                fitxer.ToUpper.Contains("DLL\UTILS") OrElse
                fitxer.ToUpper.Equals("UTILS") OrElse
                fitxer.ToUpper.Contains("NOVESCLASSES\") OrElse
                fitxer.ToUpper.Contains("PRINTER\") OrElse
                fitxer.ToUpper.Contains("MDIFORM") Then
            Return True
        End If

        '? excloim els formularis del tipus FormN
        'If RegexCercaCadena(fitxer, "Form\d+") Then
        '    Return True
        'End If


        Return False
    End Function

    ''' <summary>
    ''' Neteja labels, dataTables,
    ''' </summary>
    Private Sub NetejarTot()
        lblProcessant.Text = String.Empty
        lblFitxersModificats.Text = String.Empty

        dtVB6 = New DataTable("Items")
        dtVB6.Columns.Add("fitxer")
        dtVB6.Columns.Add("data")

        dtVB6_2 = New DataTable("Items")
        dtVB6_2.Columns.Add("fitxer")

        dtNET = New DataTable("Items")
        dtNET.Columns.Add("fitxer")

        dtNET_2 = New DataTable("Items")
        dtNET_2.Columns.Add("fitxer")

        Netejar("VB6")
        Netejar("NET")

        '! netegem ListViews i TreeListViewS (pestanyes)
        lvWarningsWorkAround.Clear()
        treeListViewUpgrades.Items.Clear()
        'lvWorkAroundPropertyCanOnly.Clear()
        lvTwipsPixels.Clear()
        lviAsObject.Clear()
        lviArquitectura.Clear()
        lviFontsTrueType.Clear()
        lviTreeViewAxGroupBox.Clear()
        lviFormsRedisseny.Clear()


        lstPatronsRegexTwipsPixelsProcessats.Clear()

        '! netegem comptadors
        comptadorLiniesTwipsPixels = 0
        comptadorAsObject = 0
        comptadorFontTrueType = 0

    End Sub

    ''' <summary>
    ''' Neteja les labels, dataTables (segons l'entorn : VB6 o .Net)
    ''' </summary>
    ''' <param name="tipus"></param>
    Private Sub Netejar(ByVal tipus As String)
        If tipus = "VB6" Then
            lstItemsFoundVB6.Items.Clear()
            lstItemsFoundVB6_2.Items.Clear()

            dtVB6.Clear()
            dtVB6_2.Clear()

            ''''numCoincidenciesVB6 = 0
            ''''numCoincidenciesVB6_2 = 0

            Label_1.Text = String.Empty
            Label_3.Text = String.Empty
            lblTrobats_1.Text = String.Empty
            lblTrobatModulsVB6.Text = String.Empty

        Else
            lstItemsFoundNet.Items.Clear()
            lstItemsFoundNet_2.Items.Clear()

            dtNET.Clear()
            dtNET_2.Clear()

            ''''numCoincidenciesNet = 0
            ''''numCoincidenciesNet_2 = 0

            Label_2.Text = String.Empty
            Label_4.Text = String.Empty
            lblTrobats_2.Text = String.Empty
            lblTrobatClassesNet.Text = String.Empty
        End If

        '! capsaleres dels "ListViews"
        lstItemsFoundNet.Columns.Item(0).Text = String.Empty
        lstItemsFoundVB6.Columns.Item(0).Text = String.Empty
        lstItemsFoundNet_2.Columns.Item(0).Text = String.Empty
        lstItemsFoundVB6_2.Columns.Item(0).Text = String.Empty
    End Sub

    ''' <summary>
    ''' Actualitzem el color d'un node
    ''' </summary>
    ''' <param name="node"></param>
    Private Sub ActualitzaColorNode(node As TreeNode)
        If lblTrobats_2.Text = lblTrobats_1.Text AndAlso lblTrobatClassesNet.Text = lblTrobatModulsVB6.Text Then
            node.ForeColor = Color.Green
        ElseIf lblTrobats_2.Text <> lblTrobats_1.Text AndAlso lblTrobatClassesNet.Text <> lblTrobatModulsVB6.Text Then
            node.ForeColor = Color.Red
        Else
            node.ForeColor = Color.Orange
        End If

        If EsProjecteNET_Eliminat(node.FullPath) Then
            node.ForeColor = Color.CornflowerBlue
        End If
    End Sub

    ''' <summary>
    ''' Recorrem tot l'arbre NET "marcant" cada projecte amb el "color/estat" que té
    ''' </summary>
    ''' <param name="nodeRoot"></param>
    Private Sub ActualitzaColorsTreeViewNET(nodeRoot As TreeNode, ByVal buscarFitxersProjecte As Boolean)
        ' recorrem tot els nodes
        For Each node As TreeNode In nodeRoot.Nodes
            If node.Text <> ".." Then

                NetejarTot()

                '! "busquem" o bé fitxers dins una carpeta o bé forms/classes/moduls a partir dels fitxers de projecte (VB6 i NET)
                BuscarItemsVB6_NET(buscarFitxersProjecte, False, False, node.FullPath)

                lblTrobats_1.Text = numCoincidenciesVB6.ToString
                lblTrobats_2.Text = numCoincidenciesNet.ToString
                lblTrobatModulsVB6.Text = numCoincidenciesVB6_2.ToString
                lblTrobatClassesNet.Text = numCoincidenciesNet_2.ToString

                ActualitzaColorNode(node)

                '! recursivitat
                ActualitzaColorsTreeViewNET(node, buscarFitxersProjecte)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Comprova si un determinat fitxer existeix en algun dels projectes GMS (*.vbp)
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <param name="carpeta"></param>
    ''' <returns></returns>
    Private Function CalMigrar(ByVal fitxer As String, ByVal carpeta As String) As Boolean
        If carpeta = "M:\MP\Docs" _
            OrElse carpeta = "M:\MP\Excel" _
                        OrElse carpeta = "M:\MP\Excel" _
                        OrElse carpeta = "M:\MP\Mbd" _
                        OrElse carpeta = "M:\MP\Grafics" _
                        OrElse carpeta = "M:\MP\Excel" _
                        OrElse carpeta = "M:\MP\Manual" _
                        OrElse carpeta = "M:\MP\Fonts" Then

            Return False
        End If



        Dim projecteVB6 As String = BuscarFitxersProjectesVB6(carpeta)

        If Not String.IsNullOrEmpty(projecteVB6) Then
            lblProcessant.Text = String.Format("Buscant fitxer {0} en el projecte {1}", fitxer, projecteVB6)

            If ExisteixFitxerEnProjecteVB6(fitxer, projecteVB6) Then Return True
        End If


        For Each subcarpeta As String In Directory.GetDirectories(carpeta)
            'Debug.WriteLine(String.Format(" -> Carpeta : {0}", subcarpeta))

            ''''projecteVB6 = BuscarFitxersProjectesVB6(subcarpeta)
            '''''Debug.WriteLine("Buscar fitxer : {0} en projecte {1}", fitxer, node)

            ''''If String.IsNullOrEmpty(projecteVB6) Then Continue For

            ''''lblProcessant.Text = String.Format("Buscant fitxer {0} en el projecte {1}", fitxer, projecteVB6)

            ''''If ExisteixFitxerEnProjecteVB6(fitxer, projecteVB6) Then Return True

            CalMigrar(fitxer, subcarpeta)
        Next

        Return False
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <param name="fitxerVB"></param>
    ''' <returns></returns>
    Private Function ExisteixFitxerEnProjecteVB6(ByVal fitxer As String, ByVal fitxerVB As String) As Boolean
        Dim liniesVB As List(Of String) = LlegeixFitxer(fitxerVB, encoding)

        Dim fitxerRegex As String = fitxer.Replace(cboDirectoryVB6.Text, "")

        ' si la carpeta del fitxer VBP i del fitxer que tractem es el mateix, cal fer un tractament diferent
        If Path.GetDirectoryName(fitxer) = Path.GetDirectoryName(fitxerVB) Then
            fitxerRegex = Path.GetFileName(fitxer)
        End If

        fitxerRegex = fitxerRegex.Replace("\", "\\")

        For i As Integer = 0 To liniesVB.Count - 1

            Dim splitList() As String = {}

            If fitxer.Contains(EXTENSIO_FRM) AndAlso RegexCercaCadena(liniesVB(i), "Form=(.*)" & fitxerRegex, splitList) Then  ' Formularis
                Return True
            ElseIf fitxer.Contains(EXTENSIO_BAS) AndAlso RegexCercaCadena(liniesVB(i), "Module=\w+;(.+)" & fitxerRegex, splitList) Then  ' classes
                Return True
            ElseIf fitxer.Contains(EXTENSIO_CLS) AndAlso RegexCercaCadena(liniesVB(i), "Class=\w+;(.+)" & fitxerRegex, splitList) Then  ' classes
                Return True
            ElseIf fitxer.Contains(EXTENSIO_CTL) AndAlso RegexCercaCadena(liniesVB(i), "UserControl=(.+)" & fitxerRegex, splitList) Then  ' classes                
                Return True
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' Retorna la data de la ultima migracio del projecte "seleccionat"
    ''' </summary>
    ''' <returns></returns>
    Private Function CarregaDataUltimaMigracio() As Date
        ' "recuperem" la data de la ultima migracio.A partir d'aquesta data podem saber els fitxer VB6 que s'han modificat
        Dim lstDatesMigracio As List(Of Date) = RetornaDatesMigracioFitxerTrazaMigracio()

        If lstDatesMigracio.Count > 0 Then
            dtDesde.Value = lstDatesMigracio(lstDatesMigracio.Count - 1)
        Else
            dtDesde.Value = New Date(2000, 1, 1)
        End If

        Return dtDesde.Value
    End Function

    ''' <summary>
    ''' Retorna el fitxer de tracing
    ''' </summary>
    ''' <param name="fitxerTracing"></param>
    ''' <returns></returns>
    Private Function GetFitxerTracingMigracio(Optional fitxerTracing As String = "") As String
        If String.IsNullOrEmpty(fitxerTracing) Then
            Return cboDirectoryVB6_BACKUP.Text & FITXER_TRACING_MIGRACIO
        End If

        Return fitxerTracing.ToUpper.Replace(CARPETA_VB6, CARPETA_VB6_BACKUP) & "\" & FITXER_TRACING_MIGRACIO
    End Function

    ''' <summary>
    ''' Mostra els projectes que s'han modificat des de la data de la seva ultima migració
    ''' </summary>
    Private Sub MostrarProjectesModificats(nodeRoot As TreeNode)

        ' recorrem tot els nodes
        For Each node As TreeNode In nodeRoot.Nodes
            If node.Text <> ".." Then

                ' comprovem si cal migrar el projecte en questio
                If EsMigrableNovament(node.FullPath) Then
                    Dim si As Boolean = True
                Else
                    Dim no As Boolean = False
                End If

                ' recursivitat
                MostrarProjectesModificats(node)
            End If
        Next

    End Sub

    ''' <summary>
    ''' Mira si estem en un "Estat" on podem iniciar una nova migració
    ''' Casos :
    '''         1.- Si chkMigrarNomesModificats AND hi ha "fitxers modificats des de" AND no estem en un "Estat" "intermig"
    '''         2.- Si NOT chkMigrarNomesModificats AND no estem en un "Estat" "intermig"
    ''' </summary>
    ''' <returns></returns>
    Private Function EsMigrableNovament(Optional fitxerTracingMigracio = "") As Boolean
        ' si volem migrar NOMES els "fitxers modificats des de"
        If chkMigrarNomesModificats.Checked Then

            Dim lstDatesMigracio As List(Of Date) = RetornaDatesMigracioFitxerTrazaMigracio(fitxerTracingMigracio)

            If lstDatesMigracio.Count = 0 Then Return True  ' el projecte no s'ha migrat mai

            If ExisteixFitxersVB6ModificatsDesDe(dtDesde.Value) Then
                numCoincidenciesVB6 = CarregarListView(dtVB6, lstItemsFoundVB6, "")
            End If

            If dtVB6.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Else  ' no estem en un estat intermig i volem migrar tots els fitxers
            Return True
        End If
    End Function

    ''' <summary>
    ''' Obtenim els "components" a partir d'un fitxer de projecte VB6
    ''' Exemple : Object = {88.0F75480-0574-11D0-8085-0000C0BD354B}#2.0#0; VisM64.ocx
    ''' </summary>
    Private Function CargarComponentsVB6(fitxerProjecteVB6 As String) As List(Of ComponentVB6)
        Netejar("VB6")

        Dim splitList() As String = {}
        Dim llistaComponentsVB6 As New List(Of ComponentVB6)
        Dim lstvi As ListViewItem

        Dim liniesVBProj As List(Of String) = LlegeixFitxer(fitxerProjecteVB6, encoding)

        For i As Integer = 0 To liniesVBProj.Count - 1

            If RegexCercaCadena(liniesVBProj(i), "Object={((\w+-)+\w+)}#(" & REGEX_NUM & "." & REGEX_NUM & ")#" & REGEX_NUM & ";\s((\w+\.)+)", splitList) Then

                Dim componentVB6 As New ComponentVB6()
                componentVB6.identificador = splitList(1)
                componentVB6.versio = splitList(3)
                componentVB6.nomComponent = splitList(4)
                llistaComponentsVB6.Add(componentVB6)
                ' 
                lstvi = New ListViewItem({splitList(4), splitList(1), splitList(3)})
                lstItemsFoundVB6.Items.Add(lstvi)

            End If

            If RegexCercaCadena(liniesVBProj(i), "Reference=\*\\G{(([0-9\-A-Z])*)}#(" & REGEX_NUM & "\." & REGEX_NUM & ")#" & REGEX_NUM & "#", splitList) Then

                Dim componentVB6 As New ComponentVB6()
                componentVB6.identificador = splitList(1)
                componentVB6.versio = splitList(3)
                componentVB6.nomComponent = splitList(4)
                llistaComponentsVB6.Add(componentVB6)
                ' 
                lstvi = New ListViewItem({splitList(4), splitList(1), splitList(3)})
                lstItemsFoundVB6.Items.Add(lstvi)

            End If
        Next

        Return llistaComponentsVB6
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sourceDirName"></param>
    ''' <param name="destDirName"></param>
    ''' <param name="copySubDirs"></param>
    Private Sub DirectoryCopy(sourceDirName As String, destDirName As String, copySubDirs As Boolean, designers_i_codi As Boolean)
        If sourceDirName = "" Then Exit Sub

        ' Get the subdirectories for the specified directory.
        Dim Dir As DirectoryInfo = New DirectoryInfo(sourceDirName)

        If (Not Dir.Exists) Then
            Throw New DirectoryNotFoundException("Source directory does Not exist Or could Not be found: " & sourceDirName)
        End If

        Try
            Dim dirs As DirectoryInfo() = Dir.GetDirectories()
            ' If the destination directory doesn't exist, create it.
            If (Not Directory.Exists(destDirName)) Then
                Directory.CreateDirectory(destDirName)
            End If

            ' Get the files in the directory And copy them to the New location.
            Dim files As FileInfo() = Dir.GetFiles()
            For Each File As FileInfo In files
                Dim temppath As String = System.IO.Path.Combine(destDirName, File.Name)
                Try
                    ' Excloim els fitxers de projecte i de solucio
                    If temppath.Contains(EXTENSIO_VBPROJ) OrElse temppath.Contains(EXTENSIO_SLN) Then Continue For

                    If designers_i_codi Then  ' hem de copiar tan el codi com el designer/resX
                        File.CopyTo(temppath, True)
                    Else
                        If (temppath.Contains(EXTENSIO_DESIGNER) OrElse temppath.Contains(EXTENSIO_RESX)) Then
                            File.CopyTo(temppath, True)
                        ElseIf (temppath.Contains(EXTENSIO_VB)) Then
                            File.CopyTo(temppath, True)

                        Else
                            Dim KK = 1
                        End If
                    End If

                Catch ex As Exception
                    If ex.Message.Contains("MSSCCPRJ.SCC") Then  ' aquests fitxers no son errors realment
                        '''' DirectoryCopy(TreeViewVB6.SelectedNode.Tag, rutaBACKUP, True)
                    Else
                        MsgBox(ex.Message, MsgBoxStyle.Information, "Error al borrar.")
                    End If
                End Try

            Next

            ' If copying subdirectories, copy them And their contents to New location.
            If copySubDirs Then

                For Each subdir As DirectoryInfo In dirs
                    Dim temppath As String = System.IO.Path.Combine(destDirName, subdir.Name)
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs, designers_i_codi)
                Next

            End If

            '
            RegistrarTrazaEstatMigracio(String.Format(FORMAT_FITXER_TRACING_MIGRACIO, Date.Now.ToString("dd/MM/yyyy HH:mm:ss"), E_EstatProcesMigracio.Executat_MigratsFitxers.ToString, "OK", "Executat final migració"))



        Catch ex As Exception

        End Try

    End Sub

    Private Function BuscarNomModul(ByVal cadena As String) As String
        Dim splitList() As String = {}

        If RegexCercaCadena(cadena, "=""([\w\&\;]+)", splitList) Then    ' els caracters & i ; són pel Drag&amp;Drop_TreeView.vb
            If splitList.Count = 4 Then
                Return splitList(2)
            ElseIf splitList.Count = 3 Then
                Return splitList(1)
            End If
        End If

        Return Nothing
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="proces"></param>
    ''' <param name="descripcio"></param>
    Private Sub RefrescarProces(ByVal proces As String, ByVal descripcio As String, ByVal numProcesat As String, ByVal numTotal As String)
        lblProces.Text = proces
        lblProces.Refresh()
        lblProcessant.Text = descripcio
        lblProcessant.Refresh()
        lblItemProcessat.Text = If(Not String.IsNullOrEmpty(numProcesat), String.Format("{0} de {1}", numProcesat, numTotal), "")
        lblItemProcessat.Refresh()
    End Sub

    ''' <summary>
    ''' Registrem una traza de l'estat del procés de migració.Marquem un "estat"
    ''' </summary>
    Private Sub RegistrarTrazaEstatMigracio(missatge As String)
        Dim fitxerEstatProcesMigracio As String = GetFitxerTracingMigracio()

        Try
            ' si existeix el fitxer
            If File.Exists(fitxerEstatProcesMigracio) Then

                Dim objWriter As New System.IO.StreamWriter(fitxerEstatProcesMigracio, True)

                objWriter.WriteLine(missatge)
                objWriter.Close()

            Else  ' si no existeix el fitxer
                Dim objWriter As New System.IO.StreamWriter(fitxerEstatProcesMigracio, False)

                ' NOTA : la primera migració, posem una data que "representi" que tots els fitxers s'han modificat (en aquest cas hem posat una data molt vella = 1/1/2000)
                objWriter.WriteLine(String.Format(FORMAT_FITXER_TRACING_MIGRACIO, New Date(2000, 1, 1), E_EstatProcesMigracio.Executat_CopiaBackupVB6.ToString, "", "Copia VB6 a BACKUP"))

                objWriter.WriteLine(missatge)
                objWriter.Close()
            End If

        Catch ex As Exception
            MessageBox.Show("Error al registre la traza de l'estat de migració")
        End Try

        '
        ActualitzaButonsEstatProcesMigracio()
    End Sub

    ''' <summary>
    '''  Retorna l'estat dins el proces de migracio
    ''' </summary>
    ''' <returns></returns>
    Private Function EstatProcesMigracio(linia As String) As E_EstatProcesMigracio
        Return DirectCast([Enum].Parse(GetType(E_EstatProcesMigracio), linia.Split("|")(1)), E_EstatProcesMigracio)
    End Function

    ''' <summary>
    ''' comprovem si la carpeta té fitxers
    ''' </summary>
    ''' <param name="directori"></param>
    ''' <returns></returns>
    Private Function EsProjecteNET_Eliminat(directori As String) As Boolean
        Try
            If Directory.GetFiles(directori).Count = 0 Then
                Return True
            End If

        Catch ex As Exception

        End Try

        Return False
    End Function

    ''' <summary>
    ''' Visibilitza/Invisibilitza opcions de menu de l'arbre NET
    ''' </summary>
    Private Sub ActualitzaMenu()
        If TreeViewNET.SelectedNode Is Nothing Then Exit Sub

        Dim nodeSeleccionat As TreeNode = TreeViewNET.SelectedNode

        For Each item As ToolStripMenuItem In ContextMenuStripProjectesNET.Items()

            ' si el node seleccionat es el "root" de l'arbre
            If nodeSeleccionat.Level = 0 Then

                If item.Name = "DiferenciesProjectesMenuItem" Then
                    item.Visible = False
                End If

                If item.Name = "DiferenciesCarpetesMenuItem" Then
                    item.Visible = False
                End If

                If item.Name = "EliminarProjecteToolStripMenuItem" Then
                    item.Visible = False
                End If
            Else  ' si el node seleccionat NO es el "root" de l'arbre

                If item.Name = "DiferenciesProjectesMenuItem" Then
                    item.Visible = True
                End If

                If item.Name = "DiferenciesCarpetesMenuItem" Then
                    item.Visible = True
                End If

                ' si no existeix un fitxer de projecte VB6, no té sentit buscar les diferencies entre projecte VB6 i NET
                If String.IsNullOrEmpty(BuscarFitxersProjectesVB6(nodeSeleccionat.FullPath.Replace(CARPETA_NET, CARPETA_VB6))) Then
                    If item.Name = "DiferenciesProjectesMenuItem" Then
                        item.Visible = False
                    End If
                Else
                    If item.Name = "DiferenciesProjectesMenuItem" Then
                        item.Visible = True
                    End If
                End If

                ' si la carpeta no té fitxers podem "clonar" però no "eliminar"
                If Directory.GetFiles(nodeSeleccionat.FullPath).Count = 0 Then
                    If item.Name = "CloneProjecteToolStripMenuItem" Then
                        item.Visible = True
                    End If

                    If item.Name = "EliminarProjecteToolStripMenuItem" Then
                        item.Visible = False
                    End If
                Else
                    If item.Name = "CloneProjecteToolStripMenuItem" Then
                        item.Visible = False
                    End If

                    If item.Name = "EliminarProjecteToolStripMenuItem" Then
                        item.Visible = True
                    End If

                End If

            End If
        Next

    End Sub

    ''' <summary>
    ''' Actualitza els combos NET i BACKUP a partir del combo de VB6
    ''' </summary>
    Private Sub ActualitzaCombos()
        cboDirectoryNet.Text = cboDirectoryVB6.Text.Replace(CARPETA_VB6, CARPETA_NET)
        cboDirectoryVB6_BACKUP.Text = cboDirectoryVB6.Text.Replace(CARPETA_VB6, CARPETA_VB6_BACKUP)
    End Sub

    ''' <summary>
    ''' Actualitza els combos NET i BACKUP a partir del combo de VB6
    ''' </summary>
    Private Sub ActualitzaCombos(nodeSeleccionat As String)
        cboDirectoryVB6.Text = nodeSeleccionat.Replace(CARPETA_NET, CARPETA_VB6)
        cboDirectoryNet.Text = nodeSeleccionat.Replace(CARPETA_NET, CARPETA_NET)
        cboDirectoryVB6_BACKUP.Text = nodeSeleccionat.Replace(CARPETA_NET, CARPETA_VB6_BACKUP)
    End Sub

    ''' <summary>
    ''' Retorna la ultima linia del fitxer FITXER_TRACING_MIGRACIO
    ''' </summary>
    ''' <param name="fitxerEstatProcesMigracio"></param>
    ''' <returns></returns>
    Private Function UltimaLiniaFitxerTracing(ByVal fitxerEstatProcesMigracio As String) As String
        Dim linies As List(Of String) = LlegeixFitxer(fitxerEstatProcesMigracio, encoding)

        Return linies(linies.Count - 1)
    End Function

    ''' <summary>
    ''' Llegim del fitxer FITXER_TRACING_MIGRACIO del projecte/carpeta seleccionada, en quin estat del proces de migracio estem
    ''' i actualitza els butons
    ''' </summary>
    Private Sub ActualitzaButonsEstatProcesMigracio()
        lblEtiqueta.Text = "Estat : "

        btnCleanMigrar.Enabled = IIf(String.IsNullOrEmpty(CarpetaDotNET), False, True)

        ' calculem el fitxer de traça del proces de migració del projecte
        Dim fitxerEstatProcesMigracio As String = GetFitxerTracingMigracio()

        ' si no existeix FITXER_TRACING_MIGRACIO
        If (Not File.Exists(fitxerEstatProcesMigracio)) Then
            btnCrearBackups.Enabled = True ''''EsNovaMigracio()

            lblProcessant.Text = "Encara no s'ha iniciat el procés de migració"
        Else
            'recuperem la ultima linia que indica en quin estat està el "procés de migracio" del projecte
            Dim ultimaLinia As String = UltimaLiniaFitxerTracing(fitxerEstatProcesMigracio)

            ' Estat = E_EstatProcesMigracio.Executat_CopiaBackupVB6 > estat intermig
            If EstatProcesMigracio(ultimaLinia) = E_EstatProcesMigracio.Executat_CopiaBackupVB6 Then
                btnCrearBackups.Enabled = EsMigrableNovament()  ' no podem tornar a fer una copia/foto fins que no s'acabi tot el proces de migracio

                lblFitxersModificats.Text = String.Empty

                ' comprovem si hem executat el Wizard2008.Això ho sabem si existeix la corresponent carpeta XXXXXX.NET
                If Not String.IsNullOrEmpty(CarpetaDotNET) Then
                    RegistrarTrazaEstatMigracio(String.Format(FORMAT_FITXER_TRACING_MIGRACIO, Date.Now.ToString("dd/MM/yyyy HH:mm:ss"), E_EstatProcesMigracio.Executat_Wizard2008.ToString, "", "Executat Wizard 2008"))

                    btnPreWizard.Enabled = True
                    btnWizard2008.Enabled = True
                    btnPostMigracio.Enabled = False
                    btnValidacions.Enabled = False
                    btnTestos.Enabled = False

                    RefrescarProces("Migració", "PAS : Executar PreWizard2008 i/o Wizard2008", "", "")
                Else
                    btnWizard2008.Enabled = True
                    RefrescarProces("Migració", "PAS : Executar el project Foto (si cal) i després el Wizard 2008", "", "")
                End If
            End If

            ' Estat = E_EstatProcesMigracio.Executat_Wizard2008 > estat intermig
            If EstatProcesMigracio(ultimaLinia) = E_EstatProcesMigracio.Executat_Wizard2008 Then
                btnCrearBackups.Enabled = EsMigrableNovament()

                lblFitxersModificats.Text = String.Empty

                ' nomes podem fer "postMigracio" sobre els fitxers NET de la carpeta BACKUP
                chkMigrarBackup.Checked = True

                btnPostMigracio.Enabled = True
                btnPreWizard.Enabled = False
                btnWizard2008.Enabled = False
                btnValidacions.Enabled = False
                btnTestos.Enabled = False

                lblProcessant.Text = "PAS : Executar PostMigracio"
            End If

            ' Estat = E_EstatProcesMigracio.Executat_PostMigracio > estat intermig
            If EstatProcesMigracio(ultimaLinia) = E_EstatProcesMigracio.Executat_PostMigracio Then
                btnCrearBackups.Enabled = EsMigrableNovament()

                btnPostMigracio.Enabled = False
                btnWizard2008.Enabled = False
                btnPreWizard.Enabled = False
                btnValidacions.Enabled = True
                btnTestos.Enabled = True

                RefrescarProces("Migració", "Fer la copia dels fitxers .NET corresponents a fitxers VB6 modificats de BACKUP a MP_Net", "", "")
            End If

            ' Estat = E_EstatProcesMigracio.Executat_MigratsFitxers > estat final > cal comprovar si igualment es "novament" "migrable"
            If EstatProcesMigracio(ultimaLinia) = E_EstatProcesMigracio.Executat_MigratsFitxers Then
                chkMigrarBackup.Checked = False

                btnPostMigracio.Enabled = False
                btnValidacions.Enabled = True

                lblProcessant.Text = "Fer les validacions"

                ' podem tornar a crear una nova "migració" ?
                btnCrearBackups.Enabled = EsMigrableNovament()
            End If

            If EstatProcesMigracio(ultimaLinia) = E_EstatProcesMigracio.Executat_Validacio Then

                btnPostMigracio.Enabled = False
                btnWizard2008.Enabled = False
                btnPreWizard.Enabled = False
                btnValidacions.Enabled = True
                btnTestos.Enabled = True
            End If

        End If

    End Sub

    ''' <summary>
    ''' Exemple : CRM\GestioCRM depen dels projectes Media\Publicitat i Media\Subscripcions
    ''' Segons això, si volem començar un "proces" de migració, si executem el Wizard2008 SENSE que hi hagin els fitxers VB6 a BACKUP dels quals depen, llavors
    ''' el Wizard 2008 petarà.En tal cas, caldria avisar que previament es copiin els fitxers VB6 a BACKUP dels quals depen    
    ''' </summary>
    Private Function FitxersQueDepenProjecte() As List(Of String)
        Dim lstFitxersQueDepen As List(Of String) = New List(Of String)()

        Dim fitxerProjecteVB6 As String = BuscarFitxersProjectesVB6(cboDirectoryVB6.Text)
        If String.IsNullOrEmpty(fitxerProjecteVB6) Then Return lstFitxersQueDepen

        GetFormsVB6_Aplicacio(fitxerProjecteVB6, True)

        ' Busquem
        For Each dr As DataRow In dtVB6.Rows
            If dr.Item(0).ToString.Contains("..") Then
                lstFitxersQueDepen.Add(dr.Item(0))
            End If

        Next

        For Each dr As DataRow In dtVB6_2.Rows
            If dr.Item(0).ToString.Contains("..") Then
                lstFitxersQueDepen.Add(dr.Item(0))
            End If
        Next

        Return lstFitxersQueDepen
    End Function

    Private Function ConcatenarLlista(llista As String()) As String
        Dim cadenaConcatenada As String = String.Empty

        For Each l As String In llista
            cadenaConcatenada = cadenaConcatenada & l
        Next

        Return cadenaConcatenada
    End Function

    Structure ColeccioTwipsPixels
        Dim cadena As String
        Dim posicio As Integer
        Dim esColeccio As Boolean

        Public Sub New(_cadena As String, _posicio As String, _esColeccio As Boolean)
            Me.New()
            Me.cadena = _cadena
            Me.posicio = _posicio
            Me.esColeccio = _esColeccio
        End Sub
    End Structure

    ''' <summary>
    ''' Retorna e ............
    ''' </summary>
    Private Function GetIndexosColeccioTwipsPixels(ByVal cadena As String, ByVal resta As Integer) As List(Of ColeccioTwipsPixels)
        Dim splitList() As String = {}
        Dim resultat As New List(Of ColeccioTwipsPixels)
        Dim pos As Integer = 0
        Dim numcoleccions As Integer = 0

        RegexCercaCadena(cadena, "(" & REGEX_COLEC_PROP & ")", splitList)

        For Each s As String In splitList
            Dim splitListIndexos() As String = {}

            RegexCercaCadena(s, "(" & REGEX_COLEC_PROP & ")", splitListIndexos)

            If splitListIndexos.Count = 0 Then pos = pos + s.Length

            For Each s2 As String In splitListIndexos
                If Not String.IsNullOrEmpty(s2) Then
                    resultat.Add(New ColeccioTwipsPixels(s, pos, True))

                    pos = pos + s.Length '+ numcoleccions

                    numcoleccions = numcoleccions + 1
                End If
            Next
        Next

        Return resultat
    End Function

    ''' <summary>
    ''' Retorna e ............
    ''' </summary>
    Private Function GetNumerosTwipsPixels(ByVal cadena As String) As List(Of ColeccioTwipsPixels)
        Dim splitList() As String = {}
        Dim resultat As New List(Of ColeccioTwipsPixels)

        RegexCercaCadena(cadena, "(\b" & REGEX_NUM & "\b)", splitList)

        For Each s As String In splitList
            Dim splitListNumeros() As String = {}

            RegexCercaCadena(s, "(\b" & REGEX_NUM & "\b)", splitListNumeros)

            For Each s2 As String In splitListNumeros
                If Not String.IsNullOrEmpty(s2) Then
                    resultat.Add(New ColeccioTwipsPixels(s, cadena.IndexOf(s), False))
                End If
            Next
        Next

        Return resultat
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <returns></returns>
    Private Function ProcesarTwipsPerPixel(ByVal cadena As String) As String
        Return RegexSubstitucioCadena(cadena, TWIPSPERPIXEL, "1")
    End Function

    ''' <summary>
    ''' Linia amb el texte Width/Top/Left/Right/Amplada/Ample/....
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <returns></returns>
    Private Function ConteMides(ByVal cadena As String) As Boolean
        Dim propietats() As String = {"Right", "Height", "Left", "Top", "Width", "Ample", "Altura", "Alçada", "Amplada", "PosX", "PosY"}

        ' cadena.ToUpper.Contains("X1") OrElse   les coordenades dels Line ja em ve en pixels
        ' cadena.ToUpper.Contains("X2") OrElse   les coordenades dels Line ja em ve en pixels
        ' cadena.ToUpper.Contains("Y1") OrElse   les coordenades dels Line ja em ve en pixels
        ' cadena.ToUpper.Contains("Y2") OrElse   les coordenades dels Line ja em ve en pixels

        Dim tePropietatTamany As Boolean = False
        For Each p As String In propietats
            If cadena.Contains(p & " =") Then tePropietatTamany = True

            tePropietatTamany = tePropietatTamany OrElse RegexCercaCadena(cadena, "(?i)[\w\.\(\)\d]*" & p & REGEX_ESPAI & REGEX_OPERADORS_SENSE_ESPAIS & REGEX_ESPAI & REGEX_NUM)
        Next


        If tePropietatTamany Then

            '? excepcio
            If cadena.ToUpper.Contains("DROPDOWNWIDTH") Then Return False

            '? casos exclosos
            '? --------------------------------------------------------------------
            '!      1.- GmsCombo(Combobox)
            If EsTamanyGmsCombo(cadena) Then Return False

                '!      2.- Grid
                If EsTamanyGrid(cadena) Then Return False

                '!      3.-
                If RegexCercaCadena(cadena, "\.DrawWidth" & REGEX_ESPAI & REGEX_OPERADORS_SENSE_ESPAIS & REGEX_ESPAI & REGEX_NUM) Then Return False

                '? --------------------------------------------------------------------

                Debug.WriteLine(String.Format("MIDES : {0}", cadena))

                '? casos inclosos
                '? --------------
                '!      1.- posLeft/posTop  , If PosicioTop < 0 Then PosicioTop = 50
                '!      2.- Me.Top/Me.Left/Me.Width/Me.Height , If Me.Height < 287 Then Me.Height = 287  , If Me.Width < 600 Then Me.Width = 600
                'x      3.- Printer.DrawWidth = 2 , .DrawWidth = 4
                '!      4.- cmdImprimir.Top = 7
                '!      5.- ChartControl.Top = 45
                '!      6.- WebBrowser1(I).Left = 0
                '!      7.- if GridWidth < 1 Then
                '!      8.- Image1.Left = 0
                '!      9.- calAgenda.Left = 0
                Return True

            Else
                Return False
        End If
    End Function

    ''' <summary>
    ''' Linies que contenen numeros que no pertanyen ni a GetTwipsPixels ni GetpixelsTwips
    ''' Exemples : 
    '''             GetTwipsPixels(6980)   --> False
    '''             If GetPixelsToTwipsX(FRM.Width) > (Amplada - 200 - WPic) Then ----> True
    ''' </summary>
    ''' <returns></returns>
    Private Function ConteNumerosForaGets(ByVal cadena As String, ByRef splitListNumeros As String()) As Boolean
        Return RegexCercaCadena(cadena, "(\()(" & REGEX_NUM & ")(\s)|(\s)(" & REGEX_NUM & ")(\s)|(\s)(" & REGEX_NUM & "$)|(\s)(" & REGEX_NUM & ")(\))|(\()(" & REGEX_NUM & "$)", splitListNumeros)
    End Function

    ''' <summary>
    ''' Linia amb el texte Width/Top/Left/Right/Amplada/Ample/.... i que conté numeros "aillats"
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <param name="splitListNumeros"></param>
    ''' <returns></returns>
    Private Function ConteLiniaNumeroIMides(ByVal cadena As String, ByRef splitListNumeros As String()) As Boolean
        If ConteMides(cadena) Then

            Return ConteNumerosForaGets(cadena, splitListNumeros)
        End If

        Return False
    End Function

    Private Function Excloure(ByVal cadena As String) As Boolean

        ' El Wizard2008 respecte les linies (?i)Me\.Line\w+\.\w+ = nnnn , ja genera el nnnn en pixels, per tant no cal migrar


        If RegexCercaCadena(cadena, REGEX_TOTS_ALFANUM & " = 0") OrElse
                        RegexCercaCadena(cadena, "Val\(Piece\(\w+, \w+, \d+\)\)") OrElse
                        RegexCercaCadena(cadena, "= Piece\(\w+, \w+, \d+\)") OrElse
                        RegexCercaCadena(cadena, "^\s*'") OrElse
                        RegexCercaCadena(cadena, "(?i)\w+\._*Line_*\w+\.\w+ =") OrElse
                        RegexCercaCadena(cadena, "Column\(\w+\)") Then

            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="nomControl"></param>
    ''' <returns></returns>
    Private Function GetTipusControl(controlsFormulari As List(Of ControlNET), nomControl As String)
        Return controlsFormulari.Where(Function(s) s.nomControl = nomControl).FirstOrDefault.tipusControl
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxerCodiNet"></param>
    ''' <param name="nomControl"></param>
    ''' <returns></returns>
    Private Function GetIndexMenorElementsControlArray(fitxerCodiNet As String, nomControl As String) As Integer
        Dim controlsFormulari As List(Of ControlNET) = ObtenirControlsByDesigner(GetFitxerDesignerByVB(cboDirectoryNet.Text & fitxerCodiNet))

        Dim tipusControl As String = GetTipusControl(controlsFormulari, nomControl)

        '! si no es un controlArray (el nom , el nom del tipusControl d'un controlArray acaba en *****Array!)
        If tipusControl Is Nothing OrElse Not tipusControl.EndsWith("Array") Then Return Nothing

        '! obtenimr els elements del controlArray
        Dim elements As List(Of ControlNET) = controlsFormulari.Where(Function(s) s.nomControl.Contains(UNDERSCORE & nomControl & UNDERSCORE)).ToList

        '? calculem l'index menor de tots els elements del controlArray
        Dim indexMenor As Integer = 999999
        Dim splitlist2() As String = {}
        For Each e As ControlNET In elements
            '! calculem l'index de l'element
            RegexCercaCadena(e.nomControl, "_\w+_(\d+)", splitlist2)

            If CInt(splitlist2(1)) < indexMenor Then indexMenor = CInt(splitlist2(1))
        Next

        Return indexMenor
    End Function

    ''' <summary>
    ''' Excepcio Twips/Pixels : Els tamanys dels comboBox de grid
    ''' </summary>
    ''' <returns></returns>
    Private Function EsTamanyGmsCombo(ByVal cadena As String) As Boolean
        Dim resultat As Boolean = False

        '! Nota: la Columns(i) NOMES té la propietat Width
        resultat = RegexCercaCadena(cadena, "(?i)cmb\w+\.Columns\(\w+\)\." & PROPIETAT_WIDTH & REGEX_ESPAI & REGEX_OPERADORS_SENSE_ESPAIS & REGEX_ESPAI & REGEX_NUM)

        Return resultat
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <returns></returns>
    Private Function ProcessaTwipsAxListViewColumnHeaders(ByVal cadena As String) As String
        '? dimarts 14/12
        ''''''''Dim patro As String = "^\s*\.Add\( , \""\w+\"", \""\w+\"", (\d+), " & REGEX_ALFANUM_DOT & "\)"
        Dim patro As String = "\w+\.ColumnHeaders\.Add\(\s*,\s*, \""[\w\s]+\"", (\d+),*[\s\w\.]*\)"

        If Not RegexCercaCadena(cadena, patro) Then Return cadena

        Dim num As String = RegexSubstitucioCadena(cadena, patro, "$1").Trim

        Dim nouNum As String = (CInt(num) \ 15).ToString
        Return cadena.Replace(num, nouNum)
    End Function

    ''' <summary>
    ''' Excepcio Twips/Pixels : Els tamanys d'un FlexCellGrid (Width,Left,....) ja m'arriben en pixels
    ''' </summary>
    ''' <returns></returns>
    Private Function EsTamanyGrid(ByVal cadena As String) As Boolean
        '! Nota: la Column NOMES té la propietat Width
        Dim resultatColumn As Boolean = RegexCercaCadena(cadena, "(?i)\.Column\(\w+\)\." & PROPIETAT_WIDTH & REGEX_ESPAI & REGEX_OPERADORS_SENSE_ESPAIS & REGEX_ESPAI & REGEX_NUM)

        '! Nota: la Row NOMES té la propietat Height
        Dim resultatRow As Boolean = RegexCercaCadena(cadena, "(?i)\.Row\(" & REGEX_TOTS_ALFANUM_SENSEPARENTESIS & "\)\." & PROPIETAT_HEIGHT & REGEX_ESPAI & REGEX_OPERADORS_SENSE_ESPAIS & REGEX_ESPAI & REGEX_NUM)

        Return resultatColumn OrElse resultatRow
    End Function

    ''' <summary>
    ''' Hi ha casos on el Wizard2008 no sap que un número correspon a un twip.
    ''' Exemple : If ample > 9650 Then
    ''' Cal definir un manera per poder automatizar la conversio (/15) de twip a pixel
    ''' </summary>
    ''' <returns></returns>
    Private Function ProcesarMidesAmbNumeros(ByVal cadena As String, ByVal fitxer As String, ByVal numLinia As Integer) As String
        '! si ja ho hem tractat en un anterior procés de migració, no ho tractem
        If cadena.ToUpper.Contains("MIGRATPIXELS") Then Return cadena

        '
        If EsTamanyGrid(cadena) Then Return cadena

        '! si no conté "cadenes" de mides (width/right/....), no cal tractar
        If Not ConteMides(cadena) Then Return cadena

        Dim splitList() As String = {}

        RegexCercaCadena(cadena, "(" & REGEX_NUM & ")", splitList)

        Dim trobat As Boolean = False
        Dim splitListNumeros() As String = {}

        For Each s3 As String In splitList
            splitListNumeros = {}

            ProcesaNumeros(s3, splitListNumeros)

            If splitListNumeros.Count > 0 Then
                splitList(splitList.ToList.IndexOf(s3)) = ConcatenarLlista(splitListNumeros)

                trobat = True
            End If
        Next

        Dim liniaConvertida As String
        If Not trobat AndAlso splitList.Count = 0 Then
            ProcesaNumeros(cadena, splitListNumeros)

            If splitListNumeros.Count = 0 Then
                Return cadena
            Else
                liniaConvertida = ConcatenarLlista(splitListNumeros)
            End If

            If Excloure(cadena) Then Return cadena

            If (chkTwipsPixelsDuplicades.Checked AndAlso Not EsLiniaTwipsPixelsDuplicada(cadena)) OrElse Not chkTwipsPixelsDuplicades.Checked Then
                Dim lvi As New ListViewItem()

                lvi.SubItems.Add(cadena)
                lvi.SubItems.Add(liniaConvertida & LINIA_MIGRADA_TWIPSPIXELS)   ' calcula la linia de substitucio
                lvi.SubItems.Add(8888)
                lvi.SubItems.Add(fitxer)
                lvi.SubItems.Add(numLinia)
                lvi.BackColor = Color.Orange

                ' només visualitzem si 
                If seleccionarTwipsPixelsNomesNumeros OrElse seleccionarTwipsPixelsTots Then
                    ' si no conté .....
                    If Not liniaConvertida.ToUpper.Contains("TWIPSTOPIXELS") AndAlso Not liniaConvertida.ToUpper.Contains("PIXELSTOTWIPS") Then
                        lvTwipsPixels.Items.Add(lvi)
                    End If
                End If

            End If
        Else
            liniaConvertida = ConcatenarLlista(splitList)
        End If

        Return liniaConvertida
    End Function

    ''' <summary>
    ''' Processa la migració d'una cadena que té PixelsToTwips
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <returns></returns>
    Private Function ProcesarPixelsToTwips(ByVal cadena As String) As String
        Dim splitListPixelsToTwips() As String = {}

        Dim rx As Regex = New Regex(GETPIXELSTOTWIPS)

        'If rx.Matches(cadena).Count = 0 Then
        RegexCercaCadena(cadena, "(" & REGEX_GPT & "\(" & REGEX_TOTS_ALFANUM & "\))", splitListPixelsToTwips)
        'Else
        'RegexCercaCadena(cadena, "(" & REGEX_GPT & "\(" & REGEX_TOTS_ALFANUM_SENSEPARENTESIS & "\))", splitListPixelsToTwips)
        'End If

        cadena = RegexSubstitucioCadena(cadena, REGEX_GPT & "\(([a-zA-ZçÑ_\t\.\d\+\-\*\\""\/|\,\&\(\)]+)\)", "$1")

        For Each s As String In splitListPixelsToTwips
            Dim splitListPixelsToTwipsAmbColeccions() As String = {}

            If s.Contains(GETPIXELSTOTWIPS) Then

                '! comprovem siel contingut del GetTwipsToPixels té parentesis
                Dim contingut As String = RegexSubstitucioCadena(s, REGEX_GPT & "\((" & REGEX_TOTS_ALFANUM & ")\)", "$1")

                If contingut.Contains("(") OrElse contingut.Contains(")") Then

                    RegexCercaCadena(s, "(" & REGEX_GPT & "\(" & REGEX_TOTS_ALFANUM & "\))", splitListPixelsToTwipsAmbColeccions)

                    For Each s2 As String In splitListPixelsToTwipsAmbColeccions

                        If s2.Contains(GETPIXELSTOTWIPS) Then
                            '! treiem el GetTwipsToPixels
                            splitListPixelsToTwipsAmbColeccions(splitListPixelsToTwipsAmbColeccions.ToList.IndexOf(s2)) = contingut
                            s2 = contingut
                        End If

                        '! comprovem si conte numeros
                        If RegexCercaCadena(s2, REGEX_CONTE_NUMS) Then
                            Dim splitListNumeros() As String = {}

                            ProcesaNumeros(s2, splitListNumeros)

                            '! modifiquem la llista en la posicio s2
                            splitListPixelsToTwipsAmbColeccions(splitListPixelsToTwipsAmbColeccions.ToList.IndexOf(s2)) = ConcatenarLlista(splitListNumeros)

                        Else
                            '! modifiquem la llista
                            splitListPixelsToTwipsAmbColeccions(splitListPixelsToTwipsAmbColeccions.ToList.IndexOf(s2)) = ProcesarFragmentPixelsToTwips(s2, 1)
                        End If

                    Next

                    '! modifiquem la llista en la posicio s
                    splitListPixelsToTwips(splitListPixelsToTwips.ToList.IndexOf(s)) = ConcatenarLlista(splitListPixelsToTwipsAmbColeccions)

                Else

                    ProcesaNumeros(s, splitListPixelsToTwipsAmbColeccions)

                    splitListPixelsToTwips(splitListPixelsToTwips.ToList.IndexOf(s)) = ProcesarFragmentPixelsToTwips(s, 1)
                End If

            Else  '! no conté getPixelsToTwips
                Dim contingutTwipsToPixels As String = RegexSubstring(s, REGEX_GTP & "\((" & REGEX_TOTS_ALFANUM & ")\)")

                '! si tampoc conté GetTwipsToPixels, qualsevol numero que hi hagi (si >=15) correspon a un numero en pixel i per tant cal convertir
                If contingutTwipsToPixels Is Nothing OrElse Not contingutTwipsToPixels.Contains(s) Then  ' hem de comporova si s pertany a GetTwipsToPixels. Si pertany no hem de processarNumeros.Si 

                    If s.ToUpper.Contains("PIXELSTOTWIPS") Then
                        ProcesaNumeros(s, splitListPixelsToTwipsAmbColeccions)   '? revisar!!!!!!!!
                    End If

                End If

                If splitListPixelsToTwipsAmbColeccions.Count = 0 Then
                    splitListPixelsToTwips(splitListPixelsToTwips.ToList.IndexOf(s)) = ProcesarFragmentPixelsToTwips(s, 1)
                Else
                    splitListPixelsToTwips(splitListPixelsToTwips.ToList.IndexOf(s)) = ConcatenarLlista(splitListPixelsToTwipsAmbColeccions)
                End If

            End If

        Next

        '! resultat final després de calcular la conversio de Pixels a Twips
        If splitListPixelsToTwips.Count > 0 Then
            cadena = ConcatenarLlista(splitListPixelsToTwips)
        End If

        Return cadena
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cadena"></param>
    Private Function ProcesarFragmentPixelsToTwips(ByVal cadena As String, ByVal resta As Integer) As String
        Dim splitList() As String = {}

        Dim posGetPixelsToTwips As Integer = cadena.IndexOf(GETPIXELSTOTWIPS)
        Dim longitudGetPixelsToTwips As Integer = GETPIXELSTOTWIPS.Length + 1  ' hem de sumar 1 per la lletra X ó Y

        Dim contingut As String = RegexSubstitucioCadena(cadena, REGEX_GPT & "\((" & REGEX_TOTS_ALFANUM & ")\)", "$1")

        '! si el contingut del GetTwipsToPixels no té coleccions (parentesis) return el seu contingut, sino cal un tractament diferent
        ''''If Not contingut.Contains("(") AndAlso Not contingut.Contains(")") Then
        Return contingut
        ''''End If

        '! 1. Busquem coleccions
        Dim coleccions As List(Of ColeccioTwipsPixels) = GetIndexosColeccioTwipsPixels(cadena, resta)

        '! 2. Treiem (temporalment) les coleccions de la cadena
        '! Nota : ho treiem per "treure" els ) i els (
        For Each c As ColeccioTwipsPixels In coleccions
            cadena = cadena.Replace(c.cadena, "")
        Next

        '! 3. Eliminem els PixelsToTwips
        Dim reglaRegex_SenseColeccions As String = REGEX_GPT & "\((" & REGEX_TOTS_ALFANUM_SENSEPARENTESIS & ")\)|" & REGEX_GPT & "\(\)"    ' \(\)
        cadena = RegexSubstitucioCadena(cadena, reglaRegex_SenseColeccions, "$1")

        Dim numColeccionsAnteriorsAGetPixelsToTwips As Integer = 0

        '! 4.- Tornem a posar les coleccions
        For Each c In coleccions
            If c.posicio > posGetPixelsToTwips Then
                numColeccionsAnteriorsAGetPixelsToTwips = numColeccionsAnteriorsAGetPixelsToTwips + 1

                Dim novaPosicioColeccio As Integer = c.posicio - longitudGetPixelsToTwips - numColeccionsAnteriorsAGetPixelsToTwips   ' restem el REGEX
                If novaPosicioColeccio < cadena.Length Then
                    cadena = cadena.Insert(novaPosicioColeccio, c.cadena)
                Else
                    '? revisar
                    Dim ko As String = "KO"
                End If
            Else
                cadena = cadena.Insert(c.posicio, c.cadena)
            End If
        Next

        'Debug.Print(String.Format("Cadena sense PixelsToTwips :  {0}", cadena))
        Return cadena
    End Function

    ''' <summary>
    ''' Processa la migració d'una cadena que té TwipsToPixels
    ''' </summary>
    ''' <param name="cadena"></param>
    Private Function ProcesarTwipsToPixels(ByVal cadena As String) As String
        Dim cadenaSubstituida As String = String.Empty


        '! DESCARTEM ELS QUE NO CONTENEN NUMEROS
        If Not RegexCercaCadena(cadena, REGEX_CONTE_NUMS) Then
            '! ELIMINEM LA PART DEL GETTWIPSTOPIXELS
            Return RegexSubstitucioCadena(cadena, REGEX_GTP & "\((" & REGEX_TOTS_ALFANUM & ")\)", "$1")
        End If

        '! comprovem si la cadena comenca en GetTwipsToPixels
        If RegexCercaCadena(cadena, "^" & REGEX_GTP) Then

            '! tot el que hi ha dins del GetTwipsToPixels
            Dim contingut As String = RegexSubstitucioCadena(cadena, REGEX_GTP & "\((" & REGEX_TOTS_ALFANUM & ")\)", "$1")

            '! totes les coleccions dins el contingut
            Dim coleccions As List(Of ColeccioTwipsPixels) = GetIndexosColeccioTwipsPixels(contingut, 0)

            '! tots el numeros dins el contingut
            Dim numeros As List(Of ColeccioTwipsPixels) = GetNumerosTwipsPixels(contingut)

            If numeros.Count = 0 Then Return contingut

            '! NO hem de dividir per 15 els numeros que pertanyen a alguna de les coleccions
            For Each n As ColeccioTwipsPixels In numeros
                Dim esColeccio As Boolean = False
                For Each c As ColeccioTwipsPixels In coleccions
                    If n.posicio > c.posicio AndAlso n.posicio < c.posicio + c.cadena.Length Then
                        If Not cadenaSubstituida.Contains(c.cadena) Then
                            cadenaSubstituida = cadenaSubstituida & c.cadena
                        End If

                        '! pertany a alguna coleccio
                        esColeccio = True

                        '! modifiquem que pertany a una coleccio
                        n.esColeccio = True
                        Exit For
                    End If

                    If esColeccio Then Exit For
                Next

                If Not esColeccio Then
                    '? IMPORTANT : d'alguna manera ha de saber si els numeros que em vénen s'ha de dividir (convertir aa pixels) per 15.
                    '? EXEMPLE : (9 * 255 * 4)
                    '? El criteri per saber si el numero correspon a un Twip és : si el numero es >=15
                    Dim t As Integer
                    If Integer.TryParse(n.cadena, t) AndAlso CInt(n.cadena) < 15 Then
                        ' no representa un Twip

                    Else
                        '! condicio : només considerem que es un twips si el valor és >=15 O BÉ es negatiu (exemple : -450)
                        If Integer.TryParse(n.cadena, t) AndAlso (CInt(n.cadena) >= 15 OrElse CInt(n.cadena) < 0) Then
                            contingut = contingut.Replace(n.cadena, CStr(Math.Round(CInt(n.cadena) / 15)))
                        End If
                    End If

                End If
            Next

            '! la cadena final
            cadenaSubstituida = contingut

        Else '! la cadena NO comença amb REGEX_GTP
            Dim splitListSeparatPerTwipsToPixels() As String = {}

            Dim indexSeparatEnTwipsToPixels As Boolean = RegexCercaCadena(cadena, "(" & REGEX_GTP & "\(" & REGEX_TOTS_ALFANUM & "\))", splitListSeparatPerTwipsToPixels)

            If indexSeparatEnTwipsToPixels Then

                For Each kk In splitListSeparatPerTwipsToPixels
                    If kk.Contains(GETTWIPSTOPIXELS) Then
                        cadenaSubstituida = cadenaSubstituida & ProcesarTwipsToPixels(kk)
                    Else
                        cadenaSubstituida = cadenaSubstituida & kk
                    End If
                Next
            Else
                Dim splitListCadenes() As String = {}

                Dim cadenes As Boolean = RegexCercaCadena(cadena, "(\w+)", splitListCadenes)

                If cadenes Then

                    For Each c As String In splitListCadenes
                        If IsNumeric(c) Then
                            cadenaSubstituida = cadenaSubstituida & CStr(Math.Round(CInt(c) / 15))   ' calculem a pixels
                        Else
                            cadenaSubstituida = cadenaSubstituida & c
                        End If
                    Next

                End If
            End If

        End If

        Return cadenaSubstituida
    End Function

    ''' <summary>
    ''' processa els numeros
    ''' CAS 1 :  grdDades.Width = VB6.TwipsToPixelsX((AmpleTotal * 18))    -->  grdDades.Width = (AmpleTotal * 1.2)
    ''' CAS 2 : 
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <param name="splitListNumeros"></param>
    Private Function ProcesaNumeros(ByVal cadena As String, ByRef splitListNumeros() As String) As String
        ''''Dim patroNumeros As String = "(\()(" & "(" & REGEX_MULTDIV & ")" & "(" & REGEX_ESPAI & ")" & REGEX_NUM & ")(" & REGEX_ESPAI & ")" & REGEX_OR & "(" & REGEX_ESPAI & ")" & "(" & REGEX_MULTDIV & ")" & "(" & REGEX_ESPAI & ")(" & REGEX_NUM & ")(" & REGEX_ESPAI & ")" & REGEX_OR & "(" & REGEX_ESPAI & ")" & "(" & REGEX_MULTDIV & ")" & "(" & REGEX_ESPAI & ")(" & REGEX_NUM & "$)" & REGEX_OR & "(" & REGEX_ESPAI & ")" & "(" & REGEX_MULTDIV & ")" & "(" & REGEX_ESPAI & ")(" & REGEX_NUM & ")(\))" & REGEX_OR & "(\()" & "(" & REGEX_MULTDIV & ")" & "(" & REGEX_ESPAI & ")(" & REGEX_NUM & "$)" & REGEX_OR & "(" & REGEX_ESPAI & ")" & "(" & REGEX_MULTDIV & ")" & "(" & REGEX_ESPAI & ")(" & REGEX_NUM & ")(,)" & "" & REGEX_OR & "^" & "(" & REGEX_MULTDIV & ")" & "(" & REGEX_ESPAI & ")(" & REGEX_NUM & ")(" & REGEX_ESPAI & ")" & REGEX_OR & "^" & "(" & REGEX_MULTDIV & ")" & "(" & REGEX_ESPAI & ")(" & REGEX_NUM & "$)"
        Dim patroNumeros As String = "(\([\*\\\/]*\s*[\+\-]*\d+\.*\d*)(\s*)|(\s*[\*\\\/]*\s*[\+\-]*\d+\.*\d*\s*)|(\s*[\*\\\/]*\s*[\+\-]*\d+\.*\d*$)|(\s*[\*\\\/]*\s*[\+\-]*\d+\.*\d*\))|(\([\*\\\/]*\s*[\+\-]*\d+\.*\d*$)|(\s*[\*\\\/]*\s*[\+\-]*\d+\.*\d*,)|^([\*\\\/]*\s*[\+\-]*\d+\.*\d*\s*)|^([\*\\\/]*\s*[\+\-]*\d+\.*\d*$)"

        Dim teNum As Boolean = RegexCercaCadena(cadena, patroNumeros, splitListNumeros)

        If Not teNum Then Return cadena

        Dim valorAnterior As String = String.Empty

        For Each s3 As String In splitListNumeros
            Dim num As Integer
            Dim numD As Double


            If valorAnterior.Trim = "*" OrElse valorAnterior.Trim = "\" OrElse valorAnterior.Trim = "/" Then  '! multiplicacio o divisio
                If Double.TryParse(s3.Replace(".", ",").Trim, num) AndAlso Double.Parse(s3.Replace(".", ",")) >= 15 Then   '! si és negatiu (exemple : -1) no ho hem de dividir
                    numD = Math.Round(num / 15, 2)

                    splitListNumeros(splitListNumeros.ToList.IndexOf(s3)) = numD.ToString.Replace(",", ".")  '! hem de substituir la , pel .
                End If

            ElseIf Integer.TryParse(s3.Trim, num) Then
                '! condicio : només considerem que es un twips si el valor és >=15 O BÉ es negatiu (exemple : -450)
                If CInt(num) >= 15 OrElse CInt(num) < 0 Then
                    num = num / 15

                    splitListNumeros(splitListNumeros.ToList.IndexOf(s3)) = num
                End If
            End If

            '! descartem els espais en blanc
            If Not String.IsNullOrEmpty(s3.Trim) Then valorAnterior = s3
        Next

        Return ConcatenarLlista(splitListNumeros)
    End Function

    ''' <summary>
    ''' Retorna la linia substituida si conté GETTWIPSTOPIXELS i/o GETPIXELSTOTWIPS
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <returns></returns>
    Private Function SubstituirTwipsPixels(ByVal cadena As String, ByVal fitxer As String, ByVal numLinia As Integer) As String
        '! abans de tot substituim el VB6.XXXXXXXX pels corresponents GET
        cadena = SubtituirVBTwipsToGets(cadena)

        '! diferents casos en els quals no hi ha ni GETTWIPSTOPIXELS ni GETPIXELSTOTWIPS
        cadena = ProcessaTWIPSPIXELS_CASOS(cadena)

        '! casos especials de GETPIXELSTOTWIPS
        cadena = ProcessaGETPIXELSTOTWIPS_CASOS(cadena)

        '! treiem tots els GetTwipsToPixels
        cadena = ProcessaGETPIXELSTOTWIPS(cadena)

        cadena = ProcesarMidesAmbNumeros(cadena, fitxer, numLinia)

        '! processa els GETTWIPSTOPIXELS
        cadena = ProcessaGETTWIPSTOPIXELS(cadena)


        '! comprovem si la cadena cal calcular la conversió Twips-Pixels
        If Not cadena.Contains(GETTWIPSTOPIXELS) AndAlso Not cadena.Contains(GETPIXELSTOTWIPS) _
            AndAlso Not cadena.Contains(TWIPSPERPIXEL) Then Return cadena

        '! inicialitzem el comptador de linies que contenen GetTwipsPixels i/o GetPixelsToTwips
        comptadorLiniesTwipsPixels = comptadorLiniesTwipsPixels + 1

        '! 0.- processem la TwipsPerPixel
        cadena = ProcesarTwipsPerPixel(cadena)

        '! 1.- processem PixelsToTwips
        cadena = ProcesarPixelsToTwips(cadena)


        '! 2.- processem TwipsToPixels
        '! fem split sobre REGEX_GTP
        Dim splitListTwipsToPixels() As String = {}
        RegexCercaCadena(cadena, "(" & REGEX_GTP & "\(" & REGEX_TOTS_ALFANUM & "\))", splitListTwipsToPixels)

        If splitListTwipsToPixels.Count = 0 Then Return cadena

        For Each s As String In splitListTwipsToPixels
            If s.Contains(GETTWIPSTOPIXELS) Then
                splitListTwipsToPixels(splitListTwipsToPixels.ToList.IndexOf(s)) = ProcesarTwipsToPixels(s)
            End If
        Next

        Return ConcatenarLlista(splitListTwipsToPixels)
    End Function

    ''' <summary>
    ''' "Calculem" les diferencies entre VB6 i NET (pot ser comparant fitxers dins projectes VB6/NET o fitxers dins carpetes)
    ''' </summary>
    Private Sub CalculaDiferenciesVB6_NET(ByVal buscarFitxersProjecte As Boolean)
        btnValidacions.Enabled = False

        If TreeViewNET.SelectedNode Is Nothing Then Exit Sub
        If TreeViewNET.SelectedNode.FullPath = CARPETA_NET Then Exit Sub

        ' Desactivem
        RemoveHandler cboDirectoryVB6.TextChanged, AddressOf CboDirectory_TextChanged
        ActualitzaCombos(TreeViewNET.SelectedNode.FullPath)
        AddHandler cboDirectoryVB6.TextChanged, AddressOf CboDirectory_TextChanged

        NetejarTot()

        '
        BuscarItemsVB6_NET(buscarFitxersProjecte, False, False, TreeViewNET.SelectedNode.FullPath)

        '! carreguem els datatables dins els listviews
        numCoincidenciesVB6 = CarregarListView(dtVB6, lstItemsFoundVB6, "")
        numCoincidenciesVB6_2 = CarregarListView(dtVB6_2, lstItemsFoundVB6_2, "")
        numCoincidenciesNet = CarregarListView(dtNET, lstItemsFoundNet, "")
        numCoincidenciesNet_2 = CarregarListView(dtNET_2, lstItemsFoundNet_2, "")

        '
        DiferenciesItemsVB6_Net()

        '
        ActualitzaColorNode(TreeViewNET.SelectedNode)
    End Sub

    ''' <summary>
    ''' Comprova si la linia (instruccio) es la declaracio d'un ControlArray
    ''' o es la declaració d'un "item" de ControlArray
    ''' </summary>
    Private Sub GetControlArray(ByVal linia As String)
        Dim lstResultat() As String = {}
        Dim lstResultat2() As String = {}


        '! 1.- "items" de "controlsArray"
        If RegexCercaCadena(linia, "Public WithEvents (_\w+_" & REGEX_NUM & ") As (([\w+-\.])*)", lstResultat2) Then

            Dim itemControlArray As New ControlArray()
            itemControlArray.nomControl = lstResultat2(1)   ' el primer element es el nom de l'item
            If lstResultat2(2) = "LineShapeArray" Then
                itemControlArray.tipusControl = "LineShape"   ' el segon element es el tipus de l'item
            ElseIf lstResultat2(2) = "AxXtremeSuiteControls.AxTabControlPage" Then
                itemControlArray.tipusControl = "GmsTabControl.GmsTabPage"   ' el segon element es el tipus de l'item
            ElseIf lstResultat2(2) = "GmsTabControl.GmsTabPage" Then
                itemControlArray.tipusControl = "GmsTabControl.GmsTabPage"   ' el segon element es el tipus de l'item
            ElseIf lstResultat2(2) = "AxXtremeSuiteControls.AxGroupBox" Then    ' WORKAROUND : si modifiquem la propietat Text en runtime de labels/NET que estan dins de AxGroupBox, aquesta propietat no es modifica
                itemControlArray.tipusControl = "DataControl.GmsGroupBox"   ' el segon element es el tipus de l'item
            Else
                itemControlArray.tipusControl = lstResultat2(2)   ' el segon element es el tipus de l'item
            End If

            ''''itemControlArray.lstItemsControlArray = New List(Of Object)

            lstItemControlArray.Add(itemControlArray)

            '! 2.- controlArray
        ElseIf RegexCercaCadena(linia, "Public WithEvents (\w+) As ([\w\.]*\w+Array)", lstResultat) Then

            Dim controlArray As New ControlArray()
            controlArray.nomControl = lstResultat(1)   ' el primer element es el nom de l'item

            '! si ja l'hem tractat surtim
            If dicControlArray.ContainsKey(controlArray.nomControl) Then Exit Sub

            If lstResultat(2) = "LineShapeArray" Then
                controlArray.tipusControl = "GmsLineShapeArray"   ' el segon element es el tipus de l'item
            ElseIf lstResultat(2) = "AxTabControlPageArray" Then
                controlArray.tipusControl = "GmsTabPageArray"   ' el segon element es el tipus de l'item
            ElseIf lstResultat(2) = "AxGroupBoxArray" Then    ' WORKAROUND : si modifiquem la propietat Text en runtime de labels/NET que estan dins de AxGroupBox, aquesta propietat no es modifica
                controlArray.tipusControl = "GmsGroupBoxArray"   ' el segon element es el tipus de l'item
            Else
                controlArray.tipusControl = lstResultat(2)   ' el segon element es el tipus de l'item
            End If


            '! 3. Construim el diccionari de controlArray + els seus items
            For Each itemControlArray As ControlArray In lstItemControlArray
                Dim splitlist() As String = {}

                RegexCercaCadena(itemControlArray.nomControl, "(?i)_(\w+)_\d", splitlist)

                Dim nomControlArray As String = splitlist(1)

                '
                If nomControlArray <> controlArray.nomControl Then Continue For

                '! busquem dins el diccionari
                If dicControlArray.ContainsKey(nomControlArray) Then
                    '! posem el valor del tipus de control del controlArray
                    itemControlArray.tipusControlArray = controlArray.tipusControl

                    '! afegim a la llista d'items del controlArray
                    dicControlArray.Item(nomControlArray).Add(itemControlArray)
                Else
                    dicControlArray.Add(nomControlArray, New List(Of ControlArray))

                    '! posem el valor del tipus de control del controlArray
                    itemControlArray.tipusControlArray = controlArray.tipusControl

                    dicControlArray.Item(nomControlArray).Add(itemControlArray)
                End If

            Next

        End If

    End Sub


    ''' <summary>
    ''' retorna el nom d'un controlArray en NET a partir del nom del controlArray en VB6
    ''' Utilitza les regles que hi ha al fitxer substitucions.txt
    ''' </summary>
    ''' <param name="nomControlArrayVB6"></param>
    ''' <returns></returns>
    Private Function NomControlArrayNET(ByVal nomControlArrayVB6 As String) As String

        '! controlArray VB6 (classes VB6)
        Dim resultat As String = BuscarISubstituir(nomControlArrayVB6, "(?i)Microsoft\.VisualBasic\.Compatibility\.VB6\.(\w+)", "Gms$1")
        If Not String.IsNullOrEmpty(resultat) Then Return resultat


        resultat = BuscarISubstituir(nomControlArrayVB6, "(?i)AxFlexCell.AxGrid", TIPUSCONTROL_FCGRID)
        If Not String.IsNullOrEmpty(resultat) Then Return resultat

        '
        resultat = BuscarISubstituir(nomControlArrayVB6, "(?i)AxSSDBComboArray", "GmsComboArray")
        If Not String.IsNullOrEmpty(resultat) Then Return resultat

        '
        resultat = BuscarISubstituir(nomControlArrayVB6, "(?i)AxFlexCell.AxGrid", TIPUSCONTROL_FCGRID)
        If Not String.IsNullOrEmpty(resultat) Then Return resultat

        '
        resultat = BuscarISubstituir(nomControlArrayVB6, "(?i)AxgmsTempsArray", "GmsTempsArray")
        If Not String.IsNullOrEmpty(resultat) Then Return resultat

        '! controlArray de controls DataControl
        resultat = BuscarISubstituir(nomControlArrayVB6, "(?i)AxGmsDataArray", "GmsDataArray")
        If Not String.IsNullOrEmpty(resultat) Then Return resultat

        resultat = BuscarISubstituir(nomControlArrayVB6, "(?i)AxGmsImportsArray", "GmsImportsArray")
        If Not String.IsNullOrEmpty(resultat) Then Return resultat

        '! controlArray de controls Codejoc
        resultat = BuscarISubstituir(nomControlArrayVB6, "(?i)(Ax\w+Array)", "Gms$1")
        If Not String.IsNullOrEmpty(resultat) Then Return resultat


        resultat = BuscarISubstituir(nomControlArrayVB6, "(?i)AxXtremeSuiteControls.TabControlPage", "GmsTabPageArray")
        If Not String.IsNullOrEmpty(resultat) Then Return resultat


        ' !la resta de controlArray
        resultat = BuscarISubstituir(nomControlArrayVB6, "(?i)[Ax]*(\w+Array\w*)", "$1")
        If Not String.IsNullOrEmpty(resultat) Then Return resultat

    End Function

    ''' <summary>
    ''' Retorna una llista de les instruccions d'inicialitzacio dels
    ''' ControlArrayS amb els seus "items"
    ''' </summary>
    Private Function LlistaInicialitzacionsControlArray() As List(Of String)
        If dicControlArray.Count = 0 Then Return New List(Of String)

        Dim lstInicialitzacio As New List(Of String)

        ' construim la/s "instrucció"/ns d'inicialització dels controlArrayS que hem d'afegir al fitxer Designer (just després del InitializeComponent)

        Dim nomArray As String = String.Empty
        Dim tipusArray As String = String.Empty
        Dim llistaItems As String = String.Empty
        Dim tipusItemArray As String = String.Empty

        '! per cada controlArray ( n'hi pot haber +1 en un formulari!!)
        For Each keysControlArray In dicControlArray.Keys
            If keysControlArray.Count > 0 Then
                llistaItems = String.Empty

                For Each itemControlArrayByControlArray As ControlArray In dicControlArray.Item(keysControlArray)
                    nomArray = keysControlArray
                    tipusArray = NomControlArrayNET(itemControlArrayByControlArray.tipusControlArray)
                    tipusItemArray = itemControlArrayByControlArray.tipusControl

                    If String.IsNullOrEmpty(llistaItems) Then
                        llistaItems = String.Format("{0}", itemControlArrayByControlArray.nomControl)
                    Else
                        llistaItems = String.Format("{0}, {1}", llistaItems, itemControlArrayByControlArray.nomControl)
                    End If
                Next

                Dim instruccioInicialitzacio As String = String.Format("{0} = New {1}(New List(Of {2})", nomArray, tipusArray, tipusItemArray) & "({" & llistaItems & "}))"

                lstInicialitzacio.Add(instruccioInicialitzacio)

                'Debug.Print(String.Format("Inicialitzacio ControlArray {0}", instruccioInicialitzacio))
            End If

        Next

        Return lstInicialitzacio
    End Function

    ''' <summary>
    ''' Carrega la llista de fitxers modificats desde la data <see cref="dtDesde"/>
    ''' </summary>
    Private Sub CarregaFitxersVB6ModificatsDesde()
        If dtDesde.Value.ToShortDateString = "01/01/2000" Then Exit Sub
        If cboDirectoryVB6.Text = CARPETA_VB6 Then Exit Sub

        Netejar("VB6")
        Netejar("NET")

        VB6 = True

        lblProcessant.Text = String.Format("Buscant VB6 modificats des de la data {0} ....", dtDesde.Value.ToShortDateString)

        Cursor = Cursors.WaitCursor

        '! mirem si hi ha fitxers modificats a partir de la data
        If ExisteixFitxersVB6ModificatsDesDe(dtDesde.Value) Then
            numCoincidenciesVB6 = CarregarListView(dtVB6, lstItemsFoundVB6, "")
        End If

        lblTrobats_1.Text = lstItemsFoundVB6.Items.Count.ToString

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Guardem linies i el numero de linia dins un fitxer
    ''' </summary>
    Structure LiniaNumLinia
        Dim linia As String
        Dim numLinia As Integer
    End Structure


    ''' <summary>
    ''' Guarda la informació necessaria per moure una linia a una altra posicio i que afecta al mateix control (designer)
    ''' <para>Exemple : volem moure la linia "Me.TabControlPage1.Controls.Add(cmdCanviQuota)" just abans de la linia "cmdCanviQuota.OcxState = XXXXXX"</para>
    ''' </summary>
    Structure MovimentLinia
        Dim numliniaDesti As Integer ' numero de linia que determina a on hem de moure una linia
        Dim numliniaOrigen As Integer  ' numero de linia de la linia que volem moure
        Dim contenidor As String  ' contenidor d'un control
        Dim trobatLiniaOnInsertar As Boolean   ' indica si s'ha trobat la linia a on volem moure una altra linia

        Public Sub New(ByVal _numliniaDesti As Integer, ByVal _numliniaOrigen As String, ByVal _contenidor As String, ByVal _trobatLiniaOnInsertar As Boolean)
            numliniaDesti = _numliniaDesti  ' numero de linia (OCX) on hauriem d'insertar la linia
            numliniaOrigen = _numliniaOrigen
            contenidor = _contenidor
            trobatLiniaOnInsertar = _trobatLiniaOnInsertar
        End Sub

    End Structure

    ''' <summary>
    ''' Guarda tots les diferents linies d'UPGRADE (NO REPETIDES!)
    ''' </summary>
    Structure Upgrade
        'Dim codi As String
        Dim descripcio As String
        Dim solucio As String
        Dim observacio As String
        Dim tipus As String
        Dim color As ColorUpgrade

        Public Sub New(ByVal _descripcio As String, ByVal _solucio As String, ByVal _observacio As String, ByVal _tipus As String, ByVal _color As ColorUpgrade)
            'codi = _codi
            descripcio = _descripcio
            solucio = _solucio
            observacio = _observacio
            tipus = _tipus
            color = _color
        End Sub
    End Structure

    Dim upgrades As New Dictionary(Of String, List(Of Upgrade))

    ''' <summary>
    ''' és un control de tipus Toolbar ?
    ''' </summary>
    ''' <returns></returns>
    Private Function EsToolbar(ByVal linia As String) As Boolean
        Dim splitList() As String = {}

        If RegexCercaCadena(linia, "\s*Begin MSComctlLib\.Toolbar", splitList) Then
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' Propietats Treeview
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="controlVB6"></param>
    Private Sub WorkAround_Properties_TreeView(ByVal linia As String, ByRef controlVB6 As ControlVB6)
        Dim splitList() As String = {}

        If RegexCercaCadena(linia, "\s*Style" & CONST_EQUALS & "(\d+)", splitList) Then
            controlVB6.TreeViewStyle = splitList(1)
        End If


        If RegexCercaCadena(linia, "\s*LineStyle" & CONST_EQUALS & "(\d+)", splitList) Then
            controlVB6.TreeViewLineStyle = splitList(1)
        End If

    End Sub

    ''' <summary>
    ''' Propietats GmsTemps
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="controlVB6"></param>
    Private Sub WorkAround_Properties_GmsTemps(ByVal linia As String, ByRef controlVB6 As ControlVB6)
        Dim splitList() As String = {}

        If RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_FONTNAME & "\s*=\s*\""([\w\s]+)\""", splitList) Then
            controlVB6.FontName = splitList(1)
        End If

        If RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_FONTSIZE & "\s*=\s*([\w\s\,]+)", splitList) Then
            controlVB6.FontSize = splitList(1)
        End If
    End Sub

    ''' <summary>
    ''' Propietats FCGrid
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="controlVB6"></param>
    Private Sub WorkAround_Properties_FCGrid(ByVal linia As String, ByRef controlVB6 As ControlVB6)
        Dim splitList() As String = {}

        If RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_DEFAULTFONTNAME & CONST_EQUALS & "\""([\w\s]+)\""", splitList) Then
            controlVB6.FCGridDefaultFontName = splitList(1)
        End If

        If RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_DEFAULTFONTSIZE & CONST_EQUALS & "([\w\s]+)", splitList) Then
            controlVB6.FCGridDefaultFontSize = splitList(1)
        End If

        If RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_DEFAULTROWHEIGHT & CONST_EQUALS & "([\w\s]+)", splitList) Then
            controlVB6.FCGridDefaultRowHeight = splitList(1)
        End If

        If RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_SELECTIONMODE & CONST_EQUALS & "(\d+)", splitList) Then
            controlVB6.SelectionMode = CalculaSelectionMode_FCGrid(splitList(1))
        End If

        '? WORKAROUND : els grid/Net la propietat Locked NO existeix en Design PERO SI en Runtime
        '? Utilitzem la propietat Enabled per indicar si es ReadOnly o no
        '? Si ReadOnly = True --> Enabled = False
        '? Si ReadOnly = False --> Enabled = True
        If RegexCercaCadena(linia, REGEX_ESPAI & PROPIETAT_READONLY & CONST_EQUALS & "([\-\d]+)", splitList) Then
            controlVB6.Enabled = If(splitList(1) = "-1", "False", "True")
        End If
    End Sub

    ''' <summary>
    ''' Calcula el valor de la propietat SelectionMode/FCGrid
    ''' </summary>
    ''' <param name="valor"></param>
    ''' <returns></returns>
    Private Function CalculaSelectionMode_FCGrid(valor As String) As String
        ' SelectionModeEnum.Free = 0 , SelectionModeEnum.ByRow = 1, SelectionModeEnum.ByColumn = 2 , SelectionModeEnum.ByCell = 3
        Select Case valor
            Case "0"
                Return "SelectionModeEnum.Free"
            Case "1"
                Return "SelectionModeEnum.ByRow"
            Case "2"
                Return "SelectionModeEnum.ByColumn"
            Case "3"
                Return "SelectionModeEnum.ByCell"
        End Select

    End Function

    ''' <summary>
    ''' Migració propietats relacionades amb la Font del control
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="controlVB6"></param>
    Private Sub WorkAround_PropertyFont(ByVal linia As String, ByRef controlVB6 As ControlVB6)
        Dim splitList() As String = {}

        '! Property Font/VB6
        If RegexCercaCadena(linia, "\s*BeginProperty Font", splitList) Then
            propertyFontTrobada = True

            '! Font + End Property
        ElseIf propertyFontTrobada AndAlso RegexCercaCadena(linia, LINIA_ENDPROPERTY, splitList) Then
            propertyFontTrobada = False

        ElseIf propertyFontTrobada Then  '! estem dins la Property Font (VB6/frm)

            '! Propietats de la Font
            If RegexCercaCadena(linia, "\s*Name\s+=\s*\""([\w\s]+)\""", splitList) Then
                '''' controlVB6.FontName = splitList(1)     '? exemple VB6 :  MS Sans Serif
            ElseIf RegexCercaCadena(linia, "\s*Size\s+=\s*([\w\s\.]+)", splitList) Then
                ''''controlVB6.FontSize = splitList(1)      '? exemple VB6 :  13.5
            ElseIf RegexCercaCadena(linia, "\s*Charset\s+=\s*([\w\s\.]+)", splitList) Then
                controlVB6.FontCharSet = splitList(1)      '? exemple VB6 :  0
            ElseIf RegexCercaCadena(linia, "\s*Weight\s+=\s*(\w+)", splitList) Then
                controlVB6.FontWeight = splitList(1)      '? exemple VB6 :  700
            ElseIf RegexCercaCadena(linia, "\s*Underline\s+=\s*(\w+)", splitList) Then
                controlVB6.FontUnderline = splitList(1)      '? exemple VB6 :  0   'False
            ElseIf RegexCercaCadena(linia, "\s*Italic\s+=\s*(\w+)", splitList) Then
                controlVB6.FontItalic = splitList(1)      '? exemple VB6 :  0   'False
            ElseIf RegexCercaCadena(linia, "\s*Strikethrough\s+=\s*(\w+)", splitList) Then
                controlVB6.FontStrikethrough = splitList(1)      '? exemple VB6 :  0   'False
            End If

        End If
    End Sub

    ''' <summary>
    ''' WORKAROUND : Size AxPushButtons el Wizard 2008 no ho fa bé
    ''' Cal modificar les linies :
    '''     - _ExtentX        =  xxxxx
    '''     - _ExtentY        =  yyyyy
    '''     
    ''' substituint xxxx i/o yyyy per 0    
    ''' </summary>
    ''' <param name="rutaBACKUP"></param>
    Private Sub WorkAround_TamanyButons(ByVal rutaBACKUP As String)
        '! recuperem tots els fitxers FRM (recursivament)
        Dim fitxersVB6 As String() = IO.Directory.GetFiles(rutaBACKUP, "*.frm", IO.SearchOption.AllDirectories)

        For Each fitxerFRM As String In fitxersVB6

            Dim splitList() As String = {}
            Dim liniaBeginTrobadaPushButton As Boolean = False
            Dim arrText As New List(Of String)

            '! llegim el fitxer FRM
            Dim liniesFitxerVB As List(Of String) = LlegeixFitxer(fitxerFRM, encoding)

            For i As Integer = 0 To liniesFitxerVB.Count - 1

                splitList = {}

                Dim novalinia As String = liniesFitxerVB(i)

                If RegexCercaCadena(novalinia, LINIA_END, splitList) Then    ' final definició del control
                    liniaBeginTrobadaPushButton = False

                    ' Inici definicio d'un control AxPushButton. Begins anidats. Excloim els controls de tipus "VB.xxxxx"   (exemple : VB.Form, VB.TextBox, VB.Label,,...)
                ElseIf RegexCercaCadena(novalinia, "(?i)Begin\s((?!VB)\w+\.\w+)\s(cmd\w+)", splitList) Then
                    liniaBeginTrobadaPushButton = True

                ElseIf liniaBeginTrobadaPushButton AndAlso RegexCercaCadena(novalinia, "(\s+_Extent\w+\s*=\s*)" & REGEX_NUM, splitList) Then
                    novalinia = RegexSubstitucioCadena(novalinia, "(\s+_Extent\w+\s*=\s*)" & REGEX_NUM, "$1 0")
                End If

                '! afegim la linia (potser igual o modificada)
                arrText.Add(novalinia)
            Next

            '! gravem el fitxer frm amb les modificacions
            File.WriteAllLines(fitxerFRM, arrText.ToArray, GetFileEncoding(fitxerFRM))
        Next
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxerDesigner"></param>
    ''' <param name="arrText"></param>
    Private Sub Workaround_ReordenarControlArray_Frame(ByVal fitxerDesigner As String, ByRef arrText As List(Of String))
        Dim patternReordenar As String = REGEX_CONST_ME & "(\w+)\.Controls\.Add\([" & REGEX_CONST_ME & "]*_(\w+)_\d+\)"   '  Exemple : Me.Frame1.Controls.Add(Me._framePreguntes_0)

        Dim index As Integer = 0

        Dim indexControlArrays As New Dictionary(Of String, Integer)

        '! recorrem tot el fitxer
        For numerolinia As Integer = 0 To arrText.Count - 1
            Dim splitList() As String = {}

            Dim trobat As Boolean = RegexCercaCadena(arrText(numerolinia), patternReordenar, splitList)
            If trobat Then

                '! calculem el contenidor al qual pertany l'element del controlArray que estem "tractant"
                Dim contenidor As ControlVB6 = ObtenirControlVB6ByName(splitList(1), lstControlsVB6)

                '! calculem totes les tabpages de la tabcontrol
                Dim elementControlArray As List(Of ControlVB6) = lstControlsVB6.Where(Function(c) c.EsElementControlArray AndAlso c.nomControl.StartsWith(UNDERSCORE & splitList(2) & UNDERSCORE) AndAlso c.nomControlPare = contenidor.nomControl).ToList

                If elementControlArray.Count = 0 Then
                    Continue For      '? dimarts 21/12
                End If


                '? calculem el index més petit dels elements del controlArray
                Dim minIndex As Integer = elementControlArray.Min(Function(s) CInt(s.Index))
                If index < minIndex Then
                    indexControlArrays.Item(splitList(2)) = minIndex
                End If

                '? ************** pot ser que els indexS dels elements del controlArray no siguin consecutius
                '? Exemple : frmUsuari/Persones  --> etiquetes _lbl1_17  i _lbl1_19 pertanyen al mateix contenidor (_GroupBox3_2) PERÒ no hi ha el _lbl1_18 !!!!!!!!
                For Each el In elementControlArray

                    '! si no existeix en el diccionari inicialitzem index 
                    If Not indexControlArrays.ContainsKey(splitList(2)) Then
                        indexControlArrays.Add(splitList(2), 0)
                    Else
                        indexControlArrays.Item(splitList(2)) = indexControlArrays.Item(splitList(2)) + 1
                    End If


                    Dim elementByIndex As ControlVB6 = elementControlArray.Where(Function(e) e.Index = indexControlArrays.Item(splitList(2))).FirstOrDefault

                    If elementByIndex Is Nothing Then
                        Debug.WriteLine(String.Format("WARNING::no existeix un element de control Array amb el index {0}", indexControlArrays.Item(splitList(2))).ToString)
                    Else
                        '! construim la nova linia
                        Dim cadenaReordenada As String = CONST_ME & contenidor.nomControl & ".Controls.Add(" & CONST_ME & elementByIndex.nomControl & ")"

                        '! actualitzem la linia
                        arrText(numerolinia) = cadenaReordenada

                        Exit For    '? hem de surtir del primer For perquè ja hem processat la linia
                    End If
                Next

            End If

        Next

    End Sub

    ''' <summary>
    ''' WORKAROUND : no sé perquè el Wizard2008, en els formularis que conté TabControl/TabPages, els TabPages es reordenen a l'inversa
    ''' Exemple : en VB6 tenim tabControl1 amb les tabPages : tabPage1, tabPage2,... tabPageN
    '''           deprés del Wizard2008 en NET tenim : tabPageN, ....., tabPage2,tabPage1    
    ''' Cal per tant resoldre el Bug, agafant el fitxer (formulari) en NET de Backup i "reordenar-lo"    
    ''' </summary>
    Private Sub WorkAround_ReordenarTabControlTabPages(ByVal fitxerDesigner As String, ByRef arrText As List(Of String))
        ' manté les TabControl i els numero de linia dels seus TabPages
        Dim tabControls As New Dictionary(Of String, List(Of LiniaNumLinia))

        Dim patternReordenar As String = "(?i)Me\.(TabControl\d*)\.Controls\.Add\((\w+(\.)*\w+)\)"           '? Exemple : Me.TabControl2.Controls.Add(TabControlPage10)

        '! la posicio de
        Dim index As Integer = 0

        '! recorrem tot el fitxer
        For numerolinia As Integer = 0 To arrText.Count - 1
            Dim splitList() As String = {}

            Dim trobat As Boolean = RegexCercaCadena(arrText(numerolinia), patternReordenar, splitList)
            If trobat Then
                '! calculem el tabControl al qual pertany la tabPage que estem "tractant"
                Dim tabcontrol As ControlVB6 = ObtenirControlVB6ByName(splitList(1), lstControlsVB6)

                '! calculem totes les tabpages de la tabcontrol
                Dim tabs As List(Of ControlVB6) = lstControlsVB6.Where(Function(c) c.nomControlPare = tabcontrol.nomControl).ToList

                '! calculem la tabpage tal que té la "propietat" "Page" igual a l'"index"
                Dim tabpageByIndex = tabs.Where(Function(c) c.Page = index).FirstOrDefault

                '! incrementem l'index
                index = index + 1

                If tabpageByIndex Is Nothing Then
                    Continue For
                End If

                '! construim la nova linia
                Dim cadenaReordenada As String = CONST_ME & tabcontrol.nomControl & ".Controls.Add(" & CONST_ME & tabpageByIndex.nomControl & ")"

                '! actualitzem la linia
                arrText(numerolinia) = cadenaReordenada
            End If

        Next

    End Sub

    ''' <summary>
    ''' Comprova si un control es ActiveX
    ''' CRITERI: revisar !!!!!
    ''' </summary>
    ''' <param name="control"></param>
    ''' <returns></returns>
    Private Function EsControlActiveX(ByVal control As String) As Boolean
        If control.ToUpper.StartsWith("CMD") Then ''''EL SEGUENT no es pot posar pq un axgroupbox pot ser que no estigui dins un contenidor NET      OrElse control.ToUpper.Contains("FRAME") OrElse control.ToUpper.Contains("_OPT") Then
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' Comprova si un control es un contenidor
    ''' CRITERI: revisar !!!!
    ''' </summary>
    ''' <param name="control"></param>
    ''' <returns></returns>
    Private Function EsControlContenidor(ByVal control As String) As Boolean
        If control.ToUpper.Contains("TABCONTROLPAGE") Then ''''EL SEGUENT no es pot posar pq un axgroupbox pot ser que no estigui dins un contenidor NET     OrElse control.ToUpper.Contains("FRAME") OrElse control.ToUpper.Contains("GROUPBOX") Then
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' WORKAROUND : el Wizard2008 NO migra correctament l'ordre dels elements dels ControlArrayS
    ''' </summary>
    ''' <param name="liniesSubstituides"></param>
    ''' <returns></returns>
    Private Function Workaround_OrdreElementsControlArray(numLinia As Integer, ByRef liniesSubstituides As List(Of String)) As Boolean
        Dim splitList() As String = {}

        '! comprovem si la linia és el tipus : Me.XXXX.Controls.Add(Me._OptVisualitzacio_0)
        If RegexCercaCadena(liniesFitxerNET(numLinia), "Controls\.Add\(([\w\.]+)\)", splitList) Then
            Dim controlVB6 As ControlVB6 = ObtenirControlVB6ByName(splitList(1), lstControlsVB6)

            '? dimecres 7/7
            If controlVB6 Is Nothing Then   '? exemple : ShapeContainer1
                Return False
            End If

            '! si és un element d'un controlArray
            If controlVB6.EsElementControlArray Then
                '! nom del controlArray al qual pertany l'element
                Dim nomControlArray As String = Split(controlVB6.nomControl, UNDERSCORE)(1)

                '? comprovem si el procés de re-ordrenament pel controlArray que estem tractant ja s'ha fet.Si és el cas, surtim
                If dicEsOrdenatElementsControlArray.ContainsKey(nomControlArray) AndAlso dicEsOrdenatElementsControlArray.Item(nomControlArray) = True Then Return True

                '! recuperem els elements del ControlArray
                Dim elements As List(Of ControlVB6) = lstControlsVB6.Where(Function(s) s.nomControl.Contains(UNDERSCORE) AndAlso Split(s.nomControl, UNDERSCORE)(1) = nomControlArray).ToList

                '? reordanem els elements del controlArray segons el seu index
                For indx As Integer = 0 To elements.Count
                    For Each e As ControlVB6 In elements
                        If indx = e.Index Then
                            '! calculem el contenidor al qual pertany l'element del controlArray que estem "tractant"
                            Dim contenidor As ControlVB6 = ObtenirControlVB6ByName(splitList(1), lstControlsVB6)

                            '! afegim la linia
                            Dim novalinia As String = CONST_ME & e.nomControlPare & ".Controls.Add(" & CONST_ME & e.nomControl & ")"

                            liniesSubstituides.Add(novalinia)

                            '! ja no cal seguir per l'element e
                            Exit For
                        End If
                    Next
                Next

                '? marquem que el procés de re-ordrenament ja s'ha fet i no caldrà repetir-lo
                dicEsOrdenatElementsControlArray.Add(nomControlArray, True)

                '! indiquem que realment hem processat el re-ordenament d'elements d'un ControlArray
                Return True
            End If
        End If

        Return False
    End Function

    ''' <summary>
    ''' Encapsula un Bug
    ''' </summary>
    ''' <param name="reglaRegex"></param>
    ''' <param name="linia"></param>
    ''' <param name="fitxer"></param>
    ''' <param name="numLinia"></param>
    Private Sub WorkAround_Bug(ByVal reglaRegex As String, ByVal tipusBug As TipusBug, ByVal color As Color, ByVal descripcioSolucio As String, ByVal linia As String, ByVal fitxer As String, ByVal numLinia As Integer, ByVal liniaSeguent As String, Optional ByVal liniaAfegida As String = "")
        If RegexCercaCadena(linia, reglaRegex) OrElse tipusBug = TipusBug.MANUAL Then

            '? dijous 15/7
            '! comprovem si ja s'ha processat en algun procés anterior
            If tipusBug <> TipusBug.MANUAL AndAlso Adicions_Modificacions_Processades(liniaSeguent, liniaAfegida) Then Exit Sub

            Dim lvi As New ListViewItem()

            With lvi
                .BackColor = color

                .SubItems.Add(tipusBug.ToString)
                .SubItems.Add(linia)
                .SubItems.Add(fitxer)
                .SubItems.Add(numLinia)
                .SubItems.Add(descripcioSolucio)
            End With

            '! afegim la linia
            lviBugs.Items.Add(lvi)
        End If
    End Sub

    ''' <summary>
    ''' retorna True/False en funció de si ja hem aplicat les adicions/modificacions associades als "bugs"
    ''' </summary>
    ''' <param name="liniaseguent"></param>
    ''' <param name="liniaAfegida"></param>
    ''' <returns></returns>
    Private Function Adicions_Modificacions_Processades(ByVal liniaseguent As String, ByVal liniaAfegida As String) As Boolean
        '! condició : la linia seguent a la que tractem = <linia que posem quan afegim/modifiquem>
        If liniaseguent.Trim = liniaAfegida Then Return True

        '? ItemChecked (ListView)
        If RegexCercaCadena(liniaseguent, "If \w+\.FocusedItem Is Nothing Then Exit Sub") Then Return True

        Return False
    End Function

    ''' <summary>
    ''' CASOS DETECTATS (de moment!) :
    '''     1.- Treeview_AfterCheck.Degut a les modificacions relacionades amb MarcaNode,... es raise més vegades.Cal evitar
    '''     2.- TextChanged . L'event es l'equivalent a l'event Change/VB6.Hem detectat que es "podria" raise onLoad (InitializeComponent + asignacio lbl.Text =) , que cal evitar
    '''     3.- AfterSelect + Load (Treeview)
    ''' </summary>
    ''' <param name="numlinia"></param>
    ''' <param name="fitxer"></param>
    ''' <param name="liniesFitxerVBNet"></param>
    Private Sub WorkAround_Bugs(ByVal numlinia As Integer, ByVal fitxer As String, ByVal liniesFitxerVBNet As List(Of String))

        '! comprovem que no estiguem tractant l'última linia del fitxer
        If numlinia + 1 >= liniesFitxerVBNet.Count Then Exit Sub

        '! linia que estem tractant
        Dim linia As String = liniesFitxerVBNet(numlinia)

        '! linia seguent
        Dim liniaSeguent As String = liniesFitxerVBNet(numlinia + 1)


        '! CAS 1
        '! La linia que caldria posar és : 
        '?              If FlagMarcaNodePare OrElse FlagMarcaNode OrElse FlagCarregantTree Then Exit Sub
        WorkAround_Bug("Private Sub \w+_AfterCheck", TipusBug.AFTERCHECK, Color.Chartreuse, "Si es TreeView, cal afegir la linia : If FlagCarregantTree Then Exit Sub", linia, fitxer, numlinia, liniaseguent, LINIA_AFEGIDA_AFTERCHECK)

        '! CAS 2
        '! Hem detectat que el control AxFormeExtender té les propietats ClientMinHeight i ClientMinWidth en PIXELS
        '! i al ser un control ActiveX els valors estàn en el fitxer Resx.
        '! SOLUCIO : cal modificar MANUALMENT els valors convertint-los a PIXELS
        WorkAround_Bug("Public WithEvents \w+ As AxXtremeSuiteControls\.AxFormExtender", TipusBug.AXFORMEXTENDER, Color.BurlyWood, "Substituir els valors de ClientMinHeight i ClientMinWidth en PIXELSEL", linia, fitxer, numlinia, liniaseguent)

        '! CAS 3
        WorkAround_Bug("My\.Computer\.Clipboard\.SetText", TipusBug.CLIPBOARD_SETTEXT, Color.Yellow, "Cal afegir : If String.IsNullOrEmpty", linia, fitxer, numlinia, liniaseguent)

        '! CAS 4     
        '! 'UPGRADE_WARNING: El evento XXXXXX.TextChanged se puede desencadenar cuando se inicializa el formulario.
        WorkAround_Bug("Private Sub \w+_TextChanged", TipusBug.TEXTCHANGED, Color.Aquamarine, "L'event es pot raise en InitializeComponent (+ assignació propietat Text)", linia, fitxer, numlinia, liniaseguent, LINIA_AFEGIDA_TEXTCHANGED)

        '! CAS 5     
        '! 'UPGRADE_WARNING: El evento calAgenda_ViewChanged se puede desencadenar cuando se inicializa el formulario.
        WorkAround_Bug("Private Sub \w+_ViewChanged", TipusBug.VIEWCHANGED, Color.Cyan, "L'event es pot raise en InitializeComponent (+ assignació propietat Text)", linia, fitxer, numlinia, liniaSeguent, LINIA_AFEGIDA_VIEWCHANGED)

        '! CAS 6
        '! UPGRADE_WARNING: El evento frmXXXXXXX.Resize se puede desencadenar cuando se inicializa el formulario
        WorkAround_Bug("Private Sub \w+_Resize", TipusBug.RESIZE, Color.LightGreen, "Cal afegir : If Not DesignMode AndAlso IsInitializing Then Exit Sub", linia, fitxer, numlinia, liniaseguent, LINIA_AFEGIDA_RESIZE)

        '! CAS 7
        '! UPGRADE_WARNING: El evento cmbXXXXXXX.SelectedIndexChanged se puede desencadenar cuando se inicializa el formulario
        WorkAround_Bug("Private Sub \w+_SelectedIndexChanged", TipusBug.SELECTEDINDEXCHANGED, Color.LightSkyBlue, "Cal afegir : If Not DesignMode AndAlso IsInitializing Then Exit Sub", linia, fitxer, numlinia, liniaSeguent, LINIA_AFEGIDA_SELECTEDINDEXCHANGED)

        '! CAS 8
        If numlinia + 2 < liniesFitxerVBNet.Count Then
            WorkAround_Bug("Private Sub \w+_ItemChecked\(", TipusBug.ITEMCHECKED, Color.LightPink, "Cal afegir : If LISTVIEW.FocusedItem Is Nothing Then Exit Sub", linia, fitxer, numlinia, liniesFitxerVBNet(numlinia + 2), LINIA_AFEGIDA_ITEMCHECKED_FOCUSEDITEM)
        End If

        '! CAS 9
        '! La linia que caldria posar és : 
        '?    If flag_formshowing Then Exit Sub  '? WORKAROUND : <treeview>.SelectedNode = XXXXX + OnLoad , no ha de processar el codi
        WorkAround_Bug("Private Sub \w+_AfterSelect", TipusBug.AFTERSELECT, Color.Crimson, "Si es TreeView, cal afegir la linia : If flag_formshowing Then Exit Sub", linia, fitxer, numlinia, liniaSeguent, LINIA_AFEGIDA_AFTERSELECT)

    End Sub

    ''' <summary>
    ''' MOLT IMPORTANT!!!!!!: Requisit imprescindible perquè els events siguin gestionats pel nostre MyEventHandler
    ''' Cal que el nom del mètode d'event sigui nomcontrol_nomevent (CASE-SENSITIVE)
    ''' </summary>
    ''' <param name="linia"></param>
    Private Sub WorkAround_NomMetodeEvent_Igual_NomEventHandler(ByVal linia As String, ByVal fitxer As String, ByVal numLinia As Integer)
        Dim splitList() As String = {}

        '! comprovem si la linia es el d'un mètode d'event
        RegexCercaCadena(linia, "Private Sub (\w+)\([\w+-\.-\(\- ]+\) Handles (\w+)\.(\w+)", splitList)

        If splitList.Count > 0 Then
            Dim nomMetodeEvent As String = splitList(1)
            Dim nomControlMetodeEvent As String = splitList(2)
            Dim nomEvent As String = splitList(3)

            '! excloim els mètodes d'event on el nom del control es MyBase i Me
            If nomControlMetodeEvent.ToUpper = "MYBASE" OrElse nomControlMetodeEvent.ToUpper = "ME" Then Exit Sub

            '! calculem el nom del control NET corresponent al mètode d'event
            Dim nomControlNET As String = String.Empty
            For Each n As ControlNET In controlsNetByForm
                If nomControlMetodeEvent.ToUpper = n.nomControl.ToUpper Then
                    nomControlNET = n.nomControl

                    Exit For
                End If
            Next

            Dim lvi As New ListViewItem()

            With lvi
                .SubItems.Add(nomControlNET)
                .SubItems.Add(linia)
                .SubItems.Add(fitxer)
                .SubItems.Add(numLinia)
            End With


            '! 1.- Comprovem si cumpleix la condicio que els noms del control dins la linia del mètode d'event son iguals (case-sensitive)
            If nomMetodeEvent <> nomControlMetodeEvent & UNDERSCORE & nomEvent Then
                lvWarningsWorkAround.Items.Add(lvi)

            Else  '! 2.- hem de comprovar TAMBÉ que el nom del control NET coincideix (case-sensitive) amb el dels metodes d'events

                If nomControlMetodeEvent <> nomControlNET AndAlso Not String.IsNullOrEmpty(nomControlNET) Then   '? case-sensitive
                    lvWarningsWorkAround.Items.Add(lvi)
                End If
            End If

        End If
    End Sub

    ''' <summary>
    ''' retorna un boolea en funcio de si la liniasegueix un patro (regex) identic algun tractat previament
    ''' </summary>
    ''' <returns></returns>
    Private Function EsLiniaTwipsPixelsDuplicada(ByVal linia As String) As Boolean
        Dim splitList() As String = {}
        Dim resultat As Boolean = False

        '! busquem quina regla Regex fa "matchs" amb la linia
        For Each rr As ReglaRegex In lstReglesRegex
            If RegexCercaCadena(linia, rr.patroCerca, splitList) Then
                '! comprovem si ja hem processat alguna altra linia previa amb el mateix patró Regex
                If lstPatronsRegexTwipsPixelsProcessats.IndexOf(rr.patroCerca) = -1 Then
                    '! afegim a la llista
                    lstPatronsRegexTwipsPixelsProcessats.Add(rr.patroCerca)

                    Return False
                Else
                    '! duplicada
                    Return True
                End If
            End If
        Next

        Return resultat
    End Function

    ''' <summary>
    ''' Refresca en funció de si volem visualitzar els correctes/incorrectes o tots
    ''' </summary>
    Private Sub RefrescarListViewTwipsPixels()
        ' recorrem tots els items
        For Each lvi As ListViewItem In lvTwipsPixels.Items

            If seleccionarTwipsPixelsTots Then
                ' TODO visibilitat item
            Else
                If Not seleccionarTwipsPixelsCorrectes AndAlso lvi.BackColor = Color.Orange Then
                    ' TODO visibilitat item
                ElseIf seleccionarTwipsPixelsCorrectes AndAlso lvi.BackColor = Color.LightGreen Then
                    ' TODO visibilitat item
                End If
            End If
        Next

    End Sub

    ''' <summary>
    ''' Busca en el fitxer substitucions_AsObject.txt si existeix una determinada linia
    ''' </summary>
    ''' <returns></returns>
    Private Function ExisteixAsObject_Llista(ByVal linia As String, ByVal fitxer As String, ByVal numLinia As Integer) As Boolean
        For Each l As String In lstLiniesSubstitucio
            If l = linia Then

                SubstituirLinia(fitxer, numLinia, "")

                Return True
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="fitxer"></param>
    ''' <param name="numLinia"></param>
    Private Sub WorkAround_LiniesAmbAsObject(ByVal linia As String, ByVal fitxer As String, ByVal numLinia As Integer, ByRef liniesFitxer As List(Of String))
        '! excepcio
        If linia.Contains(LINIA_MIGRADA_OBREFORMULARI) Then Exit Sub
        If linia.ToUpper.Contains("''ELIMINADA") Then Exit Sub


        Dim splitlist_VariableObject() As String = {}

        '! CAS 1 : la declaracio en VB6 de varies variables en una linia, el Wizard2008 converteix la declaracio de la
        '!         primera variable com As Object (WORKAROUND)
        If RegexCercaCadena(linia, "Dim (\w+, [\w\s\,]+) As Object", splitlist_VariableObject) Then
            '! descarta els "ElLIMINADA"
            ''''If linia.ToUpper.Contains("''ELIMINADA") Then Exit Sub

            Dim splitlist() As String = {}

            Dim lvi As New ListViewItem()

            '! recuperem la seguent linia que es la que té el tipus correcte
            Dim liniaSeguent As String = liniesFitxer.Item(numLinia + 1)

            '? si està a la llista de As Object, surtim
            If ExisteixAsObject_Llista(liniaSeguent, fitxer, numLinia + 1) Then Exit Sub


            RegexCercaCadena(liniaSeguent, "Dim \w+ As ([\w\.]+)", splitlist)

            If splitlist.Count > 0 Then
                lvi.BackColor = Color.LightGreen

                lvi.SubItems.Add(linia)
                lvi.SubItems.Add("Dim " & splitlist_VariableObject(1) & " As " & splitlist(1))

            Else
                lvi.BackColor = Color.Orange

                lvi.SubItems.Add(linia)
                If Not linia.ToUpper.Contains("''ELIMINADA") Then
                    lvi.SubItems.Add("''ELIMINADA" & linia)    '? comentem la linia marcant-la com "ELIMINADA"
                Else
                    lvi.SubItems.Add(linia)
                End If
            End If

            lviAsObject.Items.Add(lvi)

            lvi.SubItems.Add(fitxer)
            lvi.SubItems.Add(numLinia)

            comptadorAsObject = comptadorAsObject + 1


            '! CAS 2 : 
            '!       - Si en VB6 no està declarada una variable (exemple: Dim numero), el Wizard 2008 crea un Dim numero As Object (WORKAROUND)
            '!       - Variables que en VB6 estan declarades com As Object
            '!       - variable que pertany a un altre projecte/NameSpace
        ElseIf RegexCercaCadena(linia, "(?i)Dim \w+ As Object", splitlist_VariableObject) Then

            '? si està a la llista de As Object, surtim
            If ExisteixAsObject_Llista(linia, fitxer, numLinia) Then Exit Sub

            '! descarta els "ElLIMINADA"
            If linia.ToUpper.Contains("''ELIMINADA") Then Exit Sub

            Dim lvi As New ListViewItem()

            lvi.BackColor = Color.Orange

            lvi.SubItems.Add(linia)
            If Not linia.ToUpper.Contains("''ELIMINADA") Then
                lvi.SubItems.Add("''ELIMINADA" & linia)    ' comentem la linia marcant-la com "ELIMINADA"
            Else
                lvi.SubItems.Add(linia)
            End If

            lviAsObject.Items.Add(lvi)

            lvi.SubItems.Add(fitxer)
            lvi.SubItems.Add(numLinia)

            comptadorAsObject = comptadorAsObject + 1
        End If

    End Sub

    ''' <summary>
    ''' CAS 1 : lstPersonal.ColumnHeaders.Add( ,  , "Nombre", 5300)
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <returns></returns>
    Private Function ProcessaTWIPSPIXELS_CASOS(ByVal linia As String) As String
        Dim splitlist() As String = {}
        Dim resultat As String = String.Empty

        If RegexCercaCadena(linia, "(" & "ColumnHeaders\.Add\(\w*, \w* , \""\w+\"", \d+\)" & ")", splitlist) Then
            For Each cerca As String In splitlist
                Dim splitListNumeros() As String = {}

                resultat = resultat & ProcesaNumeros(cerca, splitListNumeros)
            Next

            Return resultat
        End If

        Return linia
    End Function

    ''' <summary>
    ''' CAS : If GetPixelsToTwips(xxxxxx) > 4000
    ''' CAS : AlçadaMinima = GetPixelsToTwipsY(grdconsulta.top) + 1000
    ''' CAS : If GetPixelsToTwips(xxxxxx) > (amplada + x)
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <returns></returns>
    Private Function ProcessaGETPIXELSTOTWIPS_CASOS(ByVal linia As String) As String
        Dim splitlist() As String = {}

        '! busquem
        If RegexCercaCadena(linia, REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)" & REGEX_OPERADORS & "(" & REGEX_NUM & ")", splitlist) Then
            Dim calcul As Integer
            Dim t As Integer
            Integer.TryParse(splitlist(2), calcul)

            If Integer.TryParse(splitlist(2), t) Then
                Return RegexSubstitucioCadena(linia, REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\) (" & REGEX_OPERADORS_SENSE_ESPAIS & ") " & REGEX_NUM, "$1 $2 " & Math.Round(calcul / 15).ToString)
            Else
                Return RegexSubstitucioCadena(linia, REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\) (" & REGEX_OPERADORS_SENSE_ESPAIS & ") " & REGEX_NUM, "$1 $2")
            End If

        End If

        '! cas : If GetPixelsToTwips(xxxxxx) > (amplada + x)
        If RegexCercaCadena(linia, REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)" & REGEX_OPERADORS & "(" & REGEX_TOTS_ALFANUM & ")", splitlist) Then

            Dim kk = 1
        End If


        Dim patro4 = "(?:" & REGEX_GTP & "\(?(\(*" & REGEX_TOTS_ALFANUM & "\)*)\))"
        Dim patro5 = "(?:" & REGEX_GPT & "\(?(\(*" & REGEX_TOTS_ALFANUM & "\)*)\))"
        Dim resultat As String = String.Empty

        If RegexCercaCadena(linia, "( [<>=]+ )" & REGEX_GPT & "\(([\w+\.\(\)]+)\)(" & REGEX_ESPAI & "[\+\-]+" & REGEX_ESPAI & ")([\+\-]*\d+)", splitlist) Then

            For Each cerca As String In splitlist
                Dim splitListNumeros() As String = {}

                resultat = resultat & ProcesaNumeros(RegexSubstitucioCadena(cerca, patro5, "$1"), splitListNumeros)
            Next

            Return resultat
        End If

        If RegexCercaCadena(linia, REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\)(" & REGEX_ESPAI & REGEX_MESMENYS & REGEX_ESPAI & ")(" & REGEX_NUM & ")", splitlist) Then
            For Each cerca As String In splitlist
                Dim splitListNumeros() As String = {}

                '! Ssi el numero està dins un GetTwipsToPixels no s'ha de tractar ara
                If linia.Contains("GetTwipsToPixels") Then
                    resultat = resultat & RegexSubstitucioCadena(cerca, patro5, "$1")
                Else
                    resultat = resultat & ProcesaNumeros(RegexSubstitucioCadena(cerca, patro5, "$1"), splitListNumeros)
                End If

            Next

            Return resultat
        End If


        Return linia
    End Function

    Private Function ProcessaGETTWIPSTOPIXELS(ByVal linia As String) As String
        If Not linia.Contains(GETTWIPSTOPIXELS) Then Return linia

        Dim splitlist() As String = {}

        Dim patro4 = "(?:" & REGEX_GTP & "\(?(\(*" & REGEX_TOTS_ALFANUM & "\)*)\))"   '! obté elS contingutS dins elS REGEX_GTP

        Dim patro5 = "(?:(" & REGEX_GTP & "\(?\(*" & REGEX_TOTS_ALFANUM & "\)*\)))"     '! obté elS contingutS dins elS REGEX_GTP (conte la cadena REGEX_GTP)

        Dim resul3 As Boolean = RegexCercaCadena(linia, patro5, splitlist)   '! conte la cadena REGEX_GTP

        Dim resultat As String = String.Empty

        '! per cada cas trobat
        For Each cerca As String In splitlist

            If Not cerca.Contains(GETTWIPSTOPIXELS) Then
                resultat = resultat & cerca
                Continue For
            End If

            Dim splitListNumeros() As String = {}

            resultat = resultat & ProcesaNumeros(RegexSubstitucioCadena(cerca, patro4, "$1"), splitListNumeros)
        Next

        Return resultat
        'Debug.WriteLine(String.Format("------------------- {0}||{1}", linia, RegexSubstitucioCadena(linia, patroSubstitucio, "$2")))

    End Function

    ''' <summary>
    ''' Treu tots els GETPIXELSTOTWIPS INNECESSARIS. 
    ''' CAS1 : GetPixelsToTwips(num1 + 33 + string)  --> num/15 + 33 + string
    ''' CAS2 : Me.Top = (GetPixelsToTwipsY(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - GetPixelsToTwipsY(Me.Height) / 2 - 1000)
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <returns></returns>
    Private Function ProcessaGETPIXELSTOTWIPS(ByVal linia As String) As String
        If Not linia.Contains(GETPIXELSTOTWIPS) Then Return linia

        Dim splitlist() As String = {}
        Dim resultat As String = String.Empty

        '! dividim en parts
        Dim patroCerca As String = "(?:(" & REGEX_GPT & "\(+[a-zA-ZçÑ_\s\t\.\d\\""\/|\,\&\(\)]+\)+" & REGEX_ESPAI & REGEX_MESMENYS & REGEX_ESPAI & "\d*))"
        RegexCercaCadena(linia, patroCerca, splitlist)

        For Each s As String In splitlist
            Dim patroSubstitucio As String = "(?:" & REGEX_GPT & "\(+([a-zA-ZçÑ_\s\t\.\d" & "\*\\\/" & """|\,\&]+)\))+"

            Dim splitlistNumeros() As String = {}

            '! Si té el numero i està dins un GetTwipsToPixels no s'ha de tractar ara
            If RegexCercaCadena(s, REGEX_NUM) Then
                If Not linia.Contains("GetTwipsToPixels") Then
                    Dim ll = ProcesaNumeros(s, splitlistNumeros)

                    resultat = resultat & RegexSubstitucioCadena(ll, patroSubstitucio, "$1")

                Else
                    resultat = resultat & RegexSubstitucioCadena(s, patroSubstitucio, "$1")
                End If

            Else  '? x
                Dim ll = ProcesaNumeros(s, splitlistNumeros)

                resultat = resultat & RegexSubstitucioCadena(ll, patroSubstitucio, "$1")
            End If

        Next

        If splitlist.Count = 0 Then
            Return linia    '?  probablement s'haurà de fer manual
        End If

        Debug.WriteLine(String.Format("------------------- {0}||{1}", linia, resultat))

        Return resultat
    End Function

    ''' <summary>
    ''' CAS 2 : GetPixelsToTwips(xxxxxx) + 4000
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <returns></returns>
    Private Function ProcessaGETPIXELSTOTWIPS_2(ByVal linia As String) As String
        Dim splitlist() As String = {}

        '! busquem
        If RegexCercaCadena(linia, REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\) " & REGEX_OPE & " (" & REGEX_NUM & ")", splitlist) Then
            Dim calcul As Integer

            Integer.TryParse(splitlist(2), calcul)

            Return RegexSubstitucioCadena(linia, REGEX_GPT & "\((" & REGEX_ALFANUM_DOT & ")\) (" & REGEX_OPE & ") " & REGEX_NUM, "$1 $2 " & Math.Round(calcul / 15).ToString)
        End If

        Return linia
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="linia"></param>
    ''' <param name="fitxer"></param>
    ''' <param name="numLinia"></param>
    Private Sub WorkAround_LiniesAmbTwipsPixels(ByVal linia As String, ByVal fitxer As String, ByVal numLinia As Integer)
        If linia.ToUpper.Contains("MIGRATPIXELS") Then Exit Sub

        Dim liniaOrigen As String = linia

        '! abans de tot substituim el VB6.XXXXXXXX pels corresponents GET
        linia = SubtituirVBTwipsToGets(linia)

        '! diferents casos en els quals  no hi ha  ni GETTWIPSTOPIXELS ni GETPIXELSTOTWIPS
        linia = ProcessaTWIPSPIXELS_CASOS(linia)

        linia = ProcessaTwipsAxListViewColumnHeaders(linia)

        '? dimarts 14/12
        '''''''''''''''linia = ProcesarMidesAmbNumeros(linia, fitxer, numLinia)

        '! comprovem si la linia és susceptible de ser migrat
        '!      1.- Conté GETTWIPSTOPIXELS i/o GETPIXELSTOTWIPS i/o TWIPSPERPIXEL
        '!      2.- Conté propietats de "taman" (exemple : .cmdCancelar.Left = .cmdAceptar.Left + 1350)
        If liniaOrigen = linia AndAlso Not linia.Contains(GETTWIPSTOPIXELS) _
                AndAlso Not linia.Contains(GETPIXELSTOTWIPS) _
                AndAlso Not linia.Contains(TWIPSPERPIXEL) AndAlso Not ConteMides(linia) Then Exit Sub


        '! casos especials de GETPIXELSTOTWIPS
        linia = ProcessaGETPIXELSTOTWIPS_CASOS(linia)

        '! eliminem tots els GetPixelsToTwips
        linia = ProcessaGETPIXELSTOTWIPS(linia)


        '! processa els GETTWIPSTOPIXELS
        linia = ProcessaGETTWIPSTOPIXELS(linia)



        '''''linia = ProcesarMidesAmbNumeros(linia, fitxer, numLinia)

        '! evitem "duplicats" (linies que "matchs" alguna regla regex anterior
        If (chkTwipsPixelsDuplicades.Checked AndAlso Not EsLiniaTwipsPixelsDuplicada(linia)) OrElse Not chkTwipsPixelsDuplicades.Checked Then

            Dim numRegla As Integer = PassaTest(liniaOrigen, fitxer, numLinia)

            Dim lvi As New ListViewItem()

            lvi.BackColor = If(numRegla > 0, Color.LightGreen, Color.Orange)

            If linia.Contains(" * ") OrElse linia.Contains(" / ") OrElse linia.Contains(" \ ") Then lvi.BackColor = Color.PaleVioletRed

            If linia.Contains(GETTWIPSTOPIXELS) OrElse linia.Contains(GETPIXELSTOTWIPS) Then lvi.BackColor = Color.OrangeRed

            lvi.SubItems.Add(liniaOrigen)
            lvi.SubItems.Add(linia & LINIA_MIGRADA_TWIPSPIXELS)   '! calcula la linia de substitucio
            lvi.SubItems.Add(numRegla)
            lvi.SubItems.Add(fitxer)
            lvi.SubItems.Add(numLinia)


            If seleccionarTwipsPixelsTots Then
                lvTwipsPixels.Items.Add(lvi)
            Else
                If Not seleccionarTwipsPixelsCorrectes AndAlso lvi.BackColor = Color.Orange Then
                    lvTwipsPixels.Items.Add(lvi)
                ElseIf seleccionarTwipsPixelsCorrectes AndAlso lvi.BackColor = Color.LightGreen Then
                    lvTwipsPixels.Items.Add(lvi)
                ElseIf seleccionarTwipsPixelsNomesNumeros AndAlso lvi.BackColor = Color.PaleVioletRed Then
                    lvTwipsPixels.Items.Add(lvi)
                End If
            End If

            If numRegla = -1 Then
                'Debug.Print(String.Format("Linia no passada : {0}", linia))
            End If

        Else
            Dim kkk = 1
        End If
    End Sub

    '''' </summary>
    '''' <param name="linia"></param>
    '''' <param name="fitxer"></param>
    '''' <param name="numLinia"></param>
    'Private Sub WorkAround__Property_can_only_be_set_to_Nothing(ByVal linia As String, ByVal fitxer As String, ByVal numLinia As Integer)
    '    Dim splitList() As String = {}

    '    RegexCercaCadena(linia, "(?i)ObreFormulari\(frm\w+", splitList)

    '    If splitList.Count > 0 Then
    '        Dim lvi As New ListViewItem()

    '        lvi.SubItems.Add(linia)
    '        lvi.SubItems.Add(fitxer)
    '        lvi.SubItems.Add(numLinia)

    '        lvWorkAroundPropertyCanOnly.Items.Add(lvi)
    '    End If
    'End Sub

    ''' <summary>
    '''     
    ''' 
    ''' <para>EL WIZARD 2008 NO GENERA FITXERS DESIGNER QUE PROVOQUEN AQUEST WORKAROUND.
    ''' EL WORKAROUND PASSA SI CONVERTIM UN CONTENIDOR ACTIVEX (EX: AXTABCONTROLPAGE) A UN CONTENIDOR NET (EX:GMSTABCONTROLPAGE), 
    ''' EL DESIGNER L'HEM DE MODIFICAR DE TAL MANERA  QUE EL Me.CONTENIDORNET.Controls.Add(CONTROLAX) ESTIGUI ABANS DEL OCX</para>
    ''' <para>HEM ESCOLLIT LA LINIA OCXSTATE PER INSERTAR LA LINIA JUST ABANS</para>
    ''' </summary>
    ''' <param name="arrText"></param>
    ''' <param name="controlsAxINContenidor"></param>  
    Private Sub WorkAround_ControlsAX_IN_ContenidorNET(ByRef arrText As List(Of String), controlsAxINContenidor As Dictionary(Of String, MovimentLinia))
        Dim input As String

        novaPosicio = -1
        For numerolinia As Integer = 0 To arrText.Count - 1
            input = arrText(numerolinia)

            ControlsAX_IN_ContenidorNET(input, arrText, controlsAxINContenidor)
        Next


        '! ara realment posarem la linia Me.<CONTENIDOR>.Controls.Add(<CONTROLAX>) en la posició correcte

        Dim index As Integer = 0
        For Each controlAx As String In controlsAxINContenidor.Keys.ToList
            Dim movimentLinia As MovimentLinia = controlsAxINContenidor.Item(controlAx)

            ' nomes hem de "tocar" el que pertanyen a una tabcontrolpage
            If Not String.IsNullOrEmpty(movimentLinia.contenidor) Then
                arrText.Insert(movimentLinia.numliniaDesti + index, CONST_ME & movimentLinia.contenidor & ".Controls.Add(" & controlAx & ")")

                index = index + 1

                '! "eliminem" la linia que hem mogut
                arrText.Item(movimentLinia.numliniaOrigen + index) = ""

                '! hem de marcar com que cal gravar el nou designer
                calGravar = True
            End If

        Next
    End Sub

    Private Sub ControlsAX_IN_ContenidorNET(ByVal input As String, ByRef arrText As List(Of String), controlsAxINContenidorNET As Dictionary(Of String, MovimentLinia))
        Dim splitList() As String = {}

        ' hem de comprovar si ja existeix la linia que volem moure
        Dim patternControlsAxINContenidor = "(?i)Me.(\w+)\.Controls\.Add\((\w+)\)"    ' (1)   control dins GmsTabControlPage (NET)

        Dim liniaPatro As Boolean = RegexCercaCadena(input, patternControlsAxINContenidor, splitList)
        Dim controlContenidor As String = String.Empty
        Dim controlAx As String = String.Empty

        '! es una linia de tipus (1)
        If liniaPatro Then
            novaPosicio = novaPosicio + 1

            controlContenidor = splitList(1)
            controlAx = splitList(2)
            If EsControlContenidor(controlContenidor) AndAlso EsControlActiveX(controlAx) AndAlso controlsAxINContenidorNET.ContainsKey(controlAx) Then

                '! si ja hem trobat la corresponent linia OCX, i ens arriba la linia (1), hem de passar de la linia
                If controlsAxINContenidorNET.Item(controlAx).trobatLiniaOnInsertar Then

                    '! posem la corresponent TabControlPage
                    Dim movimentLinia As MovimentLinia = controlsAxINContenidorNET.Item(controlAx)
                    controlsAxINContenidorNET.Item(controlAx) = New MovimentLinia(movimentLinia.numliniaDesti, novaPosicio, controlContenidor, movimentLinia.trobatLiniaOnInsertar)
                End If
            End If

            Exit Sub
        Else
            novaPosicio = novaPosicio + 1

            '! definim la linia on hem d'insertar la linia que volem moure            
            If RegexCercaCadena(input, "(?i)(\w+).OcxState =", splitList) Then
                controlAx = splitList(1)

                If EsControlActiveX(controlAx) AndAlso controlsAxINContenidorNET.ContainsKey(controlAx) Then
                    '! modifico l'estat
                    Dim movimentLinia As MovimentLinia = controlsAxINContenidorNET.Item(controlAx)
                    controlsAxINContenidorNET.Item(controlAx) = New MovimentLinia(novaPosicio, movimentLinia.numliniaOrigen, movimentLinia.contenidor, True)

                Else
                    controlsAxINContenidorNET.Add(controlAx, New MovimentLinia(novaPosicio, -1, String.Empty, True))
                End If

            End If

        End If

    End Sub

    ' Testos formulari a formulari
    ' ----------------------------
    ' Dim frm As New frmServidors : frm.ResMenu("", "mnuExport")

#Region "Events"

    Private Sub TreeViewVB6_AfterExpand(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewVB6.AfterExpand
        If e.Node Is Nothing OrElse e.Node.Text = ".." Then Exit Sub

        For Each n As TreeNode In e.Node.Nodes
            CargarSubcarpetas(n.Tag, n)
        Next
    End Sub

    Private Sub TreeViewVB6NET_AfterExpand(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewNET.AfterExpand
        If e.Node Is Nothing OrElse e.Node.Text = ".." Then Exit Sub

        For Each n As TreeNode In e.Node.Nodes
            CargarSubcarpetas(n.Tag, n)
        Next
    End Sub

    Private Sub CboDirectory_TextChanged(sender As Object, e As EventArgs) Handles cboDirectoryVB6.TextChanged
        NetejarTot()
        CargarTreeViews(cboDirectoryVB6.Text)
    End Sub

    Private Sub BtnCercaVB6ModificatsDesde_Click(sender As Object, e As EventArgs) Handles btnCercaVB6ModificatsDesDe.Click
        CarregaFitxersVB6ModificatsDesde()
    End Sub

    Private Sub BuscarFitxersNet()
        Cursor = Cursors.WaitCursor

        carpetaAProcessar = cboDirectoryNet.Text

        '! utilitzem aquest cas per provar certs Tests Unitaris
        If chkMigrarBackup.Checked = False AndAlso cboDirectoryNet.Text = CARPETA_NET Then

            NetejarTot()

            AfegirRowDatatable(dtNET, "provesTwipsPixels.vb")

            numCoincidenciesNet = CarregarListView(dtNET, lstItemsFoundNet, "")

            Exit Sub   ' no té sentit buscar tots els fitxers a partir de CARPETA_NET
        End If

        If chkMigrarBackup.Checked = True AndAlso cboDirectoryVB6_BACKUP.Text = CARPETA_VB6_BACKUP Then
            MessageBox.Show("Es pot cercar a partir de l'arrel")
            Exit Sub   ' no té sentit buscar tots els fitxers a partir de CARPETA_NET        
        End If


        chkSelectAll.Checked = False

        If chkMigrarBackup.Checked = True Then
            carpetaAProcessar = CarpetaDotNET()
        End If

        NetejarTot()

        cboDirectoryNet.Enabled = False
        VB6 = False

        lblProcessant.Text = String.Format("Cercant fitxers NET a la carpeta {0} ....", carpetaAProcessar)

        Cursor = Cursors.WaitCursor
        Application.DoEvents()

        '! en el cas de .NET, els unics fitxers que hem de "processar" son els que acaben en EXTENSIO_VB (ex: FrmAgendaCalendari.vb, FrmAgendaCalendari.Designer.vb, Conexio.vb,...)
        Dim patro As String = "*.vb"

        If Not BuscarItemsVB6_NET(True, True, True, cboDirectoryNet.Text) Then

            '! busquem els fitxers dins la propia carpeta
            FitxersModificatsDirectori(carpetaAProcessar, Date.MinValue, patro, False)     ' Data.Minvalue > tots

            If String.IsNullOrEmpty(carpetaAProcessar) Then Exit Sub

            '! comprovem si estem buscant a partir d'una carpeta associada a un projecte (conté VBP) o no
            esCarpetaRootProjecte = If(Directory.GetFiles(carpetaAProcessar.Replace(carpetaAProcessar, CARPETA_VB6), REGEX_VBP).Count = 0, False, True)

            '! busquem els fitxers a partir de la carpeta (tot l'arbre)
            DirSearch(carpetaAProcessar, Date.MinValue, patro, False)     ' Data.Minvalue > tots
        End If

        lblTrobats_2.Text = CarregarListView(dtNET, lstItemsFoundNet, "")
        lblTrobatClassesNet.Text = CarregarListView(dtNET_2, lstItemsFoundNet_2, "")

        Cursor = Cursors.Default

        '''''lblTrobats_2.Text = lstItemsFoundNet.Items.Count

        lblProcessant.Text = String.Format("Fitxers NET a la carpeta {0} calculats", carpetaAProcessar)
        lstItemsFoundNet.Columns.Item(0).Text = lblProcessant.Text
        cboDirectoryNet.Enabled = True
        'btnMigrar.Enabled = True
        btnCleanMigrar.Enabled = IIf(String.IsNullOrEmpty(CarpetaDotNET), False, True)

        chkSelectAll.Checked = True

        chkNomesProjecte.Checked = True

        Cursor = Cursors.Default
    End Sub

    Private Sub BtnCercaNET_Click(sender As Object, e As EventArgs) Handles btnCercaNET.Click
        btnValidacions.Enabled = True

        BuscarFitxersNet()
    End Sub

    Private Sub btnPostMigracio_Click(sender As Object, e As EventArgs) Handles btnPostMigracio.Click
        Cursor = Cursors.WaitCursor

        Try
            '!  Requeriment : 
            '!  per tal de fer la "postMigracio" es necessari tenir tant el fitxer de projecte VB6 (vbp) com el fitxer de projecte Net (vbproj) en la carpeta Backup Net

            '! busquem el fitxer de projecte VB6 i el copiem a la carpeta Backup Net
            Dim fitxerVB6Proj As String = BuscarFitxersProjectesVB6(cboDirectoryVB6_BACKUP.Text)
            If File.Exists(fitxerVB6Proj) Then File.Copy(fitxerVB6Proj, CarpetaDotNET() + "\" + Path.GetFileName(fitxerVB6Proj), True)

            '
            PostMigracio()

            '! un cop hem fet la PostMigració no podem tornar a fer-la fins que haguem tornat a fer un "proces de migracio"
            btnPostMigracio.Enabled = False
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, ex.Message)

            Cursor = Cursors.Default
        End Try

        Cursor = Cursors.Default
    End Sub

    Private Sub BtnValidacions_Click(sender As Object, e As EventArgs) Handles btnValidacions.Click
        respostaPreguntaOCX = False
        aplicaSeguents = False

        '! netejem el listview d'errors i seleccionem la pestanya
        lvErrorsMigracio.Clear()
        TabControl1.SelectedTab = TabControl1.TabPages(13)

        If chkMigrarBackup.Checked Then   '! codi (fitxers VB Backup)
            CarregarSubstitucionsToList("substitucions_vb.txt")
        Else   '! designer
            CarregarSubstitucionsToList("substitucions_designer.txt")
        End If

        Validar(False)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load

        'CONSTRUCT COLUMNS
        lstItemsFoundVB6.Columns.Add("Data", 100)

        encoding = Encoding.Default

        '! Inicialitzem combos
        cboDirectoryVB6.Text = CARPETA_VB6
        cboDirectoryNet.Text = CARPETA_NET
        cboDirectoryVB6_BACKUP.Text = CARPETA_VB6_BACKUP

        Me.Text = String.Format("Projecte VB6 : {0} Projecte NET : {1}", cboDirectoryVB6.Text, cboDirectoryNet.Text)

        '! Netegem combos
        cboDirectoryVB6.Items.Clear()
        cboDirectoryNet.Items.Clear()
        cboDirectoryVB6_BACKUP.Items.Clear()

        For Each s As String In Directory.GetLogicalDrives()
            cboDirectoryVB6.Items.Add(s)
        Next

        ' DragAndDrop
        'lstItemsFoundVB6.AllowDrop = True
        lstItemsFoundNet.AllowDrop = True

        '? aquestes tabs les fem invisible perque o bé ja no tenen sentit o bé no funciona
        ' ho mantenim per si de cas
        ''''TabControl1.TabPages.Remove(TabPage1)
        ''''TabControl1.TabPages.Remove(TabPage4)

        '
        '''''MostrarProjectesModificats(TreeViewVB6.TopNode)   MOLT LENT!!!

        CarregarReglesRegexTwipsPixels()
        CarregaTooltips()
    End Sub

    Private Sub TreeViewVB6_NodeMouseDoubleClick(sender As Object, e As TreeNodeMouseClickEventArgs) Handles TreeViewVB6.NodeMouseDoubleClick
        '! impedim que es pugui migrar a partir del node Root
        If e.Node.Level = 0 Then Exit Sub

        NetejarTot()

        If e.Node.Text = ".." Then
            If cboDirectoryVB6.Text = CARPETA_VB6 Then Exit Sub

            Dim posSlash As Integer = cboDirectoryVB6.Text.Substring(0, cboDirectoryVB6.Text.Length - 1).LastIndexOf("\")
            cboDirectoryVB6.Text = cboDirectoryVB6.Text.Substring(0, posSlash) & "\"

            ' 
            ActualitzaCombos()

            Me.Text = String.Format("Projecte VB6 : {0} ", cboDirectoryVB6.Text)

        Else
            cboDirectoryVB6.Text = e.Node.Tag.ToString & "\"

            '
            ActualitzaCombos()

            Me.Text = String.Format("Projecte VB6 : {0} ", cboDirectoryVB6.Text)
        End If

        'NetejarTot()

        '! "actualitzem" la data de la ultima migracio del projecte seleccionat
        CarregaDataUltimaMigracio()

        '! "actualitzem" els butons segons l'estat del procés de migració
        ActualitzaButonsEstatProcesMigracio()

        '! carreguem al "listview" els "fitxers modificats des de", si n'hi han i està marcat el check 
        'If ExisteixFitxersVB6ModificatsDesDe(dtDesde.Value) Then '''''AndAlso chkMigrarNomesModificats.Checked Then
        'CarregarListView(dtVB6, lstItemsFoundVB6, "")
        'End If

    End Sub

    Private Sub btnCleanMigrar_Click(sender As Object, e As EventArgs) Handles btnCleanMigrar.Click
        Dim projectePostMigracio As String = BuscarNomProjecteAmbCami() & "_PostMigracio.vbproj"

        '! 1.- Fitxer de traça
        If File.Exists(projectePostMigracio) Then
            Dim result1 As DialogResult = MessageBox.Show("Vols esborrar el fitxer de traça de migració del projecte?", "Clean Projecte", MessageBoxButtons.YesNo)
            If result1 = System.Windows.Forms.DialogResult.Yes Then
                File.Delete(projectePostMigracio)
            End If
        End If

        '! 2.- Fitxers 
        Dim result2 As DialogResult = MessageBox.Show("Vols esborrar els fitxers VB6 BACKUP? Si esborres, probablement no es podrà migrar (Wizard 2008) un altra projecte que depengui d'ell", "Clean Projecte", MessageBoxButtons.YesNo)

        If result2 = System.Windows.Forms.DialogResult.Yes Then
            EsborrarDirectori(cboDirectoryVB6_BACKUP.Text, True, False)
        End If
    End Sub

    ''' <summary>
    ''' Copia la carpeta VB6 seleccionada de l'arbre VB6 a M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP, copiant la mateixa estructura de carpetes
    ''' Es el primer pas dins del proces de migracio
    ''' </summary>
    Private Sub btnCrearBackups_Click(sendEer As Object, e As EventArgs) Handles btnCrearBackups.Click
        Cursor = Cursors.WaitCursor

        '! Busquem els fitxers d'altres "projectes" dels quals depèn
        For Each fitxer As String In FitxersQueDepenProjecte()
            If Not File.Exists(cboDirectoryVB6.Text.Replace(CARPETA_VB6, CARPETA_VB6_BACKUP) & "\" & fitxer) Then
                MessageBox.Show(String.Format("No pots fer la migració perque el projecte depèn del fitxer {0} que no existeix", fitxer))

                Cursor = Cursors.Default

                Exit Sub
            End If
        Next

        ' Obtenim la subcarpeta sobre la que volem fer copia
        Dim nomProjecte As String = cboDirectoryVB6.Text.Split("\")(2)
        Dim rutaSenseNomProjecte As String = String.Empty

        Try
            If cboDirectoryVB6.Text.IndexOf(nomProjecte) > 0 Then
                rutaSenseNomProjecte = cboDirectoryVB6.Text.Substring(cboDirectoryVB6.Text.IndexOf(nomProjecte))
            End If

            If rutaSenseNomProjecte = "" Then
                MessageBox.Show("No has seleccionat cap projecte per migrar")

                Cursor = Cursors.Default

                Exit Sub
            End If

            Dim calRecuperar As Boolean = False

            Dim rutaBACKUP As String = CARPETA_VB6_BACKUP & rutaSenseNomProjecte
            If Directory.Exists(rutaBACKUP) Then

                Try
                    '! 1.- "salvem" el fitxer d'"estat tracing migracio" abans d'esborrar tota la carpeta
                    If File.Exists(GetFitxerTracingMigracio()) Then
                        File.Copy(GetFitxerTracingMigracio(), CARPETA_TEMP & FITXER_TRACING_MIGRACIO, True)
                        calRecuperar = True
                    End If


                    '! si tornem a fer una nova migració NOMES dels fitxers VB6 modificats desde (carpeta .NET1), cal esborrar previament la carpeta .NET1
                    If chkMigrarNomesModificats.Checked Then
                        Dim carpetaBackup As String = CarpetaDotNET()
                        If Not String.IsNullOrEmpty(carpetaBackup) Then
                            Directory.Delete(carpetaBackup, True)
                        End If
                    End If

                    '! 2.- Eliminem la carpeta del BACKUP
                    EsborrarDirectori(rutaBACKUP, False, True)

                    '! 3.- Copiem fitxers VB6 a migrar a la carpeta de BACKUP del projecte
                    '!       Opcions:
                    '!           - "Fitxers modificats des de"
                    '!           - Tots els fitxers
                    If Not chkMigrarNomesModificats.Checked Then
                        '! Copiem de M:\MP a M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP\" & projecte
                        DirectoryCopy(cboDirectoryVB6.Text, rutaBACKUP, True, True)

                        WorkAround_TamanyButons(rutaBACKUP)

                    Else

                        Netejar("VB6")

                        '! Busquem el nombre fitxers que s'han modificat des de la ultima migracio del projecte
                        If ExisteixFitxersVB6ModificatsDesDe(dtDesde.Value) Then
                            numCoincidenciesVB6 = CarregarListView(dtVB6, lstItemsFoundVB6, "")

                            For Each d As DataRow In dtVB6.Rows
                                File.Copy(d.ItemArray(0).ToString, Path.Combine(rutaBACKUP, Path.GetFileName(d.ItemArray(0).ToString)), True)
                            Next

                            '! **************************************************************************************************
                            '! Construim un nou fitxer de projecte VB6 (vbp) AMB nomes els "fitxers que s'han modificats des de"
                            '!     A partir del fitxer vbp original construirem el nou vbp
                            Dim fitxerVBP As String = BuscarFitxersProjectesVB6(cboDirectoryVB6.Text)

                            If fitxerVBP = "" Then
                                MessageBox.Show("Aquest projecte no té associat cap fitxer de projecte VB6")

                                Cursor = Cursors.Default

                                Exit Sub
                            Else
                                Dim fitxerVBP_en_rutaBACKUP As String = Path.Combine(rutaBACKUP, Path.GetFileName(fitxerVBP))

                                File.Copy(fitxerVBP, fitxerVBP_en_rutaBACKUP, True)

                                '! 5.- "Construim" el vbp amb NOMES aquells fitxers que s'han modificat des de la última data de migració del projecte
                                ConstruirFitxerVBP_amb_llistafitxers_modificats_desde(fitxerVBP_en_rutaBACKUP)
                                ' **************************************************************************************************
                            End If
                        Else
                            MessageBox.Show("Amb la opció (NOMÉS fitxers modificats), aquest projecte no té res a migrar!")
                        End If

                    End If

                Catch ex As Exception
                    If ex.Message.Contains("MSSCCPRJ.SCC") Then  ' aquests fitxers no son errors realment
                        DirectoryCopy(cboDirectoryVB6.Text, rutaBACKUP, True, True)
                    Else
                        MsgBox(ex.Message, MsgBoxStyle.Information, "Error al borrar.")
                    End If

                    Cursor = Cursors.Default

                End Try

            Else
                '! Copiem de M:\MP a M:\Usuaris\Pere\BACKUP_LOCAL_FONTS_VB6\MP\" & projecte
                DirectoryCopy(cboDirectoryVB6.Text, rutaBACKUP, True, True)
            End If

            '! "recuperem" el fitxer d'"estat tracing migracio" despres d'esborrar tota la carpeta
            If File.Exists(CARPETA_TEMP & FITXER_TRACING_MIGRACIO) AndAlso calRecuperar Then
                File.Copy(CARPETA_TEMP & FITXER_TRACING_MIGRACIO, GetFitxerTracingMigracio(), True)
            End If

            '      
            RegistrarTrazaEstatMigracio(String.Format(FORMAT_FITXER_TRACING_MIGRACIO, Date.Now.ToString("dd/MM/yyyy HH:mm:ss"), E_EstatProcesMigracio.Executat_CopiaBackupVB6.ToString, "", "Copia VB6 a BACKUP"))

            Cursor = Cursors.Default

        Catch ex As Exception
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub TreeViewNET_MouseUp(sender As Object, e As MouseEventArgs) Handles TreeViewNET.MouseUp
        If e.Button = System.Windows.Forms.MouseButtons.Right Then
            ActualitzaMenu()

            ContextMenuStripProjectesNET.Show(TreeViewNET, e.Location)
        End If
    End Sub

    Private Sub TreeViewVB6_MouseUp(sender As Object, e As MouseEventArgs) Handles TreeViewVB6.MouseUp
        If e.Button = System.Windows.Forms.MouseButtons.Right Then
            ContextMenuStripProjectesVB6.Show(TreeViewVB6, e.Location)
        End If
    End Sub

    Private Sub lstItemsFoundVB6_MouseUp(sender As Object, e As MouseEventArgs) Handles lstItemsFoundVB6.MouseUp
        If e.Button = System.Windows.Forms.MouseButtons.Right Then
            ContextMenuStrip_FormsVB6.Show(lstItemsFoundVB6, e.Location)
        End If
    End Sub

    Private Sub lstItemsFoundNET_MouseUp(sender As Object, e As MouseEventArgs) Handles lstItemsFoundNet.MouseUp
        If e.Button = System.Windows.Forms.MouseButtons.Right Then
            ContextMenuStrip_FormsNET.Show(lstItemsFoundNet, e.Location)
        End If
    End Sub

    Private Sub lstItemsFoundVB6_2_MouseUp(sender As Object, e As MouseEventArgs) Handles lstItemsFoundVB6_2.MouseUp
        If e.Button = System.Windows.Forms.MouseButtons.Right Then
            ContextMenuStripModuls.Show(lstItemsFoundVB6_2, e.Location)
        End If
    End Sub

    Private Sub lstItemsFoundNET_2_MouseUp(sender As Object, e As MouseEventArgs) Handles lstItemsFoundNet_2.MouseUp
        If e.Button = System.Windows.Forms.MouseButtons.Right Then
            ContextMenuStripClasses.Show(lstItemsFoundNet_2, e.Location)
        End If
    End Sub

    '*************************************************************************************************
    ' ToolStripMenuItem/S
    '*************************************************************************************************

    Private Sub EliminarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EliminarFormsVB6_ToolStripMenuItem.Click
        If lstItemsFoundVB6.SelectedItems.Count = 0 Then Exit Sub

        lstItemsFoundVB6.SelectedItems(0).Remove()

        lblTrobats_1.Text = lstItemsFoundVB6.Items.Count

        '! refresquem
        CargarTreeViewVB6(cboDirectoryVB6.Text)
    End Sub

    Private Sub EliminarToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles EliminarFormsNet_ToolStripMenuItem.Click
        If lstItemsFoundNet.SelectedItems.Count = 0 Then Exit Sub

        For Each l In lstItemsFoundNet.SelectedItems
            l.Remove()
        Next

        lblTrobats_2.Text = lstItemsFoundNet.Items.Count

        '! refresquem
        CargarTreeViewNET(cboDirectoryNet.Text)
    End Sub

    Private Sub EliminarToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles EliminarModuls_ToolStripMenuItem.Click
        If lstItemsFoundVB6_2.SelectedItems.Count = 0 Then Exit Sub

        lstItemsFoundVB6_2.SelectedItems(0).Remove()

        lblTrobatModulsVB6.Text = lstItemsFoundVB6_2.Items.Count

        '! refresquem
        CargarTreeViewVB6(cboDirectoryVB6.Text)
    End Sub

    Private Sub EliminarToolStripMenuItem3_Click(sender As Object, e As EventArgs) Handles EliminarClasses_ToolStripMenuItem.Click
        If lstItemsFoundNet_2.SelectedItems.Count = 0 Then Exit Sub

        lstItemsFoundNet_2.SelectedItems(0).Remove()

        lblTrobatClassesNet.Text = lstItemsFoundNet_2.Items.Count

        '! refresquem
        CargarTreeViewNET(cboDirectoryNet.Text)
    End Sub

    ''' <summary>
    ''' Obrim VS (VB6) amb el nom del projecte seleccionat
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ToolStripMenuItemObrirProjecte_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemObrirProjecte.Click
        If TreeViewVB6.SelectedNode Is Nothing Then Exit Sub

        Dim fitxersProjecte As String() = Directory.GetFiles(TreeViewVB6.SelectedNode.FullPath, REGEX_VBP)

        If fitxersProjecte.Count = 0 Then Exit Sub

        '! Per resoldre el tema que hi hagi espais en el nom de la solucio       
        Dim nomVBP As String = caracterDobleCometa & fitxersProjecte(0) & caracterDobleCometa

        If fitxersProjecte.Count > 0 Then
            System.Diagnostics.Process.Start("C:\Program Files (x86)\Microsoft Visual Studio\VB98\VB6.exe", nomVBP)
        End If
    End Sub

    ''' <summary>
    ''' Executem : git clone https://pscglmsoft@bitbucket.org/pscglmsoft/gmsrepository_XXXXXXX.git M:\MP_Net\XXXXXX\gitTemp
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CloneProjecteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CloneProjecteToolStripMenuItem.Click
        If TreeViewNET.SelectedNode Is Nothing Then Exit Sub

        Cursor = Cursors.WaitCursor

        Try
            Dim nomRepositoryGit As String = ("gmsrepository_" & TreeViewNET.SelectedNode.FullPath.Substring(10).Replace("\", "")).Replace(" ", "")
            Dim carpetaCloneGitTemp As String = TreeViewNET.SelectedNode.FullPath & "\gitTemp"

            '! Per resoldre el tema que hi hagi espais en el nom de la carpeta          
            Dim carpetaCloneGitTempCometes As String = caracterDobleCometa & carpetaCloneGitTemp & caracterDobleCometa

            '! executem git/clone sobre el repository del projecte "seleccionat" guardant el codi en "carpetaCloneGitTemp"
            Dim ret As Integer = Shell("git clone https://pscglmsoft@bitbucket.org/pscglmsoft/" & nomRepositoryGit & ".git " & carpetaCloneGitTempCometes, AppWinStyle.Hide, True)

            If Directory.Exists(carpetaCloneGitTemp) Then

                '! copiem tots els fitxer i directoris de "carpetaCloneGitTemp" a la carpeta on hi ha d'haber el codi
                DirectoryCopy(carpetaCloneGitTemp, TreeViewNET.SelectedNode.FullPath, True, True)

                '! esborrem tota la carpeta "carpetaCloneGitTemp"
                EsborrarDirectori(carpetaCloneGitTemp, True, False)

                '! esborrem la carpeta temporal
                Directory.Delete(carpetaCloneGitTemp)

                '
                ActualitzaColorNode(TreeViewNET.SelectedNode)
            Else
                MessageBox.Show("Revisa el nom del repository Git")
            End If

        Catch ex As Exception
            MessageBox.Show("No es pot clonar")
        End Try

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Diferencies entre el projecte VB6 i el projecte NET del projecte seleccionat
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub DiferenciesProjectesMenuItem_Click(sender As Object, e As EventArgs) Handles DiferenciesProjectesNet_MenuItem.Click
        CalculaDiferenciesVB6_NET(True)
    End Sub

    ''' <summary>
    ''' Diferencies entre carpetes/fitxers corresponents al projecte seleccionat
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub DiferenciesCarpetesMenuItem_Click(sender As Object, e As EventArgs) Handles DiferenciesCarpetesNet_MenuItem.Click
        CalculaDiferenciesVB6_NET(False)
    End Sub

    ''' <summary>
    ''' Obrim VS 2017 amb la solucio del projecte seleccionat
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ObrirProjecteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ObrirProjecteNet_ToolStripMenuItem.Click
        If TreeViewNET.SelectedNode Is Nothing Then Exit Sub

        Try
            Dim fitxersSolucio As String() = Directory.GetFiles(TreeViewNET.SelectedNode.FullPath, "*.sln")

            If fitxersSolucio.Count = 0 Then
                MessageBox.Show(String.Format("La carpeta {0} no té ha cap fitxer de solució", TreeViewNET.SelectedNode.FullPath))
                Exit Sub
            End If

            '! Per resoldre el tema que hi hagi espais en el nom de la solucio            
            Dim nomSolucio As String = caracterDobleCometa & fitxersSolucio(0) & caracterDobleCometa

            If fitxersSolucio.Count > 0 Then
                Shell("C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\devenv.exe " & nomSolucio, AppWinStyle.MaximizedFocus)
                ''''Shell("C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\IDE\devenv.exe " & nomSolucio, AppWinStyle.MaximizedFocus)
            End If

        Catch ex As Exception
            MessageBox.Show(String.Format("El projecte {0} no té cap fitxer de solució", TreeViewNET.SelectedNode.FullPath))
        End Try
    End Sub

    ''' <summary>
    ''' Obre el NotePad per poder editar el fitxer de projecte Net (vbproj)
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub EditarProjecteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditarProjecteNet_ToolStripMenuItem.Click
        Dim fitxersProjecteNet As String() = Directory.GetFiles(TreeViewNET.SelectedNode.FullPath, "*.vbproj")

        If fitxersProjecteNet.Count = 0 Then Exit Sub

        '! Per resoldre el tema que hi hagi espais en el nom de la solucio       
        Dim nomFitxer As String = caracterDobleCometa & fitxersProjecteNet(0) & caracterDobleCometa

        If fitxersProjecteNet.Count > 0 Then
            Shell("C: \Program Files (x86)\Notepad++\notepad++.exe " & nomFitxer, AppWinStyle.MaximizedFocus)
        End If
    End Sub

    ''' <summary>
    ''' Obre el NotePad per poder editar el fitxer de projecte VB6 (vbp)
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub EditarProjecteToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles EditarProjecteVB6_ToolStripMenuItem1.Click
        Dim fitxersProjectesVB6 As String() = Directory.GetFiles(TreeViewVB6.SelectedNode.FullPath, REGEX_VBP)

        If fitxersProjectesVB6.Count = 0 Then Exit Sub

        '! Per resoldre el tema que hi hagi espais en el nom de la solucio        
        Dim nomFitxer As String = caracterDobleCometa & fitxersProjectesVB6(0) & caracterDobleCometa

        If fitxersProjectesVB6.Count > 0 Then
            Shell("C:\Program Files (x86)\Notepad++\notepad++.exe " & nomFitxer, AppWinStyle.MaximizedFocus)
        End If
    End Sub

    ''' <summary>
    ''' Obre la carpeta
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ObrirCarpetaProjecteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ObrirCarpetaProjecteToolStripMenuItem.Click
        If TreeViewNET.SelectedNode Is Nothing Then Exit Sub

        OpenFileDialog1.InitialDirectory = TreeViewNET.SelectedNode.FullPath.Replace("\\", "\")
        OpenFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
        OpenFileDialog1.FilterIndex = 2
        OpenFileDialog1.RestoreDirectory = True

        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            'fileName = OpenFileDialog1.FileName
        End If

    End Sub

    ''' <summary>
    ''' Projectes VB6 a partir del path seleccionat
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnProjectesVB6_Click(sender As Object, e As EventArgs) Handles btnProjectesVB6.Click
        Cursor = Cursors.WaitCursor

        NetejarTot()

        GetFitxersVBP_NET(cboDirectoryVB6.Text, REGEX_VBP, True, False)

        CarregarListView(dtVB6, lstItemsFoundVB6, String.Format("Projectes VB6 dins la carpeta {0}", cboDirectoryVB6.Text))

        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ReferenciesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReferenciesToolStripMenuItem.Click
        Dim fitxersSolucio As String() = Directory.GetFiles(cboDirectoryVB6.Text, REGEX_VBP)

        CargarComponentsVB6(fitxersSolucio(0))
    End Sub

    ''' <summary>
    ''' Carrega les "referencies" de components d'un projecte VB6
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ObtenirReferenciesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ObtenirReferencieVB6_ToolStripMenuItem.Click
        CargarComponentsVB6(Directory.GetFiles(TreeViewVB6.SelectedNode.FullPath, REGEX_VBP)(0))
    End Sub

    ''' <summary>
    ''' Elimina tots el fitxers del projecte NET seleccionat
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub EliminarProjecteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EliminarProjecteNet_ToolStripMenuItem.Click
        Dim result As DialogResult = MessageBox.Show(String.Format("Vols esborrar els fitxers NET del projecte {0} ? SI ESBORRES ES PERDRAN ELS CANVIS FETS DESPRES DEL COMMIT AL GIT!!!!", TreeViewNET.SelectedNode.FullPath), "Esborrar fitxers NET (MP_Net)", MessageBoxButtons.YesNo)

        If result = System.Windows.Forms.DialogResult.Yes Then
            EsborrarDirectori(TreeViewNET.SelectedNode.FullPath, False, False)  ' no esborrem la carpeta, NOMES els fitxers de totes les subcarpetes

            ActualitzaColorNode(TreeViewNET.SelectedNode)
        End If
    End Sub

    Private Sub SeleccionarFormsNet_ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SeleccionarFormsNet_ToolStripMenuItem.Click
        For Each l As ListViewItem In lstItemsFoundNet.SelectedItems
            l.Checked = True
        Next
    End Sub

    Private Sub btnCopiarVB6_to_NET_Click(sender As Object, e As EventArgs) Handles btnCopiarBackupNet_to_Net.Click
        Dim carpeta = CarpetaDotNET()

        If carpeta.Contains("\Common") OrElse carpeta.Contains("\FormsComuns") OrElse carpeta.Contains("\Controls") Then
            MessageBox.Show(String.Format("La carpeta {0} no farem copia perquè cambiariem modificacions", carpeta))

            Exit Sub
        End If

        If chkMigrarNomesModificats.Checked Then
            MessageBox.Show("Copiarem només els migrats.Si vols copiar tots els fitxers, desmarca 'Migrar NOMES modificats'")
        Else
            MessageBox.Show("Copiarem TOTS els fitxers.Si vols copiar NOMÉS els fitxers modificats, marca 'Migrar NOMES modificats'")
        End If

        DirectoryCopy(carpeta, cboDirectoryNet.Text, True, False)
    End Sub

    ''' <summary>
    ''' "Refresca" l'estat de tots els nodes de l'arbre NET segons les diferencies entre projectes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnDiferenciesProjectes_Click(sender As Object, e As EventArgs) Handles btnDiferenciesProjectes.Click
        Cursor = Cursors.WaitCursor
        ActualitzaColorsTreeViewNET(TreeViewNET.TopNode, True)
        Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' "Refresca" l'estat de tots els nodes de l'arbre NET segons les diferencies entre carpetes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnDiferenciesCarpetes_Click(sender As Object, e As EventArgs) Handles btnDiferenciesCarpetes.Click
        Cursor = Cursors.WaitCursor
        ActualitzaColorsTreeViewNET(TreeViewNET.TopNode, False)
        Cursor = Cursors.Default
    End Sub

    Private Sub ChkSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelectAll.CheckedChanged
        For i = 0 To lstItemsFoundNet.Items.Count - 1
            If EsObsoletFitxer(lstItemsFoundNet.Items(i).Text) Then
                lstItemsFoundNet.Items(i).Checked = False
            Else
                lstItemsFoundNet.Items(i).Checked = chkSelectAll.Checked
            End If
        Next

        For i = 0 To lstItemsFoundNet_2.Items.Count - 1
            If EsObsoletFitxer(lstItemsFoundNet_2.Items(i).Text) Then
                lstItemsFoundNet_2.Items(i).Checked = False
            Else
                lstItemsFoundNet_2.Items(i).Checked = chkSelectAll.Checked
            End If
        Next
    End Sub

    Private Sub chkNomesProjecte_CheckedChanged(sender As Object, e As EventArgs) Handles chkNomesProjecte.CheckedChanged
        Dim trobats_1 As Integer = 0
        Dim trobats_2 As Integer = 0

        For i = 0 To lstItemsFoundNet.Items.Count - 1
            If EsObsoletFitxer(lstItemsFoundNet.Items(i).Text) Then
                lstItemsFoundNet.Items(i).Checked = False
            Else
                If lstItemsFoundNet.Items(i).Text.Contains("..") AndAlso chkNomesProjecte.Checked Then
                    lstItemsFoundNet.Items(i).Checked = False
                    Continue For
                End If

                trobats_1 = trobats_1 + 1
                lstItemsFoundNet.Items(i).Checked = chkSelectAll.Checked
            End If
        Next

        For i = 0 To lstItemsFoundNet_2.Items.Count - 1
            If EsObsoletFitxer(lstItemsFoundNet_2.Items(i).Text) Then
                lstItemsFoundNet_2.Items(i).Checked = False
            Else

                If lstItemsFoundNet_2.Items(i).Text.Contains("..") AndAlso chkNomesProjecte.Checked Then
                    lstItemsFoundNet_2.Items(i).Checked = False
                    Continue For
                End If

                trobats_2 = trobats_2 + 1
                lstItemsFoundNet_2.Items(i).Checked = chkSelectAll.Checked
            End If
        Next

        lblTrobats_2.Text = trobats_1
        lblTrobatClassesNet.Text = trobats_2
    End Sub

    'Private Sub chkInclouFormsNous_CheckedChanged(sender As Object, e As EventArgs) Handles chkInclouFormsNous.CheckedChanged
    '    Cursor = Cursors.WaitCursor
    '    'Netejar("NET")
    '    'CargarTreeViewNET(cboDirectoryNet.Text)

    '    lstItemsFoundNet.Refresh()

    '    Cursor = Cursors.Default
    'End Sub

    Private Sub chkInclouProjectesNet_Nous_CheckedChanged(sender As Object, e As EventArgs) Handles chkInclouProjectesNet_Nous.CheckedChanged
        Cursor = Cursors.WaitCursor
        Netejar("NET")
        CargarTreeViewNET(cboDirectoryNet.Text)
        Cursor = Cursors.Default
    End Sub

    Private Sub chkMigrarNomesModificats_CheckedChanged(sender As Object, e As EventArgs) Handles chkMigrarNomesModificats.CheckedChanged
        If Not chkMigrarNomesModificats.IsHandleCreated Then Exit Sub

        If Not chkMigrarNomesModificats.Checked Then Netejar("VB6")  ' volem migrar TOTS els fitxers

        Dim fitxerTracingMigracio As String = GetFitxerTracingMigracio()

        If Not File.Exists(fitxerTracingMigracio) Then Exit Sub

        '! recuperem la ultima linia que indica en quin estat està el "procés de migracio" del projecte
        Dim ultimaLinia As String = UltimaLiniaFitxerTracing(fitxerTracingMigracio)

        '! Estat intermitgos. En aquests "estats", no podem tornar a iniciar una nova migració
        If EstatProcesMigracio(ultimaLinia) = E_EstatProcesMigracio.Executat_CopiaBackupVB6 OrElse
            EstatProcesMigracio(ultimaLinia) = E_EstatProcesMigracio.Executat_Wizard2008 OrElse
            EstatProcesMigracio(ultimaLinia) = E_EstatProcesMigracio.Executat_PostMigracio Then

            btnCrearBackups.Enabled = False  ' no podem tornar a iniciar una nova migració

        ElseIf EstatProcesMigracio(ultimaLinia) = E_EstatProcesMigracio.Executat_MigratsFitxers Then
            btnCrearBackups.Enabled = EsMigrableNovament()
        End If

        ' 
        NetejarTot()

        If chkMigrarNomesModificats.Checked Then
            CarregaFitxersVB6ModificatsDesde()
        End If

        ActualitzaButonsEstatProcesMigracio()

    End Sub

    Private Sub WinMergeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WinMergeToolStripMenuItem.Click
        If TreeViewNET.SelectedNode Is Nothing Then Exit Sub

        Try
            Dim carpeta As String = CarpetaDotNET()

            If carpeta = "" Then
                MsgBox("No podem comparar perque no hi ha cap carpeta Backup NET", MsgBoxStyle.Information, "WinMerge")

                Exit Sub
            End If

            '! Per resoldre el tema que hi hagi espais en el nom de la solucio            
            Dim fitxer_BackupC As String = caracterDobleCometa & carpeta & caracterDobleCometa


            Dim fitxerC As String = caracterDobleCometa & TreeViewNET.SelectedNode.FullPath & caracterDobleCometa

            Shell("C:\Program Files (x86)\WinMerge\WinMergeU.exe " & fitxer_BackupC & " " & fitxerC, AppWinStyle.MaximizedFocus)

        Catch ex As Exception
            MessageBox.Show(String.Format("El projecte {0} no té cap fitxer de solució", TreeViewNET.SelectedNode.FullPath))
        End Try
    End Sub

    Private Sub UpgradeSupportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UpgradeSupportToolStripMenuItem.Click
        If TreeViewNET.SelectedNode Is Nothing Then Exit Sub

        Try
            Dim fitxer As String = Directory.GetDirectories(cboDirectoryVB6_BACKUP.Text, "*.NET")(0) & "\_UpgradeReport.htm"

            '! Per resoldre el tema que hi hagi espais en el nom de la solucio            
            Dim fitxer2 As String = caracterDobleCometa & fitxer & caracterDobleCometa

            System.Diagnostics.Process.Start(fitxer2)

        Catch ex As Exception
            MessageBox.Show("El fitxer HTML UpgradeSupport no es pot obrir")
        End Try
    End Sub


    ''' <summary>
    ''' Copia el fitxer .Net de la carpeta BACKUP a la carpeta M:\MP_Net
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CopiarBackupAMPNetToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiarBackupAMPNetToolStripMenuItem.Click
        If lstItemsFoundVB6.SelectedItems.Count = 0 Then Exit Sub

        Try
            Dim fitxerVB As String = lstItemsFoundVB6.SelectedItems(0).Text & EXTENSIO_VB
            Dim fitxerDesigner As String = Path.GetFileNameWithoutExtension(lstItemsFoundVB6.SelectedItems(0).Text) & EXTENSIO_DESIGNER
            Dim fitxerResX As String = Path.GetFileNameWithoutExtension(lstItemsFoundVB6.SelectedItems(0).Text) & EXTENSIO_RESX

            Dim carpeta As String = CarpetaDotNET()
            If carpeta = "" Then
                MsgBox("El fitxer no existeix en la carpeta de Backup", MsgBoxStyle.Information, "Copiar fitxer Net de bAckup a MP:\MP_Net")

                Exit Sub
            End If

            File.Copy(carpeta & "\" & fitxerVB, cboDirectoryNet.Text & "\" & fitxerVB, True)
            File.Copy(carpeta & "\" & fitxerDesigner, cboDirectoryNet.Text & "\" & fitxerDesigner, True)
            File.Copy(carpeta & "\" & fitxerResX, cboDirectoryNet.Text & "\" & fitxerResX, True)

        Catch ex As Exception
            MsgBox("El fitxer no existeix en la carpeta de Backup", MsgBoxStyle.Information, "Copiar fitxer Net de bAckup a MP:\MP_Net")
        End Try

    End Sub

    ' ****************************************************************************************************************************************************

    Private Sub lstItemsFoundVB6_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles lstItemsFoundVB6.ItemDrag
        If sender Is Nothing OrElse Not TypeOf sender Is ListView Then Exit Sub
        With CType(sender, ListView)
            .DoDragDrop(e.Item, DragDropEffects.Move)
        End With
    End Sub

    Private Sub ListView_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles lstItemsFoundNet.ItemDrag
        If sender Is Nothing OrElse Not TypeOf sender Is ListView Then Exit Sub
        With CType(sender, ListView)
            .DoDragDrop(e.Item, DragDropEffects.Move)
        End With
    End Sub
    Private Sub ListView_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lstItemsFoundNet.DragEnter
        If sender Is Nothing OrElse Not TypeOf sender Is ListView Then Exit Sub
        'If this is a listview item then allow the drag
        If e.Data.GetDataPresent(GetType(ListViewItem)) Then
            e.Effect = DragDropEffects.Move
        End If
    End Sub
    Private Sub ListView_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lstItemsFoundNet.DragDrop
        If sender Is Nothing OrElse Not TypeOf sender Is ListView Then Exit Sub

        '! només podem arrossegar si tenim marcat el check "chkMigrarNomesModificats"
        If chkMigrarNomesModificats.Checked = False Then Exit Sub

        '! Remove the item from the current listview and drop it in the new listview
        With CType(sender, ListView)
            If e.Data.GetDataPresent(GetType(ListViewItem)) Then
                Dim draggedItem As ListViewItem = CType(e.Data.GetData(GetType(ListViewItem)), ListViewItem)
                draggedItem.ListView.Items.Remove(draggedItem)

                For Each fitxer As String In GetFitxersNetByFitxerVB6(draggedItem.Text)
                    '! afegim a la listView els fitxer *.vb i *.designer.vb
                    If fitxer.Contains(EXTENSIO_VB) Then
                        .Items.Add(fitxer)
                    End If

                    '! copiem el fitxer de la carpeta .NET1 ("migracio nomes modificats desde") a .NET (tots)
                    Dim carpetaBackup As String = CarpetaDotNET()
                    If String.IsNullOrEmpty(carpetaBackup) Then
                        MessageBox.Show("No existeix la carpeta NET1.Hauries de fer una nova migració")
                    Else
                        Dim fitxerBackupModificats As String = CarpetaDotNET() & "\" & Path.GetFileName(fitxer) ' NET1
                        Dim fitxerBackupTots As String = Replace(fitxerBackupModificats, ".NET1", ".NET")
                        File.Copy(fitxerBackupModificats, fitxerBackupTots, True)

                        '! copiem fisicament des de la carpeta de Backup (.NET1) a M:\MP_NET
                        File.Copy(fitxerBackupModificats, cboDirectoryNet.Text & "\" & Path.GetFileName(fitxer), True)

                    End If

                Next
            End If
        End With
    End Sub

    ''' <summary>
    ''' Retorna els fitxers .Net corresponents a un fitxer VB6
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <returns></returns>
    Private Function GetFitxersNetByFitxerVB6(ByVal fitxer As String) As List(Of String)
        Dim llistaFitxers As New List(Of String)

        Dim fitxerTemp As String

        If chkMigrarBackup.Checked Then
            fitxerTemp = CarpetaDotNET() & "\" & Path.GetFileName(fitxer)
        Else
            fitxerTemp = Replace(fitxer, CARPETA_VB6, CARPETA_NET)
        End If

        If fitxerTemp.Contains(EXTENSIO_FRM) Then
            llistaFitxers.Add(Replace(fitxerTemp, EXTENSIO_FRM, EXTENSIO_VB))
            llistaFitxers.Add(Replace(fitxerTemp, EXTENSIO_FRM, EXTENSIO_RESX))
            llistaFitxers.Add(Replace(fitxerTemp, EXTENSIO_FRM, EXTENSIO_DESIGNER))

        ElseIf fitxerTemp.Contains(EXTENSIO_CLS) Then
            llistaFitxers.Add(Replace(fitxerTemp, EXTENSIO_CLS, EXTENSIO_VB))

        ElseIf fitxerTemp.Contains(EXTENSIO_BAS) Then
            llistaFitxers.Add(Replace(fitxerTemp, EXTENSIO_BAS, EXTENSIO_VB))

        ElseIf fitxerTemp.Contains(EXTENSIO_CTL) Then
            llistaFitxers.Add(Replace(fitxerTemp, EXTENSIO_CTL, EXTENSIO_VB))
        End If

        Return llistaFitxers
    End Function

    Private Sub chkMigrarBackup_CheckedChanged(sender As Object, e As EventArgs) Handles chkMigrarBackup.CheckedChanged
        If Not Me.IsHandleCreated Then Exit Sub

        '! 1.- Només validem els fitxers designers si estem en l'entorn NET (producció)
        '! 2.- Només validem els fitxers vb si estem en l'entorn NET Backup

        If chkMigrarBackup.Checked Then
            chkMigrarDesigner.Checked = False
            chkMigrarCodi.Checked = True
            chkMigrarCodi.Enabled = True
            chkMigrarDesigner.Enabled = False
            chkMigrarDesigner.Checked = False
        Else
            chkMigrarCodi.Checked = False
            chkMigrarCodi.Enabled = False
            chkMigrarDesigner.Enabled = True
            chkMigrarDesigner.Checked = True
        End If

        BuscarFitxersNet()
    End Sub

    Private Sub dtDesde_ValueChanged(sender As Object, e As EventArgs) Handles dtDesde.ValueChanged
        If dtDesde.Value.ToShortDateString = "01/01/2000" Then Exit Sub

        CarregaFitxersVB6ModificatsDesde()
    End Sub

    Private Sub btnPreWizard_Click(sender As Object, e As EventArgs) Handles btnPreWizard.Click
        Dim rutaSenseNomProjecte As String = String.Empty

        Dim nomProjecte As String = cboDirectoryVB6.Text.Split("\")(2)

        If cboDirectoryVB6.Text.IndexOf(nomProjecte) > 0 Then
            rutaSenseNomProjecte = cboDirectoryVB6.Text.Substring(cboDirectoryVB6.Text.IndexOf(nomProjecte))
        End If

        If rutaSenseNomProjecte = "" Then
            MessageBox.Show("No has seleccionat cap projecte per migrar")
            Exit Sub
        End If

        Dim rutaBACKUP As String = CARPETA_VB6_BACKUP & rutaSenseNomProjecte

        WorkAround_TamanyButons(rutaBACKUP)
    End Sub

    '''' <summary>
    ''''    ''' Busca tots els fitxers (frm, bas, cls, ctl) que no existeix en cap projecte GMS
    '''' </summary>
    'Private Sub CalculaFitxersNoMigrables()
    '    Cursor.Current = Cursors.WaitCursor

    '    TabControl1.SelectedTab = TabControl1.TabPages(0)

    '    lblProcessant.Text = "Calculant fitxers NO migrables ...."

    '    Dim carpetes As List(Of String) = Directory.GetDirectories(CARPETA_VB6).ToList

    '    carpetes.Add(cboDirectoryVB6.Text)

    '    lvWarningsFitxersNoMigrables.Items.Clear()

    '    '
    '    FormatListViewWarnings(lvWarningsFitxersNoMigrables, New List(Of String)({"#", "Fitxer"}), New List(Of Integer)({10, 500}))

    '    For Each d As String In carpetes
    '        For Each fitxer As String In Directory.GetFiles(d)
    '            If fitxer.Contains(EXTENSIO_FRM) OrElse fitxer.Contains(EXTENSIO_BAS) OrElse fitxer.Contains(EXTENSIO_CTL) OrElse fitxer.Contains(EXTENSIO_CLS) Then

    '                If Not ExisteixFitxerEnAlgunProjecte(fitxer) Then
    '                    Dim lviWarning As New ListViewItem()

    '                    lviWarning.SubItems.Add(fitxer)

    '                    lvWarningsFitxersNoMigrables.Items.Add(lviWarning)

    '                    lblProcessant.Text = String.Format("Fitxer NO migrable : {0}", fitxer)
    '                    lblProcessant.Refresh()
    '                End If

    '            End If

    '        Next
    '    Next

    '    lblProcessant.Text = "Fitxers NO migrables calculats"

    '    Cursor.Current = Cursors.Default
    'End Sub

    ''' <summary>
    ''' Busca en TOTS els projectes GMS si el fitxer existeix (CARPETA_VB6)
    ''' </summary>
    ''' <returns></returns>
    Private Function ExisteixFitxerEnAlgunProjecte(ByVal fitxer As String) As Boolean
        Dim existeix As Boolean = False

        For Each carpeta As String In Directory.GetDirectories(CARPETA_VB6)
            existeix = CalMigrar(fitxer, carpeta)

            If existeix Then Exit For
        Next

        If Not existeix Then
            Debug.WriteLine(String.Format("Fitxer : {0} no migrable", fitxer))
        End If

        Return existeix
    End Function

    Private Sub btnTestos_Click(sender As Object, e As EventArgs) Handles btnTestos.Click
        'If chkMigrarBackup.Checked Then
        '    'MsgBox("Els testos unitaris només son aplicables a M:\MP_Net")
        '    'Exit Sub

        '    frmTestos.Text = "Testos Unitaris - " & cboDirectoryVB6_BACKUP.Text
        '    frmTestos.lv = lstItemsFoundNet.CheckedItems
        '    frmTestos.pathProjecte = cboDirectoryVB6_BACKUP.Text

        'Else

        MessageBox.Show("Aplicarem testos sobre els fitxers seleccionats de la part Producció")
        frmTestos.Text = "Testos Unitaris - " & cboDirectoryNet.Text
        frmTestos.lv = lstItemsFoundNet.CheckedItems
        frmTestos.pathProjecte = cboDirectoryNet.Text

        'End If

        frmTestos.ShowDialog()
    End Sub

    Private Sub btnUpgrades_Click(sender As Object, e As EventArgs)
        TabControl1.SelectedTab = TabControl1.TabPages(1)

        CalculaUpgrades()
    End Sub

    'Private Sub lvWarnings_ColumnClick(sender As Object, e As ColumnClickEventArgs)
    '    If lvWarningsFitxersNoMigrables.Sorting = SortOrder.Descending Then
    '        lvWarningsFitxersNoMigrables.Sorting = SortOrder.Ascending
    '    Else
    '        lvWarningsFitxersNoMigrables.Sorting = SortOrder.Descending
    '    End If
    'End Sub

    Dim sortColumn As Integer = -1
    Private Sub lvTwipsPixels_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles lvTwipsPixels.ColumnClick

        ' If current column is not the previously clicked column
        ' Add
        If e.Column <> sortColumn Then

            ' Set the sort column to the new column
            sortColumn = e.Column

            'Default to ascending sort order
            lvTwipsPixels.Sorting = SortOrder.Ascending

        Else

            'Flip the sort order
            If lvTwipsPixels.Sorting = SortOrder.Ascending Then
                lvTwipsPixels.Sorting = SortOrder.Descending
            Else
                lvTwipsPixels.Sorting = SortOrder.Ascending
            End If
        End If

        'Set the ListviewItemSorter property to a new ListviewItemComparer object
        Me.lvTwipsPixels.ListViewItemSorter = New ListViewItemComparer(e.Column, lvTwipsPixels.Sorting)

        ' Call the sort method to manually sort
        lvTwipsPixels.Sort()

    End Sub

    Private Sub treeListViewUpgrades_BeforeLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeListViewBeforeLabelEditEventArgs) Handles treeListViewUpgrades.BeforeLabelEdit
        If e.Item.ImageIndex < 1 OrElse e.Item.ImageIndex > 2 Then e.ColumnIndex = 0

        If e.ColumnIndex = 1 Then
            Dim combobox As ComboBox = New ComboBox()
            combobox.Items.AddRange(New String() {"New value 1", "New value 2", "New value 3"})
            e.Editor = combobox
        End If
    End Sub


    Private Sub treeListViewUpgrades_BeforeCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeListViewCancelEventArgs) Handles treeListViewUpgrades.BeforeCollapse
        If e.Item.ImageIndex = 2 Then e.Item.ImageIndex = 1
    End Sub


    Private Sub treeListViewUpgrades_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeListViewCancelEventArgs) Handles treeListViewUpgrades.BeforeExpand
        If e.Item.ImageIndex = 1 Then e.Item.ImageIndex = 2
    End Sub

    Private collapse As Boolean = True
    Private Sub chkCollapaseTreeListView_CheckedChanged(sender As Object, e As EventArgs)
        If collapse Then
            treeListViewUpgrades.CollapseAll()
        Else
            treeListViewUpgrades.ExpandAll()
        End If

        collapse = Not collapse
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="carpeta"></param>
    ''' <returns></returns>
    Private Function EsProjecte_O_Llibreria(ByVal carpeta As String) As Boolean
        If Directory.GetFiles(carpeta, REGEX_VBP).Count = 0 Then Return False
        Return True
    End Function

    Private Sub TabControl1_Selected(sender As Object, e As TabControlEventArgs) Handles TabControl1.Selected
        'If e.TabPageIndex = -1 Then

        '    If lvWarningsFitxersNoMigrables.Items.Count = 0 Then
        '        'CalculaFitxersNoMigrables()   NO FUNCIONA
        '    End If

        '    lblProcessant.Text = ""

        'Else
        If e.TabPageIndex = 0 Then

            If calRecarregarUpgrades Then
                CalculaUpgrades()
            End If

            calRecarregarUpgrades = False

            RefrescarProces("Upgrade", treeListViewUpgrades.Items.Count, "", "")

        ElseIf e.TabPageIndex = 1 Then

            'If calRecarregarNomMetodeEvent_Igual_NomEventHandler Then
            CalculaWorkAround_NomMetodeEvent_Igual_NomEventHandler()
            'End If

            calRecarregarNomMetodeEvent_Igual_NomEventHandler = False

            RefrescarProces("NomMetodeEvent_Igual_NomEventHandler", lvWarningsWorkAround.Items.Count, "", "")

            'ElseIf e.TabPageIndex = -1 Then  ' esta eliminada

            '    If lvWorkAroundPropertyCanOnly.Items.Count = 0 Then
            '        CalculaWorkAround_Property_can_only_be_set_to_Nothing()
            '    End If

        ElseIf e.TabPageIndex = 2 Then   ' Migracio Twips-Pixels

            ''''If calRecarregarLiniesTwipsPixels Then
            CalculaWorkAround_LiniesTwipsPixels()
            '''''End If

            calRecarregarLiniesTwipsPixels = False

            RefrescarProces("TwipsPixels", lvTwipsPixels.Items.Count, "", "")

        ElseIf e.TabPageIndex = 3 Then

            If calRecarregarAround_AsObject Then
                CalculaWorkAround_AsObject()
            End If

            calRecarregarAround_AsObject = False

            RefrescarProces("Total : AsObject", lviAsObject.Items.Count, "", "")

        ElseIf e.TabPageIndex = 4 Then     ' Fonts TrueType

            If calRecarregarFontsTrueType Then
                CalculaWorkAround_FontsTrueType()
            End If

            calRecarregarFontsTrueType = False

            RefrescarProces("Total : Fonts TrueType", lviFontsTrueType.Items.Count, "", "")

        ElseIf e.TabPageIndex = 5 Then     ' TreeView dins AxGroupBox

            If calRecarregarTreeViewDinsAxGroupBox Then
                CalculaWorkAround_TreeViewDinsAxGroupBox()
            End If

            calRecarregarTreeViewDinsAxGroupBox = False

            RefrescarProces("Total : TreeViewDinsAxGroupBox", lviTreeViewAxGroupBox.Items.Count, "", "")

        ElseIf e.TabPageIndex = 6 Then     ' formularis si estan redissenyat o no des de l'ultim proces de migracio

            CalculaFormsRedisseny()

            RefrescarProces("Total : Forms Redisseny", lviFormsRedisseny.Items.Count, "", "")

        ElseIf e.TabPageIndex = 7 Then

            'If lviArquitectura.Items.Count = 0 Then
            CalculaWorkAroundsArquitectura("")
            'End If

            RefrescarProces("Total : Arquitectura", lviArquitectura.Items.Count, "", "")

        ElseIf e.TabPageIndex = 8 Then

            'If lviBugs.Items.Count = 0 Then
            CalculaWorkAroundsBugs()
            'End If

            RefrescarProces("Total : Bugs", lviBugs.Items.Count, "", "")

        ElseIf e.TabPageIndex = 9 Then
            CalculaWorkAround_Coleccio_1_0()

            RefrescarProces("Total : Coleccio_1_0", lviColeccio1_0.Items.Count, "", "")

        ElseIf e.TabPageIndex = 10 Then
            CalculaWorkAround_Formularis_Sense_Caption()

            RefrescarProces("Total : Formularis_Sense_Caption", lviFormularis_Sense_Caption.Items.Count, "", "")

        ElseIf e.TabPageIndex = 11 Then
            CalculaWorkAround_NomClasseUserControl_igual_NomInstancia()

            RefrescarProces("Total : NomClasseUserControl_igual_NomInstancia", lviNomClasseUserControl_igual_nomInstancia.Items.Count, "", "")

        ElseIf e.TabPageIndex = 12 Then
            CalculaWorkAround_FormLoad()

            RefrescarProces("Total : FormLoad", lvFormLoad.Items.Count, "", "")

        ElseIf e.TabPageIndex = 13 Then      '! errors degut a formularis mal "construits" en VB6.Exemple : controls que no son TabPage i "pengen" de TabControlS

            Cursor = Cursors.WaitCursor

            FormatListViewErrorsMigracio(lvErrorsMigracio, New List(Of String)({"#", "Tipus", "Fitxer", "Control"}), New List(Of Integer)({20, 250, 250, 150}))

            RefrescarProces("Total : Errors migracio", lvErrorsMigracio.Items.Count, "", "")

            Cursor = Cursors.Default
        End If

    End Sub

    Private Sub chkTots_CheckedChanged(sender As Object, e As EventArgs) Handles chkTots.CheckedChanged
        If flagCheckboxes = True Then Exit Sub
        If Not Me.IsHandleCreated Then Exit Sub  ' s'està inicialitzant el formulari.Raise l'event

        If chkTots.Checked Then
            flagCheckboxes = True
            chkAltres.Checked = True
            chkWarning.Checked = True
            chkResolt.Checked = True
            chkCritic.Checked = True
            flagCheckboxes = False

            CalculaUpgrades()
        Else
            Dim compt As Integer = treeListViewUpgrades.Items.Count
            For i = compt - 1 To 0 Step -1

                treeListViewUpgrades.Items.RemoveAt(i)

                comptadorLiniesTwipsPixels = comptadorLiniesTwipsPixels - 1

                lblProcessant.Text = String.Format("Total linies amb GetTwipsToPixels/GetPixelsToTwips : {0}", comptadorLiniesTwipsPixels)
            Next

        End If
    End Sub

    Private Sub chkCritic_CheckedChanged(sender As Object, e As EventArgs) Handles chkCritic.CheckedChanged
        If flagCheckboxes = True Then Exit Sub
        If Not Me.IsHandleCreated Then Exit Sub  ' s'està inicialitzant el formulari.Raise l'event

        If chkCritic.Checked Then
            CalculaUpgrades()
        Else
            flagCheckboxes = True
            chkTots.Checked = False
            flagCheckboxes = False

            EliminarUpgradeSegonsCriticitat("OrangeRed")
        End If
    End Sub

    Private Sub chkAltres_CheckedChanged(sender As Object, e As EventArgs) Handles chkAltres.CheckedChanged
        If flagCheckboxes = True Then Exit Sub
        If Not Me.IsHandleCreated Then Exit Sub  ' s'està inicialitzant el formulari.Raise l'event

        If chkAltres.Checked Then
            CalculaUpgrades()
        Else
            flagCheckboxes = True
            chkTots.Checked = False
            flagCheckboxes = False

            EliminarUpgradeSegonsCriticitat("PaleVioletRed")
        End If
    End Sub

    Private Sub chkWarning_CheckedChanged(sender As Object, e As EventArgs) Handles chkWarning.CheckedChanged
        If flagCheckboxes = True Then Exit Sub
        If Not Me.IsHandleCreated Then Exit Sub  '! s'està inicialitzant el formulari.Raise l'event

        If chkWarning.Checked Then
            CalculaUpgrades()
        Else
            flagCheckboxes = True
            chkTots.Checked = False
            flagCheckboxes = False

            EliminarUpgradeSegonsCriticitat("Orange")
        End If
    End Sub

    Private Sub chkResolt_CheckedChanged(sender As Object, e As EventArgs) Handles chkResolt.CheckedChanged
        If flagCheckboxes = True Then Exit Sub
        If Not Me.IsHandleCreated Then Exit Sub  ' s'està inicialitzant el formulari.Raise l'event

        If chkResolt.Checked Then
            CalculaUpgrades()
        Else
            flagCheckboxes = True
            chkTots.Checked = False
            flagCheckboxes = False

            EliminarUpgradeSegonsCriticitat("LightGreen")
        End If
    End Sub

    Private Sub optTwipsPixelsTots_CheckedChanged(sender As Object, e As EventArgs) Handles optTwipsPixelsTots.CheckedChanged
        If flagCheckboxes = True Then Exit Sub

        If optTwipsPixelsTots.Checked Then
            seleccionarTwipsPixelsTots = True
            seleccionarTwipsPixelsInCorrectes = False
            seleccionarTwipsPixelsInCorrectes = False
            seleccionarTwipsPixelsCorrectes = False
            lvTwipsPixels.Clear()

            lstPatronsRegexTwipsPixelsProcessats.Clear()

            '! netegem comptadors
            comptadorLiniesTwipsPixels = 0
            CalculaWorkAround_LiniesTwipsPixels()
        End If
    End Sub

    Private Sub optTwipsPixelsCorrectes_CheckedChanged(sender As Object, e As EventArgs) Handles optTwipsPixelsCorrectes.CheckedChanged
        If optTwipsPixelsCorrectes.Checked Then
            seleccionarTwipsPixelsTots = False
            seleccionarTwipsPixelsNomesNumeros = False
            seleccionarTwipsPixelsInCorrectes = False
            seleccionarTwipsPixelsCorrectes = True
            lvTwipsPixels.Clear()

            lstPatronsRegexTwipsPixelsProcessats.Clear()

            'netegem comptadors
            comptadorLiniesTwipsPixels = 0
            CalculaWorkAround_LiniesTwipsPixels()
        End If
    End Sub

    Private Sub optTwipsPixelsIncorrectes_CheckedChanged(sender As Object, e As EventArgs) Handles optTwipsPixelsIncorrectes.CheckedChanged
        If optTwipsPixelsIncorrectes.Checked Then
            seleccionarTwipsPixelsTots = False
            seleccionarTwipsPixelsCorrectes = False
            seleccionarTwipsPixelsNomesNumeros = False
            seleccionarTwipsPixelsInCorrectes = True
            lvTwipsPixels.Clear()

            lstPatronsRegexTwipsPixelsProcessats.Clear()

            '! netegem comptadors
            comptadorLiniesTwipsPixels = 0
            CalculaWorkAround_LiniesTwipsPixels()
        End If
    End Sub

    Private Sub optTwipsPixelsNomesNumeros_CheckedChanged(sender As Object, e As EventArgs) Handles optTwipsPixelsNomesNumeros.CheckedChanged
        If optTwipsPixelsNomesNumeros.Checked Then
            seleccionarTwipsPixelsTots = False
            seleccionarTwipsPixelsCorrectes = False
            seleccionarTwipsPixelsInCorrectes = False
            seleccionarTwipsPixelsNomesNumeros = True
            lvTwipsPixels.Clear()

            lstPatronsRegexTwipsPixelsProcessats.Clear()

            '! netegem comptadors
            comptadorLiniesTwipsPixels = 0
            CalculaWorkAround_LiniesTwipsPixels()
        End If
    End Sub

    Private Sub lvWarningsWorkAround_MouseDown(sender As Object, e As MouseEventArgs) Handles lvWarningsWorkAround.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lvWarningsWorkAroundSelected = lvWarningsWorkAround.GetItemAt(e.X, e.Y)

            If lvWarningsWorkAroundSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()

                Dim aplicaMenuItem As MenuItem = New MenuItem("Aplica")
                AddHandler aplicaMenuItem.Click, AddressOf AccionsLvWarningsWorkAroundClick
                m.MenuItems.Add(aplicaMenuItem)

                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionsLvWarningsWorkAroundClick
                m.MenuItems.Add(altresMenuItem)

                m.Show(lvWarningsWorkAround, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    Private Sub lviFontsTrueType_MouseDown(sender As Object, e As MouseEventArgs) Handles lviFontsTrueType.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lviFontsTrueTypeSelected = lviFontsTrueType.GetItemAt(e.X, e.Y)

            If lviFontsTrueTypeSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()

                Dim aplicaMenuItem As MenuItem = New MenuItem("Aplica")
                AddHandler aplicaMenuItem.Click, AddressOf AccionsLvFontsTrueTypeClick
                m.MenuItems.Add(aplicaMenuItem)


                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionsLvFontsTrueTypeClick
                m.MenuItems.Add(altresMenuItem)

                m.Show(lviFontsTrueType, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Menu contextual del lvTwipsPixels
    ''' https://stackoverflow.com/questions/13437889/showing-a-context-menu-for-an-item-in-a-listview
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lvTwipsPixels_MouseDown(sender As Object, e As MouseEventArgs) Handles lvTwipsPixels.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lviTwipsPixelsSelected = lvTwipsPixels.GetItemAt(e.X, e.Y)

            If lviTwipsPixelsSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()

                Dim manteLiniaMenuItem As MenuItem = New MenuItem("ManteLinia")
                AddHandler manteLiniaMenuItem.Click, AddressOf AccionsLvTwipsPixelsClick
                m.MenuItems.Add(manteLiniaMenuItem)

                Dim aplicaSubstituidaMenuItem As MenuItem = New MenuItem("AplicaSubstituida")
                AddHandler aplicaSubstituidaMenuItem.Click, AddressOf AccionsLvTwipsPixelsClick
                m.MenuItems.Add(aplicaSubstituidaMenuItem)

                Dim aplicaPerNumReglaMenuItem As MenuItem = New MenuItem("AplicaXNumregla")
                AddHandler aplicaPerNumReglaMenuItem.Click, AddressOf AccionsLvTwipsPixelsClick
                m.MenuItems.Add(aplicaPerNumReglaMenuItem)

                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionsLvTwipsPixelsClick
                m.MenuItems.Add(altresMenuItem)

                m.Show(lvTwipsPixels, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Aplica les modificacions necessaries per corregir els "Bugs"
    ''' </summary>
    Private Sub WorkAround_Bugs_Afegir_Modificar_Linies(ByVal cadenesAfegir As List(Of String), ByVal linia As String, ByRef noveslinies As List(Of String), ByVal substituir As Boolean, Optional ByVal liniaseguent As String = "")
        '! si volem afegir hem d'incloure la propia linia
        If Not substituir Then noveslinies.Add(linia)

        '! comprovem si ja hem executat les addicions o modificacions en un procés anterior
        If liniaseguent.Trim = cadenesAfegir(0) Then Exit Sub

        For Each cadena As String In cadenesAfegir
            noveslinies.Add(cadena)
        Next
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxer"></param>
    Private Sub Aplica_Formularis_Sense_Caption(ByVal fitxer As String)
        Dim splitList() As String = {}
        Dim novesliniesNet As New List(Of String)

        Dim liniesFitxerVBNet As List(Of String) = LlegeixFitxer(fitxer, encoding)

        For Each linia As String In liniesFitxerVBNet
            '! busquem la linia Me.Name = "XXXXXXX" (tot formulari ha de tenir la propietat Name!!)
            '! Afegirem la linia Me.Text = "" just després
            If RegexCercaCadena(linia, REGEX_CONST_ME & PROPIETAT_NAME & " = \""[\w\s]+\""", splitList) Then
                '! afegim linia al fitxer Net vb
                novesliniesNet.Add(linia)

                '! afegim la linia que ens falta
                novesliniesNet.Add("Me.Text = """"")
            Else
                '! afegim linia al fitxer Net vb
                novesliniesNet.Add(linia)
            End If
        Next

        '! gravem fitxer Net vb
        File.WriteAllLines(fitxer, novesliniesNet.ToArray, GetFileEncoding(fitxer))
    End Sub

    ''' <summary>
    ''' WORKAROUND
    ''' BUG DETECTAT : que passa si el nom de la classe/UserControl té el mateix nom que el nom de l'instancia d'un control ? 
    ''' Exemple : el UserControl GmsSetmana i el control GmsSetmana del formulari frmAvisosProgramats.
    ''' El Formparent executa els events segons l'ordre adequat.En primer lloc s'executen els events de classe i després els events de l'instancia.
    ''' Al recuperar els "delegats", com es recupera pel nom del mètode i aquest és el mateix, es "raise" 2 vegades l'event
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <param name="liniesFitxerNet"></param>
    Private Sub WorkAround_NomClasseUserControl_igual_nomInstancia(ByVal fitxer As String, ByRef liniesFitxerNet As List(Of String))

        For Each ctrl As ControlVB6 In ObtenirControls(fitxer)
            '? comparem amb el nom de les classes UserControl
            If ctrl.nomControl = "GmsSetmana" OrElse ctrl.nomControl = "GmsBarra" OrElse ctrl.nomControl = "GmsData" OrElse ctrl.nomControl = "GmsImports" OrElse ctrl.nomControl = "GmsTemps" Then

                '! afegim un nou item
                Dim lvi As New ListViewItem()

                With lvi
                    .BackColor = Color.LightBlue

                    .SubItems.Add(fitxer)
                End With

                '! afegim la linia
                lviNomClasseUserControl_igual_nomInstancia.Items.Add(lvi)
            End If
        Next
    End Sub

    ''' <summary>
    ''' WORKAROUND : el procés de migració (Wizard 2008) quan troba un formulari que té la propietat Caption, migra la propietat Caption a la propietat Text 
    ''' PERÒ si el valor és "", li posa el nom del formulari.Això pot provocar efectes colaterals quan es modifica la propietat Text en Runtime
    ''' Exemple : frmExecutaProcesClase --> Load --> Me.Text = ""
    ''' </summary>
    Private Sub WorkAround_Formularis_Sense_Caption(ByVal fitxerCodiNet As String, ByRef liniesFitxerNet As List(Of String))
        Dim splitList() As String = {}

        '! aquest workaround NOMÉS s'aplica en els fitxers Designer
        If Not EsFitxerDesigner(fitxerCodiNet) Then Exit Sub

        For Each linia In liniesFitxerNet
            '! Excepció : en el cas que el formulari no tingui BorderStyle, no és necessari aplicar el WorkAround
            If RegexCercaCadena(linia, "Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None", splitList) OrElse
                    RegexCercaCadena(linia, "Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow", splitList) Then
                '! si existeix, surtim
                Exit Sub
            End If

            '! si ja existeix la linia, surtim
            If RegexCercaCadena(linia, REGEX_CONST_ME & "Text = \""[\w\s\.\/\-\(\)\,\:\·\'\?\¿]*\""", splitList) Then        '? punt feble
                '! si existeix, surtim
                Exit Sub
            End If
        Next


        '! afegim un nou item
        Dim lvi As New ListViewItem()

        With lvi
            .BackColor = Color.LightBlue

            .SubItems.Add(fitxerCodiNet)
        End With

        '! afegim la linia
        lviFormularis_Sense_Caption.Items.Add(lvi)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxerCodiNet"></param>
    ''' <param name="numlinia"></param>
    ''' <param name="liniesFitxerNet"></param>
    Private Sub WorkAround_Matriu1_0(ByVal fitxerCodiNet As String, ByVal numlinia As Integer, ByVal liniesFitxerNet As List(Of String))
        Dim splitList() As String = {}
        Dim liniaSubstituida As String

        Dim linia As String = liniesFitxerNet(numlinia)

        If RegexCercaCadena(linia, "UPGRADE_WARNING: El límite inferior de la matriz \w+ ha cambiado de 1 a 0.", splitList) Then
            Dim liniaSeguent As String = GetLiniaSeguent(numlinia, liniesFitxerNet)

            '! comprovem que no haguem processat en un procés anterior
            If liniaSeguent.Contains(LINIA_MIGRADA_COLECCIO_1_0) Then Exit Sub

            liniaSubstituida = liniaSeguent

            splitList = {}

            Dim patroCerca As String = "Dim (\w+)\((\w+)\) As ([\w\.]+)"

            '! busquem els "valors" que hem de restar en 1
            Dim trobat As Boolean = RegexCercaCadena(liniaSeguent, patroCerca, splitList)

            For i = 0 To splitList.Count - 1

                '! comprovem si el "valor" que hem de restar és un numero o no
                If Information.IsNumeric(splitList(i)) Then
                    liniaSubstituida = RegexSubstitucioCadena(liniaSubstituida, patroCerca, "Dim $1(" & (CInt(splitList(2)) - 1).ToString & ") As $3")
                Else
                    '? en regex els caracters + , - han de precedir un \
                    'Dim regexOperadorsAritmetics As String = splitList(i).Replace("+", "\+").Replace("-", "\-")

                    'Dim splitList2() As String = {}
                    'RegexCercaCadena(splitList(i), "([\w\.]+)\s+([\+\-]+)\s+(\d+)", splitList2)

                    'If splitList2.Count = 0 Then
                    '    Dim valor_menys_1 As String = splitList(1) & " - 1"

                    '    '? pot ser que contingui ( i ) . Exemple : GetAllNodes(MenuTreeView).Count
                    '    Dim rectificarRegex As String = splitList(1).Replace("(", "\(").Replace(")", "\)")

                    '    liniaSubstituida = RegexSubstitucioCadena(liniaSubstituida, "Item\(" & rectificarRegex & "\)", "Item(" & valor_menys_1 & ")")
                    'Else
                    '    Dim valor_menys_1 As String = (CInt(splitList2(3)) - 1).ToString
                    '    Dim substitucio As String = String.Empty

                    '    If valor_menys_1 = "0" Then
                    '        substitucio = splitList2(1)
                    '    Else
                    '        substitucio = splitList2(1) & " " & splitList2(2) & " " & valor_menys_1
                    '    End If

                    '    liniaSubstituida = RegexSubstitucioCadena(liniaSubstituida, "Item\((" & regexOperadorsAritmetics & ")\)", "Item(" & substitucio & ")")
                    'End If

                End If


            Next

            '! si hem de substituir (hem trobat)
            If trobat Then
                '! afegim un nou item
                Dim lvi As New ListViewItem()

                With lvi
                    .BackColor = Color.LightGreen
                    .SubItems.Add(liniaSeguent)

                    .SubItems.Add(liniaSubstituida)
                    .SubItems.Add("")
                    .SubItems.Add(fitxerCodiNet)
                    .SubItems.Add(numlinia)
                End With

                '! afegim la linia
                lviColeccio1_0.Items.Add(lvi)
            End If

        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="contingut"></param>
    ''' <returns></returns>
    Private Function Resta_1(ByVal linia As String, ByVal patro As String, ByVal contingut As String) As String
        Dim splitList() As String = {}

        RegexCercaCadena(linia, patro, splitList)

        '! comprovem si el "valor" que hem de restar és un numero o no
        If Information.IsNumeric(splitList(1)) Then
            Return RegexSubstitucioCadena(contingut, splitList(1), (CInt(splitList(1)) - 1).ToString)
        Else
            Return RegexSubstitucioCadena(contingut, splitList(1), splitList(1) & " - 1")
        End If
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    Private Sub CalculaLiniSubstituida_Coleccio_1_0_Numero_NoNumero(ByRef linia As String, ByVal contingut As String)

        '! comprovem si el "valor" que hem de restar és un numero o no
        If Information.IsNumeric(contingut) Then
            linia = RegexSubstitucioCadena(linia, "Item\((" & contingut & ")\)", "Item(" & (CInt(contingut) - 1).ToString & ")")
        Else
            '? en regex els caracters + , - han de precedir un \
            Dim regexOperadorsAritmetics As String = contingut.Replace("+", "\+").Replace("-", "\-")

            Dim splitList2() As String = {}
            RegexCercaCadena(contingut, "([\w\.]+)\s+([\+\-]+)\s+(\d+)", splitList2)

            '? cas especial : si el "contingut" conté la cadena "Piece"
            If splitList2.Count = 0 OrElse contingut.ToUpper.Contains("PIECE") Then
                Dim valor_menys_1 As String = contingut & " - 1"

                '? pot ser que contingui ( i ) . Exemple : MenuTreeView.Nodes.Count
                Dim rectificarRegex As String = contingut.Replace("(", "\(").Replace(")", "\)")

                linia = RegexSubstitucioCadena(linia, "Item\(" & rectificarRegex & "\)", "Item(" & valor_menys_1 & ")")
            Else
                Dim valor_menys_1 As String = (CInt(splitList2(3)) - 1).ToString
                Dim substitucio As String = String.Empty

                If valor_menys_1 = "0" Then
                    substitucio = splitList2(1)
                Else
                    substitucio = splitList2(1) & " " & splitList2(2) & " " & valor_menys_1
                End If

                linia = RegexSubstitucioCadena(linia, "Item\((" & regexOperadorsAritmetics & ")\)", "Item(" & substitucio & ")")
            End If

        End If

        ''''Return liniaSubstituida
    End Sub

    ''' <summary>
    ''' WORKAROUND : UPGRADE_WARNING: El límite inferior de la colección XXXXXXX cambió de 1 a 0.
    ''' </summary>
    ''' <param name="numlinia"></param>
    ''' <param name="liniesFitxerNet"></param>
    Private Sub WorkAround_Coleccio1_0(ByVal fitxerCodiNet As String, ByVal numlinia As Integer, ByVal liniesFitxerNet As List(Of String))
        Dim splitList() As String = {}
        'Dim liniaTractada As String = String.Empty
        Dim liniaSubstituida As String = String.Empty
        Dim contingut_menys_1 As String = String.Empty
        Dim trobat As Boolean = False

        Dim linia As String = liniesFitxerNet(numlinia)

        '! exemple : If Element.SubItems.Count > J Then
        If RegexCercaCadena(linia, "\." & REGEX_SUBITEMS & "\.Count", splitList) Then
            '! comprovem que no haguem processat en un procés anterior
            If linia.Contains(LINIA_MIGRADA_COLECCIO_1_0) Then Exit Sub

            liniaSubstituida = RegexSubstitucioCadena(linia, "\." & REGEX_SUBITEMS & "\.Count", "." & REGEX_SUBITEMS & ".Count -1")

            trobat = True

            '! exemple : Element.SubItems(J).Text = xxxxx
        ElseIf RegexCercaCadena(linia, "\." & REGEX_SUBITEMS & "\((\w+)\)", splitList) Then
            '! comprovem que no haguem processat en un procés anterior
            If linia.Contains(LINIA_MIGRADA_COLECCIO_1_0) Then Exit Sub

            contingut_menys_1 = Resta_1(linia, "\." & REGEX_SUBITEMS & "\((\w+)\)", "." & REGEX_SUBITEMS & "(" & splitList(1) & ")")

            liniaSubstituida = RegexSubstitucioCadena(linia, "\." & REGEX_SUBITEMS & "\((\w+)\)", contingut_menys_1)

            trobat = True

            '! exemple : Element.SubItems.Insert(J, xxxxxxxx
        ElseIf RegexCercaCadena(linia, "\." & REGEX_SUBITEMS & "\.Insert\((\w+)", splitList) Then
            '! comprovem que no haguem processat en un procés anterior
            If linia.Contains(LINIA_MIGRADA_COLECCIO_1_0) Then Exit Sub

            contingut_menys_1 = Resta_1(linia, "\." & REGEX_SUBITEMS & "\.Insert\((\w+)", "." & REGEX_SUBITEMS & ".Insert(" & splitList(1))

            liniaSubstituida = RegexSubstitucioCadena(linia, "\." & REGEX_SUBITEMS & "\.Insert\((\w+)", contingut_menys_1)

            trobat = True

            ' UPGRADE_WARNING: El límite inferior de la colección [\w\.\(\)]+. cambió de 1 a 0.
        ElseIf RegexCercaCadena(linia, "UPGRADE_WARNING: El límite inferior de la colección [\w\.\(\)]+. cambió de 1 a 0.", splitList) Then
            linia = GetLiniaSeguent(numlinia, liniesFitxerNet)

            '! comprovem que no haguem processat en un procés anterior
            If linia.Contains(LINIA_MIGRADA_COLECCIO_1_0) Then Exit Sub

            splitList = {}

            '! busquem els "valors" que hem de restar en 1
            trobat = RegexCercaCadena(linia, "Item\(([\w\s\+\-\.\,]+)\)", splitList)

            '! en una linia pot haver varis casos a tractar
            For i = 0 To splitList.Count - 1
                If i Mod 2 = 1 Then
                    CalculaLiniSubstituida_Coleccio_1_0_Numero_NoNumero(linia, splitList(i))
                End If
            Next

            '! si hem de substituir (hem trobat)
            If trobat Then
                '! afegim un nou item
                Dim lvi As New ListViewItem()

                With lvi
                    .BackColor = Color.LightBlue
                    .SubItems.Add(liniesFitxerNet(numlinia))  '! linia a tractar
                    .SubItems.Add(linia)   '! nova linia
                    .SubItems.Add("")
                    .SubItems.Add(fitxerCodiNet)
                    .SubItems.Add(numlinia)
                End With

                '! afegim la linia
                lviColeccio1_0.Items.Add(lvi)
            End If

        End If





        '? TabControl/Tab
        'If RegexCercaCadena(linia, "TabControl\w+\.TabPages\((\d+)\)", splitList) Then
        '    '! comprovem que no haguem processat en un procés anterior
        '    If Not linia.Contains(LINIA_MIGRADA_COLECCIO_1_0) Then

        '        '! afegim un nou item
        '        Dim lvi As New ListViewItem()

        '        With lvi
        '            .BackColor = Color.LightBlue

        '            .SubItems.Add(linia)

        '            Dim num_menys_1 As String = (CInt(splitList(1)) - 1).ToString
        '            .SubItems.Add(RegexSubstitucioCadena(linia, "(TabControl\w+)\.TabPages\((\d+)\)", "$1.TabPages(" & num_menys_1 & ")"))

        '            .SubItems.Add(fitxerCodiNet)
        '            .SubItems.Add(liniesFitxerNet.IndexOf(linia))
        '        End With

        '        '! afegim la linia
        '        lviColeccio1_0.Items.Add(lvi)
        '    End If

        'End If

        '? Workaround : Index - 1 coleccio en controlArrayS.
        '? Observació: en alguns casos SI que s'ha de passar de 1 a 0.
        '? ****************IMPORTANT : si la classe "controlArray" "simula" el funcionament dels index de VB6, la seguent linia cal comentar-la !!!!!!!!!!!!!!!!!!      '? dijous 21/10
        ''''''WorkAround_ControlArray_Coleccio_1_0(linia, numlinia, fitxerCodiNet)
    End Sub

    ''' <summary>
    ''' WORKAROUND : els indexS dels controlArray també passen de 1 a 0
    ''' </summary>
    Private Sub WorkAround_ControlArray_Coleccio_1_0(ByVal linia As String, ByVal numlinia As Integer, ByVal fitxerCodiNet As String)
        Dim splitList() As String = {}


        If RegexCercaCadena(linia, "(\w+)\((\d+)\)", splitList) OrElse RegexCercaCadena(linia, "(\w+)\((\d+)\)\.", splitList) Then
            '! comprovem que no haguem processat en un procés anterior
            If Not linia.Contains(LINIA_MIGRADA_COLECCIO_1_0) Then
                '? nom del control
                Dim nomControl As String = splitList(1)

                '? IMPORTANT **************** NOMES HEM D'APLICAR EL WORKAROUND DE RESTAR 1 A L'INDEX SI L'INDEX DE L'ELEMENT MES PETIT NO ES 0
                '? O sigui el primer element del controlArray no es <controlArray>_0 SINO <controlArray>_1
                If GetIndexMenorElementsControlArray(fitxerCodiNet, nomControl) = 0 Then Exit Sub   '! no "processem" el workaround


                '! afegim un nou item
                Dim lvi As New ListViewItem()

                With lvi
                    .BackColor = Color.LightGreen

                    .SubItems.Add(linia)

                    Dim num_menys_1 As String = (CInt(splitList(2)) - 1).ToString
                    .SubItems.Add(RegexSubstitucioCadena(linia, "(\w+)\((\d+)\)", "$1(" & num_menys_1 & ")"))

                    .SubItems.Add("Array")
                    .SubItems.Add(fitxerCodiNet)
                    .SubItems.Add(numlinia)
                End With

                '! afegim la linia
                lviColeccio1_0.Items.Add(lvi)
            End If
        End If

    End Sub

    ''' <summary>
    '''  WORKAROUND :  UPGRADE_WARNING: El evento frmXXXXXXX.Resize se puede desencadenar cuando se inicializa el formulario
    '''  Pot passar si trobem els seguent mètodes d'events en el codi :
    '''     TextChanged
    '''     Resize
    '''     SelectedIndexChanged
    '''     ......
    ''' </summary>
    Private Sub WorkAround_Designer_IsInitializing(ByVal fitxerCodiNET As String, ByRef novesliniesDesigner As List(Of String))
        Dim liniaEnBlanc As String = ""
        Dim canvisDesigner_IsInitializing As Boolean = False
        Dim splitList() As String = {}

        '! obtenim el corresponent fitxer Designer
        Dim fitxerDesigner As String = GetFitxerDesignerByVB(cboDirectoryNet.Text & fitxerCodiNET.Replace(CarpetaDotNET, ""))

        If Not File.Exists(fitxerDesigner) Then
            MessageBox.Show(String.Format("El fitxer {0} no existeix", fitxerDesigner))
            Exit Sub
        End If

        Dim liniesFitxerNet_Designer As List(Of String) = LlegeixFitxer(fitxerDesigner, encoding)

        For Each linia As String In liniesFitxerNet_Designer

            '! comprovem si ja hem afegit el canvis en una execució anteriro
            If RegexCercaCadena(linia, "IsInitializing\s*=\s*True", splitList) Then
                canvisDesigner_IsInitializing = True
            End If

            '? si no hem trobat cap linia del tipus : IsInitializing = True
            If Not canvisDesigner_IsInitializing AndAlso RegexCercaCadena(linia, REGEX_INICI_LINIA_BLANC & "InitializeComponent\(\)", splitList) Then
                '! linies que afegim
                Dim cadenaAfegir_11 As String = "'? UPGRADE_WARNING: El evento XXXXXX se puede desencadenar cuando se inicializa el formulario"
                Dim cadenaAfegir_12 As String = "IsInitializing = True   'ms-help://MS.VSCC.v90/MS.msdnexpress.v90.es/dv_vbvers/html/88b12ae1-6de0-48a0-86f1-60c0686c026a.htm"
                Dim cadenaAfegir_13 As String = "'Llamada necesaria para el Diseñador de Windows Forms."
                Dim cadenaAfegir_14 As String = "InitializeComponent()"
                Dim cadenaAfegir_15 As String = "'? UPGRADE_WARNING: El evento XXXXXX se puede desencadenar cuando se inicializa el formulario"
                Dim cadenaAfegir_16 As String = "IsInitializing = False  'ms-help://MS.VSCC.v90/MS.msdnexpress.v90.es/dv_vbvers/html/88b12ae1-6de0-48a0-86f1-60c0686c026a.htm"

                '! apliquem canvis
                WorkAround_Bugs_Afegir_Modificar_Linies(New List(Of String)({liniaEnBlanc, cadenaAfegir_11, cadenaAfegir_12, liniaEnBlanc, cadenaAfegir_13, cadenaAfegir_14, liniaEnBlanc, cadenaAfegir_15, cadenaAfegir_16, liniaEnBlanc}), linia, novesliniesDesigner, True, cadenaAfegir_15)

            Else  '! la resta de linies
                novesliniesDesigner.Add(linia)
            End If
        Next

        '! gravem fitxer designer
        File.WriteAllLines(fitxerDesigner, novesliniesDesigner.ToArray, GetFileEncoding(fitxerDesigner))
    End Sub

    ''' <summary>
    ''' WORKAROUND :  UPGRADE_WARNING: El evento frmXXXXXXX.Resize se puede desencadenar cuando se inicializa el formulario
    ''' </summary>    
    ''' <param name="novesliniesDesigner"></param>
    ''' <param name="liniaNet"></param>
    ''' <param name="novesliniesNet"></param>
    ''' <param name="liniaNet_seguent"></param>
    Private Sub WorkAround_Resize(ByRef novesliniesDesigner As List(Of String), ByVal liniaNet As String, ByRef novesliniesNet As List(Of String), ByVal liniaNet_seguent As String)
        Dim liniaEnBlanc As String = ""
        Dim splitList() As String = {}
        Dim canvisDesigner_IsInitializing As Boolean = False

        '! linies que afegim
        Dim cadenaAfegir_2 As String = "'? En Runtime + Initializing NO s'ha d'executar el codi"
        Dim cadenaAfegir_3 As String = "If Not DesignMode AndAlso IsInitializing Then Exit Sub"

        '! apliquem canvis
        WorkAround_Bugs_Afegir_Modificar_Linies(New List(Of String)({LINIA_AFEGIDA_RESIZE, cadenaAfegir_2, cadenaAfegir_3, liniaEnBlanc}), liniaNet, novesliniesNet, False, liniaNet_seguent)
    End Sub

    ''' <summary>
    ''' Exemple : "_Toolbar1_Button2" , "DisplayStyle"
    ''' </summary>
    ''' <param name="nomControl"></param>
    ''' <param name="tipusLinia"></param>
    ''' <returns></returns>
    Private Function ConteLiniaToolbar(ByVal nomControl As String, ByVal tipusLinia As String) As Boolean
        '! comprovem si la key (nomcontrol) existeix en el Dictionary
        If Not dictLiniesDuplicadesToolbar.ContainsKey(nomControl) Then Return False

        For Each tipus As String In dictLiniesDuplicadesToolbar.Item(nomControl)
            If tipus = tipusLinia Then Return True
        Next

        Return False
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="nomControl"></param>
    ''' <param name="tipusLinia"></param>
    Private Sub ActualitzaDicLiniesDuplicadesToolbar(ByVal nomControl As String, ByVal tipusLinia As String)
        '! comprovem si la key (nomcontrol) existeix en el Dictionary
        If Not dictLiniesDuplicadesToolbar.ContainsKey(nomControl) Then
            Dim tipusLinies As New List(Of String)({tipusLinia})

            dictLiniesDuplicadesToolbar.Add(nomControl, tipusLinies)

        Else
            '! afegim el nou tipusLinia a la llista
            Dim tipusLinies As List(Of String) = dictLiniesDuplicadesToolbar.Item(nomControl)
            tipusLinies.Add(tipusLinia)

            '! actualitzem el Dictionary
            dictLiniesDuplicadesToolbar.Item(nomControl) = tipusLinies
        End If
    End Sub

    ''' <summary>
    ''' WORKAROUND : actualitza el Dictionary que controla els duplicats de linies (si s'executa varies vegades el procés de migració//validacio
    ''' </summary>
    ''' <param name="linies"></param>
    Private Sub ActualitzaAllDicLiniesDuplicadesToolbar(ByVal linies As String())
        Dim splitList() As String = {}

        For Each linia As String In linies
            '? WORKAROUND : no repetir les linies WithEvents dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, TIPUSLINIA_TOOLBAR_WITHEVENTS & " (\w+) As [\w\.]*ToolStrip\w+", splitList) Then
                '! afegim la linia                
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_WITHEVENTS)
            End If

            '? WORKAROUND : no repetir les linies New dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & "(\w+) = " & TIPUSLINIA_TOOLBAR_NEW & " [\w\.]*ToolStrip\w+", splitList) Then
                '! afegim la linia                
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_NEW)
            End If

            '? WORKAROUND : no repetir les linies Anchor dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & REGEX_NOMTOOLBAR & "\." & TIPUSLINIA_TOOLBAR_ANCHOR & " = [\w\.]*AnchorStyles", splitList) Then
                '! afegim la linia
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_ANCHOR)
            End If

            '? WORKAROUND : no repetir les linies AutoSize dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & REGEX_NOMTOOLBAR & "\." & TIPUSLINIA_TOOLBAR_AUTOSIZE & " =", splitList) Then
                '! afegim la linia
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_AUTOSIZE)
            End If

            '? WORKAROUND : no repetir les linies DockStyle dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & REGEX_NOMTOOLBAR & "\." & TIPUSLINIA_TOOLBAR_DOCK & " = [\w\.]*DockStyle", splitList) Then
                '! afegim la linia                
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_DOCK)
            End If

            '? WORKAROUND : no repetir les linies GripStyle dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & REGEX_NOMTOOLBAR & "\." & TIPUSLINIA_TOOLBAR_GRIPSTYLE, splitList) Then
                '! afegim la linia                
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_GRIPSTYLE)
            End If

            '? WORKAROUND : no repetir les linies ImageKey dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & "(\w+)\." & TIPUSLINIA_TOOLBAR_IMAGEKEY & " = \""\w+\""", splitList) Then
                '! afegim la linia                
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_IMAGEKEY)
            End If

            '? WORKAROUND : no repetir les linies DisplayStyle dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & "(\w+)\." & TIPUSLINIA_TOOLBAR_DISPLAYSTYLE & " = [\w\.]*ToolStripItemDisplayStyle", splitList) Then
                '! afegim la linia                
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_DISPLAYSTYLE)
            End If

            '? WORKAROUND : no repetir les linies Name dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & "(_Toolbar\d+_\w+)\." & TIPUSLINIA_TOOLBAR_NAME & " = \""\w+\""", splitList) Then
                '! afegim la linia                
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_NAME)
            End If

            '? WORKAROUND : no repetir les linies Size dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            ''''If RegexCercaCadena(linia, REGEX_CONST_ME & "(_Toolbar\d+_\w+)\." & TIPUSLINIA_TOOLBAR_SIZE & " = New [\w\.]*Size", splitList) Then
            ''''    '! afegim la linia                
            ''''    ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_SIZE)
            ''''End If

            '? WORKAROUND : no repetir les linies AddRange dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & "(Toolbar\d+)\.Items\." & TIPUSLINIA_TOOLBAR_ADDRANGE, splitList) Then
                '! afegim la linia                
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_ADDRANGE)
            End If

            '? WORKAROUND : no repetir les linies ImageStream dels Toolbar (si es repeteix el procés de Validacio, per exemple)
            If RegexCercaCadena(linia, REGEX_CONST_ME & "(\w+)\." & TIPUSLINIA_TOOLBAR_IMAGESTREAM & " =", splitList) Then
                '! afegim la linia                
                ActualitzaDicLiniesDuplicadesToolbar(splitList(1), TIPUSLINIA_TOOLBAR_IMAGESTREAM)
            End If

        Next
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <param name="encoding"></param>
    ''' <returns></returns>
    Private Function LlegeixFitxer(ByVal fitxer As String, ByVal encoding As Encoding) As List(Of String)
        If Not File.Exists(fitxer) Then
            MsgBox(String.Format("El fitxer {0} no existeix !!!!", fitxer))
            Return Nothing
        End If

        Dim linies As String() = File.ReadAllLines(fitxer, encoding)

        '! aprofitem la "lectura" de les linies del fitxer per guardar linies que es poden duplicar a l'executar varies vegades el procés de migració
        ActualitzaAllDicLiniesDuplicadesToolbar(linies)

        '! retornem totes les linies del fitxer
        Return New List(Of String)(linies)
    End Function

    ''' <summary>
    ''' Accions sobre lviBugs
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionsLvBugsClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Select Case menuItemSelected.Text
            Case "Aplica"

                Cursor = Cursors.WaitCursor

                For Each lvi As ListViewItem In lviBugs.SelectedItems
                    Dim novesliniesNet As New List(Of String)
                    Dim novesliniesDesigner As New List(Of String)

                    Dim canvisDesigner_IsInitializing As Boolean = False

                    '! recuperem la fila del lviBugs
                    'Dim id As String = lvi.SubItems(0).Text
                    Dim tipusBug As TipusBug = DirectCast([Enum].Parse(GetType(TipusBug), lvi.SubItems(1).Text), TipusBug)
                    Dim liniaMetodeEvent As String = lvi.SubItems(2).Text
                    Dim fitxerCodiNET As String = lvi.SubItems(3).Text

                    '! fem les modificacions sobre els fitxers de codi (.vb) de BACKUP
                    Dim fitxerCodiBackup As String = CarpetaDotNET() & "\" & fitxerCodiNET

                    Dim liniesFitxerNet As List(Of String) = LlegeixFitxer(fitxerCodiBackup, encoding)

                    '! busquem el nom d'event
                    Dim splitList() As String = {}
                    RegexCercaCadena(liniaMetodeEvent, "Handles (\w+)\.(\w+)", splitList)

                    If splitList.Count = 0 Then
                        MessageBox.Show(String.Format("Mètode {0} sense Handles", liniaMetodeEvent))
                        Continue For
                    End If

                    Dim nomControl As String = splitList(1)
                    Dim nomEvent As String = splitList(2)
                    Dim liniaEnBlanc As String = ""


                    For n As Integer = 0 To liniesFitxerNet.Count - 1
                        Dim liniaNet As String = liniesFitxerNet.Item(n)

                        If liniaMetodeEvent = liniaNet Then '! si és la linia que hem de processar
                            '! calculem la linia seguent a la que hem de tractar
                            Dim liniaNet_seguent As String = If(n + 1 < liniesFitxerNet.Count, liniesFitxerNet.Item(n + 1), "")

                            If tipusBug = TipusBug.AFTERCHECK Then
                                '! apliquem canvis
                                WorkAround_Bugs_Afegir_Modificar_Linies(New List(Of String)({LINIA_AFEGIDA_AFTERCHECK, liniaEnBlanc}), liniaNet, novesliniesNet, False, liniaNet_seguent)

                                '! actualitzem el fitxer Designer
                                WorkAround_Designer_IsInitializing(fitxerCodiNET, novesliniesDesigner)

                            ElseIf tipusBug = TipusBug.TEXTCHANGED Then
                                '! linies que afegim
                                Dim cadenaAfegir_2 As String = "'? En Runtime + Initializing NO s'ha d'executar el codi"
                                Dim cadenaAfegir_3 As String = "If Not DesignMode AndAlso IsInitializing Then Exit Sub"

                                '! apliquem canvis
                                WorkAround_Bugs_Afegir_Modificar_Linies(New List(Of String)({LINIA_AFEGIDA_TEXTCHANGED, cadenaAfegir_2, cadenaAfegir_3, liniaEnBlanc}), liniaNet, novesliniesNet, False, liniaNet_seguent)

                                '! actualitzem el fitxer Designer
                                WorkAround_Designer_IsInitializing(fitxerCodiNET, novesliniesDesigner)


                            ElseIf tipusBug = TipusBug.VIEWCHANGED Then    '? AxCalendarControl
                                '! linies que afegim
                                Dim cadenaAfegir_2 As String = "'? En Runtime + Initializing NO s'ha d'executar el codi"
                                Dim cadenaAfegir_3 As String = "If Not DesignMode AndAlso IsInitializing Then Exit Sub"

                                '! apliquem canvis
                                WorkAround_Bugs_Afegir_Modificar_Linies(New List(Of String)({LINIA_AFEGIDA_VIEWCHANGED, cadenaAfegir_2, cadenaAfegir_3, liniaEnBlanc}), liniaNet, novesliniesNet, False, liniaNet_seguent)

                                '! actualitzem el fitxer Designer
                                WorkAround_Designer_IsInitializing(fitxerCodiNET, novesliniesDesigner)


                            ElseIf tipusBug = TipusBug.RESIZE Then
                                WorkAround_Resize(novesliniesDesigner, liniaNet, novesliniesNet, liniaNet_seguent)

                                '! actualitzem el fitxer Designer
                                WorkAround_Designer_IsInitializing(fitxerCodiNET, novesliniesDesigner)

                            ElseIf tipusBug = TipusBug.SELECTEDINDEXCHANGED Then
                                '! linies que afegim
                                Dim cadenaAfegir_2 As String = "'? En Runtime + Initializing NO s'ha d'executar el codi"
                                Dim cadenaAfegir_3 As String = "If Not DesignMode AndAlso IsInitializing Then Exit Sub"

                                '! apliquem canvis
                                WorkAround_Bugs_Afegir_Modificar_Linies(New List(Of String)({LINIA_AFEGIDA_SELECTEDINDEXCHANGED, cadenaAfegir_2, cadenaAfegir_3, liniaEnBlanc}), liniaNet, novesliniesNet, False, liniaNet_seguent)

                                '! actualitzem el fitxer Designer
                                WorkAround_Designer_IsInitializing(fitxerCodiNET, novesliniesDesigner)

                            ElseIf tipusBug = TipusBug.ITEMCHECKED Then
                                Dim cadenaAfegir As String = "'? WORKAROUND : https://stackoverflow.com/questions/7506588/why-does-listview-fire-event-indicating-that-checked-item-is-unchecked-after-add/7507293"

                                '! apliquem canvis
                                WorkAround_Bugs_Afegir_Modificar_Linies(New List(Of String)({cadenaAfegir, LINIA_AFEGIDA_ITEMCHECKED_FOCUSEDITEM, liniaEnBlanc}), liniaNet, novesliniesNet, False, liniaNet_seguent)

                            ElseIf tipusBug = TipusBug.AFTERSELECT Then
                                '! apliquem canvis
                                WorkAround_Bugs_Afegir_Modificar_Linies(New List(Of String)({LINIA_AFEGIDA_AFTERSELECT, liniaEnBlanc}), liniaNet, novesliniesNet, False, liniaNet_seguent)
                            End If

                        Else
                            '! afegim linia al fitxer Net vb
                            novesliniesNet.Add(liniaNet)
                        End If
                    Next

                    '! gravem el fitxer de codi Net vb
                    File.WriteAllLines(fitxerCodiBackup, novesliniesNet.ToArray, GetFileEncoding(fitxerCodiBackup))
                Next

                '! "eliminem" els items que hem processat
                For Each l As ListViewItem In lviBugs.SelectedItems
                    l.Remove()
                Next

                Cursor = Cursors.Default

            Case "Descartar"

                For Each i As ListViewItem In lviBugs.SelectedItems
                    lviBugs.Items.Remove(i)
                Next

            Case Else

        End Select
    End Sub

    ''' <summary>
    ''' Accions sobre lvWarningsWorkAround
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionsLvWarningsWorkAroundClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Select Case menuItemSelected.Text
            Case "Aplica"

                Cursor = Cursors.WaitCursor

                For Each lvi As ListViewItem In lvWarningsWorkAround.SelectedItems
                    Dim noveslinies As New List(Of String)

                    '! recuperem la fila del lvWarningsWorkAround
                    'Dim id As String = lvi.SubItems(0).Text
                    Dim nomControlNET As String = lvi.SubItems(1).Text
                    Dim liniaMetodeEvent As String = lvi.SubItems(2).Text
                    Dim fitxerCodiNET As String = lvi.SubItems(3).Text
                    Dim numLinia As String = lvi.SubItems(4).Text

                    Dim liniesFitxerDesigner As List(Of String) = LlegeixFitxer(fitxerCodiNET, encoding)

                    '! busquem el nom d'event
                    Dim splitList() As String = {}
                    RegexCercaCadena(liniaMetodeEvent, "Handles (\w+)\.(\w+)", splitList)
                    Dim nomControl As String = splitList(1)   '! exemple : txtUsuario
                    Dim nomEvent As String = splitList(2)   '! exemple : Leave


                    For n = 0 To liniesFitxerDesigner.Count - 1
                        If numLinia = n Then '! es la linia que hem de substituir

                            If String.IsNullOrEmpty(nomControlNET) Then
                                noveslinies.Add(RegexSubstitucioCadena(liniaMetodeEvent, "Private Sub " & "\w+" & "_\w+\((" & REGEX_TOTS_ALFANUM & ")\) Handles \w+\.(\w+)", "Private Sub " & nomControl & "_$2($1) Handles " & nomControl & ".$2"))
                            Else
                                noveslinies.Add(RegexSubstitucioCadena(liniaMetodeEvent, "Private Sub " & "\w+" & "_\w+\((" & REGEX_TOTS_ALFANUM & ")\) Handles \w+\.(\w+)", "Private Sub " & nomControlNET & "_$2($1) Handles " & nomControlNET & ".$2"))
                            End If

                        Else
                            noveslinies.Add(liniesFitxerDesigner.Item(n))
                        End If
                    Next

                    '! gravem
                    File.WriteAllLines(fitxerCodiNET, noveslinies.ToArray, GetFileEncoding(fitxerCodiNET))
                Next

                For Each l As ListViewItem In lvWarningsWorkAround.SelectedItems
                    l.Remove()
                Next

                Cursor = Cursors.Default


            Case "Descartar"
                For Each i As ListViewItem In lvWarningsWorkAround.SelectedItems
                    lvWarningsWorkAround.Items.Remove(i)
                Next

            Case Else
        End Select

    End Sub

    ''' <summary>
    ''' Accions sobre lviFontsTrueType
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionsLvFontsTrueTypeClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Try
            Select Case menuItemSelected.Text
                Case "Aplica"

                    Cursor = Cursors.WaitCursor

                    For Each lvi As ListViewItem In lviFontsTrueType.SelectedItems
                        Dim noveslinies As New List(Of String)
                        Dim liniaAnterior As String = String.Empty   '! per evitar duplicats

                        '! recuperem la fila del lviFontsTrueType
                        'Dim id As String = lvi.SubItems(0).Text
                        Dim fitxerDesigner As String = lvi.SubItems(1).Text
                        Dim controlAX As String = lvi.SubItems(2).Text
                        Dim controlNET As String = lvi.SubItems(3).Text

                        Dim liniesFitxerDesigner As List(Of String) = LlegeixFitxer(fitxerDesigner, encoding)

                        For Each l As String In liniesFitxerDesigner
                            '! busquem la primera linia que contingi el controlNET
                            '! agafarem el criteri : la linia que cumpleixi : Me\.\w+\.Name = \"\w+\"

                            Dim liniaPatroBuscar As String = REGEX_CONST_ME & controlNET & "\.Name = \""" & controlNET & "\"""

                            If RegexCercaCadena(l, liniaPatroBuscar) Then
                                Dim liniaFont As String = CONST_ME & controlNET & ".Font = New System.Drawing.Font(""Microsoft Sans Serif"", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))"

                                If liniaFont <> liniaAnterior Then noveslinies.Add(liniaFont)
                            End If

                            noveslinies.Add(l)

                            '! guardem la linia anterior per no fer duplicats
                            liniaAnterior = l
                        Next


                        '! gravem
                        File.WriteAllLines(fitxerDesigner, noveslinies.ToArray, GetFileEncoding(fitxerDesigner))

                    Next

                    For Each L In lviFontsTrueType.SelectedItems
                        lviFontsTrueType.Items.Remove(L)
                    Next

                    RefrescarProces("Total : Fonts TrueType", lviFontsTrueType.Items.Count, "", "")

                    Cursor = Cursors.Default

                Case "Descartar"
                    For Each i As ListViewItem In lviFontsTrueType.SelectedItems
                        lviFontsTrueType.Items.Remove(i)
                    Next

                Case Else

            End Select

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Accions sobre la llista dels upgradeS
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionsLvUpgradesClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Select Case menuItemSelected.Text
            Case "Descartar"

                Dim numEliminats As Integer = treeListViewUpgrades.SelectedItems.Count

                For Each i As ListViewItem In treeListViewUpgrades.SelectedItems
                    i.Remove()
                Next

                lblProcessant.Text = String.Format("UPGRADES calculats seleccionats : {0} , Totals : {1}", numTotalUpgradesSeleccionats - numEliminats, numTotalUpgrades)

                numTotalUpgradesSeleccionats = numTotalUpgradesSeleccionats - numEliminats
            Case Else

        End Select
    End Sub

    ''' <summary>
    ''' Accions sobre la llista lviArquitectura
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionsLvAsWorkAroundArquitectura(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Select Case menuItemSelected.Text
            Case "Revisat"   '! posar color ColorUpgrade.Resolt

                For Each lvi As ListViewItem In lviArquitectura.SelectedItems
                    SubstituirRevisatNoRevisatSense(lvi, ColorUpgrade.Resolt)
                Next

            Case "NoRevisat"     '! posar color ColorUpgrade.Altres

                For Each lvi As ListViewItem In lviArquitectura.SelectedItems
                    SubstituirRevisatNoRevisatSense(lvi, ColorUpgrade.Altres)
                Next

            Case "NA"     '! posar color ColorUpgrade.Sense

                For Each lvi As ListViewItem In lviArquitectura.SelectedItems
                    SubstituirRevisatNoRevisatSense(lvi, ColorUpgrade.Sense)
                Next

            Case Else


        End Select
    End Sub

    '? dilluns 21
    ''' <summary>
    ''' Accions sobre la llista lviFormsRedisseny
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionslviFormsRedisseny(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Select Case menuItemSelected.Text
            Case "Select"

                '! netejem la listview
                lstItemsFoundNet.Items.Clear()

                For Each lvi As ListViewItem In lviFormsRedisseny.SelectedItems
                    Dim fitxer As String = lvi.SubItems(1).Text    '! designer.vb

                    Dim nomFitxer As String = RegexSubstitucioCadena(fitxer, "(\w+)\.Designer\.vb", "$1")

                    '! afegim els corresponent fitxers Designer i vb
                    lstItemsFoundNet.Items.Add(nomFitxer & EXTENSIO_VB)
                    lstItemsFoundNet.Items.Add(nomFitxer & EXTENSIO_DESIGNER)

                Next

                '! check els items seleccionats
                For Each lvi As ListViewItem In lstItemsFoundNet.Items
                    lvi.Checked = True
                Next

                lblTrobats_2.Text = lstItemsFoundNet.Items.Count

            Case "UnSelect"

                For Each lvi As ListViewItem In lviFormsRedisseny.SelectedItems
                    Dim fitxer As String = lvi.SubItems(1).Text    '! resX

                    For Each lviFoundNet As ListViewItem In lstItemsFoundNet.Items

                        If RegexSubstitucioCadena(fitxer, REGEX_RESX, REGEX_DESIGNER_SUBSTITUCIO).ToUpper = lviFoundNet.SubItems(0).Text.ToUpper Then
                            lviFoundNet.Checked = False

                            Exit For
                        End If
                    Next
                Next

                lblTrobats_2.Text = lstItemsFoundNet.Items.Count

            Case "Descartar"
                For Each i As ListViewItem In lviFormsRedisseny.SelectedItems
                    lviFormsRedisseny.Items.Remove(i)
                Next

            Case Else

        End Select
    End Sub

    ''' <summary>
    ''' Accions sobre la llista lviFormularis_Sense_Caption
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionslviFormularis_Sense_Caption(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Select Case menuItemSelected.Text
            Case "Aplica"

                Cursor = Cursors.WaitCursor

                For Each lvi As ListViewItem In lviFormularis_Sense_Caption.SelectedItems
                    Dim fitxer As String = lvi.SubItems(1).Text    '! fitxer

                    '! apliquem 
                    Aplica_Formularis_Sense_Caption(fitxer)

                    lviFormularis_Sense_Caption.Items.Remove(lviFormularis_Sense_Caption.SelectedItems(0))
                Next

                Cursor = Cursors.Default

            Case Else
        End Select
    End Sub

    ''' <summary>
    ''' Accions sobre la llista lviColeccio1_0
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionsLvColeccio_1_0Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Select Case menuItemSelected.Text
            Case "Aplica"

                Cursor = Cursors.WaitCursor

                For Each lvi As ListViewItem In lviColeccio1_0.SelectedItems

                    '! recuperem la fila del lviColeccio1_0
                    '    Dim id As String = lvi.SubItems(0).Text
                    '    Dim linia As String = lvi.SubItems(1).Text    '! linia que hem de substituir
                    Dim liniaSubstituida As String = lvi.SubItems(2).Text & LINIA_MIGRADA_COLECCIO_1_0  '! possible substitució
                    Dim fitxer As String = lvi.SubItems(4).Text     '! fitxer on està la linia que hem de substituir
                    Dim numlinia As String = lvi.SubItems(5).Text     '! numero de linia

                    SubstituirLinia(fitxer, numlinia, liniaSubstituida)

                    lviColeccio1_0.Items.Remove(lviColeccio1_0.SelectedItems(0))
                Next

                Cursor = Cursors.Default

            Case "AnarLinia"

                For Each lvi As ListViewItem In lviColeccio1_0.SelectedItems

                    '! recuperem la fila del lviColeccio1_0
                    'Dim id As String = lvi.SubItems(0).Text
                    'Dim linia As String = lvi.SubItems(1).Text    ' linia que hem de substituir
                    'Dim liniaSubstituida As String = lvi.SubItems(2).Text  ' possible substitució
                    Dim fitxer As String = carpetaAProcessar & lvi.SubItems(3).Text     '! fitxer on està la linia que hem de substituir
                    Dim numlinia As String = lvi.SubItems(4).Text     '! numero de linia

                    '! Per resoldre el tema que hi hagi espais en el nom de la solucio       
                    Dim nomFitxer As String = caracterDobleCometa & fitxer & caracterDobleCometa

                    Shell("C:\Program Files (x86)\Notepad++\notepad++.exe " & nomFitxer, AppWinStyle.MaximizedFocus)
                Next

            Case "Descartar"
                For Each i As ListViewItem In lviColeccio1_0.SelectedItems
                    lviColeccio1_0.Items.Remove(i)
                Next

            Case Else


        End Select
    End Sub

    ''' <summary>
    ''' Accions sobre la llista lviAsObject
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionsLvAsObjectClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Dim lstFitxers As New List(Of String)

        Select Case menuItemSelected.Text
            Case "Aplica"

                Cursor = Cursors.WaitCursor

                For Each lvi As ListViewItem In lviAsObject.SelectedItems

                    '? EXCEPCIONS
                    If lvi.SubItems(1).Text.Contains("Dim oRef As Object") OrElse
                        lvi.SubItems(1).Text.Contains("Dim MSXML2 As Object") Then Continue For


                    '? dimecres 22/6/2022
                    If lstFitxers.Contains(lvi.SubItems(3).Text) Then
                        Continue For
                    Else
                        lstFitxers.Add(lvi.SubItems(3).Text)
                    End If

                    Dim fitxerProcessar As String = CalculaFitxerProcessar(lvi.SubItems(3).Text)
                    Dim linies As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)

                    Dim llistaXFitxer As List(Of ListViewItem) = (From s As ListViewItem In lviAsObject.Items Where s.SubItems(3).Text = lvi.SubItems(3).Text).ToList

                    For Each ll As ListViewItem In llistaXFitxer

                        '! recuperem la fila del lviAsObject
                        Dim id As String = ll.SubItems(0).Text
                        Dim linia As String = ll.SubItems(1).Text    '! linia que hem de substituir
                        Dim liniaSubstituida As String = ll.SubItems(2).Text  '! possible substitució
                        Dim fitxer As String = ll.SubItems(3).Text     '! fitxer on està la linia que hem de substituir
                        Dim numlinia As String = ll.SubItems(4).Text     '! numero de linia

                        SubstituirLiniaXFitxer(fitxer, numlinia, liniaSubstituida, linies)
                    Next

                    '! gravem
                    File.WriteAllLines(fitxerProcessar, linies.ToArray, GetFileEncoding(fitxerProcessar))

                    '! eliminem els items processats
                    For Each lr As ListViewItem In llistaXFitxer
                        lviAsObject.Items.Remove(lr)
                    Next
                Next

                Cursor = Cursors.Default

            Case "AnarLinia"

                For Each lvi As ListViewItem In lviAsObject.SelectedItems

                    '! recuperem la fila del lviAsObject
                    'Dim id As String = lvi.SubItems(0).Text
                    'Dim linia As String = lvi.SubItems(1).Text    ' linia que hem de substituir
                    'Dim liniaSubstituida As String = lvi.SubItems(2).Text  ' possible substitució
                    Dim fitxer As String = carpetaAProcessar & lvi.SubItems(3).Text     '! fitxer on està la linia que hem de substituir
                    Dim numlinia As String = lvi.SubItems(4).Text     '! numero de linia

                    '! Per resoldre el tema que hi hagi espais en el nom de la solucio       
                    Dim nomFitxer As String = caracterDobleCometa & fitxer & caracterDobleCometa

                    Shell("C:\Program Files (x86)\Notepad++\notepad++.exe " & nomFitxer, AppWinStyle.MaximizedFocus)
                Next

            Case "Descartar"
                For Each i As ListViewItem In lviAsObject.SelectedItems
                    lviAsObject.Items.Remove(i)
                Next

            Case Else


        End Select
    End Sub

    ''' <summary>
    ''' Accions sobre el menu contextual de lvTwipsPixels
    ''' https://stackoverflow.com/questions/13437889/showing-a-context-menu-for-an-item-in-a-listview
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub AccionsLvTwipsPixelsClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Select Case menuItemSelected.Text
            Case "ManteLinia"

                For Each lvi As ListViewItem In lvTwipsPixels.SelectedItems

                    '! recuperem la fila del lvTwipsPixels
                    Dim id As String = lvi.SubItems(0).Text
                    Dim linia As String = lvi.SubItems(1).Text    '! linia que hem de substituir
                    Dim fitxer As String = lvi.SubItems(4).Text     '! fitxer on està la linia que hem de substituir
                    Dim numlinia As String = lvi.SubItems(5).Text     '! numero de linia

                    '
                    If linia.ToUpper.Contains("MIGRATPIXELS") Then Continue For

                    SubstituirLinia(fitxer, numlinia, linia & LINIA_MIGRADA_TWIPSPIXELS)

                    lvTwipsPixels.Items.Remove(lvTwipsPixels.SelectedItems(0))

                    comptadorLiniesTwipsPixels = lvTwipsPixels.Items.Count - 1

                    lblProcessant.Text = String.Format("Total linies amb GetTwipsToPixels/GetPixelsToTwips : {0}", comptadorLiniesTwipsPixels)


                    Debug.Print(String.Format("||{0}||{1}", linia.Trim, lvi.SubItems(2).Text.Trim))

                Next

            Case "AplicaSubstituida"

                For Each lvi As ListViewItem In lvTwipsPixels.SelectedItems

                    '! recuperem la fila del lvTwipsPixels
                    Dim id As String = lvi.SubItems(0).Text
                    Dim linia As String = lvi.SubItems(1).Text    '! linia que hem de substituir
                    Dim liniaSubstituida As String = lvi.SubItems(2).Text  '! possible substitució
                    Dim fitxer As String = lvi.SubItems(4).Text     '! fitxer on està la linia que hem de substituir
                    Dim numlinia As String = lvi.SubItems(5).Text     '! numero de linia

                    If RegexCercaCadena(liniaSubstituida, "TwipsPerPixel(\s+' MigratPixels)+") Then
                        liniaSubstituida = RegexSubstitucioCadena(liniaSubstituida, "TwipsPerPixel(\s+' MigratPixels)+", "TwipsPerPixel      ' MigratPixels")

                        SubstituirLinia(fitxer, numlinia, liniaSubstituida)
                        Continue For
                    End If


                    '
                    If linia.ToUpper.Contains("MIGRATPIXELS") Then Continue For

                    SubstituirLinia(fitxer, numlinia, liniaSubstituida)

                    lvTwipsPixels.Items.Remove(lvTwipsPixels.SelectedItems(0))

                    comptadorLiniesTwipsPixels = lvTwipsPixels.Items.Count - 1

                    lblProcessant.Text = String.Format("Total linies amb GetTwipsToPixels/GetPixelsToTwips : {0}", comptadorLiniesTwipsPixels)
                Next


            Case "AplicaXNumregla"

                For Each lvis As ListViewItem In lvTwipsPixels.SelectedItems

                    '! numregla de la fila seleccionada
                    Dim numreglaS As String = lvis.SubItems(3).Text

                    '! recorrem tots els items
                    For Each lvi As ListViewItem In lvTwipsPixels.Items
                        If lvi.SubItems.Item(3).Text = numreglaS Then   '! comprovem si coincideix el numero de regla

                            '! recuperem la fila del lvTwipsPixels
                            'Dim id As String = lvi.SubItems(0).Text
                            Dim linia As String = lvi.SubItems(1).Text    '! linia que hem de substituir
                            Dim liniaSubstituida As String = lvi.SubItems(2).Text  '! possible substitució
                            Dim numregla As String = lvi.SubItems(3).Text     '! numero de regla
                            Dim fitxer As String = lvi.SubItems(4).Text     '! fitxer on està la linia que hem de substituir
                            Dim numlinia As String = lvi.SubItems(5).Text     '! numero de linia


                            If linia.ToUpper.Contains("MIGRATPIXELS") Then Continue For
                            If numregla = -1 Then Continue For

                            SubstituirLinia(fitxer, numlinia, liniaSubstituida)

                            lvTwipsPixels.Items.Remove(lvi)

                            comptadorLiniesTwipsPixels = comptadorLiniesTwipsPixels - 1

                            lblProcessant.Text = String.Format("Total linies amb GetTwipsToPixels/GetPixelsToTwips : {0}", comptadorLiniesTwipsPixels)
                        End If

                    Next

                Next

            Case "Descartar"
                For Each i As ListViewItem In lvTwipsPixels.SelectedItems
                    lvTwipsPixels.Items.Remove(i)

                    comptadorLiniesTwipsPixels = comptadorLiniesTwipsPixels - 1
                Next

                lblProcessant.Text = String.Format("Total linies amb GetTwipsToPixels/GetPixelsToTwips : {0} ", comptadorLiniesTwipsPixels)

            Case Else

        End Select
    End Sub

    ''' <summary>
    ''' Substitueix una linia d'un fitxer per una altra linia
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <param name="numlinia"></param>
    ''' <param name="novaLinia"></param>
    Private Sub SubstituirLinia(ByVal fitxer As String, ByVal numlinia As Integer, ByVal novaLinia As String)
        Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxer)

        Dim linies As List(Of String) = LlegeixFitxer(fitxerProcessar, encoding)
        Dim rootFitxer As String

        '! substituim per la nova linia
        linies.Item(numlinia) = novaLinia

        If chkMigrarBackup.Checked Then
            rootFitxer = cboDirectoryVB6_BACKUP.Text
        Else
            rootFitxer = cboDirectoryNet.Text
        End If

        '! gravem
        File.WriteAllLines(fitxerProcessar, linies.ToArray, GetFileEncoding(fitxerProcessar))
    End Sub

    Private Sub SubstituirLiniaXFitxer(ByVal fitxer As String, ByVal numlinia As Integer, ByVal novaLinia As String, linies As List(Of String))
        Dim fitxerProcessar As String = CalculaFitxerProcessar(fitxer)

        '! substituim per la nova linia
        linies.Item(numlinia) = novaLinia
    End Sub

    ''' <summary>
    ''' Permet canviar l'estat dels workarounds d'arquitectura
    ''' </summary>
    ''' <param name="liniaLvi"></param>
    Private Sub SubstituirRevisatNoRevisatSense(ByVal liniaLvi As ListViewItem, ByVal nouEstat As ColorUpgrade)
        Dim fitxerWorkAroundsArquitectura As String = String.Format("{0}\{1}", Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory).ToString).ToString, "workarounds_arquitectura.txt")
        Dim linies As List(Of String) = LlegeixFitxer(fitxerWorkAroundsArquitectura, encoding)
        Dim novesLinies As New List(Of String)

        For Each l As String In linies
            If Split(l, "||").Count = 1 Then Continue For

            If Split(l, "||")(2) = liniaLvi.SubItems(2).Text Then  '! comparem per descripcio
                Dim criticitat As String = "[" & liniaLvi.SubItems(1).Text & "]"
                Dim url As String = liniaLvi.SubItems(4).Text
                Dim descripcio As String = liniaLvi.SubItems(2).Text

                '! substituim per la nova linia
                If nouEstat = ColorUpgrade.Resolt Then
                    Dim novalinia As String = criticitat & "||" & url & "||" & descripcio & "||" & "ColorUpgrade.Resolt"
                    novesLinies.Add(novalinia)
                ElseIf nouEstat = ColorUpgrade.Altres Then
                    Dim novalinia As String = criticitat & "||" & url & "||" & descripcio & "||" & "ColorUpgrade.Altres"
                    novesLinies.Add(novalinia)
                ElseIf nouEstat = ColorUpgrade.Sense Then
                    Dim novalinia As String = criticitat & "||" & url & "||" & descripcio & "||" & "ColorUpgrade.Sense"
                    novesLinies.Add(novalinia)
                End If
            Else  '! deixem la mateixa linia
                novesLinies.Add(l)
            End If

        Next

        '! gravem
        File.WriteAllLines(fitxerWorkAroundsArquitectura, novesLinies.ToArray, GetFileEncoding(fitxerWorkAroundsArquitectura))

        '! refresquem el listview
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub lvWorkAroundPropertyCanOnly_MouseDown(sender As Object, e As MouseEventArgs) ''''Handles lvWorkAroundPropertyCanOnly.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            treeListViewUpgradesSelected = lviFontsTrueType.GetItemAt(e.X, e.Y)

            If treeListViewUpgradesSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()
                'Dim aplicaMenuItem As MenuItem = New MenuItem("Aplica")
                'AddHandler aplicaMenuItem.Click, AddressOf AccionsLvUpgradesClick
                'm.MenuItems.Add(aplicaMenuItem)

                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionsLvUpgradesClick
                m.MenuItems.Add(altresMenuItem)

                m.Show(treeListViewUpgrades, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    Private Sub treeListViewUpgrades_MouseDown(sender As Object, e As MouseEventArgs) Handles treeListViewUpgrades.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            treeListViewUpgradesSelected = treeListViewUpgrades.GetItemAt(e.X, e.Y)

            If treeListViewUpgradesSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()
                'Dim aplicaMenuItem As MenuItem = New MenuItem("Aplica")
                'AddHandler aplicaMenuItem.Click, AddressOf AccionsLvUpgradesClick
                'm.MenuItems.Add(aplicaMenuItem)

                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionsLvUpgradesClick
                m.MenuItems.Add(altresMenuItem)

                m.Show(treeListViewUpgrades, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    Private Sub treeListViewUpgrades_MouseClick(sender As Object, e As MouseEventArgs) Handles treeListViewUpgrades.MouseClick
        Dim url As String = String.Empty

        Dim s As TreeListViewItem = treeListViewUpgrades.SelectedItems(0)

        If s.SubItems.Count < 3 Then Exit Sub

        '! si en la columna "solucio" o "Observacio" o "Descripcio" hi ha una Url, l'obrim
        If s.SubItems(2).Text.ToUpper.Contains("HTTP") OrElse s.SubItems(3).Text.ToUpper.Contains("HTTP") OrElse s.SubItems(4).Text.ToUpper.Contains("HTTP") Then
            If s.SubItems(2).Text.ToUpper.Contains("HTTP") Then url = s.SubItems(2).Text
            If s.SubItems(3).Text.ToUpper.Contains("HTTP") Then url = s.SubItems(3).Text
            If s.SubItems(4).Text.ToUpper.Contains("HTTP") Then url = s.SubItems(4).Text
        Else
            Exit Sub
        End If

        Try
            '! Per resoldre el tema que hi hagi espais en el nom de la solucio            
            Dim urlcometa As String = caracterDobleCometa & url & caracterDobleCometa

            System.Diagnostics.Process.Start(urlcometa)

        Catch ex As Exception
            MessageBox.Show("No es pot obrir l'html!!")
        End Try
    End Sub

    Private Sub lviArquitectura_MouseDown(sender As Object, e As MouseEventArgs) Handles lviArquitectura.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lviArquitecturaSelected = lviArquitectura.GetItemAt(e.X, e.Y)

            If lviArquitecturaSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()
                Dim aplicaRevisatMenuItem As MenuItem = New MenuItem("Revisat")
                AddHandler aplicaRevisatMenuItem.Click, AddressOf AccionsLvAsWorkAroundArquitectura
                m.MenuItems.Add(aplicaRevisatMenuItem)

                Dim aplicaNoRevisatMenuItem As MenuItem = New MenuItem("NoRevisat")
                AddHandler aplicaNoRevisatMenuItem.Click, AddressOf AccionsLvAsWorkAroundArquitectura
                m.MenuItems.Add(aplicaNoRevisatMenuItem)

                Dim aplicaSenseMenuItem As MenuItem = New MenuItem("NA")
                AddHandler aplicaSenseMenuItem.Click, AddressOf AccionsLvAsWorkAroundArquitectura
                m.MenuItems.Add(aplicaSenseMenuItem)

                m.Show(lviArquitectura, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    Private Sub lviFormsRedisseny_MouseDown(sender As Object, e As MouseEventArgs) Handles lviFormsRedisseny.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lviFormsRedissenySelected = lviFormsRedisseny.GetItemAt(e.X, e.Y)

            If lviFormsRedissenySelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()
                Dim aplicaMenuItem As MenuItem = New MenuItem("Select")
                AddHandler aplicaMenuItem.Click, AddressOf AccionslviFormsRedisseny
                m.MenuItems.Add(aplicaMenuItem)

                Dim aplicaPerNumReglaMenuItem As MenuItem = New MenuItem("UnSelect")
                AddHandler aplicaPerNumReglaMenuItem.Click, AddressOf AccionslviFormsRedisseny
                m.MenuItems.Add(aplicaPerNumReglaMenuItem)

                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionslviFormsRedisseny
                m.MenuItems.Add(altresMenuItem)

                m.Show(lviFormsRedisseny, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Menu AccionsLvColeccio_1_0Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lviFormularis_Sense_Caption_MouseDown(sender As Object, e As MouseEventArgs) Handles lviFormularis_Sense_Caption.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lviFormularis_Sense_CaptionSelected = lviFormularis_Sense_Caption.GetItemAt(e.X, e.Y)

            If lviFormularis_Sense_CaptionSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()
                Dim aplicaMenuItem As MenuItem = New MenuItem("Aplica")
                AddHandler aplicaMenuItem.Click, AddressOf AccionslviFormularis_Sense_Caption
                m.MenuItems.Add(aplicaMenuItem)

                m.Show(lviFormularis_Sense_Caption, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Menu AccionsLvColeccio_1_0Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lviColeccio_1_0_MouseDown(sender As Object, e As MouseEventArgs) Handles lviColeccio1_0.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lviColeccio_1_0Selected = lviColeccio1_0.GetItemAt(e.X, e.Y)

            If lviColeccio_1_0Selected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()
                Dim aplicaMenuItem As MenuItem = New MenuItem("Aplica")
                AddHandler aplicaMenuItem.Click, AddressOf AccionsLvColeccio_1_0Click
                m.MenuItems.Add(aplicaMenuItem)

                Dim aplicaPerNumReglaMenuItem As MenuItem = New MenuItem("AnarLinia")
                AddHandler aplicaPerNumReglaMenuItem.Click, AddressOf AccionsLvColeccio_1_0Click
                m.MenuItems.Add(aplicaPerNumReglaMenuItem)

                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionsLvColeccio_1_0Click
                m.MenuItems.Add(altresMenuItem)

                m.Show(lviColeccio1_0, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Menu lviAsObject
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lviAsObject_MouseDown(sender As Object, e As MouseEventArgs) Handles lviAsObject.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lviAsObjectSelected = lviAsObject.GetItemAt(e.X, e.Y)

            If lviAsObjectSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()
                Dim aplicaMenuItem As MenuItem = New MenuItem("Aplica")
                AddHandler aplicaMenuItem.Click, AddressOf AccionsLvAsObjectClick
                m.MenuItems.Add(aplicaMenuItem)

                Dim aplicaPerNumReglaMenuItem As MenuItem = New MenuItem("AnarLinia")
                AddHandler aplicaPerNumReglaMenuItem.Click, AddressOf AccionsLvAsObjectClick
                m.MenuItems.Add(aplicaPerNumReglaMenuItem)

                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionsLvAsObjectClick
                m.MenuItems.Add(altresMenuItem)

                m.Show(lviAsObject, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    Private Sub lviArquitectura_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles lviArquitectura.MouseDoubleClick
        Dim url As String = String.Empty

        Dim info As ListViewHitTestInfo = lviArquitectura.HitTest(e.X, e.Y)

        If info.Item Is Nothing Then Exit Sub

        '! si en la columna "solucio" o "Observacio" o "Descripcio" hi ha una Url, l'obrim
        If info.Item.SubItems(4).Text.ToUpper.Contains("HTTP") Then
            url = info.Item.SubItems(4).Text
        Else
            Exit Sub
        End If

        Try
            '! Per resoldre el tema que hi hagi espais en el nom de la solucio            
            Dim urlcometa As String = caracterDobleCometa & url & caracterDobleCometa

            System.Diagnostics.Process.Start(urlcometa)

        Catch ex As Exception
            MessageBox.Show("No es pot obrir l'html!!")
        End Try
    End Sub

    Private Sub chkHOWTO_CheckedChanged(sender As Object, e As EventArgs) Handles chkHOWTO.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkINFO_CheckedChanged(sender As Object, e As EventArgs) Handles chkINFO.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkPRB_CheckedChanged(sender As Object, e As EventArgs) Handles chkPRB.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkBUG_CheckedChanged(sender As Object, e As EventArgs) Handles chkBUG.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkGeneral_CheckedChanged(sender As Object, e As EventArgs) Handles chkGeneral.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkLanguage_CheckedChanged(sender As Object, e As EventArgs) Handles chkLanguage.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkClasses_CheckedChanged(sender As Object, e As EventArgs) Handles chkClasses.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkFormsControls_CheckedChanged(sender As Object, e As EventArgs) Handles chkFormsControls.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkRevisats_CheckedChanged(sender As Object, e As EventArgs) Handles chkRevisats.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkNoRevisats_CheckedChanged(sender As Object, e As EventArgs) Handles chkNoRevisats.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkADO_CheckedChanged(sender As Object, e As EventArgs) Handles chkADO.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkDatabase_CheckedChanged(sender As Object, e As EventArgs) Handles chkDatabase.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub chkNA_CheckedChanged(sender As Object, e As EventArgs) Handles chkNA.CheckedChanged
        CalculaWorkAroundsArquitectura("")
    End Sub

    Private Sub txtCercaWorkAround_TextChanged(sender As Object, e As EventArgs) Handles txtCercaWorkAround.TextChanged
        CalculaWorkAroundsArquitectura(txtCercaWorkAround.Text)
        '        lviArquitectura.Refresh()
    End Sub

    Private Sub lstItemsFoundNet_ItemChecked(sender As Object, e As ItemCheckedEventArgs) Handles lstItemsFoundNet.ItemChecked
        calRecarregarUpgrades = True
        calRecarregarNomMetodeEvent_Igual_NomEventHandler = True
        calRecarregarAround_AsObject = True
        calRecarregarLiniesTwipsPixels = True
        calRecarregarTreeViewDinsAxGroupBox = True
        calRecarregarFontsTrueType = True
    End Sub

    Private Sub chkMigrarDesigner_CheckedChanged(sender As Object, e As EventArgs) Handles chkMigrarDesigner.CheckedChanged
        If Not Me.IsHandleCreated Then Exit Sub

        BuscarFitxersNet()
    End Sub

    Private Sub chkMigrarCodi_CheckedChanged(sender As Object, e As EventArgs) Handles chkMigrarCodi.CheckedChanged
        If Not Me.IsHandleCreated Then Exit Sub

        BuscarFitxersNet()
    End Sub

    Private Sub btnWizard2008_Click(sender As Object, e As EventArgs) Handles btnWizard2008.Click

        '! 1. Eliminem la carpeta XXXXX.NET, si existeix
        Dim carpetaNET() As String = Directory.GetDirectories(cboDirectoryVB6_BACKUP.Text, "*.NET")
        If carpetaNET.Count > 0 Then
            Directory.Delete(carpetaNET(0), True)
        End If


        '! 2. Busquem el fitxer vbp/VB6 que es el que farem servir per migrar (Wizard2008)
        Dim dir As New System.IO.DirectoryInfo(cboDirectoryVB6_BACKUP.Text)

        Dim fileList = dir.GetFiles("*.*", System.IO.SearchOption.TopDirectoryOnly)

        Dim fitxersProjecte = From file In fileList
                              Where file.Extension = EXTENSIO_VBP
                              Order By file.Name
                              Select file

        If fitxersProjecte.Count = 0 Then
            MessageBox.Show("Cal executar els passos 1 i 2")

            Exit Sub
        End If

        '! Per resoldre el tema que hi hagi espais en el nom de la solucio       
        Dim nomVBP As String = caracterDobleCometa & fitxersProjecte(0).FullName & caracterDobleCometa

        If fitxersProjecte.Count > 0 Then
            System.Diagnostics.Process.Start("C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe", nomVBP)

            '! activem el buto "PostMigracio"
            btnPostMigracio.Enabled = True
        End If
    End Sub

    Private Sub btnPreWizard_EnabledChanged(sender As Object, e As EventArgs) Handles btnPreWizard.EnabledChanged
        If btnPreWizard.Enabled = True Then
            btnPreWizard.BackColor = System.Drawing.SystemColors.ActiveCaption
        Else
            btnPreWizard.BackColor = System.Drawing.SystemColors.ControlDark
        End If
    End Sub

    Private Sub btnPostMigracio_EnabledChanged(sender As Object, e As EventArgs) Handles btnPostMigracio.EnabledChanged
        If btnPostMigracio.Enabled = True Then
            btnPostMigracio.BackColor = System.Drawing.SystemColors.ActiveCaption
        Else
            btnPostMigracio.BackColor = System.Drawing.SystemColors.ControlDark
        End If
    End Sub

    Private Sub btnCrearBackups_EnabledChanged(sender As Object, e As EventArgs) Handles btnCrearBackups.EnabledChanged
        If btnCrearBackups.Enabled = True Then
            btnCrearBackups.BackColor = System.Drawing.SystemColors.ActiveCaption
        Else
            btnCrearBackups.BackColor = System.Drawing.SystemColors.ControlDark
        End If
    End Sub

    Private Sub btnWizard2008_EnabledChanged(sender As Object, e As EventArgs) Handles btnWizard2008.EnabledChanged
        If btnWizard2008.Enabled = True Then
            btnWizard2008.BackColor = System.Drawing.SystemColors.ActiveCaption
        Else
            btnWizard2008.BackColor = System.Drawing.SystemColors.ControlDark
        End If
    End Sub

    Private Sub btnValidacions_EnabledChanged(sender As Object, e As EventArgs) Handles btnValidacions.EnabledChanged
        If btnValidacions.Enabled = True Then
            btnValidacions.BackColor = System.Drawing.SystemColors.ActiveCaption
        Else
            btnValidacions.BackColor = System.Drawing.SystemColors.ControlDark
        End If
    End Sub

    Private Sub btnTestos_EnabledChanged(sender As Object, e As EventArgs) Handles btnTestos.EnabledChanged
        If btnTestos.Enabled = True Then
            btnTestos.BackColor = System.Drawing.SystemColors.ActiveCaption
        Else
            btnTestos.BackColor = System.Drawing.SystemColors.ControlDark
        End If
    End Sub

    Private Sub ToolTip1_Popup(sender As Object, e As PopupEventArgs) Handles ToolTip1.Popup
        Dim messageBoxCS As System.Text.StringBuilder = New System.Text.StringBuilder()
        messageBoxCS.AppendFormat("{0} = {1}", "AssociatedWindow", e.AssociatedWindow)
        messageBoxCS.AppendLine()
        messageBoxCS.AppendFormat("{0} = {1}", "AssociatedControl", e.AssociatedControl)
        messageBoxCS.AppendLine()
        messageBoxCS.AppendFormat("{0} = {1}", "IsBalloon", e.IsBalloon)
        messageBoxCS.AppendLine()
        messageBoxCS.AppendFormat("{0} = {1}", "ToolTipSize", e.ToolTipSize)
        messageBoxCS.AppendLine()
        messageBoxCS.AppendFormat("{0} = {1}", "Cancel", e.Cancel)
        messageBoxCS.AppendLine()
        MessageBox.Show(messageBoxCS.ToString(), "Popup Event")
    End Sub

    Private Sub ToolTip1_Draw(sender As Object, e As DrawToolTipEventArgs) Handles ToolTip1.Draw

    End Sub

    Private Sub lviBugs_MouseMove(sender As Object, e As MouseEventArgs) Handles lviBugs.MouseMove
        Dim item As ListViewItem = lviBugs.GetItemAt(e.X, e.Y)
        If Not item Is Nothing AndAlso Not item Is Nothing Then
            If item.Text = "" Then
                Dim tip = New ToolTip
                tip.Active = True
                tip.UseAnimation = True

                tip.BackColor = Color.Firebrick
                tip.ToolTipTitle = "Solució"
                tip.ShowAlways = False
                tip.AutoPopDelay = 1000
                tip.Show(item.ToolTipText, Me, lviBugs.Location.X + e.X, Me.TabControl1.Location.Y + e.Y, 1000)
            End If
        End If
    End Sub

    Private Sub btnRefrescarVista_Click(sender As Object, e As EventArgs) Handles btnRefrescarVista.Click
        If TabControl1.SelectedTab.TabIndex = 1 Then
            CalculaUpgrades()

            RefrescarProces("Upgrade", treeListViewUpgrades.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 2 Then
            CalculaWorkAround_NomMetodeEvent_Igual_NomEventHandler()

            RefrescarProces("NomMetodeEvent_Igual_NomEventHandler", lvWarningsWorkAround.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 4 Then   ' Migracio Twips-Pixels
            CalculaWorkAround_LiniesTwipsPixels()

            RefrescarProces("TwipsPixels", lvTwipsPixels.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 5 Then
            CalculaWorkAround_AsObject()

            RefrescarProces("Total : AsObject", lviAsObject.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 7 Then     ' Fonts TrueType
            CalculaWorkAround_FontsTrueType()

            RefrescarProces("Total : Fonts TrueType", lviFontsTrueType.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 8 Then     ' TreeView dins AxGroupBox
            CalculaWorkAround_TreeViewDinsAxGroupBox()

            RefrescarProces("Total : TreeViewDinsAxGroupBox", lviTreeViewAxGroupBox.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 9 Then     ' formularis si estan redissenyat o no des de l'ultim proces de migracio
            CalculaFormsRedisseny()

            RefrescarProces("Total : Forms Redisseny", lviFormsRedisseny.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 6 Then
            CalculaWorkAroundsArquitectura("")

            RefrescarProces("Total : Arquitectura", lviArquitectura.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 10 Then
            CalculaWorkAroundsBugs()

            RefrescarProces("Total : Bugs", lviBugs.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 11 Then
            CalculaWorkAround_Coleccio_1_0()

            RefrescarProces("Total : Coleccio_1_0", lviColeccio1_0.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 12 Then
            CalculaWorkAround_Formularis_Sense_Caption()

            RefrescarProces("Total : Bugs", lviFormularis_Sense_Caption.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 13 Then
            CalculaWorkAround_NomClasseUserControl_igual_NomInstancia()

            RefrescarProces("Total : Bugs", lviFormularis_Sense_Caption.Items.Count, "", "")

        ElseIf TabControl1.SelectedTab.TabIndex = 14 Then
            CalculaWorkAround_FormLoad()

            RefrescarProces("Total : FormLoad", lvFormLoad.Items.Count, "", "")
        End If
    End Sub

    Private Sub lviBugs_MouseDown(sender As Object, e As MouseEventArgs) Handles lviBugs.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lviBugsSelected = lviBugs.GetItemAt(e.X, e.Y)

            If lviBugsSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()

                Dim aplicaMenuItem As MenuItem = New MenuItem("Aplica")
                AddHandler aplicaMenuItem.Click, AddressOf AccionsLvBugsClick
                m.MenuItems.Add(aplicaMenuItem)

                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionsLvBugsClick
                m.MenuItems.Add(altresMenuItem)

                m.Show(lviBugs, New Point(e.X, e.Y))
            End If
        End If
    End Sub

    '? dimecres 2
    'Const resxFilename As String = "M:\MP_Net\MigracioVB6_Net\FrmEntitats.resx"
    Const resxFilename As String = "M:\MP_Net\MigracioVB6_Net\CountryHeaders.resx"

    ''' <summary>
    ''' Sincronitza les dates de redisseny dels fitxers
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Button1_Click(sender As Object, e As EventArgs)

        'Dim kk As Design.AxWrapperGen = New Design.AxWrapperGen(GetType(AxXtremeSuiteControls.AxRadioButton))
        'Dim jj = kk.GeneratedSources


        'CreateResourceFile()

        Dim rsxr As ResXResourceReader = New ResXResourceReader(resxFilename)

        For Each d As DictionaryEntry In rsxr
            Console.WriteLine(d.Key.ToString() & ":" & vbTab + d.Value.ToString())
        Next

        rsxr.Close()
    End Sub

    '? dimecres 2
    Private Sub CreateResourceFile()
        Dim rw As ResXResourceWriter = New ResXResourceWriter(resxFilename)
        'Dim resNames As String() = {"Country", "Population", "Area", "Capital", "LCity"}
        Dim resNames As String() = {"Size"}
        'Dim columnHeaders As String() = {"Country Name", "Population (2010}", "Area", "Capital", "Largest City"}
        Dim columnHeaders As Object() = {New Size(29, 29)}
        'Dim comments As String() = {"The localized country name", "", "The area in square miles", "", "The largest city based on 2010 data"}
        'rw.AddResource("Title", "Country Information")
        'rw.AddResource("nColumns", resNames.Length)

        For ctr As Integer = 0 To resNames.Length - 1
            Dim node As ResXDataNode = New ResXDataNode(resNames(ctr), columnHeaders(ctr))
            'node.Comment = comments(ctr)
            rw.AddResource(node)
        Next

        rw.Generate()
        rw.Close()
    End Sub

    Private Sub btnSincrBACKUP_NET_Click(sender As Object, e As EventArgs) Handles btnSincrBACKUP_NET.Click
        Dim liniesFitxerRedisseny As List(Of String) = LlegeixFitxer(cboDirectoryNet.Text & FITXER_REDISSENY, encoding)

        Sincronitza_Fitxers_BACKUP_I_NET(liniesFitxerRedisseny)
    End Sub

    Private Sub chkMigracioIncremental_CheckedChanged(sender As Object, e As EventArgs) Handles chkMigracioIncremental.CheckedChanged
        If chkMigracioIncremental.Checked Then
            flagMigracioIncremental = True
        Else
            flagMigracioIncremental = False
        End If
    End Sub


    ' ******************************************************************************************************
    ' CONVERSIONS IMPLICITAS : Option Strict Off/On
    ' ******************************************************************************************************
    'Option Strict On no permite conversiones implÃ­citas de 'Boolean' a 'ResizeEnum'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Double' a 'Integer'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Double' a 'Short'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Integer' a 'IntPtr'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Integer' a 'MsgBoxStyle'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Integer' a 'Short'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Keys' a 'Short'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Object' a 'AxRadioButton'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Object' a 'Integer'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Object' a 'Label'.						
    'Option Strict On no permite conversiones implÃ­citas de 'Object' a 'String'.						
    'Option Strict On no permite conversiones implÃ­citas de 'String' a 'Short'.						
    'Option Strict On no permite operandos de tipo Object para el operador '<>'. Use el operador 'Is' para probar la identidad del objeto.						
    'Option Strict On no permite restricciones del tipo 'Control' al tipo 'AxCheckBox' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Control' al tipo 'GmsCombo' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Control' al tipo 'GmsData' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Control' al tipo 'GmsImports' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Control' al tipo 'TextBox' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'FormParent' al tipo 'frmkPrensa' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'FRM' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Object' al tipo 'AxCheckBox' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Object' al tipo 'FormParent' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Formulari' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Object' al tipo 'frmEntidadesVisitadasPersContacto' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Formulari' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Object' al tipo 'GmsCombo' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Object' al tipo 'GmsData' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Object' al tipo 'GmsImports' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Object' al tipo 'TextBox' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Ctrl' en el argumento correspondiente.						
    'Option Strict On no permite restricciones del tipo 'Single' al tipo 'Integer' al copiar de nuevo el valor del parÃ¡metro 'ByRef' 'Y' en el argumento correspondiente.						




End Class

Public Class ListViewItemComparer
    Implements IComparer

    Private col As Integer
    Private order As SortOrder

    Public Sub New()
        col = 0
        order = SortOrder.Ascending
    End Sub

    Public Sub New(column As Integer, order As SortOrder)
        col = column
        Me.order = order
    End Sub

    Public Function Compare(x As Object, y As Object) As Integer Implements System.Collections.IComparer.Compare
        Dim returnVal As Integer = -1

        Try
            If IsNumeric(Integer.Parse(CType(x, ListViewItem).SubItems(col).Text)) And
                IsNumeric(Integer.Parse(CType(y, ListViewItem).SubItems(col).Text)) Then

                '! Compare as numeric
                returnVal = Val(CType(x, ListViewItem).SubItems(col).Text).CompareTo(Val(CType(y, ListViewItem).SubItems(col).Text))

            Else
                '! If not numeric then compare as string
                returnVal = [String].Compare(CType(x, ListViewItem).SubItems(col).Text, CType(y, ListViewItem).SubItems(col).Text)
            End If

        Catch ex As Exception

        End Try

        '! If order is descending then invert value
        If order = SortOrder.Descending Then
            returnVal *= -1
        End If

        Return returnVal

    End Function

End Class




'<Compile Include = "..\..\Common\2018\frmFPTablas.designer.vb" >
'      <Link>frmFPTablas.designer.vb</Link>
'<DependentUpon> frmFPTablas.vb</DependentUpon>
'    </Compile>
'    <Compile Include = "..\..\Common\2018\frmFPTablas.vb" >
'      <Link>frmFPTablas.vb</Link>
'<SubType> Form</SubType>
'    </Compile>



'    <EmbeddedResource Include = "..\..\Common\2018\frmFPTablas.resX" >
'      <Link>frmFPTablas.resX</Link>
'<DependentUpon> frmFPTablas.vb</DependentUpon>
'    </EmbeddedResource>



#End Region

