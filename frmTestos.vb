﻿Imports System.Drawing
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Windows.Forms

''' <summary>
''' Defineix "testos unitaris"
''' </summary>
Public Class frmTestos

    Public lv As ListView.CheckedListViewItemCollection
    Public pathProjecte As String

    Dim caracterDobleCometa As String = New String("""")


    '? AxXtremeSuiteControls.AxPushButton
    '? AxXtremeSuiteControls.AxDateTimePicker
    '? AxXtremeSuiteControls.AxCheckBox
    '? AxXtremeSuiteControls.AxGroupBox
    '? AxXtremeSuiteControls.AxRadioButton


    '"AxXtremeSuiteControls.AxPushButton"
    'System.Windows.Forms.TextBox
    'Public WithEvents txtDataIni As DataControl.GmsData
    'Public WithEvents cmdExit As AxXtremeSuiteControls.AxPushButton
    'Public WithEvents cmbCombo As DataControl.GmsCombo
    'Public WithEvents grdHoresTreballEfectiu As FlexCell.Grid
    'Public WithEvents Label5 As System.Windows.Forms.Label


    ''' <summary>
    ''' ListViewItem del listview lvResultat seleccionat
    ''' </summary>
    Dim lvResultatSelected As ListViewItem = Nothing


    'Enum E_TipusControl
    '    System.Windows.Forms.TextBox = 0      '! XtremeSuiteControls.TabControlPage
    '    Grid = 1                '! FlexCell.Grid
    '    PushButton = 2          '! XtremeSuiteControls.PushButton
    '    GmsData = 3             '! DataControl.GmsData
    '    SSDBCombo = 4           '! SSDataWidgets_B.SSDBCombo migrat a gmsCombo
    '    CheckBox = 5            '! XtremeSuiteControls.CheckBox
    '    GroupBox = 6            '! XtremeSuiteControls.GroupBox
    'End Enum


    ''' <summary>
    ''' 
    ''' </summary>
    Structure ControlNET
        Dim nomControl As String
        Dim tipusControl As String
    End Structure

    Public Class Test
        Friend descripcio As String
        Friend esRegex As Boolean
        Friend expressioRegularCerca As String
        Friend expressioRegularSubstitucio As String
        Friend compara As Boolean
        Friend operador As String
        Friend tipusControlEsperat As String

        Public Sub New(ByVal _descripcio As String, ByVal _esRegex As Boolean, ByVal _expressioRegularCerca As String, ByVal _expressioRegularSubstitucio As String, _compara As Boolean, _operador As String, _tipusControlEsperat As String)
            descripcio = _descripcio
            esRegex = _esRegex
            expressioRegularCerca = _expressioRegularCerca
            expressioRegularSubstitucio = _expressioRegularSubstitucio
            compara = _compara
            operador = _operador
            tipusControlEsperat = _tipusControlEsperat
        End Sub

    End Class

    ' NOTA : Hem detectat que quan no hi ha el "Handles" és perquè en VB6 no existeix el control associat però s'ha deixat el mètode d'event


    Dim lstTestosSenseHandles As List(Of Test) = New List(Of Test) From
    {
        New Test("EventHandler GotFocus sense Handles", True, "(?i)Private Sub (\w+)_GotFocus\(\)", "Private Sub $1_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles $1.GotFocus", False, "", "*"),
        New Test("EventHandler LostFocus sense Handles", True, "(?i)Private Sub (\w+)_LostFocus\(\)", "Private Sub $1_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles $1.LostFocus", False, "", "*"),
        New Test("EventHandler DoubleClick sense Handles", True, "(?i)Private Sub (\w+)_DoubleClick\(\)", "Private Sub $1_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles $1.DoubleClick", False, "", "*"),
        New Test("EventHandler Click sense Handles", True, "(?i)Private Sub (cmd\w+)_Click\(\)", "Private Sub $1_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles $1.ClickEvent", True, "", "*")
    }

    ' Definicio dels "testos unitaris" que tenim
    Dim lstTestosNomDiferent As List(Of Test) = New List(Of Test) From
    {
        New Test("FindRootNode", True, "FindRootNode\(([\w+\(\)\.]+)\)", "$1.TreeView.TopNode", False, "", "System.Windows.Forms.TreeView"),
        New Test("Nom mètode event diferent) - TextBox/ResizeEvent", True, "(?i)\s(txt\w+)_ResizeEvent\(", " $1_Resize(", False, "", "System.Windows.Forms.TextBox"),
        New Test("Nom mètode event diferent) - TextBox/ClickEvent", True, "(?i)\s(txt\w+)_ClickEvent\(", " $1_Click(", False, "", "System.Windows.Forms.TextBox"),
        New Test("Nom mètode event diferent) - GmsCombo/ClickEvent", True, "(?i)\s(cmb\w+)_ClickEvent\(", " $1_Click(", False, "", "DataControl.GmsCombo"),
        New Test("Nom mètode event diferent) - Grid/MouseUp", True, "(?i)\s(grd\w+)_MouseUpEvent\(", " $1_MouseUp(", False, "", "FlexCell.Grid"),
        New Test("Nom mètode event diferent) - Grid/MouseDown", True, "(?i)\s(grd\w+)_MouseDownEvent\(", " $1_MouseDown(", False, "", "FlexCell.Grid"),
        New Test("Nom mètode event diferent) - Grid/MouseMove", True, "(?i)\s(grd\w+)_MouseMoveEvent\(", " $1_MouseMove(", False, "", "FlexCell.Grid"),
        New Test("Nom mètode event diferent) - Grid/ClickEvent", True, "(?i)\s(grd\w+)_ClickEvent\(", " $1_Click(", False, "", "FlexCell.Grid"),
        New Test("Nom mètode event diferent) - Grid/DblClick", True, "(?i)(\w+)_DblClick\(", "$1_DoubleClick(", False, "", "FlexCell.Grid"),
        New Test("Nom mètode event diferent) - KeyPressEvent", True, "(?i)(\w+)_KeyPressEvent\(", "$1_KeyPress(", False, "", "*"),
        New Test("Nom mètode event diferent) - KeyUpEvent", True, "(?i)(\w+)_KeyUpEvent\(", "$1_KeyUp(", False, "", "*"),
        New Test("Nom mètode event diferent) - AfterCheck", True, "(?i)(\w+)_AfterCheckEvent\(", "$1_AfterCheck(", False, "", "System.Windows.Forms.TreeView"),
        New Test("Nom mètode event diferent) - RadioButton/ClickEvent", True, "(?i)\s(opt\w+)_Click\(", " $1_ClickEvent(", False, "", "GmsAxRadioButtonArray"),
        New Test("Nom mètode event diferent) - AxRadioButton/Click", True, "(?i)\s(opt\w+)_Click\(", " $1_ClickEvent(", False, "", "AxXtremeSuiteControls.AxRadioButton"),
        New Test("Nom mètode event diferent) - AxRadioButton/MouseUp", True, "(?i)\s(opt\w+)_MouseUp\(", " $1_MouseUpEvent(", False, "", "AxXtremeSuiteControls.AxRadioButton"),
        New Test("Nom mètode event diferent) - PushButton/ClickEvent", True, "(?i)\s(cmd\w+)_Click\(", " $1_ClickEvent(", False, "", "AxXtremeSuiteControls.AxPushButton"),
        New Test("Nom mètode event diferent) - Validating", True, "Private Sub (\w+)_Validate\([A-Za-z0-9-\s-,-.]+\) Handles (\w+)\.Validating", "Private Sub $1_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles $2.Validating", False, "", "*"),
        New Test("Nom mètode event diferent) - DoubleClick", True, "Private Sub (\w+)_DblClick\([A-Za-z0-9-\s-,-.]+\) Handles (\w+)\.DoubleClick", "Private Sub $1_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles $2.DoubleClick", False, "", "*"),
        New Test("Nom mètode event diferent) - NodeMouseClick", True, "Private Sub (\w+)_NodeClick\([A-Za-z0-9-\s-,-.]+\) Handles (\w+)\.NodeMouseClick", "Private Sub $1_NodeMouseClick(ByVal eventSender As System.Object, ByVal eventArgs As TreeNodeMouseClickEventArgs) Handles $2.NodeMouseClick", False, "", "System.Windows.Forms.TreeView"),
        New Test("Nom mètode event diferent) - DblClick", True, "Handles (\w+).DblClick", "Handles $1.DoubleClick", False, "", "*"),
        New Test("Handles - AxRadioButton/Click", True, "(?i)(Handles opt\w+\.Click)\s*$", "$1Event", False, "", "AxXtremeSuiteControls.AxRadioButton"),
        New Test("Falta Overrides en GravaRegistre (BC40005)", True, "(?i)^(?=.*Function GravaRegistre\(\))(?!.*Overrides).*", "Overrides Function GravaRegistre() As Boolean", False, "", ""),
        New Test("Falta Overrides en MoveReg (BC40005)", True, "(?i)^(?=.*Sub MoveReg\(\))(?!.*Overrides).*", "Overrides Sub MoveReg()", False, "", ""),
        New Test("Falta Overrides en MoveReg amb paràmetre (BC40005)", True, "(?i)^(?=.*Sub MoveReg\((ByRef \w+ As String)\))(?!.*Overrides).*", "Overrides Sub MoveReg($1)", False, "", ""),
        New Test("Falta Overrides en Inici (BC40005)", True, "(?i)^(?=.*Sub Inici\(\))(?!.*Overrides).*", "Overrides Sub Inici()", False, "", ""),
        New Test("Falta Overrides en SaveReg (BC40005)", True, "(?i)^(?=.*Sub SaveReg\(\))(?!.*Overrides).*", "Overrides Sub SaveReg()", False, "", ""),
        New Test("Falta Overrides en SaveReg (BC40005)", True, "(?i)^(?=.*Sub SaveReg\(\w+\))(?!.*Overrides).*", "Overrides Sub SaveReg()", False, "", ""),   ' revisar el cas
        New Test("Falta Overrides en FGetReg (BC40005)", True, "(?i)^(?=.*Sub FGetReg\(\))(?!.*Overrides).*", "Overrides Sub FGetReg()", False, "", ""),
        New Test("Falta Overrides en Copiar (BC40005)", True, "(?i)^(?=.*Sub Copiar\(\))(?!.*Overrides).*", "Overrides Sub Copiar()", False, "", ""),
        New Test("Falta Overrides en Pegar (BC40005)", True, "(?i)^(?=.*Sub Pegar\(\))(?!.*Overrides).*", "Overrides Sub Pegar()", False, "", ""),
        New Test("Falta Overrides en DeleteReg (BC40005)", True, "(?i)^(?=.*Sub DeleteReg\(\))(?!.*Overrides).*", "Overrides Sub DeleteReg()", False, "", ""),
        New Test("Falta Overrides en Llistar (BC40005)", True, "(?i)^(?=.*Sub Llistar\(\))(?!.*Overrides).*", "Overrides Sub Llistar()", False, "", ""),
        New Test("Falta Overrides en GetCodi (BC40005)", True, "(?i)^(?=.*Sub GetCodi\(\))(?!.*Overrides).*", "Overrides Sub GetCodi()", False, "", ""),
        New Test("Falta Overrides en Consultar (BC40005)", True, "(?i)^(?=.*Sub Consultar\(\))(?!.*Overrides).*", "Overrides Sub Consultar()", False, "", ""),
        New Test("Falta Overrides en ObreFormAp (BC40005)", True, "(?i)^(?=.*Sub ObreFormAp\(\w+\))(?!.*Overrides).*", "Overrides Sub ObreFormAp()", False, "", ""),   ' revisar el cas
        New Test("Falta Overrides en Clone (BC40005)", True, "(?i)^(?=.*Sub Clone\(\))(?!.*Overrides).*", "Overrides Sub Clone()", False, "", ""),
        New Test("Declaracions As Object", True, "Dim \w+ As Object", "", False, "", ""),
        New Test("Node Treeview Selected", True, "\w+\.Selected =", "", False, "", "System.Windows.Forms.TreeView"),
        New Test("TreeView nodes", True, "(?i)(Tv\w*)\.Nodes", "GetAllNodes($1)", False, "", "System.Windows.Forms.TreeView"),
        New Test("TreeView nodes", True, "(?i)(Trv\w*)\.Nodes", "GetAllNodes($1)", False, "", "System.Windows.Forms.TreeView"),
        New Test("TreeView nodes", True, "(?i)(menutreeview\w*)\.Nodes", "GetAllNodes($1)", False, "", "System.Windows.Forms.TreeView"),
        New Test("Desactivacio Timer", True, "timer\w+\.Interval = 0", "", False, "", ""),
        New Test("TwipsToPixels", True, "(\w*)GetTwipsToPixelsX\(([\(+\w+\.\s\d+\+\-\/\\\)-*+]*)\)+", "", False, "", ""),
        New Test("TwipsToPixels", True, "(\w*)GetTwipsToPixelsY\(([\(+\w+\.\s\d+\+\-\/\\\)-*+]*)\)+", "", False, "", ""),
        New Test("PixelsToTwips", True, "GetPixelsToTwipsX\(([\w.\(\)]+)\)", "$1", False, "", ""),
        New Test("PixelsToTwips", True, "GetPixelsToTwipsY\(([\w.\(\)]+)\)", "$1", False, "", ""),
        New Test("PROVA", True, "GetPixelsToTwipsY\(([\w.\(\)]+)", "$1", False, "", ""),
        New Test("Cancel", True, "Cancel = True", "", False, "", ""),
        New Test("e.Row", True, "If Row ", "", False, "", ""),
        New Test(" * 15 (Twips)", True, "\* 15", "", False, "", ""),
        New Test("'UPGRADE_WARNING: El límite inferior de la matriz ([\w+-\.])+ ha cambiado de 1 a 0.", True, "0F1C9BE1-AF9D-476E-83B1-17D43BECFF20", "", False, "", ""),
        New Test("'UPGRADE_WARNING: La propiedad Timer ([\w+-\.])+ no puede tener un valor de 0", True, "169ECF4A-1968-402D-B243-16603CC08604", "", False, "", ""),
        New Test("'UPGRADE_WARNING: Se detectó el uso de Null/IsNull()", True, "2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A", "", False, "", ""),
        New Test("'UPGRADE_WARNING: \w+ objeto no se actualizó", True, "371DAEFA-CEB3-4528-82E8-213DF0EA63C3", "", False, "", ""),
        New Test("'UPGRADE_WARNING: El tamaño de la cadena de longitud fija debe caber en el búfer", True, "3C1E4426-0B80-443E-B943-0627CD55D48B", "", False, "", ""),
        New Test("'UPGRADE_WARNING: La variable \w+ no se actualizó", True, "671167DC-EA81-475D-B690-7A40C7BF4A23", "", False, "", ""),
        New Test("'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto", True, "6A50421D-15FE-4896-8A1B-2EC21E9037B2", "", False, "", ""),
        New Test("'UPGRADE_WARNING: El comportamiento de \w+ puede ser diferente", True, "6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32", "", False, "", ""),
        New Test("'UPGRADE_WARNING: ([\w+-\.])+ propiedad ([\w+-\.])+ tiene un nuevo comportamiento", True, "6BA9B8D2-2A32-4B6E-8D36-44949974A5B4", "", False, "", ""),
        New Test("'UPGRADE_WARNING: Puede que necesite inicializar las matrices de la estructura NtAuthority", True, "814DF224-76BD-4BB4-BFFB-EA359CB9FC48", "", False, "", ""),
        New Test("'UPGRADE_WARNING: La propiedad \w+ de \w+ no se admite en Visual Basic .NET", True, "8B377936-3DF7-4745-AA26-DD00FA5B9BE1", "", False, "", ""),
        New Test("'UPGRADE_WARNING: El evento \w+ \w+ no se admite", True, "92F3B58C-F772-4151-BE90-09F4A232AEAD", "", False, "", ""),
        New Test("'UPGRADE_WARNING: XXXXXXX y tiene un nuevo comportamiento", True, "9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7", "", False, "", ""),
        New Test("'UPGRADE_WARNING: La estructura \w+ puede requerir que se pasen atributos de cálculo de referencia", True, "C429C3A5-5D47-4CD9-8F51-74A1616405DC", "", False, "", ""),
        New Test("'UPGRADE_WARNING: La propiedad \w+ no se actualizó", True, "D94970AE-02E7-43BF-93EF-DCFCD10D27B5", "", False, "", ""),
        New Test("'UPGRADE_WARNING: El comportamiento del método \w+ ha cambiado", True, "DBD08912-7C17-401D-9BE9-BA85E7772B99", "", False, "", ""),
        New Test("'UPGRADE_WARNING: XXXXXXXXX que tiene un nuevo comportamiento", True, "DFCDE711-9694-47D7-9C50-45A99CD8E91E", "", False, "", ""),
        New Test("'UPGRADE_WARNING: XXXXXXXXX no puede devolver un entero", True, "E863E941-79BB-4962-A003-8309A0B2BB28", "", False, "", ""),
        New Test("'UPGRADE_WARNING: La creación de instancias de clase cambió a pública", True, "ED41034B-3890-49FC-8076-BD6FC2F42A85", "", False, "", ""),
        New Test("'UPGRADE_ISSUE: No se actualizó", True, "55B59875-9A95-4B71-9D6A-7C294BF7139D", "", False, "", ""),
        New Test("'UPGRADE_ISSUE: No se actualizó", True, "FB723E3C-1C06-4D2B-B083-E6CD0D334DA8", "", False, "", ""),
        New Test("'UPGRADE_ISSUE: \w+ propiedad \w+\.\w+ no se actualizó", True, "CC4C7EC0-C903-48FC-ACCC-81861D12DA4A", "", False, "", ""),
        New Test("'UPGRADE_ISSUE:\sControl\s\w+\sno\sse\spudo\sresolver\sporque\sestá\sdentro\sdel\sespacio\sde\snombres\s\w*", True, "084D22AD-ECB1-400F-B4C7-418ECEC5E36E", "", False, "", ""),
        New Test("'UPGRADE_ISSUE: \w+ propiedad \w+\.\w+ no se admite en tiempo de ejecución", True, "74E732F3-CAD8-417B-8BC9-C205714BB4A7", "", False, "", ""),
        New Test("'UPGRADE_ISSUE: ([\w+-\.])+ objeto no se actualizó", True, "6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305", "", False, "", ""),
        New Test("'UPGRADE_NOTE: El bloque #If #EndIf no se actualizó", True, "27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC", "", False, "", ""),
        New Test("'UPGRADE_NOTE: ([\w+-\.])+ pasó de ser un evento a un procedimiento", True, "4E2DC008-5EDA-4547-8317-C9316952674F", "", False, "", ""),
        New Test("'UPGRADE_NOTE: El objeto ([\w+-\.])+ no se puede destruir hasta que no se realice la recolección de los elementos no utilizados", True, "6E35BFF6-CD74-4B09-9689-3E1A43DF8969", "", False, "", ""),
        New Test("'UPGRADE_NOTE: IsMissing() ha cambiado a IsNothing()", True, "8AE1CB93-37AB-439A-A4FF-BE3B6760BB23", "", False, "", ""),
        New Test("'UPGRADE_NOTE: El nombre de la variable ([\w+-\.])+ se cambió a ([\w+-\.])+", True, "94ADAC4D-C65D-414F-A061-8FDC6B83C5EC", "", False, "", ""),
        New Test("'UPGRADE_NOTE: \w+ se actualizó a \w+", True, "A9E4979A-37FA-4718-9994-97DD76ED70A7", "", False, "", ""),
        New Test("'UPGRADE_NOTE: La propiedad \w+ se marcó con comentarios", True, "B3FC1610-34F3-43F5-86B7-16C984F0E88E", "", False, "", ""),
        New Test("'UPGRADE_NOTE: El tipo del argumento se ha cambiado a Object", True, "D0BD8832-D1AC-487C-8AA5-B36F9284E51E", "", False, "", ""),
        New Test("'UPGRADE_TODO: Se debe llamar a Initialize para inicializar instancias de esta estructura", True, "B4BFF9E0-8631-45CF-910E-62AB3970F27B", "", False, "", ""),
        New Test("'UPGRADE_TODO: Quite los comentarios y cambie la siguiente línea para devolver el enumerador de colecciones", True, "95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C", "", False, "", "")
    }

    '    New Test("EventHandler Grid/MouseUp sense Handles", True, "Private Sub \s(grd\w+)_MouseUp\(ByRef Button As Short, ByRef Shift As Short, ByRef X As Single, ByRef Y As Single\)", "Private Sub $1_MouseUp(Sender As Object, eventArgs As MouseEventArgs) Handles $1.MouseUp", False, "", "FlexCell.Grid"),

    '    New Test("Nom mètode event = nom del control (case-sensitive) - GotFocus", True, "Private Sub (\w+)_\w+\([A-Za-z0-9-\s-,-.]+\) Handles (\w+)\.GotFocus", "", True, "=", "*"),
    '    New Test("Nom mètode event = nom del control (case-sensitive) - LostFocus", True, "Private Sub (\w+)_\w+\([A-Za-z0-9-\s-,-.]+\) Handles (\w+)\.LostFocus", "", True, "=", "*"),
    '    New Test("Nom mètode event = nom del control (case-sensitive) - Leave", True, "Private Sub (\w+)_\w+\([A-Za-z0-9-\s-,-.]+\) Handles (\w+)\.Leave", "", True, "=", "*"),
    '    New Test("Nom mètode event = nom del control (case-sensitive) - SelectedIndexChanged", True, "Private Sub (\w+)_\w+\([A-Za-z0-9-\s-,-.]+\) Handles (\w+)\.SelectedIndexChanged", "", True, "=", "System.Windows.Forms.ComboBox"),
    '    New Test("Nom mètode event = nom del control (case-sensitive) - Enter", True, "Private Sub (\w+)_\w+\([A-Za-z0-9-\s-,-.]+\) Handles (\w+)\.Enter", "", True, "=", "*"),

    '    New Test("Nom mètode event = nom del control (case-sensitive) - GmsCombo/Enter", True, "(?i)\s(cmb\w+)_Enter\(", "$1_GotFocus(", False, "", "DataControl.GmsCombo"),
    '    New Test("Nom mètode event = nom del control (case-sensitive) - GmsCombo/Leave", True, "(?i)\s(cmb\w+)_Leave\(", "$1_LostFocus(", False, "", "DataControl.GmsCombo"),
    '    New Test("Nom mètode event = nom del control (case-sensitive) - TextBox/Enter", True, "(?i)\s(txt\w+)_Enter\(", "$1_GotFocus(", False, "", "System.Windows.Forms.TextBox"),
    '    New Test("Nom mètode event = nom del control (case-sensitive) - TextBox/Leave", True, "(?i)\s(txt\w+)_Leave\(", "$1_LostFocus(", False, "", "System.Windows.Forms.TextBox"),
    '    New Test("Nom mètode event = nom del control (case-sensitive) - CheckBox/Enter", True, "((?i)chk\w+)_Enter\(", "$1_GotFocus(", False, ""),
    '    New Test("Nom mètode event = nom del control (case-sensitive) - CheckBox/Leave", True, "((?i)chk\w+)_Leave\(", "$1_LostFocus(", False, ""),



    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fitxer"></param>
    ''' <param name="encoding"></param>
    ''' <returns></returns>
    Private Function LlegeixFitxer(ByVal fitxer As String, ByVal encoding As Encoding) As List(Of String)
        If Not File.Exists(fitxer) Then
            MsgBox(String.Format("El fitxer {0} no existeix !!!!", fitxer))
            Return Nothing
        End If

        Dim linies As String() = File.ReadAllLines(fitxer, encoding)

        '! retornem totes les linies del fitxer
        Return New List(Of String)(linies)
    End Function

    ''' <summary>
    ''' Retorna els controls NET a partir del fitxer Designer
    ''' </summary>
    ''' <param name="fitxerDesigner"></param>
    ''' <returns></returns>
    Private Function ObtenirControlsByDesigner(ByVal fitxerDesigner As String) As List(Of ControlNET)
        If Not File.Exists(fitxerDesigner) Then Return New List(Of ControlNET)

        Dim liniesFitxerDesigner As List(Of String) = LlegeixFitxer(fitxerDesigner, GetFileEncoding(fitxerDesigner))
        Dim splitList() As String = {}
        Dim lstControlsNET As New List(Of ControlNET)

        For i As Integer = 0 To liniesFitxerDesigner.Count - 1
            If RegexCercaCadena(liniesFitxerDesigner(i), "Public WithEvents (\w+) As (([\w+-\.])*)", splitList) Then
                Dim controlNET As New ControlNET()

                controlNET.nomControl = splitList(1)
                controlNET.tipusControl = splitList(2)

                lstControlsNET.Add(controlNET)
            End If
        Next

        Return lstControlsNET
    End Function


    Private Sub frmTestos_Load(sender As Object, e As EventArgs) Handles Me.Load
        NetejarListView(lvTestos)
        NetejarListView(lvResultat)

        Dim lstTestos As New List(Of Test)
        lstTestos.AddRange(lstTestosSenseHandles)
        lstTestos.AddRange(lstTestosNomDiferent)


        For Each test As Test In lstTestos
            Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"", test.descripcio, test.esRegex, test.expressioRegularCerca, test.expressioRegularSubstitucio, test.compara, test.operador, test.tipusControlEsperat}, -1)

            If test.descripcio.Contains("sense Handles") Then
                ListViewItem1.BackColor = Color.LightBlue
            ElseIf test.descripcio.Contains("Nom mètode event diferent") Then
                ListViewItem1.BackColor = Color.LightSlateGray
            ElseIf test.descripcio.Contains("Falta Overrides") Then
                ListViewItem1.BackColor = Color.LightCyan
            ElseIf test.descripcio.Contains("UPGRADE_WARNING") Then
                ListViewItem1.BackColor = Color.LightCoral
            ElseIf test.descripcio.Contains("UPGRADE_ISSUE") Then
                ListViewItem1.BackColor = Color.LightGoldenrodYellow
            ElseIf test.descripcio.Contains("UPGRADE_NOTE") Then
                ListViewItem1.BackColor = Color.LightSkyBlue
            ElseIf test.descripcio.Contains("TwipsToPixels") Then
                ListViewItem1.BackColor = Color.LightCyan
            ElseIf test.descripcio.Contains("TreeView Nodes") Then
                ListViewItem1.BackColor = Color.LightSeaGreen
            End If


            Me.lvTestos.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1})
        Next

    End Sub

    ''' <summary>
    ''' Executa els testos seleccionats del ListView
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnExecutarTestos_Click(sender As Object, e As EventArgs) Handles btnExecutarTestos.Click
        NetejarListView(lvResultat)

        Cursor = Cursors.WaitCursor
        Dim numFitxer As Integer = 0

        lblTotalFitxers.Text = lv.Count.ToString
        lblTotalFitxers.Refresh()

        Try
            For Each lvi In lv
                Dim linies As List(Of String) = New List(Of String)(File.ReadAllLines(Me.pathProjecte & lvi.SubItems(0).Text, Encoding.Default))

                numFitxer = numFitxer + 1

                lblFitxer.Text = lvi.SubItems(0).Text
                lblFitxer.Refresh()
                lblNumFitxer.Text = numFitxer.ToString
                lblNumFitxer.Refresh()

                '! obtenime els controls del formulari
                Dim controls As List(Of ControlNET) = ObtenirControlsByDesigner(RegexSubstitucioCadena(Me.pathProjecte & lvi.Text, "\.vb$", ".Designer.vb"))

                For Each linia As String In linies

                    For Each lviTest As ListViewItem In lvTestos.Items

                        ' executem els checked
                        If lviTest.Checked Then
                            Dim substrings() As String = {}

                            ' subItems(3) = Expressio regular cerca
                            If RegexCercaCadena(linia, lviTest.SubItems(3).Text, substrings) Then

                                '? excluim les linies que son comentaris
                                If RegexCercaCadena(linia, "^\s*'") Then Exit For


                                '! busquem el control
                                Dim control As ControlNET = controls.Where(Function(s) s.nomControl = substrings(1)).FirstOrDefault

                                '? hem de comprovar si el tipus de control és realment el que és
                                '? Ens hem trobat casos on el nom era, exemple cmbSeguent (teòricament cmb --> Combo) i realment es un AxPushButton
                                If control.tipusControl IsNot Nothing AndAlso control.tipusControl <> lviTest.SubItems(7).Text AndAlso lviTest.SubItems(7).Text <> "*" Then
                                    MessageBox.Show(String.Format("Detectat control {0} no coincideix el tipus segons patró.Linia : {1}", control.nomControl, linia))

                                    Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"", linia, lvi.SubItems(0).Text, "xxxxxxxxxxxxxxxx", linies.IndexOf(linia) + 1}, -1)
                                    ListViewItem1.BackColor = Color.IndianRed

                                    lvResultat.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1})

                                    Exit For

                                ElseIf control.tipusControl Is Nothing Then  '! no té el corresponent Handles (= no té definit el corresponent WithEvents)
                                    Dim possibleSubstitucio As String = String.Empty

                                    possibleSubstitucio = RegexSubstitucioCadena(linia, lviTest.SubItems(3).Text, lviTest.SubItems(4).Text)

                                    Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"", linia, lvi.SubItems(0).Text, possibleSubstitucio, linies.IndexOf(linia) + 1}, -1)
                                    ListViewItem1.BackColor = Color.PaleVioletRed

                                    lvResultat.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1})

                                    Exit For
                                End If

                                ' compara
                                If lviTest.SubItems(5).Text = "True" Then

                                    ' operador
                                    Select Case lviTest.SubItems(6).Text
                                        Case "="
                                            If Not String.Equals(substrings(1), substrings(2), StringComparison.CurrentCultureIgnoreCase) Then
                                                Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"", linia, lvi.SubItems(0).Text}, -1)

                                                lvResultat.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1})
                                            End If

                                    End Select

                                Else ' només cerca
                                    Dim possibleSubstitucio As String = String.Empty

                                    ' Si es TwipsToPixels, calculem la possible substitucio
                                    If lviTest.SubItems(1).Text = "TwipsToPixels" Then
                                        possibleSubstitucio = CalculaPossibleCadenaSubstitucioTwipsPixels(linia, lviTest.SubItems(3).Text)
                                    End If

                                    ' Si es PixelsToTwips, calculem la possible substitucio
                                    If lviTest.SubItems(1).Text = "PixelsToTwips" Then
                                        possibleSubstitucio = RegexSubstitucioCadena(linia, lviTest.SubItems(3).Text, lviTest.SubItems(4).Text)
                                    End If

                                    possibleSubstitucio = RegexSubstitucioCadena(linia, lviTest.SubItems(3).Text, lviTest.SubItems(4).Text)

                                    Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"", linia, lvi.SubItems(0).Text, possibleSubstitucio, linies.IndexOf(linia) + 1}, -1)

                                    lvResultat.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1})
                                End If

                            End If

                        End If
                    Next
                Next

            Next

            Cursor = Cursors.Default

        Catch ex As Exception
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Function RegexCercaCadena(ByVal cadena As String, ByVal patro As String, Optional ByRef substrings() As String = Nothing) As Boolean
        Dim rgx As New Regex(patro)

        If rgx.IsMatch(cadena) Then
            If substrings IsNot Nothing Then
                substrings = Regex.Split(cadena, patro)
            End If

            Return True
        End If

        Return False
    End Function

    Private Function RegexSubstitucioCadena(ByVal cadena As String, ByVal patro As String, ByVal patroSubstitucio As String) As String
        Dim rgx As New Regex(patro)

        Return rgx.Replace(cadena, patroSubstitucio)
    End Function

    ''' <summary>
    ''' Neteja totes les "files" d'un ListView
    ''' </summary>
    ''' <param name="lv"></param>
    Private Sub NetejarListView(ByVal lv As ListView)
        Do While lv.Items.Count <> 0
            lv.Items.Remove(lv.Items(0))
        Loop

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cadenaACalcular"></param>
    ''' <param name="patro"></param>
    ''' <returns></returns>
    Private Function CalculaPossibleCadenaSubstitucioTwipsPixels(ByVal cadenaACalcular As String, ByVal patro As String) As String
        Dim novalinia As String = String.Empty
        Dim splitList() As String = {}

        Dim pos1 As Integer = cadenaACalcular.IndexOf("(")
        Dim pos2 As Integer = cadenaACalcular.IndexOf(")")
        Dim longitud As Integer = pos2 - pos1 - 1
        Dim resto As String = cadenaACalcular.Substring(pos2 + 1)

        ' Comprovem si hem de "tractar" les "X" o les "Y"

        If cadenaACalcular.Contains("GetTwipsToPixelsX") Then
            novalinia = SubstituirTwipsPixels(cadenaACalcular, patro, "X")
        Else
            novalinia = SubstituirTwipsPixels(cadenaACalcular, patro, "Y")
        End If

        Return novalinia
    End Function

    Private Function SubstituirTwipsPixels(ByVal cadena As String, ByVal patro As String, ByVal xy As String) As String
        Dim splitList() As String = {}

        Dim resultat As Boolean = RegexCercaCadena(cadena, patro, splitList)

        Dim cadenaConcatenada As String = ConcatenarLlista(splitList)

        If Not resultat Then Return cadena

        ' obtenim els valors numerics
        Dim splitList2() As String = {}
        Dim teNumeros As Boolean = RegexCercaCadena(cadenaConcatenada, "(\d+)", splitList2)

        cadenaConcatenada = String.Empty

        If Not teNumeros Then
            For Each l As String In splitList
                cadenaConcatenada = cadenaConcatenada & l
            Next

            Return cadenaConcatenada
        Else  ' te numeros
            Dim i = 0
            For Each l As String In splitList2
                If i Mod 2 = 1 AndAlso CInt(l) <> 2 Then
                    l = CStr(Math.Round(CInt(l) / 15))   ' calculem a pixels
                End If

                cadenaConcatenada = cadenaConcatenada & l
                i = i + 1
            Next
        End If

        '''''Return RegexSubstitucioCadena(cadena, patro, cadenaConcatenada)
        Return cadenaConcatenada
    End Function

    Private Function ConcatenarLlista(llista As String()) As String
        Dim cadenaConcatenada As String = String.Empty

        For Each l As String In llista
            cadenaConcatenada = cadenaConcatenada & l
        Next

        Return cadenaConcatenada
    End Function

    ''' <summary>
    ''' Substituiexi una linia d'un fitxer per una nova cadena
    ''' </summary>
    ''' <param name="path"></param>
    ''' <param name="novaCadena"></param>
    ''' <param name="numlinia"></param>
    Private Sub SubstituirLiniaFitxer(ByVal path As String, ByVal novaCadena As String, ByVal numlinia As Integer)
        Dim objReader As New StreamReader(pathProjecte & path, GetFileEncoding(pathProjecte & path))

        Dim sLine As String = ""
        Dim arrText As New List(Of String)
        Do
            sLine = objReader.ReadLine()
            If Not sLine Is Nothing Then
                arrText.Add(sLine)
            End If
        Loop Until sLine Is Nothing
        objReader.Close()

        ' substituim
        arrText.Item(numlinia - 1) = novaCadena

        ' gravem
        File.WriteAllLines(pathProjecte & path, arrText.ToArray, GetFileEncoding(pathProjecte & path))
    End Sub

    Private Sub btnSubstitucio_Click(sender As Object, e As EventArgs) Handles btnSubstitucio.Click
        If lvResultat.SelectedItems.Count = 0 Then
            MessageBox.Show("No has seleccionat cap fila")
            Exit Sub
        End If

        For fila = lvResultat.SelectedItems.Count - 1 To 0 Step -1
            ' 2 = fitxer
            ' 3 = novaCadena
            ' 4 = num. linia
            SubstituirLiniaFitxer(lvResultat.SelectedItems(fila).SubItems(2).Text, lvResultat.SelectedItems(fila).SubItems(3).Text, CInt(lvResultat.SelectedItems(fila).SubItems(4).Text))

            ' ocultem la linia que hem substituit
            lvResultat.SelectedItems(fila).Remove()
        Next
    End Sub

    Public Function GetFileEncoding(ByVal srcFile As String) As Encoding
        Dim enc As Encoding = Encoding.GetEncoding(28591)
        Dim buffer As Byte() = New Byte(4) {}
        Dim file As FileStream = New FileStream(srcFile, FileMode.Open)
        file.Read(buffer, 0, 5)
        file.Close()
        If buffer(0) = 239 AndAlso buffer(1) = 187 AndAlso buffer(2) = 191 Then
            enc = Encoding.UTF8
        ElseIf buffer(0) = 254 AndAlso buffer(1) = 255 Then
            enc = Encoding.Unicode
        ElseIf buffer(0) = 0 AndAlso buffer(1) = 0 AndAlso buffer(2) = 254 AndAlso buffer(3) = 255 Then
            enc = Encoding.UTF32
        ElseIf buffer(0) = 43 AndAlso buffer(1) = 47 AndAlso buffer(2) = 118 Then
            enc = Encoding.UTF7
        End If

        Return enc
    End Function

    Private Sub lvResultat_MouseDown(sender As Object, e As MouseEventArgs) Handles lvResultat.MouseDown
        If e.Button = MouseButtons.Right Then

            '! recuperem el item seleccionat
            lvResultatSelected = lvResultat.GetItemAt(e.X, e.Y)

            If lvResultatSelected IsNot Nothing Then
                Dim m As ContextMenu = New ContextMenu()

                Dim aplicaMenuItem As MenuItem = New MenuItem("Aplica")
                AddHandler aplicaMenuItem.Click, AddressOf AccionslvResultat
                m.MenuItems.Add(aplicaMenuItem)

                Dim altresMenuItem As MenuItem = New MenuItem("Descartar")
                AddHandler altresMenuItem.Click, AddressOf AccionslvResultat
                m.MenuItems.Add(altresMenuItem)

                m.Show(lvResultat, New Point(e.X, e.Y))
            End If
        End If
    End Sub


    Private Sub AccionslvResultat(ByVal sender As Object, ByVal e As EventArgs)
        Dim menuItemSelected As MenuItem = TryCast(sender, MenuItem)

        Select Case menuItemSelected.Text
            Case "Aplica"

                Cursor = Cursors.WaitCursor

                For Each lvi As ListViewItem In lvResultat.SelectedItems
                    Dim noveslinies As New List(Of String)

                    '! recuperem la fila del lvResultat
                    'Dim id As String = lvi.SubItems(0).Text                   
                    Dim fitxerCodiNET As String = Me.pathProjecte & lvi.SubItems(2).Text
                    Dim liniaSubstitucio As String = lvi.SubItems(3).Text
                    Dim numLinia As String = lvi.SubItems(4).Text

                    Dim liniesFitxerDesigner As List(Of String) = LlegeixFitxer(fitxerCodiNET, GetFileEncoding(fitxerCodiNET))


                    SubstituirLinia(fitxerCodiNET, numLinia, liniaSubstitucio)
                Next

                For Each l As ListViewItem In lvResultat.SelectedItems
                    l.Remove()
                Next

                Cursor = Cursors.Default


            Case "Descartar"
                For Each i As ListViewItem In lvResultat.SelectedItems
                    lvResultat.Items.Remove(i)
                Next

            Case Else
        End Select

    End Sub

    Private Sub SubstituirLinia(ByVal fitxer As String, ByVal numlinia As Integer, ByVal novaLinia As String)
        Dim fitxerProcessar As String = fitxer   '''''CalculaFitxerProcessar(fitxer)

        Dim linies As List(Of String) = LlegeixFitxer(fitxerProcessar, GetFileEncoding(fitxerProcessar))

        '! substituim per la nova linia
        linies.Item(numlinia - 1) = novaLinia


        '! gravem
        File.WriteAllLines(fitxerProcessar, linies.ToArray, GetFileEncoding(fitxerProcessar))
    End Sub


    'Private Sub lvResultat_MouseUp(sender As Object, e As MouseEventArgs) Handles lvResultat.MouseUp
    '    '    'CHECK IF USER HAS RIGHT CLICKED
    '    If e.Button = System.Windows.Forms.MouseButtons.Right Then
    '        ' ActualitzaMenu()

    '        ContextMenuStrip.Show(lvResultat, e.Location)
    '    End If
    'End Sub

    ''' <summary>
    ''' ContextMenuItem
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub OcultaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OcultaToolStripMenuItem.Click
        If lvResultat.SelectedItems.Count = 0 Then Exit Sub

        For Each s As ListViewItem In lvResultat.SelectedItems
            s.Remove()
        Next
    End Sub

    ''' <summary>
    ''' ContextMenuItem
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ObrirFitxerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ObrirFitxerToolStripMenuItem.Click
        For Each s As ListViewItem In lvResultat.SelectedItems
            ' Per resoldre el tema que hi hagi espais en el nom de la solucio       
            Dim nomFitxer As String = caracterDobleCometa & pathProjecte & s.SubItems(2).Text & caracterDobleCometa

            Shell("C:\Program Files (x86)\Notepad++\notepad++.exe " & nomFitxer, AppWinStyle.MaximizedFocus)
        Next
    End Sub

    ''' <summary>
    ''' WorkAround : Ordering per columna
    ''' http://www.elguille.info/NET/dotnet/clasificarListView.htm
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lvresultat_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lvResultat.ColumnClick
        '
        ' ========================================
        ' Usando la clase ListViewColumnSortSimple
        ' ========================================
        '
        ' Crear una instancia de la clase que realizará la comparación
        Dim oCompare As New ListViewColumnSortSimple()
        '
        ' Asignar el orden de clasificación
        If lvResultat.Sorting = SortOrder.Ascending Then
            oCompare.Sorting = SortOrder.Descending
        Else
            oCompare.Sorting = SortOrder.Ascending
        End If
        lvResultat.Sorting = oCompare.Sorting
        '
        ' La columna en la que se ha pulsado
        oCompare.ColumnIndex = e.Column
        ' Asignar la clase que implementa IComparer
        ' y que se usará para realizar la comparación de cada elemento
        lvResultat.ListViewItemSorter = oCompare
        '
        ' Cuando se asigna ListViewItemSorter no es necesario llamar al método Sort
        'ListView1.Sort()
    End Sub

    ''' <summary>
    ''' WorkAround : Ordering per columna
    ''' http://www.elguille.info/NET/dotnet/clasificarListView.htm
    ''' </summary>
    Public Class ListViewColumnSortSimple
        ' Esta clase implementa la interfaz IComparer
        Implements IComparer
        '
        ' La columna por la que queremos clasificar
        Public ColumnIndex As Integer = 0
        ' El tipo de clasificación a realizar
        Public Sorting As SortOrder = SortOrder.Ascending
        '
        ' Función que se usará para comparar los dos elementos
        Public Overridable Function Compare(ByVal a As Object,
                                            ByVal b As Object) As Integer _
                                            Implements IComparer.Compare
            '
            ' Esta función devolverá:
            '   -1 si el primer elemento es menor que el segundo
            '    0 si los dos son iguales
            '    1 si el primero es mayor que el segundo
            '
            Dim menor, mayor As Integer
            Dim s1, s2 As String
            '
            ' Los objetos pasados a esta función serán del tipo ListViewItem.
            ' Convertir el texto en el formato adecuado
            ' y tomar el texto de la columna en la que se ha pulsado
            s1 = CType(a, ListViewItem).SubItems(ColumnIndex).Text
            s2 = CType(b, ListViewItem).SubItems(ColumnIndex).Text
            '
            ' Asignar cuando es menor o mayor, dependiendo del orden de clasificación
            Select Case Sorting
                Case SortOrder.Ascending
                    ' Esta es la forma predeterminada
                    menor = -1
                    mayor = 1
                Case SortOrder.Descending
                    ' invertimos los valores predeterminados
                    menor = 1
                    mayor = -1
                Case SortOrder.None
                    ' Todos los elementos se considerarán iguales
                    menor = 0
                    mayor = 0
            End Select
            '
            ' Realizamos la comparación y devolvemos el valor esperado
            If s1 < s2 Then
                Return menor
            ElseIf s1 = s2 Then
                Return 0
            Else
                Return mayor
            End If
        End Function
    End Class


End Class